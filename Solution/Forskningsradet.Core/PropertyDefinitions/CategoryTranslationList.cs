﻿using EPiServer.Core;
using EPiServer.PlugIn;
using Forskningsradet.Core.Models.ContentModels.PropertyList;

namespace Forskningsradet.Core.PropertyDefinitions
{
    [PropertyDefinitionTypePlugIn]
    public class CategoryTranslationListSubject : PropertyList<CategoryTranslationSubject>
    {
    }

    [PropertyDefinitionTypePlugIn]
    public class CategoryTranslationListTargetGroup : PropertyList<CategoryTranslationTargetGroup>
    {
    }

    [PropertyDefinitionTypePlugIn]
    public class CategoryTranslationListOther : PropertyList<CategoryTranslationOther>
    {
    }
}
