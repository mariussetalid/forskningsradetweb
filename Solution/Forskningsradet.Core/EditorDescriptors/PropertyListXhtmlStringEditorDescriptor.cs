﻿using System;
using System.Collections.Generic;
using EPiServer.Cms.TinyMce.Core;
using EPiServer.Core;
using EPiServer.ServiceLocation;
using EPiServer.Shell.ObjectEditing;
using EPiServer.Shell.ObjectEditing.EditorDescriptors;

namespace Forskningsradet.Core.EditorDescriptors
{
    /// <summary>
    /// EditorDescriptor to fix too wide XhtmlString fields in PropertyList items.
    /// Fix documented here: https://jakejon.es/blog/configuring-tinymce-when-its-used-in-an-episerver-propertylist
    /// </summary>
    [EditorDescriptorRegistration(TargetType = typeof(XhtmlString), EditorDescriptorBehavior = EditorDescriptorBehavior.OverrideDefault, UIHint = UIHint)]
    public class PropertyListXhtmlStringEditorDescriptor : XhtmlStringEditorDescriptor
    {
        public const string UIHint = "PropertyListXhtmlString";

        public PropertyListXhtmlStringEditorDescriptor(ServiceAccessor<TinyMceConfiguration> tinyMceConfiguration) : base(tinyMceConfiguration)
        {
        }

        public override void ModifyMetadata(ExtendedMetadata metadata, IEnumerable<Attribute> attributes)
        {
            base.ModifyMetadata(metadata, attributes);

            if (!metadata.EditorConfiguration.ContainsKey("settings"))
            {
                return;
            }

            var settings = metadata.EditorConfiguration["settings"] as TinyMceSettings;

            if (settings == null)
            {
                return;
            }

            // Set the size as desired
            settings.Width(450);
            settings.Height(200);
            settings.Resize(TinyMceResize.Vertical);
        }
    }
}