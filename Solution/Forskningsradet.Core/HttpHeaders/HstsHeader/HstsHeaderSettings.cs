﻿namespace Forskningsradet.Core.HttpHeaders.HstsHeader
{
    public class HstsHeaderSettings
    {
        public bool SkipHeader { get; set; }
        public int MaxAgeSeconds { get; set; }
        public bool IncludeSubdomains { get; set; }
        public bool Preload { get; set; }
    }
}