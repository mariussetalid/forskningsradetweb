﻿using System.Globalization;
using System.Linq;
using System.Web;
using Castle.Core.Internal;
using Forskningsradet.Core.Repositories.Contracts;

namespace Forskningsradet.Core.HttpHeaders.HstsHeader
{
    public class HstsHeaderRenderer : IHttpHeaderRenderer
    {
        private readonly IPageRepository _pageRepository;

        public HstsHeaderRenderer(IPageRepository pageRepository)
        {
            _pageRepository = pageRepository;
        }

        public void RenderHeader(HttpResponseBase response)
        {
            var settings = _pageRepository.GetCurrentStartPageAsFrontPageBase()?.HstsHeaderSettings?.Settings;

            if (settings?.SkipHeader == false)
            {
                var headerValue = string.Join("; ",
                    new[]
                        {
                            "max-age=" + settings.MaxAgeSeconds.ToString(CultureInfo.InvariantCulture),
                            settings.IncludeSubdomains ? "includeSubDomains" : null,
                            settings.Preload ? "preload" : null,
                        }
                        .Where(x => !x.IsNullOrEmpty())
                );

                response.Headers["Strict-Transport-Security"] = headerValue;
            }
        }
    }
}