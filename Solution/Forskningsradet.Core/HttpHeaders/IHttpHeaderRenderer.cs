﻿using System.Web;

namespace Forskningsradet.Core.HttpHeaders
{
    public interface IHttpHeaderRenderer
    {
        void RenderHeader(HttpResponseBase response);
    }
}