namespace Forskningsradet.Core.Logging
{
    public class LogMessage
    {
        public string Environment { get; set; }
        public string Level { get; set; }
        public string MessageObject { get; set; }
        public string RenderedMessage { get; set; }
        public string TimeStampUtc { get; set; }
        public string Logger { get; set; }
        public string Thread { get; set; }
        public string ExceptionObject { get; set; }
        public string ExceptionObjectString { get; set; }
        public string UserName { get; set; }
        public string Domain { get; set; }
        public string Identity { get; set; }
        public string Location { get; set; }
        public string CorrelationId { get; set; }
        public string Object { get; set; }
    }
}