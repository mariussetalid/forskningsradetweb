using System.Configuration;
using System.IO;
using System.Text;
using log4net.Appender;
using log4net.Core;
using Microsoft.ServiceBus.Messaging;
using Newtonsoft.Json;

namespace Forskningsradet.Core.Logging
{
    public class AzureLoggingAppender : AppenderSkeleton
    {
        protected override void Append(LoggingEvent loggingEvent)
        {
            var environment = ConfigurationManager.AppSettings["episerver:EnvironmentName"];
            if (string.IsNullOrEmpty(environment))
                return;

            var properties = loggingEvent.GetProperties();

            var logMessage = new LogMessage
            {
                Environment = environment,
                Level = loggingEvent.Level.DisplayName,
                MessageObject = Serialize(loggingEvent.MessageObject),
                RenderedMessage = RenderLoggingEvent(loggingEvent),
                TimeStampUtc = loggingEvent.TimeStamp.ToUniversalTime().ToString("O"),
                Logger = loggingEvent.LoggerName,
                Thread = loggingEvent.ThreadName,
                ExceptionObject = Serialize(loggingEvent.ExceptionObject),
                ExceptionObjectString = loggingEvent.ExceptionObject is null
                    ? null
                    : loggingEvent.GetExceptionString(),
                UserName = loggingEvent.UserName,
                Domain = loggingEvent.Domain,
                Identity = loggingEvent.Identity,
                Location = loggingEvent.LocationInformation.FullInfo,
                CorrelationId = properties["CorrelationId"]?.ToString(),
                Object = Serialize(properties["CustomObject"])
            };

            var message = new BrokeredMessage(new MemoryStream(Encoding.UTF8.GetBytes(Serialize(logMessage))))
            { ContentType = "application/json" };

            var client = QueueClient.CreateFromConnectionString(
                ConfigurationManager.AppSettings["QueueEndpoint"],
                ConfigurationManager.AppSettings["Logging.QueueName"]);
            client.Send(message);
        }

        private static string Serialize(object obj)
            => JsonConvert.SerializeObject(obj);
    }
}