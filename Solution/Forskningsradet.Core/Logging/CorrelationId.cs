using System;
using System.Diagnostics;

namespace Forskningsradet.Core.Logging
{
    public class CorrelationId
    {
        public override string ToString()
        {
            if (Trace.CorrelationManager.ActivityId == Guid.Empty)
                Trace.CorrelationManager.ActivityId = Guid.NewGuid();

            return Trace.CorrelationManager.ActivityId.ToString();
        }
    }
}