using System;
using log4net;

namespace Forskningsradet.Core.Logging
{
    public static class Log4NetExtensions
    {
        public static void Error(this ILog logger, string message, object obj, Exception exception = null)
            => Log(logger, LogLevel.Error, message, obj, exception);

        public static void Warning(this ILog logger, string message, object obj, Exception exception = null)
            => Log(logger, LogLevel.Warning, message, obj, exception);

        public static void Info(this ILog logger, string message, object obj, Exception exception = null)
            => Log(logger, LogLevel.Information, message, obj, exception);

        public static void Debug(this ILog logger, string message, object obj, Exception exception = null)
            => Log(logger, LogLevel.Debug, message, obj, exception);

        private static void Log(ILog logger, LogLevel level, string message, object obj, Exception exception)
        {
            LogicalThreadContext.Properties["CustomObject"] = obj;

            switch (level)
            {
                case LogLevel.Error:
                    logger.Error(message, exception);
                    break;
                case LogLevel.Warning:
                    logger.Warn(message, exception);
                    break;
                case LogLevel.Information:
                    logger.Info(message, exception);
                    break;
                case LogLevel.Debug:
                    logger.Debug(message, exception);
                    break;
            }

            LogicalThreadContext.Properties["CustomObject"] = null;
        }
    }
}