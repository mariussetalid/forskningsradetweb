using System.Web;
using Serilog.Core;
using Serilog.Events;

namespace Forskningsradet.Core.Logging
{
    public class IdentityEnricher : ILogEventEnricher
    {
        public void Enrich(LogEvent logEvent, ILogEventPropertyFactory propertyFactory)
        {
            var ctx = HttpContext.Current;

            if (ctx is null) return;
            logEvent.AddPropertyIfAbsent(propertyFactory.CreateProperty("Identity", ctx.User.Identity.Name));
        }
    }
}