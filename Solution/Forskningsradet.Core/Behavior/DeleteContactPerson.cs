using System;
using System.Linq;
using EPiServer;
using Forskningsradet.Core.Models.ApiModels.Idm;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Repositories.Contracts;
using MediatR;

namespace Forskningsradet.Core.Behavior
{
    public class DeleteContactPerson
    {
        public class Command : IRequest
        {
            public IdmContactPersonId Id { get; set; }
        }

        public class Handler : RequestHandler<Command>
        {
            private readonly IPageRepository _pageRepository;
            private readonly IContentRepository _contentRepository;

            public Handler(IPageRepository pageRepository, IContentRepository contentRepository)
            {
                _pageRepository = pageRepository;
                _contentRepository = contentRepository;
            }

            protected override void Handle(Command command)
            {
                if (!command.Id?.IsValid == true)
                    throw new ArgumentException("Command.Id cannot be empty");

                var pages = _contentRepository.GetChildren<PersonPage>(_pageRepository.GetCurrentStartPage()?.ContactPersonsPage);

                var personPage = command.Id.HasResourceId
                    ? pages.SingleOrDefault(x => x.ResourceId == command.Id.ResourceId.Value.ToString())
                    : pages.SingleOrDefault(x => x.ExternalId == command.Id.Guid.Value.ToString());

                if (personPage != null)
                    _pageRepository.UnPublishAllLanguageBranches<PersonPage>(personPage.PageLink);
            }
        }
    }
}