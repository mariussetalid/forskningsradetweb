using System.Globalization;
using System.Linq;
using EPiServer;
using Forskningsradet.Core.Models.ApiModels.Idm;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Repositories.Contracts;
using MediatR;

namespace Forskningsradet.Core.Behavior
{
    public class UpdateContactPerson
    {
        public class Command : IRequest
        {
            public IdmContactPerson ContactPerson { get; set; }
        }

        public class Handler : RequestHandler<Command>
        {
            private readonly IPageRepository _pageRepository;
            private readonly IContentRepository _contentRepository;

            public Handler(IPageRepository pageRepository, IContentRepository contentRepository)
            {
                _pageRepository = pageRepository;
                _contentRepository = contentRepository;
            }

            protected override void Handle(Command command)
            {
                var pages = _contentRepository.GetChildren<PersonPage>(_pageRepository.GetCurrentStartPage()?.ContactPersonsPage);

                var personPage = command.ContactPerson.Id.HasValue
                    ? pages.SingleOrDefault(x => x.ExternalId == command.ContactPerson.Id.ToString())
                    : null;

                if (personPage is null && !string.IsNullOrEmpty(command.ContactPerson.ResourceId))
                {
                    personPage = _contentRepository
                    .GetChildren<PersonPage>(_pageRepository.GetCurrentStartPage()?.ContactPersonsPage)
                    .SingleOrDefault(x => x.ResourceId == command.ContactPerson.ResourceId);
                }

                if (personPage != null && HasChanged(personPage, command.ContactPerson))
                {
                    UpdatePage(command, personPage);

                    var englishVersion = _contentRepository.Get<PersonPage>(personPage.PageLink, new CultureInfo("en"));
                    command.ContactPerson.Title = string.Empty;
                    command.ContactPerson.DepartmentName = string.Empty;
                    UpdatePage(command, englishVersion);
                }
            }

            private void UpdatePage(Command command, PersonPage personPage)
            {
                var updatedPage = personPage.CreateWritableClone() as PersonPage;
                MapToPage(updatedPage, command.ContactPerson);
                _pageRepository.Save(updatedPage);
            }

            private static bool HasChanged(PersonPage page, IdmContactPerson person) =>
                page.FirstName != person.FirstName
                || string.IsNullOrEmpty(page.ResourceId) && !string.IsNullOrEmpty(person.ResourceId)
                || page.LastName != person.LastName
                || page.JobTitle != person.Title
                || page.Department != person.DepartmentName
                || page.Phone != person.Phone
                || page.Mobile != person.Mobile
                || page.Email != person.Email
                || page.ResourceType != person.ResourceType
                || page.ExternalLastUpdate != person.AgressoLastUpdate
                || page.StopPublish.HasValue;

            private static void MapToPage(PersonPage page, IdmContactPerson person)
            {
                page.Name = $"{person.FirstName} {person.LastName}";

                if (string.IsNullOrEmpty(page.ResourceId))
                    page.ResourceId = person.ResourceId;
                page.FirstName = person.FirstName;
                page.LastName = person.LastName;
                page.JobTitle = person.Title;
                page.Department = person.DepartmentName;
                page.Mobile = person.Mobile;
                page.Phone = person.Phone;
                page.Email = person.Email;
                page.ResourceType = person.ResourceType;
                page.ExternalLastUpdate = person.AgressoLastUpdate;
                page.StopPublish = null;
            }
        }
    }
}