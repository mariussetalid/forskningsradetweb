using System;
using System.Collections.Generic;
using System.Linq;
using EPiServer;
using Forskningsradet.Core.Models.ApiModels.Idm;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Repositories.Contracts;
using MediatR;

namespace Forskningsradet.Core.Behavior
{
    public class GetContactPersons
    {
        public class Query : IRequest<IdmContactPersonsResult>
        {
            public Query() => SearchTerms = new List<string>();

            public List<string> SearchTerms { get; }
        }

        public class Handler : RequestHandler<Query, IdmContactPersonsResult>
        {
            private readonly IContentRepository _contentRepository;
            private readonly IPageRepository _pageRepository;

            public Handler(IContentRepository contentRepository, IPageRepository pageRepository)
            {
                _contentRepository = contentRepository;
                _pageRepository = pageRepository;
            }

            protected override IdmContactPersonsResult Handle(Query query)
            {
                var pages = _contentRepository
                    .GetChildren<PersonPage>(_pageRepository.GetCurrentStartPage()?.ContactPersonsPage);

                pages = _pageRepository.FilterPublished(pages)
                    .Where(x => HasExternalId(x) || HasResourceId(x));

                var contactPersons = ToContract(pages);

                return new IdmContactPersonsResult(contactPersons);

                bool HasExternalId(PersonPage p) =>
                    !string.IsNullOrEmpty(p.ExternalId)
                    &&
                    (query.SearchTerms.Count == 0 || query.SearchTerms.Contains(p.ExternalId));

                bool HasResourceId(PersonPage p) =>
                    !string.IsNullOrEmpty(p.ResourceId)
                    &&
                    (query.SearchTerms.Count == 0 || query.SearchTerms.Contains(p.ResourceId));
            }

            private static IEnumerable<IdmContactPerson> ToContract(IEnumerable<PersonPage> pages)
                => pages.Select(page => new IdmContactPerson
                {
                    Id = new Guid(page.ExternalId),
                    ResourceId = page.ResourceId,
                    FirstName = page.FirstName,
                    LastName = page.LastName,
                    Title = page.JobTitle,
                    DepartmentName = page.Department,
                    Mobile = page.Mobile,
                    Phone = page.Phone,
                    Email = page.Email,
                    ResourceType = page.ResourceType,
                    AgressoLastUpdate = page.ExternalLastUpdate
                });
        }
    }
}