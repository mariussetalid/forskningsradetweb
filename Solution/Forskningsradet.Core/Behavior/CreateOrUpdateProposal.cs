using System;
using Forskningsradet.Common.Constants;
using Forskningsradet.Common.Logging;
using Forskningsradet.Core.Models.ApiModels.Evurdering;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Processes.Contexts;
using Forskningsradet.Core.Services.Contracts;
using MediatR;

namespace Forskningsradet.Core.Behavior
{
    public class CreateOrUpdateProposal
    {
        public class Command : IRequest
        {
            public Proposal Proposal { get; set; }
            public DateTime? OriginalMessageTimestamp { get; set; }
        }

        public class Handler : RequestHandler<Command>
        {
            private readonly IProposalPageProvider _proposalPageProvider;
            private readonly IProcessFactory _processFactory;
            private readonly IIntegrationLogger _logger;

            public Handler(IProposalPageProvider proposalPageProvider, IProcessFactory processFactory, IIntegrationLogger logger)
            {
                _proposalPageProvider = proposalPageProvider;
                _processFactory = processFactory;
                _logger = logger;
            }

            protected override void Handle(Command command)
            {
                // Only create page in Norwegian
                if (string.IsNullOrEmpty(command.Proposal.EnglishTitle))
                {
                    HandlePage(command.Proposal, command.OriginalMessageTimestamp,
                        LanguageConstants.NorwegianLanguageBranch, LanguageConstants.MasterLanguageBranch);
                }
                // Only create page in English
                else if (string.IsNullOrEmpty(command.Proposal.Title))
                {
                    HandlePage(command.Proposal, command.OriginalMessageTimestamp,
                        LanguageConstants.EnglishLanguageBranch, LanguageConstants.EnglishLanguageBranch);
                }
                // Create page in Norwegian and English with Norwegian as master language
                else
                {
                    HandlePage(command.Proposal, command.OriginalMessageTimestamp,
                        LanguageConstants.NorwegianLanguageBranch, LanguageConstants.MasterLanguageBranch);
                    HandlePage(command.Proposal, command.OriginalMessageTimestamp,
                        LanguageConstants.EnglishLanguageBranch, LanguageConstants.MasterLanguageBranch);
                }
            }

            private void HandlePage(Proposal proposal, DateTime? originalMessageTimestamp, string languageBranch, string masterLanguageBranch)
            {
                var original = _proposalPageProvider.GetProposalPage(proposal, languageBranch);

                if (originalMessageTimestamp != null && original != null && original.UpdatedAt?.ToUniversalTime() > originalMessageTimestamp)
                    throw new ArgumentException($"Original message timestamp is before latest page update {original.UpdatedAt?.ToUniversalTime()}.");

                var changed = original is null
                    ? _proposalPageProvider.CreateProposalPage(proposal, languageBranch, masterLanguageBranch)
                    : _proposalPageProvider.CreateProposalPageVersion(original);

                changed.UpdatedAt = originalMessageTimestamp;
                RunProcess(original, changed, proposal, languageBranch);
            }

            private void RunProcess(ProposalPage original, ProposalPage changed, Proposal proposal, string languageBranch)
            {
                var context = new ProposalContext(_logger)
                {
                    Proposal = proposal,
                    OriginalPage = original,
                    CurrentPage = changed,
                    LanguageBranch = languageBranch
                };

                var process = _processFactory.GetProcessForContext(context);
                process.Run();
            }
        }
    }
}