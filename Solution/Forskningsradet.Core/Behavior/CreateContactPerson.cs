using System;
using System.Globalization;
using EPiServer.Core;
using EPiServer.Shell.Configuration;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ApiModels.Idm;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Repositories.Contracts;
using MediatR;

namespace Forskningsradet.Core.Behavior
{
    public class CreateContactPerson
    {
        public class Command : IRequest
        {
            public IdmContactPerson ContactPerson { get; set; }
        }

        public class Handler : RequestHandler<Command>
        {
            private readonly IPageRepository _pageRepository;

            public Handler(IPageRepository pageRepository) => _pageRepository = pageRepository;

            protected override void Handle(Command command)
            {
                var page = CreatePage(command);
                CreateEnglishVersion(page);
            }

            private PersonPage CreatePage(Command command)
            {
                var personPageFolder = _pageRepository.GetCurrentStartPage()?.ContactPersonsPage;

                if (ContentReference.IsNullOrEmpty(personPageFolder))
                    throw new MissingConfigurationException("Contact person import destination is not configured.");

                var page = _pageRepository.Create<PersonPage>(personPageFolder, CultureInfo.GetCultureInfo(LanguageConstants.MasterLanguageBranch));

                MapToPage(page, command.ContactPerson);
                _pageRepository.Save(page);
                return page;
            }

            private void CreateEnglishVersion(PageData page)
            {
                var translatedPage = _pageRepository.CreateLanguageBranch<PersonPage>(page.PageLink, LanguageConstants.EnglishLanguageBranch);
                translatedPage.Name = page.PageName;
                translatedPage.JobTitle = string.Empty;
                translatedPage.Department = string.Empty;
                _pageRepository.Save(translatedPage);
            }

            private static void MapToPage(PersonPage page, IdmContactPerson person)
            {
                page.Name = $"{person.FirstName} {person.LastName}";

                page.ExternalId = person.Id?.ToString() ?? Guid.Empty.ToString();
                page.ResourceId = person.ResourceId;
                page.FirstName = person.FirstName;
                page.LastName = person.LastName;
                page.JobTitle = person.Title;
                page.Department = person.DepartmentName;
                page.Mobile = person.Mobile;
                page.Phone = person.Phone;
                page.Email = person.Email;
                page.ResourceType = person.ResourceType;
                page.ExternalLastUpdate = person.AgressoLastUpdate;
            }
        }
    }
}