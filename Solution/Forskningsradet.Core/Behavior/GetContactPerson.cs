using System;
using System.Linq;
using EPiServer;
using Forskningsradet.Core.Models.ApiModels.Idm;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Repositories.Contracts;
using MediatR;

namespace Forskningsradet.Core.Behavior
{
    public class GetContactPerson
    {
        public class Query : IRequest<IdmContactPerson>
        {
            public IdmContactPersonId Id { get; set; }
            public bool FilterPublished { get; set; } = true;
        }

        public class Handler : RequestHandler<Query, IdmContactPerson>
        {
            private readonly IContentRepository _contentRepository;
            private readonly IPageRepository _pageRepository;

            public Handler(IContentRepository contentRepository, IPageRepository pageRepository)
            {
                _contentRepository = contentRepository;
                _pageRepository = pageRepository;
            }

            protected override IdmContactPerson Handle(Query query)
            {
                if (!query.Id?.IsValid == true)
                    throw new ArgumentException("Query.Id cannot be empty");

                var pages = _contentRepository.GetChildren<PersonPage>(_pageRepository.GetCurrentStartPage()?.ContactPersonsPage);

                if (query.FilterPublished)
                    pages = _pageRepository.FilterPublished(pages);

                var person = query.Id.HasResourceId
                    ? pages.SingleOrDefault(x => x.ResourceId == query.Id.ResourceId.Value.ToString())
                    : pages.SingleOrDefault(x => x.ExternalId == query.Id.Guid.ToString());

                return person is null ? null : ToContract(person, query.Id);
            }

            private static IdmContactPerson ToContract(PersonPage personPage, IdmContactPersonId id) =>
                new IdmContactPerson
                {
                    Id = id.HasResourceId ? Guid.Empty : new Guid(personPage.ExternalId),
                    ResourceId = personPage.ResourceId,
                    FirstName = personPage.FirstName,
                    LastName = personPage.LastName,
                    Title = personPage.JobTitle,
                    DepartmentName = personPage.Department,
                    Mobile = personPage.Mobile,
                    Phone = personPage.Phone,
                    Email = personPage.Email,
                    ResourceType = personPage.ResourceType,
                    AgressoLastUpdate = personPage.ExternalLastUpdate
                };
        }
    }
}