using Forskningsradet.Common.Logging;
using Forskningsradet.Core.Models.ApiModels.DWH;
using Forskningsradet.Core.Processes.Contexts;
using Forskningsradet.Core.Services.Contracts;
using MediatR;

namespace Forskningsradet.Core.Behavior
{
    public class UpdateApplicationResult
    {
        public class Command : IRequest
        {
            public ApplicationResult ApplicationResult { get; set; }
        }

        public class Handler : RequestHandler<Command>
        {
            private readonly IProcessFactory _processFactory;
            private readonly IIntegrationLogger _logger;

            public Handler(IProcessFactory processFactory, IIntegrationLogger logger)
            {
                _processFactory = processFactory;
                _logger = logger;
            }

            protected override void Handle(Command command)
            {
                var context = new ApplicationResultContext(_logger)
                {
                    ApplicationResult = command.ApplicationResult
                };

                var process = _processFactory.GetProcessForContext(context);
                process.Run();
            }
        }
    }
}