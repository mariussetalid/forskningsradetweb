﻿using System;
using EPiServer.Editor;
using Forskningsradet.Core.Facades.Contracts;

namespace Forskningsradet.Core.Facades
{
    public class PageEditingAdapter : IPageEditingAdapter
    {
        public bool PageIsInEditMode()
        {
            try
            {
                return PageEditing.PageIsInEditMode;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
