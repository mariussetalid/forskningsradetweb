﻿namespace Forskningsradet.Core.Facades.Contracts
{
    public interface IPageEditingAdapter
    {
        bool PageIsInEditMode();
    }
}
