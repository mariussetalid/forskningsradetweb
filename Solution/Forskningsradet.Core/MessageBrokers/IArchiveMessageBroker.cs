using System;
using System.Collections.Generic;
using Forskningsradet.Core.Processes.Contexts;

namespace Forskningsradet.Core.MessageBrokers
{
    public interface IArchiveMessageBroker
    {
        void Send(ArchivePage page);
        IEnumerable<MessageEntity<ArchivePage>> ReceiveMessages();
        void CompleteMessage(Guid token);
    }
}