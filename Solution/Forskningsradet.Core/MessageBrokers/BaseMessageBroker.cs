using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Text;
using Microsoft.ServiceBus.Messaging;
using Newtonsoft.Json;

namespace Forskningsradet.Core.MessageBrokers
{
    public abstract class BaseMessageBroker
    {
        private readonly QueueClient _queueClient;

        protected BaseMessageBroker(string connectionString, string queueName)
            => _queueClient = QueueClient.CreateFromConnectionString(connectionString, queueName, ReceiveMode.PeekLock);

        protected IEnumerable<MessageEntity<T>> ReceiveMessages<T>(int numberOfMessagesToReceive = 10)
        {
            var messages = _queueClient.ReceiveBatch(numberOfMessagesToReceive);

            foreach (var message in messages)
            {
                var entity = message.GetBody<T>(new DataContractJsonSerializer(typeof(T)));
                yield return new MessageEntity<T>(message.LockToken, entity);
            }
        }

        protected void Send(object obj)
        {
            var message = new BrokeredMessage(new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(obj))))
            {
                ContentType = "application/json"
            };

            _queueClient.Send(message);
        }

        public void CompleteMessage(Guid token) =>
            _queueClient.Complete(token);
    }
}