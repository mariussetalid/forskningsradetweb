using System.Collections.Generic;
using Forskningsradet.Core.Configuration;
using Forskningsradet.Core.Processes.Contexts;

namespace Forskningsradet.Core.MessageBrokers
{
    public class ArchiveMessageBroker : BaseMessageBroker, IArchiveMessageBroker
    {
        public ArchiveMessageBroker(IArchiveConfiguration configuration)
            : base(configuration.QueueEndpoint, configuration.ArchivingQueueName) { }

        public IEnumerable<MessageEntity<ArchivePage>> ReceiveMessages() =>
            base.ReceiveMessages<ArchivePage>();

        public void Send(ArchivePage page) =>
            base.Send(page);
    }
}