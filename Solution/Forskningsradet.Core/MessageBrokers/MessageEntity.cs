using System;

namespace Forskningsradet.Core.MessageBrokers
{
    public class MessageEntity<T>
    {
        public Guid Token { get; }
        public T Entity { get; }

        public MessageEntity(Guid token, T entity)
        {
            Token = token;
            Entity = entity;
        }
    }
}