﻿using System.Collections.Generic;
using System.Linq;
using EPiServer.Core;
using EPiServer.Web.Routing;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Services;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.Builders
{
    public class TagLinkListReactModelBuilder : ITagLinkListReactModelBuilder
    {
        private readonly IUrlResolver _urlResolver;
        private readonly ICategoryLocalizationProvider _categoryLocalizationProvider;

        public TagLinkListReactModelBuilder(IUrlResolver urlResolver, ICategoryLocalizationProvider categoryLocalizationProvider)
        {
            _urlResolver = urlResolver;
            _categoryLocalizationProvider = categoryLocalizationProvider;
        }

        public ReactModels.TagLinkList BuildReactModel(CategoryList categories, CategoryList inactiveCategories, IReadOnlyList<ContentAreaPage> categoryHomePages, bool leftAlign, bool includeCategoriesWithoutHomePage) =>
            new ReactModels.TagLinkList
            {
                Tags = BuildTags(categories, inactiveCategories, categoryHomePages, includeCategoriesWithoutHomePage)
                    .ToList(),
                LeftAligned = leftAlign
            };

        private IEnumerable<ReactModels.TagLinkList_Tags> BuildTags(CategoryList categories, CategoryList inactiveCategories, IReadOnlyList<ContentAreaPage> categoryHomePages, bool includeCategoriesWithoutHomePage)
        {
            foreach (var categoryId in categories)
            {
                var categoryHomePage = categoryHomePages
                    .FirstOrDefault(x => x.HomePageForCategories != null && x.HomePageForCategories.MemberOfAny(new List<int> { categoryId }));

                if (!includeCategoriesWithoutHomePage && categoryHomePage is null)
                    continue;

                var inactive = categoryHomePage is null
                               || inactiveCategories != null && inactiveCategories.MemberOfAny(new List<int> { categoryId });

                yield return new ReactModels.TagLinkList_Tags
                {
                    Inactive = inactive,
                    Link = new ReactModels.Link
                    {
                        Text = _categoryLocalizationProvider.GetCategoryName(categoryId),
                        Url = inactive ? null : _urlResolver.GetUrl(categoryHomePage.ContentLink),
                        Id = categoryId.ToString()
                    }
                };
            }
        }
    }
}
