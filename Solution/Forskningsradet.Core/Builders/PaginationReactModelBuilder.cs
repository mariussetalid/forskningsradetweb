﻿using System.Collections.Generic;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Services;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.Builders
{
    public class PaginationReactModelBuilder : IPaginationReactModelBuilder
    {
        private readonly ILocalizationProvider _localizationProvider;


        public PaginationReactModelBuilder(ILocalizationProvider localizationProvider) => _localizationProvider = localizationProvider;

        public ReactModels.Pagination BuildReactModel(string currentQueryString, int currentPage, int totalPages, int pageSize, string listPageUrl)
        {
            var currentPageStrictlyPositive = currentPage > 0 ? currentPage : 1;
            if (totalPages <= pageSize)
                return null;

            var pageCount = (totalPages + pageSize - 1) / pageSize;
            var pagination = new ReactModels.Pagination
            {
                Pages = new List<ReactModels.Pagination_Pages>(),
                Title = GetLabel("PaginationTitle")
            };

            if (currentPageStrictlyPositive > SearchConstants.ThresholdForFirstAndLastDirectPaginationLinks)
            {
                pagination.Pages.Add(
                    BuildPaginationNavigationLink(
                        listPageUrl,
                        GetLabel("PaginationFirst"),
                        GetLabel("PaginationFirstPage"),
                        currentQueryString,
                        1)
                );
            }

            if (currentPageStrictlyPositive > 1 && pageCount > 1)
            {
                pagination.Pages.Add(
                    BuildPaginationNavigationLink(
                        listPageUrl,
                        GetLabel("PaginationPrevious"),
                        GetLabel("PaginationPreviousPage"),
                        currentQueryString,
                        currentPageStrictlyPositive - 1)
                );
            }

            var intervalStart = GetIntervalStart();
            var intervalEnd = GetIntervalEnd();
            for (var i = intervalStart; i <= intervalEnd; i++)
            {
                pagination.Pages.Add(
                    new ReactModels.Pagination_Pages
                    {
                        IsCurrent = i == currentPageStrictlyPositive,
                        Label = string.Format(GetLabel("PaginationPage"), i),
                        Link = new ReactModels.Link
                        {
                            Text = i.ToString(),
                            Url = BuildPaginationUrl(listPageUrl, i, currentQueryString)
                        }
                    }
                );
            }

            if (currentPageStrictlyPositive != pageCount && pageCount > 1)
            {
                pagination.Pages.Add(
                    BuildPaginationNavigationLink(
                        listPageUrl,
                        GetLabel("PaginationNext"),
                        GetLabel("PaginationNextPage"),
                        currentQueryString,
                        currentPageStrictlyPositive + 1)
                );
            }

            if (currentPageStrictlyPositive < (pageCount - SearchConstants.ThresholdForFirstAndLastDirectPaginationLinks))
            {
                pagination.Pages.Add(
                    BuildPaginationNavigationLink(
                        listPageUrl,
                        GetLabel("PaginationLast"),
                        GetLabel("PaginationLastPage"),
                        currentQueryString,
                        pageCount)
                );
            }

            return pagination;

            int GetIntervalStart() =>
                (currentPageStrictlyPositive % pageSize == 0) ? (currentPageStrictlyPositive - pageSize + 1) : ((currentPageStrictlyPositive / pageSize) * pageSize) + 1;

            int GetIntervalEnd() =>
                pageCount < (intervalStart + pageSize) ? pageCount : (intervalStart + pageSize - 1);
        }

        private static ReactModels.Pagination_Pages BuildPaginationNavigationLink(string listPageUrl, string linkText, string label, string queryString, int pageIndex) =>
            new ReactModels.Pagination_Pages
            {
                IsCurrent = false,
                Label = label,
                Link = new ReactModels.Link
                {
                    Text = linkText,
                    Url = BuildPaginationUrl(listPageUrl, pageIndex, queryString)
                }
            };

        private static string BuildPaginationUrl(string listPageUrl, int pageIndex, string queryString) =>
            !string.IsNullOrEmpty(queryString)
                ? $"{listPageUrl}?{queryString}&{QueryParameters.Page}={pageIndex}"
                : $"{listPageUrl}?{QueryParameters.Page}={pageIndex}";

        private string GetLabel(string key) => _localizationProvider.GetLabel(nameof(ReactModels.Pagination), key);
    }
}
