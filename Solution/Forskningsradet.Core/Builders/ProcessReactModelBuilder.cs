﻿using System.Collections.Generic;
using System.Linq;
using EPiServer;
using EPiServer.Core;
using EPiServer.Web.Routing;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Extensions;
using Forskningsradet.Core.Models.ContentModels.Media;
using Forskningsradet.Core.Models.ContentModels.PropertyList;
using Forskningsradet.Core.Models.Enums;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.Builders
{
    public class ProcessReactModelBuilder : IProcessReactModelBuilder
    {
        private readonly IContentLoader _contentLoader;
        private readonly IUrlResolver _urlResolver;

        public ProcessReactModelBuilder(IContentLoader contentLoader, IUrlResolver urlResolver)
        {
            _contentLoader = contentLoader;
            _urlResolver = urlResolver;
        }

        public ReactModels.Process BuildReactModel(string title, string text, IList<ProcessItem> items, ProcessStyle style = ProcessStyle.None) =>
            new ReactModels.Process
            {
                Title = title,
                IntroText = text,
                Items = BuildItems(items).ToList(),
                IsCarousel = style == ProcessStyle.Carousel
            };

        private IEnumerable<ReactModels.ProcessItem> BuildItems(IList<ProcessItem> items)
        {
            if (items is null)
                yield break;

            foreach (var item in items)
            {
                yield return new ReactModels.ProcessItem
                {
                    Text = item.Ingress,
                    Title = item.Title,
                    Icon = ContentReference.IsNullOrEmpty(item.Image) ? null : BuildImage(item.Image),
                    Url = item.Url.GetUrl(_urlResolver) 
                };
            }
        }

        private ReactModels.Image BuildImage(ContentReference reference)
        {
            var imageFile = _contentLoader.Get<ImageFile>(reference);
            return new ReactModels.Image
            {
                Alt = imageFile.AltText ?? string.Empty,
                Src = _urlResolver.GetUrl(imageFile.ContentLink),
            };
        }
    }
}
