﻿using System.Collections.Generic;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Models.ApiModels.Evurdering;
using Forskningsradet.Core.Services;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.Builders
{
    public class DateCardStatusReactModelBuilder : IDateCardStatusReactModelBuilder
    {
        private readonly ILocalizationProvider _localizationProvider;

        public DateCardStatusReactModelBuilder(ILocalizationProvider localizationProvider) =>
            _localizationProvider = localizationProvider;

        public IEnumerable<ReactModels.DateCardStatus> BuildDateCardStatusList(ProposalState proposalState, bool hasResultStatus)
        {
            if (BuildReactModel(proposalState) is ReactModels.DateCardStatus status)
                yield return status;

            if (hasResultStatus)
                yield return GetResultStatus();
        }

        public ReactModels.DateCardStatus BuildReactModel(ProposalState state)
        {
            switch (state)
            {
                case ProposalState.Active:
                    return GetActiveStatus();
                case ProposalState.Completed:
                    return GetCompletedStatus();
                case ProposalState.Cancelled:
                    return GetCancelledStatus();
                case ProposalState.Planned:
                    return GetPlannedStatus();
                default:
                    return null;
            }
        }

        private ReactModels.DateCardStatus GetResultStatus() =>
            new ReactModels.DateCardStatus
            {
                Text = GetLabel("Results"),
                Theme = ReactModels.DateCardStatus_Theme.ResultIsPublished
            };

        private ReactModels.DateCardStatus GetActiveStatus() =>
            new ReactModels.DateCardStatus
            {
                Text = GetLabel("ApplyNow"),
                Theme = ReactModels.DateCardStatus_Theme.IsActive
            };

        private ReactModels.DateCardStatus GetCancelledStatus() =>
            new ReactModels.DateCardStatus
            {
                Text = GetLabel("Cancelled"),
                Theme = ReactModels.DateCardStatus_Theme.IsCanceled
            };

        private ReactModels.DateCardStatus GetCompletedStatus() =>
            new ReactModels.DateCardStatus
            {
                Text = GetLabel("Completed"),
                Theme = ReactModels.DateCardStatus_Theme.IsCompleted
            };

        private ReactModels.DateCardStatus GetPlannedStatus() =>
            new ReactModels.DateCardStatus
            {
                Text = GetLabel("Planned"),
                Theme = ReactModels.DateCardStatus_Theme.IsPlanned
            };

        private string GetLabel(string key) => _localizationProvider.GetLabel(nameof(ReactModels.DateCardStatus), key);
    }
}
