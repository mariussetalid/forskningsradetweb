﻿using System;
using System.Collections.Generic;
using System.Web.Routing;
using EPiServer.Core;
using EPiServer.Globalization;
using EPiServer.Web.Routing;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Extensions;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Services;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.Builders
{
    public class ArticleHeaderReactModelBuilder : IArticleHeaderReactModelBuilder
    {
        private readonly IUrlResolver _urlResolver;
        private readonly ILocalizationProvider _localizationProvider;
        private readonly IOptionsModalReactModelBuilder _optionsModalReactModelBuilder;

        public ArticleHeaderReactModelBuilder(
            IUrlResolver urlResolver,
            ILocalizationProvider localizationProvider,
            IOptionsModalReactModelBuilder optionsModalReactModelBuilder)
        {
            _urlResolver = urlResolver;
            _localizationProvider = localizationProvider;
            _optionsModalReactModelBuilder = optionsModalReactModelBuilder;
        }

        public ReactModels.ArticleHeader BuildReactModel(EditorialPage page) =>
            page is null ? null : MapReactModel(page, null, true);

        public ReactModels.ArticleHeader BuildReactModel(EditorialPage pageForDate, EditorialPage page) =>
            page is null ? null : MapReactModel(page, pageForDate, true);

        public ReactModels.ArticleHeader BuildReactModelWithoutShare(EditorialPage page) =>
            page is null ? null : MapReactModel(page, null, false);

        private ReactModels.ArticleHeader MapReactModel(EditorialPage page, EditorialPage pageForDate, bool includeShare) =>
            new ReactModels.ArticleHeader
            {
                Accordion = BuildAccordion(true),
                Byline = new ReactModels.Byline
                {
                    Items = BuildBylineItems(pageForDate ?? page)
                },
                Download = BuildDownloadLink(page.ContentLink),
                Share = includeShare ? _optionsModalReactModelBuilder.BuildShareContent(page) : null
            };

        private ReactModels.Accordion BuildAccordion(bool initiallyOpen) =>
            new ReactModels.Accordion
            {
                CollapseLabel = GetLabel("Collapse"),
                ExpandLabel = GetLabel("Expand"),
                Guid = Guid.NewGuid().ToString(),
                InitiallyOpen = initiallyOpen
            };

        private IList<ReactModels.Byline_Items> BuildBylineItems(EditorialPage page)
        {
            var items = new List<ReactModels.Byline_Items>();

            if (page.StartPublish?.ToDisplayDate() is DateTime startPublish)
            {
                var bylineItem = BuildBylineItem("Published", startPublish);
                items.Add(bylineItem);

                if (page.Changed.ToDisplayDate() is DateTime dateChanged && dateChanged.Date != startPublish.Date)
                {
                    var item = BuildBylineItem("Changed", dateChanged);
                    items.Add(item);
                }
            }

            return items;
        }

        private ReactModels.Byline_Items BuildBylineItem(string key, DateTime date) =>
            new ReactModels.Byline_Items
            {
                Text = $"{GetLabel(key)} {date.ToString(DateTimeFormats.TimelineDate)}"
            };

        private ReactModels.Link BuildDownloadLink(ContentReference pageReference) =>
            new ReactModels.Link
            {
                Url = BuildDownloadUrl(pageReference),
                Text = GetLabel("Download")
            };

        private string BuildDownloadUrl(ContentReference pageReference) =>
            _urlResolver.GetUrl(
                pageReference,
                ContentLanguage.PreferredCulture.Name,
                new UrlResolverArguments
                {
                    RouteValues = new RouteValueDictionary(new { action = "Download" })
                });

        private string GetLabel(string key) =>
            _localizationProvider.GetLabel(nameof(LargeDocumentPage), key);
    }
}
