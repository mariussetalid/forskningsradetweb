﻿using System.Collections.Generic;
using System.Linq;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Services.Contracts;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.Builders
{
    public class ContactInfoReactModelBuilder : IContactInfoReactModelBuilder
    {
        private readonly IPersonPageStringBuilder _personPageStringBuilder;

        public ContactInfoReactModelBuilder(IPersonPageStringBuilder personPageStringBuilder) =>
            _personPageStringBuilder = personPageStringBuilder;

        public ReactModels.ContactInfo BuildReactModel(PersonPage page, string propertyName, bool showPhoneNumber, bool showJobTitle)
            => page is PersonPage person
                ? MapPage(person, showPhoneNumber, showJobTitle)
                : null;

        public ReactModels.ContactInfo BuildReactModel(NonPersonContactBlock block, string propertyName, bool showPhoneNumber)
            => block is NonPersonContactBlock currentContent
                ? MapPage(new PersonPage
                {
                    FirstName = currentContent.Title,
                    Department = currentContent.Text,
                    Phone = currentContent.Phone,
                    Email = currentContent.Email,
                }, showPhoneNumber, false)
                : null;

        private ReactModels.ContactInfo MapPage(PersonPage page, bool showPhoneNumber, bool showJobTitle) =>
            new ReactModels.ContactInfo
            {
                Title = _personPageStringBuilder.BuildTitle(page),
                Details = BuildDetails(page, showPhoneNumber, showJobTitle).ToList(),
                OnPageEditing = null
            };

        private IEnumerable<ReactModels.Link> BuildDetails(PersonPage content, bool showPhoneNumber, bool showJobTitle)
            => BuildContactInfoDetails(content, showPhoneNumber, showJobTitle);

        private IEnumerable<ReactModels.Link> BuildContactInfoDetails(PersonPage content, bool showPhoneNumber, bool showJobTitle)
        {
            if (!string.IsNullOrWhiteSpace(content.JobTitle) && showJobTitle)
            {
                yield return new ReactModels.Link
                {
                    Text = content.JobTitle
                };
            }

            if (!string.IsNullOrWhiteSpace(content.Department))
            {
                yield return new ReactModels.Link
                {
                    Text = content.Department
                };
            }

            if (!string.IsNullOrWhiteSpace(content.Email))
            {
                yield return new ReactModels.Link
                {
                    Text = content.Email,
                    Url = _personPageStringBuilder.BuildEmailUrlScheme(content.Email)
                };
            }

            if (!string.IsNullOrWhiteSpace(content.Phone) && showPhoneNumber)
            {
                yield return new ReactModels.Link
                {
                    Text = content.Phone,
                    Url = _personPageStringBuilder.BuildPhoneUrlScheme(content.Phone)
                };
            }

            if (!string.IsNullOrWhiteSpace(content.Mobile) && showPhoneNumber)
            {
                yield return new ReactModels.Link
                {
                    Text = content.Mobile,
                    Url = _personPageStringBuilder.BuildPhoneUrlScheme(content.Mobile)
                };
            }
        }
    }
}
