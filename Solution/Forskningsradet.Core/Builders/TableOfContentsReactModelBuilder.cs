﻿using System;
using System.Collections.Generic;
using System.Linq;
using EPiServer;
using EPiServer.Core;
using EPiServer.Web.Routing;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Extensions;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.RequestModels;
using Forskningsradet.Core.Services;
using Forskningsradet.Core.Services.Contracts;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.Builders
{
    public class TableOfContentsReactModelBuilder : ITableOfContentsReactModelBuilder
    {
        private readonly IUrlResolver _urlResolver;
        private readonly IUrlCheckingService _urlCheckingService;
        private readonly IContentLoader _contentLoader;
        private readonly ILocalizationProvider _localizationProvider;

        public TableOfContentsReactModelBuilder(
            IUrlResolver urlResolver,
            IUrlCheckingService urlCheckingService,
            IContentLoader contentLoader,
            ILocalizationProvider localizationProvider)
        {
            _urlResolver = urlResolver;
            _urlCheckingService = urlCheckingService;
            _contentLoader = contentLoader;
            _localizationProvider = localizationProvider;
        }

        public ReactModels.TableOfContents BuildReactModel(LargeDocumentModel model) =>
            model?.MainPageReference != null
                ? MapReactModel(model)
                : null;

        private ReactModels.TableOfContents MapReactModel(LargeDocumentModel model) =>
            new ReactModels.TableOfContents
            {
                Title = GetLabel("TableOfContentsTitle"),
                Items = BuildTableOfContentsList(model).ToList(),
                Accordion = BuildAccordion(false)
            };

        private IEnumerable<ReactModels.TableOfContents_Items> BuildTableOfContentsList(LargeDocumentModel model)
        {
            yield return new ReactModels.TableOfContents_Items
            {
                Link = new ReactModels.ChapterLink
                {
                    Link = new ReactModels.Link
                    {
                        Text = _contentLoader.Get<LargeDocumentPage>(model.MainPageReference) is var page
                            ? page.GetListTitle()
                            : "",
                        Url = _urlResolver.GetUrl(model.MainPageReference)
                    },
                    IsCurrent = model.CurrentPageNode.ContentReference == model.MainPageReference
                }
            };

            var i = 1;
            foreach (var chapter in model.LargeDocumentStructure.Children)
            {
                var chapterPage = _contentLoader.Get<LargeDocumentPageBase>(chapter.ContentReference);
                if (chapterPage.VisibleInMenu)
                    yield return BuildChildrenNavigation(chapterPage, chapter, model, 1, i++);
            }

            foreach (var link in model.LinkItems)
            {
                yield return new ReactModels.TableOfContents_Items
                {
                    Link = new ReactModels.ChapterLink
                    {
                        Link = new ReactModels.Link
                        {
                            Text = link.Text,
                            Url = _urlResolver.GetUrl(link.Href),
                            IsExternal = link.IsExternal(_urlCheckingService)
                        }
                    }
                };
            }
        }

        private ReactModels.TableOfContents_Items BuildChildrenNavigation(LargeDocumentPageBase chapter, Node chapterNode, LargeDocumentModel model, int depth, int chapterNumber)
        {
            var children = chapterNode.Children;
            var chapterLink = new ReactModels.ChapterLink
            {
                HasDash = !model.ShowNumbers && model.ChapterDepth == 0,
                ChapterNumber = model.ShowNumbers ? chapterNumber.ToString() : null,
                Link = new ReactModels.Link
                {
                    Text = chapter.GetListTitle(),
                    Url = GetLinkOrAnchor(chapterNode, model, depth)
                },
                IsCurrent = model.CurrentPageNode.GetAncestors().Contains(chapterNode.ContentReference)
            };

            return children.Any()
                ? new ReactModels.TableOfContents_Items
                {
                    LinkOrLinkList = new ReactModels.ChapterNavigationList
                    {
                        Link = chapterLink,
                        Accordion = BuildAccordion(chapterLink.IsCurrent),
                        LinkOrLinkList = BuildChapterChildrenList(children, model, depth, $"{chapterNumber}").ToList(),
                        IsCurrent = chapterLink.IsCurrent
                    }
                }
                : new ReactModels.TableOfContents_Items
                {
                    Link = chapterLink
                };
        }

        private IEnumerable<ReactModels.ChapterNavigationList_LinkOrLinkList> BuildChapterChildrenList(List<Node> chapterNodes, LargeDocumentModel model, int depth, string chapterString)
        {
            var i = 1;
            foreach (var chapter in chapterNodes)
            {
                var chapterPage = _contentLoader.Get<LargeDocumentPageBase>(chapter.ContentReference);
                if (chapterPage.VisibleInMenu)
                    yield return BuildChapterChildrenNavigation(chapterPage, chapter, model, depth + 1, $"{chapterString}.{i++}");
            }
        }

        private ReactModels.ChapterNavigationList_LinkOrLinkList BuildChapterChildrenNavigation(LargeDocumentPageBase chapter, Node chapterNode, LargeDocumentModel model, int depth, string chapterString)
        {
            var children = chapterNode.Children;
            var chapterLink = new ReactModels.ChapterLink
            {
                HasDash = !model.ShowNumbers,
                ChapterNumber = model.ShowNumbers ? chapterString : null,
                Link = new ReactModels.Link
                {
                    Text = chapter.GetListTitle(),
                    Url = GetLinkOrAnchor(chapterNode, model, depth)
                },
                IsCurrent = model.CurrentPageNode.GetAncestors().Contains(chapterNode.ContentReference)
            };

            return children.Any()
                ? new ReactModels.ChapterNavigationList_LinkOrLinkList
                {
                    LinkOrLinkList = new ReactModels.ChapterNavigationList
                    {
                        Link = chapterLink,
                        Accordion = BuildAccordion(chapterLink.IsCurrent),
                        LinkOrLinkList = BuildChapterChildrenList(children, model, depth, chapterString).ToList(),
                        IsCurrent = chapterLink.IsCurrent
                    }
                }
                : new ReactModels.ChapterNavigationList_LinkOrLinkList
                {
                    Link = chapterLink
                };
        }

        private string GetLinkOrAnchor(Node chapterNode, LargeDocumentModel model, int depth)
        {
            var pageIsAboveChapterDepth = depth <= model.ChapterDepth;
            return pageIsAboveChapterDepth
                ? _urlResolver.GetUrl(chapterNode.ContentReference)
                : $"{_urlResolver.GetUrl(GetChapterReference(chapterNode, model, depth))}#{chapterNode.ContentReference.ID}";
        }

        private ContentReference GetChapterReference(Node chapterNode, LargeDocumentModel model, int depth)
        {
            var depthCounter = depth;
            var node = chapterNode;
            while (depthCounter > model.ChapterDepth)
            {
                node = node.ParentNode;
                depthCounter--;
            }

            return node.ContentReference;
        }

        private ReactModels.Accordion BuildAccordion(bool initiallyOpen) =>
            new ReactModels.Accordion
            {
                CollapseLabel = GetLabel("Collapse"),
                ExpandLabel = GetLabel("Expand"),
                Guid = Guid.NewGuid().ToString(),
                InitiallyOpen = initiallyOpen
            };

        private string GetLabel(string key) =>
            _localizationProvider.GetLabel(nameof(LargeDocumentPage), key);
    }
}