﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Routing;
using EPiServer;
using EPiServer.Core;
using EPiServer.Globalization;
using EPiServer.Web.Routing;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Extensions;
using Forskningsradet.Core.Models.ContentModels.Media;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Services;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.Builders
{
    public class DownloadListReactModelBuilder : IDownloadListReactModelBuilder
    {
        private readonly IContentLoader _contentLoader;
        private readonly IUrlResolver _urlResolver;
        private readonly ILocalizationProvider _localizationProvider;
        private const string Download = "Download";
        private const string DownloadTemplates = "DownloadTemplates";

        public DownloadListReactModelBuilder(IContentLoader contentLoader, IUrlResolver urlResolver, ILocalizationProvider localizationProvider)
        {
            _contentLoader = contentLoader;
            _urlResolver = urlResolver;
            _localizationProvider = localizationProvider;
        }

        public ReactModels.DownloadList BuildReactModel(ProposalPage proposalPage)
        {
            var downloadList = new ReactModels.DownloadList
            {
                Groups = GetDownloadGroups(proposalPage, true).ToList()
            };

            if (HasMultipleTemplates(proposalPage))
            {
                downloadList.DownloadAllUrl = BuildDownloadUrl(proposalPage.ContentLink, DownloadTemplates);
                downloadList.DownloadAllText = GetLabel("DownloadAll");
            }
            return downloadList;
        }

        public ReactModels.DownloadList BuildTemplatesList(ProposalPage proposalPage)
        {
            var downloadList = new ReactModels.DownloadList
            {
                Groups = GetDownloadGroups(proposalPage, false).ToList()
            };

            if (HasMultipleTemplates(proposalPage))
            {
                downloadList.DownloadAllUrl = BuildDownloadUrl(proposalPage.ContentLink, DownloadTemplates);
                downloadList.DownloadAllText = GetLabel("DownloadAll");
            }
            return downloadList;
        }

        private IEnumerable<ReactModels.DownloadList_Groups> GetDownloadGroups(ProposalPage proposalPage, bool pageDownload)
        {
            if (pageDownload)
            {
                yield return new ReactModels.DownloadList_Groups
                {
                    Heading = GetLabel("DownloadProposal"),
                    Items = GetPageDownloadLink(proposalPage).ToList()
                };
            }

            if (HasTemplates(proposalPage))
            {
                yield return new ReactModels.DownloadList_Groups
                {
                    Heading = GetLabel("DownloadTemplates"),
                    Items = GetTemplateLinks(proposalPage.ApplicationTemplates).ToList()
                };
            }
        }

        private IEnumerable<ReactModels.DownloadList_Groups_Items> GetPageDownloadLink(ProposalPage proposalPage)
        {
            yield return new ReactModels.DownloadList_Groups_Items
            {
                Url = BuildDownloadUrl(proposalPage.ContentLink, Download),
                Text = $"{proposalPage.Name.EnsureValidEnglishWindowsFileName()}.pdf",
                IconTheme = ReactModels.DocumentIcon_IconTheme.Pdf
            };
        }

        private IEnumerable<ReactModels.DownloadList_Groups_Items> GetTemplateLinks(IList<ContentReference> applicationTemplates)
        {
            foreach (var template in applicationTemplates)
            {
                var media = _contentLoader.Get<GenericMedia>(template);
                yield return new ReactModels.DownloadList_Groups_Items
                {
                    Url = _urlResolver.GetUrl(media.ContentLink),
                    Text = media.Name,
                    IconTheme = GetDocumentIcon(media)
                };
            }
        }

        private ReactModels.DocumentIcon_IconTheme GetDocumentIcon(GenericMedia genericMedia)
        {
            switch (genericMedia.FileExtension)
            {
                case ".pdf":
                    return ReactModels.DocumentIcon_IconTheme.Pdf;
                case ".doc":
                case ".docx":
                    return ReactModels.DocumentIcon_IconTheme.Word;
                case ".xls":
                case ".xlsx":
                    return ReactModels.DocumentIcon_IconTheme.Excel;
                case ".ppt":
                case ".pptx":
                    return ReactModels.DocumentIcon_IconTheme.Ppt;
                default:
                    return ReactModels.DocumentIcon_IconTheme.Fallback;
            }
        }

        private string BuildDownloadUrl(ContentReference contentReference, string actionString) =>
            _urlResolver.GetUrl(
                contentReference,
                ContentLanguage.PreferredCulture.Name,
                new UrlResolverArguments
                {
                    RouteValues = new RouteValueDictionary(new { action = actionString })
                });

        private bool HasTemplates(ProposalPage proposalPage) =>
            proposalPage.ApplicationTemplates?.Count > 0;

        private bool HasMultipleTemplates(ProposalPage proposalPage) =>
            proposalPage.ApplicationTemplates?.Count > 1;

        private string GetLabel(string key) =>
            _localizationProvider.GetLabel(nameof(ReactModels.DownloadList), key);
    }
}
