﻿using System.Collections.Generic;
using System.Linq;
using EPiServer;
using EPiServer.Core;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Facades.Contracts;
using Forskningsradet.Core.Factories;
using Forskningsradet.Core.Models.ContentModels.Contracts;
using Forskningsradet.Core.Models.QueryParameters;
using Forskningsradet.Core.Models.ViewModels.Contracts;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.Builders
{
    public class ContentAreaReactModelBuilder : IContentAreaReactModelBuilder
    {
        private readonly IContentLoader _contentLoader;
        private readonly IViewModelFactory _viewModelFactory;
        private readonly IPageEditingAdapter _pageEditingAdapter;

        private const string NotSupportedLabel = "Innholdsområder støtter ikke innhold av typen:";

        public ContentAreaReactModelBuilder(IContentLoader contentLoader, IViewModelFactory viewModelFactory, IPageEditingAdapter pageEditingAdapter)
        {
            _contentLoader = contentLoader;
            _viewModelFactory = viewModelFactory;
            _pageEditingAdapter = pageEditingAdapter;
        }

        public ReactModels.ContentArea BuildReactModel(ContentArea contentArea, string contentAreaName, RenderOptionQueryParameter renderOptionQueryParameter = null) =>
            BuildContentAreaComponent(contentAreaName, BuildContentAreaItems(contentArea, renderOptionQueryParameter));

        public ReactModels.ContentArea BuildReactModel(IList<ReactModels.ContentAreaItem> contentAreaItems, string contentAreaName) =>
            BuildContentAreaComponent(contentAreaName, contentAreaItems);

        public IReactViewModel BuildContentAreaPreviewItem(ContentReference contentLink) =>
            _viewModelFactory.GetPartialViewModel(_contentLoader.Get<IContent>(contentLink)) is IReactViewModel viewModel
                ? viewModel
                : null;

        private ReactModels.ContentArea BuildContentAreaComponent(string contentAreaName, IList<ReactModels.ContentAreaItem> contentAreaItems) =>
            new ReactModels.ContentArea
            {
                OnPageEditing = BuildOnPageEditingComponent(contentAreaName),
                BlockNotSupportedText = _pageEditingAdapter.PageIsInEditMode() ? NotSupportedLabel : string.Empty,
                Blocks = contentAreaItems ?? new List<ReactModels.ContentAreaItem>()
            };

        private ReactModels.ContentArea_OnPageEditing BuildOnPageEditingComponent(string contentAreaName) =>
            _pageEditingAdapter.PageIsInEditMode()
                ? new ReactModels.ContentArea_OnPageEditing { Name = contentAreaName }
                : null;

        private List<ReactModels.ContentAreaItem> BuildContentAreaItems(ContentArea contentArea, RenderOptionQueryParameter renderOption) =>
            contentArea
                ?.FilteredItems
                .Where(ShouldRenderItem)
                .Select((x, y) => BuildContentAreaItems(x, y, renderOption))
                .Where(x => x != null)
                .ToList() ?? new List<ReactModels.ContentAreaItem>();

        private bool ShouldRenderItem(ContentAreaItem item)
        {
            // Workaround to avoid ContentArea displaying content in master language also on all other languages https://world.episerver.com/forum/developer-forum/Problems-and-bugs/Thread-Container/2019/6/items-in-content-area-are-shown-even-if-they-are-not-available-for-the-current-language/
            return _contentLoader.Get<IContent>(item.ContentLink, new LoaderOptions { LanguageLoaderOption.Fallback() }) != null;
        }

        private ReactModels.ContentAreaItem BuildContentAreaItems(ContentAreaItem item, int index, RenderOptionQueryParameter renderOption)
        {
            var size = MapSize(item.LoadDisplayOption()?.Tag);
            var content = _contentLoader.Get<IContent>(item.ContentLink);
            return BuildContentAreaItem(index, content, size, renderOption);
        }

        private ReactModels.ContentAreaItem BuildContentAreaItem(int index, IContent content, ReactModels.ContentAreaItem_Size size, RenderOptionQueryParameter renderOption)
        {
            var viewModel = GetViewModel(content, size, renderOption);

            return new ReactModels.ContentAreaItem
            {
                OnPageEditing = BuildContentAreaItemOnPageEditingComponent(content.Name, content.ContentLink.ID.ToString()),
                Id = index.ToString(),
                ComponentName = viewModel?.ReactComponentName,
                ComponentData = viewModel?.Model,
                Size = size
            };

            ReactModels.ContentAreaItem_OnPageEditing BuildContentAreaItemOnPageEditingComponent(string contentName, string contentId) =>
                _pageEditingAdapter.PageIsInEditMode()
                    ? new ReactModels.ContentAreaItem_OnPageEditing { ContentName = contentName, ContentId = contentId }
                    : null;
        }

        private IReactViewModel GetViewModel(IContent content, ReactModels.ContentAreaItem_Size size, RenderOptionQueryParameter renderOption)
        {
            if (CanBeInSidebar() && (SizeFitsSidebar() || WidthIsSetToSidebar()))
            {
                renderOption.RenderWidthOption = RenderWidthOption.IsInSidebar;
                return _viewModelFactory.GetPartialViewModel(content, renderOption);
            }

            return _viewModelFactory.GetPartialViewModel(content);

            bool CanBeInSidebar() => content is ICanBeRenderedInSidebar;
            bool SizeFitsSidebar() => size == ReactModels.ContentAreaItem_Size.Third;
            bool WidthIsSetToSidebar() => renderOption != null && renderOption.RenderWidthOption == RenderWidthOption.IsInSidebar;
        }

        private ReactModels.ContentAreaItem_Size MapSize(string tag)
        {
            switch (tag)
            {
                case ContentAreaTags.FullWidth:
                    return ReactModels.ContentAreaItem_Size.FullScreen;
                case ContentAreaTags.HalfWidth:
                    return ReactModels.ContentAreaItem_Size.Half;
                case ContentAreaTags.TwoThirdsWidth:
                    return ReactModels.ContentAreaItem_Size.TwoThirds;
                case ContentAreaTags.OneThirdWidth:
                    return ReactModels.ContentAreaItem_Size.Third;
                default:
                    return ReactModels.ContentAreaItem_Size.None;
            }
        }
    }
}
