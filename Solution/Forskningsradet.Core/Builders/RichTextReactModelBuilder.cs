﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using EPiServer.Core;
using EPiServer.Core.Html.StringParsing;
using EPiServer.Web.Mvc.Html;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Facades.Contracts;
using Forskningsradet.Core.Factories;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.QueryParameters;
using Forskningsradet.Core.Models.ViewModels.Contracts;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.Builders
{
    public class RichTextReactModelBuilder : IRichTextReactModelBuilder
    {
        private readonly IViewModelFactory _viewModelFactory;
        private readonly IPageEditingAdapter _pageEditingAdapter;
        private readonly UrlHelper _urlResolver;

        public RichTextReactModelBuilder(IViewModelFactory viewModelFactory, IPageEditingAdapter pageEditingAdapter, UrlHelper urlResolver)
        {
            _viewModelFactory = viewModelFactory;
            _pageEditingAdapter = pageEditingAdapter;
            _urlResolver = urlResolver;
        }

        public ReactModels.RichText BuildReactModel(XhtmlString richText, string richTextName)
            => richText != null
                ? new ReactModels.RichText
                {
                    OnPageEditing = BuildOnPageEditingComponent(richTextName),
                    BlockNotSupportedText = _pageEditingAdapter.PageIsInEditMode() ? "Innholdsområder støtter ikke innhold av typen:" : string.Empty,
                    Blocks = BuildRichTextItems(richText)
                }
                : new ReactModels.RichText
                {
                    OnPageEditing = BuildOnPageEditingComponent(richTextName)
                };

        private List<ReactModels.ContentAreaItem> BuildRichTextItems(XhtmlString richText)
            => RemoveUrlFragments(richText.Fragments).Select(BuildItem).ToList();

        private ReactModels.ContentAreaItem BuildItem(IStringFragment fragment, int index)
        {
            if (fragment is ContentFragment contentFragment && contentFragment.GetContent() is IContent content)
                return BuildRichTextItem(index, content);
            var xhtmlString = new XhtmlString();
            xhtmlString.Fragments.Add(fragment);
            var richTextComponent = new ReactModels.HtmlString
            {
                Text = xhtmlString.ToHtmlString()
            };
            return new ReactModels.ContentAreaItem
            {
                Id = index.ToString(),
                ComponentName = nameof(ReactModels.HtmlString),
                ComponentData = richTextComponent
            };
        }

        private StringFragmentCollection RemoveUrlFragments(StringFragmentCollection fragments)
        {
            var newFragments = new StringFragmentCollection();
            var sb = new StringBuilder();
            foreach (var fragment in fragments)
            {
                if (fragment is UrlFragment urlFragment)
                {
                    sb.Append(_urlResolver.ContentUrl(urlFragment.Url));
                }
                else if (fragment is StaticFragment staticFragment)
                {
                    sb.Append(staticFragment.InternalFormat);
                }
                else
                {
                    if (!string.IsNullOrWhiteSpace(sb.ToString()))
                    {
                        newFragments.Add(new StaticFragment(sb.ToString()));
                        sb.Clear();
                    }
                    newFragments.Add(fragment);
                }
            }
            if (!string.IsNullOrWhiteSpace(sb.ToString()))
                newFragments.Add(new StaticFragment(sb.ToString()));
            return newFragments;
        }

        private ReactModels.ContentAreaItem BuildRichTextItem(int index, IContent data)
        {
            var viewModel = data is MediaBlock
                ? _viewModelFactory.GetPartialViewModel(data, new RenderOptionQueryParameter { RenderMediaBlockOption = RenderMediaBlockOption.MediaWithCaption })
                : _viewModelFactory.GetPartialViewModel(data);

            if (!(viewModel is IReactViewModel))
                return null;

            return new ReactModels.ContentAreaItem
            {
                Id = index.ToString(),
                ComponentName = viewModel.ReactComponentName,
                ComponentData = viewModel.Model,
                OnPageEditing = _pageEditingAdapter.PageIsInEditMode()
                    ? new ReactModels.ContentAreaItem_OnPageEditing
                    {
                        ContentName = data.Name,
                        ContentId = data.ContentLink.ID.ToString()
                    }
                    : null
            };
        }

        private ReactModels.ContentArea_OnPageEditing BuildOnPageEditingComponent(string richTextName)
            => _pageEditingAdapter.PageIsInEditMode()
                ? new ReactModels.ContentArea_OnPageEditing { Name = richTextName }
                : null;
    }
}
