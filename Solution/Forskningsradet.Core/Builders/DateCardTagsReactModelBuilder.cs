﻿using System;
using System.Collections.Generic;
using System.Linq;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Services;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.Builders
{
    public class DateCardTagsReactModelBuilder : IDateCardTagsReactModelBuilder
    {
        private readonly CategoryRepository _categoryRepository;
        private readonly ICategoryLocalizationProvider _categoryLocalizationProvider;
        private readonly ILocalizationProvider _localizationProvider;

        public DateCardTagsReactModelBuilder(CategoryRepository categoryRepository, ICategoryLocalizationProvider categoryLocalizationProvider, ILocalizationProvider localizationProvider)
        {
            _categoryRepository = categoryRepository;
            _categoryLocalizationProvider = categoryLocalizationProvider;
            _localizationProvider = localizationProvider;
        }

        public ReactModels.DateCardTags BuildReactModel(CategoryList categories, int numberOfVisibleItems) =>
            categories != null
                ? BuildDateCardTags(categories, numberOfVisibleItems)
                : null;

        private ReactModels.DateCardTags BuildDateCardTags(CategoryList categories, int numberOfVisibleItems) =>
            new ReactModels.DateCardTags
            {
                Items = BuildTagList(categories),
                Accordion = categories.Count - numberOfVisibleItems > 0
                        ? new ReactModels.Accordion
                        {
                            Guid = Guid.NewGuid().ToString(),
                            ExpandLabel = string.Format(GetLabel("ExpandTagsFormat"), categories.Count - numberOfVisibleItems),
                            CollapseLabel = GetLabel("CollapseTags")
                        }
                        : null,
                NumberOfVisibleItems = numberOfVisibleItems
            };

        private IList<ReactModels.Link> BuildTagList(CategoryList categories) =>
            categories
                .Select(cat => _categoryRepository.Get(cat))
                .Select(x => new ReactModels.Link { Text = _categoryLocalizationProvider.GetCategoryName(x.ID) })
                .OrderBy(x => x.Text)
                .ToList();

        private string GetLabel(string key) =>
            _localizationProvider.GetLabel(nameof(ReactModels.DateCardTags), key);
    }
}
