using System;
using System.Collections.Generic;
using System.Linq;
using EPiServer;
using EPiServer.Core;
using EPiServer.Web.Routing;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Extensions;
using Forskningsradet.Core.Models;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Services;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.Builders
{
    public class BreadcrumbsReactModelBuilder : IBreadcrumbsReactModelBuilder
    {
        private readonly IUrlResolver _urlResolver;
        private readonly IPageStructureService _pageStructureService;
        private readonly IContentLoader _contentLoader;
        private readonly IBreadcrumbLinkBuilder<ProposalListPage> _proposalListPageBreadcrumbLinkBuilder;
        private readonly IBreadcrumbLinkBuilder<EventListPage> _eventListBreadcrumbLinkBuilder;
        private readonly ILocalizationProvider _localizationProvider;

        public BreadcrumbsReactModelBuilder(
            IUrlResolver urlResolver,
            IPageStructureService pageStructureService,
            IContentLoader contentLoader,
            IBreadcrumbLinkBuilder<ProposalListPage> proposalListPageBreadcrumbLinkBuilder,
            IBreadcrumbLinkBuilder<EventListPage> eventLitBreadcrumbLinkBuilder,
            ILocalizationProvider localizationProvider)
        {
            _urlResolver = urlResolver;
            _pageStructureService = pageStructureService;
            _contentLoader = contentLoader;
            _proposalListPageBreadcrumbLinkBuilder = proposalListPageBreadcrumbLinkBuilder;
            _eventListBreadcrumbLinkBuilder = eventLitBreadcrumbLinkBuilder;
            _localizationProvider = localizationProvider;
        }

        public ReactModels.Breadcrumbs BuildReactModel(FrontPageBase closestFrontPage, PageReference currentPageReference) =>
            BuildReactModel(closestFrontPage.PageLink, currentPageReference, string.Empty, null);

        public ReactModels.Breadcrumbs BuildReactModel(PageReference closestFrontPageReference, PageReference currentPageReference, string pageNameOverride, IEnumerable<EditorialPage> appendedPages)
        {
            if (currentPageReference == closestFrontPageReference ||
                currentPageReference == ContentReference.EmptyReference)
            {
                return null;
            }

            var filteredAncestors = ContentReference.IsNullOrEmpty(closestFrontPageReference)
                    ? new List<EditorialPage>()
                    : _pageStructureService
                        .GetFilteredAncestors<EditorialPage>(closestFrontPageReference, currentPageReference)
                        .Reverse();

            var ancestorPages = filteredAncestors
                    .Union(appendedPages ?? new List<EditorialPage>())
                    .ToList();

            var currentPage = _contentLoader.Get<BasePageData>(currentPageReference);

            var allBreadcrumbPages = (new PageData[] { currentPage }).Concat(ancestorPages).ToList();

            return new ReactModels.Breadcrumbs
            {
                Accordion = new ReactModels.Accordion
                {
                    CollapseLabel = GetLabel("HideLinksButtonLabel"),
                    ExpandLabel = GetLabel("ShowLinksButtonLabel"),
                    Guid = Guid.NewGuid().ToString()
                },
                Items = ancestorPages.Count > 0
                    ? ancestorPages
                        .Select(x => BuildLink(x, allBreadcrumbPages))
                        .ToList()
                    : null,
                Root = new ReactModels.Link
                {
                    Text = GetLabel("HomeButtonLabel"),
                    Url = _urlResolver.GetUrl(closestFrontPageReference)
                },
                CurrentPage = GetPageName()
            };

            string GetPageName() =>
                !string.IsNullOrEmpty(pageNameOverride)
                    ? pageNameOverride
                    : currentPage is EditorialPage editorialPage
                        ? BuildLink(editorialPage, allBreadcrumbPages).Text
                        : currentPage.PageName;
        }

        private ReactModels.Link BuildLink(EditorialPage breadcrumbPage, List<PageData> allBreadcrumbPages)
        {
            switch (breadcrumbPage)
            {
                case ProposalListPage proposalListPage:
                    return _proposalListPageBreadcrumbLinkBuilder.Build(proposalListPage, CreateBreadcrumbsBuildContext(breadcrumbPage, allBreadcrumbPages));
                case EventListPage eventListPage:
                    return _eventListBreadcrumbLinkBuilder.Build(eventListPage, CreateBreadcrumbsBuildContext(breadcrumbPage, allBreadcrumbPages));
                default:
                    return new ReactModels.Link
                    {
                        Text = breadcrumbPage.GetListTitle(),
                        Url = _urlResolver.GetUrl(breadcrumbPage.ContentLink)
                    };
            }
        }

        private static BreadcrumbsBuildContext CreateBreadcrumbsBuildContext(PageData breadcrumbPage, List<PageData> allBreadcrumbPagesFromCurrentToRoot)
        {
            var buildContext = new BreadcrumbsBuildContext
            {
                ParentPages = allBreadcrumbPagesFromCurrentToRoot.SkipWhile(x => x != breadcrumbPage).Skip(1),
                ChildPages = allBreadcrumbPagesFromCurrentToRoot.TakeWhile(x => x != breadcrumbPage).Reverse()
            };
            return buildContext;
        }

        private string GetLabel(string key) => _localizationProvider.GetLabel(nameof(ReactModels.Breadcrumbs), key);
    }
}