﻿using System.Collections.Generic;
using System.Linq;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Extensions;
using Forskningsradet.Core.Facades.Contracts;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ContentModels.PropertyList;
using Forskningsradet.Core.Services;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.Builders
{
    public class AccordionWithContentAreaListReactModelBuilder : IAccordionWithContentAreaListReactModelBuilder
    {
        private readonly IRichTextReactModelBuilder _richTextReactModelBuilder;
        private readonly IContentAreaReactModelBuilder _contentAreaReactModelBuilder;
        private readonly IAccordionReactModelBuilder _accordionReactModelBuilder;
        private readonly IPageEditingAdapter _pageEditingAdapter;
        private readonly ILocalizationProvider _localizationProvider;

        public AccordionWithContentAreaListReactModelBuilder(IRichTextReactModelBuilder richTextReactModelBuilder, IContentAreaReactModelBuilder contentAreaReactModelBuilder, IAccordionReactModelBuilder accordionReactModelBuilder, IPageEditingAdapter pageEditingAdapter, ILocalizationProvider localizationProvider)
        {
            _richTextReactModelBuilder = richTextReactModelBuilder;
            _contentAreaReactModelBuilder = contentAreaReactModelBuilder;
            _accordionReactModelBuilder = accordionReactModelBuilder;
            _pageEditingAdapter = pageEditingAdapter;
            _localizationProvider = localizationProvider;
        }

        public ReactModels.AccordionWithContentAreaList BuildReactModel(params StructuredScheduleBlock[] blocks) =>
            new ReactModels.AccordionWithContentAreaList
            {
                Accordion = _accordionReactModelBuilder.BuildReactModel(),
                Content = BuildContent(blocks).ToList()
            };

        private IEnumerable<ReactModels.AccordionWithContentArea> BuildContent(params StructuredScheduleBlock[] blocks)
        {
            if (blocks.FirstOrDefault() is var first)
                yield return MapBlock(first, true);

            foreach (var block in blocks.Skip(1))
                yield return MapBlock(block, _pageEditingAdapter.PageIsInEditMode());

            ReactModels.AccordionWithContentArea MapBlock(StructuredScheduleBlock x, bool initiallyOpen = false) =>
                new ReactModels.AccordionWithContentArea
                {
                    Title = x.Title ?? x.DateTitle?.ToDisplayDate().CultureSpecificDayMonthYear(),
                    Content = new ReactModels.ContentArea
                    {
                        Blocks = GetBlocks(x).ToList(),
                    },
                    OnPageEditing = _pageEditingAdapter.PageIsInEditMode()
                    ? new ReactModels.AccordionWithContentArea_OnPageEditing
                    {
                        Title = nameof(x.DateTitle)
                    }
                    : null,
                    InitiallyOpen = initiallyOpen
                };
        }

        private IEnumerable<ReactModels.ContentAreaItem> GetBlocks(StructuredScheduleBlock currentBlock)
        {
            var contentAreaCount = currentBlock.ContentArea?.FilteredItems.Count() ?? 0;
            if (contentAreaCount > 0)
            {
                foreach (var block in _contentAreaReactModelBuilder.BuildReactModel(currentBlock.ContentArea, null).Blocks)
                    yield return block;
            }

            yield return new ReactModels.ContentAreaItem
            {
                Id = contentAreaCount.ToString(),
                ComponentName = nameof(ReactModels.RichTextBlock),
                ComponentData = new ReactModels.RichTextBlock
                {
                    Text = _richTextReactModelBuilder.BuildReactModel(currentBlock.RichText, nameof(currentBlock.RichText))
                }
            };

            yield return new ReactModels.ContentAreaItem
            {
                Id = $"{contentAreaCount + 1}",
                ComponentName = nameof(ReactModels.Schedule),
                ComponentData = new ReactModels.Schedule
                {
                    ProgramList = currentBlock.ProgramList?.Select(BuildProgramItem).ToList() ?? new List<ReactModels.Schedule_ProgramList>(),
                    OnPageEditing = _pageEditingAdapter.PageIsInEditMode()
                        ? new ReactModels.Schedule_OnPageEditing
                        {
                            ProgramList = nameof(currentBlock.ProgramList)
                        }
                        : null,
                    LastTitle = GetLabel("LastTitle")
                },
            };

            ReactModels.Schedule_ProgramList BuildProgramItem(ScheduleItem item) =>
            new ReactModels.Schedule_ProgramList
            {
                Title = item.Title,
                Time = item.Time,
                Text = _richTextReactModelBuilder.BuildReactModel(item.MainBody, null),
                IsBreak = item.IsBreak
            };
        }

        private string GetLabel(string key) => _localizationProvider.GetLabel(nameof(ReactModels.Schedule), key);
    }
}
