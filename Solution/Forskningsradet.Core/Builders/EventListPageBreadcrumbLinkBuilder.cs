﻿using System;
using System.Linq;
using System.Web;
using EPiServer.Web.Routing;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Extensions;
using Forskningsradet.Core.Models;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.SearchModels;
using Forskningsradet.Core.Services;

namespace Forskningsradet.Core.Builders
{
    public class EventListPageBreadcrumbLinkBuilder : IBreadcrumbLinkBuilder<EventListPage>
    {
        private readonly IUrlResolver _urlResolver;
        private readonly IPageRouteHelper _pageRouteHelper;
        private readonly HttpRequestBase _httpRequest;
        private readonly ILocalizationProvider _localizationProvider;

        public EventListPageBreadcrumbLinkBuilder(
            IUrlResolver urlResolver,
            IPageRouteHelper pageRouteHelper,
            HttpRequestBase httpRequest,
            ILocalizationProvider localizationProvider)
        {
            _urlResolver = urlResolver;
            _pageRouteHelper = pageRouteHelper;
            _httpRequest = httpRequest;
            _localizationProvider = localizationProvider;
        }

        public Models.ReactModels.Link Build(EventListPage page, BreadcrumbsBuildContext buildContext)
        {
            var linkText = page.GetListTitle();
            TimeFrameFilter? timeFrameFilter = null;

            if (_pageRouteHelper.PageLink.Equals(page.ContentLink))
                timeFrameFilter = Enum.TryParse(_httpRequest.QueryString[QueryParameters.TimeFrame], out TimeFrameFilter parsedTimeFrame)
                    ? parsedTimeFrame
                    : TimeFrameFilter.Future;

            if (buildContext.ChildPages.FirstOrDefault() is EventPage eventPage)
                timeFrameFilter = GetEventTimeFrame(eventPage);

            var eventGroupText = timeFrameFilter is null ? string.Empty : GetEventGroupText(timeFrameFilter.Value);
            var linkQuery = timeFrameFilter is null ? string.Empty : $"?{QueryParameters.TimeFrame}=" + (int)timeFrameFilter;

            return new Models.ReactModels.Link
            {
                Text = linkText + (string.IsNullOrEmpty(eventGroupText) ? string.Empty : " - " + eventGroupText),
                Url = _urlResolver.GetUrl(page) + linkQuery
            };
        }

        private TimeFrameFilter? GetEventTimeFrame(EventPage eventPage) =>
            eventPage.EventData is null
                ? (TimeFrameFilter?)null
                : eventPage.ComputedEventDate < DateTime.Today
                    ? TimeFrameFilter.Past
                    : TimeFrameFilter.Future;

        private string GetEventGroupText(TimeFrameFilter timeFrame)
        {
            switch (timeFrame)
            {
                case TimeFrameFilter.Future:
                    return GetLabel("OptionFuture");
                case TimeFrameFilter.Past:
                    return GetLabel("OptionPast");
                default:
                    return null;
            }
        }

        private string GetLabel(string key) => _localizationProvider.GetLabel(nameof(EventListPage), key);
    }
}