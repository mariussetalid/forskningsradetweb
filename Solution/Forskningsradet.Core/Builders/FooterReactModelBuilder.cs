﻿using System.Collections.Generic;
using System.Linq;
using EPiServer;
using EPiServer.Web.Routing;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Extensions;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Services.Contracts;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.Builders
{
    public class FooterReactModelBuilder : IFooterReactModelBuilder
    {
        private readonly IUrlResolver _urlResolver;
        private readonly IUrlCheckingService _urlCheckingService;
        private readonly IRichTextReactModelBuilder _richTextReactModelBuilder;

        public FooterReactModelBuilder(IUrlResolver urlResolver, IUrlCheckingService urlCheckingService, IRichTextReactModelBuilder richTextReactModelBuilder)
        {
            _urlResolver = urlResolver;
            _urlCheckingService = urlCheckingService;
            _richTextReactModelBuilder = richTextReactModelBuilder;
        }

        public ReactModels.Footer BuildReactModel(FooterBlock footer, string propertyName)
            => footer != null
                ? new ReactModels.Footer
                {
                    ContactInfo = _richTextReactModelBuilder.BuildReactModel(footer.ContactInfo, propertyName),
                    LinkLists = BuildLinkLists(footer.MainLinkList, footer.SecondaryLinkList),
                    NewsLetter = BuildNewsletterLink(footer.Button),
                    SocialMedia = BuildSocialMediaLinkList(footer),
                    InfoLinks = BuildInfoLinkList(footer.OtherLinkList)
                }
                : null;

        private List<ReactModels.Link> BuildInfoLinkList(LinkListBlock linkList)
            => BuildLinkList(linkList);

        private List<ReactModels.Footer_LinkLists> BuildLinkLists(params LinkListBlock[] linkListBlocks)
            => linkListBlocks
                .Where(x => x != null)
                .Select((x, index) =>
                    new ReactModels.Footer_LinkLists
                    {
                        Title = x.Title,
                        Id = index,
                        Links = BuildLinkList(x)
                    })
                .ToList();

        private List<ReactModels.Link> BuildLinkList(LinkListBlock linkList)
            => linkList?.LinkItems?.Select(x => new ReactModels.Link
            {
                Text = x.Text,
                Url = _urlResolver.GetUrl(x.Href),
                IsExternal = x.IsExternal(_urlCheckingService),
            })
            .ToList() ?? new List<ReactModels.Link>();

        private ReactModels.Link BuildNewsletterLink(EditLinkBlock linkBlock)
            => linkBlock?.Link is Url url
                ? new ReactModels.Link
                {
                    Text = linkBlock.Text,
                    Url = linkBlock.Link.GetUrl(_urlResolver),
                    IsExternal = linkBlock.Link.IsExternal(_urlCheckingService),
                    Id = linkBlock.AnchorId.EnsureValidHtmlId()
                }
                : null;

        private ReactModels.SocialMediaLinkList BuildSocialMediaLinkList(FooterBlock footer)
        {
            if (footer.FacebookLink is null && footer.LinkedinLink is null)
                return null;
            return new ReactModels.SocialMediaLinkList
            {
                Items = new List<ReactModels.SocialMediaLink>
                    {
                        footer.FacebookLink?.ToString() is string facebookUrl
                            ? new ReactModels.SocialMediaLink { Provider = ReactModels.SocialMediaLink_Provider.Facebook, Url = facebookUrl, Text = footer.FacebookAltText }
                            : null,
                        footer.LinkedinLink?.ToString() is string linkedInUrl
                            ? new ReactModels.SocialMediaLink { Provider = ReactModels.SocialMediaLink_Provider.Linkedin, Url = linkedInUrl, Text = footer.LinkedinAltText }
                            : null,
                        footer.TwitterLink?.ToString() is string twitterUrl
                            ? new ReactModels.SocialMediaLink { Provider = ReactModels.SocialMediaLink_Provider.Twitter, Url = twitterUrl, Text = footer.TwitterAltText }
                            : null,
                        _urlResolver.GetUrl(footer.RssLink) is string rssUrl
                            ? new ReactModels.SocialMediaLink { Provider = ReactModels.SocialMediaLink_Provider.Rss, Url = rssUrl, Text = footer.RssAltText }
                            : null
                    }
                    .Where(x => x != null)
                    .ToList()
            };
        }
    }
}
