﻿using System;
using System.Collections.Generic;
using System.Linq;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Extensions;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Services;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.Builders
{
    public class DateCardDatesReactModelBuilder : IDateCardDatesReactModelBuilder
    {
        private readonly ILocalizationProvider _localizationProvider;

        public DateCardDatesReactModelBuilder(ILocalizationProvider localizationProvider)
        {
            _localizationProvider = localizationProvider;
        }

        public ReactModels.DateCardDates BuildReactModel(EventPage page, string propertyName, ReactModels.EventImage eventImage = null) =>
            page is null ? null : MapEventInList(page, eventImage);

        public ReactModels.DateCardDates BuildReactModel(ProposalBasePage page, string datesTitle, string datesDescription, bool isPlanned = false) =>
            page is null ? null : MapProposalInList(page, datesTitle, datesDescription, isPlanned);

        public ReactModels.DateCardDates BuildReactModel(IList<ReactModels.DateCardDates_Dates> dates, string datesTitle) =>
            new ReactModels.DateCardDates
            {
                DatesTitle = datesTitle,
                Dates = dates
            };

        private ReactModels.DateCardDates MapEventInList(EventPage page, ReactModels.EventImage eventImage = null) =>
            new ReactModels.DateCardDates
            {
                Labels = GetEventItemLabels(),
                IsPastDate = CheckIfPastEvent(page),
                Dates = BuildDateList(page.StartDate, page.EndDate) ?? new List<ReactModels.DateCardDates_Dates>(),
                EventImage = eventImage
            };

        private ReactModels.DateCardDates MapProposalInList(ProposalBasePage page, string datesTitle, string datesDescription, bool isPlanned) =>
            new ReactModels.DateCardDates
            {
                IsPastDate = CheckIfPastProposal(page),
                IsInternational = page is InternationalProposalPage,
                IsPlanned = isPlanned,
                DatesTitle = datesTitle,
                DatesDescription = datesDescription,
                Dates = BuildDateList(page),
            };

        private ReactModels.DateCardDates_Labels GetEventItemLabels() =>
            new ReactModels.DateCardDates_Labels
            {
                PastDate = GetLabel("PastDate")
            };

        private static bool CheckIfPastEvent(EventPage page) =>
            page.ComputedEventEndDate?.IsPast() ?? false;

        private static bool CheckIfPastProposal(ProposalBasePage page) =>
            page.ProposalState == Models.ApiModels.Evurdering.ProposalState.Completed
            || (page.DeadlineComputed?.ToDisplayDate().IsPast() ?? false);

        private List<ReactModels.DateCardDates_Dates> BuildDateList(DateTime? startDate, DateTime? endDate)
        {
            var dates = new List<DateTime>();
            if (startDate is DateTime start)
                dates.Add(start);

            if (endDate is DateTime end && startDate?.Date != end.Date)
                dates.Add(end);

            return MapDateList(dates.ToArray());
        }

        private static List<ReactModels.DateCardDates_Dates> MapDateList(params DateTime[] dates) =>
            dates
                .Select(utc => utc.ToDisplayDate())
                .Select(dateTime => new ReactModels.DateCardDates_Dates
                {
                    Day = dateTime.CultureSpecificDay(),
                    Month = dateTime.ToString(DateTimeFormats.ShortMonth),
                    Year = dateTime.Year is int year && year > 0 && year != DateTime.UtcNow.Year ? year.ToString() : null,
                })
                .ToList();

        private static List<ReactModels.DateCardDates_Dates> BuildDateList(ProposalBasePage page) =>
            new List<ReactModels.DateCardDates_Dates>
            {
                page.DeadlineComputed?.ToDisplayDate() is DateTime date
                    ? new ReactModels.DateCardDates_Dates
                    {
                        Day = date.CultureSpecificDay(),
                        Month = date.ToString(DateTimeFormats.ShortMonthYear)
                    }
                    : new ReactModels.DateCardDates_Dates()
            };

        private string GetLabel(string key) =>
            _localizationProvider.GetLabel(nameof(EventListPage), key);
    }
}
