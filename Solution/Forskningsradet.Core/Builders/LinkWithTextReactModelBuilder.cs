﻿using EPiServer;
using EPiServer.Web.Routing;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Extensions;
using Forskningsradet.Core.Facades.Contracts;
using Forskningsradet.Core.Services.Contracts;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.Builders
{
    public class LinkWithTextReactModelBuilder : ILinkWithTextReactModelBuilder
    {
        private readonly IUrlResolver _urlResolver;
        private readonly IUrlCheckingService _urlCheckingService;
        private readonly IPageEditingAdapter _pageEditingAdapter;

        public LinkWithTextReactModelBuilder(
            IUrlResolver urlResolver,
            IUrlCheckingService urlCheckingService,
            IPageEditingAdapter pageEditingAdapter)
        {
            _urlResolver = urlResolver;
            _urlCheckingService = urlCheckingService;
            _pageEditingAdapter = pageEditingAdapter;
        }

        public ReactModels.LinkWithText BuildReactModel(string linkTitle, Url linkUrl, string text, ReactModels.LinkWithText_OnPageEditing onPageEditingComponent) =>
            new ReactModels.LinkWithText
            {
                OnPageEditing = _pageEditingAdapter.PageIsInEditMode() && onPageEditingComponent != null
                    ? onPageEditingComponent
                    : null,
                Text = text,
                Link = BuildLink(linkTitle, linkUrl)
            };

        ReactModels.Link BuildLink(string linkTitle, Url url) =>
            new ReactModels.Link
            {
                Text = linkTitle,
                Url = url.GetUrl(_urlResolver),
                IsExternal = url.IsExternal(_urlCheckingService)
            };
    }
}
