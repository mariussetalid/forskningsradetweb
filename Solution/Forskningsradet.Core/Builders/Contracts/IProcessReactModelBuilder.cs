﻿using System.Collections.Generic;
using Forskningsradet.Core.Models.ContentModels.PropertyList;
using Forskningsradet.Core.Models.Enums;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.Builders.Contracts
{
    public interface IProcessReactModelBuilder
    {
        ReactModels.Process BuildReactModel(string title, string text, IList<ProcessItem> items, ProcessStyle style = ProcessStyle.None);
    }
}
