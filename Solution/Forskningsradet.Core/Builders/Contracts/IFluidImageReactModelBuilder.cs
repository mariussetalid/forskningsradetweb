﻿using EPiServer.Core;
using Forskningsradet.Core.Models.ContentModels.Media;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.Builders.Contracts
{
    public interface IFluidImageReactModelBuilder
    {
        ReactModels.FluidImage BuildReactModel(ContentReference imageReference, string propertyName);
        ReactModels.FluidImage BuildReactModel(ImageFile imageFile, string propertyName);
    }
}
