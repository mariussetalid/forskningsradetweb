﻿using System.Collections.Generic;
using EPiServer.Core;
using Forskningsradet.Core.Models.ContentModels.Pages;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.Builders.Contracts
{
    public interface ITagLinkListReactModelBuilder
    {
        ReactModels.TagLinkList BuildReactModel(CategoryList categories, CategoryList inactiveCategories, IReadOnlyList<ContentAreaPage> categoryHomePages, bool leftAlign, bool includeCategoriesWithoutHomePage);
    }
}
