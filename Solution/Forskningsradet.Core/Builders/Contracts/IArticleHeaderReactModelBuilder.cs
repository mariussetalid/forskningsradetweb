﻿using Forskningsradet.Core.Models.ContentModels.Pages;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.Builders.Contracts
{
    public interface IArticleHeaderReactModelBuilder
    {
        ReactModels.ArticleHeader BuildReactModel(EditorialPage page);
        ReactModels.ArticleHeader BuildReactModel(EditorialPage pageForDate, EditorialPage page);
        ReactModels.ArticleHeader BuildReactModelWithoutShare(EditorialPage page);
    }
}
