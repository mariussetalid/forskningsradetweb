﻿using EPiServer;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.Builders.Contracts
{
    public interface ILinkWithTextReactModelBuilder
    {
        ReactModels.LinkWithText BuildReactModel(string linkTitle, Url linkUrl, string text, ReactModels.LinkWithText_OnPageEditing onPageEditingComponent);
    }
}
