﻿using EPiServer.Core;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.Builders.Contracts
{
    public interface IDateCardTagsReactModelBuilder
    {
        ReactModels.DateCardTags BuildReactModel(CategoryList categories, int numberOfVisibleItems);
    }
}
