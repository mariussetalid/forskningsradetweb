﻿using EPiServer.Core;
using Forskningsradet.Core.Models;

namespace Forskningsradet.Core.Builders.Contracts
{
    public interface IBreadcrumbLinkBuilder<TPage> where TPage : PageData
    {
        Models.ReactModels.Link Build(TPage page, BreadcrumbsBuildContext buildContext);
    }
}
