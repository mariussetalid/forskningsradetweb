﻿using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.QueryParameters;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.Builders.Contracts
{
    public interface IDateCardReactModelBuilder
    {
        ReactModels.DateCard BuildReactModel(EventPage page, RenderOptionQueryParameter renderOptionQueryParameter = null, bool useText = true, bool portfolioMode = false, bool skipTags = false);
    }
}
