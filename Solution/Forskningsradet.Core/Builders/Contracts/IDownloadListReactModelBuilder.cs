﻿using Forskningsradet.Core.Models.ContentModels.Pages;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.Builders.Contracts
{
    public interface IDownloadListReactModelBuilder
    {
        ReactModels.DownloadList BuildReactModel(ProposalPage proposalPage);

        ReactModels.DownloadList BuildTemplatesList(ProposalPage proposalPage);
    }
}
