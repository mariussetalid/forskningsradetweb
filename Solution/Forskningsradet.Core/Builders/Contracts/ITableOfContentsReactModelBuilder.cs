﻿using Forskningsradet.Core.Models.RequestModels;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.Builders.Contracts
{
    public interface ITableOfContentsReactModelBuilder
    {
        ReactModels.TableOfContents BuildReactModel(LargeDocumentModel model);
    }
}
