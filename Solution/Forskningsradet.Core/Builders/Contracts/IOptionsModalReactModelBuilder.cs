﻿using EPiServer;
﻿using EPiServer.Core;
using Forskningsradet.Core.Models.ContentModels.Pages;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.Builders.Contracts
{
    public interface IOptionsModalReactModelBuilder
    {
        ReactModels.OptionsModal BuildShareContent(EditorialPage page);
        ReactModels.OptionsModal BuildShareContent(PageReference pageReference, string queryParameters, string anchorId, string title, params ReactModels.ShareOptions_Items_Icon[] shareOptions);
        ReactModels.OptionsModal BuildDownloadContent(ProposalPage proposalPage);
    }
}
