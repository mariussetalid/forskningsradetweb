﻿using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ContentModels.Pages;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.Builders.Contracts
{
    public interface IContactInfoReactModelBuilder
    {
        ReactModels.ContactInfo BuildReactModel(PersonPage page, string propertyName, bool showPhoneNumber, bool showJobTitle);
        ReactModels.ContactInfo BuildReactModel(NonPersonContactBlock block, string propertyName, bool showPhoneNumber);
    }
}
