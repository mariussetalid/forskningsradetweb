﻿using EPiServer.Core;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.Builders.Contracts
{
    public interface ISocialMediaLinkListReactModelBuilder
    {
        ReactModels.SocialMediaLinkList BuildReactModel(PageReference currentPageReference);
    }
}
