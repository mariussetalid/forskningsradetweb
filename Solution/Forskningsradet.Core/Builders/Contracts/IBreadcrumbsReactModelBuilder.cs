﻿using System.Collections.Generic;
using EPiServer.Core;
using Forskningsradet.Core.Models.ContentModels.Pages;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.Builders.Contracts
{
    public interface IBreadcrumbsReactModelBuilder
    {
        ReactModels.Breadcrumbs BuildReactModel(FrontPageBase closestFrontPage, PageReference currentPageReference);
        ReactModels.Breadcrumbs BuildReactModel(PageReference closestFrontPageReference, PageReference currentPageReference, string pageNameOverride, IEnumerable<EditorialPage> appendedPages);
    }
}
