﻿using System.Collections.Generic;
using Forskningsradet.Core.Models.QueryParameters;
using Forskningsradet.Core.Models.ViewModels.Contracts;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.Builders.Contracts
{
    public interface IContentAreaReactModelBuilder
    {
        ReactModels.ContentArea BuildReactModel(EPiServer.Core.ContentArea contentArea, string contentAreaName, RenderOptionQueryParameter renderOptionQueryParameter = null);
        ReactModels.ContentArea BuildReactModel(IList<ReactModels.ContentAreaItem> contentAreaItems, string contentAreaName);
        IReactViewModel BuildContentAreaPreviewItem(EPiServer.Core.ContentReference contentLink);
    }
}
