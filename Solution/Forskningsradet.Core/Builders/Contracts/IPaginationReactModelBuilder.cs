﻿using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.Builders.Contracts
{
    public interface IPaginationReactModelBuilder
    {
        ReactModels.Pagination BuildReactModel(string currentQueryString, int currentPage, int totalPages, int pageSize, string listPageUrl);
    }
}
