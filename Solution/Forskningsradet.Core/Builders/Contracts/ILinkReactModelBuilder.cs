﻿using EPiServer;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.Builders.Contracts
{
    public interface ILinkReactModelBuilder
    {
        ReactModels.Link BuildLink(Url url, string linkText);
        ReactModels.Link BuildLink(EditLinkBlock editLinkBlock, string propertyName = null);
    }
}