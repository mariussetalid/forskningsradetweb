﻿using EPiServer.Core;
using Forskningsradet.Core.Models.QueryParameters;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.Builders.Contracts
{
    public interface IRichTextReactModelBuilder
    {
        ReactModels.RichText BuildReactModel(XhtmlString richText, string richTextName);
    }
}
