﻿using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ContentModels.Pages;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.Builders.Contracts
{
    public interface IProposalContactReactModelBuilder
    {
        ReactModels.ProposalContact BuildReactModel(PersonPage personPage, bool showPhoneNumber, bool showJobTitle);
        ReactModels.ProposalContact BuildReactModel(NonPersonContactBlock contactBlock, bool showPhoneNumber);
    }
}
