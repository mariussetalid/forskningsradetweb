﻿using Forskningsradet.Core.Models.ContentModels.Blocks;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.Builders.Contracts
{
    public interface ICampaignBlockReactModelBuilder
    {
        ReactModels.CampaignBlock BuildReactModel(CampaignBlock block);
    }
}
