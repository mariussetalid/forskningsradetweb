﻿using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.Builders.Contracts
{
    public interface IAccordionReactModelBuilder
    {
        ReactModels.Accordion BuildReactModel(bool initiallyOpen = false);
    }
}
