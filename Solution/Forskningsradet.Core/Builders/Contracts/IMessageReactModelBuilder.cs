﻿using Forskningsradet.Core.Models.Enums;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.Builders.Contracts
{
    public interface IMessageReactModelBuilder
    {
        ReactModels.Message BuildReactModel(string text, string propertyName, MessageTheme theme = MessageTheme.Yellow);
    }
}
