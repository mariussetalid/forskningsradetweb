﻿using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.QueryParameters;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.Builders.Contracts
{
    public interface IArticleBlockReactModelBuilder
    {
        ReactModels.ArticleBlock BuildReactModel(EditorialPage page, RenderWidthOption renderWidthOption);
    }
}
