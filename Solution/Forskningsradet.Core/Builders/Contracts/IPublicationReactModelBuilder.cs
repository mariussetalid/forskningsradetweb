﻿using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.Enums;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.Builders.Contracts
{
    public interface IPublicationReactModelBuilder
    {
        ReactModels.SearchResult BuildReactModel(PublicationBasePage page, PublicationMetadata metadata, bool showMetadataPdfIcon);
        ReactModels.SearchResult BuildReactModel(PublicationBasePage page, PublicationMetadata metadata, bool showMetadataPdfIcon, string unifiedHitExcerpt);
    }
}