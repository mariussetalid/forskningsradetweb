﻿using System.Collections;
using System.Collections.Generic;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.QueryParameters;
using MailChimp.Net.Models;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.Builders.Contracts
{
    public interface IDateCardDatesReactModelBuilder
    {
        ReactModels.DateCardDates BuildReactModel(EventPage page, string propertyName, ReactModels.EventImage eventImage = null);
        ReactModels.DateCardDates BuildReactModel(ProposalBasePage page, string datesTitle, string datesDescription, bool isPlanned = false);
        ReactModels.DateCardDates BuildReactModel(IList<ReactModels.DateCardDates_Dates> dates, string datesTitle);
    }
}
