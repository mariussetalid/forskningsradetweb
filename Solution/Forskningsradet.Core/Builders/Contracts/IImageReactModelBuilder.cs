﻿using EPiServer.Core;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.Builders.Contracts
{
    public interface IImageReactModelBuilder
    {
        ReactModels.Image BuildImage(ContentReference imageLink);
    }
}