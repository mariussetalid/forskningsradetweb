﻿using Forskningsradet.Core.Models.ContentModels.Blocks;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.Builders.Contracts
{
    public interface IAccordionWithContentAreaListReactModelBuilder
    {
        ReactModels.AccordionWithContentAreaList BuildReactModel(params StructuredScheduleBlock[] blocks);
    }
}
