﻿using System.Collections.Generic;
using Forskningsradet.Core.Models.ApiModels.Evurdering;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.Builders.Contracts
{
    public interface IDateCardStatusReactModelBuilder
    {
        IEnumerable<ReactModels.DateCardStatus> BuildDateCardStatusList(ProposalState state, bool hasResultStatus);
        ReactModels.DateCardStatus BuildReactModel(ProposalState state);
    }
}
