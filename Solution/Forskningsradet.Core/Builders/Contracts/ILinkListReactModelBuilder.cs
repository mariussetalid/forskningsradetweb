﻿using EPiServer.SpecializedProperties;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.Builders.Contracts
{
    public interface ILinkListReactModelBuilder
    {
        ReactModels.LinkList BuildReactModel(LinkItemCollection linkItemCollection, string propertyName);
        ReactModels.NestedLink_Item BuildNestedLinksReactModel(string title, LinkItemCollection linkItemCollection);
    }
}
