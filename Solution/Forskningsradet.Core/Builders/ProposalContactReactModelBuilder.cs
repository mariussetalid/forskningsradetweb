﻿using System.Collections.Generic;
using System.Linq;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Services.Contracts;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.Builders
{
    public class ProposalContactReactModelBuilder : IProposalContactReactModelBuilder
    {
        private readonly IPersonPageStringBuilder _personPageStringBuilder;

        public ProposalContactReactModelBuilder(IPersonPageStringBuilder personPageStringBuilder) =>
            _personPageStringBuilder = personPageStringBuilder;

        public ReactModels.ProposalContact BuildReactModel(PersonPage personPage, bool showPhoneNumber, bool showJobTitle) =>
            new ReactModels.ProposalContact
            {
                Title = _personPageStringBuilder.BuildTitle(personPage),
                Details = BuildLinks(personPage, showPhoneNumber, showJobTitle).ToList()
            };

        public ReactModels.ProposalContact BuildReactModel(NonPersonContactBlock contactBlock, bool showPhoneNumber) =>
            new ReactModels.ProposalContact
            {
                Title = contactBlock.Title,
                Details = BuildLinks(contactBlock, showPhoneNumber).ToList()
            };

        private IEnumerable<ReactModels.Link> BuildLinks(NonPersonContactBlock contactBlock, bool showPhoneNumber)
        {
            if (!string.IsNullOrWhiteSpace(contactBlock.Text))
                yield return new ReactModels.Link { Text = contactBlock.Text };

            if (!string.IsNullOrWhiteSpace(contactBlock.Email))
                yield return GetEmailLink(contactBlock.Email);

            if (showPhoneNumber && !string.IsNullOrWhiteSpace(contactBlock.Phone))
                yield return GetPhoneLink(contactBlock.Phone);
        }

        private IEnumerable<ReactModels.Link> BuildLinks(PersonPage personPage, bool showPhoneNumber, bool showJobTitle)
        {
            if (showJobTitle && !string.IsNullOrWhiteSpace(personPage.JobTitle))
                yield return new ReactModels.Link { Text = personPage.JobTitle };

            if (!string.IsNullOrWhiteSpace(personPage.Department))
                yield return new ReactModels.Link { Text = personPage.Department };

            if (!string.IsNullOrWhiteSpace(personPage.Email))
                yield return GetEmailLink(personPage.Email);

            if (showPhoneNumber && !string.IsNullOrWhiteSpace(personPage.Phone))
                yield return GetPhoneLink(personPage.Phone);

            if (showPhoneNumber && !string.IsNullOrWhiteSpace(personPage.Mobile))
            {
                yield return new ReactModels.Link
                {
                    Text = personPage.Mobile,
                    Url = _personPageStringBuilder.BuildPhoneUrlScheme(personPage.Mobile)
                };
            }
        }

        private ReactModels.Link GetEmailLink(string email) =>
            new ReactModels.Link
            {
                Text = email,
                Url = _personPageStringBuilder.BuildEmailUrlScheme(email)
            };

        private ReactModels.Link GetPhoneLink(string phone) =>
            new ReactModels.Link
            {
                Text = phone,
                Url = _personPageStringBuilder.BuildPhoneUrlScheme(phone)
            };
    }
}
