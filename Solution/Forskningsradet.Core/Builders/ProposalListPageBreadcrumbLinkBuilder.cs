﻿using System;
using System.Linq;
using System.Web;
using EPiServer.Web.Routing;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Extensions;
using Forskningsradet.Core.Models;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.SearchModels;
using Forskningsradet.Core.Services;
using Forskningsradet.Core.Services.Contracts;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.Builders
{
    public class ProposalListPageBreadcrumbLinkBuilder : IBreadcrumbLinkBuilder<ProposalListPage>
    {
        private readonly IProposalPageStatusHandler _proposalPageStatusHandler;
        private readonly IUrlResolver _urlResolver;
        private readonly IPageRouteHelper _pageRouteHelper;
        private readonly HttpRequestBase _httpRequest;
        private readonly ILocalizationProvider _localizationProvider;

        public ProposalListPageBreadcrumbLinkBuilder(
            IProposalPageStatusHandler proposalPageStatusHandler,
            IUrlResolver urlResolver,
            IPageRouteHelper pageRouteHelper,
            HttpRequestBase httpRequest,
            ILocalizationProvider localizationProvider)
        {
            _proposalPageStatusHandler = proposalPageStatusHandler;
            _urlResolver = urlResolver;
            _httpRequest = httpRequest;
            _pageRouteHelper = pageRouteHelper;
            _localizationProvider = localizationProvider;
        }

        public ReactModels.Link Build(ProposalListPage page, BreadcrumbsBuildContext buildContext)
        {
            var linkText = page.GetListTitle();
            TimeFrameFilter? timeFrameFilter = null;

            if (_pageRouteHelper.PageLink.Equals(page.ContentLink))
                timeFrameFilter = Enum.TryParse(_httpRequest.QueryString[QueryParameters.TimeFrame], out TimeFrameFilter parsedTimeFrame)
                    ? parsedTimeFrame
                    : TimeFrameFilter.Future;

            if (buildContext.ChildPages.FirstOrDefault() is ProposalBasePage childProposal)
                timeFrameFilter = _proposalPageStatusHandler.GetStatus(childProposal);

            var proposalGroupText = timeFrameFilter is null ? string.Empty : GetProposalGroupText(timeFrameFilter.Value);
            var linkQuery = timeFrameFilter is null ? string.Empty : $"?{QueryParameters.TimeFrame}=" + (int)timeFrameFilter;

            return new ReactModels.Link
            {
                Text = linkText + (string.IsNullOrEmpty(proposalGroupText) ? string.Empty : " - " + proposalGroupText),
                Url = _urlResolver.GetUrl(page) + linkQuery
            };
        }

        private string GetProposalGroupText(TimeFrameFilter proposalStatus)
        {
            switch (proposalStatus)
            {
                case TimeFrameFilter.Future:
                    return GetLabel("StatusFilterCurrent");
                case TimeFrameFilter.Past:
                    return GetLabel("StatusFilterCompleted");
                case TimeFrameFilter.Result:
                    return GetLabel("StatusFilterResult");
                default:
                    return null;
            }
        }

        private string GetLabel(string key) => _localizationProvider.GetLabel(nameof(ProposalListPage), key);
    }
}
