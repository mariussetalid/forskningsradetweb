﻿using System;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Services;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.Builders
{
    public class AccordionReactModelBuilder : IAccordionReactModelBuilder
    {
        private readonly ILocalizationProvider _localizationProvider;

        public AccordionReactModelBuilder(ILocalizationProvider localizationProvider)
        {
            _localizationProvider = localizationProvider;
        }

        public ReactModels.Accordion BuildReactModel(bool initiallyOpen = false) =>
            new ReactModels.Accordion
            {
                CollapseLabel = GetLabel("ShowLess"),
                ExpandLabel = GetLabel("ShowMore"),
                Guid = Guid.NewGuid().ToString(),
                InitiallyOpen = initiallyOpen
            };

        private string GetLabel(string key) =>
            _localizationProvider.GetLabel(nameof(ReactModels.AccordionBlock), key);
    }
}
