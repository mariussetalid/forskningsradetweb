﻿using EPiServer;
using EPiServer.Core;
using EPiServer.Web.Routing;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Extensions;
using Forskningsradet.Core.Facades.Contracts;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ContentModels.Media;
using Forskningsradet.Core.Services.Contracts;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.Builders
{
    public class CampaignBlockReactModelBuilder : ICampaignBlockReactModelBuilder
    {
        private readonly IFluidImageReactModelBuilder _fluidImageReactModelBuilder;
        private readonly IContentLoader _contentLoader;
        private readonly IUrlResolver _urlResolver;
        private readonly IUrlCheckingService _urlCheckingService;
        private readonly IPageEditingAdapter _pageEditingAdapter;

        public CampaignBlockReactModelBuilder(
            IFluidImageReactModelBuilder fluidImageReactModelBuilder,
            IContentLoader contentLoader,
            IUrlResolver urlResolver,
            IUrlCheckingService urlCheckingService,
            IPageEditingAdapter pageEditingAdapter)
        {
            _fluidImageReactModelBuilder = fluidImageReactModelBuilder;
            _contentLoader = contentLoader;
            _urlResolver = urlResolver;
            _urlCheckingService = urlCheckingService;
            _pageEditingAdapter = pageEditingAdapter;
        }

        public ReactModels.CampaignBlock BuildReactModel(CampaignBlock currentBlock)
        {
            var imageFile = GetImageFile(currentBlock.Image);
            return new ReactModels.CampaignBlock
            {
                OnPageEditing = _pageEditingAdapter.PageIsInEditMode()
                    ? new ReactModels.CampaignBlock_OnPageEditing
                    {
                        Title = nameof(currentBlock.Title)
                    }
                    : null,
                Title = currentBlock.Title,
                Cta = new ReactModels.Link
                {
                    OnPageEditing = _pageEditingAdapter.PageIsInEditMode()
                            ? nameof(currentBlock.Button)
                            : null,
                    Text = currentBlock.Button?.Text,
                    Url = GetUrl(currentBlock.Button?.Link),
                    IsExternal = currentBlock.Button?.Link.IsExternal(_urlCheckingService) ?? false,
                    Id = currentBlock.Button?.AnchorId.EnsureValidHtmlId()
                },
                Image = _fluidImageReactModelBuilder.BuildReactModel(imageFile, nameof(currentBlock.Image)),
                EditorTheme = (ReactModels.CampaignBlock_EditorTheme)(int)currentBlock.Style
            };
        }

        private ImageFile GetImageFile(ContentReference reference) =>
            reference is null ? null : _contentLoader.Get<ImageFile>(reference);

        private string GetUrl(Url url) =>
            url.GetUrl(_urlResolver);
    }
}
