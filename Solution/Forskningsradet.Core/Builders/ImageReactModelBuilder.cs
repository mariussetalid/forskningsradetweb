﻿using EPiServer;
using EPiServer.Core;
using EPiServer.Web.Routing;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Models.ContentModels.Media;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.Builders
{
    public class ImageReactModelBuilder : IImageReactModelBuilder
    {
        private readonly IContentLoader _contentLoader;
        private readonly IUrlResolver _urlResolver;

        public ImageReactModelBuilder(IContentLoader contentLoader, IUrlResolver urlResolver)
        {
            _contentLoader = contentLoader;
            _urlResolver = urlResolver;
        }

        public ReactModels.Image BuildImage(ContentReference reference)
        {
            return _contentLoader.TryGet(reference, out ImageData image)
                ?
                new ReactModels.Image
                {
                    Alt = (image as ImageFile)?.AltText ?? image.Name,
                    Src = _urlResolver.GetUrl(image.ContentLink),
                }
                : null;
        }
    }
}