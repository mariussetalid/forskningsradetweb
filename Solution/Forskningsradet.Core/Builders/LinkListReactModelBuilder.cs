﻿using System.Collections.Generic;
using System.Linq;
using EPiServer.SpecializedProperties;
using EPiServer.Web.Routing;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Extensions;
using Forskningsradet.Core.Facades.Contracts;
using Forskningsradet.Core.Services.Contracts;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.Builders
{
    public class LinkListReactModelBuilder : ILinkListReactModelBuilder
    {
        private readonly IUrlResolver _urlResolver;
        private readonly IUrlCheckingService _urlCheckingService;
        private readonly IPageEditingAdapter _pageEditingAdapter;

        public LinkListReactModelBuilder(
            IUrlResolver urlResolver,
            IUrlCheckingService urlCheckingService,
            IPageEditingAdapter pageEditingAdapter)
        {
            _urlResolver = urlResolver;
            _urlCheckingService = urlCheckingService;
            _pageEditingAdapter = pageEditingAdapter;
        }

        public ReactModels.LinkList BuildReactModel(LinkItemCollection linkItemCollection, string propertyName) =>
            new ReactModels.LinkList
            {
                OnPageEditing = !string.IsNullOrEmpty(propertyName) && _pageEditingAdapter.PageIsInEditMode()
                    ? new ReactModels.LinkList_OnPageEditing
                    {
                        Items = nameof(propertyName)
                    }
                    : null,
                Items = linkItemCollection?.Select(BuildLink).ToList() ?? new List<ReactModels.Link>()
            };

        public ReactModels.NestedLink_Item BuildNestedLinksReactModel(string title, LinkItemCollection linkItemCollection) =>
            new ReactModels.NestedLink_Item
            {
                Title = title,
                Items = linkItemCollection?.Select(BuildLink).ToList() ?? new List<ReactModels.Link>()
            };

        private ReactModels.Link BuildLink(LinkItem item) =>
            new ReactModels.Link
            {
                Text = item.Text,
                Url = new EPiServer.Url(item.Href).GetUrl(_urlResolver),
                IsExternal = item.IsExternal(_urlCheckingService)
            };
    }
}
