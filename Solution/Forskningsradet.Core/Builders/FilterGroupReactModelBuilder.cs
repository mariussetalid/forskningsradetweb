﻿using System.Collections.Generic;
using System.Linq;
using Forskningsradet.Core.Models.SearchModels;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.Builders
{
    public class FilterGroupReactModelBuilder
    {
        public ReactModels.FilterGroup BuildReactModel(ReactModels.Accordion accordion, string title, string optionName, List<FilterOption> options, string[] selection) =>
            new ReactModels.FilterGroup
            {
                Accordion = accordion,
                Title = title,
                Options = BuildOptions(optionName, options, selection)
            };

        public ReactModels.FilterGroup BuildReactModel(ReactModels.Accordion accordion, string title, string optionName, List<FilterOption> options, int[] selection) =>
            new ReactModels.FilterGroup
            {
                Accordion = accordion,
                Title = title,
                Options = BuildOptions(optionName, options, selection?.Select(x => x.ToString()).ToArray())
            };

        private static List<ReactModels.FilterGroup_Options> BuildOptions(string name, List<FilterOption> options, string[] checkedOptions)
        {
            return options
                .Select(x => new ReactModels.FilterGroup_Options
                {
                    Label = $"{x.Name}" + (x.Count > 0 ? $" ({x.Count})" : string.Empty),
                    Name = name,
                    Value = x.Value,
                    Checked = checkedOptions?.Contains(x.Value) ?? false,
                    SubOptions = x is NestedFilterOption nested ? BuildCheckboxes(nested.FilterOptions) : null
                })
                .ToList();

            List<ReactModels.Checkbox> BuildCheckboxes(List<FilterOption> nestedFilterOptions) =>
                nestedFilterOptions
                    .Select(x => new ReactModels.Checkbox
                    {
                        Label = $"{x.Name} ({x.Count})",
                        Name = name,
                        Value = x.Value,
                        Checked = checkedOptions?.Contains(x.Value) ?? false
                    })
                    .ToList();
        }
    }
}
