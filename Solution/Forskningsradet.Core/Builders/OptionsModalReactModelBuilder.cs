﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using EPiServer;
using EPiServer.Core;
using EPiServer.Web.Routing;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Extensions;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Services;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.Builders
{
    public class OptionsModalReactModelBuilder : IOptionsModalReactModelBuilder
    {
        private readonly IUrlResolver _urlResolver;
        private readonly ILocalizationProvider _localizationProvider;
        private readonly IContentLoader _contentLoader;
        private readonly IDownloadListReactModelBuilder _downloadListReactModelBuilder;

        private readonly ReactModels.ShareOptions_Items_Icon[] AllOptions = new ReactModels.ShareOptions_Items_Icon[]
        {
            ReactModels.ShareOptions_Items_Icon.Mail,
            ReactModels.ShareOptions_Items_Icon.Twitter,
            ReactModels.ShareOptions_Items_Icon.Facebook,
            ReactModels.ShareOptions_Items_Icon.Linkedin,
        };

        public OptionsModalReactModelBuilder(
            IUrlResolver urlResolver,
            ILocalizationProvider localizationProvider,
            IContentLoader contentLoader,
            IDownloadListReactModelBuilder downloadListReactModelBuilder)
        {
            _urlResolver = urlResolver;
            _localizationProvider = localizationProvider;
            _contentLoader = contentLoader;
            _downloadListReactModelBuilder = downloadListReactModelBuilder;
        }

        public ReactModels.OptionsModal BuildShareContent(EditorialPage page)
        {
            return new ReactModels.OptionsModal
            {
                OpenButtonText = GetLabel("OpenButtonText"),
                CloseButtonLabel = GetLabel("CloseLabel"),
                ShareContent = new ReactModels.ShareOptions
                {
                    Items = BuildShareLinks(page.PageLink, null, null, page.SeoSettings?.Title ?? page.PageName, AllOptions).ToList()
                }
            };
        }

        public ReactModels.OptionsModal BuildShareContent(PageReference pageReference, string queryParameters, string anchorId, string title, params ReactModels.ShareOptions_Items_Icon[] shareOptions) =>
            new ReactModels.OptionsModal
            {
                OpenButtonText = GetLabel("OpenButtonText"),
                CloseButtonLabel = GetLabel("CloseLabel"),
                ShareContent = new ReactModels.ShareOptions
                {
                    Items = BuildShareLinks(pageReference, queryParameters, anchorId, title, shareOptions?.Count() > 0 ? shareOptions : AllOptions).ToList()
                }
            };

        public ReactModels.OptionsModal BuildDownloadContent(ProposalPage proposalPage) =>
            new ReactModels.OptionsModal
            {
                OpenButtonText = GetLabel("DownloadTemplates"),
                CloseButtonLabel = GetLabel("CloseLabel"),
                DownloadContent = _downloadListReactModelBuilder.BuildReactModel(proposalPage)
            };

        private IEnumerable<ReactModels.ShareOptions_Items> BuildShareLinks(PageReference pageReference, string query, string anchorId, string name, params ReactModels.ShareOptions_Items_Icon[] items)
        {
            var link = pageReference.GetExternalUrl(_urlResolver) + GetQueryString(query) + GetAnchor(anchorId.EnsureValidHtmlId());
            link = HttpUtility.UrlEncode(link);
            var title = HttpUtility.UrlEncode(name);

            if (items.Contains(ReactModels.ShareOptions_Items_Icon.Mail))
                yield return new ReactModels.ShareOptions_Items
                {
                    Icon = ReactModels.ShareOptions_Items_Icon.Mail,
                    Text = ShareConstants.Mail,
                    Url = string.Format(ShareConstants.MailUrlFormat, title, link)
                };

            if (items.Contains(ReactModels.ShareOptions_Items_Icon.Twitter))
                yield return new ReactModels.ShareOptions_Items
                {
                    Icon = ReactModels.ShareOptions_Items_Icon.Twitter,
                    Text = ShareConstants.Twitter,
                    Url = string.Format(ShareConstants.TwitterUrlFormat, title, link)
                };

            if (items.Contains(ReactModels.ShareOptions_Items_Icon.Facebook))
                yield return new ReactModels.ShareOptions_Items
                {
                    Icon = ReactModels.ShareOptions_Items_Icon.Facebook,
                    Text = ShareConstants.Facebook,
                    Url = string.Format(ShareConstants.FacebookUrlFormat, link)
                };

            if (items.Contains(ReactModels.ShareOptions_Items_Icon.Linkedin))
                yield return new ReactModels.ShareOptions_Items
                {
                    Icon = ReactModels.ShareOptions_Items_Icon.Linkedin,
                    Text = ShareConstants.Linkedin,
                    Url = string.Format(ShareConstants.LinkedinUrlFormat, link)
                };

            string GetQueryString(string q) => string.IsNullOrEmpty(q) ? string.Empty : $"?{q}";
            string GetAnchor(string id) => string.IsNullOrEmpty(id) ? string.Empty : $"#{id}";
        }

        private string GetLabel(string key) =>
            _localizationProvider.GetLabel(nameof(ReactModels.OptionsModal), key);
    }
}
