﻿using System.Collections.Generic;
using System.Linq;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.Web.Routing;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Extensions;
using Forskningsradet.Core.Models.ContentModels.Contracts;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.Enums;
using Forskningsradet.Core.Models.SearchModels;
using Forskningsradet.Core.Services;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.Builders
{
    public class PublicationReactModelBuilder : IPublicationReactModelBuilder
    {
        private readonly IContentLoader _contentLoader;
        private readonly IUrlResolver _urlResolver;
        private readonly ILocalizationProvider _localizationProvider;
        private readonly CategoryRepository _categoryRepository;

        public PublicationReactModelBuilder(IContentLoader contentLoader, IUrlResolver urlResolver, ILocalizationProvider localizationProvider, CategoryRepository categoryRepository)
        {
            _contentLoader = contentLoader;
            _urlResolver = urlResolver;
            _localizationProvider = localizationProvider;
            _categoryRepository = categoryRepository;
        }

        public ReactModels.SearchResult BuildReactModel(PublicationBasePage page, PublicationMetadata renderOption, bool showMetadataPdfIcon) =>
            page is PublicationPage publication
                ? MapPageInList(publication, renderOption, showMetadataPdfIcon)
                : page is HistoricProposalPage historicProposal
                    ? MapPageInList(historicProposal)
                    : null;

        public ReactModels.SearchResult BuildReactModel(PublicationBasePage page, PublicationMetadata renderOption, bool showMetadataPdfIcon, string unifiedHitExcerpt) =>
            !string.IsNullOrEmpty(unifiedHitExcerpt) && page is PublicationPage publication
                ? MapPageInList(publication, renderOption, showMetadataPdfIcon, unifiedHitExcerpt)
                : BuildReactModel(page, renderOption, showMetadataPdfIcon);

        private ReactModels.SearchResult MapPageInList(PublicationPage page, PublicationMetadata renderOption, bool showMetadataPdfIcon, string unifiedHitExcerpt = null)
        {
            return new ReactModels.SearchResult
            {
                Title = page.PageName,
                Url = ContentReference.IsNullOrEmpty(page.Attachment) ? null : _urlResolver.GetUrl(page.Attachment),
                Text = renderOption == PublicationMetadata.Small ? null : GetSearchHitText(unifiedHitExcerpt) ?? page.Subtitle,
                Icon = new ReactModels.DocumentIcon
                {
                    IconTheme = ReactModels.DocumentIcon_IconTheme.Pdf
                },
                Metadata = new ReactModels.Metadata
                {
                    Items = page is PublicationPage publication
                        ? BuildMetadataItems(publication, renderOption, showMetadataPdfIcon).ToList()
                        : new List<ReactModels.Metadata_Items>()
                }
            };

            string GetSearchHitText(string x) => string.IsNullOrEmpty(x) ? null : x;
        }

        private ReactModels.SearchResult MapPageInList(HistoricProposalPage page)
        {
            return new ReactModels.SearchResult
            {
                Title = page.PageName,
                Url = !ContentReference.IsNullOrEmpty(page.Attachment)
                    ? _urlResolver.GetUrl(page)
                    : null,
                Icon = new ReactModels.DocumentIcon
                {
                    IconTheme = ReactModels.DocumentIcon_IconTheme.Pdf
                },
                Text = page.Subtitle,
                Metadata = new ReactModels.Metadata
                {
                    Items = BuildMetadataItems(page).ToList()
                }
            };
        }

        private IEnumerable<ReactModels.Metadata_Items> BuildMetadataItems(PublicationPage publication, PublicationMetadata renderOption, bool showMetadataPdfIcon)
        {
            yield return GetType();

            if (renderOption != PublicationMetadata.Small)
            {
                if (publication.NumberOfPages > 0)
                    yield return GetNumberOfPages();
                if (!string.IsNullOrWhiteSpace(publication.Author))
                    yield return GetAuthor();
                if (publication.Year > 0)
                    yield return GetYear();
                if (!string.IsNullOrWhiteSpace(publication.Isbn))
                    yield return GetIsnbn();
                if (publication.Attachment != null)
                    yield return GetAttachment();

                if (renderOption == PublicationMetadata.Large)
                {
                    if (!string.IsNullOrEmpty(publication.PublicationLanguage))
                        yield return GetLanguage();
                    if (!string.IsNullOrEmpty(publication.Publisher))
                        yield return GetPublisher();
                    if (publication.Category?.Count > 0)
                        yield return GetCategory();
                }
            }

            ReactModels.Metadata_Items GetType() =>
                new ReactModels.Metadata_Items
                {
                    Label = GetLabel("Type"),
                    Text = _localizationProvider.GetEnumName(publication.PublicationType),
                    Icon = showMetadataPdfIcon ? BuildPdfDocumentIcon() : null
                };

            ReactModels.Metadata_Items GetNumberOfPages() =>
                new ReactModels.Metadata_Items
                {
                    Label = GetLabel("NumberOfPages"),
                    Text = publication.NumberOfPages.ToString()
                };

            ReactModels.Metadata_Items GetAuthor() =>
                new ReactModels.Metadata_Items
                {
                    Label = GetLabel("Author"),
                    Text = publication.Author
                };

            ReactModels.Metadata_Items GetYear() =>
                new ReactModels.Metadata_Items
                {
                    Label = GetLabel("Year"),
                    Text = publication.Year.ToString()
                };

            ReactModels.Metadata_Items GetIsnbn() =>
                new ReactModels.Metadata_Items
                {
                    Label = GetLabel("Isbn"),
                    Text = publication.Isbn
                };

            ReactModels.Metadata_Items GetAttachment() =>
                new ReactModels.Metadata_Items
                {
                    Label = GetLabel("Document"),
                    Text = GetDocumentFileInfo(publication.Attachment)
                };

            ReactModels.Metadata_Items GetLanguage() =>
                new ReactModels.Metadata_Items
                {
                    Label = GetLabel("Language"),
                    Text = publication.PublicationLanguage
                };

            ReactModels.Metadata_Items GetPublisher() =>
                new ReactModels.Metadata_Items
                {
                    Label = GetLabel("Publisher"),
                    Text = publication.Publisher
                };

            ReactModels.Metadata_Items GetCategory() =>
                new ReactModels.Metadata_Items
                {
                    Label = GetLabel("Category"),
                    Text = string.Join(", ", publication.Category.Select(c => _categoryRepository.Get(c).Description))
                };
        }

        private ReactModels.DocumentIcon BuildPdfDocumentIcon() =>
            new ReactModels.DocumentIcon
            {
                IconTheme = ReactModels.DocumentIcon_IconTheme.Pdf
            };

        private IEnumerable<ReactModels.Metadata_Items> BuildMetadataItems(HistoricProposalPage page)
        {
            if (!string.IsNullOrEmpty(page.ProposalType))
            {
                yield return new ReactModels.Metadata_Items
                {
                    Label = GetLabel("ProposalType"),
                    Text = page.ProposalType
                };
            }

            if (!string.IsNullOrEmpty(page.Program))
            {
                yield return new ReactModels.Metadata_Items
                {
                    Label = GetLabel("Program"),
                    Text = page.Program
                };
            }

            if (page.Year > 0)
            {
                yield return new ReactModels.Metadata_Items
                {
                    Label = GetLabel("ProposalDeadline"),
                    Text = page.Deadline.ToDisplayDate().ToString(DateTimeFormats.TimelineDate)
                };
            }

            if (page.Attachment != null)
            {
                yield return new ReactModels.Metadata_Items
                {
                    Label = GetLabel("Document"),
                    Text = GetDocumentFileInfo(page.Attachment)
                };
            }
        }

        private string GetDocumentFileInfo(ContentReference reference)
        {
            if (reference != null && _contentLoader.TryGet<MediaData>(reference, out var content)
                                  && content is IContentMediaMetaData mediaData)
            {
                return $"{mediaData.FileExtension} ({mediaData.FileSize})";
            }

            return string.Empty;
        }

        private string GetLabel(string key) =>
            _localizationProvider.GetLabel(nameof(PublicationListPage), key);
    }
}