﻿using EPiServer;
using EPiServer.Web.Routing;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Extensions;
using Forskningsradet.Core.Facades.Contracts;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Services.Contracts;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.Builders
{
    public class LinkReactModelBuilder : ILinkReactModelBuilder
    {
        private readonly IUrlResolver _urlResolver;
        private readonly IUrlCheckingService _urlCheckingService;
        private readonly IPageEditingAdapter _pageEditingAdapter;

        public LinkReactModelBuilder(IUrlResolver urlResolver, IUrlCheckingService urlCheckingService, IPageEditingAdapter pageEditingAdapter)
        {
            _urlResolver = urlResolver;
            _urlCheckingService = urlCheckingService;
            _pageEditingAdapter = pageEditingAdapter;
        }

        public ReactModels.Link BuildLink(Url url, string linkText)
        {
            var linkUrl = url.GetUrl(_urlResolver);
            return string.IsNullOrEmpty(linkUrl) || string.IsNullOrEmpty(linkText)
                ? null
                : new ReactModels.Link
                {
                    Url = linkUrl,
                    Text = linkText,
                    IsExternal = url.IsExternal(_urlCheckingService),
                };
        }

        public ReactModels.Link BuildLink(EditLinkBlock editLinkBlock, string propertyName = null)
        {
            var link = editLinkBlock is null
                ? null
                : BuildLink(editLinkBlock.Link, editLinkBlock.Text);
            if (link is null)
                return null;

            link.OnPageEditing = _pageEditingAdapter.PageIsInEditMode() ? propertyName : null;
            link.Id = editLinkBlock.AnchorId.EnsureValidHtmlId();

            return link;
        }
    }
}