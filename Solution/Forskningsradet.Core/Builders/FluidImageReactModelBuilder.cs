﻿using EPiServer;
using EPiServer.Core;
using EPiServer.Web.Routing;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Facades.Contracts;
using Forskningsradet.Core.Models.ContentModels.Media;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.Builders
{
    public class FluidImageReactModelBuilder : IFluidImageReactModelBuilder
    {
        readonly IUrlResolver _urlResolver;
        readonly IContentLoader _contentLoader;
        private readonly IPageEditingAdapter _pageEditingAdapter;

        public FluidImageReactModelBuilder(IUrlResolver urlResolver, IContentLoader contentLoader, IPageEditingAdapter pageEditingAdapter)
        {
            _urlResolver = urlResolver;
            _contentLoader = contentLoader;
            _pageEditingAdapter = pageEditingAdapter;
        }

        public ReactModels.FluidImage BuildReactModel(ImageFile imageFile, string propertyName) =>
            new ReactModels.FluidImage
            {
                Alt = imageFile?.AltText ?? string.Empty,
                Src = imageFile is null ? string.Empty : _urlResolver.GetUrl(imageFile.ContentLink),
                OnPageEditing = _pageEditingAdapter.PageIsInEditMode()
                    ? new ReactModels.FluidImage_OnPageEditing
                    {
                        Image = propertyName
                    }
                    : null,
                InitialSize = FluidImageConstants.DefaultDownFitSize,
                FocusPoint = imageFile?.FocusPoint is null
                    ? null
                    : new ReactModels.FluidImage_FocusPoint
                    {
                        X = (int)imageFile.FocusPoint.X,
                        Y = (int)imageFile.FocusPoint.Y
                    }
            };

        public ReactModels.FluidImage BuildReactModel(ContentReference imageReference, string propertyName) =>
            BuildReactModel(GetImageFile(imageReference), propertyName);

        private ImageFile GetImageFile(ContentReference reference)
        {
            if (reference is null || !_contentLoader.TryGet(reference, out ImageFile file))
                return null;

            return file;
        }
    }
}
