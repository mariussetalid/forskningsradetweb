﻿using System.Collections.Generic;
using System.Linq;
using EPiServer;
using EPiServer.Core;
using EPiServer.Web.Routing;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ContentModels.Media;
using Forskningsradet.Core.Services.Contracts;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.Builders
{
    public class QuotesBlockReactModelBuilder : IQuotesBlockReactModelBuilder
    {
        private readonly IContentLoader _contentLoader;
        private readonly IContentLoadHelper _contentLoadHelper;
        private readonly IUrlResolver _urlResolver;

        public QuotesBlockReactModelBuilder(IContentLoader contentLoader, IContentLoadHelper contentLoadHelper, IUrlResolver urlResolver)
        {
            _contentLoader = contentLoader;
            _contentLoadHelper = contentLoadHelper;
            _urlResolver = urlResolver;
        }

        public ReactModels.QuotesBlock BuildReactModel(NpQuotesBlock block) =>
            new ReactModels.QuotesBlock
            {
                Title = block.Title,
                Quotes = IsNullOrEmpty(block.Quotes) ? null : BuildQuotes(block.Quotes)
            };

        private IList<ReactModels.QuotesBlock_Quotes> BuildQuotes(ContentArea quotes) =>
            _contentLoadHelper.GetFilteredItems<NpImageWithQuoteBlock>(quotes)
                .Select(BuildQuote)
                .ToList();

        private ReactModels.QuotesBlock_Quotes BuildQuote(NpImageWithQuoteBlock block) =>
            new ReactModels.QuotesBlock_Quotes
            {
                Image = BuildImage(block.Image),
                Quotee = block.QuoteBy,
                Text = block.Text,
                AddQuoteDash = block.AddQuoteDash
            };

        private ReactModels.Image BuildImage(ContentReference reference)
        {
            return _contentLoader.TryGet(reference, out ImageFile file) ? BuildImageReactModel(file) : null;

            ReactModels.Image BuildImageReactModel(ImageFile imageFile) =>
                new ReactModels.Image
                {
                    Alt = imageFile.AltText ?? string.Empty,
                    Src = _urlResolver.GetUrl(imageFile.ContentLink),
                };
        }

        private bool IsNullOrEmpty(ContentArea contentArea) =>
            contentArea is null || contentArea.FilteredItems.Any() == false;
    }
}
