﻿using System;
using System.Collections.Generic;
using System.Linq;
using EPiServer;
using EPiServer.Core;
using EPiServer.Web.Routing;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Extensions;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ContentModels.Media;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.QueryParameters;
using Forskningsradet.Core.Repositories.Contracts;
using Forskningsradet.Core.Services;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.Builders
{
    public class DateCardReactModelBuilder : IDateCardReactModelBuilder
    {
        private readonly IUrlResolver _urlResolver;
        private readonly IContentLoader _contentLoader;
        private readonly ILocalizationProvider _localizationProvider;
        private readonly IDateCardDatesReactModelBuilder _dateCardDatesReactModelBuilder;
        private readonly IDateCardTagsReactModelBuilder _dateCardTagsReactModelBuilder;
        private readonly IFluidImageReactModelBuilder _fluidImageReactModelBuilder;
        private readonly IPageRepository _pageRepository;

        public DateCardReactModelBuilder(IUrlResolver urlResolver, IContentLoader contentLoader, ILocalizationProvider localizationProvider, IDateCardDatesReactModelBuilder dateCardDatesReactModelBuilder, IDateCardTagsReactModelBuilder dateCardTagsReactModelBuilder, IFluidImageReactModelBuilder fluidImageReactModelBuilder, IPageRepository pageRepository)
        {
            _urlResolver = urlResolver;
            _contentLoader = contentLoader;
            _localizationProvider = localizationProvider;
            _dateCardDatesReactModelBuilder = dateCardDatesReactModelBuilder;
            _dateCardTagsReactModelBuilder = dateCardTagsReactModelBuilder;
            _fluidImageReactModelBuilder = fluidImageReactModelBuilder;
            _pageRepository = pageRepository;
        }

        public ReactModels.DateCard BuildReactModel(EventPage page, RenderOptionQueryParameter renderOptionQueryParameter = null, bool useText = true, bool portfolioMode = false, bool skipTags = false) =>
            page is null ? null : MapEventInList(page, renderOptionQueryParameter, useText, portfolioMode, skipTags);

        private ReactModels.DateCard MapEventInList(EventPage page, RenderOptionQueryParameter renderOption, bool useText, bool portfolioMode, bool skipTags) =>
            new ReactModels.DateCard
            {
                Id = page.ContentLink.ID.ToString(),
                Title = (page.Canceled ? GetLabel("CanceledEventTitlePrefix") : null) + page.GetListTitle(),
                Subtitle = page.OptionalSubtitle,
                Url = _urlResolver.GetUrl(page.PageLink),
                DateContainer = _dateCardDatesReactModelBuilder.BuildReactModel(page, null, CheckIfPastEvent(page) ? null : BuildEventImage(page)),
                Metadata = BuildMetadata(page, renderOption),
                Text = GetText(page, useText, renderOption?.RenderWidthOption ?? RenderWidthOption.None),
                Tags = portfolioMode || skipTags ? null : BuildTags(page.Category, ListPageConstants.NumberOfVisibleTags),
                Media = BuildMedia(page, portfolioMode),
                UsedInSidebar = renderOption?.RenderWidthOption == RenderWidthOption.IsInSidebar,
            };

        private string GetText(EventPage page, bool useText, RenderWidthOption renderWidthOption) =>
            !useText || renderWidthOption == RenderWidthOption.IsInSidebar
                ? null
                : page.GetListIntroOrMainIntro();

        private static bool CheckIfPastEvent(EventPage page) =>
            page.ComputedEventEndDate?.IsPast() ?? false;

        private ReactModels.Metadata BuildMetadata(EventPage page, RenderOptionQueryParameter renderOption) =>
            new ReactModels.Metadata
            {
                Items = MapMetadataItems(page, renderOption).ToList()
            };

        private IEnumerable<ReactModels.Metadata_Items> MapMetadataItems(EventPage page, RenderOptionQueryParameter renderOption)
        {
            if (page.EventData?.Type is EventType type
                && Enum.IsDefined(typeof(EventType), type)
                && type != EventType.None)
            {
                yield return new ReactModels.Metadata_Items { Label = GetLabel("Type"), Text = _localizationProvider.GetEnumName(type) };
            }

            if (!string.IsNullOrEmpty(page.Duration) && renderOption?.RenderWidthOption != RenderWidthOption.IsInSidebar)
                yield return new ReactModels.Metadata_Items { Label = GetLabel("Duration"), Text = page.Duration };
            if (!string.IsNullOrEmpty(page.Location))
                yield return new ReactModels.Metadata_Items { Label = GetLabel("Location"), Text = page.Location };
        }

        private ReactModels.DateCardTags BuildTags(CategoryList categories, int numberOfVisibleItems) =>
            _dateCardTagsReactModelBuilder.BuildReactModel(categories, numberOfVisibleItems);

        private ReactModels.DateCardMedia BuildMedia(EventPage page, bool onlyCameraIconForDigitalEvents)
        {
            return new ReactModels.DateCardMedia
            {
                Items = onlyCameraIconForDigitalEvents
                    ? BuildOnlyCameraIconForDigitalEvents().ToList()
                    : BuildItems().ToList()
            };

            IEnumerable<ReactModels.DateCardMedia_Items> BuildItems()
            {
                if (page.VideoLink.GetUrl(_urlResolver) is string url && !string.IsNullOrEmpty(url))
                {
                    yield return new ReactModels.DateCardMedia_Items
                    {
                        Url = url,
                        Text = CheckIfPastEvent(page) ? GetLabel("VideoTextPastEvent") : GetLabel("VideoText"),
                        Icon = ReactModels.DateCardMedia_Items_Icon.Video
                    };
                }
                else if (page.EventData?.Type == EventType.Digital)
                {
                    yield return new ReactModels.DateCardMedia_Items
                    {
                        Icon = ReactModels.DateCardMedia_Items_Icon.Camera,
                        Text = page.EventData?.RecordingWillBeAvailable == true ? GetLabel("StreamedAndRecordedEvent") : GetLabel("StreamedEvent")
                    };
                }
            }

            IEnumerable<ReactModels.DateCardMedia_Items> BuildOnlyCameraIconForDigitalEvents()
            {
                if (page.EventData?.Type == EventType.Digital)
                {
                    yield return new ReactModels.DateCardMedia_Items
                    {
                        Icon = ReactModels.DateCardMedia_Items_Icon.Camera,
                    };
                }
            }
        }

        private ReactModels.EventImage BuildEventImage(EventPage eventPage)
        {
            var imageLink = eventPage.Canceled
                ? TryGetCancelledEventImageLink() ?? eventPage.ListImage
                : eventPage.Image;

            return new ReactModels.EventImage
            {
                Image = imageLink is null
                    ? null
                    : _fluidImageReactModelBuilder.BuildReactModel(_contentLoader.Get<ImageFile>(imageLink), null),
                Background = (ReactModels.EventImage_Background)eventPage.EventImageStyle
            };
        }

        private ContentReference TryGetCancelledEventImageLink() =>
            _pageRepository.GetCurrentStartPageAsFrontPageBase()?.CanceledEventImageInList;

        private string GetLabel(string key) =>
            _localizationProvider.GetLabel(nameof(EventListPage), key);
    }
}
