﻿using System;
using System.Collections.Generic;
using System.Linq;
using EPiServer.Web.Routing;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Extensions;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.QueryParameters;
using Forskningsradet.Core.Services;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.Builders
{
    public class ArticleBlockReactModelBuilder : IArticleBlockReactModelBuilder
    {
        private readonly IUrlResolver _urlResolver;
        private readonly ILocalizationProvider _localizationProvider;
        private static string Published => nameof(Published);
        private static string Changed => nameof(Changed);

        public ArticleBlockReactModelBuilder(
            IUrlResolver urlResolver,
            ILocalizationProvider localizationProvider)
        {
            _urlResolver = urlResolver;
            _localizationProvider = localizationProvider;
        }

        public ReactModels.ArticleBlock BuildReactModel(EditorialPage page, RenderWidthOption renderWidthOption) =>
            page is null ? null : MapReactModel(page, renderWidthOption == RenderWidthOption.IsInSidebar);

        private ReactModels.ArticleBlock MapReactModel(EditorialPage page, bool isInSidebar) =>
            new ReactModels.ArticleBlock
            {
                Title = BuildTitleLink(page),
                Text = page.GetListIntroOrMainIntro(),
                Byline = BuildByLine(page),
                UsedInSidebar = isInSidebar
            };

        private ReactModels.Link BuildTitleLink(EditorialPage page) =>
            new ReactModels.Link
            {
                Text = page.GetListTitle(),
                Url = _urlResolver.GetUrl(page.ContentLink)
            };

        private ReactModels.Byline BuildByLine(EditorialPage page) =>
            new ReactModels.Byline
            {
                Items = BuildByLineItems(page).ToList()
            };

        private IEnumerable<ReactModels.Byline_Items> BuildByLineItems(EditorialPage page)
        {
            if (page.StartPublish is null)
            {
                yield return BuildChangedByline(page);
            }
            else if (page is PressReleasePage || page.Changed.Date.Equals(page.StartPublish.Value.Date))
            {
                yield return BuildPublishedByline(page);
            }
            else
            {
                yield return BuildChangedByline(page);
            }
        }

        private ReactModels.Byline_Items BuildChangedByline(EditorialPage page) =>
            BuildBylineDateTime(Changed, page.Changed);

        private ReactModels.Byline_Items BuildPublishedByline(EditorialPage page) =>
            BuildBylineDateTime(Published, page.StartPublish.Value);

        private ReactModels.Byline_Items BuildBylineDateTime(string label, DateTime datetime) =>
            new ReactModels.Byline_Items
            {
                Text = BuildBylineText(label, datetime)
            };

        private string BuildBylineText(string label, DateTime datetime)
        {
            return BuildLocalizedFormatString(label, FormatDateString(datetime));

            string BuildLocalizedFormatString(string key, string parameter) =>
                !string.IsNullOrWhiteSpace(parameter)
                    ? $"{GetLabel(key)} {parameter}"
                    : null;

            string FormatDateString(DateTime dateTime) =>
                dateTime.ToDisplayDate().ToNorwegianDateString();
        }

        private string GetLabel(string key) => _localizationProvider.GetLabel(nameof(ArticlePage), key);
    }
}
