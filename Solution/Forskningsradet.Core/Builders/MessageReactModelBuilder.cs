﻿using Forskningsradet.Core.Facades.Contracts;
using Forskningsradet.Core.Models.Enums;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.Builders.Contracts
{
    public class MessageReactModelBuilder : IMessageReactModelBuilder
    {
        private readonly IPageEditingAdapter _pageEditingAdapter;

        public MessageReactModelBuilder(IPageEditingAdapter pageEditingAdapter)
        {
            _pageEditingAdapter = pageEditingAdapter;
        }

        public ReactModels.Message BuildReactModel(string text, string propertyName, MessageTheme theme = MessageTheme.Yellow) =>
            new ReactModels.Message
            {
                Text = new ReactModels.HtmlString
                {
                    Text = text ?? string.Empty,
                    OnPageEditing = _pageEditingAdapter.PageIsInEditMode() && !(string.IsNullOrEmpty(propertyName))
                        ? new ReactModels.HtmlString_OnPageEditing { Text = propertyName }
                        : null
                },
                Theme = GetTheme(theme)
            };

        private ReactModels.IconWarning_Theme GetTheme(MessageTheme theme)
        {
            switch (theme)
            {
                case MessageTheme.Blue:
                    return ReactModels.IconWarning_Theme.Blue;
                case MessageTheme.Red:
                    return ReactModels.IconWarning_Theme.Red;
                case MessageTheme.Yellow:
                    return ReactModels.IconWarning_Theme.None;
                default:
                    return ReactModels.IconWarning_Theme.None;
            }
        }
    }
}
