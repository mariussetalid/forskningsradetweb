﻿using System.Collections.Generic;
using EPiServer.Core;
using EPiServer.Globalization;
using EPiServer.Web.Routing;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Extensions;
using Forskningsradet.Core.Repositories.Contracts;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.Builders
{
    public class SocialMediaLinkListReactModelBuilder : ISocialMediaLinkListReactModelBuilder
    {
        private readonly IUrlResolver _urlResolver;
        private readonly IPageRepository _pageRepository;

        public SocialMediaLinkListReactModelBuilder(
            IUrlResolver urlResolver,
            IPageRepository pageRepository)
        {
            _urlResolver = urlResolver;
            _pageRepository = pageRepository;
        }

        public ReactModels.SocialMediaLinkList BuildReactModel(PageReference currentPageReference)
        {
            var frontPage = _pageRepository.GetClosestFrontPage(currentPageReference);

            return frontPage != null
                ? MapReactModel(currentPageReference, frontPage.FacebookShareBaseUrl, frontPage.LinkedInShareUrl, frontPage.FacebookAltText, frontPage.LinkedInAltText)
                : null;
        }

        public ReactModels.SocialMediaLinkList BuildReactModel(PageReference currentPageReference, string facebookShareUrl, string linkedInShareUrl, string facebookAltText, string linkedinAltText) =>
            currentPageReference != null
                ? MapReactModel(currentPageReference, facebookShareUrl, linkedInShareUrl, facebookAltText, linkedinAltText)
                : null;

        private ReactModels.SocialMediaLinkList MapReactModel(PageReference currentPageReference, string facebookShareUrl, string linkedInShareUrl, string facebookAltText, string linkedinAltText)
        {
            return new ReactModels.SocialMediaLinkList
            {
                Items = BuildSocialMediaLinkListItems(currentPageReference, facebookShareUrl, linkedInShareUrl, facebookAltText, linkedinAltText)
            };
        }

        private List<ReactModels.SocialMediaLink> BuildSocialMediaLinkListItems(ContentReference currentPageReference, string facebookShareUrl, string linkedInShareUrl, string facebookAltText, string linkedinAltText)
        {
            var urlToCurrentPage = currentPageReference.ContentExternalUrl(ContentLanguage.PreferredCulture, true);
            var items = new List<ReactModels.SocialMediaLink>();
            if (!string.IsNullOrWhiteSpace(facebookShareUrl) && string.Format(facebookShareUrl, urlToCurrentPage) is string facebookUrl)
            {
                var facebookLink = BuildSocialMediaLink(facebookUrl, ReactModels.SocialMediaLink_Provider.Facebook, facebookAltText);
                items.Add(facebookLink);
            }
            if (!string.IsNullOrWhiteSpace(linkedInShareUrl) && string.Format(linkedInShareUrl, urlToCurrentPage) is string linkedInUrl)
            {
                var linkedInLink = BuildSocialMediaLink(linkedInUrl, ReactModels.SocialMediaLink_Provider.Linkedin, linkedinAltText);
                items.Add(linkedInLink);
            }
            return items;
        }

        private static ReactModels.SocialMediaLink BuildSocialMediaLink(string url, ReactModels.SocialMediaLink_Provider provider, string altText) =>
            new ReactModels.SocialMediaLink
            {
                Url = url,
                Provider = provider,
                Text = altText
            };
    }
}
