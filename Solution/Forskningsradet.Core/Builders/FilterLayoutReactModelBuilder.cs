﻿using System;
using System.Collections.Generic;
using System.Linq;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Extensions;
using Forskningsradet.Core.Models.RequestModels;
using Forskningsradet.Core.Models.SearchModels;
using Forskningsradet.Core.Services;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.Builders
{
    public class FilterLayoutReactModelBuilder
    {
        private readonly FilterGroupReactModelBuilder _filterGroupReactModelBuilder;
        private readonly ICategoryLocalizationProvider _categoryLocalizationProvider;
        private readonly ILocalizationProvider _localizationProvider;

        public FilterLayoutReactModelBuilder(FilterGroupReactModelBuilder filterGroupReactModelBuilder, ICategoryLocalizationProvider categoryLocalizationProvider, ILocalizationProvider localizationProvider)
        {
            _filterGroupReactModelBuilder = filterGroupReactModelBuilder;
            _categoryLocalizationProvider = categoryLocalizationProvider;
            _localizationProvider = localizationProvider;
        }

        public ReactModels.FilterLayout BuildReactModel(FilterLayoutModel model) =>
            new ReactModels.FilterLayout
            {
                Filters = BuildFilters(model.Parameters, model.CategoryGroups.ToList(), model.FilterTitle, model.ResultsCount),
                ContentArea = model.ContentArea,
                IsLeft = model.LeftSidePlacement
            };

        private ReactModels.Filters BuildFilters(FilterLayoutModelParameters parameters, List<CategoryGroup> categories, string filterTitle, int resultsCount) =>
            new ReactModels.Filters
            {
                Labels = new ReactModels.Filters_Labels
                {
                    Reset = GetLabel("Reset"),
                    ShowResults = string.Format(GetLabel("ShowResultsFormat"), resultsCount)
                },
                Items = BuildFiltersItems(parameters, categories).ToList(),
                Checkboxes = parameters.ShowVideoCheckbox ? BuildVideoCheckbox(parameters).ToList() : null,
                RangeFilter = parameters.ShowRangeSlider ? BuildSlider(parameters, categories, IsRequestEmpty(parameters)) : null,
                MobileTitle = filterTitle ?? GetLabel("FilterTitle"),
                Title = filterTitle
            };

        private static bool IsRequestEmpty(FilterLayoutModelParameters p) =>
            string.IsNullOrWhiteSpace(p.Query)
            && !(p.Subjects?.Length > 0)
            && !(p.TargetGroups?.Length > 0)
            && !(p.Types?.Length > 0)
            && !(p.Categories?.Length > 0);

        private IEnumerable<ReactModels.FilterGroup> BuildFiltersItems(FilterLayoutModelParameters parameters, List<CategoryGroup> categories)
        {
            foreach (var categoryGroup in categories)
            {
                if (categoryGroup.Type == FilterGroupType.Year || !(categoryGroup.FilterOptions.Any()))
                    continue;

                var title = _localizationProvider.GetEnumName(categoryGroup.Type);
                var (queryParameter, selection) = GetQuerySelection(categoryGroup.Type, parameters);

                if (categoryGroup.Type == FilterGroupType.None)
                {
                    title = _categoryLocalizationProvider.GetCategoryName(categoryGroup.Category.ID);
                    selection = GetSelectionWithinGroup(categoryGroup, parameters.Categories);
                }

                yield return BuildFilterGroup(
                    title,
                    queryParameter,
                    categoryGroup.FilterOptions.OrderFilterOptons(queryParameter, parameters.TimeFrame).ToList(),
                    selection);
            }
        }

        private (string, string[]) GetQuerySelection(FilterGroupType filterGroupType, FilterLayoutModelParameters parameters)
        {
            switch (filterGroupType)
            {
                case FilterGroupType.Subject:
                    return (QueryParameters.Subject, parameters.Subjects ?? new string[0]);
                case FilterGroupType.TargetGroup:
                    return (QueryParameters.TargetGroup, parameters.TargetGroups ?? new string[0]);
                case FilterGroupType.ContentType:
                    return (QueryParameters.ResultType, parameters.Types ?? new string[0]);
                case FilterGroupType.ApplicationType:
                    return (QueryParameters.ApplicationTypes, parameters.ApplicationTypes?.Select(x => x.ToString()).ToArray() ?? new string[0]);
                case FilterGroupType.DeadlineType:
                    return (QueryParameters.DeadlineTypes, parameters.DeadlineTypes ?? new string[0]);
                case FilterGroupType.EventType:
                    return (QueryParameters.EventType, parameters.Types ?? new string[0]);
                case FilterGroupType.Location:
                    return (QueryParameters.Location, parameters.Location ?? new string[0]);
                case FilterGroupType.None:
                    return (QueryParameters.Categories, parameters.Categories);
                case FilterGroupType.Year:
                default:
                    return (QueryParameters.Categories, parameters.Categories);
            }
        }

        private static string[] GetSelectionWithinGroup(CategoryGroup group, string[] selectedCategories) =>
            selectedCategories?.Where(category =>
                    group.FilterOptions.Any(filterOption =>
                        filterOption.Value == category
                        || ((filterOption as NestedFilterOption)?.FilterOptions.Any(nestedFilterOption => nestedFilterOption.Value == category) ?? false)))
                .Select(category => category.ToString())
                .ToArray() ?? new string[0];

        private IEnumerable<ReactModels.Checkbox> BuildVideoCheckbox(FilterLayoutModelParameters parameters)
        {
            yield return new ReactModels.Checkbox
            {
                Label = GetLabel("ShowEventsWithVideoOnly"),
                Name = QueryParameters.Video,
                Value = SearchConstants.EventListFilter.CheckboxVideoValue,
                Checked = parameters.VideoOnly
            };
        }

        private ReactModels.RangeSlider BuildSlider(FilterLayoutModelParameters parameters, IList<CategoryGroup> categories, bool emptyRequest)
        {
            var resultYears = categories.FirstOrDefault(x => x.Type == FilterGroupType.Year)?.FilterOptions ?? new List<FilterOption>();
            if (resultYears.Count <= 0)
                return null;

            var min = GetMinYear();
            var max = GetMaxYear();
            var from = !emptyRequest
                       && parameters.From > 0
                       && parameters.From >= min
                       && parameters.From < parameters.To
                ? parameters.From.ToString()
                : min > 0 ? min.ToString() : string.Empty;
            var to = !emptyRequest
                     && parameters.To > 0
                     && parameters.To > parameters.From
                     && parameters.To <= max
                ? parameters.To.ToString()
                : max > 0 ? max.ToString() : string.Empty;

            return new ReactModels.RangeSlider
            {
                Label = _localizationProvider.GetEnumName(FilterGroupType.Year),
                Min = min,
                Max = max,
                From = new ReactModels.TextInput
                {
                    Name = QueryParameters.YearFrom,
                    Label = GetLabel("FilterYearFrom"),
                    Placeholder = GetLabel("FilterYearPlaceholder"),
                    Value = from
                },
                To = new ReactModels.TextInput
                {
                    Name = QueryParameters.YearTo,
                    Label = GetLabel("FilterYearTo"),
                    Placeholder = GetLabel("FilterYearPlaceholder"),
                    Value = to
                },
                Submit = new ReactModels.Button { Text = GetLabel("SubmitButtonText") }
            };

            int GetMinYear() => resultYears.Count <= 0 ? 0 : resultYears.Min(x => int.Parse(x.Value ?? "0"));
            int GetMaxYear() => resultYears.Count <= 0 ? 0 : resultYears.Max(x => int.Parse(x.Value ?? "0"));
        }

        private ReactModels.FilterGroup BuildFilterGroup(string title, string optionName, List<FilterOption> options, string[] selection) =>
            BuildFilterGroupReactModel(
                BuildAccordion(selection.Any()),
                title,
                optionName,
                options ?? new List<FilterOption>(),
                selection);

        private ReactModels.Accordion BuildAccordion(bool initiallyOpen) =>
            new ReactModels.Accordion
            {
                CollapseLabel = GetLabel("Collapse"),
                ExpandLabel = GetLabel("Expand"),
                Guid = Guid.NewGuid().ToString(),
                InitiallyOpen = initiallyOpen
            };

        private ReactModels.FilterGroup BuildFilterGroupReactModel(ReactModels.Accordion accordion, string title, string optionName, List<FilterOption> options, string[] selection) =>
            _filterGroupReactModelBuilder.BuildReactModel(accordion, title, optionName, options, selection);

        private string GetLabel(string key) => _localizationProvider.GetLabel(nameof(ReactModels.FilterLayout), key);
    }
}
