﻿using EPiServer.Core;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.QueryParameters;
using Forskningsradet.Core.Models.ViewModels.Contracts;
using Forskningsradet.Core.Models.ViewModels.Shared;

namespace Forskningsradet.Core.Factories
{
    public interface IViewModelFactory
    {
        IReactViewModel GetPartialViewModel(IContentData currentContent);
        IReactViewModel GetPartialViewModel(IContentData currentContent, QueryParameterBase parameters);
        IReactViewModel GetPageViewModel(IContentData currentContent);
        IReactViewModel GetPageViewModel(IContentData currentContent, QueryParameterBase parameters);
        LayoutViewModel GetLayoutViewModel(FrontPageBase frontPage, PageReference currentPageReference);
    }
}