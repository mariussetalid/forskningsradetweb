using System;
using Forskningsradet.Core.MessageBrokers;
using Forskningsradet.Core.Models.ApiModels.Evurdering;
using Forskningsradet.Core.Processes;
using Forskningsradet.Core.Processes.Contexts;
using Forskningsradet.Core.Processes.Providers;
using Forskningsradet.Core.Processes.Steps.ApplicationResults;
using Forskningsradet.Core.Processes.Steps.Archiving;
using Forskningsradet.Core.Processes.Steps.PrepareForArchiving;
using Forskningsradet.Core.Processes.Steps.Proposals;
using Forskningsradet.Core.Processes.Steps.UpdatePressReleases;
using Forskningsradet.Core.Repositories.Contracts;
using Forskningsradet.Core.Services;
using Forskningsradet.Core.Services.Contracts;
using Forskningsradet.ServiceAgents.Archiving;
using Forskningsradet.ServiceAgents.Contracts;

namespace Forskningsradet.Core.Factories
{
    public class ProcessFactory : IProcessFactory
    {
        private readonly IPageRepository _pageRepository;
        private readonly IBlockRepository _blockRepository;
        private readonly IProposalValidationService _proposalValidationService;
        private readonly IProposalPageProvider _proposalPageProvider;
        private readonly IPressReleasePageProvider _pressReleasePageProvider;
        private readonly ILocalizationProvider _localizationProvider;
        private readonly IEpiPageVersionProvider _pageVersionProvider;
        private readonly IArchiveMessageBroker _archiveMessageBroker;
        private readonly IHtmlToPdfProvider _htmlToPdfProvider;
        private readonly IArchivingServiceAgent _archivingServiceAgent;
        private readonly INtbServiceAgent _ntbServiceAgent;

        public ProcessFactory(
            IPageRepository pageRepository,
            IBlockRepository blockRepository,
            IProposalValidationService proposalValidationService,
            IProposalPageProvider proposalPageProvider,
            IPressReleasePageProvider pressReleasePageProvider,
            ILocalizationProvider localizationProvider,
            IEpiPageVersionProvider pageVersionProvider,
            IArchiveMessageBroker archiveMessageBroker,
            IHtmlToPdfProvider htmlToPdfProvider,
            IArchivingServiceAgent archivingServiceAgent,
            INtbServiceAgent ntbServiceAgent)
        {
            _pageRepository = pageRepository;
            _blockRepository = blockRepository;
            _proposalValidationService = proposalValidationService;
            _proposalPageProvider = proposalPageProvider;
            _pressReleasePageProvider = pressReleasePageProvider;
            _localizationProvider = localizationProvider;
            _pageVersionProvider = pageVersionProvider;
            _archiveMessageBroker = archiveMessageBroker;
            _htmlToPdfProvider = htmlToPdfProvider;
            _archivingServiceAgent = archivingServiceAgent;
            _ntbServiceAgent = ntbServiceAgent;
        }

        public IProcess GetProcessForContext<T>(T context) where T : Context
        {
            if (typeof(T) == typeof(ProposalContext))
                return CreateProposalProcess(context as ProposalContext) as ProcessRunner<T>;

            if (typeof(T) == typeof(ApplicationResultContext))
                return CreateApplicationResultProcess(context as ApplicationResultContext) as ProcessRunner<T>;

            if (typeof(T) == typeof(PrepareForArchivingContext))
                return CreatePrepareForArchivingProcess(context as PrepareForArchivingContext) as ProcessRunner<T>;

            if (typeof(T) == typeof(ArchivingContext))
                return CreateArchivingProcess(context as ArchivingContext) as ProcessRunner<T>;

            if (typeof(T) == typeof(UpdatePressReleasesContext))
                return CreateUpdatePressReleasesProcess(context as UpdatePressReleasesContext) as ProcessRunner<T>;

            throw new NotSupportedException($"Unable to create process for context of type {typeof(T)}");
        }

        private ProcessRunner<ProposalContext> CreateProposalProcess(ProposalContext context)
        {
            var process = new ProcessRunner<ProposalContext>(context);
            if (ShouldHandleActiveDraft(context))
                process.AddStep(new HandleActiveDraft(_proposalPageProvider, _pageRepository));
            process.AddStep(new UpdateProposalValues(_pageRepository));
            process.AddStep(new UpdateSubjects(_blockRepository));
            process.AddStep(new UpdateAssessmentCriteria(_blockRepository));
            process.AddStep(new UpdateTimeline(_localizationProvider));

            switch (context.Proposal.Status)
            {
                case ProposalState.Planned:
                case ProposalState.Completed:
                    process.AddStep(new HandleInactiveState(_pageRepository));
                    break;
                case ProposalState.Active:
                    process.AddStep(new HandleActiveState(_pageRepository));
                    break;
                case ProposalState.Cancelled:
                case ProposalState.Deleted:
                    process.AddStep(new HandleActiveState(_pageRepository));
                    process.AddStep(new CancelProposal());
                    break;
            }

            process.AddStep(new PublishProposal(_pageRepository, _proposalValidationService));

            return process;
        }

        private bool ShouldHandleActiveDraft(ProposalContext context)
        {
            if (context.Proposal.Status != ProposalState.Completed)
                return false;

            if (context.OriginalPage.ProposalState != ProposalState.Active)
                return false;

            if (_pageRepository.IsPublished(context.OriginalPage))
                return false;

            return true;
        }

        private ProcessRunner<ApplicationResultContext> CreateApplicationResultProcess(ApplicationResultContext context)
        {
            var process = new ProcessRunner<ApplicationResultContext>(context);
            process.AddStep(new PrepareApplicationResultUpdate(_pageRepository));
            process.AddStep(new UpdateApplicationResultPage());
            process.AddStep(new SaveChanges(_pageRepository));
            process.AddStep(new ConnectResultToProposal(_pageRepository));
            return process;
        }

        private ProcessRunner<PrepareForArchivingContext> CreatePrepareForArchivingProcess(PrepareForArchivingContext context) =>
            new ProcessRunner<PrepareForArchivingContext>(context)
            .AddStep(new ValidatePageForArchiving())
            .AddStep(new PopulateMetaData(_pageVersionProvider, _localizationProvider))
            .AddStep(new GenerateArchivedPagePdf(_htmlToPdfProvider))
            .AddStep(new SavePageForArchiving(_archiveMessageBroker));

        private ProcessRunner<ArchivingContext> CreateArchivingProcess(ArchivingContext context) =>
            new ProcessRunner<ArchivingContext>(context)
            .AddStep(new GetPagesPreparedForArchiving(_archiveMessageBroker))
            .AddStep(new DispatchPagesToArchive(_archivingServiceAgent))
            .AddStep(new ResendFailedMessages(_archiveMessageBroker));

        private ProcessRunner<UpdatePressReleasesContext> CreateUpdatePressReleasesProcess(UpdatePressReleasesContext context)
        {
            return new ProcessRunner<UpdatePressReleasesContext>(context)
                .AddStep(new FetchPressReleases(_ntbServiceAgent))
                .AddStep(new CreateOrUpdatePressReleases(_pressReleasePageProvider));
        }
    }
}