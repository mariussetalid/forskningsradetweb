﻿using System;
using EPiServer;
using EPiServer.Core;
using EPiServer.ServiceLocation;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.QueryParameters;
using Forskningsradet.Core.Models.ViewModels.Contracts;
using Forskningsradet.Core.Models.ViewModels.Shared;
using Forskningsradet.Core.ViewModelMappers;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.Factories
{
    public class ViewModelFactory : IViewModelFactory
    {
        public IReactViewModel GetPartialViewModel(IContentData currentContent)
        {
            var mapper = GetMapper<IPartialViewModelMapper>(currentContent);
            return mapper?.GetPartialViewModel(currentContent);
        }

        public IReactViewModel GetPartialViewModel(IContentData currentContent, QueryParameterBase parameters)
        {
            var mapper = GetMapper<IPartialViewModelMapperWithQuery>(currentContent);
            return mapper?.GetPartialViewModel(currentContent, parameters);
        }

        public IReactViewModel GetPageViewModel(IContentData currentContent)
        {
            var mapper = GetMapper<IPageViewModelMapper>(currentContent);
            return mapper?.GetPageViewModel(currentContent);
        }

        public IReactViewModel GetPageViewModel(IContentData currentContent, QueryParameterBase parameters)
        {
            var mapper = GetMapper<IPageViewModelMapperWithQuery>(currentContent);
            return mapper?.GetPageViewModel(currentContent, parameters);
        }

        public LayoutViewModel GetLayoutViewModel(FrontPageBase currentContent, PageReference currentPageReference) =>
            GetMapper<ILayoutViewModelMapper>(currentContent)?.GetViewModel(currentContent, currentPageReference);

        private static T GetMapper<T>(IContentData currentContent)
        {
            try
            {
                return ServiceLocator.Current.GetInstance<T>(currentContent.GetOriginalType().Name);
            }
            catch (Exception)
            {
                return default(T);
            }
        }
    }
}
