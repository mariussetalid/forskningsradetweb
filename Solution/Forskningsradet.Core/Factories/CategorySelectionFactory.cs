using System;
using System.Collections.Generic;
using System.Linq;
using EPiServer.DataAbstraction;
using EPiServer.ServiceLocation;
using EPiServer.Shell.ObjectEditing;
using Forskningsradet.Core.Models.ContentModels.Support;

namespace Forskningsradet.Core.Factories
{
    [SelectionFactoryRegistration]
    public class CategorySelectionFactory : ISelectionFactory
    {
        public IEnumerable<ISelectItem> GetSelections(ExtendedMetadata metadata)
        {
            if (metadata is null)
                return Enumerable.Empty<ISelectItem>();

            var configSelectAttribute = metadata.Attributes.OfType<SingleCategorySelectionAttribute>().SingleOrDefault();
            if (configSelectAttribute is null)
                throw new NotSupportedException("This selection factory is only supported when used with the SingleCategorySelectionAttribute");

            var root = ServiceLocator.Current.GetInstance<CategoryRepository>().GetRoot();

            var categoryList = Enumerable.Empty<Category>();
            if (string.IsNullOrEmpty(configSelectAttribute.Category))
            {
                var topCategories = root.Categories?
                    .Where(x => x.Available)
                    .Where(x => !configSelectAttribute.ExcludeCategories.Contains(x.Name))
                    .ToList() ?? new List<Category>();

                categoryList = topCategories.SelectMany(x => x.Categories).Concat(topCategories.Where(x => x.Selectable));
            }
            else
            {
                categoryList = root.FindChild(configSelectAttribute.Category)?.Categories ?? categoryList;
            }

            var selectItems = new List<ISelectItem>();
            foreach (var category in categoryList.Where(x => x.Available && x.Selectable))
            {
                var catSelectItem = new SelectItem
                {
                    Text = category.LocalizedDescription,
                    Value = category.ID
                };

                selectItems.Add(catSelectItem);
            }
            return selectItems;
        }
    }
}