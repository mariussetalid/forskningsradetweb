﻿using System.Collections.Generic;
using System.Linq;
using EPiServer.DataAbstraction;
using EPiServer.Shell.ObjectEditing;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.ContentModels.Support;

namespace Forskningsradet.Core.Factories
{
    [SelectionFactoryRegistration]
    public class CustomPageTypeSelectionFactory : ISelectionFactory
    {
        private readonly IContentTypeRepository<PageType> _pageTypeRepository;

        public CustomPageTypeSelectionFactory(IContentTypeRepository<PageType> pageTypeRepository) => _pageTypeRepository = pageTypeRepository;

        public IEnumerable<ISelectItem> GetSelections(ExtendedMetadata metadata)
        {
            var pageTypes = _pageTypeRepository.List()
                .Where(t => typeof(EditorialPage).IsAssignableFrom(t.ModelType));

            var specificPageTypeSet = GetPageTypeSet(metadata.Attributes);

            if (specificPageTypeSet.Count > 0)
            {
                pageTypes = pageTypes.Where(x => specificPageTypeSet.Contains(x));
            }

            return pageTypes
                .Select(pageType => (ISelectItem)new SelectItem
                {
                    Text = pageType.LocalizedFullName,
                    Value = pageType.ID
                });
        }

        private List<PageType> GetPageTypeSet(IEnumerable<System.Attribute> attributes)
        {
            var specificPageTypeSet = attributes
                                          .OfType<CustomPageTypeAttribute>()
                                          .FirstOrDefault()
                                          ?.CustomPageTypeSet ?? 0;
            if (specificPageTypeSet == CustomPageTypeSet.Content)
            {
                return new List<PageType>
                {
                    _pageTypeRepository.Load(typeof(ContentAreaPage)),
                    _pageTypeRepository.Load(typeof(InformationArticlePage)),
                    _pageTypeRepository.Load(typeof(NewsArticlePage)),
                    _pageTypeRepository.Load(typeof(PressReleasePage))
                };
            }

            return new List<PageType>();
        }
    }
}
