﻿using System;
using EPiServer.Framework;
using EPiServer.Framework.Initialization;
using EPiServer.ServiceLocation;
using Forskningsradet.Common.Logging;
using Forskningsradet.Core.Builders;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Facades;
using Forskningsradet.Core.Facades.Contracts;
using Forskningsradet.Core.Factories;
using Forskningsradet.Core.MessageBrokers;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Processes.Providers;
using Forskningsradet.Core.Repositories;
using Forskningsradet.Core.Repositories.Contracts;
using Forskningsradet.Core.Services;
using Forskningsradet.Core.Services.Contracts;
using Forskningsradet.Core.ViewModelMappers;
using Forskningsradet.Core.ViewModelMappers.Pages;
using Forskningsradet.ServiceAgents;
using Forskningsradet.ServiceAgents.Archiving;
using Forskningsradet.ServiceAgents.Contracts;
using Forskningsradet.ServiceAgents.DynamicsCrm;
using Forskningsradet.ServiceAgents.MailChimp;

namespace Forskningsradet.Core.Initialization
{
    [InitializableModule]
    public class DependencyResolverConfiguration : IConfigurableModule
    {
        public void ConfigureContainer(ServiceConfigurationContext context)
        {
            // Register services in Forskningsradet.Web like these examples:

            //Register IService1 with implementation Service1, create new instance every time
            //context.Services.AddTransient<IService, Service>();
            //Register IService1 with implementation Service1, re-use a single instance
            //context.Services.AddSingleton<IService, Service>();
            //Register IService1 to be cached per HTTP request or per thread if no HTTP request exist
            //context.Services.AddHttpContextOrThreadScoped<IService, Service>();

            context.Services.AddSingleton<IArchiveMessageBroker, ArchiveMessageBroker>();
            context.Services.AddSingleton<FilterGroupReactModelBuilder, FilterGroupReactModelBuilder>();
            context.Services.AddSingleton<IImageMetadataService, CachedImageMetadataService>();
            context.Services.AddSingleton<IProposalPageStatusHandler, ProposalPageStatusHandler>();
            context.Services.AddSingleton<IAutomaticallyChangeTrackableHandler, AutomaticallyChangeTrackableHandler>();
            context.Services.AddSingleton<IAutomaticallyChangeTrackableMetadataHandler, AutomaticallyChangeTrackableMetadataHandler>();
            context.Services.AddHttpContextOrThreadScoped<FilterLayoutReactModelBuilder, FilterLayoutReactModelBuilder>();
            context.Services.AddHttpContextOrThreadScoped<IAccordionReactModelBuilder, AccordionReactModelBuilder>();
            context.Services.AddHttpContextOrThreadScoped<IAccordionWithContentAreaListReactModelBuilder, AccordionWithContentAreaListReactModelBuilder>();
            context.Services.AddHttpContextOrThreadScoped<IProposalContactReactModelBuilder, ProposalContactReactModelBuilder>();
            context.Services.AddHttpContextOrThreadScoped<IProcessReactModelBuilder, ProcessReactModelBuilder>();
            context.Services.AddHttpContextOrThreadScoped<IMessageReactModelBuilder, MessageReactModelBuilder>();
            context.Services.AddHttpContextOrThreadScoped<IContentAreaReactModelBuilder, ContentAreaReactModelBuilder>();
            context.Services.AddHttpContextOrThreadScoped<IOptionsModalReactModelBuilder, OptionsModalReactModelBuilder>();
            context.Services.AddHttpContextOrThreadScoped<IDownloadListReactModelBuilder, DownloadListReactModelBuilder>();
            context.Services.AddHttpContextOrThreadScoped<IRichTextReactModelBuilder, RichTextReactModelBuilder>();
            context.Services.AddHttpContextOrThreadScoped<IDateCardReactModelBuilder, DateCardReactModelBuilder>();
            context.Services.AddHttpContextOrThreadScoped<IDateCardDatesReactModelBuilder, DateCardDatesReactModelBuilder>();
            context.Services.AddHttpContextOrThreadScoped<IDateCardStatusReactModelBuilder, DateCardStatusReactModelBuilder>();
            context.Services.AddHttpContextOrThreadScoped<IDateCardStatusHandler, DateCardStatusHandler>();
            context.Services.AddHttpContextOrThreadScoped<IDateCardTagsReactModelBuilder, DateCardTagsReactModelBuilder>();
            context.Services.AddHttpContextOrThreadScoped<IFluidImageReactModelBuilder, FluidImageReactModelBuilder>();
            context.Services.AddHttpContextOrThreadScoped<IFooterReactModelBuilder, FooterReactModelBuilder>();
            context.Services.AddHttpContextOrThreadScoped<IPublicationReactModelBuilder, PublicationReactModelBuilder>();
            context.Services.AddHttpContextOrThreadScoped<IArticleHeaderReactModelBuilder, ArticleHeaderReactModelBuilder>();
            context.Services.AddHttpContextOrThreadScoped<ITableOfContentsReactModelBuilder, TableOfContentsReactModelBuilder>();
            context.Services.AddHttpContextOrThreadScoped<ISocialMediaLinkListReactModelBuilder, SocialMediaLinkListReactModelBuilder>();
            context.Services.AddHttpContextOrThreadScoped<IContactInfoReactModelBuilder, ContactInfoReactModelBuilder>();
            context.Services.AddHttpContextOrThreadScoped<IHrManagerSynchronizationService, HrManagerSynchronizationService>();
            context.Services.AddHttpContextOrThreadScoped<ITagLinkListReactModelBuilder, TagLinkListReactModelBuilder>();
            context.Services.AddHttpContextOrThreadScoped<IPaginationReactModelBuilder, PaginationReactModelBuilder>();
            context.Services.AddHttpContextOrThreadScoped<IBreadcrumbsReactModelBuilder, BreadcrumbsReactModelBuilder>();
            context.Services.AddHttpContextOrThreadScoped<IBreadcrumbLinkBuilder<ProposalListPage>, ProposalListPageBreadcrumbLinkBuilder>();
            context.Services.AddHttpContextOrThreadScoped<IBreadcrumbLinkBuilder<EventListPage>, EventListPageBreadcrumbLinkBuilder>();
            context.Services.AddHttpContextOrThreadScoped<ILinkListReactModelBuilder, LinkListReactModelBuilder>();
            context.Services.AddHttpContextOrThreadScoped<ILinkWithTextReactModelBuilder, LinkWithTextReactModelBuilder>();
            context.Services.AddHttpContextOrThreadScoped<IImageReactModelBuilder, ImageReactModelBuilder>();
            context.Services.AddHttpContextOrThreadScoped<ILinkReactModelBuilder, LinkReactModelBuilder>();
            context.Services.AddHttpContextOrThreadScoped<IQuotesBlockReactModelBuilder, QuotesBlockReactModelBuilder>();
            context.Services.AddHttpContextOrThreadScoped<IArticleBlockReactModelBuilder, ArticleBlockReactModelBuilder>();
            context.Services.AddHttpContextOrThreadScoped<ICampaignBlockReactModelBuilder, CampaignBlockReactModelBuilder>();
            context.Services.AddHttpContextOrThreadScoped<IViewModelFactory, ViewModelFactory>();
            context.Services.AddHttpContextOrThreadScoped<ICalendarService, CalendarService>();
            context.Services.AddHttpContextOrThreadScoped<IEventListService, EventListService>();
            context.Services.AddHttpContextOrThreadScoped<ILocalizationProvider, LocalizationProvider>();
            context.Services.AddHttpContextOrThreadScoped<ICategoryLocalizationProvider, LocalizationProvider>();
            context.Services.AddHttpContextOrThreadScoped<ILanguageContext, LanguageContext>();
            context.Services.AddHttpContextOrThreadScoped<IPageStructureService, PageStructureService>();
            context.Services.AddHttpContextOrThreadScoped<IContentFilterService, ContentFilterService>();
            context.Services.AddHttpContextOrThreadScoped<ISearchService, SearchService>();
            context.Services.AddHttpContextOrThreadScoped<ISearchTrackingService,FindSearchTrackingService>();
            context.Services.AddHttpContextOrThreadScoped<IContentListService, ContentListService>();
            context.Services.AddHttpContextOrThreadScoped<ICategoryGroupService, CategoryGroupService>();
            context.Services.AddHttpContextOrThreadScoped<ICategoryFilterService, CategoryFilterService>();
            context.Services.AddHttpContextOrThreadScoped<IGlossaryService, GlossaryService>();
            context.Services.AddHttpContextOrThreadScoped<IEmployeeService, EmployeeService>();
            context.Services.AddHttpContextOrThreadScoped<IProposalService, ProposalService>();
            context.Services.AddHttpContextOrThreadScoped<IHrManagerServiceAgent, HrManagerServiceAgent>();
            context.Services.AddHttpContextOrThreadScoped<IPageRepository, PageRepository>();
            context.Services.AddHttpContextOrThreadScoped<IArchivingServiceAgent, ArchivingServiceAgent>();
            context.Services.AddHttpContextOrThreadScoped<IApiKeyStore, SimpleApiKeyStore>();
            context.Services.AddHttpContextOrThreadScoped<IEpiPageVersionProvider, EpiPageVersionProvider>();
            context.Services.AddHttpContextOrThreadScoped<IHtmlToPdfProvider, EvoPdfHtmlToPdfProvider>();
            context.Services.AddHttpContextOrThreadScoped<IHtmlExtractor, HtmlExtractor>();
            context.Services.AddHttpContextOrThreadScoped<IPageEditingAdapter, PageEditingAdapter>();
            context.Services.AddHttpContextOrThreadScoped<IPressReleasePageProvider, PressReleasePageProvider>();
            context.Services.AddHttpContextOrThreadScoped<IProposalPageProvider, ProposalPageProvider>();
            context.Services.AddHttpContextOrThreadScoped<IProcessFactory, ProcessFactory>();
            context.Services.AddHttpContextOrThreadScoped<IProposalValidationService, ProposalValidationService>();
            context.Services.AddHttpContextOrThreadScoped<IArchiveValidationService, ArchiveValidationService>();
            context.Services.AddHttpContextOrThreadScoped<IBlockRepository, BlockRepository>();
            context.Services.AddHttpContextOrThreadScoped<IPublicationListService, PublicationListService>();
            context.Services.AddHttpContextOrThreadScoped<ILargeDocumentService, LargeDocumentService>();
            context.Services.AddHttpContextOrThreadScoped<IMailChimpService, MailChimpService>();
            context.Services.AddHttpContextOrThreadScoped<IMailChimpServiceAgent, MailChimpServiceAgent>();
            context.Services.AddHttpContextOrThreadScoped<IDynamicsCrmServiceAgent, DynamicsCrmServiceAgent>();
            context.Services.AddHttpContextOrThreadScoped<IPersonPageStringBuilder, PersonPageStringBuilder>();
            context.Services.AddHttpContextOrThreadScoped<IRevisionService, RevisionService>();
            context.Services.AddHttpContextOrThreadScoped<INtbServiceAgent, NtbServiceAgent>();
            context.Services.AddHttpContextOrThreadScoped<IUrlCheckingService, UrlCheckingService>();
            context.Services.AddHttpContextOrThreadScoped<IContentLoadHelper, ContentLoadHelper>();

            context.StructureMap().Configure(x =>
            {
                x.For<IPageViewModelMapperWithQuery>().Add<LargeDocumentPageBaseViewModelMapper>().Named("LargeDocumentPage");
                x.For<IPageViewModelMapperWithQuery>().Add<LargeDocumentPageBaseViewModelMapper>().Named("LargeDocumentChapterPage");
            });

            context.StructureMap().Configure(c =>
            {
                c.Scan(x =>
                {
                    x.TheCallingAssembly();
                    x.IncludeNamespaceContainingType<IPartialViewModelMapper>();
                    x.AddAllTypesOf<IPageViewModelMapper>().NameBy(type => type.Name.Replace("ViewModelMapper", ""));
                    x.AddAllTypesOf<IPageViewModelMapperWithQuery>().NameBy(type => type.Name.Replace("ViewModelMapper", ""));
                    x.AddAllTypesOf<IPartialViewModelMapper>().NameBy(type => type.Name.Replace("ViewModelMapper", ""));
                    x.AddAllTypesOf<IPartialViewModelMapperWithQuery>().NameBy(type => type.Name.Replace("ViewModelMapper", ""));
                    x.AddAllTypesOf<ILayoutViewModelMapper>().NameBy(type => type.Name.Replace("LayoutViewModelMapper", ""));
                });
            });

            context.StructureMap().Configure(c =>
                c.For<IIntegrationLogger>()
                    .Use<IntegrationLogger>()
                    .AlwaysUnique()
                    .Ctor<Type>().Is(ctx =>
                        ctx != null ? ctx.ParentType : null)
                );
        }

        public void Initialize(InitializationEngine context)
        {
        }

        public void Uninitialize(InitializationEngine context)
        {
        }
    }
}