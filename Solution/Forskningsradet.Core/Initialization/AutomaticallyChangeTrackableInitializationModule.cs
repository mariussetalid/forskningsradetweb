﻿using EPiServer;
using EPiServer.Core;
using EPiServer.Framework;
using EPiServer.Framework.Initialization;
using EPiServer.Shell.ObjectEditing;
using Forskningsradet.Core.Models.ContentModels.Contracts;
using Forskningsradet.Core.Services.Contracts;

namespace Forskningsradet.Core.Initialization
{
    [InitializableModule]
    [ModuleDependency(typeof(EPiServer.Cms.Shell.InitializableModule))]
    public class AutomaticallyChangeTrackableInitializationModule : IInitializableModule
    {
        private IAutomaticallyChangeTrackableHandler _automaticallyChangeTrackableHandler;
        private IContentEvents _contentEvents;

        public void Initialize(InitializationEngine context)
        {
            var serviceLocator = context.Locate.Advanced;
            _contentEvents = serviceLocator.GetInstance<IContentEvents>();
            _automaticallyChangeTrackableHandler = serviceLocator.GetInstance<IAutomaticallyChangeTrackableHandler>();

            var metadataHandlerRegistry = serviceLocator.GetInstance<MetadataHandlerRegistry>();
            metadataHandlerRegistry.RegisterMetadataHandler(typeof(ContentData), serviceLocator.GetInstance<IAutomaticallyChangeTrackableMetadataHandler>());

            _contentEvents.PublishingContent += ContentEventsOnPublishingContent;
        }

        private void ContentEventsOnPublishingContent(object sender, ContentEventArgs e)
        {
            if (e.Content is IAutomaticallyChangeTrackable automaticallyChangeTrackable)
                _automaticallyChangeTrackableHandler.OnPublishing(automaticallyChangeTrackable);
        }

        public void Uninitialize(InitializationEngine context) => _contentEvents.PublishingContent -= ContentEventsOnPublishingContent;
    }
}
