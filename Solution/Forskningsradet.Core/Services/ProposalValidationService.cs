using System;
using System.Collections.Generic;
using System.Linq;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.Security;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ApiModels.Evurdering;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.Validation;
using Forskningsradet.Core.Services.Contracts;

namespace Forskningsradet.Core.Services
{
    public class ProposalValidationService : IProposalValidationService
    {
        private readonly CategoryRepository _categoryRepository;

        public ProposalValidationService(CategoryRepository categoryRepository)
            => _categoryRepository = categoryRepository;

        public ValidationResult Validate(ProposalPage page)
        {
            var fieldErrorMessages = ValidateFields(page);
            var permissionsErrorMessages = BuildPermissionsErrorMessage(page);
            var messages = fieldErrorMessages.Union(permissionsErrorMessages);

            return new ValidationResult
            {
                ErrorMessages = messages
            };
        }

        private IEnumerable<string> BuildPermissionsErrorMessage(ProposalPage page)
        {
            if (IsPermissionsNeeded(page) && !IsProposalAdmin())
                yield return "Du må ha tilstrekkelige rettigheter for å publisere";
        }

        private static bool IsPermissionsNeeded(ProposalPage page)
            => page.DeadlineType == DeadlineType.Date
                && page.ProposalState != ProposalState.Planned;

        private static bool IsProposalAdmin()
            => PrincipalAccessor.Current.IsInRole(EpiserverRoleConstants.ProposalAdmin);

        private IEnumerable<string> ValidateFields(ProposalPage page)
        {
            if (string.IsNullOrEmpty(page.PageName))
                yield return "Sidetittel må ha en verdi";

            if (string.IsNullOrEmpty(page.Purpose?.ToString()))
                yield return "Formål må ha en verdi";

            if (string.IsNullOrEmpty(page.About?.ToString()))
                yield return "Om utlysningen må ha en verdi";

            if (string.IsNullOrEmpty(page.IntendedApplicants?.ToString()))
                yield return "Hvem kan søke må ha en verdi";

            if (string.IsNullOrEmpty(page.ProjectParticipants?.ToString()))
                yield return "Hvem kan delta i prosjektet må ha en verdi";

            if (string.IsNullOrEmpty(page.Subject?.ToString()))
                yield return "Hva kan du søke om må ha en verdi";

            if (!Enum.IsDefined(typeof(ProposalState), page.ProposalState))
                yield return "Status må ha en gyldig verdi";

            if (page.ApplicationTypeReference is null)
                yield return "Søknadstype må ha en verdi";

            if (!DeadlineIsValid(page))
                yield return "Søknadsfrist må ha en verdi";

            if (!HasCategoryInGroup(page, CategoryConstants.TargetGroupRoot))
                yield return "En eller flere kategorier for målgruppe må være valgt";
        }

        private bool HasCategoryInGroup(ICategorizable page, string categoryRoot)
        {
            foreach (var categoryId in page.Category)
            {
                var category = _categoryRepository.Get(categoryId);
                if (category.Parent.Name == categoryRoot)
                    return true;
            }

            return false;
        }

        private static bool DeadlineIsValid(ProposalPage page)
            => page.DeadlineType != DeadlineType.Date || page.Deadline.HasValue;
    }
}