using Forskningsradet.Core.Processes;

namespace Forskningsradet.Core.Services.Contracts
{
    public interface IProcessFactory
    {
        IProcess GetProcessForContext<T>(T context) where T : Context;
    }
}