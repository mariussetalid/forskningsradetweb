﻿using System.Threading.Tasks;

namespace Forskningsradet.Core.Services.Contracts
{
    public interface IHrManagerSynchronizationService
    {
        Task<string> Synchronize();
    }
}
