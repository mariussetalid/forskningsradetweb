using EPiServer.Find;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.SearchModels;

namespace Forskningsradet.Core.Services.Contracts
{
    public interface IProposalPageStatusHandler
    {
        TimeFrameFilter? GetStatus(ProposalBasePage proposalPage);

        ITypeSearch<ProposalBasePage> FilterForStatus(ITypeSearch<ProposalBasePage> query,
            TimeFrameFilter statusFilter);
    }
}