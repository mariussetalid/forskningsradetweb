﻿using Forskningsradet.Core.Models.ContentModels.Pages;

namespace Forskningsradet.Core.Services.Contracts
{
    public interface IPersonPageStringBuilder
    {
        string BuildTitle(PersonPage personPage);
        string BuildPhoneUrlScheme(string phoneNumber);
        string BuildEmailUrlScheme(string email);
    }
}
