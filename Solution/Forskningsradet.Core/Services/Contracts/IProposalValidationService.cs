using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.Validation;

namespace Forskningsradet.Core.Services.Contracts
{
    public interface IProposalValidationService
    {
        ValidationResult Validate(ProposalPage page);
    }
}