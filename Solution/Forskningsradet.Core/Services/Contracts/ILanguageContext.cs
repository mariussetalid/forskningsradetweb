﻿using System.Globalization;

namespace Forskningsradet.Core.Services.Contracts
{
    public interface ILanguageContext
    {
        CultureInfo CurrentCulture { get; }
    }
}