using Forskningsradet.Core.Models.ApiModels.Ntb;
using Forskningsradet.Core.Models.ContentModels.Pages;

namespace Forskningsradet.Core.Services.Contracts
{
    public interface IPressReleasePageProvider
    {
        PressReleasePage GetPageByExternalId(long externalId);
        PressReleasePage CreatePage(PressRelease pressRelease);
        PressReleasePage CreatePageVersion(PressReleasePage original);
        void Publish(PressReleasePage original);
    }
}