﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Forskningsradet.ServiceAgents.MailChimp.Models;

namespace Forskningsradet.Core.Services.Contracts
{
    public interface IMailChimpService
    {
        Task<List<Group>> GetGroups(string mailChimpApiKey, string mailChimpListId);
    }
}