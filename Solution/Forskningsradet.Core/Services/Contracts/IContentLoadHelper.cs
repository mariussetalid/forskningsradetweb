﻿using System.Collections.Generic;
using EPiServer.Core;

namespace Forskningsradet.Core.Services.Contracts
{
    public interface IContentLoadHelper
    {
        IEnumerable<T> GetFilteredItems<T>(ContentArea contentArea) where T : class, IContentData;
    }
}