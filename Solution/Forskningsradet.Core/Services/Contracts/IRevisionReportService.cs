﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.RequestModels;

namespace Forskningsradet.Core.Services.Contracts
{
    public interface IRevisionService
    {
        IEnumerable<EditorialPage> GetEditorialPages(RevisionReportRequestModel formModel);
        Task<string> NotifyEditorsWithUnrevisedContent();
    }
}
