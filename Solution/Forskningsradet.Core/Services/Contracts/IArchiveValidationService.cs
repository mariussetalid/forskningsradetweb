using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.Validation;

namespace Forskningsradet.Core.Services.Contracts
{
    public interface IArchiveValidationService
    {
        ValidationResult Validate(EditorialPage page);
    }
}