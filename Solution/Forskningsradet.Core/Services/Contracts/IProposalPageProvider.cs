using Forskningsradet.Core.Models.ApiModels.Evurdering;
using Forskningsradet.Core.Models.ContentModels.Pages;

namespace Forskningsradet.Core.Services.Contracts
{
    public interface IProposalPageProvider
    {
        ProposalPage GetProposalPage(Proposal proposal, string languageBranch);
        ProposalPage CreateProposalPage(Proposal proposal, string languageBranch, string masterLanguageBranch);
        ProposalPage CreateProposalPageVersion(ProposalPage original);
    }
}