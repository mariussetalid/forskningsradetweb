﻿using System.Drawing;
using EPiServer.Core;

namespace Forskningsradet.Core.Services.Contracts
{
    public interface IImageMetadataService
    {
        Size TryGetImageSize(string imageUrl);
    }
}
