﻿using Forskningsradet.Core.Models.RequestModels;
using Forskningsradet.Core.Models.SearchModels;

namespace Forskningsradet.Core.Services.Contracts
{
    public interface IPublicationListService
    {
        PublicationListSearchResults Search(PublicationRequest requestModel);
        PublicationListSearchResults Search(HistoricProposalRequest requestModel);
    }
}