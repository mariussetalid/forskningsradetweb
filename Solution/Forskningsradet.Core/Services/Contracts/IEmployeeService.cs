using System.Collections.Generic;
using Forskningsradet.Core.Models.SearchModels;

namespace Forskningsradet.Core.Services.Contracts
{
    public interface IEmployeeService
    {
        IEnumerable<string> GetAllLetters();
        EmployeeSearchResults Search(string query, char? letter, int currentPage, int pageSize);
    }
}