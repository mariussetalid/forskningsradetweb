﻿using Forskningsradet.Core.Models.ContentModels.Contracts;

namespace Forskningsradet.Core.Services.Contracts
{
    public interface IAutomaticallyChangeTrackableHandler
    {
        void OnPublishing(IAutomaticallyChangeTrackable automaticallyChangeTrackable);
    }
}