﻿using System;
using Forskningsradet.Core.Models.ContentModels.Pages;

namespace Forskningsradet.Core.Services.Contracts
{
    public interface ICalendarService
    {
        string GetCalendarString(DateTime startDate, DateTime endDate, string summary, string location, string description, string url);
        string GetCalendarStringForEvent(EventPage eventPage);
    }
}