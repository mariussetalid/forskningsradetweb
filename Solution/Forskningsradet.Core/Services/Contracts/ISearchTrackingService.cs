﻿using System.Collections.Generic;
using EPiServer.Core;
using Forskningsradet.Core.Models.SearchModels;

namespace Forskningsradet.Core.Services.Contracts
{
    public interface ISearchTrackingService
    {
        SearchHitTrackRequest GetTrackHitRequest(string searchQuery, SearchQueryTrackResult queryTrackResult, IContent searchHitContent, int hitPosition);
        void TrackHit(SearchHitTrackRequest trackRequest);
        SearchQueryTrackResult TrackQuery(string query, int numberOfHits);
    }
}