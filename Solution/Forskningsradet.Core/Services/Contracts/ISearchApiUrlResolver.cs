﻿using Forskningsradet.Core.Models.SearchModels;

namespace Forskningsradet.Core.Services.Contracts
{
    public interface ISearchApiUrlResolver
    {
        string GetSearchUrl();
        string GetSearchProposalUrl();
        string GetSearchEventUrl();
        string GetSearchCategoryUrl();
        string GetSearchPublicationUrl();
        string GetSearchHitTrackUrl(SearchHitTrackRequest trackRequest);
    }
}
