﻿using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.SearchModels;

namespace Forskningsradet.Core.Services.Contracts
{
    public interface IGlossaryService
    {
        GlossarySearchResults GetWordsMatchingQuery(GlossaryPage currentPage, string query);
    }
}