using System.Collections.Generic;
using Forskningsradet.Core.Models.ContentModels.Pages;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.Services.Contracts
{
    public interface IDateCardStatusHandler
    {
        IEnumerable<ReactModels.DateCardStatus> BuildDateCardStatusList(ProposalBasePage proposalPage);
        ReactModels.DateCardStatus GetCurrentStatus(ProposalBasePage proposalPage);
    }
}