﻿namespace Forskningsradet.Core.Services.Contracts
{
    public interface IHtmlExtractor
    {
        string ExtractTextFromFirstDiv(string htmlText);
        string ExtractTextFromFirstParagraph(string htmlText);
    }
}
