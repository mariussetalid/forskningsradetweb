using System.Collections.Generic;
using EPiServer.Core;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.SearchModels;

namespace Forskningsradet.Core.Services.Contracts
{
    public interface IProposalService
    {
        ProposalSearchResult Search(
            ContentReference pageRoot,
            TimeFrameFilter statusFilter,
            string[] subjects,
            string[] targetGroups,
            int[] applicationTypes,
            string[] deadlineTypes);

        IEnumerable<SubjectBlock> GetSortedSubjects(ProposalPage page);
    }
}