﻿namespace Forskningsradet.Core.Services.Contracts
{
    public interface IUrlCheckingService
    {
        bool CheckIsExternal(string url);
    }
}