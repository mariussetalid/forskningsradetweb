using System.Collections.Generic;
using System.Linq;
using EPiServer.Core;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Services.Contracts;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.Services
{
    public class DateCardStatusHandler : IDateCardStatusHandler
    {
        private readonly IDateCardStatusReactModelBuilder _dateCardStatusReactModelBuilder;

        public DateCardStatusHandler(IDateCardStatusReactModelBuilder dateCardStatusReactModelBuilder)
        {
            _dateCardStatusReactModelBuilder = dateCardStatusReactModelBuilder;
        }

        public IEnumerable<ReactModels.DateCardStatus> BuildDateCardStatusList(ProposalBasePage proposalPage)
        {
            if (proposalPage is ProposalPage proposal && ProposalHasApplicationResult(proposal))
                return _dateCardStatusReactModelBuilder.BuildDateCardStatusList(proposalPage.ProposalState, ProposalHasApplicationResult(proposal));

            return new List<ReactModels.DateCardStatus>
            {
                _dateCardStatusReactModelBuilder.BuildReactModel(proposalPage.ProposalState)
            }
            .Where(x => x != null);

        }

        public ReactModels.DateCardStatus GetCurrentStatus(ProposalBasePage proposalPage)
        {
            if (proposalPage is ProposalPage proposal)
                return _dateCardStatusReactModelBuilder.BuildDateCardStatusList(proposalPage.ProposalState, ProposalHasApplicationResult(proposal)).LastOrDefault();

            return _dateCardStatusReactModelBuilder.BuildReactModel(proposalPage.ProposalState);
        }

        private bool ProposalHasApplicationResult(ProposalPage proposalPage) =>
            !ContentReference.IsNullOrEmpty(proposalPage.ApplicationResult);
    }
}