﻿using System.Collections.Generic;
using System.Linq;
using EPiServer;
using EPiServer.Core;
using EPiServer.Filters;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.RequestModels;

namespace Forskningsradet.Core.Services
{
    public interface IPageStructureService
    {
        IEnumerable<T> GetFilteredChildPages<T>(PageReference parentReference, bool filterVisibleInMenu = false) where T : BasePageData;
        IEnumerable<T> GetFilteredAncestors<T>(PageReference rootAncestor, PageReference currentReference) where T : BasePageData;
        string GetMenuPageTitle(MenuStructurePage page);
        Node PopulateTree(ContentReference rootPage, int startDepth);
    }

    public class PageStructureService : IPageStructureService
    {
        private readonly IContentLoader _contentLoader;

        public PageStructureService(IContentLoader contentLoader)
            => _contentLoader = contentLoader;

        public IEnumerable<T> GetFilteredChildPages<T>(PageReference parentReference, bool filterVisibleInMenu = false)
            where T : BasePageData
            => filterVisibleInMenu
                ? GetFilteredChildrenForMenu<T>(parentReference)
                : GetFilteredChildren<T>(parentReference);

        private IEnumerable<T> GetFilteredChildrenForMenu<T>(PageReference parentReference) where T : BasePageData
            => _contentLoader.GetChildren<T>(parentReference)
                .Filter(new FilterAccess())
                .Filter(new FilterPublished())
                .Where(x => x.VisibleInMenu)
                .Cast<T>();

        private IEnumerable<T> GetFilteredChildren<T>(PageReference parentReference) where T : BasePageData
            => _contentLoader.GetChildren<T>(parentReference)
                .Filter(new FilterAccess())
                .Filter(new FilterPublished())
                .Cast<T>();

        public IEnumerable<T> GetFilteredAncestors<T>(PageReference rootAncestor, PageReference currentReference)
            where T : BasePageData
        {
            if (currentReference == rootAncestor)
                return new List<T>();

            return _contentLoader.GetAncestors(currentReference)
                .TakeWhile(x => x.ContentLink != rootAncestor)
                .Select(x => _contentLoader.Get<BasePageData>(x.ContentLink))
                .Filter(new FilterAccess())
                .Filter(new FilterPublished())
                .Where(x => x.VisibleInMenu)
                .OfType<T>()
                .ToList();
        }

        public string GetMenuPageTitle(MenuStructurePage page)
            => page.UseLinkPageName && page.LinkToPage is PageReference linkedPage
                ? _contentLoader.Get<EditorialPage>(linkedPage).PageName
                : page.PageName;

        public Node PopulateTree(ContentReference rootPage, int startDepth) =>
            AddChildren(new Node(null, rootPage, startDepth), startDepth);

        private Node AddChildren(Node parentNode, int depth)
        {
            var childReferences = GetFilteredChildPages<LargeDocumentChapterPage>(new PageReference(parentNode.ContentReference))
                .Select(x => x.ContentLink);

            foreach (var childReference in childReferences)
            {
                var childNode = new Node(parentNode, childReference, depth + 1);
                parentNode.AddChild(AddChildren(childNode, depth + 1));
            }

            return parentNode;
        }
    }
}