﻿using System;
using System.Collections.Generic;
using EPiServer.Core;
using EPiServer.Find;
using EPiServer.Find.Framework.Statistics;
using EPiServer.Find.Statistics;
using Forskningsradet.Core.Models.SearchModels;
using Forskningsradet.Core.Services.Contracts;

namespace Forskningsradet.Core.Services
{
    public class FindSearchTrackingService : ISearchTrackingService
    {
        private readonly IClient _findClient;
        private readonly IStatisticTagsHelper _statisticTagsHelper;

        public FindSearchTrackingService(IClient searchClient, IStatisticTagsHelper statisticTagsHelper)
        {
            _findClient = searchClient;
            _statisticTagsHelper = statisticTagsHelper;
        }

        public SearchHitTrackRequest GetTrackHitRequest(string searchQuery, SearchQueryTrackResult queryTrackResult, IContent searchHitContent, int hitPosition)
        {
            if (string.IsNullOrEmpty(searchQuery)) throw new ArgumentNullException(nameof(searchQuery));
            if (queryTrackResult is null) throw new ArgumentNullException(nameof(queryTrackResult));
            if (string.IsNullOrEmpty(queryTrackResult.TrackId)) throw new ArgumentException("TrackId is not defined", nameof(queryTrackResult));
            if (searchHitContent is null) throw new ArgumentNullException(nameof(searchHitContent));

            return new SearchHitTrackRequest
            {
                QueryTrackId = queryTrackResult.TrackId,
                Query = searchQuery,
                HitId = _findClient.Conventions.TypeNameConvention.GetTypeName(searchHitContent.GetType()) + "/" + _findClient.Conventions.IdConvention.GetId(searchHitContent),
                HitPosition = hitPosition
            };
        }

        public void TrackHit(SearchHitTrackRequest trackRequest)
            => _findClient.Statistics().TrackHit(trackRequest.Query, trackRequest.HitId, x =>
            {
                x.Hit.Id = trackRequest.HitId;
                x.Id = trackRequest.QueryTrackId;
                x.Tags = _statisticTagsHelper.AddDefaultTags(null, false);
                x.Hit.Position = trackRequest.HitPosition;
            });

        public SearchQueryTrackResult TrackQuery(string query, int numberOfHits)
        {
            if (string.IsNullOrEmpty(query))
                return default;

            var trackResult = _findClient.Statistics().TrackQuery(query, x =>
            {
                x.Id = new TrackContext().Id;
                x.Tags = _statisticTagsHelper.AddDefaultTags(null, false);
                x.Query.Hits = numberOfHits;
            });

            return new SearchQueryTrackResult
            {
                TrackId = trackResult.TrackId,
            };
        }
    }
}
