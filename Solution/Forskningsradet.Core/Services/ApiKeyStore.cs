using System.Configuration;

namespace Forskningsradet.Core.Services
{
    public interface IApiKeyStore
    {
        string FindSecretByUsername(string username);
    }

    public class SimpleApiKeyStore : IApiKeyStore
    {
        public string FindSecretByUsername(string username)
            => ConfigurationManager.AppSettings[$"ApiUser.{username}"];
    }
}