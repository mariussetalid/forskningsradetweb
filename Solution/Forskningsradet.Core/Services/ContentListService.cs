﻿using System.Collections.Generic;
using System.Linq;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.Find;
using EPiServer.Find.Api;
using EPiServer.Find.Api.Facets;
using EPiServer.Find.Cms;
using Forskningsradet.Core.Extensions;
using Forskningsradet.Core.Models.ApiModels.Evurdering;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.RequestModels;
using Forskningsradet.Core.Models.SearchModels;

namespace Forskningsradet.Core.Services
{
    public interface IContentListService
    {
        ContentListSearchResults Search<T>(ContentListRequestModel request) where T : EditorialPage;
    }

    public class ContentListService : IContentListService
    {
        private readonly IClient _searchClient;
        private readonly ICategoryGroupService _categoryGroupService;
        private readonly CategoryRepository _categoryRepository;
        private readonly ICategoryFilterService _categoryFilterService;

        public ContentListService(IClient searchClient, ICategoryGroupService categoryGroupService, CategoryRepository categoryRepository, ICategoryFilterService categoryFilterService)
        {
            _searchClient = searchClient;
            _categoryGroupService = categoryGroupService;
            _categoryRepository = categoryRepository;
            _categoryFilterService = categoryFilterService;
        }

        public ContentListSearchResults Search<T>(ContentListRequestModel request) where T : EditorialPage
        {
            var result = SearchForEditorialPages<T>(request);
            return new ContentListSearchResults(
                result,
                BuildAllCategoryGroups(result, request.TopCategories ?? new int[0]),
                result.TotalMatching);
        }

        private PagesResult<T> SearchForEditorialPages<T>(ContentListRequestModel request) where T : EditorialPage
        {
            ITypeSearch<T> searchQuery;

            if (typeof(T) == typeof(ProposalPage))
            {
                searchQuery = (ITypeSearch<T>)_searchClient.Search<ProposalPage>()
                    .CategoriesFacet()
                    .FilterForVisitor()
                    .Filter(x => !x.ProposalState.Match(ProposalState.Completed))
                    .OrderBy(x => x.DeadlineComputed, SortMissing.Last, SortOrder.Ascending, false);
            }
            else if (typeof(T) == typeof(ContentAreaPage) && request.OnlyGetCategoryHomePages)
            {
                searchQuery = (ITypeSearch<T>)_searchClient.Search<ContentAreaPage>()
                    .CategoriesFacet()
                    .FilterForVisitor()
                    .Filter(x => x.IsCategoryHomePage.Match(true));
            }
            else if (typeof(T) == typeof(PressReleasePage))
            {
                searchQuery = (ITypeSearch<T>)_searchClient.Search<PressReleasePage>()
                    .CategoriesFacet()
                    .FilterForVisitor()
                    .OrderBy(x => x.ExternalPublishedAt, SortMissing.Last, SortOrder.Descending, false)
                    .Skip(request.Page <= 0 ? 0 : (request.Page - 1) * request.Take)
                    .Take(request.Take);
            }
            else
            {
                searchQuery = _searchClient.Search<T>()
                    .CategoriesFacet()
                    .FilterForVisitor()
                    .OrderBy(x => x.StartPublish, SortMissing.Last, SortOrder.Descending, false)
                    .Skip(request.Page <= 0 ? 0 : (request.Page - 1) * request.Take)
                    .Take(request.Take);
            }

            if (request.PageRoot > 0)
                searchQuery = searchQuery.Filter(x => x.Ancestors().Match(request.PageRoot.ToString()));

            if (!ContentReference.IsNullOrEmpty(request.CurrentPageLink))
                searchQuery = searchQuery.Filter(x => !x.ContentLink.Match(request.CurrentPageLink));

            if (request.PageCategories?.Length > 0)
            {
                var pageCategoryList = request.PageCategories.Select(x => _categoryRepository.Get(x)).ToList();

                foreach (var filter in BuildCategoryFilterList(pageCategoryList, _categoryFilterService.GetDefaultTopCategories()))
                {
                    searchQuery = searchQuery.Filter(filter);
                }
            }

            if (request.Categories?.Length > 0)
            {
                var categoryList = request.Categories.Select(x => _categoryRepository.Get(x)).ToList();
                var topCategories = request.TopCategories != null && request.TopCategories.Any()
                    ? request.TopCategories.Select(x => _categoryRepository.Get(x)).ToList()
                    : _categoryFilterService.GetDefaultTopCategories();

                foreach (var filter in BuildCategoryFilterList(categoryList, topCategories))
                {
                    searchQuery = searchQuery.FilterHits(filter);
                }
            }

            if (request.ContentTypes?.Length > 0)
                searchQuery = searchQuery.Filter(x => x.ContentTypeID.In(request.ContentTypes));

            if (request.Take > 0)
                searchQuery = searchQuery.Take(request.Take);

            return searchQuery.GetPagesResultSafe();
        }

        private IEnumerable<CategoryGroup> BuildAllCategoryGroups<T>(PagesResult<T> result, params int[] topCategories) where T : EditorialPage
        {
            var categoryFacets = result.Facets["Category"] as HistogramFacet;
            var categories = categoryFacets?.Entries.Select(x => _categoryRepository.Get(x.Key)).ToList() ?? new List<Category>();

            foreach (var topCategory in topCategories.Select(x => _categoryRepository.Get(x)))
            {
                yield return BuildCategoryGroup(FilterGroupType.None, topCategory, categoryFacets, categories);
            }
        }

        private CategoryGroup BuildCategoryGroup(FilterGroupType type, Category topCategory, HistogramFacet categoryFacets, List<Category> categories) =>
            _categoryGroupService.BuildCategoryGroup(type, topCategory, categoryFacets, categories);

        private FilterBuilder<EditorialPage> BuildCategoryFilter(IEnumerable<int> categories)
        {
            var categoryFilter = _searchClient.BuildFilter<EditorialPage>();

            foreach (var category in categories)
            {
                categoryFilter = categoryFilter.Or(x => x.Category.Match(category));
            }

            return categoryFilter;
        }

        private IEnumerable<FilterBuilder<EditorialPage>> BuildCategoryFilterList(IList<Category> categoryList, IList<Category> topCategoryList)
        {
            foreach (var topCategory in topCategoryList)
            {
                var categories = _categoryFilterService.GetCategoriesForTopCategoryInList(categoryList, topCategory);
                if (categories.Any())
                    yield return BuildCategoryFilter(categories);
            }
        }
    }
}
