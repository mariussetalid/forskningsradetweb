﻿using System.Collections.Generic;
using System.Linq;
using EPiServer;
using EPiServer.Core;
using Forskningsradet.Core.Services.Contracts;

namespace Forskningsradet.Core.Services
{
    public class ContentLoadHelper : IContentLoadHelper
    {
        private readonly IContentLoader _contentLoader;

        public ContentLoadHelper(IContentLoader contentLoader) =>
            _contentLoader = contentLoader;

        public IEnumerable<T> GetFilteredItems<T>(ContentArea contentArea) where T : class, IContentData =>
            contentArea?.FilteredItems
                .Select(x => _contentLoader.TryGet(x.ContentLink, out T content) ? content : null)
                .Where(x => x != null)
            ?? Enumerable.Empty<T>();
    }
}
