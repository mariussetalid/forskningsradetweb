using System.Collections.Generic;
using System.Text.RegularExpressions;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.Validation;
using Forskningsradet.Core.Services.Contracts;

namespace Forskningsradet.Core.Services
{
    public class ArchiveValidationService : IArchiveValidationService
    {
        public ValidationResult Validate(EditorialPage page)
        {
            var messages = ValidateFields(page);

            return new ValidationResult
            {
                ErrorMessages = messages
            };
        }

        private static IEnumerable<string> ValidateFields(EditorialPage page)
        {
            var messages = new List<string>();
            if (!page.IsArchivable)
                return messages;

            if (string.IsNullOrEmpty(page.CaseResponsible))
                messages.Add("Saksansvarlig må ha en verdi hvis siden skal arkiveres.");

            if (!IsValidCaseResponsible(page.CaseResponsible))
                messages.Add("Ugyldig saksansvarlig. Saksansvarlig må være en ansatt, og det er bokstavene foran @ i epostadressen som er signaturen.");

            return messages;
        }

        private static bool IsValidCaseResponsible(string caseResponsible)
            => Regex.IsMatch(caseResponsible ?? string.Empty, "^[a-zA-Z0-9_]+$");
    }
}