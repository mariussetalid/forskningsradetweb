﻿using EPiServer;
using Forskningsradet.Core.Services.Contracts;

namespace Forskningsradet.Core.Services
{
    public class UrlCheckingService : IUrlCheckingService
    {
        public bool CheckIsExternal(string url) =>
            !string.IsNullOrEmpty(url) && !UriSupport.IsSiteUrlByRequestOrSettings(url);
    }
}
