﻿using System;
using System.Collections.Generic;
using System.Linq;
using EPiServer.Cms.Shell.UI.ObjectEditing;
using EPiServer.Core;
using EPiServer.Shell.ObjectEditing;
using Forskningsradet.Core.Models.ContentModels.Contracts;
using Forskningsradet.Core.Services.Contracts;

namespace Forskningsradet.Core.Services
{
    public class AutomaticallyChangeTrackableMetadataHandler : IAutomaticallyChangeTrackableMetadataHandler
    {
        private static readonly string SetChangedOnPublishPropertyName = (nameof(IChangeTrackable) + "_" + nameof(IChangeTrackable.SetChangedOnPublish)).ToLower();

        public void ModifyMetadata(ExtendedMetadata metadata, IEnumerable<Attribute> attributes)
        {
            if (!(metadata.Model is IAutomaticallyChangeTrackable))
                return;

            ReplaceSetChangedOnPublishWithDoNotSetChangedOnPublish(metadata);
        }

        private void ReplaceSetChangedOnPublishWithDoNotSetChangedOnPublish(ExtendedMetadata metadata)
        {
            var oldProperty = TryGetContentDataProperty(metadata, SetChangedOnPublishPropertyName);
            var newProperty = TryGetContentDataProperty(metadata, nameof(IAutomaticallyChangeTrackable.DoNotSetChangedOnPublish));

            if (oldProperty is null || newProperty is null)
                return;

            newProperty.Order = oldProperty.Order;
            newProperty.GroupName = oldProperty.GroupName;
            oldProperty.ShowForEdit = false;
            newProperty.ShowForEdit = true;
        }

        private ContentDataMetadata TryGetContentDataProperty(ExtendedMetadata metadata, string propertyName) =>
            metadata.Properties
                    .FirstOrDefault(x => x.PropertyName.Equals(propertyName))
                as ContentDataMetadata;
    }
}