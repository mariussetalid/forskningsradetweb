﻿using System.Collections.Generic;
using System.Linq;
using EPiServer.Core;
using EPiServer.Filters;
using Forskningsradet.Core.Models.ContentModels.Blocks;

namespace Forskningsradet.Core.Services
{
    public interface IContentFilterService
    {
        IEnumerable<T> FilterBlocks<T>(IList<T> contentItems) where T : BaseBlockData;
    }

    public class ContentFilterService : IContentFilterService
    {
        private readonly IPublishedStateAssessor _publishedStateAssessor;

        public ContentFilterService(IPublishedStateAssessor publishedStateAssessor) =>
            _publishedStateAssessor = publishedStateAssessor;

        public IEnumerable<T> FilterBlocks<T>(IList<T> contentItems) where T : BaseBlockData =>
            Filter(contentItems.Cast<IContent>().ToList(), true, true)
                .Cast<T>();

        public IEnumerable<T> Filter<T>(IList<T> contentItems, bool checkPublished, bool checkAccess) where T : IContent
        {
            var list = contentItems.Cast<IContent>().ToList();

            if (checkPublished)
                new FilterPublished(_publishedStateAssessor).Filter(list);

            if (checkAccess)
                new FilterAccess().Filter(list);

            return list.Cast<T>();
        }
    }
}