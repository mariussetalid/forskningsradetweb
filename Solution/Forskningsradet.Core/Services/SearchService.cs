﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.Find;
using EPiServer.Find.Api.Facets;
using EPiServer.Find.Cms;
using EPiServer.Find.Framework.Statistics;
using EPiServer.Find.Statistics;
using EPiServer.Find.UnifiedSearch;
using EPiServer.Web;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Extensions;
using Forskningsradet.Core.Models.ApiModels.Evurdering;
using Forskningsradet.Core.Models.ContentModels.Media;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.RequestModels;
using Forskningsradet.Core.Models.SearchModels;
using Forskningsradet.Core.Repositories.Contracts;

namespace Forskningsradet.Core.Services
{
    public interface ISearchService
    {
        SearchPageResults Search(SearchRequestBase request);
        List<string> GetDidYouMeanSuggestions(string query);
    }

    public class SearchService : ISearchService
    {
        private readonly IClient _searchClient;
        private readonly ICategoryGroupService _categoryGroupService;
        private readonly CategoryRepository _categoryRepository;
        private readonly ILocalizationProvider _localizationProvider;
        private readonly IPageRepository _pageRepository;

        private const double GenericMediaWeight = 0.1;
        private const double ContentAreaWeight = 1.5;
        private const double ProposalWeight = 2;
        private const double ProposalActivePlannedWeight = 16;
        private const double ProposalCompletedWeight = 0.5;
        private const double ProposalTestWeight = -100;

        public SearchService(IClient searchClient, ICategoryGroupService categoryGroupService, CategoryRepository categoryRepository, ILocalizationProvider localizationProvider, IPageRepository pageRepository)
        {
            _searchClient = searchClient;
            _categoryGroupService = categoryGroupService;
            _categoryRepository = categoryRepository;
            _localizationProvider = localizationProvider;
            _pageRepository = pageRepository;
        }

        public SearchPageResults Search(SearchRequestBase request)
        {
            var language = CultureInfo.CurrentUICulture.Name == LanguageConstants.NorwegianLanguageBranch
                ? Language.Norwegian
                : Language.English;

            var query = _searchClient.UnifiedSearch(language)
                .For(request.Query.RemoveNorwegianStopwords())
                .UsingSynonyms()
                .UsingUnifiedWeights()
                .BoostMatching(x => x.MatchTypeHierarchy(typeof(GenericMedia)), GenericMediaWeight)
                .BoostMatching(x => x.MatchType(typeof(ContentAreaPage)), ContentAreaWeight)
                .Apply(BoostProposals, BoostTestProposals)
                .ApplyBestBets()
                .HistogramFacetFor(x => x.SearchPublishDate, DateInterval.Year)
                .TermsFacetFor(x => x.SearchCategories, action => action.Size = 100)
                .TermsFacetFor(x => x.SearchTypeName, action => action.Size = 100)
                .TermsFacetFor(x => x.SearchSection, action => action.Size = 100)
                .Skip(Math.Max(0, request.Page - 1) * request.PageSize)
                .Take(request.PageSize > 0 ? request.PageSize : SearchConstants.MaxPageSize);

            if (!request.SkipTracking)
            {
                query = query.Track();
            }

            var publicationAttachmentIds = SearchForPublicationAttachmentIds();
            if (publicationAttachmentIds.Any())
            {
                var publicationFilter = _searchClient.BuildFilter<GenericMedia>();
                foreach (var publicationAttachment in publicationAttachmentIds)
                {
                    publicationFilter = publicationFilter.And(x => !x.ContentLink.Match(publicationAttachment));
                }
                query = query.Filter(publicationFilter);
            }

            if (request.PageRoot > 0)
                query = MatchPageRootAndExcludeGlobalAssetsRoot(request, query);

            if (request is CustomCategoriesSearchRequest categoryRequest)
            {
                if (categoryRequest.Categories?.Length > 0)
                {
                    var categoryGroups = GetCategoryGroups(categoryRequest.Categories, categoryRequest.TopCategories);
                    foreach (var categoryList in categoryGroups)
                    {
                        query = query.FilterHits(x => x.SearchCategories.In(categoryList));
                    }
                }
            }
            else if (request is SearchRequest searchRequest)
            {
                if (searchRequest.Subjects?.Length > 0)
                    query = query.FilterHits(x => x.SearchCategories.In(searchRequest.Subjects));

                if (searchRequest.TargetGroups?.Length > 0)
                    query = query.FilterHits(x => x.SearchCategories.In(searchRequest.TargetGroups));
            }
            else if (request is PublicationSearchRequest publicationSearchRequest)
            {
                if (publicationSearchRequest.PublicationTypes?.Length > 0)
                    query = query.FilterHits(BuildPublicationTypeFilter(publicationSearchRequest.PublicationTypes));

                if (publicationSearchRequest.Year > 0)
                    query = query.FilterHits(BuildPublicationYearFilter(publicationSearchRequest.Year));

                query = query.OrderByDescending(x => ((PublicationBasePage)x).Year);
            }

            if (request.Types?.Length > 0)
                query = query.FilterHits(x => x.SearchTypeName.In(request.Types));

            if (request.From > 0 && request.To >= request.From)
            {
                var yearFilter = BuildYearFilter(request.From, request.To);
                query = query.FilterHits(yearFilter);
            }

            var result = query.GetResultSafe(new HitSpecification
            {
                HighlightExcerpt = true,
                HighlightTitle = true,
                EncodeTitle = false,
                EncodeExcerpt = false,
                PreTagForAllHighlights = SearchConstants.PreTagForHighlights,
                PostTagForAllHighlights = SearchConstants.PostTagForHighlights
            });

            var filterOptions = Enumerable.Empty<CategoryGroup>();
            if (request is CustomCategoriesSearchRequest customRequest)
            {
                filterOptions = BuildAllCategoryGroups(result, true, customRequest.TopCategories);
            }
            else if (request is PublicationSearchRequest)
            {
                filterOptions = BuildPublicationCategoryGroup(result);
            }
            else
            {
                filterOptions = BuildAllCategoryGroups(result, false);
            }

            return new SearchPageResults(
                result.ToList(),
                filterOptions,
                result.TotalMatching);
        }

        public List<string> GetDidYouMeanSuggestions(string query)
        {
            if (string.IsNullOrEmpty(query))
                return Enumerable.Empty<string>().ToList();
            var didYouMeanResult = _searchClient.Statistics().GetDidYouMean(query);
            return didYouMeanResult.Hits.Select(x => x.Suggestion).ToList();
        }

        private ITypeSearch<ISearchContent> BoostProposals(ITypeSearch<ISearchContent> query) =>
            query
                .BoostMatching(
                    x => x.MatchType(typeof(ProposalPage)), 
                    ProposalWeight)
                .BoostMatching(
                    x => x.MatchTypeHierarchy(typeof(ProposalPage)) 
                         & (((ProposalPage)x).ProposalState.Match(ProposalState.Active) 
                            | ((ProposalPage)x).ProposalState.Match(ProposalState.Planned)), 
                    ProposalActivePlannedWeight)
                .BoostMatching(
                    x => x.MatchTypeHierarchy(typeof(ProposalPage)) 
                         & ((ProposalPage)x).ProposalState.Match(ProposalState.Completed), 
                    ProposalCompletedWeight);

        private ITypeSearch<ISearchContent> BoostTestProposals(ITypeSearch<ISearchContent> query)
        {
            var testProposalsRootLink = _pageRepository.GetCurrentStartPage()?.ProposalsTestPage;
            if (testProposalsRootLink != null)
                query = query.BoostMatching(
                    x =>
                        x.MatchTypeHierarchy(typeof(ProposalPage))
                        & ((ProposalPage)x).Ancestors()
                        .Match(testProposalsRootLink.ToReferenceWithoutVersion().ToString()),
                    ProposalTestWeight);

            query = query.BoostMatching(
                x =>
                    x.MatchTypeHierarchy(typeof(ProposalPage))
                    & ((ProposalPage)x).Activity.PrefixCaseInsensitive("Test"),
                ProposalTestWeight);

            return query;
        }

        private IEnumerable<ContentReference> SearchForPublicationAttachmentIds() =>
            GetAllPublicationPagesWithAttachment()
            .Select(x => x.Attachment);

        private IEnumerable<PublicationPage> GetAllPublicationPagesWithAttachment()
        {
            int totalNumberOfPages;

            var query = _searchClient.Search<PublicationPage>()
                .Filter(p => p.HasAttachment.Match(true))
                .Take(1000);

            var batch = query.GetContentResultSafe();
            foreach (var page in batch)
            {
                yield return page;
            }

            totalNumberOfPages = batch.TotalMatching;

            var nextBatchFrom = 1000;
            while (nextBatchFrom < totalNumberOfPages)
            {
                batch = query.Skip(nextBatchFrom).GetContentResultSafe();
                foreach (var page in batch)
                {
                    yield return page;
                }
                nextBatchFrom += 1000;
            }
        }

        private static ITypeSearch<ISearchContent> MatchPageRootAndExcludeGlobalAssetsRoot(SearchRequestBase request, ITypeSearch<ISearchContent> query) =>
            query.Filter(x =>
                (x.MatchTypeHierarchy(typeof(PageData)) & ((PageData)x).Ancestors().Match(request.PageRoot.ToString()))
                |
                (x.MatchTypeHierarchy(typeof(MediaData)) & !((MediaData)x).Ancestors()
                     .Match(SiteDefinition.Current.GlobalAssetsRoot.ToReferenceWithoutVersion().ID.ToString())));

        private IEnumerable<CategoryGroup> BuildPublicationCategoryGroup(UnifiedSearchResults result)
        {
            yield return new CategoryGroup(FilterGroupType.PublicationType, BuildPublicationTypeOptions(result).ToList());
        }

        private IEnumerable<CategoryGroup> BuildAllCategoryGroups(UnifiedSearchResults result, bool mapCustomCategories, params int[] topCategories)
        {
            var categoryFacets = result.Facets["SearchCategories"] as TermsFacet;
            var categories = categoryFacets?.Terms.Select(x => _categoryRepository.Get(x.Term)).ToList() ?? new List<Category>();

            if (mapCustomCategories)
            {
                foreach (var topCategory in topCategories.Select(x => _categoryRepository.Get(x)))
                {
                    yield return BuildCategoryGroup(FilterGroupType.None, topCategory, categoryFacets, categories);
                }
            }
            else
            {
                yield return BuildCategoryGroup(FilterGroupType.Subject, _categoryRepository.Get(CategoryConstants.SubjectRoot), categoryFacets, categories);
                yield return BuildCategoryGroup(FilterGroupType.TargetGroup, _categoryRepository.Get(CategoryConstants.TargetGroupRoot), categoryFacets, categories);
                yield return new CategoryGroup(FilterGroupType.ContentType, BuildTypeOptions(result).ToList());
            }
            yield return new CategoryGroup(FilterGroupType.Year, BuildYearOptions(result).ToList());
        }

        private CategoryGroup BuildCategoryGroup(FilterGroupType type, Category topCategory, TermsFacet categoryFacets, List<Category> categories) =>
            _categoryGroupService.BuildCategoryGroup(type, topCategory, categoryFacets, categories);

        private IEnumerable<FilterOption> BuildTypeOptions(UnifiedSearchResults result) =>
            result.TermsFacetFor(x => x.SearchTypeName) is TermsFacet typeFacet
                ? typeFacet
                    .Select(t => new FilterOption
                    {
                        Name = Enum.TryParse(t.Term, out SearchResultType typeValue)
                            ? _localizationProvider.GetEnumName(typeValue)
                            : null,
                        Value = ((int)typeValue).ToString(),
                        Count = t.Count
                    })
                    .Where(x => x.Name != null)
                    .OrderBy(t => t.Name)
                : Enumerable.Empty<FilterOption>();

        private IEnumerable<FilterOption> BuildPublicationTypeOptions(UnifiedSearchResults result) =>
            result.TermsFacetFor(x => x.SearchSection) is TermsFacet typeFacet
                ? typeFacet
                    .Select(t => new FilterOption
                    {
                        Name = Enum.TryParse(t.Term, out PublicationType typeValue)
                            ? _localizationProvider.GetEnumName(typeValue)
                            : null,
                        Value = ((int)typeValue).ToString(),
                        Count = t.Count
                    })
                    .Where(x => !(x.Name is null))
                    .OrderBy(t => t.Name)
                : Enumerable.Empty<FilterOption>();

        private static IEnumerable<FilterOption> BuildYearOptions(UnifiedSearchResults result) =>
            result.HistogramFacetFor(x => x.SearchPublishDate)
                ?.Entries
                ?.Select(t => new FilterOption
                {
                    Name = t.Key.ToDisplayDate().Year.ToString(),
                    Value = t.Key.Year.ToString(),
                    Count = t.Count
                })
            ?? Enumerable.Empty<FilterOption>();

        private FilterBuilder<EditorialPage> BuildYearFilter(int yearFrom, int yearTo) =>
            _searchClient.BuildFilter<EditorialPage>()
                .Or(x =>
                    x.StartPublish.InRange(
                        new DateTime(yearFrom, 1, 1),
                        new DateTime(yearTo, 1, 1).AddYears(1),
                        true,
                        false));

        private FilterBuilder<PublicationBasePage> BuildPublicationYearFilter(int year) =>
            _searchClient.BuildFilter<PublicationBasePage>()
                .Or(x => x.Year.Match(year));

        private FilterBuilder<PublicationBasePage> BuildPublicationTypeFilter(IEnumerable<int> types)
        {
            var filter = _searchClient.BuildFilter<PublicationBasePage>();

            foreach (var type in types.Where(x => Enum.IsDefined(typeof(PublicationType), x)))
            {
                filter = filter.Or(x => x.SearchSection.Match(Enum.GetName(typeof(PublicationType), type)));
            }

            return filter;
        }

        private IEnumerable<string[]> GetCategoryGroups(IEnumerable<string> categories, IEnumerable<int> topCategories)
        {
            var categoryList = categories.Select(x => _categoryRepository.Get(x)).ToList();
            var topCategoryList = topCategories.Select(x => _categoryRepository.Get(x));

            foreach (var topCategory in topCategoryList)
            {
                var descendants = categoryList.Where(x => x.Parent == topCategory)
                    .Union(categoryList.Where(x => x.Parent?.Parent == topCategory))
                    .ToList();

                if (descendants.Any())
                    yield return descendants.Select(x => x.Name).ToArray();
            }
        }
    }
}