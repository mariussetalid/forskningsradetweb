﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Forskningsradet.Core.Services.Contracts;
using Forskningsradet.ServiceAgents.Contracts;
using Forskningsradet.ServiceAgents.MailChimp.Models;
using Caching = System.Web.Caching;
using HttpContext = System.Web.HttpContext;

namespace Forskningsradet.Core.Services
{
    public class MailChimpService : IMailChimpService
    {
        private readonly IMailChimpServiceAgent _agent;

        public MailChimpService(IMailChimpServiceAgent serviceAgent) => _agent = serviceAgent;

        public async Task<List<Group>> GetGroups(string mailChimpApiKey, string mailChimpListId)
        {
            if (mailChimpApiKey is null || mailChimpListId is null)
                throw new ArgumentException("Mailchimp settings cannot be null.");

            if (HttpContext.Current is null)
                return await _agent.GetGroups(mailChimpApiKey, mailChimpListId);
            var cacheKey = $"MailChimpService_GetGroups_{mailChimpListId}";
            var result = HttpContext.Current.Cache[cacheKey] as List<Group>;
            if (result is null)
            {
                result = await _agent.GetGroups(mailChimpApiKey, mailChimpListId);
                HttpContext.Current.Cache.Insert(cacheKey, result, null, DateTime.UtcNow.AddMinutes(10), Caching.Cache.NoSlidingExpiration);
            }
            return result;
        }
    }
}