using System;
using System.Collections.Generic;
using System.Linq;
using EPiServer.Find;
using EPiServer.Find.Api;
using EPiServer.Find.Api.Facets;
using EPiServer.Find.Cms;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Configuration;
using Forskningsradet.Core.Extensions;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.SearchModels;
using Forskningsradet.Core.Services.Contracts;

namespace Forskningsradet.Core.Services
{
    public class EmployeeService : IEmployeeService
    {
        private readonly IClient _client;
        private readonly ICacheConfiguration _cacheConfiguration;

        public EmployeeService(IClient client, ICacheConfiguration cacheConfiguration)
        {
            _client = client;
            _cacheConfiguration = cacheConfiguration;
        }

        public IEnumerable<string> GetAllLetters()
        {
            var results = _client.Search<PersonPage>()
                .TermsFacetFor(x => x.Letter, action => action.Size = 100)
                .FilterForVisitor()
                .Filter(x => x.ResourceType.Match(SearchConstants.EmployeeSearch.EmployedResourceType))
                .StaticallyCacheFor(TimeSpan.FromDays(_cacheConfiguration.CacheEmployeeLettersInDays ?? 0))
                .Take(0)
                .Select(x => x.Letter)
                .GetResultSafe();

            return GetSortedLetters(results.Facets);
        }

        public EmployeeSearchResults Search(string query, char? letter, int currentPage, int pageSize)
        {
            query = query?.Trim();

            var searchQuery = _client.Search<PersonPage>()
                .For(query, action => { action.Query = $"*{query}*"; })
                .InFields(
                    x => x.FirstName,
                    x => x.LastName,
                    x => x.JobTitle,
                    x => x.Department,
                    x => x.Email)
                .TermsFacetFor(x => x.Letter, action => action.Size = 100)
                .FilterForVisitor()
                .Filter(x => x.ResourceType.Match(SearchConstants.EmployeeSearch.EmployedResourceType))
                .Skip((currentPage - 1) * pageSize)
                .Take(pageSize)
                .OrderBy(x => x.LastName, SortMissing.Last, SortOrder.Ascending, false)
                .ThenBy(x => x.FirstName, SortMissing.Last, SortOrder.Ascending, false);

            if (letter.HasValue)
                searchQuery = searchQuery.Filter(x => x.Letter.Match(letter.ToString()));

            var results = searchQuery
                .Select(x => new EmployeeSearchHit
                {
                    Name = x.Name,
                    JobTitle = x.JobTitle,
                    Department = x.Department,
                    Email = x.Email,
                    Letter = x.Letter
                })
                .GetResultSafe();

            return new EmployeeSearchResults(results, GetSortedLetters(results.Facets), results.TotalMatching);
        }

        private static IEnumerable<string> GetSortedLetters(FacetResults results)
        {
            if (!(results["Letter"] is TermsFacet letterFacet))
                yield break;

            foreach (var term in letterFacet.Terms.OrderBy(l => l.Term))
            {
                yield return term.Term;
            }
        }
    }
}