﻿using Forskningsradet.Core.Models.ContentModels.Contracts;
using Forskningsradet.Core.Services.Contracts;

namespace Forskningsradet.Core.Services
{
    public class AutomaticallyChangeTrackableHandler : IAutomaticallyChangeTrackableHandler
    {

        public void OnPublishing(IAutomaticallyChangeTrackable automaticallyChangeTrackable)
        {
            automaticallyChangeTrackable.SetChangedOnPublish = !automaticallyChangeTrackable.DoNotSetChangedOnPublish;
            automaticallyChangeTrackable.DoNotSetChangedOnPublish = false;
        }
    }
}