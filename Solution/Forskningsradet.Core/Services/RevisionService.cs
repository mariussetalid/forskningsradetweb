﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EPiServer.Editor;
using EPiServer.Find;
using EPiServer.Find.Cms;
using EPiServer.Find.Framework;
using EPiServer.Notification;
using EPiServer.ServiceLocation;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.RequestModels;
using Forskningsradet.Core.Services.Contracts;

namespace Forskningsradet.Core.Services
{
    public class RevisionService : IRevisionService
    {
        public IEnumerable<EditorialPage> GetEditorialPages(RevisionReportRequestModel formModel)
        {
            var pages = GetPages();

            if (formModel.DateStart != null)
                pages = pages.Where(p => p.RevisionDate > formModel.DateStart);
            if (formModel.DateEnd != null)
                pages = pages.Where(p => p.RevisionDate < formModel.DateEnd);
            if (!string.IsNullOrWhiteSpace(formModel.PageType))
                pages = pages.Where(p => p.PageTypeName.ToLowerInvariant().Contains(formModel.PageType.ToLowerInvariant()));

            if (formModel.ShowNorwegianPages && formModel.ShowEnglishPages)
                pages = pages.Where(p => p.Language.Name == LanguageConstants.NorwegianLanguageBranch || p.Language.Name == LanguageConstants.EnglishLanguageBranch);
            else if (formModel.ShowEnglishPages)
                pages = pages.Where(p => p.Language.Name == LanguageConstants.EnglishLanguageBranch);
            else if (formModel.ShowNorwegianPages)
                pages = pages.Where(p => p.Language.Name == LanguageConstants.NorwegianLanguageBranch);

            pages = pages.OrderBy(p => p.RevisionDate);

            return pages;
        }

        public async Task<string> NotifyEditorsWithUnrevisedContent()
        {
            var notifier = ServiceLocator.Current.GetInstance<INotifier>();
            var pages = GetPages().Where(p => ShouldPostNotification(p));
            var count = 0;
            foreach (var page in pages)
            {
                await SendMessage(notifier, page);
                count++;
            }
            return $"Complete, sent {count} notifications";
        }

        private static IEnumerable<EditorialPage> GetPages() =>
            SearchClient.Instance.Search<EditorialPage>()
                            .Filter(x => x.RevisionDate.After(new DateTime(2000, 1, 1)))
                            .Take(SearchConstants.MaxPageSize)
                            .GetContentResult()
                            .Items;

        private bool ShouldPostNotification(EditorialPage page)
        {
            if (page is NewsArticlePage || page is EventPage || page is ProposalPage)
                return false;
            var dateChecks = new List<DateTime>() { page.RevisionDate.AddMonths(-1).Date, page.RevisionDate.AddDays(-7).Date, page.RevisionDate.Date };
            return dateChecks.Contains(DateTime.Today);
        }

        private async Task SendMessage(INotifier notifier, EditorialPage page)
        {
            var message = GetNotificationMessage(page);
            await notifier.PostNotificationAsync(message);
        }

        private NotificationMessage GetNotificationMessage(EditorialPage page) =>
            new NotificationMessage()
            {
                ChannelName = "epi.Revision",
                Content = GetNotificationContent(page),
                Subject = "Revisjon av side",
                Recipients = new[] { new NotificationUser(page.ChangedBy) },
                Sender = new NotificationUser("Revision Bot"),
                TypeName = "RevisionNeeded"
            };

        private static string GetNotificationContent(EditorialPage page) =>
            $"<a href=\"{ PageEditing.GetEditUrl(page.ContentLink)}\">{page.Name} {page.ContentLink} " +
            $"må revideres {(page.RevisionDate.Date == DateTime.Today ? "i dag" : $"innen {page.RevisionDate.ToShortDateString()}")}</a>";
    }
}