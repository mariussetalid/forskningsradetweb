﻿using System;
using System.Collections.Generic;
using System.Linq;
using EPiServer.Core;
using EPiServer.Find;
using EPiServer.Find.Api;
using EPiServer.Find.Api.Facets;
using EPiServer.Find.Cms;
using EPiServer.Find.Framework.Statistics;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Extensions;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.RequestModels;
using Forskningsradet.Core.Models.SearchModels;
using Forskningsradet.Core.Services.Contracts;

namespace Forskningsradet.Core.Services
{
    public class PublicationListService : IPublicationListService
    {
        private readonly IClient _searchClient;
        private readonly ILocalizationProvider _localizationProvider;
        private readonly ISearchService _searchService;

        public PublicationListService(
            IClient searchClient,
            ILocalizationProvider localizationProvider,
            ISearchService searchService)
        {
            _searchClient = searchClient;
            _localizationProvider = localizationProvider;
            _searchService = searchService;
        }

        public PublicationListSearchResults Search(PublicationRequest requestModel)
        {
            var allYears = GetAllYears(requestModel);
            if (requestModel.IsEmptyRequest)
            {
                var yearEntries = GetYearFacet(allYears).ToList();
                var defaultYear = yearEntries.Any()
                    ? yearEntries.Max(x => x.Key)
                    : requestModel.Year;

                requestModel = new PublicationRequest(
                    requestModel.Query,
                    requestModel.PageRoot,
                    defaultYear,
                    requestModel.Types);
            }

            return !string.IsNullOrWhiteSpace(requestModel.Query) && requestModel.UseGlobalSearchWithQuery
                ? GetFromGlobalSearch(requestModel, allYears)
                : GetFromTypedSearch(requestModel, allYears);
        }

        public PublicationListSearchResults GetFromGlobalSearch(PublicationRequest requestModel, SearchResults<int> allYears)
        {
            var result = SearchForPages(requestModel);
            var publications = result.Results
                .Select(x => x.OriginalObjectGetter() as PublicationBasePage)
                .Where(x => !(x is null));
            return new PublicationListSearchResults(
                publications,
                result.Categories.FirstOrDefault(x => x.Type is FilterGroupType.PublicationType)?.FilterOptions ?? new List<FilterOption>(),
                BuildYearOptions(allYears),
                result.TotalMatching,
                result);
        }

        public PublicationListSearchResults GetFromTypedSearch(PublicationRequest requestModel, SearchResults<int> allYears)
        {
            var searchQuery = !string.IsNullOrWhiteSpace(requestModel.Query)
                ? BuildSearchWithQuery(requestModel)
                : BuildSearchWithoutQuery(requestModel);

            if (requestModel.Types?.Length > 0)
            {
                var typeFilter = BuildTypeFilter(requestModel.Types);
                searchQuery = searchQuery.FilterHits(typeFilter);
            }

            if (requestModel.Year > 0)
                searchQuery = searchQuery.FilterHits(x => x.Year.Match(requestModel.Year));

            var results = searchQuery.GetPagesResultSafe();

            return new PublicationListSearchResults(
                results,
                BuildTypeOptions(results),
                BuildYearOptions(allYears),
                results.TotalMatching);
        }

        public PublicationListSearchResults Search(HistoricProposalRequest requestModel)
        {
            var result = SearchForPages(requestModel);
            var allYears = GetAllYears(requestModel);
            return new PublicationListSearchResults(
                result,
                BuildTypeOptions(result),
                BuildYearOptions(allYears),
                result.TotalMatching);
        }

        private SearchResults<int> GetAllYears(PublicationRequest requestModel) =>
            BuildSearchWithoutQuery(requestModel)
                .HistogramFacetFor(x => x.Year, 1)
                .Select(x => x.Year)
                .GetResultSafe();

        private SearchPageResults SearchForPages(PublicationRequest requestModel)
        {
            var publicationSearchRequest = MapPublicationRequestToPublicationSearchRequest(requestModel);
            return _searchService.Search(publicationSearchRequest);
        }

        private PublicationSearchRequest MapPublicationRequestToPublicationSearchRequest(PublicationRequest request) =>
            new PublicationSearchRequest(
                    request.Query,
                    request.Year,
                    request.Categories,
                    publicationTypes: request.Types,
                    types: GetDefaultPublicationPageTypes(),
                    from: 0,
                    to: 0,
                    page: request.Page,
                    request.PageSize,
                    request.PageRoot
                );

        private string[] GetDefaultPublicationPageTypes() =>
            new string[] { ((int)SearchResultType.Publication).ToString() };

        private ITypeSearch<PublicationPage> BuildSearchWithQuery(PublicationRequest requestModel) =>
            BuildSearch(_searchClient.Search<PublicationPage>()
                .For(requestModel.Query.Trim())
                .InFields(
                    p => p.Name,
                    p => p.Subtitle,
                    p => p.TagLine,
                    p => p.Author,
                    p => p.Location,
                    p => p.ContactPerson,
                    p => p.Division,
                    p => p.Isbn,
                    p => p.NetIsbn,
                    p => p.Program,
                    p => p.Publisher)
                , requestModel)
                .Track();

        private ITypeSearch<PublicationPage> BuildSearchWithoutQuery(PublicationRequest requestModel) =>
            BuildSearch(_searchClient.Search<PublicationPage>(), requestModel);

        private static ITypeSearch<PublicationPage> BuildSearch(ITypeSearch<PublicationPage> search, PublicationRequest request) =>
            search
                .FilterForVisitor()
                .Filter(x => x.Ancestors().Match(request.PageRoot.ToString()))
                .OrderBy(x => x.StartPublish, SortMissing.Last, SortOrder.Descending, false)
                .TermsFacetFor(t => t.ComputedType, action => action.Size = 100)
                .Skip(Math.Max(0, request.Page - 1) * request.PageSize)
                .Take(request.PageSize > 0 ? request.PageSize : SearchConstants.MaxPageSize);

        private IEnumerable<FilterOption> BuildTypeOptions(IHasFacetResults<PublicationBasePage> result) =>
            result.TermsFacetFor(x => x.ComputedType) is TermsFacet typeFacet
                ? typeFacet.Select(t => new FilterOption
                {
                    Name = Enum.TryParse(t.Term, out PublicationType typeValue)
                    ? _localizationProvider.GetEnumName(typeValue)
                    : t.Term,
                    Value = ((int)typeValue).ToString(),
                    Count = t.Count
                })
                .OrderBy(t => t.Name)
                : Enumerable.Empty<FilterOption>();

        private FilterBuilder<PublicationBasePage> BuildTypeFilter(IEnumerable<int> types)
        {
            var filter = _searchClient.BuildFilter<PublicationBasePage>();

            foreach (var type in types.Where(x => Enum.IsDefined(typeof(PublicationType), x)))
            {
                filter = filter.Or(x => x.ComputedType.Match(Enum.GetName(typeof(PublicationType), type)));
            }

            return filter;
        }

        private static IEnumerable<FilterOption> BuildYearOptions(IHasFacetResults<int> result) =>
            GetYearFacet(result)
                .Select(x => new FilterOption
                {
                    Name = x.Key.ToString(),
                    Value = x.Key.ToString(),
                    Count = x.Count
                })
                .OrderByDescending(t => t.Value);

        private static IEnumerable<HistogramFacet.IntervalCount> GetYearFacet(IHasFacetResults<int> result) =>
            (result.Facets.FirstOrDefault(x => x.Name == "Year") as HistogramFacet)?.Entries
            ?? Enumerable.Empty<HistogramFacet.IntervalCount>();

        private SearchResults<int> GetAllYears(HistoricProposalRequest requestModel) =>
            BuildSearchWithoutQuery(requestModel)
                .HistogramFacetFor(x => x.Year, 1)
                .Select(x => x.Year)
                .GetResultSafe();

        private PagesResult<HistoricProposalPage> SearchForPages(HistoricProposalRequest requestModel)
        {
            var searchQuery = !string.IsNullOrWhiteSpace(requestModel.Query)
                ? BuildSearchWithQuery(requestModel)
                : BuildSearchWithoutQuery(requestModel);

            if (requestModel.Types?.Length > 0)
            {
                var typeFilter = BuildTypeFilter(requestModel.Types);
                searchQuery = searchQuery.FilterHits(typeFilter);
            }

            if (requestModel.Year > 0)
                searchQuery = searchQuery.FilterHits(x => x.Year.Match(requestModel.Year));

            return searchQuery.GetPagesResultSafe();
        }

        private ITypeSearch<HistoricProposalPage> BuildSearchWithQuery(HistoricProposalRequest requestModel) =>
            BuildSearch(
                _searchClient.Search<HistoricProposalPage>()
                    .For(requestModel.Query.Trim())
                    .InFields(
                        p => p.Name,
                        p => p.Subtitle,
                        p => p.Id,
                        p => p.ProposalType,
                        p => p.Program),
                requestModel)
                .Track();

        private ITypeSearch<HistoricProposalPage> BuildSearchWithoutQuery(HistoricProposalRequest requestModel) =>
            BuildSearch(_searchClient.Search<HistoricProposalPage>(), requestModel);

        private static ITypeSearch<HistoricProposalPage> BuildSearch(ITypeSearch<HistoricProposalPage> search, HistoricProposalRequest request) =>
            search
                .FilterForVisitor()
                .Filter(x => x.Ancestors().Match(request.PageRoot.ToString()))
                .OrderBy(x => x.Deadline, SortMissing.Last, SortOrder.Descending, false)
                .TermsFacetFor(t => t.ComputedType, action => action.Size = 100)
                .Take(SearchConstants.MaxPageSize);
    }
}