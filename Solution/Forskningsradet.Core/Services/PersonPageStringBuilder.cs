﻿using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Services.Contracts;

namespace Forskningsradet.Core.Services
{
    public class PersonPageStringBuilder : IPersonPageStringBuilder
    {
        private const string PhoneUrlPrefix = "tel:";
        private const string EmailUrlPrefix = "mailto:";

        public string BuildPhoneUrlScheme(string phoneNumber)
            => PhoneUrlPrefix + phoneNumber;

        public string BuildEmailUrlScheme(string email)
            => EmailUrlPrefix + email;

        public string BuildTitle(PersonPage page)
            => $"{page.FirstName} {page.LastName}".Trim() is var fullName
               && !string.IsNullOrEmpty(fullName)
                ? fullName
                : page.PageName;
    }
}
