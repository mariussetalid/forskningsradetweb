using System;
using System.Collections.Generic;
using System.Linq;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.Find;
using EPiServer.Find.Api.Facets;
using EPiServer.Find.Cms;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Extensions;
using Forskningsradet.Core.Models.ApiModels.Evurdering;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.SearchModels;
using Forskningsradet.Core.Repositories.Contracts;
using Forskningsradet.Core.Services.Contracts;

namespace Forskningsradet.Core.Services
{
    public class ProposalService : IProposalService
    {
        private readonly IClient _client;
        private readonly IPageStructureService _pageStructureService;
        private readonly IPageRepository _pageRepository;
        private readonly IContentLoader _contentLoader;
        private readonly ICategoryGroupService _categoryGroupService;
        private readonly CategoryRepository _categoryRepository;
        private readonly ILocalizationProvider _localizationProvider;
        private readonly IProposalPageStatusHandler _proposalPageStatusHandler;

        public ProposalService(
            IClient client,
            IPageStructureService pageStructureService,
            IPageRepository pageRepository,
            IContentLoader contentLoader,
            ICategoryGroupService categoryGroupService,
            CategoryRepository categoryRepository,
            ILocalizationProvider localizationProvider,
            IProposalPageStatusHandler proposalPageStatusHandler)
        {
            _client = client;
            _pageStructureService = pageStructureService;
            _pageRepository = pageRepository;
            _contentLoader = contentLoader;
            _categoryGroupService = categoryGroupService;
            _categoryRepository = categoryRepository;
            _localizationProvider = localizationProvider;
            _proposalPageStatusHandler = proposalPageStatusHandler;
        }

        public ProposalSearchResult Search(
            ContentReference pageRoot,
            TimeFrameFilter statusFilter,
            string[] subjects,
            string[] targetGroups,
            int[] applicationTypes,
            string[] deadlineTypes)
        {
            var result = SearchForProposalPages(pageRoot, statusFilter, subjects, targetGroups, applicationTypes, deadlineTypes);
            var applicationTypePages = GetApplicationTypePages().ToList();
            var applicationGroups = GroupByApplicationType(result, applicationTypePages);

            return new ProposalSearchResult(
                applicationGroups,
                BuildAllCategoryGroups(result, applicationTypePages),
                result.TotalMatching);
        }

        public IEnumerable<SubjectBlock> GetSortedSubjects(ProposalPage page)
        {
            foreach (var item in page.SubjectBlocks?.FilteredItems ?? new List<ContentAreaItem>())
            {
                if (_contentLoader.Get<SubjectBlock>(item.ContentLink) is SubjectBlock subjectBlock)
                {
                    yield return subjectBlock;
                }
            }
        }

        private IEnumerable<CategoryGroup> BuildAllCategoryGroups(IContentResult<ProposalBasePage> result, IEnumerable<ApplicationTypePage> applicationTypePages)
        {
            var categoryFacets = result.Facets["Category"] as HistogramFacet;
            var categories = categoryFacets?.Entries.Select(x => _categoryRepository.Get(x.Key)).ToList() ?? new List<Category>();

            yield return BuildCategoryGroup(FilterGroupType.Subject, _categoryRepository.Get(CategoryConstants.SubjectRoot), categoryFacets, categories);
            yield return BuildCategoryGroup(FilterGroupType.TargetGroup, _categoryRepository.Get(CategoryConstants.TargetGroupRoot), categoryFacets, categories);
            yield return new CategoryGroup(FilterGroupType.DeadlineType, BuildDeadlineTypeOptions(result).ToList());
            yield return new CategoryGroup(FilterGroupType.ApplicationType, BuildApplicationTypeOptions(result, applicationTypePages.ToList()).ToList());
        }

        private CategoryGroup BuildCategoryGroup(FilterGroupType type, Category topCategory, HistogramFacet categoryFacets, List<Category> categories) =>
            _categoryGroupService.BuildCategoryGroup(type, topCategory, categoryFacets, categories);

        private IContentResult<ProposalBasePage> SearchForProposalPages(
            ContentReference pageRoot,
            TimeFrameFilter statusFilter,
            IReadOnlyCollection<string> subjects,
            IReadOnlyCollection<string> targetGroups,
            IReadOnlyCollection<int> applicationTypes,
            IReadOnlyCollection<string> deadlineTypes)
        {
            var query = _client.Search<ProposalBasePage>()
                .HistogramFacetFor(p => p.DeadlineComputed, DateInterval.Day)
                .HistogramFacetFor(p => p.DeadlineComputedForSearchFacet, DateInterval.Day)
                .HistogramFacetFor(p => p.ApplicationTypeReference.ID, 1)
                .TermsFacetFor(p => p.ComputedDeadlineType)
                .CategoriesFacet()
                .FilterForVisitor()
                .Filter(x => x.Ancestors().Match(pageRoot.ToReferenceWithoutVersion().ToString()))
                .Take(SearchConstants.MaxPageSize);

            query = _proposalPageStatusHandler.FilterForStatus(query, statusFilter);

            if (subjects?.Count > 0)
            {
                var subjectFilter = BuildCategoryFilter(subjects);
                query = query.FilterHits(subjectFilter);
            }

            if (targetGroups?.Count > 0)
            {
                var targetGroupFilter = BuildCategoryFilter(targetGroups);
                query = query.FilterHits(targetGroupFilter);
            }

            if (applicationTypes?.Count > 0)
            {
                var applicationTypeFilter = BuildApplicationTypeFilter(applicationTypes);
                query = query.FilterHits(applicationTypeFilter);
            }

            if (deadlineTypes?.Count > 0)
            {
                var deadlineTypeFilter = BuildDeadlineTypeFilter(deadlineTypes);
                query = query.FilterHits(deadlineTypeFilter);
            }

            return query.GetContentResultSafe();
        }

        private IEnumerable<ApplicationTypePage> GetApplicationTypePages()
        {
            var applicationTypeRoot = (_pageRepository.GetCurrentStartPage() ?? _pageRepository.GetStartPageFromWildcardHost())?.ApplicationTypeRoot;

            return applicationTypeRoot is null
                ? new List<ApplicationTypePage>()
                : _pageStructureService.GetFilteredChildPages<ApplicationTypePage>(applicationTypeRoot, true);
        }

        private static IEnumerable<ApplicationTypeGroup> GroupByApplicationType(IEnumerable<ProposalBasePage> result, IEnumerable<ApplicationTypePage> applicationTypePages) =>
            applicationTypePages.Any()
            ? applicationTypePages
                .Select(page => new ApplicationTypeGroup
                {
                    Title = page.PageName,
                    SubTitle = page.Subtitle,
                    Text = page.Text,
                    Pages = GetProposalsInGroup(result, page.PageLink)
                })
                .Where(group => group.Pages.Any())
            : new List<ApplicationTypeGroup>
            {
                new ApplicationTypeGroup
                {
                    Pages = result
                }
            };

        private static IEnumerable<ProposalBasePage> GetProposalsInGroup(IEnumerable<ProposalBasePage> proposalPages, ContentReference applicationTypeReference) =>
            proposalPages.Where(x => x.ApplicationTypeReference == applicationTypeReference);

        private FilterBuilder<ProposalBasePage> BuildCategoryFilter(IEnumerable<string> categories)
        {
            var categoryFilter = _client.BuildFilter<ProposalBasePage>();

            foreach (var category in categories)
            {
                categoryFilter = categoryFilter.Or(x => x.Category.Match(int.Parse(category)));
            }

            return categoryFilter;
        }

        private FilterBuilder<ProposalBasePage> BuildApplicationTypeFilter(IEnumerable<int> applicationTypes)
        {
            var buildApplicationTypeFilter = _client.BuildFilter<ProposalBasePage>();

            foreach (var type in applicationTypes)
            {
                buildApplicationTypeFilter = buildApplicationTypeFilter.Or(x => x.ApplicationTypeReference.ID.Match(type));
            }

            return buildApplicationTypeFilter;
        }

        private FilterBuilder<ProposalBasePage> BuildDeadlineTypeFilter(IEnumerable<string> deadlineTypes)
        {
            var buildTypeFilter = _client.BuildFilter<ProposalBasePage>();

            foreach (var deadline in deadlineTypes)
            {
                if (Enum.TryParse(deadline, out DeadlineType typeValue))
                {
                    buildTypeFilter = buildTypeFilter.Or(x => x.ComputedDeadlineType.Match(deadline));
                }
                else
                {
                    var deadlineAsDateTime = ToDateTime(deadline);
                    buildTypeFilter = buildTypeFilter.Or(x => x.DeadlineComputed.MatchDay(deadlineAsDateTime.Year, deadlineAsDateTime.Month, deadlineAsDateTime.Day));
                }
            }

            return buildTypeFilter;
        }

        private static DateTime ToDateTime(string deadline) => DateTime.FromBinary(long.Parse(deadline));

        private static IEnumerable<FilterOption> BuildDeadlineOptions(IHasFacetResults<ProposalBasePage> result)
        {
            var deadlineFacets = result.Facets["DeadlineComputedForSearchFacet"] as DateHistogramFacet;
            return deadlineFacets?.Entries
                .Select(
                    t => new FilterOption
                    {
                        Name = t.Key.ToDisplayDate().ToNorwegianDateString(),
                        Value = t.Key.ToBinary().ToString(),
                        Count = t.Count
                    }) ?? Enumerable.Empty<FilterOption>();
        }

        private static IEnumerable<FilterOption> BuildApplicationTypeOptions(
            IHasFacetResults<ProposalBasePage> result,
            IReadOnlyCollection<ApplicationTypePage> applicationTypePages)
        {
            var applicationTypeFacets = result.Facets["ApplicationTypeReference.ID"] as HistogramFacet;
            return applicationTypeFacets?.Entries
                .Select(
                    t => new FilterOption
                    {
                        Name = applicationTypePages
                            .FirstOrDefault(x => x.ContentLink.ID == t.Key)?.PageName,
                        Value = t.Key.ToString(),
                        Count = t.Count
                    }) ?? Enumerable.Empty<FilterOption>();
        }

        private IEnumerable<FilterOption> BuildDeadlineTypeOptions(IHasFacetResults<ProposalBasePage> result) =>
            result.TermsFacetFor(x => x.ComputedDeadlineType) is TermsFacet typeFacet
                ? typeFacet
                    .Where(t => !Enum.TryParse(t.Term, out DeadlineType typeValue) || typeValue != DeadlineType.Date)
                    .Select(t => new FilterOption
                    {
                        Name = Enum.TryParse(t.Term, out DeadlineType typeValue)
                            ? _localizationProvider.GetEnumName(typeValue)
                            : t.Term,
                        Value = t.Term,
                        Count = t.Count
                    })
                    .OrderBy(t => t.Name)
                    .Concat(BuildDeadlineOptions(result))
                : Enumerable.Empty<FilterOption>();
    }
}