using System;
using System.Globalization;
using System.Linq;
using EPiServer.Core;
using EPiServer.Shell.Configuration;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ApiModels.Evurdering;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Repositories.Contracts;
using Forskningsradet.Core.Services.Contracts;

namespace Forskningsradet.Core.Services
{
    public class ProposalPageProvider : IProposalPageProvider
    {
        private readonly IPageRepository _pageRepository;

        public ProposalPageProvider(IPageRepository pageRepository) =>
            _pageRepository = pageRepository;

        public ProposalPage GetProposalPage(Proposal proposal, string languageBranch)
        {
            var parentPage = GetParentPage(proposal, languageBranch);
            return _pageRepository.GetPageByExternalId<ProposalPage>(proposal.Id.ToString(), parentPage, languageBranch);
        }

        public ProposalPage CreateProposalPage(Proposal proposal, string languageBranch, string masterLanguageBranch)
        {
            var parentPage = GetParentPage(proposal, languageBranch);
            var yearFolder = GetOrCreateYearFolder(proposal, languageBranch, parentPage);
            var createdPage = CreatePage(proposal.Id.ToString(), languageBranch, yearFolder, masterLanguageBranch);
            createdPage.Id = proposal.Id.ToString();
            createdPage.Name = proposal.GetTitle(languageBranch);

            _pageRepository.SaveAsDraft(createdPage);

            return createdPage;
        }

        public ProposalPage CreateProposalPageVersion(ProposalPage original) =>
            original?.CreateWritableClone() as ProposalPage;

        private ProposalPage CreatePage(string externalId, string languageBranch, PageReference parentPage, string masterLanguageBranch)
        {
            if (languageBranch == masterLanguageBranch)
                return _pageRepository.Create<ProposalPage>(parentPage, new CultureInfo(languageBranch));

            var masterLanguageVersion = _pageRepository.GetPageByExternalId<ProposalPage>(externalId, parentPage, masterLanguageBranch);
            return _pageRepository.CreateLanguageBranch<ProposalPage>(masterLanguageVersion.ContentLink, languageBranch);
        }

        private PageReference GetParentPage(Proposal proposal, string languageBranch)
        {
            var frontPage = _pageRepository.GetCurrentStartPage() ?? _pageRepository.GetStartPageFromWildcardHost();
            var parentPage = GetProposalPage(frontPage, proposal.Program?.ShortName);

            if (parentPage is null)
                throw new MissingConfigurationException("Proposal import location has not been configured.");

            if (proposal.IsRff)
            {
                if (string.IsNullOrEmpty(proposal.Program?.ShortName))
                    throw new ArgumentException("Program short name must have a value when IsRff is true.");

                if (GetRffParentPageReference(proposal.Program.ShortName, frontPage) is PageReference rffPage)
                    parentPage = _pageRepository.GetOrCreateFolder(rffPage, proposal.Program.ShortName, languageBranch).PageLink;
                else
                    throw new MissingConfigurationException("RFF proposal import location has not been configured.");
            }

            return parentPage;
        }

        private PageReference GetOrCreateYearFolder(Proposal proposal, string languageBranch, PageReference parentPage)
        {
            var folderName = proposal.DeadlineType == DeadlineType.Date && proposal.ApplicationDeadline.HasValue
                ? proposal.ApplicationDeadline.Value.Year.ToString()
                : DateTime.UtcNow.Year.ToString();

            return _pageRepository.GetOrCreateFolder(parentPage, folderName, languageBranch).PageLink;
        }

        private static PageReference GetRffParentPageReference(string programShortName, FrontPage frontPage)
        {
            var specificParent = frontPage?.RffLocations?.FirstOrDefault(x => x.Code == programShortName);
            return specificParent?.Location ?? frontPage?.RffProposalPage;
        }

        private static PageReference GetProposalPage(FrontPage frontPage, string activity) =>
            activity == SearchConstants.ProposalSearch.TestActivity
                ? frontPage?.ProposalsTestPage
                : frontPage?.ProposalsPage;
    }
}