﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EPiServer;
using EPiServer.Core;
using EPiServer.Find.Helpers;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Repositories.Contracts;
using Forskningsradet.Core.Services.Contracts;
using Forskningsradet.ServiceAgents.Contracts;
using Forskningsradet.ServiceAgents.ResponseModels;

namespace Forskningsradet.Core.Services
{
    public class HrManagerSynchronizationService : IHrManagerSynchronizationService
    {
        private readonly IHrManagerServiceAgent _serviceAgent;
        private readonly IPageRepository _pageRepository;
        private readonly IHtmlExtractor _htmlExtractor;
        private readonly PageReference _vacanciesPageReference;

        public HrManagerSynchronizationService(IHrManagerServiceAgent serviceAgent, IPageRepository pageRepository, IHtmlExtractor htmlExtractor)
        {
            _serviceAgent = serviceAgent;
            _pageRepository = pageRepository;
            _htmlExtractor = htmlExtractor;
            _vacanciesPageReference = _pageRepository.GetCurrentStartPage()?.VacanciesPage;
        }

        public async Task<string> Synchronize()
        {
            var synchronizationLog = new StringBuilder();
            if (ContentReference.IsNullOrEmpty(_vacanciesPageReference))
                throw new Exception("Det finnes ingen side for ledige stillinger");

            var hrManagerVacanciesResponseModel = await GetHrManagerVacanciesResponseModel();
            var vacancies = await GetVacancies(hrManagerVacanciesResponseModel);
            synchronizationLog.Append($"Found {vacancies.Length} vacancies in HrManager\n");

            var episerverVacancyPages = _pageRepository.GetPublishedDescendants<VacancyPage>(_vacanciesPageReference);
            var hrManagerVacancyIds = vacancies.Select(x => x.Id).ToArray();
            var episerverIds = episerverVacancyPages.Select(x => x.HrManagerId).ToArray();
            var pageIdsToBeUnPublished = episerverIds.Except(hrManagerVacancyIds).ToArray();
            var pagesToBeUnpublished = GetPagesToBeUnPublished(episerverVacancyPages, pageIdsToBeUnPublished).ToArray();
            pagesToBeUnpublished.ForEach(_pageRepository.UnPublish);
            synchronizationLog.Append($"Unpublished {pagesToBeUnpublished.Length} vacancy pages\n");

            var hrManagerIdsToBeCreated = hrManagerVacancyIds.Except(episerverIds);
            var vacanciesToBeCreatedAsPages = GetHrManagerVacanciesToBeCreated(vacancies, hrManagerIdsToBeCreated).ToArray();
            vacanciesToBeCreatedAsPages.ForEach(CreateAndPublishPage);
            synchronizationLog.Append($"Created {vacanciesToBeCreatedAsPages.Length} vacancy pages\n");

            var changedVacancyPages = GetChangedVacancyPages(episerverVacancyPages, vacancies);
            changedVacancyPages.ForEach(x => UpdateVacancyPage(x.Item1, x.Item2));

            synchronizationLog.Append($"Updated {changedVacancyPages.Count} vacancies.");
            return synchronizationLog.ToString();
        }

        private async Task<Vacancies> GetHrManagerVacanciesResponseModel()
            => await _serviceAgent.GetVacancies();

        private async Task<Vacancy[]> GetVacancies(Vacancies hrManagerVacanciesResponseModel)
            => await Task
                .WhenAll(hrManagerVacanciesResponseModel.Items
                    .Select(x => x.Id)
                    .Select(async vacancyId => await _serviceAgent.GetVacancy(vacancyId))
                    .ToList());

        private static IEnumerable<VacancyPage> GetPagesToBeUnPublished(IEnumerable<VacancyPage> episerverVacancies, IEnumerable<int> pageIds)
            => episerverVacancies
                .Where(vacancy => pageIds
                    .Contains(vacancy.HrManagerId));

        private static IEnumerable<Vacancy> GetHrManagerVacanciesToBeCreated(
            IEnumerable<Vacancy> hrManagerVacancies, IEnumerable<int> hrManagerIds)
            => hrManagerVacancies
                .Where(vacancy => hrManagerIds
                    .Contains(vacancy.Id));

        private List<(VacancyPage, Vacancy)> GetChangedVacancyPages(IList<VacancyPage> episerverPages, IList<Vacancy> hrManagerPages) =>
            hrManagerPages
                .Select(vacancy => (EpiserverPage: episerverPages.FirstOrDefault(y => y.HrManagerId.Equals(vacancy.Id)), hrManagerPage: vacancy))
                .Where(x => x.EpiserverPage != null && ResponseModelHasChanged(x.EpiserverPage, x.hrManagerPage))
                .ToList();

        private void CreateAndPublishPage(Vacancy vacancyToAdd)
        {
            var vacancyPage = _pageRepository.Create<VacancyPage>(_vacanciesPageReference, CultureInfo.GetCultureInfo("no"));
            vacancyPage.ArchiveLink = ContentReference.WasteBasket;
            MapVacancyToVacancyPage(vacancyPage, vacancyToAdd);
            _pageRepository.Save(vacancyPage);
        }

        private void MapVacancyToVacancyPage(VacancyPage vacancyPage, Vacancy vacancyToAdd)
        {
            vacancyPage.StopPublish = null;
            vacancyPage.HrManagerId = vacancyToAdd.Id;
            vacancyPage.MainIntro = GetVacancyMainIntro(vacancyToAdd);
            vacancyPage.Title = vacancyToAdd.Name;
            vacancyPage.Name = vacancyToAdd.Name;
            vacancyPage.DueDate = vacancyToAdd.ApplicationDue;
            vacancyPage.HrManagerUri = Url.Decode(vacancyToAdd.AdvertisementUrl);
            vacancyPage.WorkplaceLocation = vacancyToAdd.WorkPlace?.Trim();
        }

        private string GetVacancyMainIntro(Vacancy vacancyToAdd)
        {
            var text = _htmlExtractor.ExtractTextFromFirstDiv(vacancyToAdd.Advertisements?.FirstOrDefault()?.Content);
            return text == string.Empty
                ? _htmlExtractor.ExtractTextFromFirstParagraph(vacancyToAdd.Advertisements?.FirstOrDefault()?.Content)
                : text;
        }

        private void UpdateVacancyPage(VacancyPage page, Vacancy vacancy)
        {
            var writableClone = page.CreateWritableClone() as VacancyPage;
            MapVacancyToVacancyPage(writableClone, vacancy);
            _pageRepository.Save(writableClone);
        }

        private bool ResponseModelHasChanged(VacancyPage page, Vacancy responseModel)
            => !_pageRepository.IsPublished(page)
                   || (!page.Title?.Equals(responseModel.Name) ?? responseModel.Name != null)
                   || !page.DueDate.Equals(responseModel.ApplicationDue)
                   || (!page.MainIntro?.Equals(GetVacancyMainIntro(responseModel)) ?? !string.IsNullOrEmpty(GetVacancyMainIntro(responseModel)))
                   || !page.HrManagerUri.OriginalString.Equals(responseModel.AdvertisementUrl)
                   || (!page.WorkplaceLocation?.Equals(responseModel.WorkPlace?.Trim()) ?? !string.IsNullOrEmpty(responseModel.WorkPlace?.Trim()));
    }
}
