﻿using System;
using System.Text;
using EPiServer.Core;
using EPiServer.Web.Routing;
using Forskningsradet.Core.Extensions;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Services.Contracts;

namespace Forskningsradet.Core.Services
{
    public class CalendarService : ICalendarService
    {
        const string IcalSpecifiedLineBreak = "\r\n";
        const string DateFormat = "yyyyMMddTHHmmssZ";

        private readonly IUrlResolver _urlResolver;

        public CalendarService(IUrlResolver urlResolver) =>
            _urlResolver = urlResolver;

        public string GetCalendarString(DateTime startDate, DateTime endDate, string summary, string location, string description, string url)
        {
            const string lineBreak = IcalSpecifiedLineBreak;
            var sb = new StringBuilder();
            sb.Append("BEGIN:VCALENDAR");
            sb.Append(lineBreak);
            sb.Append("PRODID:-//The Research Council of Norway//Forskningsradet Web//EN");
            sb.Append(lineBreak);
            sb.Append("VERSION:2.0");
            sb.Append(lineBreak);
            sb.Append("METHOD:PUBLISH");
            sb.Append(lineBreak);
            sb.Append("BEGIN:VEVENT");
            sb.Append(lineBreak);
            sb.Append("DTSTART:" + startDate.ToUniversalTime().ToString(DateFormat));
            sb.Append(lineBreak);
            sb.Append("DTEND:" + endDate.ToUniversalTime().ToString(DateFormat));
            sb.Append(lineBreak);
            sb.Append("UID:" + NewGuid());
            sb.Append(lineBreak);
            sb.Append("DESCRIPTION:" + string.Join(" ", url, description).Trim());
            sb.Append(lineBreak);
            sb.Append("LOCATION:" + location);
            sb.Append(lineBreak);
            sb.Append("SUMMARY:" + summary);
            sb.Append(lineBreak);
            sb.Append("END:VEVENT");
            sb.Append(lineBreak);
            sb.Append("END:VCALENDAR");
            return sb.ToString();
        }

        public string GetCalendarStringForEvent(EventPage eventPage) =>
            eventPage.StartDate != null && eventPage.EndDate != null
                ? GetCalendarString(
                    eventPage.StartDate ?? new DateTime(),
                    eventPage.EndDate ?? new DateTime(),
                    eventPage.ListTitle ?? eventPage.PageName,
                    eventPage.EventData.Where ?? eventPage.Location,
                    eventPage.ListIntro,
                    (eventPage.ContentLink as PageReference)?.GetExternalUrl(_urlResolver) ?? string.Empty)
                : null;

        public virtual Guid NewGuid() => Guid.NewGuid();
    }
}
