﻿using System;
using System.Globalization;
using System.Linq;
using System.Threading;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.Framework.Localization;
using EPiServer.Globalization;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Repositories.Contracts;

namespace Forskningsradet.Core.Services
{
    public interface ILocalizationProvider
    {
        string GetLabel(string resourceName, string resourceKey);
        string GetEnumName<TEnum>(TEnum value) where TEnum : Enum;
        string GetEnumName<TEnum>(TEnum value, CultureInfo cultureInfo) where TEnum : Enum;
    }

    public interface ICategoryLocalizationProvider
    {
        string GetCategoryName(int id);
    }

    public class LocalizationProvider : ILocalizationProvider, ICategoryLocalizationProvider
    {
        private readonly LocalizationService _localizationService;
        private readonly IPageRepository _pageRepository;
        private readonly IContentLoader _contentLoader;
        private readonly CategoryRepository _categoryRepository;

        public LocalizationProvider(LocalizationService localizationService, IPageRepository pageRepository, IContentLoader contentLoader, CategoryRepository categoryRepository)
        {
            _localizationService = localizationService;
            _pageRepository = pageRepository;
            _contentLoader = contentLoader;
            _categoryRepository = categoryRepository;
        }

        public string GetLabel(string resourceName, string resourceKey) =>
            _localizationService.GetString($"/{resourceName}/{resourceKey}");

        public string GetEnumName<TEnum>(TEnum value) where TEnum : Enum =>
            GetEnumName(value, Thread.CurrentThread.CurrentUICulture);

        public string GetEnumName<TEnum>(TEnum value, CultureInfo cultureInfo) where TEnum : Enum
        {
            var staticName = Enum.GetName(typeof(TEnum), value);
            var localizationPath = $"/property/enum/{typeof(TEnum).Name.ToLowerInvariant()}/{staticName?.ToLowerInvariant()}";
            return _localizationService.TryGetStringByCulture(localizationPath, cultureInfo, out var localizedName)
                ? localizedName
                : staticName;
        }


        public string GetCategoryName(int id)
        {
            if (ContentLanguage.PreferredCulture.Name == LanguageConstants.MasterLanguageBranch)
                return _categoryRepository.Get(id).LocalizedDescription;

            var pageReference = (_pageRepository.GetCurrentStartPage() ?? _pageRepository.GetStartPageFromWildcardHost())?.CategoryTranslationPage;
            if (ContentReference.IsNullOrEmpty(pageReference))
                return _categoryRepository.Get(id).LocalizedDescription;

            var translationPage = _contentLoader.Get<CategoryTranslationPage>(pageReference);
            return translationPage.Subjects?.FirstOrDefault(x => x.Subject == id)?.Translation
                   ?? translationPage.TargetGroups?.FirstOrDefault(x => x.TargetGroup == id)?.Translation
                   ?? translationPage.Other?.FirstOrDefault(x => x.Other == id)?.Translation
                   ?? _categoryRepository.Get(id).LocalizedDescription;
        }
    }
}