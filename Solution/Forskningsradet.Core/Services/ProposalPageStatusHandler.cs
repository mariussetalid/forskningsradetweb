using System;
using EPiServer.Find;
using Forskningsradet.Core.Models.ApiModels.Evurdering;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.SearchModels;
using Forskningsradet.Core.Services.Contracts;

namespace Forskningsradet.Core.Services
{
    public class ProposalPageStatusHandler : IProposalPageStatusHandler
    {
        public TimeFrameFilter? GetStatus(ProposalBasePage proposalPage)
        {
            var state = proposalPage.ProposalState;

            if (proposalPage is ProposalPage proposal && proposal.HasApplicationResult)
                return TimeFrameFilter.Result;

            if (state == ProposalState.Planned || state == ProposalState.Active ||
                (state == ProposalState.Cancelled && !proposalPage.CancellationShouldHideFromActiveTab &&
                 (proposalPage.DeadlineType == DeadlineType.Continuous || DateTime.Now < proposalPage.Deadline)))
                return TimeFrameFilter.Future;

            if (state == ProposalState.Completed || state == ProposalState.Deleted || state == ProposalState.Cancelled)
                return TimeFrameFilter.Past;

            return null;
        }

        public ITypeSearch<ProposalBasePage> FilterForStatus(ITypeSearch<ProposalBasePage> query,
            TimeFrameFilter statusFilter)
        {
            if (statusFilter == TimeFrameFilter.Past)
            {
                query = query.Filter(p => p.ProposalState.Match(ProposalState.Completed)
                                          | p.ProposalState.Match(ProposalState.Deleted)
                                          | p.ProposalState.Match(ProposalState.Cancelled));
            }
            else if (statusFilter == TimeFrameFilter.Result)
            {
                query = query.Filter(p => ((ProposalPage)p).HasApplicationResult.Match(true));
            }
            else
            {
                query = query.Filter(p => p.ProposalState.Match(ProposalState.Planned)
                                          | p.ProposalState.Match(ProposalState.Active)
                                          | (p.ProposalState.Match(ProposalState.Cancelled) & p.CancellationShouldHideFromActiveTab.Match(false)
                                              & (p.DeadlineType.Match(DeadlineType.Continuous) | p.Deadline.AfterNow())));
            }

            return query;
        }

    }
}