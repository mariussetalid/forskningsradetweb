﻿using System;
using System.Collections.Generic;
using System.Linq;
using EPiServer.DataAbstraction;
using EPiServer.Find;
using EPiServer.Find.Api;
using EPiServer.Find.Api.Facets;
using EPiServer.Find.Cms;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Extensions;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.RequestModels;
using Forskningsradet.Core.Models.SearchModels;

namespace Forskningsradet.Core.Services
{
    public interface IEventListService
    {
        EventFilterSearchResults Search(EventListRequestModel request);
    }

    public class EventListService : IEventListService
    {
        private readonly IClient _searchClient;
        private readonly ICategoryGroupService _categoryGroupService;
        private readonly ICategoryFilterService _categoryFilterService;
        private readonly CategoryRepository _categoryRepository;
        private readonly ILocalizationProvider _localizationProvider;

        public EventListService(IClient searchClient, ICategoryGroupService categoryGroupService, ICategoryFilterService categoryFilterService, CategoryRepository categoryRepository, ILocalizationProvider localizationProvider)
        {
            _searchClient = searchClient;
            _categoryGroupService = categoryGroupService;
            _categoryFilterService = categoryFilterService;
            _categoryRepository = categoryRepository;
            _localizationProvider = localizationProvider;
        }

        public EventFilterSearchResults Search(EventListRequestModel request)
        {
            var result = SearchForEventPages(request);
            return new EventFilterSearchResults(
                result,
                BuildAllCategoryGroups(result, request.OnlyCustomCategories, request.TopCategories),
                result.TotalMatching);
        }

        private PagesResult<EventPage> SearchForEventPages(EventListRequestModel requestModel)
        {
            var searchQuery = _searchClient.Search<EventPage>()
                .TermsFacetFor(t => t.ComputedEventType, action => action.Size = 100)
                .TermsFacetFor(t => t.Location, action => action.Size = 100)
                .CategoriesFacet()
                .FilterForVisitor();

            if (requestModel.PageRoot > 0)
                searchQuery = searchQuery.Filter(x => x.Ancestors().Match(requestModel.PageRoot.ToString()));

            if (requestModel.TimeFrame == TimeFrameFilter.Past)
            {
                searchQuery = searchQuery
                    .Filter(x => x.ComputedEventEndDate.Before(DateTime.Today))
                    .OrderBy(e => e.ComputedEventDate, SortMissing.Last, SortOrder.Descending, false);
            }
            else
            {
                searchQuery = searchQuery
                    .Filter(x => x.ComputedEventEndDate.After(DateTime.Today))
                    .OrderBy(e => e.ComputedEventDate, SortMissing.Last, SortOrder.Ascending, false);
            }

            if (requestModel.Type?.Length > 0)
            {
                var typeFilter = BuildEventTypeFilter(requestModel.Type);
                searchQuery = searchQuery.FilterHits(typeFilter);
            }

            if (requestModel.PageCategories?.Length > 0)
            {
                var pageCategoryList = requestModel.PageCategories.Select(x => _categoryRepository.Get(x)).ToList();

                foreach (var filter in BuildCategoryFilterList(pageCategoryList, _categoryFilterService.GetDefaultTopCategories()))
                {
                    searchQuery = searchQuery.Filter(filter);
                }
            }

            if (requestModel.Categories?.Length > 0)
            {
                var categoryList = requestModel.Categories.Select(x => _categoryRepository.Get(x)).Where(x => x != null).ToList();
                var topCategories = requestModel.TopCategories != null && requestModel.TopCategories.Any()
                    ? requestModel.TopCategories.Select(x => _categoryRepository.Get(x)).ToList()
                    : _categoryFilterService.GetDefaultTopCategories();

                foreach (var categoryFilter in BuildCategoryFilterList(categoryList, topCategories))
                {
                    searchQuery = searchQuery.FilterHits(categoryFilter);
                }
            }

            if (requestModel.Location?.Length > 0)
                searchQuery = searchQuery.FilterHits(x => x.Location.In(requestModel.Location));

            if (requestModel.VideoOnly)
                searchQuery = searchQuery.Filter(x => x.ComputedHasVideoLink.Match(true));

            searchQuery = requestModel.Take > 0
                ? searchQuery.Take(requestModel.Take)
                : searchQuery.Take(SearchConstants.MaxPageSize);

            return searchQuery.GetPagesResultSafe();
        }

        private IEnumerable<CategoryGroup> BuildAllCategoryGroups(PagesResult<EventPage> result, bool mapCustomCategories, params int[] topCategories)
        {
            var categoryFacets = result.Facets["Category"] as HistogramFacet;
            var categories = categoryFacets?.Entries.Select(x => _categoryRepository.Get(x.Key)).ToList() ?? new List<Category>();

            if (mapCustomCategories)
            {
                foreach (var topCategory in topCategories.Select(x => _categoryRepository.Get(x)))
                {
                    yield return BuildCategoryGroup(FilterGroupType.None, topCategory, categoryFacets, categories);
                }
            }
            else
            {
                yield return BuildCategoryGroup(FilterGroupType.Subject, _categoryRepository.Get(CategoryConstants.SubjectRoot), categoryFacets, categories);
                yield return BuildCategoryGroup(FilterGroupType.TargetGroup, _categoryRepository.Get(CategoryConstants.TargetGroupRoot), categoryFacets, categories);
                yield return new CategoryGroup(FilterGroupType.EventType, BuildEventTypeOptions(result).ToList());
                yield return new CategoryGroup(FilterGroupType.Location, BuildLocationOptions(result).ToList());
            }
        }

        private CategoryGroup BuildCategoryGroup(FilterGroupType type, Category topCategory, HistogramFacet categoryFacets, List<Category> categories) =>
            _categoryGroupService.BuildCategoryGroup(type, topCategory, categoryFacets, categories);

        private IEnumerable<FilterOption> BuildEventTypeOptions(IHasFacetResults<EventPage> result) =>
            result.TermsFacetFor(x => x.ComputedEventType) is TermsFacet typeFacet
                ? typeFacet
                    .Select(t => new FilterOption
                    {
                        Name = Enum.TryParse(t.Term, out EventType typeValue)
                            ? _localizationProvider.GetEnumName(typeValue)
                            : t.Term,
                        Value = t.Term,
                        Count = t.Count
                    })
                    .OrderBy(t => t.Name)
                : Enumerable.Empty<FilterOption>();

        private static IEnumerable<FilterOption> BuildLocationOptions(IHasFacetResults<EventPage> result) =>
            result.TermsFacetFor(x => x.Location) is TermsFacet locationFacet
                ? locationFacet
                    .Select(t => new FilterOption
                    {
                        Name = t.Term,
                        Value = t.Term.ToString(),
                        Count = t.Count
                    })
                    .OrderBy(t => t.Name)
                : Enumerable.Empty<FilterOption>();

        private FilterBuilder<EditorialPage> BuildCategoryFilter(IEnumerable<int> categories)
        {
            var categoryFilter = _searchClient.BuildFilter<EditorialPage>();

            foreach (var category in categories)
            {
                categoryFilter = categoryFilter.Or(x => x.Category.Match(category));
            }

            return categoryFilter;
        }

        private FilterBuilder<EventPage> BuildEventTypeFilter(IEnumerable<string> types)
        {
            var filter = _searchClient.BuildFilter<EventPage>();

            foreach (var eventType in types)
            {
                filter = filter.Or(x => x.ComputedEventType.Match(eventType));
            }

            return filter;
        }

        private IEnumerable<FilterBuilder<EditorialPage>> BuildCategoryFilterList(IList<Category> categoryList, IList<Category> topCategoryList)
        {
            foreach (var topCategory in topCategoryList)
            {
                var categories = _categoryFilterService.GetCategoriesForTopCategoryInList(categoryList, topCategory);
                if (categories.Any())
                    yield return BuildCategoryFilter(categories);
            }
        }
    }
}
