﻿using System.Globalization;
using EPiServer.Globalization;
using Forskningsradet.Core.Services.Contracts;

namespace Forskningsradet.Core.Services
{
    public class LanguageContext : ILanguageContext
    {
        public CultureInfo CurrentCulture => ContentLanguage.PreferredCulture;
    }
}