using EPiServer.Core;
using Forskningsradet.Common.Logging;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Processes.Contexts;
using Forskningsradet.Core.Services.Contracts;

namespace Forskningsradet.Core.Services
{
    public interface IArchiveService
    {
        ArchivePageResult Archive(IContent content);
    }

    public class ArchiveService : IArchiveService
    {
        private readonly IProcessFactory _processFactory;
        private readonly IIntegrationLogger _integrationLogger;

        public ArchiveService(IProcessFactory processFactory, IIntegrationLogger integrationLogger)
        {
            _processFactory = processFactory;
            _integrationLogger = integrationLogger;
        }

        public ArchivePageResult Archive(IContent content)
        {
            if (!(content is EditorialPage page) || !page.IsArchivable)
                return new ArchivePageResult { Archivable = false };

            var context = new PrepareForArchivingContext(_integrationLogger) { Page = page };
            var process = _processFactory.GetProcessForContext(context);

            process.Run();

            return new ArchivePageResult
            {
                Archivable = true,
                Success = !context.Abort,
                Message = context.GetProgressReport()
            };
        }
    }

    public class ArchivePageResult
    {
        public bool Archivable { get; set; }
        public bool Success { get; set; }
        public string Message { get; set; }
    }
}