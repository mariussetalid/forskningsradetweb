﻿using System.Collections.Generic;
using System.Linq;
using EPiServer.Core;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.RequestModels;

namespace Forskningsradet.Core.Services
{
    public interface ILargeDocumentService
    {
        ContentReference GetPreviousPage(LargeDocumentModel model);
        ContentReference GetNextPage(LargeDocumentModel model);
        List<ContentReference> GetChapters(LargeDocumentModel model);
        List<ContentReference> GetChildren(ContentReference reference);
        Node GetTree(ContentReference rootReference);
    }

    public class LargeDocumentService : ILargeDocumentService
    {
        private readonly IPageStructureService _pageStructureService;

        public LargeDocumentService(IPageStructureService pageStructureService) => _pageStructureService = pageStructureService;

        public ContentReference GetPreviousPage(LargeDocumentModel model)
        {
            if (model.CurrentPageNode.ParentNode == null)
                return ContentReference.EmptyReference;

            var current = model.CurrentPageNode;
            var previousSibling = GetPreviousSibling(current);
            if (model.ChapterDepth > current.Depth && previousSibling.ContentReference.ID != current.ParentNode.ContentReference.ID)
                return GetLastPageOf(previousSibling, model.ChapterDepth)?.ContentReference;

            return previousSibling?.ContentReference;
        }

        public ContentReference GetNextPage(LargeDocumentModel model)
        {
            if (model.ChapterDepth == 0)
            {
                return model.CurrentPageNode.Depth == 0
                    ? ContentReference.EmptyReference
                    : model.LargeDocumentStructure.ContentReference;
            }

            var current = model.CurrentPageNode;

            if (model.ChapterDepth > current.Depth && current.Children.Any())
                return current.Children[0].ContentReference;

            return GetNextSibling(current)?.ContentReference ?? ContentReference.EmptyReference;
        }

        private Node GetLastPageOf(Node node, int depth)
        {
            if (node == null)
                return null;

            if (node.Depth >= depth)
                return node;

            return node.Children.Any()
                ? GetLastPageOf(node.Children.Last(), depth)
                : node;
        }

        private Node GetPreviousSibling(Node node)
        {
            if (node.ParentNode == null)
                return null;

            var siblings = node.ParentNode.Children;
            return siblings != null && siblings.First() != node
                ? siblings[siblings.IndexOf(node) - 1]
                : node.ParentNode;
        }

        private Node GetNextSibling(Node node)
        {
            if (node.ParentNode == null)
                return null;

            var siblings = node.ParentNode.Children;
            return siblings != null && siblings.Last() != node
                ? siblings[siblings.IndexOf(node) + 1]
                : GetNextSibling(node.ParentNode);
        }

        public Node GetTree(ContentReference rootReference) =>
            _pageStructureService.PopulateTree(rootReference, 0);

        public List<ContentReference> GetChapters(LargeDocumentModel model)
        {
            var currentPageDepth = model.CurrentPageNode.Depth;
            var currentChapterReference = GetCurrentChapterReferenceFromModel(model);

            return GetPagesForCurrentDepth(model.ChapterDepth, currentPageDepth, model.CurrentPageNode.ContentReference, currentChapterReference);
        }

        public List<ContentReference> GetChildren(ContentReference reference) =>
            _pageStructureService.GetFilteredChildPages<LargeDocumentPageBase>(new PageReference(reference))
                .Select(x => x.ContentLink.ToReferenceWithoutVersion())
                .ToList();

        private ContentReference GetCurrentChapterReferenceFromModel(LargeDocumentModel model) =>
            model.ChapterDepth >= 0
                ? model.ChapterDepth >= model.CurrentPageNode.Depth
                    ? model.CurrentPageNode.ContentReference
                    : GetAncestorAtDepth(model.CurrentPageNode.ParentNode, model.ChapterDepth).ContentReference
                : model.LargeDocumentStructure.ContentReference;

        private Node GetAncestorAtDepth(Node node, int depth) =>
            node.Depth > 0 && node.Depth > depth
                ? GetAncestorAtDepth(node.ParentNode, depth)
                : node;

        private List<ContentReference> GetPagesForCurrentDepth(int chapterDepth, int currentPageDepth, ContentReference currentPageReference, ContentReference currentChapterReference)
        {
            if (currentPageDepth < chapterDepth)
                return new List<ContentReference> { currentPageReference };

            if (currentPageDepth == chapterDepth)
                return GetChildren(currentPageReference);

            if (currentPageDepth > chapterDepth && currentChapterReference != ContentReference.EmptyReference)
                return GetChildren(currentChapterReference);

            return new List<ContentReference> { currentPageReference };
        }
    }
}