﻿using System.Collections.Generic;
using System.Linq;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using Forskningsradet.Common.Constants;

namespace Forskningsradet.Core.Services
{
    public interface ICategoryFilterService
    {
        List<Category> GetDefaultTopCategories();
        List<int> GetCategoriesForTopCategoryInList(IList<Category> categoryList, Category topCategory);
        IEnumerable<Category> GetCategoriesForLinkTags(CategoryList filterCategoryList, CategoryList tagCategoryRoot);
        IEnumerable<Category> GetDefaultCategoriesForLinkTags();
    }

    public class CategoryFilterService : ICategoryFilterService
    {
        private readonly CategoryRepository _categoryRepository;

        public CategoryFilterService(CategoryRepository categoryRepository) => _categoryRepository = categoryRepository;

        public List<Category> GetDefaultTopCategories() =>
            _categoryRepository.GetRoot().Categories
                .Where(x => x.Available)
                .Where(x => x.Name != CategoryConstants.SubjectRoot && x.Name != CategoryConstants.TargetGroupRoot)
                .SelectMany(x => x.Categories.Where(y => y.Available && y.Selectable))
                .Select(x => x.ID)
                .Concat(new[]
                {
                    _categoryRepository.Get(CategoryConstants.SubjectRoot).ID,
                    _categoryRepository.Get(CategoryConstants.TargetGroupRoot).ID
                })
                .Select(x => _categoryRepository.Get(x))
                .ToList();

        public List<int> GetCategoriesForTopCategoryInList(IList<Category> categoryList, Category topCategory)
        {
            var descendants = GetDescendantsInList(categoryList, topCategory);
            var topCategoryAndDescendants = BuildTopCategory().Concat(BuildDescendants()).ToList();
            return topCategoryAndDescendants;

            IEnumerable<int> BuildTopCategory() =>
                categoryList.Contains(topCategory)
                    ? new List<int> { topCategory.ID }
                    : new List<int>();

            IEnumerable<int> BuildDescendants() =>
                descendants.Any()
                    ? descendants.Select(x => x.ID)
                    : new List<int>();
        }

        public IEnumerable<Category> GetCategoriesForLinkTags(CategoryList includeCategoryList, CategoryList excludeCategoryList)
        {
            var top = includeCategoryList?
                          .Select(x => _categoryRepository.Get(x))
                          .Where(x => excludeCategoryList is null
                                      || !excludeCategoryList.Contains(x.ID))
                          .ToList() ?? new List<Category>();
            var children = top
                .SelectMany(x => x.Categories)
                .Distinct()
                .ToList();
            var grandChildren = children.SelectMany(x => x.Categories);
            return top
                .Union(children.Distinct())
                .Union(grandChildren.Distinct());
        }

        public IEnumerable<Category> GetDefaultCategoriesForLinkTags()
        {
            var defaultCategoriesForLinkTags = new List<int>
            {
                _categoryRepository.Get(CategoryConstants.SubjectRoot).ID,
                _categoryRepository.Get(CategoryConstants.TargetGroupRoot).ID
            };
            return GetCategoriesForLinkTags(new CategoryList(defaultCategoriesForLinkTags), null);
        }

        private static List<Category> GetDescendantsInList(IList<Category> categoryList, Category ancestor) =>
            categoryList.Where(x => x.Parent == ancestor)
                .Union(categoryList.Where(x => x.Parent?.Parent == ancestor))
                .ToList();
    }
}
