﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Forskningsradet.Core.Services.CacheConfigurations
{
    public class ImageMetadataServiceCacheConfiguration : CacheConfigurationBase
    {
        public override int TimeoutInSeconds => ReadTimeoutFromAppSettings("ImageMetadataServiceCache.LifetimeInSeconds", 0);

        public override bool Enabled => TimeoutInSeconds >= 0;

        public bool Unlimited => TimeoutInSeconds == 0;
    }
}
