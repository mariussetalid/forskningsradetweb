﻿using System;
using System.Configuration;
using System.Globalization;

namespace Forskningsradet.Core.Services.CacheConfigurations
{
    public abstract class CacheConfigurationBase
    {
        public virtual TimeSpan Timeout => TimeSpan.FromSeconds(TimeoutInSeconds);

        protected virtual int ReadTimeoutFromAppSettings(string name, int defaultTimeoutInSeconds) 
            => int.TryParse(ConfigurationManager.AppSettings[name], NumberStyles.Integer, CultureInfo.InvariantCulture, out var configured) 
                ? configured 
                : defaultTimeoutInSeconds;

        public virtual bool Enabled => TimeoutInSeconds > 0;

        public abstract int TimeoutInSeconds { get; }
    }
}