﻿using System;
using System.Drawing;
using EPiServer;
using EPiServer.Core;
using EPiServer.Framework.Cache;
using EPiServer.Logging;
using EPiServer.Web;
using EPiServer.Web.Routing;
using Forskningsradet.Core.Services.CacheConfigurations;
using Forskningsradet.Core.Services.Contracts;

namespace Forskningsradet.Core.Services
{
    public class ImageMetadataService : IImageMetadataService
    {
        private static readonly ILogger Log = LogManager.GetLogger();

        protected IUrlResolver UrlResolver { get; }
        protected IContentLoader ContentLoader { get; }

        public ImageMetadataService(IUrlResolver urlResolver, IContentLoader contentLoader)
        {
            UrlResolver = urlResolver;
            ContentLoader = contentLoader;
        }

        public Size TryGetImageSize(string imageUrl)
        {
            try
            {
                var imageLink = TryGetContentLink(imageUrl);
                if (imageLink != null && ContentLoader.TryGet(imageLink, out ImageData image))
                    return TryGetImageSize(image);
            }
            catch (Exception ex)
            {
                LogGetImageSizeError(ex);
            }

            return Size.Empty;
        }

        public Size TryGetImageSize(ImageData imageData)
        {
            try
            {
                return GetImageSize(imageData);
            }
            catch (Exception ex)
            {
                LogGetImageSizeError(ex);
            }

            return Size.Empty;
        }

        protected virtual Size GetImageSize(ImageData imageData)
        {
            if (imageData is null) throw new ArgumentNullException(nameof(imageData));

            using (var image = Image.FromStream(imageData.BinaryData.OpenRead()))
                return image.Size;
        }

        private ContentReference TryGetContentLink(string imageUrl)
        {
            if (string.IsNullOrEmpty(imageUrl)) 
                return null;

            var imageUri = new Uri(imageUrl, UriKind.RelativeOrAbsolute);
            var urlBuilder = new UrlBuilder(imageUri.IsAbsoluteUri ? imageUri.PathAndQuery : imageUrl);
            return UrlResolver.Route(urlBuilder)?.ContentLink ??
                   UrlResolver.Route(urlBuilder, ContextMode.Preview)?.ContentLink;

        }

        private void LogGetImageSizeError(Exception ex)
            => Log.Error("Unexpected exception of getting image size", ex);
    }

    public class CachedImageMetadataService : ImageMetadataService
    {
        protected IObjectInstanceCache Cache { get; }
        protected IContentCacheKeyCreator ContentCacheKeyCreator { get; }
        protected ImageMetadataServiceCacheConfiguration CacheConfiguration { get; }

        public CachedImageMetadataService(ImageMetadataServiceCacheConfiguration cacheConfiguration, IObjectInstanceCache cache, IContentCacheKeyCreator contentCacheKeyCreator, IUrlResolver urlResolver, IContentLoader contentLoader)
            : base(urlResolver, contentLoader)
        {
            Cache = cache;
            CacheConfiguration = cacheConfiguration;
            ContentCacheKeyCreator = contentCacheKeyCreator;
        }

        protected override Size GetImageSize(ImageData imageData)
        {
            if (imageData == null) throw new ArgumentNullException(nameof(imageData));

            if (CacheConfiguration.Enabled)
            {
                var cacheKey = $"{GetType().FullName}.{nameof(GetImageSize)}|ImagePath-{imageData.BinaryData.ID.LocalPath}";
                var size = Cache.ReadThrough(
                    cacheKey,
                    () => (object)base.GetImageSize(imageData),
                    () => CreateCacheEvictionPolicy(imageData.ContentLink)) as Size?;
                return size ?? Size.Empty;
            }

            return base.GetImageSize(imageData);
        }

        private CacheEvictionPolicy CreateCacheEvictionPolicy(ContentReference imageLink)
        {
            var masterKeys = new[] { ContentCacheKeyCreator.CreateVersionCacheKey(imageLink) };

            return CacheConfiguration.Unlimited
                ? new CacheEvictionPolicy(null, masterKeys)
                : new CacheEvictionPolicy(TimeSpan.FromSeconds(CacheConfiguration.TimeoutInSeconds), CacheTimeoutType.Absolute, null, masterKeys);
        }
    }
}
