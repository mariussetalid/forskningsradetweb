using System.Globalization;
using EPiServer.Core;
using EPiServer.Shell.Configuration;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ApiModels.Ntb;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Repositories.Contracts;
using Forskningsradet.Core.Services.Contracts;

namespace Forskningsradet.Core.Services
{
    public class PressReleasePageProvider : IPressReleasePageProvider
    {
        private readonly IPageRepository _pageRepository;
        private PageReference _parentPage;

        public PressReleasePageProvider(IPageRepository pageRepository) =>
            _pageRepository = pageRepository;

        public PressReleasePage GetPageByExternalId(long externalId)
        {
            var parentPage = GetParentPage();
            return _pageRepository.GetPageByExternalId<PressReleasePage>(externalId.ToString(), parentPage, LanguageConstants.MasterLanguageBranch);
        }

        public PressReleasePage CreatePage(PressRelease pressRelease)
        {
            var parentPage = GetParentPage();
            var yearFolder = GetOrCreateYearFolder(pressRelease, LanguageConstants.MasterLanguageBranch, parentPage);
            var createdPage = CreatePage(LanguageConstants.MasterLanguageBranch, yearFolder);
            createdPage.Id = pressRelease.Id.ToString();
            createdPage.Name = pressRelease.Title;

            _pageRepository.SaveAsDraft(createdPage);

            return createdPage;
        }

        public PressReleasePage CreatePageVersion(PressReleasePage original) =>
            original?.CreateWritableClone() as PressReleasePage;

        private PressReleasePage CreatePage(string languageBranch, PageReference parentPage) =>
            _pageRepository.Create<PressReleasePage>(parentPage, new CultureInfo(languageBranch));

        public void Publish(PressReleasePage original) =>
            _pageRepository.Save(original);

        private PageReference GetParentPage()
        {
            if (PageReference.IsNullOrEmpty(_parentPage))
            {
                var frontPage = _pageRepository.GetCurrentStartPage() ?? _pageRepository.GetStartPageFromWildcardHost();
                _parentPage = frontPage.PressReleasesPage;

                if (_parentPage is null)
                    throw new MissingConfigurationException("Press release import location has not been configured.");
            }

            return _parentPage;
        }

        private PageReference GetOrCreateYearFolder(PressRelease pressRelease, string languageBranch, PageReference parentPage)
        {
            var folderName = pressRelease.Published.Year.ToString();
            return _pageRepository.GetOrCreateFolder(parentPage, folderName, languageBranch).PageLink;
        }
    }
}