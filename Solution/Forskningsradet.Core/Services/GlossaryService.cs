﻿using System.Collections.Generic;
using System.Linq;
using EPiServer.Core;
using EPiServer.Find;
using EPiServer.Find.Api;
using EPiServer.Find.Api.Facets;
using EPiServer.Find.Cms;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Extensions;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.SearchModels;
using Forskningsradet.Core.Services.Contracts;
using GlossaryPage = Forskningsradet.Core.Models.ContentModels.Pages.GlossaryPage;

namespace Forskningsradet.Core.Services
{
    public class GlossaryService : IGlossaryService
    {
        private readonly IClient _searchClient;

        public GlossaryService(IClient searchClient) => _searchClient = searchClient;

        public GlossarySearchResults GetWordsMatchingQuery(GlossaryPage currentPage, string query)
        {
            var allWords = SearchForWordPages("", currentPage);
            if (allWords is null)
                return new GlossarySearchResults(new List<WordPage>(), new List<string>(), 0);

            if (string.IsNullOrWhiteSpace(query))
                return new GlossarySearchResults(allWords.Items, GetSortedLetters(allWords.Facets), allWords.TotalMatching);

            var result = SearchForWordPages(query, currentPage);
            return new GlossarySearchResults(result.Items, GetSortedLetters(allWords.Facets), result.TotalMatching);
        }

        private IContentResult<WordPage> SearchForWordPages(string query, GlossaryPage currentPage)
            => _searchClient.Search<WordPage>()
                .For(query, q => { q.Query = $"*{query}*"; })
                .InFields(
                    p => p.PageName,
                    p => p.MainIntro)
                .TermsFacetFor(t => t.Letter, action => action.Size = 100)
                .ExcludeDeleted()
                .FilterForVisitor()
                .Filter(x => x.ParentLink.Match(currentPage.ContentLink.ToReferenceWithoutVersion()))
                .Take(SearchConstants.MaxPageSize)
                .OrderBy(x => x.PageName, SortMissing.Last, SortOrder.Ascending, false)
                .GetContentResultSafe();

        private static IEnumerable<string> GetSortedLetters(FacetResults results)
        {
            if (!(results[nameof(WordPage.Letter)] is TermsFacet letterFacet))
                yield break;

            foreach (var term in letterFacet.Terms.OrderBy(l => l.Term))
            {
                yield return term.Term;
            }
        }
    }
}
