﻿using System.Collections.Generic;
using System.Linq;
using EPiServer.DataAbstraction;
using EPiServer.Find.Api.Facets;
using Forskningsradet.Core.Models.SearchModels;

namespace Forskningsradet.Core.Services
{
    public interface ICategoryGroupService
    {
        CategoryGroup BuildCategoryGroup(FilterGroupType type, Category topCategory, Facet categoryFacets, List<Category> categories);
    }

    public class CategoryGroupService : ICategoryGroupService
    {
        private readonly ICategoryLocalizationProvider _categoryLocalizationProvider;

        public CategoryGroupService(ICategoryLocalizationProvider categoryLocalizationProvider) => _categoryLocalizationProvider = categoryLocalizationProvider;

        public CategoryGroup BuildCategoryGroup(FilterGroupType type, Category topCategory, Facet categoryFacets, List<Category> categories)
        {
            var filterOptions = MapFilterOptions(categories, topCategory, categoryFacets).ToList();

            return new CategoryGroup(topCategory, type, filterOptions);
        }

        private IEnumerable<FilterOption> MapFilterOptions(List<Category> categories, Category topCategory, Facet categoryFacets)
        {
            var allChildren = categories.Where(x => x?.Parent == topCategory).ToList();
            var allGrandChildren = categories.Where(x => x?.Parent?.Parent == topCategory).ToList();

            allChildren = allChildren
                .Union(allGrandChildren
                    .Select(x => x.Parent)
                    .SkipWhile(x => allChildren.Contains(x))
                    .ToList())
                .ToList();

            foreach (var child in allChildren)
            {
                var grandChildren = allGrandChildren.Where(x => x.Parent == child).ToList();
                if (!grandChildren.Any())
                    yield return MapFilterOption(child, categoryFacets);
                else
                    yield return MapNestedFilterOption(child, grandChildren, categoryFacets);
            }
        }

        private FilterOption MapFilterOption(Category category, Facet categoryFacets)
        {
            var filterOption = new FilterOption
            {
                Name = _categoryLocalizationProvider.GetCategoryName(category.ID),
                Value = category.ID.ToString()
            };

            if (categoryFacets is HistogramFacet histogramFacet
                && histogramFacet.Entries.FirstOrDefault(x => x.Key == category.ID) is HistogramFacet.IntervalCount intervalCount)
            {
                filterOption.Value = intervalCount.Key.ToString();
                filterOption.Count = intervalCount.Count;
            }

            if (categoryFacets is TermsFacet termsFacet
                && termsFacet.Terms.FirstOrDefault(x => x.Term == category.Name) is TermCount termCount)
            {
                filterOption.Value = termCount.Term;
                filterOption.Count = termCount.Count;
            }

            return filterOption;
        }

        private FilterOption MapNestedFilterOption(Category category, IEnumerable<Category> children, Facet categoryFacets)
        {
            var nestedFilterOption = new NestedFilterOption
            {
                Name = _categoryLocalizationProvider.GetCategoryName(category.ID),
                Value = category.ID.ToString(),
                FilterOptions = children.Select(x => MapFilterOption(x, categoryFacets)).ToList()
            };

            if (categoryFacets is HistogramFacet histogramFacet)
            {
                nestedFilterOption.Value = category.ID.ToString();
                nestedFilterOption.Count = histogramFacet.Entries.FirstOrDefault(x => x.Key == category.ID)?.Count ?? 0;
            }

            if (categoryFacets is TermsFacet termsFacet)
            {
                nestedFilterOption.Value = category.Name;
                nestedFilterOption.Count = termsFacet.Terms.FirstOrDefault(x => x.Term == category.Name)?.Count ?? 0;
            }

            return nestedFilterOption;
        }
    }
}
