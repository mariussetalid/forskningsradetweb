﻿using Forskningsradet.Core.Services.Contracts;
using HtmlAgilityPack;

namespace Forskningsradet.Core.Services
{
    public class HtmlExtractor : IHtmlExtractor
    {
        public string ExtractTextFromFirstDiv(string content)
        {
            if (string.IsNullOrWhiteSpace(content) || !content.Contains("<div>") || !content.Contains("</div>"))
                return string.Empty;
            var htmlDocument = new HtmlDocument();
            htmlDocument.LoadHtml(content);
            return htmlDocument.DocumentNode.SelectSingleNode("//div")?.InnerText;
        }

        public string ExtractTextFromFirstParagraph(string content)
        {
            if (string.IsNullOrWhiteSpace(content) || !content.Contains("<p>") || !content.Contains("</p>"))
                return string.Empty;
            var htmlDocument = new HtmlDocument();
            htmlDocument.LoadHtml(content);
            return htmlDocument.DocumentNode.SelectSingleNode("//p")?.InnerText;
        }
    }
}
