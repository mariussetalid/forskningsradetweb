using System.Collections.Generic;
using EPiServer.DataAbstraction;

namespace Forskningsradet.Core.Models.SearchModels
{
    public class CategoryGroup
    {
        public Category Category { get; }
        public FilterGroupType Type { get; }
        public List<FilterOption> FilterOptions { get; }

        public CategoryGroup(FilterGroupType type, List<FilterOption> filterOptions)
        {
            Type = type;
            FilterOptions = filterOptions;
        }

        public CategoryGroup(Category category, FilterGroupType type, List<FilterOption> filterOptions)
        {
            Category = category;
            Type = type;
            FilterOptions = filterOptions;
        }
    }
}