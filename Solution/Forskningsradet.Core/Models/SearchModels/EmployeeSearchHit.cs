namespace Forskningsradet.Core.Models.SearchModels
{
    public class EmployeeSearchHit
    {
        public string Name { get; set; }
        public string JobTitle { get; set; }
        public string Department { get; set; }
        public string Email { get; set; }
        public string Letter { get; set; }
    }
}