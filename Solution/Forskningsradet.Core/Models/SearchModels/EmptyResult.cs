using System.Collections.Generic;
using EPiServer.Find;
using EPiServer.Find.Api;

namespace Forskningsradet.Core.Models.SearchModels
{
    public class EmptyResult<T> : SearchResults<T>
    {
        public EmptyResult() : base(
            new SearchResult<T>
            {
                Facets = new FacetResults(),
                Hits = new HitCollection<T>
                {
                    Hits = new List<SearchHit<T>>()
                },
                Shards = new Shards()
            })
        {
        }
    }
}