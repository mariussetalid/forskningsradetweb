using System.Collections.Generic;

namespace Forskningsradet.Core.Models.SearchModels
{
    public class NestedFilterOption : FilterOption
    {
        public List<FilterOption> FilterOptions { get; set; }
    }
}