namespace Forskningsradet.Core.Models.SearchModels
{
    public enum FilterGroupType
    {
        None = 0,
        Subject = 1,
        TargetGroup = 2,
        ContentType = 3,
        Year = 4,
        ApplicationType = 6,
        DeadlineType = 7,
        EventType = 8,
        Location = 9,
        PublicationType = 10
    }
}