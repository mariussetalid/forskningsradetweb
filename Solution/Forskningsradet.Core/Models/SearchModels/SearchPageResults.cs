using System.Collections.Generic;
using System.Linq;
using EPiServer.Find.UnifiedSearch;

namespace Forskningsradet.Core.Models.SearchModels
{
    public class SearchPageResults : SearchResultBase<UnifiedSearchHit>
    {
        public IEnumerable<CategoryGroup> Categories { get; }

        public SearchPageResults(
            IEnumerable<UnifiedSearchHit> results,
            IEnumerable<CategoryGroup> categories,
            int totalMatching)
            : base(results, totalMatching) => Categories = categories ?? Enumerable.Empty<CategoryGroup>();
    }
}