using System.Collections.Generic;
using EPiServer.Core;
using Forskningsradet.Core.Models.ContentModels.Pages;

namespace Forskningsradet.Core.Models.SearchModels
{
    public class ApplicationTypeGroup
    {
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public XhtmlString Text { get; set; }
        public IEnumerable<ProposalBasePage> Pages { get; set; }
    }
}