﻿namespace Forskningsradet.Core.Models.SearchModels
{
    public enum PublicationType
    {
        None = 0,
        AnnualReport = 1,
        StrategyPolicyPlan = 2,
        Program = 3,
        Evaluation = 4,
        Proposal = 5,
    }
}