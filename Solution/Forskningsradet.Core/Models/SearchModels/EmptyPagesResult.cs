using System.Collections.Generic;
using EPiServer.Core;
using EPiServer.Find;
using EPiServer.Find.Api;
using EPiServer.Find.Cms;

namespace Forskningsradet.Core.Models.SearchModels
{
    public class EmptyPagesResult<T> : PagesResult<T> where T : PageData
    {
        public EmptyPagesResult() : base(
            new PageDataCollection(),
            new SearchResults<PageReference>(
                new SearchResult<PageReference>
                {
                    Facets = new FacetResults(),
                    Hits = new HitCollection<PageReference>
                    {
                        Hits = new List<SearchHit<PageReference>>()
                    },
                    Shards = new Shards()
                }))
        {
        }
    }
}