using System.Collections.Generic;

namespace Forskningsradet.Core.Models.SearchModels
{
    public class EmployeeSearchResults : SearchResultBase<EmployeeSearchHit>
    {
        public IEnumerable<string> Letters { get; }

        public EmployeeSearchResults(IEnumerable<EmployeeSearchHit> results, IEnumerable<string> letters, int totalMatching)
            : base(results, totalMatching) => Letters = letters;
    }
}