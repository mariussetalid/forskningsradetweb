namespace Forskningsradet.Core.Models.SearchModels
{
    public class SearchHitTrackRequest
    {
        public string QueryTrackId { get; set; }
        public string Query { get; set; }
        public string HitId { get; set; }
        public int HitPosition { get; set; }
    }
}