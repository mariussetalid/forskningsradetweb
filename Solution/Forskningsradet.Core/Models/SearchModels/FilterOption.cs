namespace Forskningsradet.Core.Models.SearchModels
{
    public class FilterOption
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public int Count { get; set; }
    }
}