using System.Collections.Generic;

namespace Forskningsradet.Core.Models.SearchModels
{
    public class SearchResultBase<T>
    {
        public IEnumerable<T> Results { get; }
        public int TotalMatching { get; }

        protected SearchResultBase(IEnumerable<T> results, int totalMatching)
        {
            Results = results;
            TotalMatching = totalMatching;
        }
    }
}