using System.Collections.Generic;
using EPiServer.Find.Api;
using EPiServer.Find.UnifiedSearch;

namespace Forskningsradet.Core.Models.SearchModels
{
    public class EmptyUnifiedSearchResult : UnifiedSearchResults
    {
        public EmptyUnifiedSearchResult()
            : base(new SearchResult<UnifiedSearchHit>
            {
                Facets = new FacetResults(),
                Hits = new HitCollection<UnifiedSearchHit>
                {
                    Hits = new List<SearchHit<UnifiedSearchHit>>()
                },
                Shards = new Shards()
            })
        {
        }
    }
}