﻿namespace Forskningsradet.Core.Models.SearchModels
{
    public enum SearchResultType
    {
        None = 0,
        Event = 1,
        Landing = 2,
        Article = 3,
        News = 4,
        Proposal = 5,
        Person = 6,
        File = 7,
        Publication = 8,
        PressRelease = 9
    }
}