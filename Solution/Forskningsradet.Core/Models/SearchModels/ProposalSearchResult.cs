using System.Collections.Generic;

namespace Forskningsradet.Core.Models.SearchModels
{
    public class ProposalSearchResult : SearchResultBase<ApplicationTypeGroup>
    {
        public IEnumerable<CategoryGroup> Categories { get; }

        public ProposalSearchResult(
            IEnumerable<ApplicationTypeGroup> results,
            IEnumerable<CategoryGroup> categories,
            int totalMatching)
            : base(results, totalMatching) => Categories = categories;
    }
}