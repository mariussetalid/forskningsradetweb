using System.Collections.Generic;
using Forskningsradet.Core.Models.ContentModels.Pages;

namespace Forskningsradet.Core.Models.SearchModels
{
    public class GlossarySearchResults : SearchResultBase<WordPage>
    {
        public IEnumerable<string> Letters { get; }

        public GlossarySearchResults(IEnumerable<WordPage> results, IEnumerable<string> letters, int totalMatching)
            : base(results, totalMatching) => Letters = letters;
    }
}