using System.Collections.Generic;
using Forskningsradet.Core.Models.ContentModels.Pages;

namespace Forskningsradet.Core.Models.SearchModels
{
    public class PublicationListSearchResults : SearchResultBase<PublicationBasePage>
    {
        public IEnumerable<FilterOption> Types { get; }
        public IEnumerable<FilterOption> Years { get; }
        public SearchPageResults SearchPageResults { get; }

        public PublicationListSearchResults(
            IEnumerable<PublicationBasePage> results,
            IEnumerable<FilterOption> types,
            IEnumerable<FilterOption> years,
            int totalMatching,
            SearchPageResults searchPageResults = null)
            : base(results, totalMatching)
        {
            Types = types;
            Years = years;
            SearchPageResults = searchPageResults;
        }
    }
}