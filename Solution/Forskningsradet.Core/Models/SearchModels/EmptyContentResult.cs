using System.Collections.Generic;
using System.Linq;
using EPiServer.Core;
using EPiServer.Find;
using EPiServer.Find.Api;
using EPiServer.Find.Cms;

namespace Forskningsradet.Core.Models.SearchModels
{
    public class EmptyContentResult<T> : ContentResult<T> where T : IContentData
    {
        public EmptyContentResult() : base(
            Enumerable.Empty<T>(),
            new SearchResults<ContentInLanguageReference>(
                new SearchResult<ContentInLanguageReference>
                {
                    Facets = new FacetResults(),
                    Hits = new HitCollection<ContentInLanguageReference>
                    {
                        Hits = new List<SearchHit<ContentInLanguageReference>>()
                    },
                    Shards = new Shards()
                }))
        {
        }
    }
}