using System.Collections.Generic;
using Forskningsradet.Core.Models.ContentModels.Pages;

namespace Forskningsradet.Core.Models.SearchModels
{
    public class EventFilterSearchResults : SearchResultBase<EventPage>
    {
        public IEnumerable<CategoryGroup> Categories { get; }

        public EventFilterSearchResults(
            IEnumerable<EventPage> results,
            IEnumerable<CategoryGroup> categories,
            int totalMatching
            ) : base(results, totalMatching) => Categories = categories;
    }
}