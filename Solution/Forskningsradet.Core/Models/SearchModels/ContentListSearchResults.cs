using System.Collections.Generic;
using Forskningsradet.Core.Models.ContentModels.Pages;

namespace Forskningsradet.Core.Models.SearchModels
{
    public class ContentListSearchResults : SearchResultBase<EditorialPage>
    {
        public IEnumerable<CategoryGroup> Categories { get; }

        public ContentListSearchResults(IEnumerable<EditorialPage> results, IEnumerable<CategoryGroup> categories, int totalMatching)
            : base(results, totalMatching) => Categories = categories;
    }
}