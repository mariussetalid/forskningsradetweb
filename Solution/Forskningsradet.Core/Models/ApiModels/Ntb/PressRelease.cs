using System;

namespace Forskningsradet.Core.Models.ApiModels.Ntb
{
    public class PressRelease
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public string Lead { get; set; }
        public string Body { get; set; }
        public DateTime Published { get; set; }
        public string ImageCaption { get; set; }
        public string ImageUrl { get; set; }
        public string ImageThumbnailUrl { get; set; }
    }
}