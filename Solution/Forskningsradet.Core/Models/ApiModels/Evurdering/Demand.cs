using Newtonsoft.Json;

namespace Forskningsradet.Core.Models.ApiModels.Evurdering
{
    public class Demand
    {
        [JsonProperty("kravId")]
        public long Id { get; set; }

        [JsonProperty("kravVersjonsnummer")]
        public int VersionNumber { get; set; }

        [JsonProperty("kravtype")]
        public string Type { get; set; }

        [JsonProperty("kravSprakkode")]
        public string LanguageCode { get; set; }

        [JsonProperty("kravSporsmal")]
        public string Question { get; set; }

        [JsonProperty("kravSvaralternativer")]
        public string[] Alternatives { get; set; }
    }
}