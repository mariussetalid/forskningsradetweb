using System;
using System.Collections.Generic;
using Forskningsradet.Core.Models.ApiModels.Evurdering.Converters;
using Newtonsoft.Json;

namespace Forskningsradet.Core.Models.ApiModels.Evurdering
{
    public class ApplicationType
    {
        [JsonProperty("utlystsoknadstypeId")]
        public long Id { get; set; }

        [JsonProperty("sprakkoder")]
        public string[] LanguageCodes { get; set; }

        [JsonProperty("maksLengdePaProsjektperiodeIMnd")]
        public int? MaxProjectLength { get; set; }

        [JsonProperty("minLengdePaProsjektperiodeIMnd")]
        public int? MinProjectLength { get; set; }

        [JsonProperty("minSoktBelopITusen")]
        public int? MinApplicationAmount { get; set; }

        [JsonProperty("maksSoktBelopITusen")]
        public int? MaxApplicationAmount { get; set; }

        [JsonProperty("tidligstStartDato")]
        [JsonConverter(typeof(EpochDateTimeConverter))]
        public DateTime? EarliestStartDate { get; set; }

        [JsonProperty("senesteProsjektperiodeStart")]
        [JsonConverter(typeof(EpochDateTimeConverter))]
        public DateTime? LatestProjectPeriodStart { get; set; }

        [JsonProperty("senesteProsjektperiodeSlutt")]
        [JsonConverter(typeof(EpochDateTimeConverter))]
        public DateTime? LatestProjectPeriodEnd { get; set; }

        [JsonProperty("soknadstypeNavn")]
        public string ApplicationName { get; set; }

        [JsonProperty("soknadstypeNavnEngelsk")]
        public string ApplicationNameEnglish { get; set; }

        [JsonProperty("soknadstypeId")]
        public long ApplicationId { get; set; }

        [JsonProperty("tema")]
        public List<Subject> Subjects { get; set; }

        [JsonProperty("hovedkriterier")]
        public MainCriterion[] MainCriteria { get; set; }
    }
}