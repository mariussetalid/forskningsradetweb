using System;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ApiModels.Evurdering.Converters;
using Newtonsoft.Json;

namespace Forskningsradet.Core.Models.ApiModels.Evurdering
{
    public class Proposal
    {
        [JsonProperty("utlysningId")]
        public long Id { get; set; }

        [JsonProperty("utlysningTittel")]
        public string Title { get; set; }

        [JsonProperty("utlysningTittelEngelsk")]
        public string EnglishTitle { get; set; }

        [JsonProperty("startdato")]
        [JsonConverter(typeof(EpochDateTimeConverter))]
        public DateTime? StartDate { get; set; }

        [JsonProperty("utlystBelop")]
        public double? Amount { get; set; }

        [JsonProperty("saksansvarligBrukernavn")]
        public string CaseResponsible { get; set; }

        [JsonProperty("soknadsfrist")]
        [JsonConverter(typeof(EpochDateTimeConverter))]
        public DateTime? ApplicationDeadline { get; set; }

        [JsonProperty("typefrist")]
        [JsonConverter(typeof(DeadlineTypeConverter))]
        public DeadlineType DeadlineType { get; set; }

        [JsonProperty("utlysningsstatus")]
        [JsonConverter(typeof(ProposalStateConverter))]
        public ProposalState Status { get; set; }

        [JsonProperty("rffUtlysning")]
        public bool IsRff { get; set; }

        [JsonProperty("skissekrav")]
        [JsonConverter(typeof(ProjectOutlineRequirementConverter))]
        public ProjectOutlineRequirement ProjectOutlineRequirement { get; set; }

        [JsonProperty("soknadstyper")]
        public ApplicationType[] ApplicationTypes { get; set; }

        public ApplicationType ApplicationType
            => ApplicationTypes?.Length > 0
                ? ApplicationTypes[0]
                : null;

        [JsonProperty("program")]
        public Program Program { get; set; }

        public string GetTitle(string culture) =>
            culture == LanguageConstants.NorwegianLanguageBranch && !string.IsNullOrEmpty(Title)
                ? Title
                : EnglishTitle;
    }
}