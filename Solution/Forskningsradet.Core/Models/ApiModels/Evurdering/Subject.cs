using System.Collections.Generic;
using Forskningsradet.Common.Constants;
using Newtonsoft.Json;

namespace Forskningsradet.Core.Models.ApiModels.Evurdering
{
    public class Subject
    {
        [JsonProperty("kode")]
        public string Code { get; set; }

        [JsonProperty("tittel")]
        public string Title { get; set; }

        [JsonProperty("navn")]
        public string NameNorwegian { get; set; }

        public string Name => NameNorwegian ?? Title;

        [JsonProperty("engelskNavn")]
        public string NameEnglish { get; set; }

        [JsonProperty("underTema")]
        public List<Subject> SubSubjects { get; set; }

        public string GetTranslatedName(string culture) =>
            culture == LanguageConstants.MasterLanguageBranch
            ? Name
            : NameEnglish;
    }
}