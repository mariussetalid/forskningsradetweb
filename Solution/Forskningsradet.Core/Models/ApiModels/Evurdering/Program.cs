using Newtonsoft.Json;

namespace Forskningsradet.Core.Models.ApiModels.Evurdering
{
    public class Program
    {
        [JsonProperty("programaktivitetId")]
        public int Id { get; set; }

        [JsonProperty("programaktivitetKortnavn")]
        public string ShortName { get; set; }

        [JsonProperty("programaktivitetTittel")]
        public string Title { get; set; }
    }
}