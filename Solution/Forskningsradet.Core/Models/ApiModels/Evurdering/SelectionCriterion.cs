using Newtonsoft.Json;

namespace Forskningsradet.Core.Models.ApiModels.Evurdering
{
    public class SelectionCriterion
    {
        [JsonProperty("seleksjonskritiereId")]
        public long Id { get; set; }

        [JsonProperty("seleksjonskriterieVersjonsnummer")]
        public int VersionNumber { get; set; }

        [JsonProperty("seleksjonskriterieType")]
        public string Type { get; set; }

        [JsonProperty("seleksjonskriterieSprakkode")]
        public string LanguageCode { get; set; }

        [JsonProperty("seleksjonskriterieSporsmal")]
        public string Question { get; set; }

        [JsonProperty("seleksjonskriterieSvaralternativer")]
        public string[] Alternatives { get; set; }
    }
}