using System;
using Newtonsoft.Json;

namespace Forskningsradet.Core.Models.ApiModels.Evurdering.Converters
{
    public class ProjectOutlineRequirementConverter : JsonConverter<ProjectOutlineRequirement>
    {
        public override void WriteJson(JsonWriter writer, ProjectOutlineRequirement value, JsonSerializer serializer) =>
            writer.WriteValue(value.ToString());

        public override ProjectOutlineRequirement ReadJson(JsonReader reader, Type objectType, ProjectOutlineRequirement existingValue, bool hasExistingValue, JsonSerializer serializer)
        {
            switch (reader.Value)
            {
                case null:
                case "INGEN":
                    return ProjectOutlineRequirement.None;
                case "IKKE_OBLIGATORISK":
                    return ProjectOutlineRequirement.NonMandatory;
                case "OBLIGATORISK":
                    return ProjectOutlineRequirement.Mandatory;
                default:
                    throw new NotSupportedException($"'{reader.Value}' is not a valid draft requirement type.");
            }
        }
    }
}