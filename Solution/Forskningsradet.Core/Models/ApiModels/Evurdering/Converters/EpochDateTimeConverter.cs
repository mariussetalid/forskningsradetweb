using System;
using Forskningsradet.Core.Extensions;
using Newtonsoft.Json;

namespace Forskningsradet.Core.Models.ApiModels.Evurdering.Converters
{
    public class EpochDateTimeConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
            => objectType == typeof(DateTime);

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (!long.TryParse(reader.Value?.ToString(), out var value))
                return null;

            return value.FromEpochDateTime();
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            if (value is DateTime date)
                writer.WriteRawValue(date.ToEpochDateTime().ToString());
        }
    }
}