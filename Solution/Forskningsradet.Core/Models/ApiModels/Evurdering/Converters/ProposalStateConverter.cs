using System;
using Newtonsoft.Json;

namespace Forskningsradet.Core.Models.ApiModels.Evurdering.Converters
{
    public class ProposalStateConverter : JsonConverter<ProposalState>
    {
        public override void WriteJson(JsonWriter writer, ProposalState value, JsonSerializer serializer) => writer.WriteValue(value.ToString());

        public override ProposalState ReadJson(JsonReader reader, Type objectType, ProposalState existingValue, bool hasExistingValue, JsonSerializer serializer)
        {
            switch (reader.Value)
            {
                case "PLANLAGT":
                    return ProposalState.Planned;
                case "AKTIV":
                    return ProposalState.Active;
                case "KANSELLERT":
                    return ProposalState.Cancelled;
                case "SLETTET":
                    return ProposalState.Deleted;
                case "GJENNOMFORT":
                    return ProposalState.Completed;
                default:
                    throw new NotSupportedException("Provided state is not supported.");
            }
        }
    }
}