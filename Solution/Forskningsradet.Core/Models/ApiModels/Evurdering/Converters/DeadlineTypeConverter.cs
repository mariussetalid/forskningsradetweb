using System;
using Newtonsoft.Json;

namespace Forskningsradet.Core.Models.ApiModels.Evurdering.Converters
{
    public class DeadlineTypeConverter : JsonConverter<DeadlineType>
    {
        public override void WriteJson(JsonWriter writer, DeadlineType value, JsonSerializer serializer) => writer.WriteValue(value.ToString());

        public override DeadlineType ReadJson(JsonReader reader, Type objectType, DeadlineType existingValue, bool hasExistingValue, JsonSerializer serializer)
        {
            switch (reader.Value)
            {
                case "DATO":
                    return DeadlineType.Date;
                case "LOPENDE":
                    return DeadlineType.Continuous;
                case "IKKE_ANGITT":
                    return DeadlineType.NotSet;
                default:
                    throw new NotSupportedException("Provided deadline type is not supported.");
            }
        }
    }
}