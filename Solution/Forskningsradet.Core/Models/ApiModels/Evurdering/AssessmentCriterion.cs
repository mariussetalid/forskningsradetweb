using Newtonsoft.Json;

namespace Forskningsradet.Core.Models.ApiModels.Evurdering
{
    public class AssessmentCriterion
    {
        [JsonProperty("vurderingspunktSporsmal")]
        public string Question { get; set; }

        [JsonProperty("vurderingspunktSvaralternativer")]
        public string[] Alternatives { get; set; }
    }
}