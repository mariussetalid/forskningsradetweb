namespace Forskningsradet.Core.Models.ApiModels.Evurdering
{
    public enum ProjectOutlineRequirement
    {
        None,
        NonMandatory,
        Mandatory
    }
}