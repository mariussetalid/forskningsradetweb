using Forskningsradet.Core.Models.Contracts;

namespace Forskningsradet.Core.Models.ApiModels.Evurdering
{
    public class TranslatedSubject : IBlockDataSource
    {
        public string Id { get; set; }
        public string Identity => Id;
        public string Title { get; set; }
        public string SubjectChildren { get; set; }
    }
}