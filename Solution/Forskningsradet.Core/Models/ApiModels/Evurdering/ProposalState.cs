namespace Forskningsradet.Core.Models.ApiModels.Evurdering
{
    public enum ProposalState
    {
        Planned = 0,
        Active = 1,
        Cancelled = 2,
        Completed = 3,
        Deleted = 4
    }
}