using Newtonsoft.Json;

namespace Forskningsradet.Core.Models.ApiModels.Evurdering
{
    public class MainCriterionAlternative
    {
        [JsonProperty("svaralternativKarakter")]
        public string Grade { get; set; }

        [JsonProperty("svaralternativForankringstekst")]
        public string ApprovalText { get; set; }
    }
}