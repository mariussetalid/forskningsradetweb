using Forskningsradet.Core.Models.Contracts;
using Newtonsoft.Json;

namespace Forskningsradet.Core.Models.ApiModels.Evurdering
{
    public class MainCriterion : IBlockDataSource
    {
        [JsonProperty("hovedkriterieId")]
        public long Id { get; set; }

        public string Identity => Id.ToString();

        [JsonProperty("hovedkriterieVersjonsummer")]
        public int VersionNumber { get; set; }

        [JsonProperty("hovedkriterietype")]
        public string Type { get; set; }

        [JsonProperty("hovedkriterieSprakkode")]
        public string LanguageCode { get; set; }

        [JsonProperty("hovedkriterieTittel")]
        public string Title { get; set; }

        [JsonProperty("hovedkriterieSporsmal")]
        public string Question { get; set; }

        [JsonProperty("hovedkriterieBeskrivelse")]
        public string Description { get; set; }

        [JsonProperty("hovedkriterieSvaralternativer")]
        public MainCriterionAlternative[] MainCriterionAlternatives { get; set; }

        [JsonProperty("hovedkriterieVurderingspunkter")]
        public AssessmentCriterion[] AssessmentCriteria { get; set; }
    }
}