using System;
using Destructurama.Attributed;
using Newtonsoft.Json;

namespace Forskningsradet.Core.Models.ApiModels.Idm
{
    public class IdmContactPerson
    {
        [JsonProperty("id")]
        public Guid? Id { get; set; }

        [JsonProperty("resourceId")]
        public string ResourceId { get; set; }

        [JsonProperty("givenName")]
        public string FirstName { get; set; }

        [NotLogged]
        [JsonProperty("sn")]
        public string LastName { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("departmentName")]
        public string DepartmentName { get; set; }

        [JsonProperty("resourceType")]
        public string ResourceType { get; set; }

        [JsonProperty("agressoLastUpdate")]
        public string AgressoLastUpdate { get; set; }

        [NotLogged]
        [JsonProperty("mobile")]
        public string Mobile { get; set; }

        [NotLogged]
        [JsonProperty("telephoneNumber")]
        public string Phone { get; set; }

        [NotLogged]
        [JsonProperty("mail")]
        public string Email { get; set; }
    }
}