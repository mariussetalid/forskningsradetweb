using System;

namespace Forskningsradet.Core.Models.ApiModels.Idm
{
    public class IdmContactPersonId
    {
        public Guid? Guid { get; }
        public int? ResourceId { get; }
        public bool IsValid => ResourceId != null || Guid != null;
        public bool HasResourceId => ResourceId != null;

        public IdmContactPersonId(string id)
        {
            if (int.TryParse(id, out int resourceId) && resourceId > 0)
            {
                ResourceId = resourceId;
            }
            else if (System.Guid.TryParse(id, out var guid) && guid != System.Guid.Empty)
            {
                Guid = guid;
            }
        }
    }
}