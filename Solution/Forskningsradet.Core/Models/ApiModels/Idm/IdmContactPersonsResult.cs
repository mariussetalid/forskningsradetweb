using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace Forskningsradet.Core.Models.ApiModels.Idm
{
    public class IdmContactPersonsResult
    {
        public IdmContactPersonsResult(IEnumerable<IdmContactPerson> contactPersons) => Result = contactPersons;

        [JsonProperty("result")]
        public IEnumerable<IdmContactPerson> Result { get; set; }

        [JsonProperty("resultCount")]
        public int ResultCount => Result.Count();

        [JsonProperty("pagedResultsCookie")]
        public object PagedResultsCookie => null;

        [JsonProperty("totalPagedResultsPolicy")]
        public string TotalPagedResultsPolicy => "NONE";

        [JsonProperty("totalPagedResults")]
        public int TotalPagedResults => -1;

        [JsonProperty("remainingPagedResults")]
        public int RemainingPagedResults => -1;
    }
}