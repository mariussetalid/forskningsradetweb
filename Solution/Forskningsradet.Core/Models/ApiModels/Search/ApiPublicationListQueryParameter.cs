﻿using Forskningsradet.Core.Models.QueryParameters;
using Newtonsoft.Json;

namespace Forskningsradet.Core.Models.ApiModels.Search
{
    public class ApiPublicationListQueryParameter
    {
        public int Year { get; set; }
        [JsonProperty("type")]
        public int[] Types { get; set; }
        [JsonProperty("q")]
        public string Query { get; set; }

        public PublicationListQueryParameter ToPublicationListQueryParameter() =>
            new PublicationListQueryParameter
            {
                Year = Year,
                Types = Types,
                Query = Query
            };
    }
}
