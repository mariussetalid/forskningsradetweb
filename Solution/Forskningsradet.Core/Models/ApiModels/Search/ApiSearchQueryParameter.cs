﻿using Forskningsradet.Core.Models.QueryParameters;
using Newtonsoft.Json;

namespace Forskningsradet.Core.Models.ApiModels.Search
{
    public class ApiSearchQueryParameter
    {
        public string[] Subjects { get; set; }
        public string[] TargetGroups { get; set; }
        [JsonProperty("type")]
        public string[] Types { get; set; }
        public string[] Categories { get; set; }
        [JsonProperty("q")]
        public string Query { get; set; }
        public int? From { get; set; }
        public int? To { get; set; }
        public int? Page { get; set; }

        public SearchQueryParameter ToSearchQueryParameter() 
            => new SearchQueryParameter(Subjects, TargetGroups, Types, Categories, Query, From, To, Page);
    }
}
