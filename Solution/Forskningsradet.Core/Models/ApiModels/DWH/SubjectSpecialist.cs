using Newtonsoft.Json;

namespace Forskningsradet.Core.Models.ApiModels.DWH
{
    public class SubjectSpecialist
    {
        [JsonProperty("navn")]
        public string Name { get; set; }

        [JsonProperty("land")]
        public string Country { get; set; }
    }
}