using Newtonsoft.Json;

namespace Forskningsradet.Core.Models.ApiModels.DWH
{
    public class Application
    {
        [JsonProperty("prosjektnummer")]
        public long ProjectNumber { get; set; }

        [JsonProperty("prosjekttittel")]
        public string ProjectTitle { get; set; }

        [JsonProperty("organisasjon")]
        public string Organization { get; set; }
    }
}