using Newtonsoft.Json;

namespace Forskningsradet.Core.Models.ApiModels.DWH
{
    public class SoknadsTyper
    {
        [JsonProperty("soknadstypeNavn")]
        public string SoknadstypeNavn { get; set; }

        [JsonProperty("antall")]
        public long Antall { get; set; }
    }
}