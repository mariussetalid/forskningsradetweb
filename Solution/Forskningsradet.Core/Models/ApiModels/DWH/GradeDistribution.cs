using Newtonsoft.Json;

namespace Forskningsradet.Core.Models.ApiModels.DWH
{
    public class GradeDistribution
    {
        [JsonProperty("karakter")]
        public long Grade { get; set; }

        [JsonProperty("antall")]
        public long Count { get; set; }
    }
}