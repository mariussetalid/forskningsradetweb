using Newtonsoft.Json;

namespace Forskningsradet.Core.Models.ApiModels.DWH
{
    public class ApplicationResult
    {
        [JsonProperty("utlysningId")]
        public long ProposalId { get; set; }

        [JsonProperty("utlysningsTittel")]
        public string ProposalName { get; set; }

        [JsonProperty("sokteMidler")]
        public double AppliedAmount { get; set; }

        [JsonProperty("antallMottatteSoknader")]
        public long ReceivedApplications { get; set; }

        [JsonProperty("antallAvisteSoknader")]
        public long RejectedApplications { get; set; }

        [JsonProperty("tildelteMidler")]
        public double AwardedAmount { get; set; }

        [JsonProperty("fageksperter")]
        public SubjectSpecialist[] SubjectSpecialists { get; set; }

        [JsonProperty("soknader")]
        public Application[] Applications { get; set; }

        [JsonProperty("hovedkarakterfordelinger")]
        public GradeDistribution[] GradeDistributions { get; set; }
    }
}