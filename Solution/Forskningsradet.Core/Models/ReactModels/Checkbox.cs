using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class Checkbox : ReactComponent
    {
        public bool Checked { get; set; }
        [Required]
        public string Label { get; set; }
        [Required]
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
