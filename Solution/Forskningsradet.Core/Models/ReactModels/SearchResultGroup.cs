using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class SearchResultGroup : ReactComponent
    {
        public string HtmlId { get; set; }
        public ContentArea Results { get; set; }
        public string Title { get; set; }
    }
}
