using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class Vacancies : ReactComponent
    {
        public EmptyList EmptyList { get; set; }
        public IList<DateCard> Items { get; set; }
        public Link Link { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }
    }
}
