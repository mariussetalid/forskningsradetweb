using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class QuotesBlock : ReactComponent
    {
        public string Title { get; set; }
        public IList<QuotesBlock_Quotes> Quotes { get; set; }
    }
    
    public class QuotesBlock_Quotes
    {
        public Image Image { get; set; }
        public string Quotee { get; set; }
        public string Text { get; set; }
        public bool AddQuoteDash { get; set; }
    }
}
