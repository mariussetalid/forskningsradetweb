using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class OptionsModal : ReactComponent
    {
        public ShareOptions ShareContent { get; set; }
        public DownloadList DownloadContent { get; set; }
        public string OpenButtonText { get; set; }
        public string CloseButtonLabel { get; set; }
    }
}
