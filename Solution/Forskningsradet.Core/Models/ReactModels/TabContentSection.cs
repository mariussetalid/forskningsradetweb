using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class TabContentSection : ReactComponent
    {
        public ContentArea Content { get; set; }
        public RichTextBlock ContentDescription { get; set; }
        public DownloadList DownloadList { get; set; }
        public RichText Ingress { get; set; }
        public ContentArea MoreContent { get; set; }
        public IList<RichTextBlock> MoreTexts { get; set; }
        public string Title { get; set; }
        public IList<RichTextBlock> Texts { get; set; }
        public Process Process { get; set; }
        public string HtmlId { get; set; }
        public TabContentSection_Application Application { get; set; }
    }
    
    public class TabContentSection_Application
    {
        public string Title { get; set; }
        public string Text { get; set; }
        public Link Link { get; set; }
    }
}
