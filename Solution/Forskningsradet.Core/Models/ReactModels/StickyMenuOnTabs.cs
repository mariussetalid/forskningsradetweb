using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class StickyMenuOnTabs : ReactComponent
    {
        public IList<StickyMenuOnTabs_NavGroups> NavGroups { get; set; }
        public string Title { get; set; }
    }
    
    public class StickyMenuOnTabs_NavGroups
    {
        public Link TitleLink { get; set; }
        public StickyMenuOnTabs_NavGroups_Links Links { get; set; }
    }
    
    public class StickyMenuOnTabs_NavGroups_Links
    {
        [Required]
        public IList<StickyMenuOnTabsItem> Items { get; set; }
    }
}
