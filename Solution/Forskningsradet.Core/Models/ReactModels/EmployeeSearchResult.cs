using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class EmployeeSearchResult : ReactComponent
    {
        public IList<EmployeeSearchResult_LabeledLinks> LabeledLinks { get; set; }
        public string SubTitle { get; set; }
        public IList<string> Texts { get; set; }
        [Required]
        public string Title { get; set; }
    }
    
    public class EmployeeSearchResult_LabeledLinks
    {
        public string Label { get; set; }
        public Link Link { get; set; }
    }
}
