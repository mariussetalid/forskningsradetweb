using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class Quote : ReactComponent
    {
        public Quote_OnPageEditing OnPageEditing { get; set; }
        public string Text { get; set; }
        public string QuoteBy { get; set; }
    }
    
    public class Quote_OnPageEditing
    {
        public string Text { get; set; }
        public string QuoteBy { get; set; }
    }
}
