using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class SocialMediaLink : ReactComponent
    {
        public SocialMediaLink_Provider Provider { get; set; }
        public string Url { get; set; }
        public string Text { get; set; }
    }
    
    public enum SocialMediaLink_Provider
    {
        [EnumMember(Value = "")]
        None = 0,
        [EnumMember(Value = "facebook")]
        Facebook = 1,
        [EnumMember(Value = "linkedin")]
        Linkedin = 2,
        [EnumMember(Value = "twitter")]
        Twitter = 3,
        [EnumMember(Value = "rss")]
        Rss = 4,
    }
}
