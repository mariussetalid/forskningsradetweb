using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class FilterGroup : ReactComponent
    {
        public Accordion Accordion { get; set; }
        public IList<FilterGroup_Options> Options { get; set; }
        public string Title { get; set; }
    }
    
    public class FilterGroup_Options
    {
        public bool Checked { get; set; }
        public string Name { get; set; }
        public string Label { get; set; }
        public string Value { get; set; }
        public IList<Checkbox> SubOptions { get; set; }
    }
}
