using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class TagLinkList : ReactComponent
    {
        public bool LeftAligned { get; set; }
        public IList<TagLinkList_Tags> Tags { get; set; }
    }
    
    public class TagLinkList_Tags
    {
        public Link Link { get; set; }
        public bool Inactive { get; set; }
    }
}
