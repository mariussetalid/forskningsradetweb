using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class NavigationGroup : ReactComponent
    {
        public IList<NavigationGroup_Items> Items { get; set; }
        [Required]
        public string Title { get; set; }
    }
    
    public class NavigationGroup_Items
    {
        public Link Link { get; set; }
        public NavigationList LinkList { get; set; }
    }
}
