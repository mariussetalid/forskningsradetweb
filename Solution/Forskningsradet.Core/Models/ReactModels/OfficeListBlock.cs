using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class OfficeListBlock : ReactComponent
    {
        public IList<OfficeListBlock_Links> Links { get; set; }
        public ContentArea Offices { get; set; }
    }
    
    public class OfficeListBlock_Links
    {
        [Required]
        public string Id { get; set; }
        public bool IsActive { get; set; }
        [Required]
        public Link Link { get; set; }
    }
}
