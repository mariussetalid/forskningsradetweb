using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class ResponsiveIframe : ReactComponent
    {
        [Required]
        public string Src { get; set; }
        [Required]
        public string Title { get; set; }
    }
}
