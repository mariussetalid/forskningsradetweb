using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class SearchResult : ReactComponent
    {
        public string Text { get; set; }
        public string Title { get; set; }
        public Metadata Metadata { get; set; }
        public string Url { get; set; }
        public string TrackUrl { get; set; }
        public FluidImage Image { get; set; }
        public DocumentIcon Icon { get; set; }
        public IList<DateCardStatus> StatusList { get; set; }
        public DateCardTags ThemeTags { get; set; }
        public string DescriptiveTitle { get; set; }
    }
}
