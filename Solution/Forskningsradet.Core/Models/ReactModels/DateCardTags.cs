using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class DateCardTags : ReactComponent
    {
        public Accordion Accordion { get; set; }
        public IList<Link> Items { get; set; }
        public int NumberOfVisibleItems { get; set; }
    }
}
