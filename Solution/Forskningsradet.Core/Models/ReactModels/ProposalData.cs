using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class ProposalData : ReactComponent
    {
        public IList<ProposalData_Items> Items { get; set; }
    }
    
    public class ProposalData_Items
    {
        [Required]
        public string Label { get; set; }
        public string Text { get; set; }
        public IList<Link> Links { get; set; }
    }
}
