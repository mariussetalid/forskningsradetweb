using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class PageFooter : ReactComponent
    {
        public Byline Byline { get; set; }
        public Link Download { get; set; }
        public OptionsModal Share { get; set; }
    }
}
