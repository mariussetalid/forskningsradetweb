using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class ShareOptions : ReactComponent
    {
        public IList<ShareOptions_Items> Items { get; set; }
    }
    
    public class ShareOptions_Items
    {
        public string Text { get; set; }
        public string Url { get; set; }
        public ShareOptions_Items_Icon Icon { get; set; }
    }
    
    public enum ShareOptions_Items_Icon
    {
        [EnumMember(Value = "")]
        None = 0,
        [EnumMember(Value = "facebook")]
        Facebook = 1,
        [EnumMember(Value = "linkedin")]
        Linkedin = 2,
        [EnumMember(Value = "twitter")]
        Twitter = 3,
        [EnumMember(Value = "mail")]
        Mail = 4,
    }
}
