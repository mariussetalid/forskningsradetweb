using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class MenuSearch : ReactComponent
    {
        public Form Form { get; set; }
        public SearchInput Input { get; set; }
        public string SubmitLabel { get; set; }
    }
}
