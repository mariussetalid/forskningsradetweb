using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class CampaignBlock : ReactComponent
    {
        public Link Cta { get; set; }
        public CampaignBlock_EditorTheme EditorTheme { get; set; }
        public FluidImage Image { get; set; }
        public CampaignBlock_OnPageEditing OnPageEditing { get; set; }
        [Required]
        public string Title { get; set; }
    }
    
    public enum CampaignBlock_EditorTheme
    {
        [EnumMember(Value = "")]
        None = 0,
        [EnumMember(Value = "-theme-dark-blue")]
        DarkBlue = 1,
    }
    
    public class CampaignBlock_OnPageEditing
    {
        public string Title { get; set; }
    }
}
