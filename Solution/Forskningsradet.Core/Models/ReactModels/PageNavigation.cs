using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class PageNavigation : ReactComponent
    {
        public Link Previous { get; set; }
        public Link Next { get; set; }
    }
}
