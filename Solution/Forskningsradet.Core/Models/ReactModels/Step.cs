using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class Step : ReactComponent
    {
        public string Text { get; set; }
        public bool IsActive { get; set; }
    }
}
