using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class MediaBlock : ReactComponent
    {
        public string Title { get; set; }
        public MediaBlock_OnPageEditing OnPageEditing { get; set; }
        public YoutubeVideo YouTubeVideo { get; set; }
        public EmbedBlock Embed { get; set; }
        public FluidImage Image { get; set; }
        public string Url { get; set; }
    }
    
    public class MediaBlock_OnPageEditing
    {
        public string Title { get; set; }
    }
}
