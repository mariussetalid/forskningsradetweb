using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class SearchPage : ReactComponent
    {
        public EmptyList EmptyList { get; set; }
        public FilterLayout FilterLayout { get; set; }
        public Form Form { get; set; }
        public Pagination Pagination { get; set; }
        public IList<SearchResult> Results { get; set; }
        public string ResultDescription { get; set; }
        public Search Search { get; set; }
        public string Title { get; set; }
        public string FetchFilteredResultsEndpoint { get; set; }
    }
}
