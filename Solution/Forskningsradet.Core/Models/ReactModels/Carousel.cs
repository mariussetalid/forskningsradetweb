using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class Carousel : ReactComponent
    {
        public string NextLabel { get; set; }
        public string PreviousLabel { get; set; }
    }
}
