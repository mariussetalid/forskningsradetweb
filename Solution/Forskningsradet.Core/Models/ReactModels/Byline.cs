using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class Byline : ReactComponent
    {
        public IList<Byline_Items> Items { get; set; }
    }
    
    public class Byline_Items
    {
        public string OnPageEditing { get; set; }
        public string Text { get; set; }
    }
}
