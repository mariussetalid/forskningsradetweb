using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class Tabs : ReactComponent
    {
        public string ActiveTab { get; set; }
        public IList<Tabs_Items> Items { get; set; }
        public bool StickyMenuOnTabs { get; set; }
    }
    
    public class Tabs_Items
    {
        [Required]
        public string Guid { get; set; }
        public TabContent Content { get; set; }
        [Required]
        public string Name { get; set; }
    }
}
