using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class AccordionWithContentAreaList : ReactComponent
    {
        public Accordion Accordion { get; set; }
        public IList<AccordionWithContentArea> Content { get; set; }
        public string Description { get; set; }
        public string HtmlId { get; set; }
        public AccordionWithContentAreaList_OnPageEditing OnPageEditing { get; set; }
        public RichText RichText { get; set; }
        public string Title { get; set; }
        public IList<string> ThemeTags { get; set; }
        public string Text { get; set; }
    }
    
    public class AccordionWithContentAreaList_OnPageEditing
    {
        public string Title { get; set; }
    }
}
