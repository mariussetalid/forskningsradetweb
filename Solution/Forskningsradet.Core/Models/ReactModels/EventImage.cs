using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class EventImage : ReactComponent
    {
        public FluidImage Image { get; set; }
        public EventImage_Background Background { get; set; }
    }
    
    public enum EventImage_Background
    {
        [EnumMember(Value = "")]
        None = 0,
        [EnumMember(Value = "background1")]
        Background1 = 1,
        [EnumMember(Value = "background2")]
        Background2 = 2,
        [EnumMember(Value = "background3")]
        Background3 = 3,
    }
}
