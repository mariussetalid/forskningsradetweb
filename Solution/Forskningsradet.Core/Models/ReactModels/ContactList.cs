using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class ContactList : ReactComponent
    {
        public IList<ContactListItem> ContactGroups { get; set; }
    }
}
