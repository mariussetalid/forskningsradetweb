using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class FilterLayout : ReactComponent
    {
        public ContentArea ContentArea { get; set; }
        public Filters Filters { get; set; }
        public bool IsLeft { get; set; }
    }
}
