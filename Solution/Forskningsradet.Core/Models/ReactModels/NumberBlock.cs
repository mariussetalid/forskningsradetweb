using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class NumberBlock : ReactComponent
    {
        public string Description { get; set; }
        public string Number { get; set; }
        public NumberBlock_OnPageEditing OnPageEditing { get; set; }
        public string Text { get; set; }
        public string Url { get; set; }
    }
    
    public class NumberBlock_OnPageEditing
    {
        public string Description { get; set; }
        public string Number { get; set; }
        public string Text { get; set; }
        public string Url { get; set; }
    }
}
