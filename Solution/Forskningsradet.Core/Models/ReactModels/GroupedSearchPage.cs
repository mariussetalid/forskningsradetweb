using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class GroupedSearchPage : ReactComponent
    {
        public EmptyList EmptyList { get; set; }
        public FilterLayout FilterLayout { get; set; }
        public Form Form { get; set; }
        public IList<GroupedSearchPageLink> Navigation { get; set; }
        public Pagination Pagination { get; set; }
        public IList<SearchResultGroup> Results { get; set; }
        public string ResultsDescription { get; set; }
        public Search Search { get; set; }
        public string Title { get; set; }
        public string FetchFilteredResultsEndpoint { get; set; }
    }
}
