using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class StickyMenu : ReactComponent
    {
        public Accordion Accordion { get; set; }
        public IList<StickyMenu_NavGroups> NavGroups { get; set; }
        public string Title { get; set; }
    }
    
    public class StickyMenu_NavGroups
    {
        public Link TitleLink { get; set; }
        public StickyMenu_NavGroups_Links Links { get; set; }
    }
    
    public class StickyMenu_NavGroups_Links
    {
        [Required]
        public IList<StickyMenuItem> Items { get; set; }
    }
}
