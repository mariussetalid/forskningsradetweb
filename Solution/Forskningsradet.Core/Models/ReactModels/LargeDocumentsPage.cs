using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class LargeDocumentsPage : ReactComponent
    {
        public RichText Content { get; set; }
        public PageFooter Footer { get; set; }
        public ArticleHeader Header { get; set; }
        public string Ingress { get; set; }
        public PageNavigation PageNavigation { get; set; }
        public TableOfContents TableOfContents { get; set; }
        public string Title { get; set; }
    }
}
