using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class ProposalGroup : ReactComponent
    {
        public Accordion Accordion { get; set; }
        public Accordion HeaderAccordion { get; set; }
        public int NumberOfVisibleItems { get; set; }
        public IList<DateCard> Proposals { get; set; }
        public string Subtitle { get; set; }
        public RichText Text { get; set; }
        public string Title { get; set; }
    }
}
