using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class LinkListBlock : ReactComponent
    {
        public string HtmlId { get; set; }
        public LinkList Links { get; set; }
        public string Title { get; set; }
    }
}
