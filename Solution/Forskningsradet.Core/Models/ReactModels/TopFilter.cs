using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class TopFilter : ReactComponent
    {
        public IList<TopFilter_Options> Options { get; set; }
        public string Title { get; set; }
        public TopFilter_Timeframe Timeframe { get; set; }
    }
    
    public class TopFilter_Options
    {
        public bool IsCurrent { get; set; }
        public Link Link { get; set; }
    }
    
    public class TopFilter_Timeframe
    {
        public int Value { get; set; }
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
