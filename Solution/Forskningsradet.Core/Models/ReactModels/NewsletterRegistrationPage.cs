using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class NewsletterRegistrationPage : ReactComponent
    {
        public IList<CheckboxGroup> CheckboxGroups { get; set; }
        public PageFooter Footer { get; set; }
        public Form Form { get; set; }
        public string InputTitle { get; set; }
        public IList<InputEmail> InputFields { get; set; }
        public RichText RichText { get; set; }
        public ContentArea Sidebar { get; set; }
        public Button SubmitButton { get; set; }
        public string Title { get; set; }
        public Message Message { get; set; }
        public string HtmlString { get; set; }
    }
}
