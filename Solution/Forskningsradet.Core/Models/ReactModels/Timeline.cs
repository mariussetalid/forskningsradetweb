using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class Timeline : ReactComponent
    {
        public Timeline_EditorTheme EditorTheme { get; set; }
        public Timeline_Labels Labels { get; set; }
        public IList<TimelineItem> Items { get; set; }
        public int? StartIndex { get; set; }
        public string Title { get; set; }
    }
    
    public enum Timeline_EditorTheme
    {
        [EnumMember(Value = "")]
        None = 0,
        [EnumMember(Value = "-theme-gray")]
        Gray = 1,
    }
    
    public class Timeline_Labels
    {
        public string Next { get; set; }
        public string Previous { get; set; }
    }
}
