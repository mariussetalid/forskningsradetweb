using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class ProcessCarousel : ReactComponent
    {
        public IList<ProcessItem> Items { get; set; }
    }
}
