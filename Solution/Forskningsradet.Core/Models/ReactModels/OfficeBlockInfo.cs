using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class OfficeBlockInfo : ReactComponent
    {
        public Link Link { get; set; }
        public string MoreText { get; set; }
        public string Text { get; set; }
        [Required]
        public string Title { get; set; }
    }
}
