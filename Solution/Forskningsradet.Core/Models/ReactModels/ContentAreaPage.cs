using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class ContentAreaPage : ReactComponent
    {
        public ContentArea Content { get; set; }
        public PageHeader PageHeader { get; set; }
    }
}
