using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class ImageWithCaption : ReactComponent
    {
        public string Caption { get; set; }
        public ImageWithCaption_EditorTheme EditorTheme { get; set; }
        public Image Image { get; set; }
        public ImageWithCaption_OnPageEditing OnPageEditing { get; set; }
        public string Title { get; set; }
    }
    
    public enum ImageWithCaption_EditorTheme
    {
        [EnumMember(Value = "")]
        None = 0,
        [EnumMember(Value = "-theme-half-width")]
        HalfWidth = 1,
    }
    
    public class ImageWithCaption_OnPageEditing
    {
        public string Caption { get; set; }
        public string Title { get; set; }
    }
}
