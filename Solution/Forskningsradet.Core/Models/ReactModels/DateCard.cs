using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class DateCard : ReactComponent
    {
        public DateCardDates DateContainer { get; set; }
        public IList<Message> MessageList { get; set; }
        public Metadata Metadata { get; set; }
        [Required]
        public string Id { get; set; }
        public DateCardMedia Media { get; set; }
        public DateCardStatus Status { get; set; }
        public string Subtitle { get; set; }
        public DateCardTags Tags { get; set; }
        public string Text { get; set; }
        public string Title { get; set; }
        public string Url { get; set; }
        public bool UsedInSidebar { get; set; }
    }
}
