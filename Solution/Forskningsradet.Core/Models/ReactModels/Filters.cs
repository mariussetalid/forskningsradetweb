using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class Filters : ReactComponent
    {
        public IList<Checkbox> Checkboxes { get; set; }
        public IList<FilterGroup> Items { get; set; }
        public Filters_Labels Labels { get; set; }
        public RangeSlider RangeFilter { get; set; }
        public string Title { get; set; }
        public string MobileTitle { get; set; }
    }
    
    public class Filters_Labels
    {
        public string ShowResults { get; set; }
        public string Reset { get; set; }
    }
}
