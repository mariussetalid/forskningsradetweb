using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class AccordionWithContentArea : ReactComponent
    {
        public ContentArea Content { get; set; }
        public Accordion Accordion { get; set; }
        public AccordionWithContentArea_OnPageEditing OnPageEditing { get; set; }
        public string Title { get; set; }
        public IconWarning IconWarning { get; set; }
        public OptionsModal Share { get; set; }
        public string HtmlId { get; set; }
        public bool InitiallyOpen { get; set; }
    }
    
    public class AccordionWithContentArea_OnPageEditing
    {
        public string Title { get; set; }
    }
}
