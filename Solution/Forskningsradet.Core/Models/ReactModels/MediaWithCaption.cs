using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class MediaWithCaption : ReactComponent
    {
        public string Caption { get; set; }
        public MediaWithCaption_OnPageEditing OnPageEditing { get; set; }
        public string Title { get; set; }
        public YoutubeVideo YouTubeVideo { get; set; }
        public EmbedBlock Embed { get; set; }
        public Image Image { get; set; }
        public string Url { get; set; }
    }
    
    public class MediaWithCaption_OnPageEditing
    {
        public string Caption { get; set; }
        public string Title { get; set; }
    }
}
