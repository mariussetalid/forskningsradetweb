using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class TagList : ReactComponent
    {
        public IList<TagList_Tags> Tags { get; set; }
    }
    
    public class TagList_Tags
    {
        public string Text { get; set; }
    }
}
