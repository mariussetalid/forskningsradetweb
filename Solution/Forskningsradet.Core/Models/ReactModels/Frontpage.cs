using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class Frontpage : ReactComponent
    {
        public ContentArea Content { get; set; }
        public FrontpageHeader Header { get; set; }
        public LinkList LinkList { get; set; }
    }
}
