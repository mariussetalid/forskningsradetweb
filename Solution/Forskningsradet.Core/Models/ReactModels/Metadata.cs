using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class Metadata : ReactComponent
    {
        public IList<Metadata_Items> Items { get; set; }
    }
    
    public class Metadata_Items
    {
        public DocumentIcon Icon { get; set; }
        public string Label { get; set; }
        public string Text { get; set; }
        public string Url { get; set; }
        public bool IsDisabled { get; set; }
    }
}
