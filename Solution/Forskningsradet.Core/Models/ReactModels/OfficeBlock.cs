using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class OfficeBlock : ReactComponent
    {
        public IList<OfficeBlockInfo> Info { get; set; }
        public string Title { get; set; }
    }
}
