using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class EmbedBlock : ReactComponent
    {
        public int? Height { get; set; }
        public string OnPageEditing { get; set; }
        [Required]
        public string Src { get; set; }
        [Required]
        public string Title { get; set; }
        public int? Width { get; set; }
    }
}
