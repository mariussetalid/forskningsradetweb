using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class AccordionBlock : ReactComponent
    {
        public Accordion Accordion { get; set; }
        public AccordionBlock_OnPageEditing OnPageEditing { get; set; }
        public string Title { get; set; }
        public RichText Text { get; set; }
    }
    
    public class AccordionBlock_OnPageEditing
    {
        public string Title { get; set; }
    }
}
