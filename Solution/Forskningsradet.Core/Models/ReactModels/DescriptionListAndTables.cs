using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class DescriptionListAndTables : ReactComponent
    {
        public DescriptionListBlock DescriptionList { get; set; }
        public IList<TableBlock> Tables { get; set; }
    }
}
