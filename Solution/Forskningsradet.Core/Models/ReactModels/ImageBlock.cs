using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class ImageBlock : ReactComponent
    {
        public Image Image { get; set; }
        public ImageBlock_OnPageEditing OnPageEditing { get; set; }
        public string Title { get; set; }
    }
    
    public class ImageBlock_OnPageEditing
    {
        public string Title { get; set; }
    }
}
