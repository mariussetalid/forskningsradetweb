using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class GroupedSearchPageLink : ReactComponent
    {
        public bool IsCurrent { get; set; }
        public Link Link { get; set; }
    }
}
