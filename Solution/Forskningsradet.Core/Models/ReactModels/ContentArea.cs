using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class ContentArea : ReactComponent
    {
        public IList<ContentAreaItem> Blocks { get; set; }
        public string BlockNotSupportedText { get; set; }
        public ContentArea_OnPageEditing OnPageEditing { get; set; }
    }
    
    public class ContentArea_OnPageEditing
    {
        public string Name { get; set; }
    }
}
