using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class LinkList : ReactComponent
    {
        public IList<Link> Items { get; set; }
        public IList<NestedLink_Item> NestedLinks { get; set; }
        public LinkList_OnPageEditing OnPageEditing { get; set; }
    }
    
    public class LinkList_OnPageEditing
    {
        public string Items { get; set; }
    }
}
