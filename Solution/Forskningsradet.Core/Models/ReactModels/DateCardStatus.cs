using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class DateCardStatus : ReactComponent
    {
        public DateCardStatus_Theme Theme { get; set; }
        public string Text { get; set; }
    }
    
    public enum DateCardStatus_Theme
    {
        [EnumMember(Value = "")]
        None = 0,
        [EnumMember(Value = "-is-active")]
        IsActive = 1,
        [EnumMember(Value = "-is-completed")]
        IsCompleted = 2,
        [EnumMember(Value = "-result-is-published")]
        ResultIsPublished = 3,
        [EnumMember(Value = "-is-canceled")]
        IsCanceled = 4,
        [EnumMember(Value = "-is-planned")]
        IsPlanned = 5,
    }
}
