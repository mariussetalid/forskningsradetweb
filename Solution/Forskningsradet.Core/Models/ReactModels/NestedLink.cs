using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class NestedLink : ReactComponent
    {
        public NestedLink_Item Item { get; set; }
    }
    
    public class NestedLink_Item
    {
        public string Icon { get; set; }
        public string Id { get; set; }
        public string OnPageEditing { get; set; }
        public string Text { get; set; }
        public string Url { get; set; }
        public string Label { get; set; }
        public string Title { get; set; }
        public IList<Link> Items { get; set; }
    }
}
