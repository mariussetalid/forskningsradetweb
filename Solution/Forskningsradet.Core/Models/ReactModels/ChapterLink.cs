using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class ChapterLink : ReactComponent
    {
        public string ChapterNumber { get; set; }
        public bool HasDash { get; set; }
        public bool IsCurrent { get; set; }
        public Link Link { get; set; }
    }
}
