using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class RelatedContent : ReactComponent
    {
        public EmptyList EmptyList { get; set; }
        public Link Link { get; set; }
        public RelatedContent_OnPageEditing OnPageEditing { get; set; }
        public string Title { get; set; }
        public RelatedContent_Border Border { get; set; }
    }
    
    public class RelatedContent_OnPageEditing
    {
        public string Title { get; set; }
    }
    
    public enum RelatedContent_Border
    {
        [EnumMember(Value = "")]
        None = 0,
        [EnumMember(Value = "top")]
        Top = 1,
        [EnumMember(Value = "bottom")]
        Bottom = 2,
    }
}
