using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class StickyMenuItem : ReactComponent
    {
        public StickyMenuItem_Link Link { get; set; }
        public StickyMenuItem_SubLinks SubLinks { get; set; }
    }
    
    public class StickyMenuItem_Link
    {
        public string Text { get; set; }
        public string Url { get; set; }
    }
    
    public class StickyMenuItem_SubLinks
    {
        public Accordion Accordion { get; set; }
        public StickyMenuItem_SubLinks_Links Links { get; set; }
    }
    
    public class StickyMenuItem_SubLinks_Links
    {
        public IList<StickyMenuItemSublink> Items { get; set; }
    }
}
