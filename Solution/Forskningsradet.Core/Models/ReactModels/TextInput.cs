using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class TextInput : ReactComponent
    {
        public string Label { get; set; }
        [Required]
        public string Name { get; set; }
        public string Placeholder { get; set; }
        public string ValidationError { get; set; }
        public string Value { get; set; }
    }
}
