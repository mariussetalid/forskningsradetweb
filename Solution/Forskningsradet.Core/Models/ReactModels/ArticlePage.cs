using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class ArticlePage : ReactComponent
    {
        public RichText Content { get; set; }
        public PageFooter Footer { get; set; }
        public ArticleHeader Header { get; set; }
        public string Ingress { get; set; }
        public ArticlePage_OnPageEditing OnPageEditing { get; set; }
        public ContentArea Sidebar { get; set; }
        public string Title { get; set; }
    }
    
    public class ArticlePage_OnPageEditing
    {
        public string Ingress { get; set; }
        public string Title { get; set; }
    }
}
