using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class TimelineItem : ReactComponent
    {
        public bool IsPastDate { get; set; }
        public string SubTitle { get; set; }
        public RichText Text { get; set; }
        public string Title { get; set; }
        public int Progress { get; set; }
    }
}
