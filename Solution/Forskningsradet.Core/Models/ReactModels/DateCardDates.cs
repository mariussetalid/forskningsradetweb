using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class DateCardDates : ReactComponent
    {
        public IList<DateCardDates_Dates> Dates { get; set; }
        public string DatesDescription { get; set; }
        public string DatesTitle { get; set; }
        public bool IsPastDate { get; set; }
        public bool IsInternational { get; set; }
        public bool IsPlanned { get; set; }
        public string ClassName { get; set; }
        public EventImage EventImage { get; set; }
        public DateCardDates_Labels Labels { get; set; }
    }
    
    public class DateCardDates_Dates
    {
        public string Day { get; set; }
        public string Month { get; set; }
        public string Year { get; set; }
    }
    
    public class DateCardDates_Labels
    {
        public string PastDate { get; set; }
    }
}
