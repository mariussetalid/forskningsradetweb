using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class Accordion : ReactComponent
    {
        [Required]
        public string CollapseLabel { get; set; }
        [Required]
        public string ExpandLabel { get; set; }
        [Required]
        public string Guid { get; set; }
        public bool InitiallyOpen { get; set; }
    }
}
