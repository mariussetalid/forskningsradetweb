using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class StickyMenuOnTabsItem : ReactComponent
    {
        public StickyMenuOnTabsItem_Link Link { get; set; }
    }
    
    public class StickyMenuOnTabsItem_Link
    {
        public string Text { get; set; }
        public string Url { get; set; }
    }
}
