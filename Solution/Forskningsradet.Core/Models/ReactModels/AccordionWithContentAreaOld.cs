using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class AccordionWithContentAreaOld : ReactComponent
    {
        public Accordion Accordion { get; set; }
        public ContentArea Content { get; set; }
        public string Description { get; set; }
        public string HtmlId { get; set; }
        public AccordionWithContentAreaOld_OnPageEditing OnPageEditing { get; set; }
        public RichText RichText { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }
    }
    
    public class AccordionWithContentAreaOld_OnPageEditing
    {
        public string Title { get; set; }
    }
}
