using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class Process : ReactComponent
    {
        public string Title { get; set; }
        public string IntroText { get; set; }
        public bool IsCarousel { get; set; }
        public IList<ProcessItem> Items { get; set; }
    }
}
