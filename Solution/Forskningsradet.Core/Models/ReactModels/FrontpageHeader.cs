using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class FrontpageHeader : ReactComponent
    {
        public FluidImage Image { get; set; }
        public IList<Link> Links { get; set; }
        public FrontpageHeader_OnPageEditing OnPageEditing { get; set; }
        public string Text { get; set; }
        public string Title { get; set; }
        public FrontpageHeader_Theme Theme { get; set; }
    }
    
    public class FrontpageHeader_OnPageEditing
    {
        public string Links { get; set; }
        public string Numbers { get; set; }
        public string Text { get; set; }
    }
    
    public enum FrontpageHeader_Theme
    {
        [EnumMember(Value = "")]
        None = 0,
        [EnumMember(Value = "-theme-dark-blue")]
        DarkBlue = 1,
        [EnumMember(Value = "-theme-grey-blue")]
        GreyBlue = 2,
        [EnumMember(Value = "-theme-purple")]
        Purple = 3,
    }
}
