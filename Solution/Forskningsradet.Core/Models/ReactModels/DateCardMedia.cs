using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class DateCardMedia : ReactComponent
    {
        public IList<DateCardMedia_Items> Items { get; set; }
    }
    
    public class DateCardMedia_Items
    {
        public string Text { get; set; }
        [Required]
        public DateCardMedia_Items_Icon Icon { get; set; }
        public string Url { get; set; }
    }
    
    public enum DateCardMedia_Items_Icon
    {
        [EnumMember(Value = "slideshow")]
        Slideshow = 0,
        [EnumMember(Value = "video")]
        Video = 1,
        [EnumMember(Value = "camera")]
        Camera = 2,
    }
}
