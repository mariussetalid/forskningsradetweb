using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class Footer : ReactComponent
    {
        public RichText ContactInfo { get; set; }
        public IList<Link> InfoLinks { get; set; }
        public IList<Footer_LinkLists> LinkLists { get; set; }
        public Link NewsLetter { get; set; }
        public SocialMediaLinkList SocialMedia { get; set; }
    }
    
    public class Footer_LinkLists
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public IList<Link> Links { get; set; }
    }
}
