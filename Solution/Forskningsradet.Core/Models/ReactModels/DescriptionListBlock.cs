using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class DescriptionListBlock : ReactComponent
    {
        public IList<DescriptionListBlock_Items> Items { get; set; }
        public string Title { get; set; }
    }
    
    public class DescriptionListBlock_Items
    {
        public string Label { get; set; }
        public string Text { get; set; }
    }
}
