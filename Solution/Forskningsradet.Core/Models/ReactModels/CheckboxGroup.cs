using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class CheckboxGroup : ReactComponent
    {
        public string Title { get; set; }
        public IList<Checkbox> Options { get; set; }
        public string ValidationError { get; set; }
    }
}
