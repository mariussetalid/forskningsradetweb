using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class TableBlock : ReactComponent
    {
        [Required]
        public Table Table { get; set; }
        [Required]
        public string Title { get; set; }
    }
}
