using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class TableOfContents : ReactComponent
    {
        public IList<TableOfContents_Items> Items { get; set; }
        [Required]
        public string Title { get; set; }
        public Accordion Accordion { get; set; }
    }
    
    public class TableOfContents_Items
    {
        public ChapterLink Link { get; set; }
        public ChapterNavigationList LinkOrLinkList { get; set; }
    }
}
