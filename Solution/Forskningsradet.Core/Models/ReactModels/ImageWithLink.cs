using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class ImageWithLink : ReactComponent
    {
        public FluidImage Image { get; set; }
        public ImageWithLink_OnPageEditing OnPageEditing { get; set; }
        public string Text { get; set; }
        public string Url { get; set; }
        public bool ShareImageHeight { get; set; }
    }
    
    public class ImageWithLink_OnPageEditing
    {
        public string Text { get; set; }
        public string Url { get; set; }
    }
}
