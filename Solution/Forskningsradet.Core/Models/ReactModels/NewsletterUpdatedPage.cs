using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class NewsletterUpdatedPage : ReactComponent
    {
        public string CategoriesTitle { get; set; }
        public IList<NewsletterUpdatedPage_CategoriesList> CategoriesList { get; set; }
        public string EmailTitle { get; set; }
        public string EmailText { get; set; }
        public NewsletterAdministrationLayout Layout { get; set; }
        public string Title { get; set; }
    }
    
    public class NewsletterUpdatedPage_CategoriesList
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
