using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class ContentAreaItem : ReactComponent
    {
        [Required]
        public string ComponentName { get; set; }
        [Required]
        public ReactComponent ComponentData { get; set; }
        [Required]
        public string Id { get; set; }
        public ContentAreaItem_OnPageEditing OnPageEditing { get; set; }
        public ContentAreaItem_Size Size { get; set; }
    }
    
    public class ContentAreaItem_OnPageEditing
    {
        public string ContentName { get; set; }
        public string ContentId { get; set; }
    }
    
    public enum ContentAreaItem_Size
    {
        [EnumMember(Value = "")]
        None = 0,
        [EnumMember(Value = "-size-full-screen")]
        FullScreen = 1,
        [EnumMember(Value = "-size-half")]
        Half = 2,
        [EnumMember(Value = "-size-third")]
        Third = 3,
        [EnumMember(Value = "-size-quarter")]
        Quarter = 4,
        [EnumMember(Value = "-size-two-thirds")]
        TwoThirds = 5,
    }
}
