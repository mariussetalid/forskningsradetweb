using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class Menu : ReactComponent
    {
        public string CloseButtonText { get; set; }
        public IList<NavigationGroup> NavigationGroups { get; set; }
    }
}
