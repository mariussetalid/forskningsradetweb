using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class ChapterNavigationList : ReactComponent
    {
        public Accordion Accordion { get; set; }
        public ChapterLink Link { get; set; }
        public bool IsCurrent { get; set; }
        [Required]
        public IList<ChapterNavigationList_LinkOrLinkList> LinkOrLinkList { get; set; }
    }
    
    public class ChapterNavigationList_LinkOrLinkList
    {
        public ChapterNavigationList LinkOrLinkList { get; set; }
        public ChapterLink Link { get; set; }
    }
}
