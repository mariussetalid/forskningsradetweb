using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class Breadcrumbs : ReactComponent
    {
        public Accordion Accordion { get; set; }
        public string CurrentPage { get; set; }
        public IList<Link> Items { get; set; }
        public Link Root { get; set; }
    }
}
