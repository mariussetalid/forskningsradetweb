using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class ErrorPage : ReactComponent
    {
        public Image Downtime { get; set; }
        public string ErrorCode { get; set; }
        public string LinkUrl { get; set; }
        public Image Logo { get; set; }
        public RichText Text { get; set; }
    }
}
