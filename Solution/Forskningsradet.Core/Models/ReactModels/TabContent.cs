using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class TabContent : ReactComponent
    {
        public IList<TabContentSection> ContentSections { get; set; }
        public StickyMenu Menu { get; set; }
    }
}
