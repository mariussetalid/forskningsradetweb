using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class Link : ReactComponent
    {
        public string Id { get; set; }
        public string OnPageEditing { get; set; }
        public string Text { get; set; }
        public string Url { get; set; }
        public string TrackUrl { get; set; }
        public string Label { get; set; }
        public bool IsExternal { get; set; }
        public bool IsPrimary { get; set; }
    }
}
