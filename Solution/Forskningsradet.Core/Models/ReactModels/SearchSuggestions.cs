using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class SearchSuggestions : ReactComponent
    {
        [Required]
        public string Label { get; set; }
        [Required]
        public IList<string> Suggestions { get; set; }
    }
}
