using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class DownloadList : ReactComponent
    {
        public IList<DownloadList_Groups> Groups { get; set; }
        public string DownloadAllText { get; set; }
        public string DownloadAllUrl { get; set; }
    }
    
    public class DownloadList_Groups
    {
        public string Heading { get; set; }
        public IList<DownloadList_Groups_Items> Items { get; set; }
    }
    
    public class DownloadList_Groups_Items
    {
        public string Text { get; set; }
        public DocumentIcon_IconTheme IconTheme { get; set; }
        public string Url { get; set; }
    }
}
