using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class PageLinksBlock : ReactComponent
    {
        public ContentArea HighlightedLinks { get; set; }
        public IList<PageLinksBlock_LinkLists> LinkLists { get; set; }
        public PageLinksBlock_OnPageEditing OnPageEditing { get; set; }
        public string Text { get; set; }
        public string Title { get; set; }
    }
    
    public class PageLinksBlock_LinkLists
    {
        public string Id { get; set; }
        public ContentArea Links { get; set; }
    }
    
    public class PageLinksBlock_OnPageEditing
    {
        public string Text { get; set; }
        public string Title { get; set; }
    }
}
