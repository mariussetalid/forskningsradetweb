using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class PortfolioContent : ReactComponent
    {
        public string Title { get; set; }
        public ContentArea Content { get; set; }
        public bool IsInsidePriorityBlock { get; set; }
        public bool SingleColumn { get; set; }
    }
}
