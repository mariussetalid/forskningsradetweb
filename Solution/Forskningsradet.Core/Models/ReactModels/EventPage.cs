using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class EventPage : ReactComponent
    {
        public ContentArea ContactInfo { get; set; }
        public EventPage_Labels Labels { get; set; }
        public IList<EventPageLink> Links { get; set; }
        public EventData Metadata { get; set; }
        public EventPage_OnPageEditing OnPageEditing { get; set; }
        public ContentArea Speakers { get; set; }
        public RichText RichText { get; set; }
        public Link RegistrationLink { get; set; }
        public ContentArea Schedule { get; set; }
        public string Title { get; set; }
        public OptionsModal Share { get; set; }
        public EventImage EventImage { get; set; }
        public DateCardDates DateContainer { get; set; }
        public DateCardMedia Media { get; set; }
        public StickyMenuOnTabs Menu { get; set; }
    }
    
    public class EventPage_Labels
    {
        public Label About { get; set; }
        public Label Contact { get; set; }
        public Label Speakers { get; set; }
        public Label Schedule { get; set; }
    }
    
    public class EventPage_OnPageEditing
    {
        public string Title { get; set; }
    }
}
