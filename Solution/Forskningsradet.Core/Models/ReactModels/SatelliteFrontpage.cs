using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class SatelliteFrontpage : ReactComponent
    {
        public SatelliteHeader SatelliteHeader { get; set; }
        public ContentArea Content { get; set; }
    }
}
