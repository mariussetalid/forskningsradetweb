using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class SatelliteHeader : ReactComponent
    {
        public FluidImage Image { get; set; }
        public string Ingress { get; set; }
        public IList<Link> Links { get; set; }
        public Map Map { get; set; }
        public SatelliteHeader_OnPageEditing OnPageEditing { get; set; }
        [Required]
        public string Title { get; set; }
        public string Text { get; set; }
        public SatelliteHeader_TextColor TextColor { get; set; }
    }
    
    public class SatelliteHeader_OnPageEditing
    {
        public string Title { get; set; }
        public string Ingress { get; set; }
        public string Text { get; set; }
        public string Links { get; set; }
    }
    
    public enum SatelliteHeader_TextColor
    {
        [EnumMember(Value = "")]
        None = 0,
        [EnumMember(Value = "-light-text")]
        Light = 1,
    }
}
