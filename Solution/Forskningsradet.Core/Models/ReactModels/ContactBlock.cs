using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class ContactBlock : ReactComponent
    {
        public ContentArea Items { get; set; }
        public ContactBlock_OnPageEditing OnPageEditing { get; set; }
        public string Title { get; set; }
    }
    
    public class ContactBlock_OnPageEditing
    {
        public string Title { get; set; }
    }
}
