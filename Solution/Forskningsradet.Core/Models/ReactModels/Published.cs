using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class Published : ReactComponent
    {
        public string Type { get; set; }
        public string Date { get; set; }
    }
}
