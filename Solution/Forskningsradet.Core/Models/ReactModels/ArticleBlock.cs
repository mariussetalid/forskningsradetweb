using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class ArticleBlock : ReactComponent
    {
        public Byline Byline { get; set; }
        public Link Title { get; set; }
        public FluidImage Image { get; set; }
        public string Text { get; set; }
        public Published Published { get; set; }
        public TagLinkList LinkTags { get; set; }
        public TagList Tags { get; set; }
        public bool UsedInSidebar { get; set; }
        public Metadata Metadata { get; set; }
        public DocumentIcon Icon { get; set; }
        public FluidImage DocumentImage { get; set; }
        public EventImage EventImage { get; set; }
    }
}
