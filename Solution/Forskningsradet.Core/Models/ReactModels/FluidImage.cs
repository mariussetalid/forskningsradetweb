using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class FluidImage : ReactComponent
    {
        [Required]
        public string Alt { get; set; }
        public FluidImage_FocusPoint FocusPoint { get; set; }
        public int InitialSize { get; set; }
        public FluidImage_OnPageEditing OnPageEditing { get; set; }
        [Required]
        public string Src { get; set; }
    }
    
    public class FluidImage_FocusPoint
    {
        [Required]
        public int X { get; set; }
        [Required]
        public int Y { get; set; }
    }
    
    public class FluidImage_OnPageEditing
    {
        public string Image { get; set; }
    }
}
