using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class Map : ReactComponent
    {
        [Required]
        public string GoogleMapsAPIKey { get; set; }
        public Map_Labels Labels { get; set; }
        public IList<Map_Markers> Markers { get; set; }
    }
    
    public class Map_Labels
    {
        public string ZoomIn { get; set; }
        public string ZoomOut { get; set; }
    }
    
    public class Map_Markers
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public float Latitude { get; set; }
        [Required]
        public float Longitude { get; set; }
        public MapPopup Popup { get; set; }
    }
}
