using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class Table : ReactComponent
    {
        public IList<string> Footer { get; set; }
        public IList<string> Header { get; set; }
        public IList<IList<string>> Rows { get; set; }
    }
}
