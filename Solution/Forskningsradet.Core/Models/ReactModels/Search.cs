using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class Search : ReactComponent
    {
        public string ExternalResultsLabel { get; set; }
        public string ExternalResultsEndpoint { get; set; }
        public TextInput Input { get; set; }
        public string ResultsDescription { get; set; }
        public Button Submit { get; set; }
        public SearchSuggestions SearchSuggestions { get; set; }
    }
}
