using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class SocialMediaLinkList : ReactComponent
    {
        public IList<SocialMediaLink> Items { get; set; }
    }
}
