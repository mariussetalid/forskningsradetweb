using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class Message : ReactComponent
    {
        public HtmlString Text { get; set; }
        public IconWarning_Theme Theme { get; set; }
    }
}
