using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class ContactListItem : ReactComponent
    {
        [Required]
        public string Title { get; set; }
        public IList<ContactListItem_ContactLists> ContactLists { get; set; }
        public ContactListItem_OnPageEditing OnPageEditing { get; set; }
    }
    
    public class ContactListItem_ContactLists
    {
        public ContactListItem_ContactLists_Name Name { get; set; }
        public Link Email { get; set; }
        public Link Phone { get; set; }
    }
    
    public class ContactListItem_ContactLists_Name
    {
        public string Text { get; set; }
        public string Position { get; set; }
    }
    
    public class ContactListItem_OnPageEditing
    {
        public string Title { get; set; }
    }
}
