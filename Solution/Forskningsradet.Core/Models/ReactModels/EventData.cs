using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class EventData : ReactComponent
    {
        public IList<EventData_Items> Items { get; set; }
        public EventData_OnPageEditing OnPageEditing { get; set; }
    }
    
    public class EventData_Items
    {
        public string Label { get; set; }
        public IList<IList<string>> Text { get; set; }
    }
    
    public class EventData_OnPageEditing
    {
        public string Items { get; set; }
    }
}
