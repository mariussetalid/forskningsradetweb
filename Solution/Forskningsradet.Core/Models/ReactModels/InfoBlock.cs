using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class InfoBlock : ReactComponent
    {
        public Link Cta { get; set; }
        public InfoBlock_EditorTheme EditorTheme { get; set; }
        public Image Icon { get; set; }
        public LinkList LinkList { get; set; }
        public InfoBlock_OnPageEditing OnPageEditing { get; set; }
        public string Title { get; set; }
        public RichText Text { get; set; }
        public string Url { get; set; }
    }
    
    public enum InfoBlock_EditorTheme
    {
        [EnumMember(Value = "")]
        None = 0,
        [EnumMember(Value = "-theme-blue")]
        Blue = 1,
        [EnumMember(Value = "-theme-dark-blue")]
        DarkBlue = 2,
        [EnumMember(Value = "-theme-orange")]
        Orange = 3,
    }
    
    public class InfoBlock_OnPageEditing
    {
        public string Title { get; set; }
    }
}
