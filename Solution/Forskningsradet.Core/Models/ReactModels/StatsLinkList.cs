using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class StatsLinkList : ReactComponent
    {
        public string Heading { get; set; }
        public IList<StatsLink> StatsLinks { get; set; }
        public Image AboutImage { get; set; }
        public Link SeeMoreLink { get; set; }
    }
}
