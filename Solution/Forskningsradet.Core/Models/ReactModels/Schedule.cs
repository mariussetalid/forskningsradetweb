using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class Schedule : ReactComponent
    {
        public IList<Schedule_ProgramList> ProgramList { get; set; }
        public string LastTitle { get; set; }
        public Schedule_OnPageEditing OnPageEditing { get; set; }
    }
    
    public class Schedule_ProgramList
    {
        public string Time { get; set; }
        public string Title { get; set; }
        public RichText Text { get; set; }
        public bool IsBreak { get; set; }
    }
    
    public class Schedule_OnPageEditing
    {
        public string ProgramList { get; set; }
    }
}
