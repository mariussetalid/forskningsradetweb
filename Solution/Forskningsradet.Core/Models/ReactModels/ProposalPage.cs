using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class ProposalPage : ReactComponent
    {
        public Link ApplyButton { get; set; }
        public ProposalContact Contact { get; set; }
        public string ContactLabel { get; set; }
        public OptionsModal Download { get; set; }
        public ArticleHeader Header { get; set; }
        public ProposalData MetadataLeft { get; set; }
        public ProposalData MetadataRight { get; set; }
        public Message Message { get; set; }
        public string DescriptionTitle { get; set; }
        public RichText DescriptionText { get; set; }
        public string DescriptionIngress { get; set; }
        public Tabs Tabs { get; set; }
        public Timeline Timeline { get; set; }
        public string Title { get; set; }
        public bool IsInternational { get; set; }
        public OptionsModal Share { get; set; }
        public IList<DateCardStatus> StatusList { get; set; }
    }
}
