using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class StepsBlock : ReactComponent
    {
        public IList<StepsBlock_Pages> Pages { get; set; }
    }
    
    public class StepsBlock_Pages
    {
        public string Title { get; set; }
        public string Text { get; set; }
        public string ButtonText { get; set; }
        public Image Image { get; set; }
    }
}
