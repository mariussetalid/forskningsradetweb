using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class Pagination : ReactComponent
    {
        public IList<Pagination_Pages> Pages { get; set; }
        public string Title { get; set; }
    }
    
    public class Pagination_Pages
    {
        public bool IsCurrent { get; set; }
        [Required]
        public string Label { get; set; }
        public Link Link { get; set; }
    }
}
