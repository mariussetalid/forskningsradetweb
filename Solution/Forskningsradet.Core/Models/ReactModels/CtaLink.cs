using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class CtaLink : ReactComponent
    {
        public Image Image { get; set; }
        public string Text { get; set; }
        public string Url { get; set; }
    }
}
