using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class Header : ReactComponent
    {
        [Required]
        public GlobalSearch GlobalSearch { get; set; }
        public Link LinkToHome { get; set; }
        [Required]
        public IList<Link> LinkList { get; set; }
        public Header_Logo Logo { get; set; }
        public Menu Menu { get; set; }
        [Required]
        public string MenuText { get; set; }
        public Header_Satellite Satellite { get; set; }
        public TabMenu TabMenu { get; set; }
        public Link PriorityLink { get; set; }
    }
    
    public class Header_Logo
    {
        public string Default { get; set; }
        public string White { get; set; }
    }
    
    public class Header_Satellite
    {
        public bool IsVisible { get; set; }
        public SimpleHeader Header { get; set; }
    }
}
