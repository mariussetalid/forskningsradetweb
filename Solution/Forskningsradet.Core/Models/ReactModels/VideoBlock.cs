using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class VideoBlock : ReactComponent
    {
        public YoutubeVideo Video { get; set; }
        public string Step { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }
        public string PlayText { get; set; }
    }
}
