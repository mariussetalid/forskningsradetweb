using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class YoutubeVideo : ReactComponent
    {
        public string OnPageEditing { get; set; }
        public string UrlOrEmbed { get; set; }
    }
}
