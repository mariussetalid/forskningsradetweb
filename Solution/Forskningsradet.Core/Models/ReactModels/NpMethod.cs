using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class NpMethod : ReactComponent
    {
        public string Title { get; set; }
        public string Ingress { get; set; }
        public Link Link { get; set; }
        public IList<Step> Menu { get; set; }
        public StepsContent StepsContent { get; set; }
    }
}
