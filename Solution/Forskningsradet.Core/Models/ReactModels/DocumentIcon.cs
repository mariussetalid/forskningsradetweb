using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class DocumentIcon : ReactComponent
    {
        public DocumentIcon_IconTheme IconTheme { get; set; }
    }
    
    public enum DocumentIcon_IconTheme
    {
        [EnumMember(Value = "")]
        None = 0,
        [EnumMember(Value = "word")]
        Word = 1,
        [EnumMember(Value = "pdf")]
        Pdf = 2,
        [EnumMember(Value = "ppt")]
        Ppt = 3,
        [EnumMember(Value = "csv")]
        Csv = 4,
        [EnumMember(Value = "excel")]
        Excel = 5,
        [EnumMember(Value = "image")]
        Image = 6,
        [EnumMember(Value = "fallback")]
        Fallback = 7,
    }
}
