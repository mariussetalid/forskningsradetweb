using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class EventPageLink : ReactComponent
    {
        [Required]
        public EventPageLink_Icon Icon { get; set; }
        public string Text { get; set; }
        public string Url { get; set; }
    }
    
    public enum EventPageLink_Icon
    {
        [EnumMember(Value = "video")]
        Video = 0,
        [EnumMember(Value = "calendar")]
        Calendar = 1,
    }
}
