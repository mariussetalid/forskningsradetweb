using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class ListItemImage : ReactComponent
    {
        public ListItemImage_Size Size { get; set; }
    }
    
    public enum ListItemImage_Size
    {
        [EnumMember(Value = "")]
        None = 0,
        [EnumMember(Value = "-size-small")]
        SmallImage = 1,
    }
}
