using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class RangeSlider : ReactComponent
    {
        [Required]
        public TextInput From { get; set; }
        [Required]
        public string Label { get; set; }
        [Required]
        public int Max { get; set; }
        [Required]
        public int Min { get; set; }
        [Required]
        public Button Submit { get; set; }
        [Required]
        public TextInput To { get; set; }
    }
}
