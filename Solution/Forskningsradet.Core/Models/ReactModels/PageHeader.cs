using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class PageHeader : ReactComponent
    {
        public FluidImage Image { get; set; }
        public string Ingress { get; set; }
        public IList<Link> Links { get; set; }
        public IList<Link> PortfolioLinks { get; set; }
        public Map Map { get; set; }
        public PageHeader_OnPageEditing OnPageEditing { get; set; }
        [Required]
        public string Title { get; set; }
        public TagLinkList Tags { get; set; }
    }
    
    public class PageHeader_OnPageEditing
    {
        public string Title { get; set; }
        public string Ingress { get; set; }
        public string Links { get; set; }
    }
}
