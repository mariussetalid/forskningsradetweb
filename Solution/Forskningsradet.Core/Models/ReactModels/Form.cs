using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class Form : ReactComponent
    {
        [Required]
        public string Endpoint { get; set; }
        public string Method { get; set; }
        public string SubmitButtonText { get; set; }
    }
}
