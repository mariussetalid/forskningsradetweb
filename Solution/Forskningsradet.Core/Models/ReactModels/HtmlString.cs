using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class HtmlString : ReactComponent
    {
        public HtmlString_OnPageEditing OnPageEditing { get; set; }
        [Required]
        public string Text { get; set; }
    }
    
    public class HtmlString_OnPageEditing
    {
        public string Text { get; set; }
    }
}
