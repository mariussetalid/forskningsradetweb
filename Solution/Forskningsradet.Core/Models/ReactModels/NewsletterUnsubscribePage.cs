using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class NewsletterUnsubscribePage : ReactComponent
    {
        public Form Form { get; set; }
        public TextInput Input { get; set; }
        public NewsletterAdministrationLayout Layout { get; set; }
        public Button SubmitButton { get; set; }
        public string Title { get; set; }
    }
}
