using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class ProposalListPage : ReactComponent
    {
        public EmptyList EmptyList { get; set; }
        public FilterLayout FilterLayout { get; set; }
        public Form Form { get; set; }
        public IList<ProposalGroup> Groups { get; set; }
        public string Title { get; set; }
        public TopFilter TopFilter { get; set; }
        public string SectionTitle { get; set; }
        public string FetchFilteredResultsEndpoint { get; set; }
    }
}
