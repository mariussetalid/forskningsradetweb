using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class TextWithSidebar : ReactComponent
    {
        public ContentArea Sidebar { get; set; }
        public RichText Text { get; set; }
        public string Title { get; set; }
        public string HtmlId { get; set; }
    }
}
