using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class MapPopup : ReactComponent
    {
        public RichText Address { get; set; }
        public Link Link { get; set; }
        public string Title { get; set; }
    }
}
