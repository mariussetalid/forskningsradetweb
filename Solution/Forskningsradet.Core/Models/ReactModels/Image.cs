using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class Image : ReactComponent
    {
        public string Alt { get; set; }
        public Image_OnPageEditing OnPageEditing { get; set; }
        public string Src { get; set; }
    }
    
    public class Image_OnPageEditing
    {
        public string Image { get; set; }
    }
}
