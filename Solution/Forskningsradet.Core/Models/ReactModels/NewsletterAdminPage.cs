using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class NewsletterAdminPage : ReactComponent
    {
        public IList<CheckboxGroup> CheckboxGroups { get; set; }
        public Form Form { get; set; }
        public string InputFieldsTitle { get; set; }
        public IList<TextInput> InputFieldsItems { get; set; }
        public NewsletterAdministrationLayout Layout { get; set; }
        public Button SubmitButton { get; set; }
        public string Title { get; set; }
        public Link UnsubscribeLink { get; set; }
        public Validation Validation { get; set; }
    }
}
