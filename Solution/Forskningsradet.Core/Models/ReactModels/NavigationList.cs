using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class NavigationList : ReactComponent
    {
        public Accordion Accordion { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public LinkList LinkList { get; set; }
    }
}
