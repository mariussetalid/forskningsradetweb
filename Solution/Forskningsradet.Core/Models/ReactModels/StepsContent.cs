using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class StepsContent : ReactComponent
    {
        public StepsContent_Close Close { get; set; }
        public int ActiveStepIndex { get; set; }
        public IList<StepsContent_Steps> Steps { get; set; }
    }
    
    public class StepsContent_Close
    {
        public string TextMobile { get; set; }
        public string TextDesktop { get; set; }
    }
    
    public class StepsContent_Steps
    {
        public VideoBlock VideoBlock { get; set; }
        public StepsBlock StepsBlock { get; set; }
        public QuotesBlock QuotesBlock { get; set; }
    }
}
