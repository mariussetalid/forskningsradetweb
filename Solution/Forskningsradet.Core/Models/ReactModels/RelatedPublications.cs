using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class RelatedPublications : ReactComponent
    {
        public RelatedContent RelatedContent { get; set; }
        public IList<SearchResult> Publications { get; set; }
        public bool UsedInSidebar { get; set; }
    }
}
