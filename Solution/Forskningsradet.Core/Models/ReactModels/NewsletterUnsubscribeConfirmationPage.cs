using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class NewsletterUnsubscribeConfirmationPage : ReactComponent
    {
        public NewsletterAdministrationLayout Layout { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }
    }
}
