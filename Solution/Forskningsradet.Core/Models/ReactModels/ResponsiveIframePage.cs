using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class ResponsiveIframePage : ReactComponent
    {
        public PageFooter Footer { get; set; }
        public ResponsiveIframe Iframe { get; set; }
        public RichText RichText { get; set; }
        public ContentArea Sidebar { get; set; }
        public string Title { get; set; }
    }
}
