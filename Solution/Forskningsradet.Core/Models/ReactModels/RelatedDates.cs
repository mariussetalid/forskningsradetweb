using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class RelatedDates : ReactComponent
    {
        public IList<DateCard> Dates { get; set; }
        public RelatedContent RelatedContent { get; set; }
        public bool UsedInSidebar { get; set; }
        public bool IsGrid { get; set; }
    }
}
