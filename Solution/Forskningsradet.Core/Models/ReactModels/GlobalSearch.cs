using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class GlobalSearch : ReactComponent
    {
        [Required]
        public string CloseButtonText { get; set; }
        [Required]
        public Form Form { get; set; }
        [Required]
        public TextInput Input { get; set; }
        [Required]
        public Button Button { get; set; }
        public string SubmitLabel { get; set; }
        public string Endpoint { get; set; }
    }
}
