using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class PersonBlock : ReactComponent
    {
        public string Company { get; set; }
        public Image Image { get; set; }
        public string JobTitle { get; set; }
        public string Name { get; set; }
        public PersonBlock_OnPageEditing OnPageEditing { get; set; }
        public string Text { get; set; }
    }
    
    public class PersonBlock_OnPageEditing
    {
        public string Company { get; set; }
        public string JobTitle { get; set; }
        public string Name { get; set; }
        public string Text { get; set; }
    }
}
