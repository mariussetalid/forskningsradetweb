using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class CampaignBlockList : ReactComponent
    {
        public IList<CampaignBlock> List { get; set; }
    }
}
