using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class LinkWithText : ReactComponent
    {
        public Link Link { get; set; }
        public LinkWithText_OnPageEditing OnPageEditing { get; set; }
        public string Text { get; set; }
    }
    
    public class LinkWithText_OnPageEditing
    {
        public string Link { get; set; }
        public string Text { get; set; }
    }
}
