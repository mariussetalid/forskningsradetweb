using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class LinkLists : ReactComponent
    {
        public IList<NestedLink_Item> NestedLinks { get; set; }
    }
}
