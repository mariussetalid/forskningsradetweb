using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class InputEmail : ReactComponent
    {
        public string Name { get; set; }
        public string Label { get; set; }
        public string ValidationError { get; set; }
        public string Value { get; set; }
    }
}
