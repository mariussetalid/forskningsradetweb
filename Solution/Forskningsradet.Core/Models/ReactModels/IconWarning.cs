using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class IconWarning : ReactComponent
    {
        public IconWarning_Theme Theme { get; set; }
    }
    
    public enum IconWarning_Theme
    {
        [EnumMember(Value = "")]
        None = 0,
        [EnumMember(Value = "-theme-red")]
        Red = 1,
        [EnumMember(Value = "-theme-blue")]
        Blue = 2,
    }
}
