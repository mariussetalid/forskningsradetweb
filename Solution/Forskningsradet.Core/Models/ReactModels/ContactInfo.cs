using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class ContactInfo : ReactComponent
    {
        public IList<Link> Details { get; set; }
        public ContactInfo_OnPageEditing OnPageEditing { get; set; }
        [Required]
        public string Title { get; set; }
    }
    
    public class ContactInfo_OnPageEditing
    {
        public string Title { get; set; }
    }
}
