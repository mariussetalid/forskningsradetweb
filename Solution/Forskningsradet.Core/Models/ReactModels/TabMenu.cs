using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class TabMenu : ReactComponent
    {
        public Link MainContentLink { get; set; }
        public string OpenMenuLabel { get; set; }
    }
}
