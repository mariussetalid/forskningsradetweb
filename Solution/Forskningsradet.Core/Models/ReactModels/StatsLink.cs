using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Forskningsradet.Core.Models.ReactModels
{
    public class StatsLink : ReactComponent
    {
        public Image Image { get; set; }
        public string Title { get; set; }
        public Link Link { get; set; }
    }
}
