﻿using System.ComponentModel.DataAnnotations;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ContentModels.Support;

namespace Forskningsradet.Core.Models.ContentModels.PropertyList
{
    public class CategoryTranslationSubject
    {
        [Display(
            Name = "Tema",
            GroupName = GroupNames.Content,
            Order = 10)]
        [SingleCategorySelection(CategoryConstants.SubjectRoot)]
        public virtual int Subject { get; set; }

        [Display(
            Name = "Oversettelse",
            GroupName = GroupNames.Content,
            Order = 20)]
        [Required]
        public virtual string Translation { get; set; }
    }
}