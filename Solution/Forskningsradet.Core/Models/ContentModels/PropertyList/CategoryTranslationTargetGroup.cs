﻿using System.ComponentModel.DataAnnotations;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ContentModels.Support;

namespace Forskningsradet.Core.Models.ContentModels.PropertyList
{
    public class CategoryTranslationTargetGroup
    {
        [Display(
            Name = "Målgruppe",
            GroupName = GroupNames.Content,
            Order = 10)]
        [SingleCategorySelection(CategoryConstants.TargetGroupRoot)]
        public virtual int TargetGroup { get; set; }

        [Display(
            Name = "Oversettelse",
            GroupName = GroupNames.Content,
            Order = 20)]
        [Required]
        public virtual string Translation { get; set; }
    }
}