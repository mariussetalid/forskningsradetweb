using System.ComponentModel;

namespace Forskningsradet.Core.Models.ContentModels.PropertyList
{
    public class GradeDistribution
    {
        [DisplayName("Karakter")]
        public string Grade { get; set; }
        [DisplayName("Antall")]
        public string Count { get; set; }
    }
}