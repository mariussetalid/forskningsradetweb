﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAnnotations;
using EPiServer.Shell.ObjectEditing;
using EPiServer.Web;
using Forskningsradet.Core.EditorDescriptors;
using Forskningsradet.Core.Models.ContentModels.Support;

namespace Forskningsradet.Core.Models.ContentModels.PropertyList
{
    public class TimeLineItem
    {
        [Display(
            Name = "Tid (Nødvendig på alle nye blokker)",
            Description = "Skal fylles ut på nye blokker.",
            Order = 10)]
        [ListItems(1)]
        public virtual IList<DateTime> DateTimes { get; set; }

        [Display(
            Name = "Tittel for tidspunkt",
            Description = "Brukes dersom det skal stå noe annet enn dato over punktet.",
            Order = 20)]
        [CultureSpecific]
        public virtual string DateTitle { get; set; }

        [Display(
            Name = "Tittel for tekst",
            Order = 30)]
        [CultureSpecific]
        public virtual string Title { get; set; }

        [Display(
            Name = "Tekst",
            Order = 40)]
        [CultureSpecific]
        [UIHint(PropertyListXhtmlStringEditorDescriptor.UIHint)]
        [XhtmlStringRestrictContentTypes]
        public virtual XhtmlString MainBody { get; set; }

        [Display(
            Name = "Type",
            Order = 50)]
        [BackingType(typeof(PropertyNumber))]
        [EditorDescriptor(EditorDescriptorType = typeof(EnumEditorDescriptor<TimeLineItemType>))]
        [Editable(false)]
        public virtual TimeLineItemType Type { get; set; }
    }
}