using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using Forskningsradet.Core.Models.ContentModels.Support;

namespace Forskningsradet.Core.Models.ContentModels.PropertyList
{
    public class RffLocation
    {
        [Display(
            Name = "Kortkode",
            GroupName = GroupNames.Content,
            Order = 10)]
        public virtual string Code { get; set; }

        [Display(
            Name = "Plassering",
            GroupName = GroupNames.Content,
            Order = 20)]
        public virtual PageReference Location { get; set; }
    }
}