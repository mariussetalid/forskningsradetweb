﻿using System.ComponentModel.DataAnnotations;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ContentModels.Support;

namespace Forskningsradet.Core.Models.ContentModels.PropertyList
{
    public class CategoryTranslationOther
    {
        [Display(
            Name = "Øvrige",
            Description = "Andre kategorier som ikke tilhører de andre gruppene",
            GroupName = GroupNames.Content,
            Order = 10)]
        [SingleCategorySelection(CategoryConstants.SubjectRoot, CategoryConstants.TargetGroupRoot)]
        public virtual int Other { get; set; }

        [Display(
            Name = "Oversettelse",
            GroupName = GroupNames.Content,
            Order = 20)]
        [Required]
        public virtual string Translation { get; set; }
    }
}