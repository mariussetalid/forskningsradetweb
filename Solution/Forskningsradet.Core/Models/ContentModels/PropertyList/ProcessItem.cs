﻿using System.ComponentModel.DataAnnotations;
using EPiServer;
using EPiServer.Cms.Shell.Json.Internal;
using EPiServer.Core;
using EPiServer.DataAnnotations;
using EPiServer.Web;
using Newtonsoft.Json;

namespace Forskningsradet.Core.Models.ContentModels.PropertyList
{
    public class ProcessItem
    {
        [Display(
            Name = "Tittel (Maks 100 tegn)",
            Order = 10)]
        [CultureSpecific]
        [StringLength(100)]
        public virtual string Title { get; set; }

        [Display(
            Name = "Tekst (Maks 200 tegn)",
            Order = 20)]
        [CultureSpecific]
        [StringLength(200)]
        public virtual string Ingress { get; set; }

        [Display(
            Name = "Ikon (overstyrer fast element)",
            Description = "Ikon for prosesselement.",
            Order = 30)]
        [UIHint(UIHint.Image)]
        public virtual ContentReference Image { get; set; }

        [Display(
            Name = "Link",
            Description = "Legger til en lenke som gjør hele prosesselementet klikkbart.",
            Order = 40)]
        [JsonProperty]
        [JsonConverter(typeof(UrlConverter))]
        public virtual Url Url { get; set; }
    }
}