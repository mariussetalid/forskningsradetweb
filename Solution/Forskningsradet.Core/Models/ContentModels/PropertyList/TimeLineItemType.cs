﻿namespace Forskningsradet.Core.Models.ContentModels.PropertyList
{
    public enum TimeLineItemType
    {
        None,
        StartDate,
        Deadline,
        EarliestStartDate,
        LatestProjectPeriodStart,
        LatestProjectPeriodEnd
    }
}