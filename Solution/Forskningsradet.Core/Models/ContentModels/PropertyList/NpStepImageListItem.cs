﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.Web;

namespace Forskningsradet.Core.Models.ContentModels.PropertyList
{
    public class NpStepImageListItem
    {
        [Display(
            Name = "Tittel (Maks 40 tegn)",
            Order = 10)]
        [StringLength(40)]
        public virtual string Title { get; set; }

        [Display(
            Name = "Tekst (Maks 70 tegn)",
            Order = 20)]
        [StringLength(70)]
        [UIHint(UIHint.Textarea)]
        public virtual string Ingress { get; set; }

        [Display(
            Name = "Tekst på knapp",
            Description = "Tekst for knappen på dette elementet.",
            Order = 30)]
        public virtual string ButtonText { get; set; }

        [Display(
            Name = "Bilde",
            Description = "Bilde eller gif for dette elementet.",
            Order = 40)]
        [UIHint(UIHint.Image)]
        public virtual ContentReference Image { get; set; }
    }
}