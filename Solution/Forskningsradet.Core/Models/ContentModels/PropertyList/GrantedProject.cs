using System.ComponentModel;

namespace Forskningsradet.Core.Models.ContentModels.PropertyList
{
    public class GrantedProject
    {
        [DisplayName("Nummer")]
        public string Number { get; set; }
        [DisplayName("Tittel")]
        public string Title { get; set; }
        [DisplayName("Organisasjon")]
        public string Organization { get; set; }
    }
}