﻿using System.ComponentModel.DataAnnotations;
using EPiServer;
using EPiServer.Cms.Shell.Json.Internal;
using EPiServer.DataAnnotations;
using Newtonsoft.Json;

namespace Forskningsradet.Core.Models.ContentModels.PropertyList
{
    public class OfficeInformation
    {
        [Display(
            Name = "Tittel",
            Order = 10)]
        [CultureSpecific]
        public string Title { get; set; }

        [Display(
            Name = "Tekst",
            Order = 20)]
        [CultureSpecific]
        public string Text { get; set; }

        [Display(
            Name = "Mer tekst",
            Order = 30)]
        [CultureSpecific]
        public string MoreText { get; set; }

        [JsonProperty]
        [JsonConverter(typeof(UrlConverter))]
        [Display(
            Name = "Lenke",
            Order = 40)]
        public Url Link { get; set; }

        [Display(
            Name = "Lenketekst",
            Order = 50)]
        [CultureSpecific]
        public string LinkText { get; set; }
    }
}
