﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAnnotations;
using EPiServer.Web;
using Forskningsradet.Core.EditorDescriptors;
using Forskningsradet.Core.Models.ContentModels.Support;

namespace Forskningsradet.Core.Models.ContentModels.PropertyList
{
    public class ScheduleItem
    {
        [Display(
            Name = "Klokkeslett [hh:mm]",
            Description = "Firesifret klokkeslett med kolon som separator. Skrives inn manuelt.",
            Order = 10)]
        [MaxLength(5)]
        public virtual string Time { get; set; }

        [Display(
            Name = "Tittel",
            Description = "Tittel eller beskrivelse av hva som skal skje.",
            Order = 20)]
        [CultureSpecific]
        public virtual string Title { get; set; }

        [Display(
            Name = "Tekst",
            Description = "Fritekst for å skrive mer utfyllende informasjon om aktiviteten.",
            Order = 30)]
        [CultureSpecific]
        [UIHint(PropertyListXhtmlStringEditorDescriptor.UIHint)]
        [XhtmlStringRestrictContentTypes]
        public virtual XhtmlString MainBody { get; set; }

        [Display(
            Name = "Pause",
            Description = "Spesifiserer at dette punktet er en pauseaktivitet.",
            Order = 40)]
        public virtual bool IsBreak { get; set; }
    }
}