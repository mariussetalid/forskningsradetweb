using System.ComponentModel;

namespace Forskningsradet.Core.Models.ContentModels.PropertyList
{
    public class SubjectSpecialist
    {
        [DisplayName("Navn")]
        public string Name { get; set; }
        [DisplayName("Organisasjon")]
        public string Organization { get; set; } // TODO We don't have organization, we have country
    }
}