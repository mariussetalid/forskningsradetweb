﻿using System.ComponentModel.DataAnnotations;
using EPiServer;
using EPiServer.Cms.Shell.Json.Internal;
using EPiServer.Core;
using EPiServer.DataAnnotations;
using Forskningsradet.Core.Models.ContentModels.Support;
using Newtonsoft.Json;

namespace Forskningsradet.Core.Models.ContentModels.PropertyList
{
    public class MapMarker
    {
        [Display(
            Name = "Navn",
            Order = 10)]
        [Required]
        [CultureSpecific]
        public string Name { get; set; }

        [Display(
            Name = "Breddegrad",
            Order = 20)]
        [Required]
        public float Latitude { get; set; }

        [Display(
            Name = "Lengdegrad",
            Order = 30)]
        [Required]
        public float Longitude { get; set; }

        [Display(
            Name = "Tittel for popup",
            Order = 40)]
        [CultureSpecific]
        public string PopUpTitle { get; set; }

        [Display(
            Name = "Addresse",
            Order = 50)]
        [XhtmlStringRestrictContentTypes]
        [CultureSpecific]
        public XhtmlString Address { get; set; }

        [Display(
            Name = "Lenke",
            Order = 60)]
        [JsonProperty]
        [JsonConverter(typeof(UrlConverter))]
        public Url Link { get; set; }

        [Display(
            Name = "Lenketekst",
            Order = 70)]
        [CultureSpecific]
        public string LinkText { get; set; }
    }
}
