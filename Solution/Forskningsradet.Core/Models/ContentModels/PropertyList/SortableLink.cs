﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAnnotations;
using Forskningsradet.Core.Models.ContentModels.Pages;

namespace Forskningsradet.Core.Models.ContentModels.PropertyList
{
    public class SortableLink
    {
        [Display(
            Name = "Tittel",
            Order = 10)]
        [CultureSpecific]
        public string Title { get; set; }

        [Display(
            Name = "Lenke",
            Order = 40)]
        [AllowedTypes(typeof(ContentAreaPage))]
        public PageReference Link { get; set; }

        [Display(
            Name = "Rekkefølge",
            Order = 50)]
        [CultureSpecific]
        public int SortOrder { get; set; }
    }
}
