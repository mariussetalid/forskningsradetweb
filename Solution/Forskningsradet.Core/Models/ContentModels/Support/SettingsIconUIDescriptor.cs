﻿using EPiServer.Shell;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.Contracts;

namespace Forskningsradet.Core.Models.ContentModels.Support
{
    [UIDescriptorRegistration]
    public class SettingsIconUIDescriptor : UIDescriptor<IPageHasSettingsIcon>
    {
        public SettingsIconUIDescriptor() : base(IconConstants.CssClasses.Settings)
        {
        }
    }
}