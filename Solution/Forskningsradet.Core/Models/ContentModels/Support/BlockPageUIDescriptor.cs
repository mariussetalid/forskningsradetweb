﻿using EPiServer.Shell;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ContentModels.Pages;

namespace Forskningsradet.Core.Models.ContentModels.Support
{
    [UIDescriptorRegistration]
    public class BlockPageUIDescriptor : UIDescriptor<ContentAreaPage>
    {
        public BlockPageUIDescriptor() : base(IconConstants.CssClasses.Blocks)
        {
        }
    }
}