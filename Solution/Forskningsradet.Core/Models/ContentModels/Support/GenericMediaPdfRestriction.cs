﻿using System.ComponentModel.DataAnnotations;
using EPiServer;
using EPiServer.Core;
using EPiServer.ServiceLocation;
using Forskningsradet.Core.Models.ContentModels.Media;

namespace Forskningsradet.Core.Models.ContentModels.Support
{
    public class GenericMediaPdfRestriction : ValidationAttribute
    {
        private const string PdfExtension = ".pdf";
        protected override ValidationResult IsValid(object value, ValidationContext context)
        {
            if (context.ObjectInstance is IContentData contentData)
            {
                var reference = contentData.Property[context.MemberName]?.Value as ContentReference;
                if (!ServiceLocator.Current.GetInstance<IContentLoader>().TryGet(reference, out IContent referencedContent))
                    return ValidationResult.Success;

                if ((referencedContent as GenericMedia)?.FileExtension == PdfExtension)
                    return ValidationResult.Success;
            }
            return new ValidationResult($"Du kan kun bruke filer av typen {PdfExtension} på {context.DisplayName}");
        }
    }
}
