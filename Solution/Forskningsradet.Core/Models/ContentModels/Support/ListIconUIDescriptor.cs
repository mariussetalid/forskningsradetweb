﻿using EPiServer.Shell;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.Contracts;

namespace Forskningsradet.Core.Models.ContentModels.Support
{
    [UIDescriptorRegistration]
    public class ListIconUIDescriptor : UIDescriptor<IPageHasListIcon>
    {
        public ListIconUIDescriptor() : base(IconConstants.CssClasses.List)
        {
        }
    }
}