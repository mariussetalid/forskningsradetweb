﻿using EPiServer.Shell;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.Contracts;

namespace Forskningsradet.Core.Models.ContentModels.Support
{
    [UIDescriptorRegistration]
    public class NoViewIconUIDescriptor : UIDescriptor<IPageHasNoViewIcon>
    {
        public NoViewIconUIDescriptor() : base(IconConstants.CssClasses.NoView)
        {
        }
    }
}