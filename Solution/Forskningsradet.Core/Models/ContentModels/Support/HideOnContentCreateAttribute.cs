﻿using System;

namespace Forskningsradet.Core.Models.ContentModels.Support
{
    [AttributeUsage(AttributeTargets.Property)]
    public class HideOnContentCreateAttribute : Attribute
    {
    }
}