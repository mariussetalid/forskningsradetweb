﻿using System;

namespace Forskningsradet.Core.Models.ContentModels.Support
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = true)]
    public class IncludeFullWidthPreview : Attribute
    {
    }
}