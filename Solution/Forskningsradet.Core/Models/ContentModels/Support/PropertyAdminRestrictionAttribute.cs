﻿using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using EPiServer.Core;

namespace Forskningsradet.Core.Models.ContentModels.Support
{
    // Inspired by https://world.episerver.com/blogs/Linus-Ekstrom/Dates/2014/5/Restricting-access-to-who-is-allowed-to-edit-a-certain-property/
    public class PropertyAdminRestrictionAttribute : ValidationAttribute, IMetadataAware
    {
        public void OnMetadataCreated(ModelMetadata metadata)
        {
            if (EPiServer.Security.PrincipalInfo.HasAdminAccess)
            {
                return;
            }

            metadata.IsReadOnly = true;
            foreach (var propertyMetadata in metadata.Properties ?? Enumerable.Empty<ModelMetadata>())
                propertyMetadata.IsReadOnly = true;
        }

        public override string FormatErrorMessage(string name) => "You do not have access to change " + name;

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (!(validationContext.ObjectInstance is IContentData contentData))
            {
                return ValidationResult.Success;
            }

            if (!contentData.Property[validationContext.MemberName].IsModified)
            {
                return ValidationResult.Success;
            }

            return Validate()
                ? ValidationResult.Success
                : new ValidationResult("You do not have access");
        }

        public override bool RequiresValidationContext => true;

        public bool Validate() => EPiServer.Security.PrincipalInfo.HasAdminAccess;
    }
}