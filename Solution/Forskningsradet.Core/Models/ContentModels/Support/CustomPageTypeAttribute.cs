﻿using System;

namespace Forskningsradet.Core.Models.ContentModels.Support
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class CustomPageTypeAttribute : Attribute
    {
        public CustomPageTypeSet CustomPageTypeSet { get; set; }

        public CustomPageTypeAttribute(CustomPageTypeSet customPageTypeSet = CustomPageTypeSet.Editorial) => CustomPageTypeSet = customPageTypeSet;
    }

    public enum CustomPageTypeSet
    {
        None = 0,
        Editorial = 1,
        Content = 2
    }
}