﻿using System.ComponentModel.DataAnnotations;
using System.Linq;
using EPiServer;
using EPiServer.Core;
using EPiServer.Core.Html.StringParsing;
using EPiServer.ServiceLocation;
using Forskningsradet.Core.Models.ContentModels.Blocks;

namespace Forskningsradet.Core.Models.ContentModels.Support
{
    public class XhtmlStringMessageBlockRestriction : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext context)
        {
            if (context.ObjectInstance is IContentData contentData
                && contentData.Property[context.MemberName]?.Value is XhtmlString richTextProperty)
            {
                foreach (var stringFragment in richTextProperty.Fragments.Where(x => x is ContentFragment))
                {
                    var fragment = (ContentFragment)stringFragment;
                    var content = ServiceLocator.Current.GetInstance<IContentRepository>().Get<IContentData>(fragment.ContentLink);

                    if (content is MessageBaseBlock)
                        return new ValidationResult($"Du kan ikke bruke {content.GetOriginalType().Name} på {context.MemberName}");
                }
            }
            return ValidationResult.Success;
        }
    }
}
