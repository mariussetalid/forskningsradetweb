﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using System.Windows;
using EPiServer.Cms.Shell.UI.ObjectEditing;
using EPiServer.Core;

namespace Forskningsradet.Core.Models.ContentModels.Support
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
    public class HiddenForNonStartPageAttribute : ValidationAttribute, IMetadataAware
    {
        public override bool RequiresValidationContext => true;

        public void OnMetadataCreated(ModelMetadata metadata)
        {
            if (metadata is ContentDataMetadata contentMetadata)
            {
                var hide = !CheckCanBeShown(contentMetadata.Parent?.Model);
                if (hide)
                {
                    metadata.IsReadOnly = true;
                    metadata.ShowForEdit = false;
                    foreach (var propertyMetadata in metadata.Properties ?? Enumerable.Empty<ModelMetadata>())
                    {
                        propertyMetadata.IsReadOnly = true;
                        propertyMetadata.ShowForEdit = false;
                    }
                }
            }
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var propertyNotModified = (validationContext.ObjectInstance as IContentData)?.Property[validationContext.MemberName]?.IsModified == false;
            if (CheckCanBeShown(validationContext.ObjectInstance) || propertyNotModified)
            {
                return ValidationResult.Success;
            }

            return new ValidationResult($"{validationContext.DisplayName} can be edited on start page of site only.");
        }

        private bool CheckCanBeShown(object contentObject)
        {
            return contentObject is PageData page && ContentReference.StartPage.Equals(page.ContentLink, true);
        }
    }
}