﻿using EPiServer.DataAnnotations;

namespace Forskningsradet.Core.Models.ContentModels.Support
{
    public class ContentPageTypeAttribute : ContentTypeAttribute
    {
        public ContentPageTypeAttribute()
        {
            GroupName = "Innholdssidetyper";
            Order = 100;
        }
    }
}