﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using EPiServer.Core;
using EPiServer.SpecializedProperties;

namespace Forskningsradet.Core.Models.ContentModels.Support
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class MaxItemsAttribute : ValidationAttribute
    {
        private readonly int _max;
        private const string NotSupportedPropertyMessage = "MaxItemsAttribute cannot be used with this property";
        private const string MaxItemsErrorMessageFormat = " is restricted to {0} item(s)";

        public MaxItemsAttribute(int max) => _max = max;

        public override bool IsValid(object value)
        {
            if (NotSupportedProperty())
                throw new ValidationException(NotSupportedPropertyMessage);

            if (value is ContentArea contentArea)
            {
                var allItems = contentArea?.Items ?? Enumerable.Empty<ContentAreaItem>();
                var groups = allItems.GroupBy(x => string.IsNullOrEmpty(x.ContentGroup) ? "no group" : x.ContentGroup);
                if (groups.Any(x => x.Count() > _max))
                {
                    ErrorMessage = GetErrorMessage();
                    return false;
                }
                return true;
            }

            switch (value)
            {
                case IList<string> list when list.Count > _max:
                case LinkItemCollection links when links.Count > _max:
                    ErrorMessage = GetErrorMessage();
                    return false;
            }
            return true;

            bool NotSupportedProperty() =>
                (value != null) && !(value is ContentArea || value is IList<string> || value is LinkItemCollection);

            string GetErrorMessage() => string.Format(MaxItemsErrorMessageFormat, _max);
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var result = base.IsValid(value, validationContext);
            if (!string.IsNullOrEmpty(result?.ErrorMessage))
                result.ErrorMessage = $"{validationContext.DisplayName} {ErrorMessage}";
            return result;
        }
    }
}