﻿using System.ComponentModel.DataAnnotations;
using EPiServer.DataAnnotations;
using EPiServer.Security;

namespace Forskningsradet.Core.Models.ContentModels.Support
{
    [GroupDefinitions]
    public static class GroupNames
    {
        public static class Blocks
        {
            [Display(Order = 10)]
            public const string Common = "Innholdsblokker (brukes av flest til mest)";

            [Display(Order = 20)]
            public const string Embed = "Embed-blokker (video og grafikk)";

            [Display(Order = 30)]
            public const string Uncommon = "Spesifikke blokker (brukes av få til lite. Spesiallaget til bruk på spesifikke sider)";

            [Display(Order = 40)]
            public const string RelatedContent = "Relatert innhold-blokker";
        }

        /// <summary>
        /// Standard tab "Innhold" in Norwegian.
        /// </summary>
        [Display(Order = 10)]
        public const string Content = EPiServer.DataAbstraction.SystemTabNames.Content;

        [Display(Order = 20)]
        public const string Listevisning = nameof(Listevisning);

        [Display(Order = 30)]
        public const string SEOsettings = nameof(SEOsettings);

        /// <summary>
        /// Specific tab for "Kartmarkører".
        /// </summary>
        [Display(Order = 40)]
        public const string Kart = nameof(Kart);

        /// <summary>
        /// Specific tab for "Globale driftsmeldinger".
        /// </summary>
        [Display(Order = 50)]
        [RequiredAccess(AccessLevel.Administer)]
        public const string Meldinger = nameof(Meldinger);

        /// <summary>
        /// Specific tab for "Globale driftsmeldinger" with different Access Level for satellite sites.
        /// </summary>
        [Display(Order = 55)]
        [RequiredAccess(AccessLevel.Edit)]
        public const string Driftsmeldinger = nameof(Driftsmeldinger);

        [Display(Order = 60)]
        public const string Arkivering = nameof(Arkivering);

        [Display(Order = 90)]
        public const string Import = nameof(Import);

        [Display(Order = 100, Name = "Revisjon")]
        public const string Revision = nameof(Revision);

        /// <summary>
        /// Specific for proposal page
        /// </summary>
        [Display(Order = 11, Name = "Utlysningsdetaljer")]
        public const string ProposalDetails = nameof(ProposalDetails);

        /// <summary>
        /// Specific for proposal page
        /// </summary>
        [Display(Order = 12, Name = "Under behandling")]
        public const string ApplicationProcessing = nameof(ApplicationProcessing);

        /// <summary>
        /// Specific for proposal page
        /// </summary>
        [Display(Order = 13, Name = "Søknadsresultater")]
        public const string ApplicationResults = nameof(ApplicationResults);

        [Display(Order = 90)]
        [RequiredAccess(AccessLevel.Administer)]
        public const string Mailchimp = nameof(Mailchimp);

        /// <summary>
        ///  Standard tab "Innstillinger" in Norwegian.
        /// </summary>
        [Display(Order = 100)]
        public const string Settings = EPiServer.DataAbstraction.SystemTabNames.Settings;

        [Display(Order = 200)]
        public const string NettstedsInnstillinger = "Nettstedsinnstillinger";

        [Display(Order = 210)]
        [RequiredAccess(AccessLevel.Administer)]
        public const string AdministratorInnstillinger = "Administratorinnstillinger";

        [Display(Order = 220)]
        public const string SatellittInnstillinger = "Satellittinnstillinger";

        [Display(Order = 1000)]
        public const string MigratedContent = "Migrert innhold";
    }
}