﻿using EPiServer.Shell;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ContentModels.Pages;

namespace Forskningsradet.Core.Models.ContentModels.Support
{
    [UIDescriptorRegistration]
    public class DefaultPageUIDescriptor : UIDescriptor<BasePageData>
    {
        public DefaultPageUIDescriptor() : base(IconConstants.CssClasses.Blank)
        {
        }
    }
}