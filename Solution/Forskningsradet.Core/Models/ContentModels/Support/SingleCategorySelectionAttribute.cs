using System;
using EPiServer.Shell.ObjectEditing;
using Forskningsradet.Core.Factories;

namespace Forskningsradet.Core.Models.ContentModels.Support
{
    [AttributeUsage(AttributeTargets.Property)]
    public class SingleCategorySelectionAttribute : SelectOneAttribute
    {
        public string Category { get; }
        public string[] ExcludeCategories { get; } = new string[0];

        public SingleCategorySelectionAttribute() => SelectionFactoryType = typeof(CategorySelectionFactory);

        public SingleCategorySelectionAttribute(string categoryRoot)
        {
            Category = categoryRoot;
            SelectionFactoryType = typeof(CategorySelectionFactory);
        }

        public SingleCategorySelectionAttribute(params string[] excludeCategories)
        {
            Category = string.Empty;
            ExcludeCategories = excludeCategories ?? new string[0];
            SelectionFactoryType = typeof(CategorySelectionFactory);
        }
    }
}