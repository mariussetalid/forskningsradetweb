﻿using EPiServer.DataAnnotations;

namespace Forskningsradet.Core.Models.ContentModels.Support
{
    public class SystemPageTypeAttribute : ContentTypeAttribute
    {
        public SystemPageTypeAttribute()
        {
            GroupName = "Systemsidetyper";
            Order = 10000;
            Description = "Tekniske sidetyper som brukes til spesielle formål. Skal normalt sett ikke benyttes av redaktører.";
        }
    }
}