﻿using EPiServer.Shell;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ContentModels.Pages;

namespace Forskningsradet.Core.Models.ContentModels.Support
{
    [UIDescriptorRegistration]
    public class StartPageUIDescriptor : UIDescriptor<FrontPageBase>
    {
        public StartPageUIDescriptor() : base(IconConstants.CssClasses.StartPage) => DefaultView = CmsViewNames.AllPropertiesView;
    }
}