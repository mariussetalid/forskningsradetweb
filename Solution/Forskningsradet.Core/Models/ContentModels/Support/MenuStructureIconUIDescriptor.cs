﻿using EPiServer.Shell;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ContentModels.Pages;

namespace Forskningsradet.Core.Models.ContentModels.Support
{
    [UIDescriptorRegistration]
    public class MenuStructureIconUIDescriptor : UIDescriptor<MenuStructurePage>
    {
        public MenuStructureIconUIDescriptor() : base(IconConstants.CssClasses.Menu)
        {
        }
    }
}