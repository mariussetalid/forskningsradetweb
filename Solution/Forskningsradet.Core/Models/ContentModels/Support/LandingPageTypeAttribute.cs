﻿using EPiServer.DataAnnotations;

namespace Forskningsradet.Core.Models.ContentModels.Support
{
    public class LandingPageTypeAttribute : ContentTypeAttribute
    {
        public LandingPageTypeAttribute()
        {
            GroupName = "Landingssidetyper";
            Order = 200;
        }
    }
}