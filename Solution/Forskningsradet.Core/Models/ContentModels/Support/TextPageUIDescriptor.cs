﻿using EPiServer.Shell;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ContentModels.Pages;

namespace Forskningsradet.Core.Models.ContentModels.Support
{
    [UIDescriptorRegistration]
    public class TextPageUIDescriptor : UIDescriptor<ArticlePage>
    {
        public TextPageUIDescriptor() : base(IconConstants.CssClasses.Text)
        {
        }
    }
}