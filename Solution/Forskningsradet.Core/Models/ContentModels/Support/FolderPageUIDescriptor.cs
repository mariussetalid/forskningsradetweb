﻿using EPiServer.Shell;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ContentModels.Pages;

namespace Forskningsradet.Core.Models.ContentModels.Support
{
    [UIDescriptorRegistration]
    public class FolderPageUIDescriptor : UIDescriptor<FolderPage>
    {
        public FolderPageUIDescriptor() : base(IconConstants.CssClasses.Folder) => DefaultView = CmsViewNames.AllPropertiesView;
    }
}