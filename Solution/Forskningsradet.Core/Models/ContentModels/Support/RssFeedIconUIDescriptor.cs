﻿using EPiServer.Shell;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ContentModels.Pages;

namespace Forskningsradet.Core.Models.ContentModels.Support
{
    [UIDescriptorRegistration]
    public class RssFeedIconUIDescriptor : UIDescriptor<RssPage>
    {
        public RssFeedIconUIDescriptor() : base(IconConstants.CssClasses.Rss)
        {
        }
    }
}