﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using EPiServer;
using EPiServer.Core;
using EPiServer.Core.Html.StringParsing;
using EPiServer.ServiceLocation;
using Forskningsradet.Core.Models.ContentModels.Blocks;

namespace Forskningsradet.Core.Models.ContentModels.Support
{
    public class XhtmlStringRestrictContentTypes : ValidationAttribute
    {
        private readonly AllowInXhtmlString _allowInXhtmlString;

        private static readonly Type[] _allowedMediaTypes =
        {
            typeof(ImageBlock),
            typeof(ImageWithLinkBlock),
            typeof(ImageWithCaptionBlock),
            typeof(VideoBlock),
            typeof(LocalMessageBlock),
            typeof(MediaBlock)
        };

        private static readonly Type[] _allowedIframeTypes =
        {
            typeof(IframeBlock)
        };

        public XhtmlStringRestrictContentTypes(AllowInXhtmlString allowInXhtmlString = AllowInXhtmlString.None) =>
            _allowInXhtmlString = allowInXhtmlString;

        protected override ValidationResult IsValid(object value, ValidationContext context)
        {
            if (context.ObjectInstance is IContentData contentData
                && contentData.Property[context.MemberName]?.Value is XhtmlString richTextProperty)
            {
                foreach (var stringFragment in richTextProperty.Fragments.Where(x => x is ContentFragment))
                {
                    var fragment = (ContentFragment)stringFragment;
                    var content = ServiceLocator.Current.GetInstance<IContentRepository>().Get<IContentData>(fragment.ContentLink);

                    if (CheckIsValidMedia(content, _allowInXhtmlString))
                        continue;

                    return new ValidationResult($"Du kan ikke bruke {content.GetOriginalType().Name} på {context.MemberName}");
                }
            }
            return ValidationResult.Success;
        }

        private bool CheckIsValidMedia(IContentData content, AllowInXhtmlString allow)
        {
            switch (allow)
            {
                case AllowInXhtmlString.SomeMedia:
                    return _allowedMediaTypes.Any(x => x.IsInstanceOfType(content));
                case AllowInXhtmlString.SomeMediaAndIframe:
                    return _allowedMediaTypes.Any(x => x.IsInstanceOfType(content))
                        || _allowedIframeTypes.Any(x => x.IsInstanceOfType(content));
                case AllowInXhtmlString.None:
                default:
                    return false;
            }
        }
    }

    public enum AllowInXhtmlString
    {
        None = 0,
        SomeMedia = 1,
        SomeMediaAndIframe = 2
    };
}