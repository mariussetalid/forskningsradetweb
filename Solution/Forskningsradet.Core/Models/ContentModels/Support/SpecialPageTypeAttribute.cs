﻿using EPiServer.DataAnnotations;

namespace Forskningsradet.Core.Models.ContentModels.Support
{
    public class SpecialPageTypeAttribute : ContentTypeAttribute
    {
        public SpecialPageTypeAttribute()
        {
            GroupName = "Spesielle sidetyper (brukes kun i spesielle tilfeller)";
            Order = 9000;
        }
    }
}