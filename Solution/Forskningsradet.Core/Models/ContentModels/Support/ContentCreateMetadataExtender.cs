﻿using System;
using System.Collections.Generic;
using System.Linq;
using EPiServer.Core;
using EPiServer.Shell.ObjectEditing;

namespace Forskningsradet.Core.Models.ContentModels.Support
{
    public class ContentCreateMetadataExtender : IMetadataExtender
    {
        public void ModifyMetadata(ExtendedMetadata metadata, IEnumerable<Attribute> attributes)
        {
            // When content is being created the content link is 0
            if (metadata.Model is IContent data && data.ContentLink.ID == 0)
            {
                foreach (var modelMetadata in metadata.Properties)
                {
                    var property = (ExtendedMetadata)modelMetadata;
                    if (property.Attributes.OfType<HideOnContentCreateAttribute>().Any())
                    {
                        property.ShowForEdit = false;
                    }
                }
            }
        }
    }
}