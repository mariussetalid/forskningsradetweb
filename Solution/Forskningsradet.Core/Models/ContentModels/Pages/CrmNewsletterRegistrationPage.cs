﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAnnotations;
using EPiServer.Web;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ContentModels.Support;

namespace Forskningsradet.Core.Models.ContentModels.Pages
{
    [SpecialPageType(
        GUID = "68CD27FB-95E3-4614-9C8B-E8220C1006BB",
        DisplayName = "Påmelding til nyhetsbrev (CRM)",
        Description = "Skjemaside for påmelding til nyhetsbrev.",
        Order = 155)]
    [ImageUrl(IconConstants.Thumbnails.NewsletterPage)]
    public class CrmNewsletterRegistrationPage : EditorialPage
    {
        [Display(
            Order = 10,
            GroupName = GroupNames.Content,
            Name = "Tittel",
            Description = "Overskrift på siden.")]
        [CultureSpecific]
        public virtual string Title { get; set; }

        [Display(
            Order = 20,
            GroupName = GroupNames.Content,
            Name = "Ingress",
            Description = "Introduksjon som vises øverst på siden.")]
        [UIHint(UIHint.Textarea)]
        [CultureSpecific]
        [XhtmlStringRestrictContentTypes]
        public virtual XhtmlString MainIntro { get; set; }

        [Display(
            Order = 30,
            GroupName = GroupNames.Content,
            Name = "Høyre blokkfelt",
            Description = "Blokkfelt i høyre marg.")]
        public virtual ContentArea RightBlockArea { get; set; }

        [Display(
            Order = 40,
            GroupName = GroupNames.Content,
            Name = "Overstyr valideringsmelding",
            Description = "Melding som vises dersom man har fylt inn skjemaet feil.")]
        [CultureSpecific]
        [UIHint(UIHint.Textarea)]
        public virtual string ValidationMessage { get; set; }

        [Display(
            Order = 50,
            Name = "Bekreftelsesside",
            Description = "Bekreftelsesside for påmelding til nyhetsbrev",
            GroupName = GroupNames.Content)]
        public virtual ContentReference ConfirmationPage { get; set; }

        public override string ReactComponentName() =>
            nameof(ReactModels.NewsletterCrmRegistrationPage);
    }
}
