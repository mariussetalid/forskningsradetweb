﻿using EPiServer.DataAnnotations;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ContentModels.Support;
using Forskningsradet.Core.Models.SearchModels;

namespace Forskningsradet.Core.Models.ContentModels.Pages
{
    [ContentPageType(
        GUID = "C0E9296F-1459-4DC7-9B97-17616C68C680",
        Order = 101,
        DisplayName = "Innholdsside",
        Description = "Standard artikkelsidemal for innhold.")]
    [ImageUrl(IconConstants.Thumbnails.InformationArticlePage)]
    public class InformationArticlePage : ArticlePage
    {
        public override SearchResultType ComputedSearchResultType => SearchResultType.Article;
        public override string ReactComponentName() => nameof(ReactModels.ArticlePage);
        public override string PartialReactComponentName() => nameof(ReactModels.ArticleBlock);
    }
}