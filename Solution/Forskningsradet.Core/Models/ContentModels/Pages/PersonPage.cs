using System.ComponentModel.DataAnnotations;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ContentModels.Support;
using Forskningsradet.Core.Models.Contracts;
using Forskningsradet.Core.Models.SearchModels;
using Geta.SEO.Sitemaps.Models;

namespace Forskningsradet.Core.Models.ContentModels.Pages
{
    [ContentPageType(
        GUID = "CD5088A4-D653-477C-83E7-3C70AF0C6DBB",
        DisplayName = "Personside",
        Description = "Kontaktinformasjon om en person som kan brukes som et innholdselement.",
        Order = 120)]
    [ImageUrl(IconConstants.Thumbnails.PersonPage)]
    public class PersonPage : EditorialPage, IPageHasNoViewIcon, IExcludeFromSitemap
    {
        [Display(
            Name = "Fornavn",
            Description = "Personens fornavn.",
            GroupName = GroupNames.Content,
            Order = 100)]
        public virtual string FirstName { get; set; }

        [Display(
            Name = "Etternavn",
            Description = "Personens etternavn.",
            GroupName = GroupNames.Content,
            Order = 110)]
        public virtual string LastName { get; set; }

        [Display(
            Name = "Stilling",
            Description = "Personens stilling.",
            GroupName = GroupNames.Content,
            Order = 120)]
        [CultureSpecific]
        public virtual string JobTitle { get; set; }

        [Display(
            Name = "Avdeling",
            Description = "Personens avdeling.",
            GroupName = GroupNames.Content,
            Order = 130)]
        [CultureSpecific]
        public virtual string Department { get; set; }

        [Display(
            Name = "Telefon",
            Description = "Personens telefonnummer.",
            GroupName = GroupNames.Content,
            Order = 140)]
        public virtual string Phone { get; set; }

        [Display(
            Name = "Mobil",
            Description = "Personens mobilnummer.",
            GroupName = GroupNames.Content,
            Order = 150)]
        public virtual string Mobile { get; set; }

        [Display(
            Name = "E-post",
            Description = "Personens epostadresse.",
            GroupName = GroupNames.Content,
            Order = 160)]
        public virtual string Email { get; set; }

        [Searchable]
        public string Letter => !string.IsNullOrWhiteSpace(LastName)
            ? LastName.Substring(0, 1).ToUpper()
            : string.Empty;

        public override SearchResultType ComputedSearchResultType =>
            SearchResultType.Person;

        public override string ReactComponentName() =>
            nameof(ReactModels.ContactInfo);

        #region Values used during import

        [Display(
            Name = "Ekstern id (Utg�r)",
            Description = "Tidligere GUID brukt som id. Framover skal ressursnummer benyttes.",
            GroupName = GroupNames.Import,
            Order = 100)]
        [Editable(false)]
        public virtual string ExternalId { get; set; }

        [Display(
            Name = "Ansattype",
            GroupName = GroupNames.Import,
            Order = 110)]
        [Editable(false)]
        public virtual string ResourceType { get; set; }

        [Display(
            Name = "Eksternt ressursnummer",
            Description = "Extern id",
            GroupName = GroupNames.Import,
            Order = 120)]
        [Editable(false)]
        public virtual string ResourceId { get; set; }

        [Display(
            Name = "Eksternt sist oppdatert",
            Description = "Sist oppdatert av Agresso.",
            GroupName = GroupNames.Import,
            Order = 130)]
        [Editable(false)]
        public virtual string ExternalLastUpdate { get; set; }

        #endregion

        public override void SetDefaultValues(ContentType contentType)
        {
            base.SetDefaultValues(contentType);
            SeoSettings.MetaNoIndex = true;
            SeoSettings.MetaNoFollow = true;
        }
    }
}