﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAnnotations;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ContentModels.Support;
using Forskningsradet.Core.Models.Contracts;

namespace Forskningsradet.Core.Models.ContentModels.Pages
{
    [SpecialPageType(GUID = "FCDC418E-5AA9-4AB0-9B14-5644BD02CAF5",
        DisplayName = "Arrangementer listeside",
        Description = "Lister opp arrangementer.",
        Order = 9020)]
    [ImageUrl(IconConstants.Thumbnails.ListWithFilter)]
    public class EventListPage : EditorialPage, IPageHasListIcon
    {
        [Display(
            Name = "Rotside for arrangementer",
            Description = "Her kan man angi en side som skal være rot for søket etter arrangementer. Bruk denne til å begrense hvor liten eller stor del av nettstedet listen skal hentes fra.",
            GroupName = GroupNames.Content,
            Order = 10)]
        public virtual PageReference PageRoot { get; set; }

        [Display(
            Name = "(Valgfri) Kategorigrupper for filter",
            Description = "Kategorier som skal brukes til filtrering",
            GroupName = GroupNames.Content,
            Order = 20)]
        public virtual CategoryList FilterCategoryList { get; set; }

        [Display(
           Name = "Tittel over filtervalg",
           Description = "Legg til en tittel her som vil befinne seg over filtreringsgruppene.",
           GroupName = GroupNames.Content,
           Order = 30)]
        [CultureSpecific]
        public virtual string FilterTitle { get; set; }

        [Display(
            Name = "Skjul tagger",
            Description = "Velg denne for å skjule de grå taggene som dukker opp under hvert arrangement i listen.",
            GroupName = GroupNames.Content,
            Order = 40)]
        public virtual bool HideEventTags { get; set; }

        public override string ReactComponentName() => nameof(ReactModels.EventListPage);
    }
}
