﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAnnotations;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ContentModels.Support;
using Forskningsradet.Core.Models.Contracts;

namespace Forskningsradet.Core.Models.ContentModels.Pages
{
    [SpecialPageType(
        GUID = "3808E9C6-7218-443E-A054-917FF75EE890",
        DisplayName = "Side for publikasjoner")]
    [AvailableContentTypes(Include = new[] { typeof(PublicationPage), typeof(FolderPage) })]
    [ImageUrl(IconConstants.Thumbnails.ListWithSearchAndFilterAndGroups)]
    public class PublicationListPage : EditorialPage, IPageHasListIcon
    {
        [Display(
            Name = "Rotside for publikasjoner",
            Description = "Her kan man angi en side som skal være rot for søket etter publikasjoner. Bruk denne til å begrense hvor liten eller stor del av nettstedet listen skal hentes fra.",
            GroupName = GroupNames.Content,
            Order = 10)]
        public virtual PageReference PageRoot { get; set; }

        [Display(
            Name = "Søk også i hele innholdet av filene til publikasjonene",
            Description = "Velg om søk med søkeord skal lete i innholdet til alle publikasjonene. Dette er et betydelig tregere søk, men kan gi bedre treff.",
            GroupName = GroupNames.Content,
            Order = 20)]
        public virtual bool IncludeContentOfAttachmentWhenSearching { get; set; }

        [Display(
            Name = "Tittel over filtervalg",
            Description = "Legg til en tittel her som vil befinne seg over filtreringsgruppene.",
            GroupName = GroupNames.Content,
            Order = 30)]
        [CultureSpecific]
        public virtual string FilterTitle { get; set; }

        public override string ReactComponentName()
            => nameof(ReactModels.PublicationsPage);
    }
}
