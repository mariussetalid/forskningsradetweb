﻿namespace Forskningsradet.Core.Models.ContentModels.Pages
{
    /// <summary>
    /// Base class for pages not which are not system pages, but are not content pages either.
    /// </summary>
    public abstract class NonContentPage : BasePageData
    {

    }
}