﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using EPiServer.DataAnnotations;
using EPiServer.Web;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ContentModels.Support;
using Forskningsradet.Core.Models.SearchModels;

namespace Forskningsradet.Core.Models.ContentModels.Pages
{
    [SpecialPageType(
        GUID = "18D9C61C-C2EE-4D2E-84F2-1DA203027149",
        DisplayName = "Nysgjerrigpermetoden landingsside",
        Description = "Sidemal for landingsside for Nysgjerrigpermetoden.",
        Order = 9002)]
    [ImageUrl(IconConstants.Thumbnails.ContentAreaPage)]
    public class NpMethodPage : EditorialPage
    {
        [Display(
            Order = 10,
            GroupName = GroupNames.Content,
            Name = "Overstyr sidens navn",
            Description = "Hvis fylt ut brukes denne i stedet for sidens navn i visning. Da kan du ha forskjellig navn i visning og i Episervertreet.")]
        [CultureSpecific]
        public virtual string Title { get; set; }

        [Display(
            Order = 20,
            GroupName = GroupNames.Content,
            Name = "Ingress",
            Description = "Introduksjon som vises øverst på siden.")]
        [UIHint(UIHint.Textarea)]
        [CultureSpecific]
        public virtual string MainIntro { get; set; }

        [Display(
            Name = "Tekst på lukke-knapp - desktop",
            Description = "Overstyr teks som står på knappen for å lukke et steg på desktop.",
            GroupName = GroupNames.Content,
            Order = 30)]
        [CultureSpecific]
        public virtual string CloseTextDesktop { get; set; }

        [Display(
            Name = "Tekst på lukke-knapp - mobil",
            Description = "Overstyr teks som står på knappen for å lukke et steg på mobil.",
            GroupName = GroupNames.Content,
            Order = 40)]
        [CultureSpecific]
        public virtual string CloseTextMobile { get; set; }

        [Display(
            Name = "Lenke",
            Description = "Valgfri lenke.",
            GroupName = GroupNames.Content,
            Order = 50)]
        public virtual EditLinkBlock EditLink { get; set; }

        [Display(
            Name = "Steg 1",
            Description = "Innhold på steg 1.",
            GroupName = GroupNames.Content,
            Order = 60)]
        public virtual NpStepBlock Step1 { get; set; }

        [Display(
            Name = "Steg 2",
            Description = "Innhold på steg 2.",
            GroupName = GroupNames.Content,
            Order = 70)]
        public virtual NpStepBlock Step2 { get; set; }

        [Display(
            Name = "Steg 3",
            Description = "Innhold på steg 3.",
            GroupName = GroupNames.Content,
            Order = 80)]
        public virtual NpStepBlock Step3 { get; set; }

        [Display(
            Name = "Steg 4",
            Description = "Innhold på steg 4.",
            GroupName = GroupNames.Content,
            Order = 90)]
        public virtual NpStepBlock Step4 { get; set; }

        [Display(
            Name = "Steg 5",
            Description = "Innhold på steg 5.",
            GroupName = GroupNames.Content,
            Order = 100)]
        public virtual NpStepBlock Step5 { get; set; }

        [Display(
            Name = "Steg 6",
            Description = "Innhold på steg 6.",
            GroupName = GroupNames.Content,
            Order = 110)]
        public virtual NpStepBlock Step6 { get; set; }

        public IEnumerable<NpStepBlock> Steps =>
            new[]
            {
                Step1,
                Step2,
                Step3,
                Step4,
                Step5,
                Step6
            };

        public override SearchResultType ComputedSearchResultType => SearchResultType.Landing;

        public override string ReactComponentName() => nameof(ReactModels.NpMethod);
    }
}