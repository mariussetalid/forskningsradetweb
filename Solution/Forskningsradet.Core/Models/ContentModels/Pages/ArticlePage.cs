﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAnnotations;
using EPiServer.Web;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ContentModels.Contracts;
using Forskningsradet.Core.Models.ContentModels.Support;

namespace Forskningsradet.Core.Models.ContentModels.Pages
{
    public abstract class ArticlePage : EditorialPage, ICanBeRenderedInSidebar
    {
        [Display(
            Order = 10,
            GroupName = GroupNames.Content,
            Name = "Forfatter",
            Description = "Hvis fylt ut vises forfatter i byline.")]
        public virtual string Author { get; set; }

        [Display(
            Order = 15,
            GroupName = GroupNames.Content,
            Name = "Oversatt av",
            Description = "Hvis fylt ut vises oversetter i byline.")]
        public virtual string TranslatedBy { get; set; }

        [Display(
            Order = 20,
            GroupName = GroupNames.Content,
            Name = "Ingress",
            Description = "Introduksjon som vises øverst på siden.")]
        [UIHint(UIHint.Textarea)]
        [CultureSpecific]
        public virtual string MainIntro { get; set; }

        [Display(
            Order = 30,
            GroupName = GroupNames.Content,
            Name = "Brødtekst",
            Description = "Hovedparten av sidens innhold, inkludert bilder.")]
        [UIHint(UIHint.Textarea)]
        [XhtmlStringMessageBlockRestriction]
        [CultureSpecific]
        public virtual XhtmlString MainBody { get; set; }

        [Display(
            Order = 40,
            GroupName = GroupNames.Content,
            Name = "Høyremeny",
            Description = "Blokkfelt i høyre marg.")]
        public virtual ContentArea RightBlockArea { get; set; }

        [Display(
            Order = 10,
            GroupName = GroupNames.SatellittInnstillinger,
            Name = "Vis kategoriene som er lagt til under Innholdsfanen",
            Description = "Velges for å automatisk vise fram kategorier som er satt på siden.")]
        public virtual bool ShowCategories { get; set; }

        [Display(
            Order = 1000,
            GroupName = GroupNames.MigratedContent,
            Name = "Øvrige kategorier",
            Description = "Fra import av gammelt innhold.")]
        public virtual IList<string> OvrigeKategorierArray { get; set; }

        [Display(
            Order = 1001,
            GroupName = GroupNames.MigratedContent,
            Name = "Temaord",
            Description = "Fra import av gammelt innhold.")]
        public virtual IList<string> Temaord { get; set; }

        [Display(
            Order = 1002,
            GroupName = GroupNames.MigratedContent,
            Name = "StikkTittel",
            Description = "Fra import av gammelt innhold.")]
        public virtual string StikkTittel { get; set; }

        [Display(
            Order = 1005,
            GroupName = GroupNames.MigratedContent,
            Name = "Nysgjerrigper - importerte felter",
            Description = "Felter for importerte data fra gamle artikler for Nysgjerrigper")]
        public virtual SatelliteArticleDataBlock MigratedSatelliteData { get; set; }

        public abstract string PartialReactComponentName();
    }
}