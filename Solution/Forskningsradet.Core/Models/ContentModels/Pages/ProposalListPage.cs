using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ContentModels.Support;
using Forskningsradet.Core.Models.Contracts;

namespace Forskningsradet.Core.Models.ContentModels.Pages
{
    [SpecialPageType(GUID = "1AE98E4B-4EFC-47D7-B06A-2FF4DEBD8973",
        DisplayName = "Utlysninger listeside",
        Description = "Lister opp utlysninger.",
        Order = 260)]
    [ImageUrl(IconConstants.Thumbnails.ListWithFilter)]
    public class ProposalListPage : EditorialPage, IPageHasListIcon
    {
        [Display(
            Name = "Rotside for utlysninger",
            Description = "Her kan man angi en side som skal være rot for søket etter utlysninger. Bruk denne til å begrense hvor liten eller stor del av nettstedet listen skal hentes fra.",
            GroupName = GroupNames.Content,
            Order = 10)]
        public virtual PageReference PageRoot { get; set; }

        [Display(
            Order = 12,
            GroupName = GroupNames.Content,
            Name = "Aktuelle fanetittel",
            Description = "Her setter man tittelen som kommer opp når man har valgt 'Aktuelle' fanen.")]
        [CultureSpecific]
        public virtual string CurrentSectionTitle { get; set; }

        [Display(
            Order = 14,
            GroupName = GroupNames.Content,
            Name = "Gjennomførte fanetittel",
            Description = "Her setter man tittelen som kommer opp når man har valgt 'Gjennomførte' fanen.")]
        [CultureSpecific]
        public virtual string ClosedSectionTitle { get; set; }

        [Display(
            Order = 16,
            GroupName = GroupNames.Content,
            Name = "Søknadsresultater fanetittel",
            Description = "Her setter man tittelen som kommer opp når man har valgt 'Søknadsresultater' fanen.")]
        [CultureSpecific]
        public virtual string ResultSectionTitle { get; set; }

        [Display(
            Order = 17,
            GroupName = GroupNames.Content,
            Name = "Tittel over filtervalg",
            Description = "Legg til en tittel her som vil befinne seg over filtreringsgruppene.")]
        [CultureSpecific]
        public virtual string FilterTitle { get; set; }

        [Display(
            Order = 20,
            GroupName = GroupNames.Content,
            Name = "Høyre blokkfelt",
            Description = "Blokkfelt i høyre marg")]
        public virtual ContentArea RightBlockArea { get; set; }

        [Display(
            Name = "Antall synlige utlysninger",
            Description = "Angir hvor mange utlysninger som er synlig for hver søknadstype før man må ekspandere innholdet.",
            GroupName = GroupNames.Content,
            Order = 100)]
        [Required]
        [Range(1, 100)]
        public virtual int NumberOfVisibleItems { get; set; }

        public override string ReactComponentName() => nameof(ReactModels.ProposalListPage);

        public override void SetDefaultValues(ContentType contentType)
        {
            base.SetDefaultValues(contentType);

            NumberOfVisibleItems = 4;
        }
    }
}