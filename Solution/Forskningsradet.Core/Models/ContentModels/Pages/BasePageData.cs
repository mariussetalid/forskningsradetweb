﻿using EPiServer.Core;

namespace Forskningsradet.Core.Models.ContentModels.Pages
{
    /// <summary>
    /// Base class for all pages in the project. 
    /// </summary>
    public abstract class BasePageData : PageData
    {

    }
}