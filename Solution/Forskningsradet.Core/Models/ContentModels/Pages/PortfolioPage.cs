﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAnnotations;
using EPiServer.SpecializedProperties;
using EPiServer.Web;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ContentModels.Support;
using Forskningsradet.Core.Models.SearchModels;

namespace Forskningsradet.Core.Models.ContentModels.Pages
{
    [ContentPageType(
        GUID = "0ABD3A05-CC26-4271-9FF0-BF7F93295B65",
        DisplayName = "Porteføljeside",
        Description = "Sidemal for portefølje.",
        Order = 108)]
    [ImageUrl(IconConstants.Thumbnails.ContentAreaPage)]
    public class PortfolioPage : EditorialPage
    {
        [Display(
            Order = 10,
            GroupName = GroupNames.Content,
            Name = "Overstyr sidens navn",
            Description = "Hvis fylt ut brukes denne i stedet for sidens navn i visning. Da kan du ha forskjellig navn i visning og i Episervertreet.")]
        [CultureSpecific]
        public virtual string Title { get; set; }

        [Display(
            Order = 20,
            GroupName = GroupNames.Content,
            Name = "Overstyr tittel. Bruk _ for å sette hvor et langt ord kan deles",
            Description = "Hvis fylt ut brukes denne som overskrift på siden. Bruk _ for å bestemme hvor et langt ord skal deles for mobilvisning.")]
        [CultureSpecific]
        [Searchable(false)]
        public virtual string TitleWithCustomSeparator { get; set; }

        [Display(
            Order = 30,
            GroupName = GroupNames.Content,
            Name = "Ingress",
            Description = "Introduksjon som vises øverst på siden.")]
        [UIHint(UIHint.Textarea)]
        [CultureSpecific]
        public virtual string MainIntro { get; set; }

        [Display(
            Name = "Lenker (maks 2)",
            Description = "Lenker som ligger øverst på siden.",
            GroupName = GroupNames.Content,
            Order = 40)]
        [CultureSpecific]
        [MaxItems(2)]
        public virtual LinkItemCollection LinkItems { get; set; }

        [Display(
            Order = 50,
            GroupName = GroupNames.Content,
            Name = "Blokkområde",
            Description = "Blokkområdet som utgjør hoveddelen av siden.")]
        public virtual ContentArea MainContent { get; set; }

        public override SearchResultType ComputedSearchResultType => SearchResultType.Landing;

        public override string ReactComponentName() => nameof(ReactModels.ContentAreaPage);
    }
}