﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using EPiServer.Cms.Shell.UI.ObjectEditing.EditorDescriptors;
using EPiServer.DataAnnotations;
using EPiServer.Shell.ObjectEditing;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ContentModels.PropertyList;
using Forskningsradet.Core.Models.ContentModels.Support;
using Forskningsradet.Core.Models.Contracts;

namespace Forskningsradet.Core.Models.ContentModels.Pages
{
    [SpecialPageType(
        GUID = "7F6ED486-BF6F-475A-A8D7-B0D14924999D",
        DisplayName = "Kategorioversettelser",
        Description = "Side for å oversette kategorier",
        Order = 9070)]
    [ImageUrl(IconConstants.Thumbnails.SettingsPage)]
    public class CategoryTranslationPage : BasePageData, IPageHasNoView, IPageHasSettingsIcon
    {
        [Display(
            Name = "Kategorioversettelser - Tema",
            GroupName = GroupNames.Content,
            Order = 30)]
        [EditorDescriptor(EditorDescriptorType = typeof(CollectionEditorDescriptor<CategoryTranslationSubject>))]
        public virtual IList<CategoryTranslationSubject> Subjects { get; set; }

        [Display(
            Name = "Kategorioversettelser - Målgrupppe",
            GroupName = GroupNames.Content,
            Order = 40)]
        [EditorDescriptor(EditorDescriptorType = typeof(CollectionEditorDescriptor<CategoryTranslationTargetGroup>))]
        public virtual IList<CategoryTranslationTargetGroup> TargetGroups { get; set; }

        [Display(
            Name = "Kategorioversettelser - Øvrige",
            GroupName = GroupNames.Content,
            Order = 50)]
        [EditorDescriptor(EditorDescriptorType = typeof(CollectionEditorDescriptor<CategoryTranslationOther>))]
        public virtual IList<CategoryTranslationOther> Other { get; set; }

        #region ToBeRemoved

        [Display(
            Name = "Tema - Skal fjernes",
            GroupName = GroupNames.Content,
            Order = 130)]
        [EditorDescriptor(EditorDescriptorType = typeof(CollectionEditorDescriptor<CategoryTranslationSubject>))]
        [CultureSpecific]
        [Editable(false)]
        public virtual IList<CategoryTranslationSubject> SubjectTranslations { get; set; }

        [Display(
            Name = "Målgrupppe - Skal fjernes",
            GroupName = GroupNames.Content,
            Order = 140)]
        [EditorDescriptor(EditorDescriptorType = typeof(CollectionEditorDescriptor<CategoryTranslationTargetGroup>))]
        [CultureSpecific]
        [Editable(false)]
        public virtual IList<CategoryTranslationTargetGroup> TargetGroupTranslations { get; set; }

        [Display(
            Name = "Øvrige - Skal fjernes",
            GroupName = GroupNames.Content,
            Order = 150)]
        [EditorDescriptor(EditorDescriptorType = typeof(CollectionEditorDescriptor<CategoryTranslationOther>))]
        [CultureSpecific]
        [Editable(false)]
        public virtual IList<CategoryTranslationOther> OtherTranslations { get; set; }

        #endregion
    }
}
