using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAnnotations;
using EPiServer.Shell.ObjectEditing;
using EPiServer.SpecializedProperties;
using EPiServer.Web;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.EditorDescriptors;
using Forskningsradet.Core.Extensions;
using Forskningsradet.Core.Models.ApiModels.Evurdering;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ContentModels.Media;
using Forskningsradet.Core.Models.ContentModels.Support;
using Forskningsradet.Core.Models.SearchModels;

namespace Forskningsradet.Core.Models.ContentModels.Pages
{
    [ContentPageType(
        GUID = "D832792A-B5B9-45E2-AAA1-C1A8C2E424BD",
        DisplayName = "Utlysning",
        Description = "Sidemal for en utlysning.",
        Order = 200)]
    [ImageUrl(IconConstants.Thumbnails.ProposalPage)]
    public class ProposalPage : ProposalBasePage
    {
        #region Content

        [Display(
            Name = "Skisseutlysning",
            Description = "Skisse for denne utlysningen. For at denne skal vises må utlysningen ha skissekrav i eVurdering.",
            GroupName = GroupNames.Content,
            Order = 50)]
        [AllowedTypes(typeof(ProposalPage))]
        public virtual PageReference ProjectOutline { get; set; }

        [Display(
            Name = "Skisse for",
            Description = "Utlysninger denne utlysningen er skisse for",
            GroupName = GroupNames.Content,
            Order = 60)]
        [BackingType(typeof(PropertyContentReferenceList))]
        [AllowedTypes(typeof(ProposalPage))]
        public virtual IList<ContentReference> ChildProjectOutlines { get; set; }

        [Display(
            Name = "Utløpsdato for løpende",
            GroupName = GroupNames.Content,
            Order = 100)]
        public virtual DateTime? DeadlineOverride { get; set; }

        [Display(
            Name = "Tekst til utlyst beløp",
            Order = 120)]
        [CultureSpecific]
        [UIHint(UIHint.Textarea)]
        public virtual string ProposalAmountText { get; set; }

        [Display(
            Order = 121,
            GroupName = GroupNames.Content,
            Name = "Lenketekst til utlyst beløp")]
        [CultureSpecific]
        [UIHint(UIHint.Textarea)]
        public virtual string ProposalAmountUrlText { get; set; }

        [Display(
            Name = "Neste utlysning",
            GroupName = GroupNames.Content,
            Order = 130)]
        [AllowedTypes(typeof(ProposalPage))]
        public virtual PageReference UpcomingProposalReference { get; set; }

        [Display(
            Order = 140,
            GroupName = GroupNames.Content,
            Name = "Utlysningsmeldinger",
            Description = "Legg inn blokker av typen Lokal driftsmelding")]
        [AllowedTypes(typeof(LocalMessageBlock), typeof(GlobalMessageBlock))]
        public virtual ContentArea Messages { get; set; }

        [Display(
            Order = 145,
            GroupName = GroupNames.Content,
            Name = "Kontakt for utlysning")]
        public virtual ContactBlock Contact { get; set; }

        [Display(
            Order = 150,
            GroupName = GroupNames.Content,
            Name = "Formål")]
        [XhtmlStringRestrictContentTypes(AllowInXhtmlString.SomeMedia)]
        [CultureSpecific]
        public virtual XhtmlString Purpose { get; set; }

        [Display(
            Name = "Tidslinje",
            GroupName = GroupNames.Content,
            Order = 160)]
        public virtual TimeLineBlock Timeline { get; set; }

        #endregion

        #region Import

        [Display(
            Name = "ID eVurdering",
            GroupName = GroupNames.Import,
            Order = 100)]
        [Editable(false)]
        [Searchable(false)]
        public override string Id { get; set; }

        [Display(
            Name = "Søknadstype",
            GroupName = GroupNames.Import,
            Order = 110)]
        [Editable(false)]
        public override PageReference ApplicationTypeReference { get; set; }

        [Display(
            Name = "Utlyst søknadstypeid",
            GroupName = GroupNames.Import,
            Order = 115)]
        [Editable(false)]
        [Searchable(false)]
        public virtual string SpecificApplicationTypeId { get; set; }

        [Display(
            Name = "Status",
            GroupName = GroupNames.Import,
            Order = 120)]
        [BackingType(typeof(PropertyNumber))]
        [EditorDescriptor(EditorDescriptorType = typeof(EnumEditorDescriptor<ProposalState>))]
        [Editable(false)]
        public override ProposalState ProposalState { get; set; }

        [Display(
            Name = "Type frist",
            GroupName = GroupNames.Import,
            Order = 130)]
        [BackingType(typeof(PropertyNumber))]
        [EditorDescriptor(EditorDescriptorType = typeof(EnumEditorDescriptor<DeadlineType>))]
        [Editable(false)]
        public override DeadlineType DeadlineType { get; set; }

        [Display(
            Name = "Utlyst beløp",
            GroupName = GroupNames.Import,
            Order = 140)]
        [Editable(false)]
        public virtual string ProposalAmount { get; set; }

        [Display(
            Name = "Minimum søknadsbeløp",
            GroupName = GroupNames.Import,
            Order = 150)]
        [Editable(false)]
        public virtual int MinApplicationAmount { get; set; }

        [Display(
            Name = "Maksimalt søknadsbeløp",
            GroupName = GroupNames.Import,
            Order = 160)]
        [Editable(false)]
        public virtual int MaxApplicationAmount { get; set; }

        [Display(
            Name = "Minimumslengde på prosjektet",
            GroupName = GroupNames.Import,
            Order = 170)]
        [Editable(false)]
        public virtual int MinProjectLength { get; set; }

        [Display(
            Name = "Maksimal lengde på prosjektet",
            GroupName = GroupNames.Import,
            Order = 180)]
        [Editable(false)]
        public virtual int MaxProjectLength { get; set; }

        [Display(
            Name = "Startdato",
            GroupName = GroupNames.Import,
            Order = 190)]
        [Editable(false)]
        public virtual DateTime? StartDate { get; set; }

        [Display(
            Name = "Søknadsfrist",
            GroupName = GroupNames.Import,
            Order = 200)]
        [Editable(false)]
        public override DateTime? Deadline { get; set; }

        [Display(
            Name = "Tidligste startdato",
            GroupName = GroupNames.Import,
            Order = 210)]
        [Editable(false)]
        public virtual DateTime? EarliestProjectPeriodStart { get; set; }

        [Display(
            Name = "Seneste startdato",
            GroupName = GroupNames.Import,
            Order = 220)]
        [Editable(false)]
        public virtual DateTime? LatestProjectPeriodStart { get; set; }

        [Display(
            Name = "Seneste sluttdato",
            GroupName = GroupNames.Import,
            Order = 230)]
        [Editable(false)]
        public virtual DateTime? LatestProjectPeriodEnd { get; set; }

        [Display(
            Name = "Aktivitet",
            GroupName = GroupNames.Import,
            Order = 240)]
        [Editable(false)]
        public virtual string Activity { get; set; }

        [Display(
            Name = "RFF-utlysning",
            GroupName = GroupNames.Import,
            Order = 250)]
        [Editable(false)]
        public virtual bool IsRff { get; set; }

        [Display(
            Name = "Skissekrav",
            GroupName = GroupNames.Import,
            Order = 260)]
        [BackingType(typeof(PropertyNumber))]
        [EditorDescriptor(EditorDescriptorType = typeof(EnumEditorDescriptor<ProjectOutlineRequirement>))]
        [Editable(false)]
        public virtual ProjectOutlineRequirement ProjectOutlineRequirement { get; set; }

        [Display(
            Name = "Oppdatering sendt fra eVurdering",
            GroupName = GroupNames.Import,
            Order = 270)]
        [Editable(false)]
        public virtual DateTime? UpdatedAt { get; set; }

        #endregion

        #region Proposal details

        [Display(
            Order = 100,
            GroupName = GroupNames.ProposalDetails,
            Name = "Om utlysningen")]
        [XhtmlStringRestrictContentTypes(AllowInXhtmlString.SomeMedia)]
        [CultureSpecific]
        public virtual XhtmlString About { get; set; }

        [Display(
            Name = "Søknadsprosessen (Prosessblokk)",
            Description = "Legg inn blokk av typen Prosessblokk.",
            GroupName = GroupNames.ProposalDetails,
            Order = 105)]
        [MaxItems(1)]
        [AllowedTypes(typeof(ProcessBlock))]
        public virtual ContentArea ProcessContentArea { get; set; }

        [Display(
            Order = 110,
            GroupName = GroupNames.ProposalDetails,
            Name = "Hvem kan søke")]
        [XhtmlStringRestrictContentTypes(AllowInXhtmlString.SomeMedia)]
        [CultureSpecific]
        public virtual XhtmlString IntendedApplicants { get; set; }

        [Display(
            Order = 120,
            GroupName = GroupNames.ProposalDetails,
            Name = "Hvem kan delta i prosjektet")]
        [XhtmlStringRestrictContentTypes(AllowInXhtmlString.SomeMedia)]
        [CultureSpecific]
        public virtual XhtmlString ProjectParticipants { get; set; }

        [Display(
            Order = 130,
            GroupName = GroupNames.ProposalDetails,
            Name = "Hva kan du søke om")]
        [XhtmlStringRestrictContentTypes(AllowInXhtmlString.SomeMedia)]
        [CultureSpecific]
        public virtual XhtmlString Subject { get; set; }

        [Display(
            Order = 140,
            GroupName = GroupNames.ProposalDetails,
            Name = "Arkivering av forskningsdata")]
        [XhtmlStringRestrictContentTypes(AllowInXhtmlString.SomeMedia)]
        [CultureSpecific]
        public virtual XhtmlString ArchivingOfDataText { get; set; }

        [Display(
            Order = 150,
            GroupName = GroupNames.ProposalDetails,
            Name = "Tittel øvrig utlysningstekst")]
        [CultureSpecific]
        public virtual string AdditionalTextTitle { get; set; }

        [Display(
            Order = 155,
            GroupName = GroupNames.ProposalDetails,
            Name = "Øvrig utlysningstekst")]
        [XhtmlStringRestrictContentTypes(AllowInXhtmlString.SomeMedia)]
        [CultureSpecific]
        public virtual XhtmlString AdditionalText { get; set; }

        [Display(
            Order = 160,
            GroupName = GroupNames.ProposalDetails,
            Name = "Beskrivelse av aktuelle temaer")]
        [XhtmlStringRestrictContentTypes(AllowInXhtmlString.SomeMedia)]
        [CultureSpecific]
        public virtual XhtmlString RelevantSubjectsText { get; set; }

        [Display(
            Name = "Temablokker",
            GroupName = GroupNames.ProposalDetails,
            Order = 170)]
        [AllowedTypes(typeof(SubjectBlock))]
        public virtual ContentArea SubjectBlocks { get; set; }

        [Display(
            Name = "Alternativ visning av temablokker",
            GroupName = GroupNames.ProposalDetails,
            Order = 175)]
        public virtual bool UseAlternativeSubjectBlockPresentation { get; set; }

        [Display(
            Order = 180,
            GroupName = GroupNames.ProposalDetails,
            Name = "Krav til utforming")]
        [XhtmlStringRestrictContentTypes(AllowInXhtmlString.SomeMedia)]
        [CultureSpecific]
        public virtual XhtmlString DesignRequirements { get; set; }

        [Display(
            Order = 190,
            GroupName = GroupNames.ProposalDetails,
            Name = "Vurderingskriterier")]
        [XhtmlStringRestrictContentTypes(AllowInXhtmlString.SomeMedia)]
        [CultureSpecific]
        public virtual XhtmlString AssessmentCriteriaText { get; set; }

        [Display(
            Name = "Kriterier",
            GroupName = GroupNames.ProposalDetails,
            Order = 200)]
        [AllowedTypes(typeof(AssessmentCriterionBlock))]
        public virtual ContentArea AssessmentCriteria { get; set; }

        [Display(
            Order = 205,
            GroupName = GroupNames.ProposalDetails,
            Name = "Generelle vurderingskriterier")]
        [XhtmlStringRestrictContentTypes(AllowInXhtmlString.SomeMedia)]
        [CultureSpecific]
        public virtual XhtmlString GeneralAssessmentCriteria { get; set; }

        [Display(
            Order = 210,
            GroupName = GroupNames.ProposalDetails,
            Name = "Behandlingsprosedyre")]
        [XhtmlStringRestrictContentTypes(AllowInXhtmlString.SomeMedia)]
        [CultureSpecific]
        public virtual XhtmlString Evaluation { get; set; }

        [Display(
            Order = 220,
            GroupName = GroupNames.ProposalDetails,
            Name = "Tekst til opprett søknad",
            Prompt = "Søknader til...")]
        [UIHint(UIHint.Textarea)]
        [CultureSpecific]
        public virtual string ApplicationText { get; set; }

        [Display(
            Order = 230,
            GroupName = GroupNames.ProposalDetails,
            Name = "Søknadsmaler")]
        [AllowedTypes(typeof(GenericMedia))]
        public virtual IList<ContentReference> ApplicationTemplates { get; set; }

        [Display(
            Name = "Blokkområde",
            GroupName = GroupNames.ProposalDetails,
            Order = 240)]
        public virtual ContentArea Content { get; set; }

        #endregion

        #region Application processing

        [Display(
            Order = 100,
            GroupName = GroupNames.ApplicationProcessing,
            Name = "Om søknadsbehandlingen")]
        [XhtmlStringRestrictContentTypes(AllowInXhtmlString.SomeMedia)]
        [CultureSpecific]
        public virtual XhtmlString ApplicationProcessingText { get; set; }

        [Display(
            Name = "Innhold fra Tableau",
            GroupName = GroupNames.ApplicationProcessing,
            Order = 110)]
        [AllowedTypes(typeof(TableauBlock))]
        public virtual ContentArea TableauContent { get; set; }

        #endregion

        #region Application results

        [Display(
            Order = 100,
            GroupName = GroupNames.ApplicationResults,
            Name = "Om søknadsresultatene")]
        [XhtmlStringRestrictContentTypes(AllowInXhtmlString.SomeMedia)]
        [CultureSpecific]
        public virtual XhtmlString ApplicationResultsText { get; set; }

        [Display(
            Name = "Søknadsresultat",
            GroupName = GroupNames.ApplicationResults,
            Order = 110)]
        [AllowedTypes(typeof(ApplicationResultPage))]
        public virtual PageReference ApplicationResult { get; set; }

        #endregion

        #region Listevisning

        [Display(
            Order = 50,
            GroupName = GroupNames.Listevisning,
            Name = "Meldinger for utlysningslisteside",
            Description = "Legg inn blokker av typen Lokal driftsmelding")]
        [AllowedTypes(typeof(LocalMessageBlock), typeof(GlobalMessageBlock))]
        public virtual ContentArea ListMessages { get; set; }

        #endregion

        [Searchable]
        public override DateTime? DeadlineComputed =>
            Deadline ?? DeadlineOverride;

        [Searchable]
        public override DateTime? DeadlineComputedForSearchFacet => DeadlineComputed;

        [Searchable]
        public string DeadlineComputedAsString =>
            DeadlineComputed?.ToDisplayDate().ToNorwegianDateString() ?? string.Empty;

        [Searchable]
        public override string ComputedDeadlineType =>
            DeadlineType.ToString();

        [Searchable]
        public bool HasApplicationResult =>
            !PageReference.IsNullOrEmpty(ApplicationResult);

        public override SearchResultType ComputedSearchResultType =>
            SearchResultType.Proposal;

        public override string ReactComponentName() => nameof(ReactModels.ProposalPage);
    }
}