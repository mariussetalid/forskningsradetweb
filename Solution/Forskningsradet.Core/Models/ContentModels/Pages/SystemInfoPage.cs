﻿using EPiServer.DataAnnotations;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ContentModels.Support;
using Forskningsradet.Core.Models.Contracts;
using Geta.SEO.Sitemaps.Models;

namespace Forskningsradet.Core.Models.ContentModels.Pages
{
    [SystemPageType(
        GUID = "C4A1B1C3-C201-4583-B3F7-6F49A26933DA",
        DisplayName = "[System] Systeminfo",
        Description = "Systemside som viser info om nettstedet. Ikke til redaksjonell bruk.",
        Order = 10000,
        AvailableInEditMode = false)]
    [ImageUrl(IconConstants.Thumbnails.SettingsPage)]
    public class SystemInfoPage : NonContentPage, IPageHasSettingsIcon, IExcludeFromSitemap
    {

    }
}