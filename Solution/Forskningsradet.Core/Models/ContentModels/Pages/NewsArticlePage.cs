﻿using EPiServer.DataAnnotations;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ContentModels.Support;
using Forskningsradet.Core.Models.SearchModels;

namespace Forskningsradet.Core.Models.ContentModels.Pages
{
    [ContentPageType(
        GUID = "A1426B5D-278D-49DD-B46B-95050582B9BD",
        DisplayName = "Nyhetsside",
        Description = "Sidemal for nyheter.",
        Order = 105)]
    [ImageUrl(IconConstants.Thumbnails.NewsArticlePage)]
    public class NewsArticlePage : ArticlePage
    {
        public override SearchResultType ComputedSearchResultType => SearchResultType.News;
        public override string ReactComponentName() => nameof(ReactModels.ArticlePage);
        public override string PartialReactComponentName() => nameof(ReactModels.ArticleBlock);
    }
}