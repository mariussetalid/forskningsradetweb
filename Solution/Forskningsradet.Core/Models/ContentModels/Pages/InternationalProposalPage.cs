﻿using System;
using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAnnotations;
using EPiServer.Shell.ObjectEditing;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.EditorDescriptors;
using Forskningsradet.Core.Models.ApiModels.Evurdering;
using Forskningsradet.Core.Models.ContentModels.Support;
using Forskningsradet.Core.Models.SearchModels;
using Forskningsradet.Core.Extensions;
using EPiServer.DataAbstraction;
using Forskningsradet.Core.Models.ContentModels.Blocks;

namespace Forskningsradet.Core.Models.ContentModels.Pages
{
    [ContentPageType(
        GUID = "307A6B1E-ECE4-4BAF-8A26-4C9C73C87173",
        DisplayName = "Internasjonal Utlysning",
        Description = "Sidemal for internasjonal utlysning.",
        Order = 210)]
    [ImageUrl(IconConstants.Thumbnails.ProposalPage)]
    public class InternationalProposalPage : ProposalBasePage
    {
        [Display(
            Name = "Søknadstype",
            GroupName = GroupNames.Content,
            Order = 10)]
        [AllowedTypes(typeof(ApplicationTypePage))]
        [Required]
        public override PageReference ApplicationTypeReference { get; set; }

        [Display(
            Name = "Visningsnavn for aktiveringsdato",
            GroupName = GroupNames.Content,
            Order = 20)]
        [CultureSpecific]
        public virtual string ActivationDateDisplayName { get; set; }

        [Display(
            Name = "Aktiveringsperiode (for Aktiveringsdato)",
            GroupName = GroupNames.Content,
            Order = 30)]
        [EditorDescriptor(EditorDescriptorType = typeof(EnumEditorDescriptor<TimeUnit>))]
        public virtual TimeUnit ActivationTimeUnit { get; set; }

        [Display(
            Name = "Aktiveringsdato",
            Description = "Velg en dato og ut i fra hva som er valgt i Aktiveringsperiode, så hentes måned, ukenummer eller dato",
            GroupName = GroupNames.Content,
            Order = 40)]
        public virtual DateTime? ActivationDate { get; set; }

        [Display(
            Name = "Visningsnavn for søknadsfrist 1",
            GroupName = GroupNames.Content,
            Order = 50)]
        [CultureSpecific]
        public virtual string FirstDeadlineDisplayName { get; set; }

        [Display(
            Name = "Søknadsfrist 1",
            GroupName = GroupNames.Content,
            Order = 60)]
        public virtual DateTime? FirstDeadline { get; set; }

        [Display(
            Name = "Visningsnavn for søknadsfrist (2)",
            GroupName = GroupNames.Content,
            Order = 70)]
        [CultureSpecific]
        public virtual string DeadlineDisplayName { get; set; }

        [Display(
            Name = "Søknadsfrist (2)",
            GroupName = GroupNames.Content,
            Order = 80)]
        public override DateTime? Deadline { get; set; }

        [Display(
            Name = "Program",
            GroupName = GroupNames.Content,
            Order = 90)]
        [CultureSpecific]
        public virtual string Program { get; set; }

        [Display(
           Name = "Kontaktperson",
           GroupName = GroupNames.Content,
           Order = 100)]
        [AllowedTypes(typeof(PersonPage))]
        public virtual PageReference ContactPerson { get; set; }

        [Display(
            Name = "Innhold",
            GroupName = GroupNames.Content,
            Order = 110)]
        public virtual InternationalProposalPageTextBlock TextBlock { get; set; }

        [Ignore]
        public override ProposalState ProposalState 
        { 
            get
            {
                return Deadline?.IsPast() == true ? 
                    ProposalState.Completed 
                    :  ActivationDate?.IsPast() == true
                        ? ProposalState.Active 
                        : ProposalState.Planned;
            }
            set { }
        }

        [Ignore]
        public override DeadlineType DeadlineType
        {
            get
            {
                return Deadline is null && FirstDeadline is null
                    ? DeadlineType.NotSet
                    : DeadlineType.Date;
            }
            set => base.DeadlineType = value;
        }

        [Ignore]
        public override string Id { get; set; }

        [Searchable]
        public override string ComputedDeadlineType => DeadlineType.ToString();

        [Searchable]
        public override DateTime? DeadlineComputed
        {
            get
            {
                return FirstDeadline?.IsPast() == true
                    ? Deadline ?? FirstDeadline
                    : FirstDeadline ?? Deadline;
            }
        }

        [Searchable]
        public override DateTime? DeadlineComputedForSearchFacet => null;

        public override SearchResultType ComputedSearchResultType => SearchResultType.Proposal;

        public override string ReactComponentName() => nameof(ReactModels.ProposalPage);

        public override void SetDefaultValues(ContentType contentType)
        {
            base.SetDefaultValues(contentType);
            Id = Guid.NewGuid().ToString();
        }
    }

    public enum TimeUnit
    {
        Date = 0,
        Week = 1,
        Month = 2
    }
}
