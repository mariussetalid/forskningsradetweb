﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.Shell.ObjectEditing;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ContentModels.Support;

namespace Forskningsradet.Core.Models.ContentModels.Pages
{
    [SpecialPageType(
        GUID = "BA9245C9-0605-4358-8A28-9DD384620642",
        DisplayName = "RSS feed",
        Description = "Feed for et utvalg av innhold.",
        Order = 9015)]
    [AvailableContentTypes(Availability.None)]
    [ImageUrl(IconConstants.Thumbnails.RssPage)]
    public class RssPage : BasePageData
    {
        [Display(
            Name = "Beskrivelse",
            Description = "Valgfri beskrivelse av RSSen, kommer ut i starten av RSS feeden.",
            GroupName = GroupNames.Content,
            Order = 10)]
        [CultureSpecific]
        public virtual string Description { get; set; }

        [Display(
            Name = "Rotside for søket",
            Description = "Siden hvor alle resultater skal ligge under.",
            GroupName = GroupNames.Content,
            Order = 20)]
        public virtual PageReference PageRoot { get; set; }

        [Display(
            Name = "Antall resultater som vises",
            Description = "Begrenser totalt antall viste elementer.",
            GroupName = GroupNames.Content,
            Order = 30)]
        public virtual int NumberOfVisibleItems { get; set; }

        [Display(
            Name = "Sidetyper som skal vises",
            Description = "Velg hvilke sidetyper sider som skal inkluderes kan være av.",
            GroupName = GroupNames.Content,
            Order = 40)]
        [CustomPageType(CustomPageTypeSet.Editorial)]
        [SelectMany(SelectionFactoryType = typeof(Factories.CustomPageTypeSelectionFactory))]
        public virtual string PageTypes { get; set; }

        [Display(
            Name = "Manuell liste",
            Description = "Overstyrer innholdet istedenfor å søke",
            GroupName = GroupNames.Content,
            Order = 50)]
        [AllowedTypes(typeof(EditorialPage))]
        public virtual ContentArea ContentArea { get; set; }
    }
}