using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using Forskningsradet.Core.Models.ContentModels.Support;
using Forskningsradet.Core.Models.Contracts;

namespace Forskningsradet.Core.Models.ContentModels.Pages
{
    [SpecialPageType(GUID = "C4F8CDD8-43FE-42B5-A14E-0100841C9DF6",
        DisplayName = "Søknadstype",
        Description = "Søknadstype brukt til å gruppere utlysninger.",
        Order = 270)]
    [AvailableContentTypes(Availability.None)]
    public class ApplicationTypePage : BasePageData, IPageHasNoView, IPageHasNoViewIcon
    {
        [Display(
            Order = 10,
            GroupName = GroupNames.Import,
            Name = "Id")]
        [Editable(false)]
        public virtual string Id { get; set; }

        [Display(
            Order = 20,
            GroupName = GroupNames.Content,
            Name = "Undertittel",
            Description = "Sidens undertittel.")]
        [CultureSpecific]
        public virtual string Subtitle { get; set; }

        [Display(
            Order = 30,
            GroupName = GroupNames.Content,
            Name = "Tekst",
            Description = "Sidens tekst.")]
        [CultureSpecific]
        [XhtmlStringRestrictContentTypes]
        public virtual XhtmlString Text { get; set; }

        [Display(
            Order = 40,
            GroupName = GroupNames.Content,
            Name = "Artikkel",
            Description = "Referanse til en artikkelside som beskriver søknadstypen")]
        [AllowedTypes(typeof(InformationArticlePage))]
        public virtual PageReference ArticlePageReference { get; set; }
    }
}