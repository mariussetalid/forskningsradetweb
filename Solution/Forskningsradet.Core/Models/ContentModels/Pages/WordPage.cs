﻿using System.ComponentModel.DataAnnotations;
using EPiServer.DataAnnotations;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ContentModels.Support;
using Forskningsradet.Core.Models.Contracts;
using Geta.SEO.Sitemaps.Models;

namespace Forskningsradet.Core.Models.ContentModels.Pages
{
    [SpecialPageType(
        GUID = "2CD5D626-3288-465E-A659-BAB010F84640",
        DisplayName = "Ord i ordlisten",
        Description = "Et ord som skal kunne vises på en ordlisteside.",
        Order = 9017)]
    [ImageUrl(IconConstants.Thumbnails.WordPage)]
    public class WordPage : BasePageData, IPageHasNoViewIcon, IExcludeFromSitemap
    {
        [Display(
            Order = 10,
            GroupName = GroupNames.Content,
            Name = "Ingress")]
        [CultureSpecific]
        public virtual string MainIntro { get; set; }

        [Searchable]
        public string Letter => !string.IsNullOrWhiteSpace(PageName)
            ? PageName.Substring(0, 1).ToUpper()
            : string.Empty;
    }
}
