﻿using System.ComponentModel.DataAnnotations;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAnnotations;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ContentModels.Support;
using Forskningsradet.Core.Models.Contracts;

namespace Forskningsradet.Core.Models.ContentModels.Pages
{
    [SpecialPageType(
        GUID = "6CAD4BAA-27F0-4F1D-B088-349DB3595387",
        DisplayName = "Menypunkt",
        Description = "Representerer et punkt i en meny.",
        Order = 9050)]
    [AvailableContentTypes(Include = new[] { typeof(MenuStructurePage) })]
    [ImageUrl(IconConstants.Thumbnails.MenuPage)]
    public class MenuStructurePage : NonContentPage, IPageHasNoView
    {
        [Display(
            Order = 10,
            GroupName = GroupNames.Content,
            Name = "Lenke til side i løsningen",
            Description = "Hvis satt lenker menypunktet til en side i nettstedet.")]
        public virtual PageReference LinkToPage { get; set; }

        [Display(
            Order = 15,
            GroupName = GroupNames.Content,
            Name = "Vis navnet på siden det lenkes til",
            Description = "Hvis satt brukes navnet på siden det lenkes til i stedet for navnet på dette menypunktet.")]
        public virtual bool UseLinkPageName { get; set; }

        [Display(
            Order = 20,
            GroupName = GroupNames.Content,
            Name = "Lenke til ekstern adresse",
            Description = "Url som peker på andre ting enn sider i denne løsningen")]
        public virtual Url UrlLink { get; set; }
    }
}