﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.Filters;
using Forskningsradet.Core.Models.ContentModels.Support;
using Forskningsradet.Core.Models.SearchModels;

namespace Forskningsradet.Core.Models.ContentModels.Pages
{
    public abstract class LargeDocumentPageBase : EditorialPage
    {
        [Display(
            Order = 10,
            GroupName = GroupNames.Content,
            Name = "Tittel",
            Description = "Tittel på siden")]
        [CultureSpecific]
        public virtual string Title { get; set; }

        [Display(
            Order = 20,
            GroupName = GroupNames.Content,
            Name = "Brødtekst",
            Description = "Brødtekst med støtte for blokker")]
        [XhtmlStringMessageBlockRestriction]
        [CultureSpecific]
        public virtual XhtmlString MainBody { get; set; }

        public override SearchResultType ComputedSearchResultType => SearchResultType.None;

        public override string ReactComponentName() => nameof(ReactModels.LargeDocumentsPage);

        public override void SetDefaultValues(ContentType contentType)
        {
            base.SetDefaultValues(contentType);

            ChildSortOrder = FilterSortOrder.Index;
        }
    }
}
