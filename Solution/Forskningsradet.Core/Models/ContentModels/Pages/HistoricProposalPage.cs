﻿using System;
using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using Forskningsradet.Core.Models.ContentModels.Support;
using Forskningsradet.Core.Models.SearchModels;

namespace Forskningsradet.Core.Models.ContentModels.Pages
{
    [SpecialPageType(
        GUID = "0D379065-8FA7-45B8-8BFD-065080D2EBCC",
        DisplayName = "Side for tidligere utlysning")]
    [AvailableContentTypes(Availability.None)]
    public class HistoricProposalPage : PublicationBasePage
    {
        [Display(
            Order = 20,
            GroupName = GroupNames.Content,
            Name = "ID",
            Description = "Angi utlysningens ID.")]
        [CultureSpecific]
        public virtual string Id { get; set; }

        public override PublicationType PublicationType => PublicationType.Proposal;

        [Display(
            Order = 30,
            GroupName = GroupNames.Content,
            Name = "Søknadsfrist",
            Description = "Angi utlysningens søknadsfrist.")]
        public virtual DateTime Deadline { get; set; }

        public override int Year => Deadline.Year;

        [Display(
            Order = 40,
            GroupName = GroupNames.Content,
            Name = "Type",
            Description = "Angi utlysningens type.")]
        [CultureSpecific]
        public virtual string ProposalType { get; set; }

        [Display(
            Order = 50,
            GroupName = GroupNames.Content,
            Name = "Program",
            Description = "Angi utlysningens program.")]
        [CultureSpecific]
        public virtual string Program { get; set; }

        [Display(
            Order = 100,
            GroupName = GroupNames.Content,
            Name = "Søknadsresultat",
            Description = "Angi kobling til søknadsresultat.")]
        [AllowedTypes(typeof(ApplicationResultPage))]
        public virtual ContentReference ApplicationResultPage { get; set; }

        [Display(
            Order = 110,
            GroupName = GroupNames.Content,
            Name = "Søknadsresultat tittel",
            Description = "Overstyr tittel på søknadsresultat.")]
        [CultureSpecific]
        public virtual string ApplicationResultTitle { get; set; }
    }
}
