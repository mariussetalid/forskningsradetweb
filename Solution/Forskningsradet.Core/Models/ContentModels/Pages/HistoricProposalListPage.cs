﻿using EPiServer.DataAnnotations;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ContentModels.Support;

namespace Forskningsradet.Core.Models.ContentModels.Pages
{
    [SpecialPageType(
        GUID = "75D74F52-5101-43FB-B737-2B4950F956BB",
        DisplayName = "Listeside for tidligere utlysninger")]
    [AvailableContentTypes(Include = new[] { typeof(HistoricProposalPage) })]
    [ImageUrl(IconConstants.Thumbnails.ListWithSearchAndFilterAndGroups)]
    public class HistoricProposalListPage : PublicationListPage
    {
    }
}
