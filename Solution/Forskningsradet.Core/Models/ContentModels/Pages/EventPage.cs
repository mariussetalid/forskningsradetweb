﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.Shell.ObjectEditing;
using EPiServer.Web;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.EditorDescriptors;
using Forskningsradet.Core.Extensions;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ContentModels.Support;
using Forskningsradet.Core.Models.Enums;
using Forskningsradet.Core.Models.SearchModels;

namespace Forskningsradet.Core.Models.ContentModels.Pages
{
    [ContentPageType(
        GUID = "4A67E91C-4D0A-44F1-B87F-C7FCF2B5C2AB",
        DisplayName = "Et arrangement",
        Description = "Sidemal for et arrangement.",
        Order = 110)]
    [ImageUrl(IconConstants.Thumbnails.EventPage)]
    public class EventPage : EditorialPage
    {
        [Display(
            Order = 15,
            GroupName = GroupNames.Listevisning,
            Name = "Valgfri undertittel",
            Description = "Tekst som dukker opp når arrangementet listes opp på listesiden for arrangementer")]
        [CultureSpecific]
        public virtual string OptionalSubtitle { get; set; }

        [Display(
            Order = 20,
            GroupName = GroupNames.Content,
            Name = "Startdato",
            Description = "Datoen som skal brukes for sortering og visning i listesiden for arrangementer")]
        public virtual DateTime? StartDate { get; set; }

        [Display(
            Order = 25,
            GroupName = GroupNames.Content,
            Name = "Sluttdato",
            Description = "Datoen som skal brukes for sortering og visning i listesiden for arrangementer")]
        public virtual DateTime? EndDate { get; set; }

        [Display(
            Order = 26,
            GroupName = GroupNames.Content,
            Name = "Avlyst")]
        public virtual bool Canceled { get; set; }

        [Display(
            Order = 27,
            GroupName = GroupNames.Content,
            Name = "Skjul lenke til kalender",
            Description = "Skjul knappen for å legge arrangement til kalender")]
        public virtual bool HideCalendarLink { get; set; }

        [Display(
            Order = 30,
            GroupName = GroupNames.Listevisning,
            Name = "Varighet",
            Description = "Varighet som skal vises i listesiden for arrangementer")]
        [CultureSpecific]
        public virtual string Duration { get; set; }

        [Display(
            Order = 35,
            GroupName = GroupNames.Listevisning,
            Name = "Sted",
            Description = "Sted som skal vises i listesiden for arrangementer")]
        public virtual string Location { get; set; }

        [Display(
            Name = "Bakgrunnsgrafikk",
            Description = "Velg hvilket utseende blokken skal ha.",
            GroupName = GroupNames.Listevisning,
            Order = 40)]
        [BackingType(typeof(PropertyNumber))]
        [EditorDescriptor(EditorDescriptorType = typeof(EnumEditorDescriptor<EventImageStyle>))]
        public virtual EventImageStyle EventImageStyle { get; set; }

        [Display(
            Order = 30,
            GroupName = GroupNames.Content,
            Name = "Bilde - anbefalt størrelse 1270x396",
            Description = "Bilde som vises øverst på siden.")]
        [UIHint(UIHint.Image)]
        public virtual ContentReference Image { get; set; }

        [Display(
            Order = 40,
            GroupName = GroupNames.Content,
            Name = "Arrangement info",
            Description = "Informasjon om arrangement")]
        public virtual EventDataBlock EventData { get; set; }

        [Searchable]
        public virtual string ComputedEventType
            => EventData?.Type.ToString() ?? EventType.None.ToString();

        [Display(
            Order = 50,
            GroupName = GroupNames.Content,
            Name = "Påmeldingslenke",
            Description = "Lenke for å melde seg på")]
        public virtual Url RegistrationLink { get; set; }

        [Display(
            Order = 55,
            GroupName = GroupNames.Content,
            Name = "Videolenke",
            Description = "Lenke til stream eller video")]
        public virtual Url VideoLink { get; set; }

        [Display(
            Order = 60,
            GroupName = GroupNames.Content,
            Name = "Rediger tittel for brødtekst",
            Description = "")]
        [CultureSpecific]
        [UIHint(UIHint.PreviewableText)]
        public virtual string MainBodyTitle { get; set; }

        [Display(
            Order = 70,
            GroupName = GroupNames.Content,
            Name = "Brødtekst",
            Description = "Informasjon om arrangementet")]
        [XhtmlStringRestrictContentTypes(AllowInXhtmlString.SomeMediaAndIframe)]
        [CultureSpecific]
        public virtual XhtmlString MainBody { get; set; }

        [Display(
            Order = 80,
            GroupName = GroupNames.Content,
            Name = "Rediger tittel for foredragsholdere",
            Description = "Tittelen for foredragsholderdelen av siden.")]
        [CultureSpecific]
        [UIHint(UIHint.PreviewableText)]
        public virtual string SpeakersTitle { get; set; }

        [Display(
            Order = 90,
            GroupName = GroupNames.Content,
            Name = "Blokkområde for foredragsholdere",
            Description = "Blokker for foredragsholdere.")]
        [AllowedTypes(typeof(ProfileBlock))]
        public virtual ContentArea SpeakersContentArea { get; set; }

        [Display(
            Order = 100,
            GroupName = GroupNames.Content,
            Name = "Rediger tittel for program",
            Description = "Tittel for programdelen av siden.")]
        [CultureSpecific]
        [UIHint(UIHint.PreviewableText)]
        public virtual string ScheduleTitle { get; set; }

        [Display(
            Order = 110,
            GroupName = GroupNames.Content,
            Name = "Blokkområde for program",
            Description = "Blokker for programdelen av siden.")]
        [AllowedTypes(typeof(StructuredScheduleBlock), typeof(ScheduleBlock))]
        public virtual ContentArea ScheduleContentArea { get; set; }

        [Display(
            Order = 130,
            GroupName = GroupNames.Content,
            Name = "Blokkområde for kontaktinformasjon",
            Description = "Her legger du inn en kontaktblokk som har tittel og kontakter.")]
        [AllowedTypes(typeof(ContactBlock))]
        public virtual ContentArea ContactContentArea { get; set; }

        [Display(
            Order = 1000,
            GroupName = GroupNames.MigratedContent,
            Name = "Øvrige kategorier",
            Description = "")]
        public virtual IList<string> OvrigeKategorierArray { get; set; }

        [Display(
            Order = 1001,
            GroupName = GroupNames.MigratedContent,
            Name = "Temaord",
            Description = "")]
        public virtual IList<string> Temaord { get; set; }

        [Display(
            Order = 1002,
            GroupName = GroupNames.MigratedContent,
            Name = "Importert Ingress",
            Description = "")]
        [UIHint(UIHint.Textarea)]
        public virtual string Ingress { get; set; }

        [Searchable]
        public virtual DateTime? ComputedEventDate
            => StartDate ?? StartPublish;

        [Searchable]
        public virtual DateTime? ComputedEventEndDate
            => EndDate ?? ComputedEventDate;

        [Searchable]
        public virtual string ComputedEventDateAsString
            => ComputedEventDate?.ToDisplayDate().ToNorwegianDateString() ?? string.Empty;

        [Searchable]
        public virtual string ComputedEventEndDateAsString
            => ComputedEventEndDate?.ToDisplayDate().ToNorwegianDateString() ?? string.Empty;

        [Searchable]
        public virtual bool ComputedHasVideoLink
            => VideoLink != null;

        public override SearchResultType ComputedSearchResultType =>
            SearchResultType.Event;

        public override string ReactComponentName() => nameof(ReactModels.EventPage);

        public override void SetDefaultValues(ContentType contentType)
        {
            base.SetDefaultValues(contentType);

            var random = new Random();
            EventImageStyle = (EventImageStyle)random.Next(1, Enum.GetNames(typeof(EventImageStyle)).Length);
        }
    }
}
