﻿using EPiServer.Core;
using Forskningsradet.Core.Models.ContentModels.Support;

namespace Forskningsradet.Core.Models.ContentModels.Pages
{
    [SystemPageType(
        GUID = "B8D65C2E-2F7A-4355-9FA4-F322DB889E82",
        DisplayName = "[System] Side for forhåndsvisning av blokker",
        Order = 10000,
        AvailableInEditMode = false)]
    public class BlockPreviewPage : SystemPage
    {
        public virtual ContentArea BlockArea { get; set; }
    }
}