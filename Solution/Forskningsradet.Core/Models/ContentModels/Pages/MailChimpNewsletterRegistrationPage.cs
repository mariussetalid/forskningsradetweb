﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAnnotations;
using EPiServer.Web;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ContentModels.Support;

namespace Forskningsradet.Core.Models.ContentModels.Pages
{
    [SpecialPageType(
        GUID = "CF35BAC9-B76A-4786-90C8-8FC4BB186094",
        DisplayName = "Påmelding til nyhetsbrev (MailChimp)",
        Description = "Skjemaside for påmelding til nyhetsbrev.",
        Order = 150)]
    [ImageUrl(IconConstants.Thumbnails.NewsletterPage)]
    public class MailChimpNewsletterRegistrationPage : EditorialPage
    {
        [Display(
            Order = 20,
            GroupName = GroupNames.Content,
            Name = "Ingress",
            Description = "Introduksjon som vises øverst på siden")]
        [UIHint(UIHint.Textarea)]
        [CultureSpecific]
        [XhtmlStringRestrictContentTypes]
        public virtual XhtmlString MainIntro { get; set; }

        [Display(
            Order = 30,
            GroupName = GroupNames.Content,
            Name = "Valideringsmelding",
            Description = "Melding som vises dersom man har fylt inn skjemaet feil.")]
        [CultureSpecific]
        [UIHint(UIHint.Textarea)]
        public virtual string ValidationMessage { get; set; }

        [Display(
            Order = 40,
            GroupName = GroupNames.Content,
            Name = "Høyre blokkfelt",
            Description = "Blokkfelt i høyre marg")]
        public virtual ContentArea RightBlockArea { get; set; }

        [Display(
            Name = "Bekreftelsesside",
            Description = "Bekreftelsesside for påmelding til nyhetsbrev",
            GroupName = GroupNames.Content,
            Order = 50)]
        public virtual ContentReference ConfirmationPage { get; set; }

        [Display(
            Order = 10,
            GroupName = GroupNames.Mailchimp,
            Name = "Mailchimp API key",
            Description = "API nøkkel for den Mailchimp kontoen hvor abonnenter skal legges til.")]
        public virtual string MailChimpApiKey { get; set; }

        [Display(
            Order = 20,
            GroupName = GroupNames.Mailchimp,
            Name = "Mailchimp list ID",
            Description = "ID til den abonnentslisten hvor nye abonnenter skal legges til.")]
        public virtual string MailChimpListId { get; set; }


        public override string ReactComponentName()
            => nameof(ReactModels.NewsletterRegistrationPage);
    }
}
