﻿using EPiServer.DataAnnotations;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ContentModels.Support;
using Forskningsradet.Core.Models.Contracts;

namespace Forskningsradet.Core.Models.ContentModels.Pages
{
    [SpecialPageType(
        GUID = "EDC612D9-7A5C-42CF-8D27-AAEDD8B9D2D4",
        DisplayName = "Ordlisteside",
        Description = "Sidemal for ordliste.",
        Order = 9015)]
    [AvailableContentTypes(Include = new[] { typeof(WordPage) })]
    [ImageUrl(IconConstants.Thumbnails.ListWithSearchAndFilterAndGroups)]
    public class GlossaryPage : GroupedSearchPage, IPageHasListIcon { }
}