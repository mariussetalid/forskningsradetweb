﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.SpecializedProperties;
using EPiServer.Web;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ContentModels.Media;
using Forskningsradet.Core.Models.ContentModels.Support;
using Forskningsradet.Core.Models.SearchModels;

namespace Forskningsradet.Core.Models.ContentModels.Pages
{
    [ContentPageType(
        GUID = "382F9C82-1F81-4A74-B855-EFFF14DC92A7",
        DisplayName = "Stort dokument",
        Description = "Hovedside for store dokumenter",
        Order = 200)]
    [AvailableContentTypes(Include = new[] { typeof(LargeDocumentChapterPage) })]
    [ImageUrl(IconConstants.Thumbnails.LargeDocumentMainPage)]
    public class LargeDocumentPage : LargeDocumentPageBase
    {
        [Display(
            Order = 12,
            GroupName = GroupNames.Content,
            Name = "Introtittel",
            Description = "Overskrift til oppsumeringen av dokumentet")]
        [CultureSpecific]
        public virtual string IntroTitle { get; set; }

        [Display(
            Order = 15,
            GroupName = GroupNames.Content,
            Name = "Ingress",
            Description = "Introduksjon som vises øverst på siden")]
        [UIHint(UIHint.Textarea)]
        [CultureSpecific]
        public virtual string MainIntro { get; set; }

        [Display(
            Order = 50,
            GroupName = GroupNames.Content,
            Name = "Visningsdybde",
            Description = "Bestemmer hvilket nivå av kapitler som skal vises på samme side.")]
        [Range(0, 4)]
        public virtual int DisplayDepth { get; set; }

        [Display(
            Order = 60,
            GroupName = GroupNames.Content,
            Name = "Vis nummerering",
            Description = "Skru på nummerering av menypunktene")]
        public virtual bool ShowNumbered { get; set; }

        [Display(
            Order = 70,
            GroupName = GroupNames.Content,
            Name = "Lenker",
            Description = "Valgfrie lenker som vises i innholdsnavigasjonen")]
        [CultureSpecific]
        public virtual LinkItemCollection LinkItems { get; set; }

        [Display(
            Order = 80,
            GroupName = GroupNames.Content,
            Name = "Overstyr last ned pdf")]
        [GenericMediaPdfRestriction]
        [AllowedTypes(typeof(GenericMedia))]
        [CultureSpecific]
        public virtual ContentReference OverrideDownloadPdf { get; set; }

        public override SearchResultType ComputedSearchResultType => SearchResultType.None;

        public override string ReactComponentName() => nameof(ReactModels.LargeDocumentsPage);

        public override void SetDefaultValues(ContentType contentType)
        {
            base.SetDefaultValues(contentType);

            DisplayDepth = 1;
            ShowNumbered = true;
        }
    }
}
