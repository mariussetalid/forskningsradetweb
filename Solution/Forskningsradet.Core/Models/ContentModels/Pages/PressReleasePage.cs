using System;
using System.ComponentModel.DataAnnotations;
using EPiServer.DataAnnotations;
using EPiServer.Web;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ContentModels.Contracts;
using Forskningsradet.Core.Models.ContentModels.Support;
using Forskningsradet.Core.Models.SearchModels;

namespace Forskningsradet.Core.Models.ContentModels.Pages
{
    [ContentPageType(
        GUID = "B424AFEB-2E23-4149-93E2-CF997EB62952",
        DisplayName = "Pressemelding",
        Description = "Sidemal for en pressemelding.",
        Order = 102)]
    [ImageUrl(IconConstants.Thumbnails.NewsArticlePage)]
    public class PressReleasePage : EditorialPage, ICanBeRenderedInSidebar
    {
        [Display(
            Name = "ID Ntb",
            GroupName = GroupNames.Import,
            Order = 100)]
        [Editable(false)]
        [Searchable(false)]
        public virtual string Id { get; set; }

        [Display(
            Name = "Tittel",
            GroupName = GroupNames.Import,
            Order = 110)]
        [Editable(false)]
        [CultureSpecific]
        public virtual string Title { get; set; }

        [Display(
            Name = "Ingress",
            Description = "Lead",
            GroupName = GroupNames.Import,
            Order = 120)]
        [Editable(false)]
        [UIHint(UIHint.Textarea)]
        [CultureSpecific]
        public virtual string Lead { get; set; }

        [Display(
            Name = "Body",
            GroupName = GroupNames.Import,
            Order = 130)]
        [Editable(false)]
        [UIHint(UIHint.Textarea)]
        [CultureSpecific]
        public virtual string Body { get; set; }

        [Display(
            Name = "Bildetekst",
            GroupName = GroupNames.Import,
            Order = 140)]
        [Editable(false)]
        [Searchable(false)]
        [CultureSpecific]
        public virtual string ImageCaption { get; set; }

        [Display(
            Name = "Bildeurl",
            GroupName = GroupNames.Import,
            Order = 150)]
        [Editable(false)]
        [Searchable(false)]
        public virtual string ImageUrl { get; set; }

        [Display(
            Name = "Bilde thumbnailurl",
            GroupName = GroupNames.Import,
            Order = 160)]
        [Editable(false)]
        [Searchable(false)]
        public virtual string ImageThumbnailUrl { get; set; }

        [Display(
            Name = "Publisert hos Ntb",
            GroupName = GroupNames.Import,
            Order = 170)]
        [Editable(false)]
        public virtual DateTime? ExternalPublishedAt { get; set; }

        [Display(
            Name = "Sist oppdatert fra Ntb",
            GroupName = GroupNames.Import,
            Order = 180)]
        [Editable(false)]
        public virtual DateTime? UpdatedAt { get; set; }

        public override SearchResultType ComputedSearchResultType =>
            SearchResultType.PressRelease;

        public override string ReactComponentName() => nameof(ReactModels.ArticlePage);

        public string PartialReactComponentName() => nameof(ReactModels.ArticleBlock);
    }
}