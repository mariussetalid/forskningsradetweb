﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAnnotations;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ContentModels.Support;
using Forskningsradet.Core.Models.Contracts;
using Geta.SEO.Sitemaps.Models;

namespace Forskningsradet.Core.Models.ContentModels.Pages
{
    [SpecialPageType(
        GUID = "9057A457-5D98-45EB-B678-0070A5C06226",
        DisplayName = "Søkeresultatside",
        Description = "Sidemal for søkeresultater.",
        Order = 9010)]
    [ImageUrl(IconConstants.Thumbnails.ListWithSearchAndFilter)]
    public class SearchPage : EditorialPage, IPageHasSettingsIcon, IExcludeFromSitemap
    {
        [Display(
            Order = 5,
            GroupName = GroupNames.Content,
            Name = "Tittel over filtervalg",
            Description = "Legg til en tittel her som vil befinne seg over filtreringsgruppene.")]
        [CultureSpecific]
        public virtual string FilterTitle { get; set; }

        [Display(
            Order = 10,
            GroupName = GroupNames.Content,
            Name = "Legg filter på venstre side",
            Description = "Huk av denne for å plassere filtervalgene på venstre side av søkeresultatene.")]
        public virtual bool LeftSideFilterPlacement { get; set; }

        [Display(
            Name = "(valgfri) Rotside for søk",
            Description = "Brukes som rot for søket.",
            GroupName = GroupNames.SatellittInnstillinger,
            Order = 10)]
        public virtual PageReference PageRoot { get; set; }

        [Display(
            Name = "(Valgfri) Kategorigrupper for filter",
            Description = "Kategorier som skal brukes til filtrering.",
            GroupName = GroupNames.SatellittInnstillinger,
            Order = 20)]
        public virtual CategoryList FilterCategoryList { get; set; }

        [Display(
            Name = "(Valgfri) Kategorirot for tagg i søketreff",
            Description = "Kategorigruppen som skal brukes som tagg på søketreffene.",
            GroupName = GroupNames.SatellittInnstillinger,
            Order = 30)]
        public virtual CategoryList TagCategoryRoot { get; set; }

        [Display(
            Order = 40,
            GroupName = GroupNames.SatellittInnstillinger,
            Name = "Vis tidsperiodeslider",
            Description = "Huk av denne for å vise tidsperiodeslideren.")]
        public virtual bool ShowRangeSlider { get; set; }

        [Display(
            Order = 10,
            GroupName = GroupNames.AdministratorInnstillinger,
            Name = "Overstyr endepunkt for Prosjektbanken",
            Description = "https://prosjektbanken.forskningsradet.no/prosjektbanken/rest/csglobalsearch?q={0}")]
        public virtual string ExternalSearchEndpoint { get; set; }

        [Display(
            Order = 15,
            GroupName = GroupNames.AdministratorInnstillinger,
            Name = "Skru på søk mot Prosjektbanken",
            Description = "Huk av denne for å skru på søk mot Prosjektbanken.")]
        public virtual bool EnableExternalSearch { get; set; }

        [Display(
            Order = 20,
            GroupName = GroupNames.AdministratorInnstillinger,
            Name = "Stavekontrollforslag")]
        public virtual SpellCheckOptionsBlock SpellCheck { get; set; }

        public override string ReactComponentName() => nameof(ReactModels.SearchPage);
    }
}