﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAnnotations;
using EPiServer.Web;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ContentModels.Support;

namespace Forskningsradet.Core.Models.ContentModels.Pages
{
    [SpecialPageType(
        GUID = "4241F4B1-E76D-425D-A827-914E7A29A0E8",
        DisplayName = "Påmelding til nyhetsbrev (Apsis)",
        Description = "Skjemaside for påmelding til nyhetsbrev.",
        Order = 155)]
    [ImageUrl(IconConstants.Thumbnails.NewsletterPage)]
    public class ApsisNewsletterRegistrationPage : EditorialPage
    {
        [Display(
            Order = 10,
            GroupName = GroupNames.Content,
            Name = "Tittel",
            Description = "Overskrift på siden.")]
        [CultureSpecific]
        public virtual string Title { get; set; }

        [Display(
            Order = 20,
            GroupName = GroupNames.Content,
            Name = "Ingress",
            Description = "Introduksjon som vises øverst på siden.")]
        [UIHint(UIHint.Textarea)]
        [CultureSpecific]
        [XhtmlStringRestrictContentTypes]
        public virtual XhtmlString MainIntro { get; set; }

        [Display(
            Order = 40,
            GroupName = GroupNames.Content,
            Name = "Høyre blokkfelt",
            Description = "Blokkfelt i høyre marg.")]
        public virtual ContentArea RightBlockArea { get; set; }

        [Display(
            Order = 50,
            GroupName = GroupNames.Content,
            Name = "FormId for Apsis script")]
        public virtual string FormId { get; set; }

        [Display(
            Order = 60,
            GroupName = GroupNames.Content,
            Name = "DivId for Apsis script")]
        public virtual string DivId { get; set; }

        public override string ReactComponentName() =>
            nameof(ReactModels.ResponsiveIframePage);
    }
}
