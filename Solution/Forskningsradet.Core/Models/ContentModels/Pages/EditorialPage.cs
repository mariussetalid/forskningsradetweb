﻿using System;
using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.Web;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ContentModels.Contracts;
using Forskningsradet.Core.Models.ContentModels.Support;
using Forskningsradet.Core.Models.SearchModels;

namespace Forskningsradet.Core.Models.ContentModels.Pages
{
    /// <summary>
    /// Base class for all pages meant to be editorial content and shown to end users. 
    /// </summary>
    public abstract class EditorialPage : BasePageData, IReactComponentData, IAutomaticallyChangeTrackable
    {
        [Display(
            Order = 10,
            GroupName = GroupNames.Listevisning,
            Name = "Listetittel",
            Description = "Tittel som skal brukes når siden vises i liste")]
        [CultureSpecific]
        public virtual string ListTitle { get; set; }

        [Display(
            Order = 20,
            GroupName = GroupNames.Listevisning,
            Name = "Ingress",
            Description = "Tekst som dukker opp når siden vises i liste")]
        [CultureSpecific]
        [UIHint(UIHint.Textarea)]
        public virtual string ListIntro { get; set; }

        [Display(
            Order = 30,
            GroupName = GroupNames.Listevisning,
            Name = "Listebilde",
            Description = "Bilde som skal brukes når siden vises i liste")]
        [UIHint(UIHint.Image)]
        public virtual ContentReference ListImage { get; set; }

        [Display(
            Order = 10,
            GroupName = GroupNames.SEOsettings,
            Name = "SEO innstillinger")]
        public virtual SeoSettingsBlock SeoSettings { get; set; }

        [Display(
            Order = 15,
            GroupName = GroupNames.Arkivering,
            Name = "Skal arkiveres",
            Description = "Angir om denne siden skal sendes til arkiv ved publisering.")]
        public virtual bool IsArchivable { get; set; }

        [Display(
            Order = 20,
            GroupName = GroupNames.Arkivering,
            Name = "Signatur saksansvarlig",
            Description = "Saksansvarlig må være en ansatt, og det er bokstavene foran @ i epostadressen som er signaturen.")]
        public virtual string CaseResponsible { get; set; }

        [Display(
            Order = 30,
            GroupName = GroupNames.Arkivering,
            Name = "Tittel",
            Description = "Denne verdien overstyrer sakstittel ved arkivering.")]
        [CultureSpecific]
        public virtual string ArchiveTitleOverride { get; set; }

        [Display(
            GroupName = GroupNames.Revision,
            Name = "Revisjonsdato")]
        public virtual DateTime RevisionDate { get; set; }

        [Searchable]
        public virtual SearchResultType ComputedSearchResultType =>
            SearchResultType.None;

        public virtual bool DoNotSetChangedOnPublish { get; set; }

        public abstract string ReactComponentName();

        public ReactModels.ReactComponent BuildReactComponent(bool pageIsInEditMode) =>
            null;

        public string ContentName() =>
            (this as IContent).Name;

        public int ContentId() =>
            (this as IContent).ContentLink.ID;

        public override void SetDefaultValues(ContentType contentType)
        {
            base.SetDefaultValues(contentType);

            RevisionDate = DateTime.UtcNow.AddYears(1);
        }
    }
}