using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.Filters;
using Forskningsradet.Core.Models.ContentModels.Support;

namespace Forskningsradet.Core.Models.ContentModels.Pages
{
    public abstract class GroupedSearchPage : EditorialPage
    {
        [Display(
            Order = 20,
            GroupName = GroupNames.Content,
            Name = "Høyre blokkfelt",
            Description = "Blokkfelt i høyre marg")]
        public virtual ContentArea RightBlockArea { get; set; }

        public override string ReactComponentName()
            => nameof(ReactModels.GroupedSearchPage);

        public override void SetDefaultValues(ContentType contentType)
        {
            base.SetDefaultValues(contentType);

            ChildSortOrder = FilterSortOrder.Alphabetical;
        }
    }
}