﻿using System;
using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAnnotations;
using Forskningsradet.Core.Models.ApiModels.Evurdering;
using Forskningsradet.Core.Models.ContentModels.Support;

namespace Forskningsradet.Core.Models.ContentModels.Pages
{
    public abstract class ProposalBasePage : EditorialPage
    {
        [Display(
            Name = "ID",
            GroupName = GroupNames.Content,
            Order = 1)]
        [Editable(false)]
        [Searchable(false)]
        public virtual string Id { get; set; }

        [Display(
            Name = "Sorteringsindeks for listevisning",
            Description = "Indekstall som sorterer utlysningene i samme gruppe, med lik status og lik søknadsfrist i lister.",
            GroupName = GroupNames.Listevisning,
            Order = 40)]
        public virtual int? SortingNumberInList { get; set; }

        [Display(
            Order = 60,
            GroupName = GroupNames.Listevisning,
            Name = "Fjern lenke til utlysningssiden i lister",
            Description = "Viser tittel på utlysningen kun som tekst i lister.")]
        public virtual bool DisableLink { get; set; }

        [Display(
            Order = 70,
            GroupName = GroupNames.Listevisning,
            Name = "Kansellert tekst",
            Description = "Tekst som vises i boks i listevisning av utlysningen.")]
        [CultureSpecific]
        public virtual XhtmlString CancellationText { get; set; }

        [Display(
            Order = 80,
            GroupName = GroupNames.Listevisning,
            Name = "Kansellert utlysning skal skjules fra 'Aktuelle'",
            Description = "Skjuler utlysninger fra 'Aktuelle' fanen i utlysningslisten.")]
        public virtual bool CancellationShouldHideFromActiveTab { get; set; }

        public abstract PageReference ApplicationTypeReference { get; set; }

        public abstract DateTime? Deadline { get; set; }

        public abstract DateTime? DeadlineComputed { get; }

        public abstract DateTime? DeadlineComputedForSearchFacet { get; }

        public abstract string ComputedDeadlineType { get; }

        public virtual DeadlineType DeadlineType { get; set; }

        public virtual ProposalState ProposalState { get; set; }
    }
}
