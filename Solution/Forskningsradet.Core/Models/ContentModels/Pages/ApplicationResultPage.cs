using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using EPiServer.Cms.Shell.UI.ObjectEditing.EditorDescriptors;
using EPiServer.Core;
using EPiServer.Shell.ObjectEditing;
using Forskningsradet.Core.Models.ContentModels.Contracts;
using Forskningsradet.Core.Models.ContentModels.PropertyList;
using Forskningsradet.Core.Models.ContentModels.Support;
using Forskningsradet.Core.Models.Contracts;
using Geta.SEO.Sitemaps.Models;

namespace Forskningsradet.Core.Models.ContentModels.Pages
{
    [ContentPageType(
        GUID = "F5726B43-97C1-405C-A319-E518F24CA7A1",
        DisplayName = "Søknadsresultat",
        Description = "Søknadsresultat på en utlysning.",
        Order = 1020,
        AvailableInEditMode = false)]
    public class ApplicationResultPage : BasePageData, IReactComponentData, IPageHasNoViewIcon, IExcludeFromSitemap
    {
        [Display(
            Name = "Utlysningsid",
            GroupName = GroupNames.Import,
            Order = 100)]
        [Editable(false)]
        public virtual string Id { get; set; }

        [Display(
            Name = "Utlysningsid (utgår)",
            GroupName = GroupNames.Import,
            Order = 120)]
        [Obsolete("Use Id instead.")]
        [Editable(false)]
        public virtual string ProposalId { get; set; }

        [Display(
            Name = "Søkte midler",
            GroupName = GroupNames.Content,
            Order = 100)]
        public virtual string AppliedAmount { get; set; }

        [Display(
            Name = "Tildelte midler",
            GroupName = GroupNames.Content,
            Order = 200)]
        public virtual string AwardedAmount { get; set; }

        [Display(
            Name = "Mottatte søknader",
            GroupName = GroupNames.Content,
            Order = 300)]
        public virtual string NumberOfApplications { get; set; }

        [Display(
            Name = "Innvilgede søknader",
            GroupName = GroupNames.Content,
            Order = 400)]
        public virtual string NumberOfApprovedApplications { get; set; }

        [Display(
            Name = "Karakterfordeling",
            GroupName = GroupNames.Content,
            Order = 500)]
        [EditorDescriptor(EditorDescriptorType = typeof(CollectionEditorDescriptor<GradeDistribution>))]
        public virtual IList<GradeDistribution> GradeDistribution { get; set; }

        [Display(
            Name = "Prosjekter",
            GroupName = GroupNames.Content,
            Order = 600)]
        [EditorDescriptor(EditorDescriptorType = typeof(CollectionEditorDescriptor<GrantedProject>))]
        public virtual IList<GrantedProject> GrantedProjects { get; set; }

        [Display(
            Name = "Fageksperter",
            GroupName = GroupNames.Content,
            Order = 700)]
        [EditorDescriptor(EditorDescriptorType = typeof(CollectionEditorDescriptor<SubjectSpecialist>))]
        public virtual IList<SubjectSpecialist> SubjectSpecialists { get; set; }

        public string ReactComponentName() => nameof(ReactModels.DescriptionListAndTables);

        public string ContentName() => (this as IContent).Name;

        public int ContentId() => (this as IContent).ContentLink.ID;
    }
}