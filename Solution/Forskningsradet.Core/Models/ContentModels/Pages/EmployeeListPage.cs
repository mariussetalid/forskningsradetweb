using EPiServer.DataAnnotations;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ContentModels.Support;
using Forskningsradet.Core.Models.Contracts;

namespace Forskningsradet.Core.Models.ContentModels.Pages
{
    [SpecialPageType(
        GUID = "A281747E-4834-404A-8461-DA43132E64B3",
        DisplayName = "Ansattliste",
        Description = "Sidemal for ansattliste.",
        Order = 250)]
    [AvailableContentTypes(Include = new[] { typeof(PersonPage) })]
    [ImageUrl(IconConstants.Thumbnails.ListWithSearchAndFilterAndGroups)]
    public class EmployeeListPage : GroupedSearchPage, IPageHasListIcon { }
}