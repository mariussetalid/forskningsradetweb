using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAnnotations;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ContentModels.Support;

namespace Forskningsradet.Core.Models.ContentModels.Pages
{
    [SpecialPageType(GUID = "03A13107-890F-4F21-BB35-A9835A3C8746",
        DisplayName = "Pressemeldinger",
        Description = "Lister opp pressemeldinger.",
        Order = 9030)]
    [ImageUrl(IconConstants.Thumbnails.ListPageWithVisualElement)]
    public class PressReleaseListPage : EditorialPage
    {
        [Display(
            Order = 10,
            GroupName = GroupNames.Content,
            Name = "Høyremeny",
            Description = "Blokkfelt i høyre marg.")]
        public virtual ContentArea RightBlockArea { get; set; }

        public override string ReactComponentName() => nameof(ReactModels.ArticlePage);
    }
}