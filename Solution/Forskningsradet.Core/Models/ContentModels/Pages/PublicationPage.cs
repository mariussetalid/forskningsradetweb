﻿using System.ComponentModel.DataAnnotations;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.Find.Cms;
using EPiServer.ServiceLocation;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ContentModels.Support;

namespace Forskningsradet.Core.Models.ContentModels.Pages
{
    [SpecialPageType(
        GUID = "26800AB3-46C5-4BB5-8511-FC1B87FA549D",
        DisplayName = "Side for publikasjon")]
    [AvailableContentTypes(Availability.None)]
    [ImageUrl(IconConstants.Thumbnails.PublicationPage)]
    public class PublicationPage : PublicationBasePage
    {
        [Display(
            Order = 40,
            GroupName = GroupNames.Content,
            Name = "Divisjon",
            Description = "Angi publikasjonens divisjon.")]
        public virtual string Division { get; set; }

        [Display(
            Order = 50,
            GroupName = GroupNames.Content,
            Name = "Kontaktperson",
            Description = "Angi publikasjonens kontaktperson.")]
        public virtual string ContactPerson { get; set; }

        [Display(
            Order = 60,
            GroupName = GroupNames.Content,
            Name = "Sideantall",
            Description = "Angi antall sider i publikasjonen.")]
        public virtual int NumberOfPages { get; set; }

        [Display(
            Order = 70,
            GroupName = GroupNames.Content,
            Name = "Sted",
            Description = "Angi publikasjonens sted.")]
        public virtual string Location { get; set; }

        [Display(
            Order = 90,
            GroupName = GroupNames.Content,
            Name = "Forfatter",
            Description = "Angi publikasjonens forfatter.")]
        public virtual string Author { get; set; }

        [Display(
            Order = 100,
            GroupName = GroupNames.Content,
            Name = "Språk",
            Description = "Angi publikasjonens språk.")]
        public virtual string PublicationLanguage { get; set; }

        [Display(
            Order = 110,
            GroupName = GroupNames.Content,
            Name = "Stikktittel",
            Description = "Angi publikasjonens stikktittel.")]
        [CultureSpecific]
        public virtual string TagLine { get; set; }

        [Display(
            Order = 200,
            GroupName = GroupNames.MigratedContent,
            Name = "Migrert type publikasjon",
            Description = "Publikasjonens type fra migrert innhold.")]
        public virtual string Type { get; set; }

        [Display(
            Order = 210,
            GroupName = GroupNames.MigratedContent,
            Name = "ISBN",
            Description = "Publikasjonens ISBN.")]
        public virtual string Isbn { get; set; }

        [Display(
            Order = 220,
            GroupName = GroupNames.MigratedContent,
            Name = "Program",
            Description = "Publikasjonens program.")]
        [CultureSpecific]
        public virtual string Program { get; set; }

        [Display(
            Order = 230,
            GroupName = GroupNames.MigratedContent,
            Name = "Ansvarlig utgiver",
            Description = "Publikasjonens ansvarlige utgiver.")]
        public virtual string Publisher { get; set; }

        [Display(
            Order = 240,
            GroupName = GroupNames.MigratedContent,
            Name = "NettISBN",
            Description = "Publikasjonens nettISBN.")]
        public virtual string NetIsbn { get; set; }

        [Searchable]
        public virtual string SearchSummary => ContentReference.IsNullOrEmpty(Attachment)
            ? string.Empty
            : (ServiceLocator.Current.GetInstance<IContentLoader>().Get<ContentData>(Attachment) as MediaData)?.SearchAttachmentText();
    }
}
