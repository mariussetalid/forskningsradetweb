﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.Shell.ObjectEditing;
using EPiServer.Web;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.EditorDescriptors;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ContentModels.Support;

namespace Forskningsradet.Core.Models.ContentModels.Pages
{
    [SpecialPageType(
        GUID = "CFCD655D-7E63-4C85-A549-4E44BA5AE5E0",
        DisplayName = "Satellittforside",
        Description = "Forsidemal for satellitter og andre nettsteder enn forskningsradet.no. Tillater egen meny, innstillinger, etc. under denne siden.",
        Order = 9001)]
    [ImageUrl(IconConstants.Thumbnails.FrontPage)]
    public class SatelliteFrontPage : FrontPageBase
    {
        [Display(
            Order = 5,
            GroupName = GroupNames.Content,
            Name = "Overstyr sidens navn",
            Description = "Hvis fylt ut brukes denne i stedet for sidens navn i visning. Da kan du ha forskjellig navn i visning og i Episervertreet.")]
        [CultureSpecific]
        public virtual string Title { get; set; }

        [Display(
            Order = 10,
            GroupName = GroupNames.Driftsmeldinger,
            Name = "Globale driftsmeldinger",
            Description = "Globale driftsmeldinger som vises på alle sider.")]
        public override ContentArea GlobalMessages { get; set; }

        [Display(
            Order = 12,
            GroupName = GroupNames.Driftsmeldinger,
            Name = "Globale felles driftsmeldinger",
            Description = "Globale driftsmeldinger som vises på alle sider og som er felles på tvers av forsider.")]
        [UIHint(UIHint.AssetsFolder)]
        public override ContentReference GlobalSharedMessages { get; set; }

        [Display(
            Order = 10,
            GroupName = GroupNames.Content,
            Name = "Bilde",
            Description = "Stort bilde som vises øverst på siden.")]
        [UIHint(UIHint.Image)]
        public virtual ContentReference Image { get; set; }

        [Display(
            Order = 15,
            GroupName = GroupNames.Content,
            Name = "Tekst",
            Description = "Vises som tekst øverst på siden")]
        [UIHint(UIHint.Textarea)]
        public virtual string Text { get; set; }

        [Display(
            Order = 17,
            GroupName = GroupNames.Content,
            Name = "Tekstfarge",
            Description = "Tekstfarge. Velg den som kommer tydeligst fram basert på bildevalget.")]
        [BackingType(typeof(PropertyNumber))]
        [EditorDescriptor(EditorDescriptorType = typeof(EnumEditorDescriptor<SatelliteFrontPageHeaderTextStyle>))]
        public virtual ReactModels.SatelliteHeader_TextColor TextStyle { get; set; }

        [Display(
            Order = 27,
            GroupName = GroupNames.NettstedsInnstillinger,
            Name = "Fargetema",
            Description = "Setter gjennomgående fargevalg for nettstedet")]
        [EditorDescriptor(EditorDescriptorType = typeof(EnumEditorDescriptor<ColorTheme>))]
        public virtual ColorTheme ColorTheme { get; set; }

        [Display(
            Order = 30,
            GroupName = GroupNames.NettstedsInnstillinger,
            Name = "Vis forskningsrådets toppbanner",
            Description = "Skru på banner med Forskningsrådets logo og lenke til www.forskningsradet.no")]
        public virtual bool ShowTopBanner { get; set; }

        [Display(
            Order = 20,
            GroupName = GroupNames.Content,
            Name = "Valgfri lenke 1")]
        public virtual EditPageLinkBlock LinkBlock1 { get; set; }

        [Display(
            Order = 30,
            GroupName = GroupNames.Content,
            Name = "Valgfri lenke 2")]
        public virtual EditPageLinkBlock LinkBlock2 { get; set; }

        [Display(
            Order = 40,
            GroupName = GroupNames.Content,
            Name = "Valgfri lenke 3")]
        public virtual EditPageLinkBlock LinkBlock3 { get; set; }

        [Display(
            Order = 50,
            GroupName = GroupNames.Content,
            Name = "Blokkområde",
            Description = "Blokkområdet midt i siden.")]
        public virtual ContentArea MainContent { get; set; }

        public override string ReactComponentName()
            => nameof(ReactModels.SatelliteFrontpage);

        public override void SetDefaultValues(ContentType contentType)
        {
            ShowTopBanner = true;
            base.SetDefaultValues(contentType);
        }
    }

    public enum ColorTheme
    {
        Forskningsradet = 0,
        RFF = 1,
        Nysgjerrigper = 2,
        Kilden = 3
    }

    public enum SatelliteFrontPageHeaderTextStyle
    {
        Dark = 0,
        Light = 1
    }
}
