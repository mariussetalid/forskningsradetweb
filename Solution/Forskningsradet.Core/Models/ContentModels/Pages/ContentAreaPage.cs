﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using EPiServer.Cms.Shell.UI.ObjectEditing.EditorDescriptors;
using EPiServer.Core;
using EPiServer.DataAnnotations;
using EPiServer.Shell.ObjectEditing;
using EPiServer.Web;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ContentModels.PropertyList;
using Forskningsradet.Core.Models.ContentModels.Support;
using Forskningsradet.Core.Models.SearchModels;

namespace Forskningsradet.Core.Models.ContentModels.Pages
{
    [ContentPageType(
        GUID = "637CEA92-60E3-4CDF-8113-78BA5107059B",
        DisplayName = "Landingsside",
        Description = "Sidemal for landingsside.",
        Order = 107)]
    [ImageUrl(IconConstants.Thumbnails.ContentAreaPage)]
    public class ContentAreaPage : EditorialPage
    {
        [Display(
            Order = 10,
            GroupName = GroupNames.Content,
            Name = "Overstyr sidens navn",
            Description = "Hvis fylt ut brukes denne i stedet for sidens navn i visning. Da kan du ha forskjellig navn i visning og i Episervertreet.")]
        [CultureSpecific]
        public virtual string Title { get; set; }

        [Display(
            Order = 20,
            GroupName = GroupNames.Content,
            Name = "Overstyr tittel. Bruk _ for å sette hvor et langt ord kan deles",
            Description = "Hvis fylt ut brukes denne som overskrift på siden. Bruk _ for å bestemme hvor et langt ord skal deles for mobilvisning.")]
        [CultureSpecific]
        [Searchable(false)]
        public virtual string TitleWithCustomSeparator { get; set; }

        [Display(
            Name = "Kartmarkører",
            Description = "Hvis man legger inn markører vises et kart øverst på siden.",
            GroupName = GroupNames.Kart,
            Order = 100)]
        [EditorDescriptor(EditorDescriptorType = typeof(CollectionEditorDescriptor<MapMarker>))]
        public virtual IList<MapMarker> Markers { get; set; }

        [Display(
            Order = 110,
            GroupName = GroupNames.Content,
            Name = "Bilde",
            Description = "Bilde som vises i stort format øverst på siden.")]
        [UIHint(UIHint.Image)]
        public virtual ContentReference Image { get; set; }

        [Display(
            Order = 115,
            GroupName = GroupNames.Content,
            Name = "Ingress",
            Description = "Introduksjon som vises øverst på siden")]
        [UIHint(UIHint.Textarea)]
        [CultureSpecific]
        public virtual string MainIntro { get; set; }

        [Display(
            Order = 120,
            GroupName = GroupNames.Content,
            Name = "Blokkområde",
            Description = "Blokkområdet som utgjør hoveddelen av siden.")]
        public virtual ContentArea MainContent { get; set; }

        [Display(
            Order = 100,
            GroupName = GroupNames.SatellittInnstillinger,
            Name = "Vis kategoriene som er lagt til under Innholdsfanen",
            Description = "Velges for å automatisk vise fram kategorier som er satt på siden.")]
        public virtual bool ShowCategories { get; set; }

        [Display(
            Order = 110,
            GroupName = GroupNames.SatellittInnstillinger,
            Name = "Valg for å bruke siden som hjemmeside for en kategori:",
            Description = "Liste over kategorier som skal vises.")]
        public virtual CategoryListBlock CategoryListBlock { get; set; }

        [Display(
            Order = 120,
            GroupName = GroupNames.SatellittInnstillinger,
            Name = "Denne siden er hjemmeside for kategorien:",
            Description = "Brukes til å velge hvilken kategori som skal peke til denne siden.")]
        public virtual CategoryList HomePageForCategories { get; set; }

        [Searchable]
        public virtual bool IsCategoryHomePage => HomePageForCategories?.Count > 0;

        public override SearchResultType ComputedSearchResultType => SearchResultType.Landing;

        public override string ReactComponentName() => nameof(ReactModels.ContentAreaPage);
    }
}