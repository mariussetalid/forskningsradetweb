﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAnnotations;
using EPiServer.Shell.ObjectEditing;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ContentModels.Support;
using Forskningsradet.Core.Models.Contracts;

namespace Forskningsradet.Core.Models.ContentModels.Pages
{
    [SpecialPageType(GUID = "D2DB9002-C4AD-4791-9525-5A02C5683B05",
        DisplayName = "Kategori listeside",
        Description = "Lister opp innhold basert på kategori med filtrering.",
        Order = 9030)]
    [ImageUrl(IconConstants.Thumbnails.ListWithFilter)]
    public class CategoryListPage : EditorialPage, IPageHasListIcon
    {
        [Display(
            Order = 10,
            GroupName = GroupNames.Content,
            Name = "Listens navn",
            Description = "Hvis fylt ut brukes denne i stedet for sidens navn i visning. Da kan du ha forskjellig navn i visning og i Episervertreet.")]
        [CultureSpecific]
        public virtual string Title { get; set; }

        [Display(
            Order = 20,
            GroupName = GroupNames.Content,
            Name = "Høyre blokkfelt",
            Description = "Blokkfelt i høyre marg")]
        public virtual ContentArea RightBlockArea { get; set; }

        [Display(
            Name = "Rotside for listen",
            Description = "Her kan man angi en side som skal være rot for søket etter innhold. Bruk denne til å begrense hvor liten eller stor del av nettstedet listen skal hentes fra.",
            GroupName = GroupNames.Content,
            Order = 30)]
        public virtual PageReference PageRoot { get; set; }

        [Display(
            Name = "Sidetyper som skal vises",
            Description = "Velg hvilke sidetyper sider som skal inkluderes kan være av.",
            GroupName = GroupNames.Content,
            Order = 40)]
        [CustomPageType(CustomPageTypeSet.Content)]
        [SelectMany(SelectionFactoryType = typeof(Factories.CustomPageTypeSelectionFactory))]
        public virtual string PageTypes { get; set; }

        [Display(
            Name = "Tittle over filtervalg",
            Description = "Legg til en tittel her som vil befinne seg over filtreringsgruppene.",
            GroupName = GroupNames.Content,
            Order = 50)]
        [CultureSpecific]
        public virtual string FilterTitle { get; set; }

        [Display(
            Name = "(Valgfri) Kategorigrupper for filter",
            Description = "Kategorier som skal brukes til filtrering",
            GroupName = GroupNames.Content,
            Order = 60)]
        public virtual CategoryList FilterCategoryList { get; set; }

        [Display(
            Name = "(Valgfri) Kategorirot for tagg i søketreff",
            Description = "Kategorigruppen som skal brukes som tagg på søketreffene.",
            GroupName = GroupNames.Content,
            Order = 70)]
        public virtual CategoryList TagCategoryRoot { get; set; }

        [Display(
            Name = "Kategorirot for tagg i søketreff viser mer enn én tagg",
            Description = "Viser flere tags hvis innhold tilhører flere kategorier under kategorirot.",
            GroupName = GroupNames.AdministratorInnstillinger,
            Order = 100)]
        public virtual bool ShowMultipleTags { get; set; }

        public override string ReactComponentName() => nameof(ReactModels.CategoryListPage);
    }
}
