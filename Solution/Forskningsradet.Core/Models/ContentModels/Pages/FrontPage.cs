﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using EPiServer.Cms.Shell.UI.ObjectEditing.EditorDescriptors;
using EPiServer.Core;
using EPiServer.DataAnnotations;
using EPiServer.Shell.ObjectEditing;
using EPiServer.SpecializedProperties;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ContentModels.PropertyList;
using Forskningsradet.Core.Models.ContentModels.Support;

namespace Forskningsradet.Core.Models.ContentModels.Pages
{
    [SpecialPageType(
        GUID = "CA09A4DD-10EB-4B8A-9359-5240C9328B61",
        DisplayName = "Forside",
        Description = "Forsidemal for forskningsradet.no.",
        Order = 9001)]
    [ImageUrl(IconConstants.Thumbnails.FrontPage)]
    public class FrontPage : FrontPageBase
    {
        [Display(
            Order = 10,
            GroupName = GroupNames.Content,
            Name = "Toppblokker på forsiden (viser kun én om gangen)",
            Description = "Øverste del av siden som viser en prioritert sak med bilde og lenke.")]
        [AllowedTypes(typeof(FrontPageHeaderBlock))]
        public virtual ContentArea TopContent { get; set; }

        [Display(
            Order = 20,
            GroupName = GroupNames.Content,
            Name = "Lenkeliste",
            Description = "Lenkeliste som legger seg under toppblokkene")]
        [CultureSpecific]
        [MaxItems(5)]
        public virtual LinkItemCollection LinkList { get; set; }

        [Display(
            Order = 25,
            GroupName = GroupNames.Content,
            Name = "Blokkområde",
            Description = "Blokkområdet midt i siden.")]
        public virtual ContentArea MainContent { get; set; }

        [Display(
            Order = 200,
            GroupName = GroupNames.NettstedsInnstillinger,
            Name = "Ledige Stillinger",
            Description = "Lenke til foreldresiden for ledige stillinger")]
        [AllowedTypes(typeof(ContentAreaPage))]
        public virtual PageReference VacanciesPage { get; set; }

        [Display(
            Order = 210,
            GroupName = GroupNames.NettstedsInnstillinger,
            Name = "Pressemeldinger",
            Description = "Lenke til foreldresiden for pressemeldinger")]
        [AllowedTypes(typeof(PressReleaseListPage), typeof(FolderPage))]
        public virtual PageReference PressReleasesPage { get; set; }

        [Display(
            Order = 220,
            GroupName = GroupNames.NettstedsInnstillinger,
            Name = "Kategorioversettelser",
            Description = "Lenke til siden for redaksjonelle kategorioversettelser. Settes kun på hovedforsiden.")]
        [AllowedTypes(typeof(CategoryTranslationPage))]
        public virtual PageReference CategoryTranslationPage { get; set; }

        [Display(
            Order = 230,
            GroupName = GroupNames.NettstedsInnstillinger,
            Name = "Ansattliste",
            Description = "Siden hvor alle kontaktpersoner/ansatte som blir importert legger seg under.")]
        [AllowedTypes(typeof(EmployeeListPage))]
        public virtual PageReference ContactPersonsPage { get; set; }

        [Display(
            Order = 240,
            GroupName = GroupNames.NettstedsInnstillinger,
            Name = "Utlysningsliste",
            Description = "Listeside for utlysninger. Alle utlysninger legger seg under denne siden.")]
        [AllowedTypes(typeof(ProposalListPage))]
        public virtual PageReference ProposalsPage { get; set; }

        [Display(
            Order = 250,
            GroupName = GroupNames.NettstedsInnstillinger,
            Name = "Utlysningsliste for test",
            Description = "Listeside for utlysninger med aktiviteten TEST. Alle utlysninger med denne aktiviteten legger seg under denne siden.")]
        [AllowedTypes(typeof(ProposalListPage))]
        public virtual PageReference ProposalsTestPage { get; set; }

        [Display(
            Order = 260,
            GroupName = GroupNames.NettstedsInnstillinger,
            Name = "Mappe for søknadstyper",
            Description = "Mappen hvor alle søknadstyper som blir brukt til å gruppere utlysninger blir importert til.")]
        [AllowedTypes(typeof(FolderPage))]
        public virtual PageReference ApplicationTypeRoot { get; set; }

        [Display(
            Order = 270,
            GroupName = GroupNames.NettstedsInnstillinger,
            Name = "Mappe for søknadsresultater",
            Description = "Mappen hvor alle søknadsresultater blir importert til.")]
        [AllowedTypes(typeof(FolderPage))]
        public virtual PageReference ApplicationResultFolder { get; set; }

        [Display(
            Order = 280,
            GroupName = GroupNames.NettstedsInnstillinger,
            Name = "Mappe for RFF-utlysninger",
            Description = "Alle RFF-utlysninger legger seg under denne mappen.")]
        [AllowedTypes(typeof(FolderPage))]
        public virtual PageReference RffProposalPage { get; set; }

        [Display(
            Order = 290,
            GroupName = GroupNames.NettstedsInnstillinger,
            Name = "Plassering for RFF-utlysninger",
            Description = "Spesifikk plasering for hver RFF-programkode")]
        [EditorDescriptor(EditorDescriptorType = typeof(CollectionEditorDescriptor<RffLocation>))]
        public virtual IList<RffLocation> RffLocations { get; set; }

        public override string ReactComponentName() =>
            nameof(ReactModels.Frontpage);
    }
}