﻿using EPiServer.DataAnnotations;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ContentModels.Support;

namespace Forskningsradet.Core.Models.ContentModels.Pages
{
    [SpecialPageType(
        GUID = "F45ADB83-B0AD-469B-AC96-AC80DE964F20",
        DisplayName = "Stort dokument kapittelside",
        Description = "Kapittelside for store dokumenter",
        Order = 200)]
    [AvailableContentTypes(Include = new[] { typeof(LargeDocumentChapterPage) })]
    [ImageUrl(IconConstants.Thumbnails.LargeDocumentChapterPage)]
    public class LargeDocumentChapterPage : LargeDocumentPageBase
    {
    }
}
