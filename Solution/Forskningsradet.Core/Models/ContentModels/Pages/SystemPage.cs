﻿using Forskningsradet.Core.Models.Contracts;

namespace Forskningsradet.Core.Models.ContentModels.Pages
{
    /// <summary>
    /// Base class for system pages. System pages are not meant to be displayed to end users and are only
    /// for internal workings such as settings, hidden data structures, etc. 
    /// </summary>
    public abstract class SystemPage : BasePageData, IPageHasNoView, IPageHasSettingsIcon
    {

    }
}