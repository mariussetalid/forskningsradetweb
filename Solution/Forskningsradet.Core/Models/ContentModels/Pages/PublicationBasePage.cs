﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.Shell.ObjectEditing;
using EPiServer.Web;
using Forskningsradet.Core.EditorDescriptors;
using Forskningsradet.Core.Models.ContentModels.Support;
using Forskningsradet.Core.Models.SearchModels;

namespace Forskningsradet.Core.Models.ContentModels.Pages
{
    [AvailableContentTypes(Availability.None)]
    public class PublicationBasePage : BasePageData
    {
        [Display(
            Order = 10,
            GroupName = GroupNames.Listevisning,
            Name = "Listebilde",
            Description = "Bilde som skal brukes når siden vises i liste.")]
        [UIHint(UIHint.Image)]

        public virtual ContentReference ListImage { get; set; }

        [Display(
            Order = 10,
            GroupName = GroupNames.Content,
            Name = "Undertittel",
            Description = "Publikasjonens undertittel.")]
        [CultureSpecific]
        public virtual string Subtitle { get; set; }

        [Display(
            Name = "Type publikasjon",
            Description = "Angi publikasjonens type.",
            GroupName = GroupNames.Content,
            Order = 20)]
        [BackingType(typeof(PropertyNumber))]
        [EditorDescriptor(EditorDescriptorType = typeof(EnumEditorDescriptor<PublicationType>))]
        public virtual PublicationType PublicationType { get; set; }

        [Display(
            Order = 30,
            GroupName = GroupNames.Content,
            Name = "År",
            Description = "Angi år.")]
        public virtual int Year { get; set; }

        [Display(
            Order = 80,
            GroupName = GroupNames.Content,
            Name = "Vedlegg",
            Description = "Angi vedlegg(PDF).")]
        [CultureSpecific]
        public virtual ContentReference Attachment { get; set; }

        [Searchable]
        public bool HasAttachment =>
            !ContentReference.IsNullOrEmpty(Attachment);

        [Searchable]
        public virtual string ComputedType =>
            PublicationType.ToString();

        [Searchable]
        public virtual string SearchSection =>
            ComputedType;
    }
}
