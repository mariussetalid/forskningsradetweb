﻿using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ContentModels.Support;
using Forskningsradet.Core.Models.Contracts;

namespace Forskningsradet.Core.Models.ContentModels.Pages
{
    [ContentPageType(
        GUID = "C3267CDD-290A-4F9D-B34F-0744620292AE",
        Order = 199,
        DisplayName = "Mappeside",
        Description = "Ikke en visningsside i seg selv, men en mappeside som kan organisere andre sider.")]
    [ImageUrl(IconConstants.Thumbnails.FolderPage)]
    public class FolderPage : NonContentPage, IPageHasNoView
    {
        public override void SetDefaultValues(ContentType contentType)
        {
            base.SetDefaultValues(contentType);

            VisibleInMenu = false;
        }
    }
}