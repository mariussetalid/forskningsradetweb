﻿using System.ComponentModel.DataAnnotations;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAnnotations;
using EPiServer.SpecializedProperties;
using EPiServer.Web;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ContentModels.Blocks.Settings;
using Forskningsradet.Core.Models.ContentModels.Support;

namespace Forskningsradet.Core.Models.ContentModels.Pages
{
    public abstract class FrontPageBase : EditorialPage
    {
        [Display(
            Order = 10,
            GroupName = GroupNames.Meldinger,
            Name = "Globale driftsmeldinger",
            Description = "Globale driftsmeldinger som vises på alle sider.")]
        [AllowedTypes(typeof(GlobalMessageBlock), typeof(LocalMessageBlock))]
        public virtual ContentArea GlobalMessages { get; set; }

        [Display(
            Order = 12,
            GroupName = GroupNames.Meldinger,
            Name = "Globale felles driftsmeldinger",
            Description = "Globale driftsmeldinger som vises på alle sider og som er felles på tvers av forsider.")]
        [UIHint(UIHint.AssetsFolder)]
        [PropertyAdminRestriction]
        public virtual ContentReference GlobalSharedMessages { get; set; }

        [Display(
            Order = 10,
            GroupName = GroupNames.NettstedsInnstillinger,
            Name = "Logo, standard",
            Description = "Sidens logo")]
        [UIHint(UIHint.Image)]
        public virtual ContentReference MainLogo { get; set; }

        [Display(
            Order = 11,
            GroupName = GroupNames.NettstedsInnstillinger,
            Name = "Logo, hvit",
            Description = "Sidens logo invertert, brukes når søkeboksen er aktiv.")]
        [UIHint(UIHint.Image)]
        public virtual ContentReference MainWhiteLogo { get; set; }

        [Display(
            Order = 12,
            GroupName = GroupNames.NettstedsInnstillinger,
            Name = "Språkspesifikk logo, standard",
            Description = "Overstyr standard logo for dette språket.")]
        [UIHint(UIHint.Image)]
        [CultureSpecific]
        public virtual ContentReference CultureSpecificLogo
        {
            get => this.GetPropertyValue(page => page.CultureSpecificLogo)
                   ?? this.GetPropertyValue(page => page.MainLogo);
            set => this.SetPropertyValue(page => page.CultureSpecificLogo, value);
        }

        [Display(
            Order = 13,
            GroupName = GroupNames.NettstedsInnstillinger,
            Name = "Språkspesifikk logo, hvit",
            Description = "Overstyr invertert logo for dette språket.")]
        [UIHint(UIHint.Image)]
        [CultureSpecific]
        public virtual ContentReference CultureSpecificWhiteLogo
        {
            get => this.GetPropertyValue(page => page.CultureSpecificWhiteLogo)
                   ?? this.GetPropertyValue(page => page.MainWhiteLogo)
                   ?? this.GetPropertyValue(page => page.CultureSpecificLogo)
                   ?? this.GetPropertyValue(page => page.MainLogo);
            set => this.SetPropertyValue(page => page.CultureSpecificWhiteLogo, value);
        }

        [Display(
            Order = 15,
            GroupName = GroupNames.NettstedsInnstillinger,
            Name = "Overstyr favicon.ico",
            Description = "Overstyrer favicon.ico med en annen fil. Brukes for å sette egne favicons på satellitter.")]
        [UIHint(UIHint.Image)]
        public virtual ContentReference FaviconIcoOverride { get; set; }

        [Display(
            Order = 16,
            GroupName = GroupNames.NettstedsInnstillinger,
            Name = "Overstyr favicon.svg",
            Description = "Overstyrer favicon.svg med en annen fil. Brukes for å sette egne favicons på satellitter.")]
        [UIHint(UIHint.Image)]
        public virtual ContentReference FaviconSvgOverride { get; set; }

        [Display(
            Order = 17,
            GroupName = GroupNames.NettstedsInnstillinger,
            Name = "Overstyr favicon.png 16x16",
            Description = "Overstyrer favicon.png 16x16 med en annen fil. Brukes for å sette egne favicons på satellitter.")]
        [UIHint(UIHint.Image)]
        public virtual ContentReference FaviconPng16Override { get; set; }

        [Display(
            Order = 18,
            GroupName = GroupNames.NettstedsInnstillinger,
            Name = "Overstyr favicon.png 32x32",
            Description = "Overstyrer favicon.png 32x32 med en annen fil. Brukes for å sette egne favicons på satellitter.")]
        [UIHint(UIHint.Image)]
        public virtual ContentReference FaviconPng32Override { get; set; }

        [Display(
            Order = 20,
            GroupName = GroupNames.NettstedsInnstillinger,
            Name = "Last opp favicon.png 96x96",
            Description = "Last opp favicon.png 96x96. Brukes for å sette egne favicons på satellitter.")]
        [UIHint(UIHint.Image)]
        public virtual ContentReference FaviconPng96Override { get; set; }

        [Display(
            Order = 21,
            GroupName = GroupNames.NettstedsInnstillinger,
            Name = "Last opp ms tile favicon.png 150x150",
            Description = "Last opp ms tile favicon.png 150x150. Brukes for å sette egne favicons på satellitter.")]
        [UIHint(UIHint.Image)]
        public virtual ContentReference FaviconPng150Override { get; set; }

        [Display(
            Order = 22,
            GroupName = GroupNames.NettstedsInnstillinger,
            Name = "Last opp apple touch favicon.png 180x180",
            Description = "Last opp apple touch favicon.png 180x180. Brukes for å sette egne favicons på satellitter.")]
        [UIHint(UIHint.Image)]
        public virtual ContentReference FaviconPng180Override { get; set; }

        [Display(
            Order = 23,
            GroupName = GroupNames.NettstedsInnstillinger,
            Name = "Last opp favicon.png 192x192",
            Description = "Last opp favicon.png 192x192. Brukes for å sette egne favicons på satellitter.")]
        [UIHint(UIHint.Image)]
        public virtual ContentReference FaviconPng192Override { get; set; }

        [Display(
            Order = 24,
            GroupName = GroupNames.NettstedsInnstillinger,
            Name = "Last opp favicon.png 512x512",
            Description = "Last opp favicon.png 512x512. Brukes for å sette egne favicons på satellitter.")]
        [UIHint(UIHint.Image)]
        public virtual ContentReference FaviconPng512Override { get; set; }

        [Display(
            Order = 25,
            GroupName = GroupNames.NettstedsInnstillinger,
            Name = "Bruk alle favicon fallback",
            Description = "Bruk alle favicon fallback. Dette vil sørge for at alle størrelsene på ikoner fallbacker til FR sine ikoner.")]
        public virtual bool UseFallbackIcons { get; set; }

        [Display(
            Order = 26,
            GroupName = GroupNames.NettstedsInnstillinger,
            Name = "Default SEO bilde",
            Description = "Hvis ikke bilde under SEOsettings-fanen er fylt ut, brukes dette bildet istedet.")]
        [UIHint(UIHint.Image)]
        public virtual ContentReference FallbackSeoImage { get; set; }

        [Display(
            Order = 30,
            GroupName = GroupNames.NettstedsInnstillinger,
            Name = "Logg inn - lenke (Synlig utenfor menyen på mobil)",
            Description = "Lenke for logg inn punktet i menyen.")]
        [CultureSpecific]
        public virtual Url LoginUrl { get; set; }

        [Display(
            Order = 31,
            GroupName = GroupNames.NettstedsInnstillinger,
            Name = "Logg inn - tekst",
            Description = "Tekst for lenke for logg inn punktet i menyen.")]
        [CultureSpecific]
        public virtual string LoginUrlText { get; set; }

        [Display(
            Order = 32,
            GroupName = GroupNames.NettstedsInnstillinger,
            Name = "Logg inn - åpne i ny fane",
            Description = "Konfigurer logg inn lenke til å åpnes i ny fane.")]
        public virtual bool LoginUrlIsExternal { get; set; }

        [Display(
            Order = 33,
            GroupName = GroupNames.NettstedsInnstillinger,
            Name = "Lenker i toppmeny",
            Description = "Lenker som vises på toppen av siden. På mobil vil lenkene vises øverst i menyen, og den siste lenken vil være synlig på toppen av siden.")]
        [CultureSpecific]
        public virtual LinkItemCollection HeaderLinks { get; set; }

        [Display(
            Order = 35,
            GroupName = GroupNames.NettstedsInnstillinger,
            Name = "Twitter SEO bruker",
            Description = "Twitter-brukernavn i tilknytning til websiden, brukes til analyse.")]
        public virtual string SeoTwitterSite { get; set; }

        [Display(
            Order = 40,
            GroupName = GroupNames.NettstedsInnstillinger,
            Name = "Facebook SEO application id",
            Description = "Unikt ID-nummer i forbindelse med Facebook-konto, brukes til analyse.")]
        public virtual string SeoFacebookAppId { get; set; }

        [Display(
            Order = 41,
            GroupName = GroupNames.NettstedsInnstillinger,
            Name = "Facebook",
            Description = "Delingslenke for Facebook.")]
        public virtual string FacebookShareBaseUrl { get; set; }

        [Display(
            Order = 42,
            GroupName = GroupNames.NettstedsInnstillinger,
            Name = "Facebook alt tekst")]
        public virtual string FacebookAltText { get; set; }

        [Display(
            Order = 45,
            GroupName = GroupNames.NettstedsInnstillinger,
            Name = "Linked In",
            Description = "Delingslenke for LinkedIn.")]
        public virtual string LinkedInShareUrl { get; set; }

        [Display(
            Order = 47,
            GroupName = GroupNames.NettstedsInnstillinger,
            Name = "LinkedIn alt tekst")]
        public virtual string LinkedInAltText { get; set; }

        [Display(
            Order = 50,
            GroupName = GroupNames.NettstedsInnstillinger,
            Name = "Google Tag Manager Id",
            Description = "GTM-XXXXXXX")]
        [Searchable(false)]
        public virtual string GoogleTagManagerId { get; set; }

        [Display(
            Order = 55,
            GroupName = GroupNames.NettstedsInnstillinger,
            Name = "Bruk Apsis marketing automation script på dette nettstedet",
            Description = "Hvis valgt legges det til script for Apsis marketing automation i header for dette nettstedet.")]
        [Searchable(false)]
        public virtual bool UseApsisMarketingAutomationScript { get; set; }

        [Display(
            Order = 60,
            GroupName = GroupNames.NettstedsInnstillinger,
            Name = "HSTS HTTP Header",
            Description = "Advarsel: Kan kun settes på startsiden for nettstedet.")]
        [PropertyAdminRestriction]
        [HiddenForNonStartPage]
        public virtual HstsHeaderSettingsBlock HstsHeaderSettings { get; set; }

        [Display(
            Order = 70,
            GroupName = GroupNames.NettstedsInnstillinger,
            Name = "Default bilde på avlyst arrangement",
            Description = "Vises på arrangementsiden på avlyste arrangementer.")]
        [UIHint(UIHint.Image)]
        [CultureSpecific]
        public virtual ContentReference CanceledEventImage { get; set; }

        [Display(
            Order = 80,
            GroupName = GroupNames.NettstedsInnstillinger,
            Name = "Default bilde på avlyst arrangement i lister",
            Description = "Vises i arrangementlister på avlyste arrangementer.")]
        [UIHint(UIHint.Image)]
        [CultureSpecific]
        public virtual ContentReference CanceledEventImageInList { get; set; }

        [Display(
            Order = 100,
            GroupName = GroupNames.NettstedsInnstillinger,
            Name = "Søkeside",
            Description = "Lenke til siden som skal brukes for søket.")]
        [AllowedTypes(typeof(SearchPage))]
        public virtual PageReference SearchPage { get; set; }

        [Display(
            Order = 110,
            GroupName = GroupNames.NettstedsInnstillinger,
            Name = "Øverste siden i hovedmenystrukturen",
            Description = "Lenke til siden som skal være toppen av hovedmenyen.")]
        [AllowedTypes(typeof(MenuStructurePage))]
        public virtual PageReference MainMenuRoot { get; set; }

        [Display(
            Order = 500,
            GroupName = GroupNames.NettstedsInnstillinger,
            Name = "Footer",
            Description = "Innstillinger for footeren.")]
        public virtual FooterBlock Footer { get; set; }
    }
}
