﻿using System;
using System.ComponentModel.DataAnnotations;
using EPiServer;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using Forskningsradet.Core.Models.ContentModels.Support;
using Forskningsradet.Core.Models.Contracts;
using Geta.SEO.Sitemaps.Models;

namespace Forskningsradet.Core.Models.ContentModels.Pages
{
    [SpecialPageType(
        GUID = "8CAEC9A9-B925-4CD0-9CA0-C0E394BFEAE7",
        DisplayName = "Side for ledig stilling",
        AvailableInEditMode = false)]
    [AvailableContentTypes(Availability.None)]
    public class VacancyPage : BasePageData, IPageHasNoViewIcon, IExcludeFromSitemap
    {
        [Editable(false)]
        public virtual int HrManagerId { get; set; }

        [Display(
            Order = 10,
            GroupName = GroupNames.Content,
            Name = "Tittel")]
        public virtual string Title { get; set; }

        [Display(
            Order = 20,
            GroupName = GroupNames.Content,
            Name = "Ingress")]
        [CultureSpecific]
        public virtual string MainIntro { get; set; }

        [Display(
            Order = 30,
            GroupName = GroupNames.Content,
            Name = "Søknadsfrist")]
        public virtual DateTime DueDate { get; set; }

        [Display(
            Order = 40,
            GroupName = GroupNames.Content)]
        public virtual string WorkplaceLocation { get; set; }

        [Display(
            Order = 50,
            GroupName = GroupNames.Content,
            Name = "Url",
            Description = "Url til side i HRManager")]
        public virtual Url HrManagerUri { get; set; }
    }
}