﻿using System.ComponentModel.DataAnnotations;
using Creuna.FluidImages;
using EPiServer.Core;
using EPiServer.DataAnnotations;
using EPiServer.Framework.DataAnnotations;

namespace Forskningsradet.Core.Models.ContentModels.Media
{
    /// <summary>
    /// Content type for images.
    /// </summary>
    [ContentType(DisplayName = "Bilde", GUID = "04626613-243E-4956-8C3A-75F9FDAD727A")]
    [MediaDescriptor(ExtensionString = "jpg,jpeg,jpe,ico,gif,bmp,png")]
    public class ImageFile : ImageData
    {
        /// <summary>
        /// Alternative text for the image.
        /// </summary>
        [CultureSpecific]
        [Display(Name = "Alternativ tekst",
            Description = "Vil bli brukt som bildets alternative tekst. Påkrevd i følge universell utforming.")]
        public virtual string AltText { get; set; }

        public virtual PercentageCoordinates FocusPoint { get; set; }
    }
}