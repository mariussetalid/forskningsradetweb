﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAnnotations;
using EPiServer.Framework.DataAnnotations;
using Forskningsradet.Core.Models.ContentModels.Contracts;

namespace Forskningsradet.Core.Models.ContentModels.Media
{
    /// <summary>
    /// Content type for files which are not images.
    /// Add to extensions to allow other files to be uploaded. 
    /// </summary>
    [ContentType(DisplayName = "Generisk fil", GUID = "47ED9CAA-E44A-4193-96E7-5FAD638581DE")]
    [MediaDescriptor(ExtensionString = "pdf,doc,docx,xls,xlsx,ppt,pptx")]
    public class GenericMedia : MediaData, IContentMediaMetaData
    {
        [Editable(false)]
        public virtual string FileExtension { get; set; }

        [Editable(false)]
        public virtual string FileSize { get; set; }
    }
}