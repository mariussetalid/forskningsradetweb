﻿using EPiServer.DataAnnotations;
using EPiServer.Framework.Blobs;
using EPiServer.Framework.DataAnnotations;

namespace Forskningsradet.Core.Models.ContentModels.Media
{
    [ContentType(GUID = "8031168B-3662-44B9-B29A-438C1C56B4C8")]
    [MediaDescriptor(ExtensionString = "svg")]
    public class VectorImageFile : ImageFile
    {
        public override Blob Thumbnail => BinaryData;
    }
}
