﻿namespace Forskningsradet.Core.Models.ContentModels.Contracts
{
    public interface IReactComponentData
    {
        string ReactComponentName();
        string ContentName();
        int ContentId();
    }
}
