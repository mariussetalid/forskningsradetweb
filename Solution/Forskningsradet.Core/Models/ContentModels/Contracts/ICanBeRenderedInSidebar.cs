﻿namespace Forskningsradet.Core.Models.ContentModels.Contracts
{
    public interface ICanBeRenderedInSidebar
    {
        string PartialReactComponentName();
    }
}
