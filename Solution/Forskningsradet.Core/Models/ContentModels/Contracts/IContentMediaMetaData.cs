﻿namespace Forskningsradet.Core.Models.ContentModels.Contracts
{
    public interface IContentMediaMetaData
    {
        string FileExtension { get; set; }
        string FileSize { get; set; }
    }
}
