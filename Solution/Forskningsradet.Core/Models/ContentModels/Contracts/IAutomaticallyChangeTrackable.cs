﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EPiServer.Core;

namespace Forskningsradet.Core.Models.ContentModels.Contracts
{
    public interface IAutomaticallyChangeTrackable : IChangeTrackable
    {
        bool DoNotSetChangedOnPublish { get; set; }
    }
}
