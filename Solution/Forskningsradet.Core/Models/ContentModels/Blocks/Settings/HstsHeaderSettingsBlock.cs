﻿using System;
using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAnnotations;
using EPiServer.Shell.ObjectEditing;
using Forskningsradet.Core.Factories;
using Forskningsradet.Core.HttpHeaders.HstsHeader;
using Forskningsradet.Core.Models.Enums;

namespace Forskningsradet.Core.Models.ContentModels.Blocks.Settings
{
    [ContentType(
        DisplayName = "Hsts Http header settings",
        Description = "",
        GUID = "c3acb9fa-5c53-453a-aa86-478bee91c440",
        AvailableInEditMode = false)]
    public class HstsHeaderSettingsBlock : BlockData
    {
        [Display( 
            Name = "Atferd, se <a href='https://hstspreload.org/'>https://hstspreload.org/</a>", 
            Description = "HSTS http header atferd, se https://hstspreload.org/ for mer info",
            Order = 10)]
        [SelectOne(SelectionFactoryType = typeof(EnumSelectionFactory<HstsHeaderBehaviour>))]
        public virtual HstsHeaderBehaviour Behaviour { get; set; }

        public HstsHeaderSettings Settings => BuildSettings();

        private HstsHeaderSettings BuildSettings()
        {
            switch (Behaviour)
            {
                case HstsHeaderBehaviour.Skip:
                    return new HstsHeaderSettings { SkipHeader = true };
                case HstsHeaderBehaviour.Enable5Mins:
                    return BuildEnabledHstsSettings(300);
                case HstsHeaderBehaviour.Enable1Week:
                    return BuildEnabledHstsSettings(604800);
                case HstsHeaderBehaviour.Enable1Month:
                    return BuildEnabledHstsSettings(2592000);
                case HstsHeaderBehaviour.Enable2YearsWithPreload:
                    return BuildEnabledHstsSettings(63072000, withPreload: true);
                case HstsHeaderBehaviour.Disable:
                    return new HstsHeaderSettings { IncludeSubdomains = false, Preload = false };
                default:
                    throw new NotSupportedException($"{nameof(HstsHeaderBehaviour)} {Behaviour} is not supported.");
            }
        }

        private HstsHeaderSettings BuildEnabledHstsSettings(int maxAgeSeconds, bool withPreload = false)
            => new HstsHeaderSettings
            {
                IncludeSubdomains = true,
                MaxAgeSeconds = maxAgeSeconds,
                Preload = withPreload,
            };
    }
}