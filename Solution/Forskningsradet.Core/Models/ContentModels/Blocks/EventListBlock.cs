﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAnnotations;
using EPiServer.Web;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ContentModels.Support;

namespace Forskningsradet.Core.Models.ContentModels.Blocks
{
    [ContentType(
        DisplayName = "Arrangementlisteblokk",
        Description = "Viser en liste av kommende arrangementer.",
        GroupName = GroupNames.Blocks.Uncommon,
        Order = 430,
        GUID = "54195329-31F7-4C05-93DC-C4FC40DEB5AA")]
    [ImageUrl(IconConstants.Thumbnails.EventListBlock)]
    public class EventListBlock : BaseBlockData
    {
        [Display(
            Name = "Tittel",
            Description = "Vises som blokkens tittel.",
            GroupName = GroupNames.Content,
            Order = 10)]
        [CultureSpecific]
        public virtual string Title { get; set; }

        [Display(
            Name = "Beskrivelse",
            Description = "Vises som blokkens beskrivelse.",
            GroupName = GroupNames.Content,
            Order = 20)]
        [CultureSpecific]
        [UIHint(UIHint.Textarea)]
        public virtual string Description { get; set; }

        [Display(
            Name = "Rotside for arrangementer",
            Description = "Her kan man angi en side som skal være rot for søket etter arrangementer. Bruk denne til å begrense hvor liten eller stor del av nettstedet listen skal hentes fra.",
            GroupName = GroupNames.Content,
            Order = 30)]
        public virtual PageReference PageRoot { get; set; }

        [Display(
            Name = "Lenke til arrangementer",
            Description = "Hvis fylt ut vises en knapp til flere arrangementer.",
            GroupName = GroupNames.Content,
            Order = 40)]
        public virtual EditLinkBlock Button { get; set; }

        public override string ReactComponentName()
            => nameof(ReactModels.FutureEvents);
    }
}