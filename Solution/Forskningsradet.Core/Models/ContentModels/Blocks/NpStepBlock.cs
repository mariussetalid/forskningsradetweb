﻿using System.ComponentModel.DataAnnotations;
using EPiServer.DataAnnotations;
using EPiServer.Web;
using Forskningsradet.Core.Models.ContentModels.Support;

namespace Forskningsradet.Core.Models.ContentModels.Blocks
{
    [ContentType(
        GUID = "84B526B5-2F98-4903-9751-6A56308FFD58",
        DisplayName = "Nysgjerrigpermetoden - Steginfo",
        Description = "Innhold på et steg i Nysgjerrigpermetoden",
        GroupName = GroupNames.Blocks.Uncommon,
        AvailableInEditMode = false)]
    public class NpStepBlock : BaseBlockData
    {
        [Display(
            Name = "Tittel",
            Description = "Overstyr tittel på steg.",
            GroupName = GroupNames.Content,
            Order = 10)]
        [CultureSpecific]
        public virtual string Title { get; set; }

        [Display(
            Name = "Ingress",
            Description = "Ingress på steg.",
            GroupName = GroupNames.Content,
            Order = 20)]
        [UIHint(UIHint.Textarea)]
        [CultureSpecific]
        public virtual string Intro { get; set; }

        [Display(
            Name = "Video",
            Description = "Youtubevideo brukt på toppen av steget.",
            GroupName = GroupNames.Content,
            Order = 30)]
        public virtual VideoBlock Video { get; set; }

        [Display(
            Name = "Bildeblokk med flere steg (Bruk minst 2 steg)",
            Description = "Bildeblokk med flere steg.",
            GroupName = GroupNames.Content,
            Order = 40)]
        public virtual NpStepImagesBlock ImageListBlock { get; set; }

        [Display(
            Name = "Sitatlisteblokk",
            Description = "Sitatlisteblokk som inneholder tittel og liste med 'Bilde med tekst/sitat' blokker",
            GroupName = GroupNames.Content,
            Order = 50)]
        public virtual NpQuotesBlock QuotesBlock { get; set; }

        public override string ReactComponentName() => nameof(ReactModels.StepsContent);
    }
}