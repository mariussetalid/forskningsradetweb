﻿using System.ComponentModel.DataAnnotations;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAnnotations;
using Forskningsradet.Core.Models.ContentModels.Support;

namespace Forskningsradet.Core.Models.ContentModels.Blocks
{
    [ContentType(
        DisplayName = "Rediger lenke",
        Description = "Blokk for å redigere knapp/lenke",
        GUID = "88DD1456-6738-44E1-863E-4CA564BCB86B",
        AvailableInEditMode = false)]
    public class EditLinkBlock : BlockData
    {
        [Display(
            Name = "Tekst",
            Description = "Teksten som skal vises på knappen.",
            GroupName = GroupNames.Content,
            Order = 10)]
        [CultureSpecific]
        public virtual string Text { get; set; }

        [Display(
            Name = "Lenke",
            Description = "Lenken som knappen skal gå til.",
            GroupName = GroupNames.Content,
            Order = 20)]
        public virtual Url Link { get; set; }

        [Display(
            Name = "Kampanje-ID",
            Description = "Valgfritt felt. Hvis fylt ut vil teksten brukes som html id på knappen og kan dermed brukes til å måle klikk på knappen i statistikkverktøy. IDen må være unik på sidene hvor den vises. Må starte med en bokstav (a-zA-Z) og kan ikke inneholde mellomrom.",
            GroupName = GroupNames.Content,
            Order = 10)]
        [CultureSpecific]
        public virtual string AnchorId { get; set; }
    }
}