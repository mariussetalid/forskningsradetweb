﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using EPiServer.Cms.Shell.UI.ObjectEditing.EditorDescriptors;
using EPiServer.DataAnnotations;
using EPiServer.Shell.ObjectEditing;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ContentModels.PropertyList;
using Forskningsradet.Core.Models.ContentModels.Support;

namespace Forskningsradet.Core.Models.ContentModels.Blocks
{
    [ContentType(
        GUID = "C58EEB08-1C24-4124-9075-87DB4BB598D5",
        DisplayName = "Nysgjerrigpermetoden blokk med flere bilder",
        Description = "Viser en liste av bilder ett om gangen.",
        GroupName = GroupNames.Blocks.Uncommon,
        Order = 400,
        AvailableInEditMode = false)]
    [ImageUrl(IconConstants.Thumbnails.CampaignBlock)]
    public class NpStepImagesBlock : BaseBlockData
    {
        [Display(
            Name = "Bildeliste",
            Description = "Elementene som skal vises.",
            GroupName = GroupNames.Content,
            Order = 10)]
        [EditorDescriptor(EditorDescriptorType = typeof(CollectionEditorDescriptor<NpStepImageListItem>))]
        [CultureSpecific]
        public virtual IList<NpStepImageListItem> ImageList { get; set; }

        public override string ReactComponentName() =>
            nameof(ReactModels.StepsBlock_Pages);
    }
}
