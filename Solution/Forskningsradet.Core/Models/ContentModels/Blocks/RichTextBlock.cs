﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAnnotations;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ContentModels.Support;

namespace Forskningsradet.Core.Models.ContentModels.Blocks
{
    [ContentType(
        DisplayName = "Fritekstblokk",
        Description = "Brukes til løpende tekst på landingssider.",
        GroupName = GroupNames.Blocks.Common,
        Order = 180,
        GUID = "38EFAFDA-AFAF-40F6-82D9-2FEF5CC70D73")]
    [ImageUrl(IconConstants.Thumbnails.RichTextBlock)]
    public class RichTextBlock : BaseBlockData
    {
        [Display(
            Name = "Fritekst (ikke tillatt å legge inn blokker)",
            Description = "Ment for løpende tekst med blant annet bilder, lenker og lister.",
            GroupName = GroupNames.Content,
            Order = 10)]
        [XhtmlStringRestrictContentTypes]
        [CultureSpecific]
        public virtual XhtmlString MainBody { get; set; }

        public override string ReactComponentName()
            => nameof(ReactModels.RichText);
    }
}
