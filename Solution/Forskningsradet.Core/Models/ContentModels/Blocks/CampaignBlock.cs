﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAnnotations;
using EPiServer.Shell.ObjectEditing;
using EPiServer.Web;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.EditorDescriptors;
using Forskningsradet.Core.Models.ContentModels.Support;

namespace Forskningsradet.Core.Models.ContentModels.Blocks
{
    [ContentType(
        DisplayName = "Kampanjeblokk",
        Description = "Brukes til å fremheve innhold en periode. Brukes i full bredde eller utfallende.",
        GroupName = GroupNames.Blocks.Common,
        Order = 120,
        GUID = "25EF962E-B439-48A4-84E2-1BA243CE8072")]
    [IncludeFullWidthPreview]
    [ImageUrl(IconConstants.Thumbnails.CampaignBlock)]
    public class CampaignBlock : BaseBlockData
    {
        [Display(
            Name = "Kampanjebilde",
            Description = "Vises som stort bilde i blokken.",
            GroupName = GroupNames.Content,
            Order = 10)]
        [HideOnContentCreate]
        [UIHint(UIHint.Image)]
        public virtual ContentReference Image { get; set; }

        [Display(
            Name = "Tittel",
            Description = "Vises som blokkens tittel.",
            GroupName = GroupNames.Content,
            Order = 20)]
        [CultureSpecific]
        public virtual string Title { get; set; }

        [Display(
            Name = "Knapp med lenke",
            Description = "Hvis fylt ut vises en knapp som lenker til satt url.",
            GroupName = GroupNames.Content,
            Order = 30)]
        public virtual EditLinkBlock Button { get; set; }

        [Display(
            Name = "Stil",
            Description = "Bestemmer hvilket utseende blokken skal ha.",
            GroupName = GroupNames.Content,
            Order = 40)]
        [BackingType(typeof(PropertyNumber))]
        [EditorDescriptor(EditorDescriptorType = typeof(EnumEditorDescriptor<CampaignBlockStyle>))]
        public virtual CampaignBlockStyle Style { get; set; }

        public override string ReactComponentName() =>
            nameof(ReactModels.CampaignBlock);
    }

    public enum CampaignBlockStyle
    {
        None = 0,
        DarkBlue = 1
    }
}
