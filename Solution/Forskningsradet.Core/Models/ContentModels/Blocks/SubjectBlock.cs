using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAnnotations;
using EPiServer.Web;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ContentModels.Support;
using Forskningsradet.Core.Models.Contracts;

namespace Forskningsradet.Core.Models.ContentModels.Blocks
{
    [ContentType(
        DisplayName = "Temablokk",
        Description = "Blokk for visning av et tema til bruk på utlysninger.",
        GroupName = GroupNames.Blocks.Uncommon,
        Order = 340,
        GUID = "A1288C40-7390-4E00-8A89-99315CC968BA",
        AvailableInEditMode = false)]
    public class SubjectBlock : BaseBlockData, IIdentifiable
    {
        [Display(
            Name = "Tema",
            GroupName = GroupNames.Content,
            Order = 10)]
        [Editable(false)]
        [Searchable(false)]
        [SingleCategorySelection(CategoryConstants.SubjectRoot)]
        public virtual int Subject { get; set; }

        [Display(
            Name = "TemaKode",
            GroupName = GroupNames.Content,
            Order = 13)]
        [Editable(false)]
        [Searchable(false)]
        public virtual string SubjectCode { get; set; }

        [Display(
            Name = "TemaNavn",
            GroupName = GroupNames.Content,
            Order = 16)]
        [Editable(false)]
        [CultureSpecific]
        public virtual string SubjectName { get; set; }

        [Display(
            Name = "Beskrivelse",
            GroupName = GroupNames.Content,
            Order = 20)]
        [CultureSpecific]
        [UIHint(UIHint.Textarea)]
        public virtual string Description { get; set; }

        [Display(
            Name = "Undertemaer",
            GroupName = GroupNames.Content,
            Order = 30)]
        [Editable(false)]
        [CultureSpecific]
        [UIHint(UIHint.Textarea)]
        public virtual string SubjectChildren { get; set; }

        [Display(
            Name = "Beskrivelse av undertema",
            GroupName = GroupNames.Content,
            Order = 40)]
        [CultureSpecific]
        [XhtmlStringRestrictContentTypes(AllowInXhtmlString.SomeMedia)]
        public virtual XhtmlString SubjectChildrenDescription { get; set; }

        [Display(
            Name = "Prioriteringer",
            GroupName = GroupNames.Content,
            Order = 50)]
        [AllowedTypes(typeof(SubjectPriorityBlock))]
        public virtual ContentArea Priorities { get; set; }

        public override string ReactComponentName() =>
            nameof(ReactModels.AccordionWithContentAreaList);

        public string Id =>
            string.IsNullOrEmpty(SubjectCode)
                ? Subject.ToString()
                : SubjectCode;
    }
}