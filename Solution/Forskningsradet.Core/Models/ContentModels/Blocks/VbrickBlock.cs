﻿using System.ComponentModel.DataAnnotations;
using EPiServer.DataAnnotations;
using EPiServer.Web;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ContentModels.Support;

namespace Forskningsradet.Core.Models.ContentModels.Blocks
{
    [ContentType(
        DisplayName = "Video fra Vbrick-blokk",
        Description = "Blokk for visning av video fra Vbrick.",
        GroupName = GroupNames.Blocks.Embed,
        Order = 310,
        GUID = "57EAB29B-7A37-4E63-BF0E-92ABF9CCCDA0")]
    [ImageUrl(IconConstants.Thumbnails.VbrickBlock)]
    public class VbrickBlock : EmbedBaseBlock
    {
        [Required]
        [Display(
            Name = "Src",
            Description = "Lenke til det eksterne innholdet som skal vises. Lenke må starte med https://videoportal.rcn.no/embed, og kan finnes i Embed-koden.",
            Prompt = EmbedSourceConstants.Vbrick.ValidSrcStart + "embed..",
            Order = 20,
            GroupName = GroupNames.Content)]
        [UIHint(UIHint.Textarea)]
        public override string Src { get; set; }
    }
}
