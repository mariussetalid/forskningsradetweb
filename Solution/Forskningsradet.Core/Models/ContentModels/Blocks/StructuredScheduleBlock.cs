﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using EPiServer.Cms.Shell.UI.ObjectEditing.EditorDescriptors;
using EPiServer.Core;
using EPiServer.DataAnnotations;
using EPiServer.Shell.ObjectEditing;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ContentModels.PropertyList;
using Forskningsradet.Core.Models.ContentModels.Support;

namespace Forskningsradet.Core.Models.ContentModels.Blocks
{
    [ContentType(
        DisplayName = "Agendablokk - Strukturert Dagsprogram",
        Description = "(Arrangement) Brukes som agenda på et arrangement.",
        GroupName = GroupNames.Blocks.Uncommon,
        Order = 440,
        GUID = "3DC39002-0C9D-4E24-99A4-E884B8B2A234")]
    [ImageUrl(IconConstants.Thumbnails.ScheduleBlock)]
    public class StructuredScheduleBlock : BaseBlockData
    {
        [Display(
            Order = 10,
            GroupName = GroupNames.Content,
            Name = "Tittel - tekst",
            Description = "Dagens tittel.")]
        [CultureSpecific]
        public virtual string Title { get; set; }

        [Display(
            Order = 20,
            GroupName = GroupNames.Content,
            Name = "Tittel - dato",
            Description = "Dagens tittel.")]
        public virtual DateTime? DateTitle { get; set; }

        [Display(
            Order = 30,
            GroupName = GroupNames.Content,
            Name = "Innholdsområde for meldingsblokk",
            Description = "Innholdsområde for å legge inn meldingsblokk.")]
        [AllowedTypes(typeof(LocalMessageBlock))]
        public virtual ContentArea ContentArea { get; set; }

        [Display(
            Order = 40,
            GroupName = GroupNames.Content,
            Name = "Tekst",
            Description = "Fritekstområde for agendaen.")]
        [XhtmlStringRestrictContentTypes]
        [CultureSpecific]
        public virtual XhtmlString RichText { get; set; }

        [Display(
            Order = 50,
            GroupName = GroupNames.Content,
            Name = "Programliste - siste aktivitet må være en pauseaktivitet",
            Description = "Liste over dagens program.")]
        [EditorDescriptor(EditorDescriptorType = typeof(CollectionEditorDescriptor<ScheduleItem>))]
        [CultureSpecific]
        public virtual IList<ScheduleItem> ProgramList { get; set; }

        public override string ReactComponentName() =>
            nameof(ReactModels.AccordionWithContentAreaList);
    }
}
