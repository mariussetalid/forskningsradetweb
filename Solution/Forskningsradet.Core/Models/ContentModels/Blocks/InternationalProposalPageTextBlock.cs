﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAnnotations;
using EPiServer.Web;
using Forskningsradet.Core.Models.ContentModels.Support;

namespace Forskningsradet.Core.Models.ContentModels.Blocks
{
    [ContentType(
        DisplayName = "Innhold",
        GroupName = GroupNames.Blocks.Uncommon,
        Order = 800,
        GUID = "2442BBD0-DAD6-43DA-8FA0-A593D31F2083",
        AvailableInEditMode = false)]
    public class InternationalProposalPageTextBlock : BaseBlockData
    {
        [Display(
            Name = "Undertittel",
            GroupName = GroupNames.Content,
            Order = 10)]
        [CultureSpecific]
        public virtual string Title { get; set; }

        [Display(
            Name = "Ingress",
            GroupName = GroupNames.Content,
            Order = 20)]
        [UIHint(UIHint.Textarea)]
        [CultureSpecific]
        public virtual string Intro { get; set; }

        [Display(
            Name = "Tekst",
            GroupName = GroupNames.Content,
            Order = 30)]
        [CultureSpecific]
        public virtual XhtmlString RichText { get; set; }

        public override string ReactComponentName() => null;
    }
}
