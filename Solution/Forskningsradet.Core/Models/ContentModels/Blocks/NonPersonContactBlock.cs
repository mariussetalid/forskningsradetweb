﻿using System.ComponentModel.DataAnnotations;
using EPiServer.DataAnnotations;
using Forskningsradet.Core.Models.ContentModels.Support;

namespace Forskningsradet.Core.Models.ContentModels.Blocks
{
    [ContentType(
        DisplayName = "Kontaktinformasjonblokk",
        Description = "Blokk for generell kontaktinformasjon.",
        GroupName = GroupNames.Blocks.Uncommon,
        Order = 470,
        GUID = "96636A79-C297-4005-A2AD-F2E84C48103B")]
    public class NonPersonContactBlock : BaseBlockData
    {
        [Display(
            Name = "Tittel",
            Description = "Valgfri tittel.",
            GroupName = GroupNames.Content,
            Order = 10)]
        [CultureSpecific]
        public virtual string Title { get; set; }

        [Display(
            Name = "Tekst",
            Description = "Valgfri tekst.",
            GroupName = GroupNames.Content,
            Order = 20)]
        [CultureSpecific]
        public virtual string Text { get; set; }

        [Display(
            Name = "E-post",
            Description = "Valgfri epostadresse.",
            GroupName = GroupNames.Content,
            Order = 30)]
        public virtual string Email { get; set; }

        [Display(
            Name = "Telefon",
            Description = "Valgfritt telefonnummer.",
            GroupName = GroupNames.Content,
            Order = 40)]
        public virtual string Phone { get; set; }

        public override string ReactComponentName() => nameof(ReactModels.ContactInfo);
    }
}
