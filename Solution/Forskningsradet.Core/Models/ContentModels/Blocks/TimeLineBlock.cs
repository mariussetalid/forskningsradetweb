﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using EPiServer.Cms.Shell.UI.ObjectEditing.EditorDescriptors;
using EPiServer.Core;
using EPiServer.DataAnnotations;
using EPiServer.Shell.ObjectEditing;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.EditorDescriptors;
using Forskningsradet.Core.Models.ContentModels.PropertyList;
using Forskningsradet.Core.Models.ContentModels.Support;

namespace Forskningsradet.Core.Models.ContentModels.Blocks
{
    [ContentType(
        DisplayName = "Tidslinjeblokk",
        Description = "Brukes i full bredde eller utfallende. Blå med datoer, eller grå tidslinje uten dato.",
        GroupName = GroupNames.Blocks.Common,
        Order = 190,
        GUID = "ED131DC6-C816-4D04-84CC-B0492BF5F8B1")]
    [IncludeFullWidthPreview]
    [ImageUrl(IconConstants.Thumbnails.TimeLineBlock)]
    public class TimeLineBlock : BaseBlockData
    {
        [Display(
            Name = "Tittel",
            Description = "Tidslinjens tittel",
            GroupName = GroupNames.Content,
            Order = 10)]
        [CultureSpecific]
        [Searchable(false)]
        public virtual string Title { get; set; }

        [Display(
            Name = "Stil - Ikke i bruk, skal fjernes",
            Description = "Velg tidslinjens utseende.",
            GroupName = GroupNames.AdministratorInnstillinger,
            Order = 20)]
        [BackingType(typeof(PropertyNumber))]
        [EditorDescriptor(EditorDescriptorType = typeof(EnumEditorDescriptor<TimelineBlockStyle>))]
        [Editable(false)]
        public virtual TimelineBlockStyle Style { get; set; }

        [Display(
            Name = "Tidslinjeelement",
            Description = "Elementene som skal vises på tidslinjen.",
            GroupName = GroupNames.Content,
            Order = 30)]
        [EditorDescriptor(EditorDescriptorType = typeof(CollectionEditorDescriptor<TimeLineItem>))]
        [CultureSpecific]
        public virtual IList<TimeLineItem> TimeLineItems { get; set; }

        public override string ReactComponentName() => nameof(ReactModels.TimelineBlock);
    }

    public enum TimelineBlockStyle
    {
        None = 0,
        Gray = 1
    }
}
