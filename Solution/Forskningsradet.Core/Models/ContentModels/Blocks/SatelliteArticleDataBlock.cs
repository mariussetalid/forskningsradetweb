﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAnnotations;
using EPiServer.Web;
using Forskningsradet.Core.Models.ContentModels.Support;

namespace Forskningsradet.Core.Models.ContentModels.Blocks
{
    [ContentType(
        DisplayName = "Data for artikler til satellitter",
        Description = "Importerte data for satelitter til bruk på artikkelsider.",
        GUID = "A5EFE229-D851-4612-8D1C-085EC36ADD40",
        AvailableInEditMode = false)]
    public class SatelliteArticleDataBlock : BlockData
    {
        [Display(
            Order = 10,
            GroupName = GroupNames.MigratedContent,
            Name = "Id")]
        public virtual int Id { get; set; }

        [Display(
            Order = 20,
            GroupName = GroupNames.MigratedContent,
            Name = "Title")]
        [UIHint(UIHint.PreviewableText)]
        public virtual string Title { get; set; }

        [Display(
            Order = 30,
            GroupName = GroupNames.MigratedContent,
            Name = "Url")]
        [UIHint(UIHint.PreviewableText)]
        public virtual string Url { get; set; }

        [Display(
            Order = 40,
            GroupName = GroupNames.MigratedContent,
            Name = "Text")]
        [UIHint(UIHint.Textarea)]
        public virtual string MainText { get; set; }

        [Display(
            Order = 50,
            GroupName = GroupNames.MigratedContent,
            Name = "Authors")]
        public virtual IList<string> Authors { get; set; }

        [Display(
            Order = 60,
            GroupName = GroupNames.MigratedContent,
            Name = "Url Id")]
        [UIHint(UIHint.PreviewableText)]
        public virtual string UrlId { get; set; }

        [Display(
            Order = 70,
            GroupName = GroupNames.MigratedContent,
            Name = "Date")]
        [UIHint(UIHint.PreviewableText)]
        public virtual string Date { get; set; }

        [Display(
            Order = 80,
            GroupName = GroupNames.MigratedContent,
            Name = "Readable Url")]
        [UIHint(UIHint.PreviewableText)]
        public virtual string ReadableUrl { get; set; }

        [Display(
            Order = 90,
            GroupName = GroupNames.MigratedContent,
            Name = "Factbox")]
        [UIHint(UIHint.Textarea)]
        public virtual string Factbox { get; set; }

        [Display(
            Order = 100,
            GroupName = GroupNames.MigratedContent,
            Name = "Creation Date")]
        [UIHint(UIHint.PreviewableText)]
        public virtual string CreationDate { get; set; }

        [Display(
            Order = 110,
            GroupName = GroupNames.MigratedContent,
            Name = "Effective Date")]
        [UIHint(UIHint.PreviewableText)]
        public virtual string EffectiveDate { get; set; }

        [Display(
            Order = 120,
            GroupName = GroupNames.MigratedContent,
            Name = "Show Published Date")]
        [UIHint(UIHint.PreviewableText)]
        public virtual string ShowPublishedDate { get; set; }

        [Display(
            Order = 130,
            GroupName = GroupNames.MigratedContent,
            Name = "About")]
        public virtual IList<string> About { get; set; }

        [Display(
            Order = 140,
            GroupName = GroupNames.MigratedContent,
            Name = "Image Url")]
        [UIHint(UIHint.PreviewableText)]
        public virtual string ImageUrl { get; set; }

        [Display(
            Order = 141,
            GroupName = GroupNames.MigratedContent,
            Name = "Image Photographer")]
        [UIHint(UIHint.PreviewableText)]
        public virtual string ImagePhotographer { get; set; }

        [Display(
            Order = 142,
            GroupName = GroupNames.MigratedContent,
            Name = "Image Readable Url")]
        [UIHint(UIHint.PreviewableText)]
        public virtual string ImageReadableUrl { get; set; }

        [Display(
            Order = 143,
            GroupName = GroupNames.MigratedContent,
            Name = "Image Original")]
        [UIHint(UIHint.PreviewableText)]
        public virtual string ImageOriginal { get; set; }

        [Display(
            Order = 144,
            GroupName = GroupNames.MigratedContent,
            Name = "Image Alt Text")]
        [UIHint(UIHint.PreviewableText)]
        public virtual string ImageAltText { get; set; }

        [Display(
            Order = 150,
            GroupName = GroupNames.MigratedContent,
            Name = "Ingress Image Url")]
        [UIHint(UIHint.PreviewableText)]
        public virtual string IngressImageUrl { get; set; }

        [Display(
            Order = 151,
            GroupName = GroupNames.MigratedContent,
            Name = "Ingress Image Photographer")]
        [UIHint(UIHint.PreviewableText)]
        public virtual string IngressImagePhotographer { get; set; }

        [Display(
            Order = 152,
            GroupName = GroupNames.MigratedContent,
            Name = "Ingress Image Readable Url")]
        [UIHint(UIHint.PreviewableText)]
        public virtual string IngressImageReadableUrl { get; set; }

        [Display(
            Order = 153,
            GroupName = GroupNames.MigratedContent,
            Name = "Ingress Image Original")]
        [UIHint(UIHint.PreviewableText)]
        public virtual string IngressImageOriginal { get; set; }

        [Display(
            Order = 154,
            GroupName = GroupNames.MigratedContent,
            Name = "Ingress Image Alt Text")]
        [UIHint(UIHint.PreviewableText)]
        public virtual string IngressImageAltText { get; set; }

        [Display(
            Order = 160,
            GroupName = GroupNames.MigratedContent,
            Name = "Campaign Image Photographer")]
        [UIHint(UIHint.PreviewableText)]
        public virtual string CampaignImagePhotographer { get; set; }

        [Display(
            Order = 161,
            GroupName = GroupNames.MigratedContent,
            Name = "Campaign Image Original")]
        [UIHint(UIHint.PreviewableText)]
        public virtual string CampaignImageOriginal { get; set; }

        [Display(
            Order = 162,
            GroupName = GroupNames.MigratedContent,
            Name = "Campaign Image Alt Text")]
        [UIHint(UIHint.PreviewableText)]
        public virtual string CampaignImageAltText { get; set; }

        [Display(
            Order = 170,
            GroupName = GroupNames.MigratedContent,
            Name = "Also Published In")]
        public virtual IList<string> AlsoPublishedIn { get; set; }

        [Display(
            Order = 180,
            GroupName = GroupNames.MigratedContent,
            Name = "Syndikerbar")]
        [UIHint(UIHint.PreviewableText)]
        public virtual string CanBeSyndicated { get; set; }

        [Display(
            Order = 190,
            GroupName = GroupNames.MigratedContent,
            Name = "Modified")]
        [UIHint(UIHint.PreviewableText)]
        public virtual string Modified { get; set; }

        [Display(
            Order = 200,
            GroupName = GroupNames.MigratedContent,
            Name = "Review State")]
        [UIHint(UIHint.PreviewableText)]
        public virtual string ReviewState { get; set; }
    }
}