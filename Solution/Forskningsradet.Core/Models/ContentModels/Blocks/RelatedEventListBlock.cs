﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAnnotations;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ContentModels.Contracts;
using Forskningsradet.Core.Models.ContentModels.Support;

namespace Forskningsradet.Core.Models.ContentModels.Blocks
{
    [ContentType(
        DisplayName = "Relaterte arrangementer",
        Description = "Viser en liste av relaterte arrangementer basert på kategorier.",
        GroupName = GroupNames.Blocks.RelatedContent,
        Order = 510,
        GUID = "F551ACF3-7777-4C2D-AE34-2BAA0B6F760E")]
    [ImageUrl(IconConstants.Thumbnails.RelatedEventListBlock)]
    public class RelatedEventListBlock : RelatedListBaseBlock, ICanBeRenderedInSidebar
    {
        [Display(
            Name = "(Valgfri) Rotside for arrangementer",
            Description = "Her kan man angi en side som skal være rot for søket etter arrangementer. Bruk denne til å begrense hvor liten eller stor del av nettstedet listen skal hentes fra.")]
        public override PageReference PageRoot { get; set; }

        [Display(
            Name = "Porteføljevisning",
            Description = "Skrur på porteføljevisning for blokken.",
            GroupName = GroupNames.Content,
            Order = 50)]
        public virtual bool PortfolioMode { get; set; }

        public override string ReactComponentName() => nameof(ReactModels.RelatedDates);

        public string PartialReactComponentName() => ReactComponentName();
    }
}