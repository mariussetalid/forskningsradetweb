﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAnnotations;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ContentModels.Support;

namespace Forskningsradet.Core.Models.ContentModels.Blocks
{
    [ContentType(
        DisplayName = "Agendablokk (Utdatert)",
        Description = "Brukt på tidligere arrangementer. Bruk heller blokken: Agendablokk - Strukturert Dagsprogram",
        GroupName = GroupNames.Blocks.Uncommon,
        Order = 445,
        GUID = "E4DF2263-808C-4310-B3F9-1A9E29AF750E")]
    [ImageUrl(IconConstants.Thumbnails.ScheduleBlock)]
    public class ScheduleBlock : BaseBlockData
    {
        [Display(
            Name = "Tittel",
            Description = "Blokkens tittel.",
            GroupName = GroupNames.Content,
            Order = 10)]
        [CultureSpecific]
        public virtual string Title { get; set; }

        [Display(
            Order = 20,
            GroupName = GroupNames.Content,
            Name = "Tidspunkter",
            Description = "Fritekstområde for agendaen.")]
        [XhtmlStringRestrictContentTypes]
        [CultureSpecific]
        public virtual XhtmlString Times { get; set; }

        public override string ReactComponentName()
            => nameof(ReactModels.AccordionWithContentAreaList);
    }
}
