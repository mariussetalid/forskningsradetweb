﻿using EPiServer.DataAnnotations;
using Forskningsradet.Core.Models.ContentModels.Support;

namespace Forskningsradet.Core.Models.ContentModels.Blocks
{
    [ContentType(
        DisplayName = "Lokal driftsmelding",
        Description = "Brukes til å sette meldinger som vises på denne siden.",
        GroupName = GroupNames.Blocks.Uncommon,
        Order = 490,
        GUID = "BEE24294-14AA-4FFE-AA8D-FADB29189D00")]
    public class LocalMessageBlock : MessageBaseBlock
    {
        public override string ReactComponentName() =>
            nameof(ReactModels.Message);
    }
}
