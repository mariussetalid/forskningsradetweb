﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAnnotations;
using EPiServer.Shell.ObjectEditing;
using EPiServer.Web;
using Forskningsradet.Core.EditorDescriptors;
using Forskningsradet.Core.Models.ContentModels.Support;

namespace Forskningsradet.Core.Models.ContentModels.Blocks
{
    [ContentType(
        DisplayName = "Arrangementinfo",
        GUID = "BE4491CE-6294-4520-AFF3-E7245809B1C2",
        AvailableInEditMode = false)]
    public class EventDataBlock : BaseBlockData
    {
        [Display(
            Order = 10,
            GroupName = GroupNames.Content,
            Name = "Når: [Dag, dato, klokkeslett]",
            Description = "Vises i arrangementsdetaljene. Fritekstfelter for når arrangementet går.")]
        [UIHint(UIHint.Textarea)]
        [CultureSpecific]
        public virtual IList<string> WhenList { get; set; }

        [Display(
            Order = 20,
            GroupName = GroupNames.Content,
            Name = "Hvor: [Rom, adresse, by]",
            Description = "Fritekstfelt for hvor arrangementet holdes.")]
        [CultureSpecific]
        public virtual string Where { get; set; }

        [Display(
            Name = "Type:",
            Description = "Hvilken type arrangement dette er.",
            GroupName = GroupNames.Content,
            Order = 30)]
        [BackingType(typeof(PropertyNumber))]
        [EditorDescriptor(EditorDescriptorType = typeof(EnumEditorDescriptor<EventType>))]
        public virtual EventType Type { get; set; }

        [Display(
            Order = 35,
            GroupName = GroupNames.Content,
            Name = "Opptak blir tilgjengeliggjort",
            Description = "Opptak av digitalt arrangement blir gjort tilgjengelig.")]
        public virtual bool RecordingWillBeAvailable { get; set; }

        [Display(
            Order = 40,
            GroupName = GroupNames.Content,
            Name = "Passer for: [Målgruppe]",
            Description = "Fritekst om hvem dette arrangementet passer for.")]
        [CultureSpecific]
        public virtual string TargetGroup { get; set; }

        [Display(
            Order = 50,
            GroupName = GroupNames.Content,
            Name = "Pris: [Pris]",
            Description = "Fritekstfelt om arrangementets pris.")]
        [CultureSpecific]
        public virtual string Price { get; set; }

        [Display(
            Order = 60,
            GroupName = GroupNames.Content,
            Name = "Påmeldingsfrist: [Påmeldingsfrist]",
            Description = "Fritekstfelt om arrangementets påmeldingsfrist.")]
        [CultureSpecific]
        public virtual string Deadline { get; set; }

        [Display(
            Order = 70,
            GroupName = GroupNames.Content,
            Name = "Neste kurs: [Dato for neste kurs]",
            Description = "Fritekstfelt om når neste arrangement av samme type går, hvis relevant.")]
        [CultureSpecific]
        public virtual string Next { get; set; }

        public override string ReactComponentName()
            => nameof(ReactModels.EventData);
    }

    public enum EventType
    {
        None = 0,
        Meeting = 1,
        Course = 2,
        Seminar = 3,
        Conference = 4,
        Workshop = 5,
        Debate = 6,
        Digital = 7
    }
}