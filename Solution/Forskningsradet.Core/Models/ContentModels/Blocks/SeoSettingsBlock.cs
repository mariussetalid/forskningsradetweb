﻿using System.ComponentModel.DataAnnotations;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAnnotations;
using EPiServer.Web;
using Forskningsradet.Core.Models.ContentModels.Support;

namespace Forskningsradet.Core.Models.ContentModels.Blocks
{
    [ContentType(
        DisplayName = "SEO innstillinger",
        GUID = "096E568B-7CB5-46C8-B21E-6CA70A08392F",
        AvailableInEditMode = false)]
    public class SeoSettingsBlock : BlockData
    {
        [Display(
            Name = "Tittel",
            Description = "Sidenavn brukes hvis feltet er tomt",
            Order = 10,
            GroupName = GroupNames.Content)]
        [CultureSpecific]
        public virtual string Title { get; set; }

        [Display(
            Name = "Metabeskrivelse",
            Order = 20,
            Description = "Brukes av søkemotorer",
            GroupName = GroupNames.Content)]
        [UIHint(UIHint.Textarea)]
        [CultureSpecific]
        public virtual string MetaDescription { get; set; }

        [Display(
            Name = "Open Graph: Tittel</br>(Anbefalt maks: 70 tegn)",
            Description = "Dette er tittelen på innholdet når det deles.",
            Order = 30,
            GroupName = GroupNames.Content)]
        [CultureSpecific]
        public virtual string OpenGraphTitle { get; set; }

        [Display(
            Name = "Open Graph: Beskrivelse<br/>(Anbefalt maks: 155 tegn)",
            Description = "En beskrivelse av siden på en til to setninger. Brukes når innholdet deles. Brukes ikke av søkemotorer",
            Order = 40,
            GroupName = GroupNames.Content)]
        [UIHint(UIHint.Textarea)]
        [CultureSpecific]
        public virtual string OpenGraphDescription { get; set; }

        [Display(
             Name = "Sett Open Graph: Type til å være uspesifisert",
             Description = "Velg denne hvis siden ikke skal ha typen 'article'.",
             Order = 50,
            GroupName = GroupNames.Content)]
        public virtual bool HideOpenGraphType { get; set; }

        [Ignore]
        public virtual string OpenGraphType
            => HideOpenGraphType
                ? null
                : "article";

        [Display(
            Name = "Open Graph: Bilde<br/>Hovedbildet på siden (hvis det finnes) vil bli brukt dersom feltet er tomt",
            Description = "Dette er bildet som benyttes når innholdet deles i sosiale medier.",
            Order = 60,
            GroupName = GroupNames.Content)]
        [UIHint(UIHint.Image)]
        public virtual Url OpenGraphImage { get; set; }

        [Display(
            Name = "Sett meta NOINDEX",
            Order = 70,
            Description = "Hvis valgt settes NOINDEX i sidens metadata. Da vil søkemotorer ikke indeksere denne siden og den vil ikke kunne finnes via for eksempel Google.",
            GroupName = GroupNames.Content)]
        public virtual bool MetaNoIndex { get; set; }

        [Display(
            Name = "Sett meta NOFOLLOW",
            Order = 80,
            Description = "Hvis valgt settes NOFOLLOW i sidens metadata. Da vil søkemotorer ikke følge lenker fra denne siden og lenkene vil for eksempel ikke telle i Googles Pagerank rangering.",
            GroupName = GroupNames.Content)]
        public virtual bool MetaNoFollow { get; set; }
    }
}