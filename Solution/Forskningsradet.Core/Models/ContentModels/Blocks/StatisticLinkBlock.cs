﻿using System.ComponentModel.DataAnnotations;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAnnotations;
using EPiServer.Web;
using Forskningsradet.Core.Models.ContentModels.Media;
using Forskningsradet.Core.Models.ContentModels.Support;

namespace Forskningsradet.Core.Models.ContentModels.Blocks
{
    [ContentType(
        DisplayName = "Statistikk lenke blokk",
        Description = "Viser statistikk-lenke i listeblokken.",
        GroupName = GroupNames.Blocks.Uncommon,
        Order = 435,
        GUID = "9E8F565A-C016-4C2D-85B6-00C9333490DD")]
    public class StatisticLinkBlock : BlockData
    {
        [Display(
            Name = "Bilde",
            Description = "Ikon som vises sammen med teksten.",
            GroupName = GroupNames.Content,
            Order = 10)]
        [UIHint(UIHint.Image)]
        [AllowedTypes(typeof(VectorImageFile))]
        public virtual ContentReference Image { get; set; }

        [Display(
            Name = "Tittel",
            Description = "Vises som tittel på elementet.",
            GroupName = GroupNames.Content,
            Order = 20)]
        [CultureSpecific]
        public virtual string Title { get; set; }

        [Display(
            Name = "Tekst",
            Description = "Tekst for lenken.",
            GroupName = GroupNames.Content,
            Order = 30)]
        [UIHint(UIHint.Textarea)]
        [CultureSpecific]
        public virtual string Text { get; set; }

        [Display(
            Name = "Lenke",
            Description = "Url for lenken.",
            GroupName = GroupNames.Content,
            Order = 40)]
        public virtual Url Url { get; set; }
    }
}