﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAnnotations;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ContentModels.Support;

namespace Forskningsradet.Core.Models.ContentModels.Blocks
{
    [ContentType(
        DisplayName = "Faktaboksblokk",
        Description = "Brukes på nyhets- og artikkelsider i løpende tekst og høyremarg.",
        GroupName = GroupNames.Blocks.Common,
        Order = 210,
        GUID = "3E472A69-BEB5-46D0-8412-7FA77245AFA2")]
    [ImageUrl(IconConstants.Thumbnails.AccordionBlock)]
    public class AccordionBlock : BaseBlockData
    {
        [Display(
            Name = "Tittel",
            Description = "Vil vises som blokkens tittel.",
            GroupName = GroupNames.Content,
            Order = 10)]
        [CultureSpecific]
        public virtual string Title { get; set; }

        [Display(
            Name = "Tekst",
            Description = "Vil vises som tekst i blokken.",
            GroupName = GroupNames.Content,
            Order = 20)]
        [XhtmlStringRestrictContentTypes]
        [CultureSpecific]
        public virtual XhtmlString Text { get; set; }

        [Display(
            Name = "Åpen",
            Description = "Vis trekkspill som åpen i utgangspunktet.",
            GroupName = GroupNames.Content,
            Order = 30)]
        public virtual bool InitiallyOpen { get; set; }

        public override string ReactComponentName()
            => nameof(ReactModels.AccordionBlock);
    }
}
