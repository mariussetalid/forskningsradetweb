﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAnnotations;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ContentModels.Support;

namespace Forskningsradet.Core.Models.ContentModels.Blocks
{
    [ContentType(
        DisplayName = "Kampanjelisteblokk",
        Description = "Liste med kampanjeblokker",
        GroupName = GroupNames.Blocks.Common,
        Order = 125,
        GUID = "6819CE06-690F-4134-8393-2BF32AFEAE44")]
    [IncludeFullWidthPreview]
    [ImageUrl(IconConstants.Thumbnails.CampaignBlock)]
    public class CampaignListBlock : BaseBlockData
    {
        [Display(
            Name = "Kampanjeblokker",
            Description = "Liste med kampanjer som skal vises i blokken.",
            GroupName = GroupNames.Content,
            Order = 10)]
        [AllowedTypes(typeof(CampaignBlock))]
        public virtual ContentArea Campaigns { get; set; }

        public override string ReactComponentName() =>
            nameof(ReactModels.CampaignBlockList);
    }
}
