﻿using System.ComponentModel.DataAnnotations;
using EPiServer;
using EPiServer.DataAnnotations;
using Forskningsradet.Core.Models.ContentModels.Contracts;
using Forskningsradet.Core.Models.ContentModels.Support;

namespace Forskningsradet.Core.Models.ContentModels.Blocks
{
    [ContentType(
        DisplayName = "MediaBlokk",
        Description = "Felles blokk for bilder, YouTube-videoer og videoer og grafer som vises i en iframe.",
        GroupName = GroupNames.Blocks.Embed,
        Order = 140,
        GUID = "0D7DB152-CB77-4C2D-9AE8-EA951A7ED96F")]
    public class MediaBlock : BaseBlockData, ICanBeRenderedInSidebar
    {
        [Display(
            Name = "Tittel",
            GroupName = GroupNames.Content,
            Order = 10)]
        [CultureSpecific]
        public virtual string Title { get; set; }

        [Display(
            Name = "Lenke for tittel",
            Description = "Lenke vises kun når blokken brukes på landingsside",
            GroupName = GroupNames.Content,
            Order = 20)]
        public virtual Url Url { get; set; }

        [Display(
            Name = "Undertekst (kun på innholdssider)",
            Description = "Undertekst vises kun når blokken brukes i riktekstfelt på artikkelsider",
            GroupName = GroupNames.Content,
            Order = 30)]
        [CultureSpecific]
        public virtual string Caption { get; set; }

        [Display(
            Name = "Legg til Bilde",
            GroupName = GroupNames.Content,
            Order = 40)]
        public virtual MediaBlockImageBlock ImageBlock { get; set; }

        [Display(
            Name = "Legg til Youtube video",
            GroupName = GroupNames.Content,
            Order = 50)]
        public virtual VideoBlock VideoBlock { get; set; }

        [Display(
            Name = "Legg til video eller graf som skal vises i iFrame",
            GroupName = GroupNames.Content,
            Order = 60)]
        public virtual MediaBlockEmbedBlock EmbedBlock { get; set; }

        public override string ReactComponentName() => nameof(ReactModels.MediaBlock);

        public string PartialReactComponentName() => ReactComponentName();
    }
}
