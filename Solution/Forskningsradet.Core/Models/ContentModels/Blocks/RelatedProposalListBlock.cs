﻿using EPiServer.Core;
using EPiServer.DataAnnotations;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ContentModels.Contracts;
using Forskningsradet.Core.Models.ContentModels.Support;

namespace Forskningsradet.Core.Models.ContentModels.Blocks
{
    [ContentType(
        DisplayName = "Relaterte utlysninger",
        Description = "Viser en liste av relaterte utlysninger basert på kategorier.",
        GroupName = GroupNames.Blocks.RelatedContent,
        Order = 520,
        GUID = "8E47435D-FDA3-4A84-B140-02027FDFAE6D")]
    [ImageUrl(IconConstants.Thumbnails.RelatedProposalListBlock)]
    public class RelatedProposalListBlock : RelatedListBaseBlock, ICanBeRenderedInSidebar
    {
        public override PageReference PageRoot { get; set; }

        public override string ReactComponentName() => nameof(ReactModels.RelatedDates);

        public string PartialReactComponentName() => ReactComponentName();
    }
}