﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAnnotations;
using Forskningsradet.Core.Models.ContentModels.Support;

namespace Forskningsradet.Core.Models.ContentModels.Blocks
{
    public abstract class RelatedListBaseBlock : BaseBlockData
    {
        [Display(
            Name = "Tittel",
            GroupName = GroupNames.Content,
            Order = 10)]
        [CultureSpecific]
        public virtual string Title { get; set; }

        [Display(
            Name = "Antall som skal vises",
            Description = "Begrenser listen til dette antallet selv om det er flere treff.",
            GroupName = GroupNames.Content,
            Order = 20)]
        public virtual int NumberOfVisibleItems { get; set; }

        [Display(
            Name = "(valgfri) Rotside for søk",
            Description = "Brukes som rot for søket.",
            GroupName = GroupNames.Content,
            Order = 50)]
        public virtual PageReference PageRoot { get; set; }

        [Display(
            Name = "(valgfri) Lenke til listeside",
            Description = "Hvis fylt ut vises en knapp med lenke til et annet sted, for eksempel lenger liste as samme type innhold.",
            GroupName = GroupNames.Content,
            Order = 55)]
        public virtual EditLinkBlock Button { get; set; }

        public override string ReactComponentName()
            => nameof(ReactModels.RelatedArticles);
    }
}