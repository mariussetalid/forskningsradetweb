using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAnnotations;
using Forskningsradet.Core.Models.ContentModels.Support;
using Forskningsradet.Core.Models.Contracts;

namespace Forskningsradet.Core.Models.ContentModels.Blocks
{
    [ContentType(
        DisplayName = "Vurderingskritere",
        Description = "Brukes på utlysninger for å liste ut kriterier for å søke midler.",
        GroupName = GroupNames.Blocks.Common,
        Order = 360,
        GUID = "1120E8CD-F361-47AE-84FD-1DA6FC14CCD4",
        AvailableInEditMode = false)]
    public class AssessmentCriterionBlock : BaseBlockData, IIdentifiable
    {
        [Display(
            Name = "Id",
            GroupName = GroupNames.Content,
            Order = 10)]
        [Editable(false)]
        [Searchable(false)]
        public virtual string Id { get; set; }

        [Display(
            Name = "Tittel",
            GroupName = GroupNames.Content,
            Order = 20)]
        [CultureSpecific]
        public virtual string Title { get; set; }

        [Display(
            Name = "Tekst",
            GroupName = GroupNames.Content,
            Order = 30)]
        [CultureSpecific]
        [XhtmlStringRestrictContentTypes]
        public virtual XhtmlString CriterionText { get; set; }

        public override string ReactComponentName()
            => nameof(ReactModels.RichTextBlock);
    }
}