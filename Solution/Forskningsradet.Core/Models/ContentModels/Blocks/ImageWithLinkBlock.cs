﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAnnotations;
using EPiServer.Web;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ContentModels.Support;

namespace Forskningsradet.Core.Models.ContentModels.Blocks
{
    [ContentType(
        DisplayName = "Lenkeblokk med bilde",
        Description = "Viser et bilde med tekst på blå bakgrunn. Hele blokka blir en lenke.",
        GroupName = GroupNames.Blocks.Common,
        Order = 140,
        GUID = "FD39055C-95CD-46A0-BF30-82BC0EDC32B3")]
    [ImageUrl(IconConstants.Thumbnails.ImageWithLinkBlock)]
    public class ImageWithLinkBlock : BaseBlockData
    {
        [Display(
            Name = "Lenke",
            Description = "Lenken blokken skal lenke til.",
            GroupName = GroupNames.Content,
            Order = 10)]
        public virtual EditLinkBlock EditLink { get; set; }

        [Display(
            Name = "Bilde",
            Description = "Bildet som skal vises.",
            GroupName = GroupNames.Content,
            Order = 20)]
        [HideOnContentCreate]
        [UIHint(UIHint.Image)]
        public virtual ContentReference Image { get; set; }

        public override string ReactComponentName()
            => nameof(ReactModels.ImageWithLink);
    }
}
