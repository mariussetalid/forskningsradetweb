﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAnnotations;
using Forskningsradet.Core.Models.ContentModels.Support;

namespace Forskningsradet.Core.Models.ContentModels.Blocks
{
    [ContentType(
        GUID = "5A12FCAD-7421-43E1-B2D3-391DC32A1C4F",
        DisplayName = "Nysgjerrigpermetoden - Liste med sitatblokker",
        Description = "Blokk med tittel og en liste med 'Bilde med sitat' blokker.",
        GroupName = GroupNames.Blocks.Uncommon,
        AvailableInEditMode = false)]
    public class NpQuotesBlock : BaseBlockData
    {
        [Display(
            Name = "Tittel",
            Description = "Tittelen som vises over en liste med bilde og tekst/sitat blokker.",
            GroupName = GroupNames.Content,
            Order = 10)]
        [CultureSpecific]
        public virtual string Title { get; set; }

        [Display(
            Name = "Bilde med sitat",
            Description = "Legg til flere blokker med bilde og tekst/sitat i dette området.",
            GroupName = GroupNames.Content,
            Order = 20)]
        [AllowedTypes(typeof(NpImageWithQuoteBlock))]
        public virtual ContentArea Quotes { get; set; }

        public override string ReactComponentName() => nameof(ReactModels.QuotesBlock);
    }
}