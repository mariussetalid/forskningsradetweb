﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAnnotations;
using EPiServer.Web;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ContentModels.Support;

namespace Forskningsradet.Core.Models.ContentModels.Blocks
{
    [ContentType(
        GUID = "D2047B77-17AF-4C70-88D5-ADDCB66D5BD0",
        DisplayName = "Nysgjerrigpermetoden - Bilde med sitat blokk",
        Description = "Blokk med bilde og tilhørende tekst/sitat",
        GroupName = GroupNames.Blocks.Uncommon)]
    [ImageUrl(IconConstants.Thumbnails.QuoteBlock)]
    public class NpImageWithQuoteBlock : BaseBlockData
    {
        [Display(
            Name = "Bilde",
            Description = "Bilde som vises sammen med tekst eller sitat",
            GroupName = GroupNames.Content,
            Order = 10)]
        [UIHint(UIHint.Image)]
        public virtual ContentReference Image { get; set; }

        [Display(
            Name = "Tekst",
            Description = "Selve teksten eller sitatet som skal vises (max 300 tegn)",
            GroupName = GroupNames.Content,
            Order = 20)]
        [CultureSpecific]
        [MaxLength(300)]
        [UIHint(UIHint.Textarea)]
        public virtual string Text { get; set; }

        [Display(
            Name = "Navn",
            Description = "Navn/avsender på sitatet som vises (max 100 tegn)",
            GroupName = GroupNames.Content,
            Order = 30)]
        [CultureSpecific]
        [MaxLength(100)]
        public virtual string QuoteBy { get; set; }

        [Display(
            Name = "Legg til sitattegn",
            Description = "Legger til en bindestrek foran navnet",
            GroupName = GroupNames.Content,
            Order = 40)]
        public virtual bool AddQuoteDash { get; set; }

        public override string ReactComponentName() => nameof(ReactModels.QuotesBlock);
    }
}
