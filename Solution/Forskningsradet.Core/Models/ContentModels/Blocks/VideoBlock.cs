﻿using System.ComponentModel.DataAnnotations;
using EPiServer.DataAnnotations;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ContentModels.Support;

namespace Forskningsradet.Core.Models.ContentModels.Blocks
{
    [ContentType(
        DisplayName = "Video fra Youtube-blokk",
        Description = "Blokk for visning av video fra YouTube.",
        GroupName = GroupNames.Blocks.Embed,
        Order = 300,
        GUID = "1A115CD5-355D-4919-BF2E-5AFE7415E4A3")]
    [ImageUrl(IconConstants.Thumbnails.VideoBlock)]
    public class VideoBlock : BaseBlockData
    {
        [Display(
            Name = "Url eller kodesnutt",
            Description = "Lim inn delingskode eller url fra Youtube",
            Order = 10,
            GroupName = GroupNames.Content)]
        public virtual string UrlOrEmbedCode { get; set; }

        public override string ReactComponentName()
            => nameof(ReactModels.YoutubeVideo);
    }
}
