﻿using System.ComponentModel.DataAnnotations;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAnnotations;
using Forskningsradet.Core.Models.ContentModels.Support;

namespace Forskningsradet.Core.Models.ContentModels.Blocks
{
    [ContentType(
        DisplayName = "Footer",
        GUID = "0C84D71A-7C4C-42B6-B8FF-71ECF02034D0",
        AvailableInEditMode = false)]
    public class FooterBlock : BaseBlockData
    {
        [Display(
            Order = 10,
            GroupName = GroupNames.Content,
            Name = "Kontaktinformasjon",
            Description = "Vises i første kolonne i footeren.")]
        [XhtmlStringRestrictContentTypes]
        [CultureSpecific]
        public virtual XhtmlString ContactInfo { get; set; }

        [Display(
            Order = 20,
            GroupName = GroupNames.Content,
            Name = "Knapp med lenke (Nyhetsbrev)",
            Description = "Hvis fylt ut vises en knapp under første kolonne. Tiltenkt nyhetsbrev.")]
        public virtual EditLinkBlock Button { get; set; }

        [Display(
            Order = 30,
            GroupName = GroupNames.Content,
            Name = "Lenkeliste (Satelitter)",
            Description = "Vises som andre kolonne i footeren. Tiltenkt liste over andre nettsteder underlagt Forskningsrådet.")]
        public virtual LinkListBlock MainLinkList { get; set; }

        [Display(
            Order = 40,
            GroupName = GroupNames.Content,
            Name = "Lenkeliste (Snarveier)",
            Description = "Vises som tredje kolonne i footeren.")]
        public virtual LinkListBlock SecondaryLinkList { get; set; }

        [Display(
            Order = 50,
            GroupName = GroupNames.Content,
            Name = "Lenkeliste (Annet innhold)",
            Description = "Vises som fjerde kolonne i footeren.")]
        public virtual LinkListBlock OtherLinkList { get; set; }

        [Display(
            Order = 60,
            GroupName = GroupNames.Content,
            Name = "Facebook side",
            Description = "Lenke til vår side på Facebook")]
        public virtual Url FacebookLink { get; set; }

        [Display(
            Order = 61,
            GroupName = GroupNames.Content,
            Name = "Alt tekst for Facebooklink")]
        public virtual string FacebookAltText { get; set; }

        [Display(
            Order = 62,
            GroupName = GroupNames.Content,
            Name = "LinkedIn",
            Description = "Lenke til vår side på LinkedIn")]
        public virtual Url LinkedinLink { get; set; }

        [Display(
           Order = 63,
           GroupName = GroupNames.Content,
           Name = "Alt tekst for Linkedinlink")]
        public virtual string LinkedinAltText { get; set; }

        [Display(
            Order = 64,
            GroupName = GroupNames.Content,
            Name = "Twitter",
            Description = "Lenke til vår side på Twitter")]
        public virtual Url TwitterLink { get; set; }

        [Display(
           Order = 65,
           GroupName = GroupNames.Content,
           Name = "Alt tekst for Twitterlink")]
        public virtual string TwitterAltText { get; set; }

        [Display(
            Order = 66,
            GroupName = GroupNames.Content,
            Name = "Rss",
            Description = "Lenke til vår side på Rss")]
        public virtual ContentReference RssLink { get; set; }

        [Display(
           Order = 67,
           GroupName = GroupNames.Content,
           Name = "Alt tekst for Rsslink")]
        public virtual string RssAltText { get; set; }

        public override string ReactComponentName()
            => nameof(ReactModels.Footer);
    }
}