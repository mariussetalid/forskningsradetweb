using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAnnotations;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ContentModels.Support;

namespace Forskningsradet.Core.Models.ContentModels.Blocks
{
    [ContentType(
        DisplayName = "Tekst med blokkmarg",
        Description = "Blokk med tittel, riktekst og mulighet for blokker i en høyremarg.",
        GroupName = GroupNames.Blocks.Uncommon,
        Order = 350,
        GUID = "2E825659-D833-40AE-AC3E-1893ECC983B6")]
    [ImageUrl(IconConstants.Thumbnails.SubjectPriorityBlock)]
    public class SubjectPriorityBlock : BaseBlockData
    {
        [Display(
            Name = "Tittel",
            Description = "Blokkens tittel.",
            GroupName = GroupNames.Content,
            Order = 10)]
        [CultureSpecific]
        public virtual string Title { get; set; }

        [Display(
            Name = "Tekst",
            Description = "Teksten som vises i blokken.",
            GroupName = GroupNames.Content,
            Order = 20)]
        [CultureSpecific]
        [XhtmlStringRestrictContentTypes(AllowInXhtmlString.SomeMedia)]
        public virtual XhtmlString Text { get; set; }

        [Display(
            Order = 30,
            GroupName = GroupNames.Content,
            Name = "Høyre blokkfelt",
            Description = "Blokkfelt på høyre side.")]
        [AllowedTypes(typeof(InformationBlock), typeof(LinkListBlock), typeof(ContactBlock))]
        public virtual ContentArea RightBlockArea { get; set; }

        [Display(
            Order = 40,
            GroupName = GroupNames.Content,
            Name = "Portfolio content",
            Description = "Portefølje-lenkelisteblokker.")]
        [AllowedTypes(typeof(LinkListContainerBlock))]
        public virtual ContentArea PortfolioContent { get; set; }

        public string AnchorId => $"sub{(this as IContent).ContentLink.ID}";

        public override string ReactComponentName() =>
            nameof(ReactModels.AccordionWithContentArea);
    }
}