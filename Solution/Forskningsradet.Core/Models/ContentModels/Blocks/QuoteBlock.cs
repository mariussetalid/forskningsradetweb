﻿using System.ComponentModel.DataAnnotations;
using EPiServer.DataAnnotations;
using EPiServer.Web;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ContentModels.Support;

namespace Forskningsradet.Core.Models.ContentModels.Blocks
{
    [ContentType(
        DisplayName = "Sitatblokk",
        Description = "Viser et uthevet sitat med kilde/opphav. Egner seg best i løpende tekst.",
        GroupName = GroupNames.Blocks.Common,
        Order = 220,
        GUID = "F047890F-937B-47EB-A046-7543B691B2F6")]
    [ImageUrl(IconConstants.Thumbnails.QuoteBlock)]
    public class QuoteBlock : BaseBlockData
    {
        [Display(
            Name = "Navn",
            Description = "Navn/avsender på sitatet som vises.",
            GroupName = GroupNames.Content,
            Order = 10)]
        public virtual string QuoteBy { get; set; }

        [Display(
            Name = "Sitat",
            Description = "Selve sitatet som skal vises.",
            GroupName = GroupNames.Content,
            Order = 20)]
        [CultureSpecific]
        [UIHint(UIHint.Textarea)]
        public virtual string Quote { get; set; }

        public override string ReactComponentName()
            => nameof(ReactModels.Quote);
    }
}
