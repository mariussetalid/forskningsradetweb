﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAnnotations;
using EPiServer.Web;
using Forskningsradet.Core.Models.ContentModels.Support;

namespace Forskningsradet.Core.Models.ContentModels.Blocks
{
    [ContentType(
        DisplayName = "Bildeblokk",
        Description = "Brukes på landingssider, med eller uten tittel. Egner seg til å vise infografikk og lignende.",
        GroupName = GroupNames.Blocks.Common,
        Order = 130,
        GUID = "17ACAC90-91AF-4007-95A8-32C74EC9633C")]
    public class ImageBlock : BaseBlockData
    {
        [Display(
            Name = "Tittel",
            Description = "Brukes som blokkens tittel.",
            GroupName = GroupNames.Content,
            Order = 10)]
        [CultureSpecific]
        public virtual string Title { get; set; }

        [Display(
            Name = "Bilde",
            Description = "Bildet som skal vises i blokken.",
            GroupName = GroupNames.Content,
            Order = 20)]
        [HideOnContentCreate]
        [UIHint(UIHint.Image)]
        public virtual ContentReference Image { get; set; }

        public override string ReactComponentName()
            => nameof(ReactModels.ImageBlock);
    }
}
