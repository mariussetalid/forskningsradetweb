﻿using System.ComponentModel.DataAnnotations;
using EPiServer.DataAnnotations;
using EPiServer.Web;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ContentModels.Support;

namespace Forskningsradet.Core.Models.ContentModels.Blocks
{
    [ContentType(
        DisplayName = "Iframe-blokk",
        Description = "Blokk for visning av innhold fra hvor som helst.",
        GroupName = GroupNames.Blocks.Embed,
        Order = 340,
        GUID = "0540AAAB-274C-4EB3-A3D2-5FDC693F6EB7")]
    [ImageUrl(IconConstants.Thumbnails.IframeBlock)]
    public class IframeBlock : EmbedBaseBlock
    {
        [Required]
        [Display(
            Name = "Link",
            Description = "Lenke til det eksterne innholdet som skal vises.",
            Prompt = EmbedSourceConstants.Iframe.ValidSrcStart + "..",
            Order = 20,
            GroupName = GroupNames.Content)]
        [UIHint(UIHint.Textarea)]
        public override string Src { get; set; }
    }
}
