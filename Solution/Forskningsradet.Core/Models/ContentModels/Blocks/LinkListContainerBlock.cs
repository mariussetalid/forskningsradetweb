﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAnnotations;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ContentModels.Support;

namespace Forskningsradet.Core.Models.ContentModels.Blocks
{
    [ContentType(
        DisplayName = "Lenkelisteblokker - portefølje",
        Description = "Brukes for å vise lenkelisteblokker i 1/1 (to kolonner), 2/3 (2 kolonner), 1/2 (1 kolonne) og 1/3 (1 kolonne). Bruker standard lenkelisteblokker under denne blokken.",
        GroupName = GroupNames.Blocks.Uncommon,
        Order = 150,
        GUID = "11E23C6F-3499-4C87-B484-6CC8BD5DED86")]
    [ImageUrl(IconConstants.Thumbnails.LinkListBlock)]
    public class LinkListContainerBlock : BaseBlockData
    {
        [Display(
            Name = "Tittel",
            Description = "Vises som blokkens tittel.",
            GroupName = GroupNames.Content,
            Order = 10)]
        [CultureSpecific]
        public virtual string Title { get; set; }

        [Display(
            Name = "Lenkelisteblokker",
            Description = "Listen med lenklisteblokker som skal presenteres i blokken.",
            GroupName = GroupNames.Content,
            Order = 20)]
        [AllowedTypes(typeof(LinkListBlock))]
        public virtual ContentArea LinkList { get; set; }

        [Display(
            Name = "Kontaktblokker",
            Description = "Liste med blokk for kontaktinformasjon.",
            GroupName = GroupNames.Content,
            Order = 30)]
        [AllowedTypes(typeof(ContactBlock))]
        public virtual ContentArea ContactList { get; set; }

        [Display(
            Name = "Lenke med tekst blokker",
            Description = "Liste med lenker med tilhørende tekst.",
            GroupName = GroupNames.Content,
            Order = 40)]
        [AllowedTypes(typeof(LinkWithTextBlock))]
        public virtual ContentArea LinkWithTextList { get; set; }

        [Display(
            Name = "Legg lenkene under hverandre på én kolonne",
            Description = "Legg lenkene i lenkeblokkene under hverandre.",
            GroupName = GroupNames.Content,
            Order = 50)]
        public virtual bool SingleColumn { get; set; }

        public override string ReactComponentName() =>
            nameof(ReactModels.PortfolioContent);
    }
}