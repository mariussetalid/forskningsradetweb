﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAnnotations;
using EPiServer.Shell.ObjectEditing;
using Forskningsradet.Core.EditorDescriptors;
using Forskningsradet.Core.Models.ContentModels.Support;
using Forskningsradet.Core.Models.Enums;

namespace Forskningsradet.Core.Models.ContentModels.Blocks
{
    public abstract class MessageBaseBlock : BaseBlockData
    {
        [Display(
            Name = "Meldingstype",
            Description = "Hvor viktig er meldingen? Varslinger bør kun brukes ved meldinger som det er veldig viktig at brukeren får med seg.",
            GroupName = GroupNames.Content,
            Order = 10)]
        [BackingType(typeof(PropertyNumber))]
        [EditorDescriptor(EditorDescriptorType = typeof(EnumEditorDescriptor<MessageTheme>))]
        public virtual MessageTheme Theme { get; set; }

        [Display(
            Name = "Tekst",
            Description = "Teksten som skal vises i blokken.",
            GroupName = GroupNames.Content,
            Order = 20)]
        [CultureSpecific]
        [XhtmlStringRestrictContentTypes]
        public virtual XhtmlString Text { get; set; }

        [Display(
            Name = "Vis kun på forsiden",
            Description = "Hvis valgt vises meldingen kun på forsiden.",
            GroupName = GroupNames.Content,
            Order = 30)]
        public virtual bool FrontPageOnly { get; set; }
    }
}
