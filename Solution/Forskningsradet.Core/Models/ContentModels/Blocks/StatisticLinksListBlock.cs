﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAnnotations;
using EPiServer.Web;
using Forskningsradet.Core.Models.ContentModels.Media;
using Forskningsradet.Core.Models.ContentModels.Support;

namespace Forskningsradet.Core.Models.ContentModels.Blocks
{
    [ContentType(
        DisplayName = "Statistikk-lenkeliste blokk",
        Description = "Lenker med statistikk. Opptil tre elementer.",
        GroupName = GroupNames.Blocks.Uncommon,
        Order = 430,
        GUID = "29DB9C40-6694-4F81-BF4D-BD022ACBB5CD")]
    public class StatisticLinksListBlock : BaseBlockData
    {
        [Display(
            Name = "Overskrift",
            GroupName = GroupNames.Content,
            Order = 10)]
        [CultureSpecific]
        public virtual string Heading { get; set; }

        [Display(
            Order = 20,
            GroupName = GroupNames.Content,
            Name = "Lenker",
            Description = "Lenker med statistikk. Opptil tre elementer.")]
        [MaxItems(3)]
        [AllowedTypes(typeof(StatisticLinkBlock))]
        public virtual ContentArea Links { get; set; }

        [Display(
            Name = "Bilde",
            Description = "Bilde illustrerer statistikken eller annet av interesse.",
            GroupName = GroupNames.Content,
            Order = 30)]
        [UIHint(UIHint.Image)]
        public virtual ContentReference AboutImage { get; set; }

        [Display(
            Name = "Se flere lenke",
            Description = "Hvis fylt ut vises en knapp med lenke.",
            GroupName = GroupNames.Content,
            Order = 40)]
        public virtual EditLinkBlock SeeMoreLink { get; set; }

        public override string ReactComponentName() => nameof(ReactModels.StatsLinkList);
    }
}