﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAnnotations;

namespace Forskningsradet.Core.Models.ContentModels.Blocks
{
    [ContentType(
        DisplayName = "Stavekontroll alternativer",
        GUID = "D8DF0636-745E-47BD-A67F-1B231990CE70",
        AvailableInEditMode = false)]
    public class SpellCheckOptionsBlock : BlockData
    {
        [Display(
            Order = 10,
            Name = "Deaktiver")]
        public virtual bool Disabled { get; set; }

        [Display(
            Order = 20,
            Name = "Vis stavekontrollforslag når det er mindre enn X søkeresultater",
            Prompt = "X",
            Description = "Som standard vises stavekontrollforslag kun nå det er ingen søkeresultater.")]
        public virtual int ShowWhenFoundLessThenX { get; set; }

        public virtual bool IsDisabled(int totalSearchResultsCount)
        {
            return Disabled || (ShowWhenFoundLessThenX == 0 && totalSearchResultsCount > 0)
                            || (ShowWhenFoundLessThenX > 0 && !(totalSearchResultsCount < ShowWhenFoundLessThenX));
        }
    }
}
