﻿using System.ComponentModel.DataAnnotations;
using EPiServer;
using EPiServer.DataAnnotations;
using EPiServer.Web;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ContentModels.Support;

namespace Forskningsradet.Core.Models.ContentModels.Blocks
{
    [ContentType(
        DisplayName = "Stort tall-blokk",
        Description = "Brukes først og fremst som dekorativt element på landingssider.",
        GroupName = GroupNames.Blocks.Common,
        Order = 160,
        GUID = "8D872B79-45E4-48A6-A2F1-71415B4B03F8")]
    [ImageUrl(IconConstants.Thumbnails.NumberBlock)]
    public class NumberBlock : BaseBlockData
    {
        [Display(
            Name = "Tall",
            Description = "Tallet som skal vises.",
            GroupName = GroupNames.Content,
            Order = 10)]
        public virtual string Number { get; set; }

        [Display(
            Name = "Tall som gjelder kun for dette språket",
            Description = "Fyll inn på den engelske versjonen av blokken ved behov.",
            GroupName = GroupNames.Content,
            Order = 15)]
        [CultureSpecific]
        public virtual string CultureSpecificNumber { get; set; }

        [Display(
            Name = "Benevning",
            Description = "Tallets benevning.",
            GroupName = GroupNames.Content,
            Order = 20)]
        [CultureSpecific]
        public virtual string Suffix { get; set; }

        [Display(
            Name = "Tekst",
            Description = "Teksten som skal vises i blokken.",
            GroupName = GroupNames.Content,
            Order = 30)]
        [CultureSpecific]
        [UIHint(UIHint.Textarea)]
        public virtual string Text { get; set; }

        [Display(
            Name = "Lenke (valgfri)",
            Description = "Hvis fylt ut vil blokken lenke til denne urlen.",
            GroupName = GroupNames.Content,
            Order = 40)]
        public virtual Url Link { get; set; }

        public override string ReactComponentName()
            => nameof(ReactModels.NumberBlock);
    }
}
