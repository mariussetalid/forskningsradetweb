﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using EPiServer.Cms.Shell.UI.ObjectEditing.EditorDescriptors;
using EPiServer.Core;
using EPiServer.DataAnnotations;
using EPiServer.Shell.ObjectEditing;
using Forskningsradet.Core.EditorDescriptors;
using Forskningsradet.Core.Models.ContentModels.PropertyList;
using Forskningsradet.Core.Models.ContentModels.Support;
using Forskningsradet.Core.Models.Enums;

namespace Forskningsradet.Core.Models.ContentModels.Blocks
{
    [ContentType(
        DisplayName = "Prosessblokk",
        Description = "Brukes i full bredde. Brukes til å illustrere prosesser med ferdige konstruerte elementer.",
        GroupName = GroupNames.Blocks.Uncommon,
        Order = 200,
        GUID = "8A845451-1D77-43E4-9D6D-F0CCBE280E0F")]
    [IncludeFullWidthPreview]
    public class ProcessBlock : BaseBlockData
    {
        [Display(
            Name = "Tittel",
            Description = "Prosessens tittel.",
            GroupName = GroupNames.Content,
            Order = 10)]
        [CultureSpecific]
        [Searchable(false)]
        public virtual string Title { get; set; }

        [Display(
            Name = "Tekst",
            Description = "Kort intro etter overskriften.",
            GroupName = GroupNames.Content,
            Order = 10)]
        [CultureSpecific]
        [Searchable(false)]
        public virtual string IntroText { get; set; }

        [Display(
            Name = "Stil",
            Description = "Velg prosessblokkens utseende.",
            GroupName = GroupNames.Content,
            Order = 20)]
        [BackingType(typeof(PropertyNumber))]
        [EditorDescriptor(EditorDescriptorType = typeof(EnumEditorDescriptor<ProcessStyle>))]
        public virtual ProcessStyle Style { get; set; }

        [Display(
            Name = "Prosesselementer",
            Description = "Elementene som skal vises.",
            GroupName = GroupNames.Content,
            Order = 30)]
        [EditorDescriptor(EditorDescriptorType = typeof(CollectionEditorDescriptor<ProcessItem>))]
        [CultureSpecific]
        public virtual IList<ProcessItem> ProcessItems { get; set; }

        public override string ReactComponentName() => nameof(ReactModels.Process);
    }
}
