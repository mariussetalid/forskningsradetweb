﻿using System.ComponentModel.DataAnnotations;
using EPiServer.DataAnnotations;
using EPiServer.Web;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ContentModels.Support;

namespace Forskningsradet.Core.Models.ContentModels.Blocks
{
    [ContentType(
        DisplayName = "Tableau-blokk",
        Description = "Blokk for visning av innhold fra Tableau.",
        GroupName = GroupNames.Blocks.Embed,
        Order = 330,
        GUID = "0CCC4618-A95B-48AF-A021-FD0CAA101AA8")]
    [ImageUrl(IconConstants.Thumbnails.TableauBlock)]
    public class TableauBlock : EmbedBaseBlock
    {
        [Required]
        [Display(
            Name = "Link",
            Description = "Lenke til det eksterne innholdet som skal vises. Må starte med https://public.tableau.com/views",
            Prompt = EmbedSourceConstants.Tableau.ValidSrcStart + "..",
            Order = 20,
            GroupName = GroupNames.Content)]
        [UIHint(UIHint.Textarea)]
        public override string Src { get; set; }
    }
}
