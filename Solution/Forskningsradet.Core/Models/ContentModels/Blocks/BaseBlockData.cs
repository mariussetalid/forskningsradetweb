﻿using EPiServer.Core;
using EPiServer.Find.Cms;
using Forskningsradet.Core.Models.ContentModels.Contracts;

namespace Forskningsradet.Core.Models.ContentModels.Blocks
{
    // TODO: NYWEB-236 Find out if there are any blocks that should not be indexed
    [IndexInContentAreas]
    public abstract class BaseBlockData : BlockData, IReactComponentData
    {
        public abstract string ReactComponentName();

        public string ContentName()
            => (this as IContent).Name;

        public int ContentId()
            => (this as IContent).ContentLink.ID;
    }
}