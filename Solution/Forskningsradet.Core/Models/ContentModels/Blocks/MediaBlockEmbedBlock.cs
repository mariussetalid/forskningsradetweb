﻿using System.ComponentModel.DataAnnotations;
using EPiServer.DataAnnotations;
using EPiServer.Web;

namespace Forskningsradet.Core.Models.ContentModels.Blocks
{
    [ContentType(
        AvailableInEditMode = false,
        GUID = "5A840CAA-6694-46D1-8D00-445345DC41F3")]
    public class MediaBlockEmbedBlock : BaseBlockData
    {
        private const string SrcDescription = "Vbrick eks: https://videoportal.rcn.no/embed \n" +
            "Tableu eks: https://public.tableau.com/views \n" +
            "Prosjektbank eks: https://prosjektbanken.forskningsradet.no/ \n" +
            "Instagram eks: https://instagram.com/p/XXXXXXXXXX/embed \n" +
            "Facebook eks: https://facebook.com/video/embed?video_id=XXXXXXXXXXXXXXX";

        [Display(
            Name = "Legg til Url(lenke)",
            Description = SrcDescription,
            Prompt = SrcDescription,
            Order = 10)]
        [UIHint(UIHint.Textarea)]
        public virtual string Src { get; set; }

        [Display(
             Name = "Alternativ tekst",
             Description = "Beskrivelse av innholdet. Nødvendig for å oppfylle UU krav",
             Order = 20,
             Prompt = "Beskrivelse av innholdet i iFrame")]
        [CultureSpecific]
        [UIHint(UIHint.Textarea)]
        public virtual string AlternativeText { get; set; }

        [Display(
            Name = "Bredde",
            Description = "Bredden blokken skal vises i, i piksler",
            Order = 30)]
        public virtual int Width { get; set; }

        [Display(
            Name = "Høyde",
            Description = "Høyden blokken skal vises i, i piksler",
            Order = 40)]
        public virtual int Height { get; set; }

        public override string ReactComponentName() => null;
    }
}
