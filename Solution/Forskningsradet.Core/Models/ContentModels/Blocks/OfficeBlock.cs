﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using EPiServer.Cms.Shell.UI.ObjectEditing.EditorDescriptors;
using EPiServer.DataAnnotations;
using EPiServer.Shell.ObjectEditing;
using Forskningsradet.Core.Models.ContentModels.PropertyList;
using Forskningsradet.Core.Models.ContentModels.Support;

namespace Forskningsradet.Core.Models.ContentModels.Blocks
{
    [ContentType(
        DisplayName = "Kontorblokk",
        Description = "Tittel og kontorinformasjon.",
        GroupName = GroupNames.Blocks.Uncommon,
        Order = 420,
        GUID = "8D1EB10B-8F05-46A2-9746-9CD96579C906")]
    public class OfficeBlock : BaseBlockData
    {
        [Display(
            Name = "Tittel",
            Description = "Blokkens tittel.",
            GroupName = GroupNames.Content,
            Order = 10)]
        [CultureSpecific]
        public virtual string Title { get; set; }

        [Display(
            Name = "Informasjon",
            GroupName = GroupNames.Content,
            Description = "Tittel, tekst, og lenke",
            Order = 20)]
        [EditorDescriptor(EditorDescriptorType = typeof(CollectionEditorDescriptor<OfficeInformation>))]
        [CultureSpecific]
        public virtual IList<OfficeInformation> Information { get; set; }

        public override string ReactComponentName()
            => nameof(ReactModels.OfficeBlock);
    }
}
