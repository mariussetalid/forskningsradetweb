﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using EPiServer.Cms.Shell.UI.ObjectEditing.EditorDescriptors;
using EPiServer.Core;
using EPiServer.DataAnnotations;
using EPiServer.Shell.ObjectEditing;
using Forskningsradet.Core.Models.ContentModels.PropertyList;
using Forskningsradet.Core.Models.ContentModels.Support;

namespace Forskningsradet.Core.Models.ContentModels.Blocks
{
    [ContentType(
        DisplayName = "Kontorlisteblokk",
        Description = "Blokk med én eller flere kontorer, og lenker til de andre 'Kontakt Oss'-sidene.",
        GroupName = GroupNames.Blocks.Uncommon,
        Order = 410,
        GUID = "332D8105-3F10-4B5E-AFBB-63A70ED924FE")]
    public class OfficeListBlock : BaseBlockData
    {
        [Display(
            Name = "Kontaktsidelenker",
            GroupName = GroupNames.Content,
            Description = "Tittel, tekst, lenke og sorteringstall",
            Order = 20)]
        [EditorDescriptor(EditorDescriptorType = typeof(CollectionEditorDescriptor<SortableLink>))]
        [CultureSpecific]
        public virtual IList<SortableLink> Links { get; set; }

        [Display(
            Name = "Kontorer",
            Description = "Liste med kontorer som skal vises i blokken.",
            GroupName = GroupNames.Content,
            Order = 20)]
        [AllowedTypes(typeof(OfficeBlock))]
        public virtual ContentArea Offices { get; set; }

        public override string ReactComponentName()
            => nameof(ReactModels.OfficeListBlock);
    }
}
