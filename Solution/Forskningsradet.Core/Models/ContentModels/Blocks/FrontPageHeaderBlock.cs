using System.ComponentModel.DataAnnotations;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAnnotations;
using EPiServer.Shell.ObjectEditing;
using EPiServer.Web;
using Forskningsradet.Core.EditorDescriptors;
using Forskningsradet.Core.Models.ContentModels.Support;

namespace Forskningsradet.Core.Models.ContentModels.Blocks
{
    [ContentType(
        DisplayName = "Toppfelt på forsiden",
        Description = "Toppfeltblokk. Brukes kun på forsiden.",
        GroupName = GroupNames.Blocks.Uncommon,
        Order = 700,
        GUID = "AF48CCE4-76EA-4BE4-AEA6-D8AFEEA78161")]
    public class FrontPageHeaderBlock : BaseBlockData
    {
        [Display(
            Name = "Bilde",
            Description = "Vises som stort bilde på forsiden.",
            GroupName = GroupNames.Content,
            Order = 10)]
        [HideOnContentCreate]
        [UIHint(UIHint.Image)]
        public virtual ContentReference Image { get; set; }

        [Display(
            Name = "Tittel",
            Description = "Vises som tittel over knappene.",
            GroupName = GroupNames.Content,
            Order = 20)]
        [Required]
        [CultureSpecific]
        public virtual string Title { get; set; }

        [Display(
            Name = "Tekst",
            Description = "Viser standardtekst dersom den ikke fylles ut.",
            GroupName = GroupNames.Content,
            Order = 30)]
        [CultureSpecific]
        [UIHint(UIHint.Textarea)]
        public virtual string Text { get; set; }

        [Display(
            Name = "Fargetema",
            Description = "Lys eller mørk. Velg den som kommer tydeligst fram basert på bildevalget.",
            GroupName = GroupNames.Content,
            Order = 40)]
        [BackingType(typeof(PropertyNumber))]
        [EditorDescriptor(EditorDescriptorType = typeof(EnumEditorDescriptor<FrontPageHeaderBlockTheme>))]
        public virtual FrontPageHeaderBlockTheme Theme { get; set; }

        [Display(
            Order = 50,
            GroupName = GroupNames.Content,
            Name = "Primærknapp lenke",
            Description = "Primærknappen vil lenke til denne urlen.")]
        [Required]
        public virtual Url MainButtonUrl { get; set; }

        [Display(
            Order = 60,
            GroupName = GroupNames.Content,
            Name = "Primærknapp tekst",
            Description = "Tekst for primærknappen.")]
        [Required]
        [CultureSpecific]
        public virtual string MainButtonText { get; set; }

        [Display(
            Order = 70,
            GroupName = GroupNames.Content,
            Name = "Sekundærknapp lenke (valgfri)",
            Description = "Hvis fylt ut vil sekundærknappen lenke til denne urlen.")]
        public virtual Url SecondaryButtonUrl { get; set; }

        [Display(
            Order = 80,
            GroupName = GroupNames.Content,
            Name = "Sekundærknapp tekst",
            Description = "Tekst for sekundærknappen.")]
        [CultureSpecific]
        public virtual string SecondaryButtonText { get; set; }

        public override string ReactComponentName() =>
            nameof(ReactModels.FrontpageHeader);
    }

    public enum FrontPageHeaderBlockTheme
    {
        DarkBlue = 1,
        GreyBlue = 2,
        Purple = 3
    }
}