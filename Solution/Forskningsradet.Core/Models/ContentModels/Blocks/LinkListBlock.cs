﻿using System.ComponentModel.DataAnnotations;
using EPiServer.DataAnnotations;
using EPiServer.SpecializedProperties;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ContentModels.Support;

namespace Forskningsradet.Core.Models.ContentModels.Blocks
{
    [ContentType(
        DisplayName = "Lenkelisteblokk",
        Description = "Brukes i 1/2 eller 1/3 på landingssider. Aldri full bredde, og aldri i løpende tekst. Kan også brukes i høyremarger.",
        GroupName = GroupNames.Blocks.Common,
        Order = 150,
        GUID = "6D3D7C23-97CF-4A9D-A9CD-6A62A93C3097")]
    [ImageUrl(IconConstants.Thumbnails.LinkListBlock)]
    public class LinkListBlock : BaseBlockData
    {
        [Display(
            Name = "Tittel (maks 50 tegn)",
            Description = "Vises som blokkens tittel.",
            GroupName = GroupNames.Content,
            Order = 10)]
        [CultureSpecific]
        public virtual string Title { get; set; }

        [Display(
            Name = "Lenker",
            Description = "Listen med lenker som skal presenteres i blokken.",
            GroupName = GroupNames.Content,
            Order = 20)]
        [CultureSpecific]
        public virtual LinkItemCollection LinkItems { get; set; }

        public override string ReactComponentName()
            => nameof(ReactModels.InfoBlock);
    }
}