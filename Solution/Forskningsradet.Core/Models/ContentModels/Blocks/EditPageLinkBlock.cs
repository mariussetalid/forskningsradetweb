﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAnnotations;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.ContentModels.Support;

namespace Forskningsradet.Core.Models.ContentModels.Blocks
{
    [ContentType(
        DisplayName = "Rediger intern lenke til side",
        Description = "Blokk for å redigere knapp/lenke",
        GUID = "0DCA302B-ED04-4D78-A74D-4F5BBC23926D",
        AvailableInEditMode = false)]
    public class EditPageLinkBlock : BlockData
    {
        [Display(
            Name = "Tittel",
            Description = "Teksten som skal vises på knappen.",
            GroupName = GroupNames.Content,
            Order = 10)]
        [CultureSpecific]
        public virtual string Text { get; set; }

        [Display(
            Name = "Lenke",
            Description = "Lenken som knappen skal gå til.",
            GroupName = GroupNames.Content,
            Order = 20)]
        [AllowedTypes(typeof(EditorialPage))]
        public virtual PageReference Link { get; set; }
    }
}