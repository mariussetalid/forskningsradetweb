﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAnnotations;
using EPiServer.Shell.ObjectEditing;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ContentModels.Contracts;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.ContentModels.Support;

namespace Forskningsradet.Core.Models.ContentModels.Blocks
{
    [ContentType(
        DisplayName = "Relatert innhold",
        Description = "Viser en liste av relaterte innholdsider basert på kategorier.",
        GroupName = GroupNames.Blocks.RelatedContent,
        Order = 500,
        GUID = "C9A6150D-4314-40C4-81F6-D899201504A5")]
    [ImageUrl(IconConstants.Thumbnails.RelatedContentListBlock)]
    public class RelatedContentListBlock : RelatedListBaseBlock, ICanBeRenderedInSidebar
    {
        public override PageReference PageRoot { get; set; }

        [Display(
            Name = "Sidetyper som skal vises",
            Description = "Angi hvilke sidetyper sider som vises kan være av.",
            GroupName = GroupNames.Content,
            Order = 40)]
        [CustomPageType(CustomPageTypeSet.Content)]
        [SelectMany(SelectionFactoryType = typeof(Factories.CustomPageTypeSelectionFactory))]
        public virtual string PageTypes { get; set; }

        [Display(
            Name = "Porteføljevisning",
            Description = "Skrur på porteføljevisning for blokken.",
            GroupName = GroupNames.Content,
            Order = 50)]
        public virtual bool PortfolioMode { get; set; }

        [Display(
            Name = "Publikasjoner - kun med porteføljevisning",
            Description = "Publikasjonssider legges manuelt inn istedenfor å hentes automatisk basert på kategorier.",
            GroupName = GroupNames.Content,
            Order = 60)]
        [AllowedTypes(typeof(PublicationBasePage))]
        public virtual ContentArea Publications { get; set; }

        public override string ReactComponentName() => nameof(ReactModels.RelatedArticles);

        public string PartialReactComponentName() => ReactComponentName();
    }
}