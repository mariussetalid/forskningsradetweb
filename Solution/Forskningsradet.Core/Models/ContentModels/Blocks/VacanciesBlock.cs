﻿using System.ComponentModel.DataAnnotations;
using EPiServer.DataAnnotations;
using Forskningsradet.Core.Models.ContentModels.Support;

namespace Forskningsradet.Core.Models.ContentModels.Blocks
{
    [ContentType(
        DisplayName = "Ledige stillinger-blokk",
        Description = "Viser en liste med ledige stillinger hentet fra HR manager.",
        GroupName = GroupNames.Blocks.Uncommon,
        Order = 460,
        GUID = "3C4609FB-3F39-4483-8102-86036606009E")]
    public class VacanciesBlock : BaseBlockData
    {
        [Display(
            Name = "Tittel",
            Description = "Blokkens tittel.",
            GroupName = GroupNames.Content,
            Order = 10)]
        [CultureSpecific]
        public virtual string Title { get; set; }

        [Display(
            Name = "Tekst",
            Description = "Teksten som vises i blokken.",
            GroupName = GroupNames.Content,
            Order = 20)]
        [CultureSpecific]
        public virtual string Text { get; set; }

        [Display(
            Name = "Knapp",
            Description = "Dersom fylt ut vises en knapp med lenke. Denne er ment å lenke tilbake til listen med ledige stillinger.",
            GroupName = GroupNames.Content,
            Order = 30)]
        public virtual EditLinkBlock ButtonBlock { get; set; }

        public override string ReactComponentName()
            => nameof(ReactModels.Vacancies);
    }
}
