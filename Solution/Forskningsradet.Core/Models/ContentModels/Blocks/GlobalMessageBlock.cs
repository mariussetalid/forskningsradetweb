﻿using System.ComponentModel.DataAnnotations;
using EPiServer.DataAnnotations;
using Forskningsradet.Core.Models.ContentModels.Support;

namespace Forskningsradet.Core.Models.ContentModels.Blocks
{
    [ContentType(
        DisplayName = "Global driftsmelding",
        Description = "Kun for Admin. Brukes til å sette meldinger som vises på alle sider.",
        GroupName = GroupNames.Blocks.Uncommon,
        Order = 480,
        GUID = "6DDA1CCC-616D-412E-94D0-BE3DF0566CDA")]
    public class GlobalMessageBlock : MessageBaseBlock
    {
        [Display(
            Name = "Felles global driftsmelding",
            Description = "Meldingen skal vises på tvers av flere sider.",
            GroupName = GroupNames.Content,
            Order = 40)]
        [PropertyAdminRestriction]
        public virtual bool IsSharedMessage { get; set; }

        public override string ReactComponentName() =>
            nameof(ReactModels.Message);
    }
}
