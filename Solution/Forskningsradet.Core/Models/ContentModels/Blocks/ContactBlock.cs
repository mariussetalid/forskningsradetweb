﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAnnotations;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.ContentModels.Support;

namespace Forskningsradet.Core.Models.ContentModels.Blocks
{
    [ContentType(
        DisplayName = "Kontaktblokk",
        Description = "Blokk for kontaktinformasjon.",
        GroupName = GroupNames.Blocks.Common,
        Order = 250,
        GUID = "D2958447-5ECF-473E-9C21-63FC7F1CD778")]
    [ImageUrl(IconConstants.Thumbnails.ContactBlock)]
    public class ContactBlock : BaseBlockData
    {
        [Display(
            Name = "Tittel",
            Description = "Vises som blokkens tittel.",
            GroupName = GroupNames.Content,
            Order = 10)]
        [CultureSpecific]
        public virtual string Title { get; set; }

        [Display(
            Name = "Vis telefonnummer",
            Description = "Hvorvidt det skal vises telefonnummer for kontaktene i denne listen eller ikke.",
            GroupName = GroupNames.Content,
            Order = 20)]
        public virtual bool ShowPhoneNumber { get; set; }

        [Display(
            Name = "Vis stillingstittel",
            Description = "Hvorvidt det skal vises stillingstittel for kontaktene i denne listen eller ikke.",
            GroupName = GroupNames.Content,
            Order = 30)]
        public virtual bool ShowJobTitle { get; set; }

        [Display(
            Name = "Kontaktpersoner",
            Description = "Legg til en liste av de kontaktene som skal vises.",
            GroupName = GroupNames.Content,
            Order = 40)]
        [AllowedTypes(typeof(PersonPage), typeof(NonPersonContactBlock))]
        public virtual ContentArea Contacts { get; set; }

        public override string ReactComponentName() =>
            nameof(ReactModels.ContactBlock);
    }
}
