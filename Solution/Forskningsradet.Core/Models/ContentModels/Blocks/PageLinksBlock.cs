﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAnnotations;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.ContentModels.Support;

namespace Forskningsradet.Core.Models.ContentModels.Blocks
{
    [ContentType(
        DisplayName = "Temainnganger",
        Description = "(Forsiden) Viser en liste med sider.",
        GroupName = GroupNames.Blocks.Uncommon,
        Order = 400,
        GUID = "6EBE55C6-FB34-4627-B140-0651C0B7FDD6")]
    [ImageUrl(IconConstants.Thumbnails.PageLinksBlock)]
    public class PageLinksBlock : BaseBlockData
    {
        [Display(
            Name = "Tittel",
            Description = "Blokkens tittel.",
            GroupName = GroupNames.Content,
            Order = 10)]
        [CultureSpecific]
        public virtual string Title { get; set; }

        [Display(
            Name = "Sider det skal lenkes til",
            Description = "Liste over sider som skal vises i blokken. Tidligere kalt: 'Sider som vises 3 i bredden'.",
            GroupName = GroupNames.Content,
            Order = 30)]
        [AllowedTypes(typeof(EditorialPage))]
        public virtual ContentArea FirstPageGroup { get; set; }

        [Display(
            Name = "(Ikke lenger i bruk) - 'Beskrivelse'",
            Description = "Tekst som vises i blokken.",
            GroupName = GroupNames.Content,
            Order = 40)]
        [CultureSpecific]
        public virtual string Text { get; set; }

        [Display(
            Name = "(Ikke lenger i bruk) - 'Sider som vises 4 i bredden'",
            Description = "Liste over sider som skal vises i blokken.",
            GroupName = GroupNames.Content,
            Order = 50)]
        [AllowedTypes(typeof(EditorialPage))]
        public virtual ContentArea SecondPageGroup { get; set; }

        public override string ReactComponentName() => nameof(ReactModels.PageLinksBlock);
    }
}