﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAnnotations;
using EPiServer.Shell.ObjectEditing;
using EPiServer.Web;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.EditorDescriptors;
using Forskningsradet.Core.Models.ContentModels.Support;

namespace Forskningsradet.Core.Models.ContentModels.Blocks
{
    [ContentType(
        DisplayName = "Bilde med bildetekst",
        Description = "Brukes i løpende tekst på nyhets- og artikkelsider.",
        GroupName = GroupNames.Blocks.Common,
        Order = 200,
        GUID = "804EE162-726C-4D9B-891C-BE8FF4813E13")]
    [ImageUrl(IconConstants.Thumbnails.ImageWithCaptionBlock)]
    public class ImageWithCaptionBlock : BaseBlockData
    {
        [Display(
            Name = "Tittel",
            Description = "Tittel som vises over bildet.",
            GroupName = GroupNames.Content,
            Order = 1)]
        [CultureSpecific]
        public virtual string Title { get; set; }

        [Display(
            Name = "Bildetekst",
            Description = "Bildetekst som vises under bildet.",
            GroupName = GroupNames.Content,
            Order = 10)]
        [CultureSpecific]
        public virtual string Caption { get; set; }

        [Display(
            Name = "Bilde",
            Description = "Bildet som skal vises.",
            GroupName = GroupNames.Content,
            Order = 20)]
        [HideOnContentCreate]
        [UIHint(UIHint.Image)]
        public virtual ContentReference Image { get; set; }

        [Display(
            Name = "Stil",
            Description = "Angi hvordan blokken skal vises.",
            GroupName = GroupNames.Content,
            Order = 30)]
        [BackingType(typeof(PropertyNumber))]
        [EditorDescriptor(EditorDescriptorType = typeof(EnumEditorDescriptor<ImageBlockStyle>))]
        public virtual ImageBlockStyle Style { get; set; }

        public override string ReactComponentName()
            => nameof(ReactModels.ImageWithCaption);
    }

    public enum ImageBlockStyle
    {
        None = 0,
        ThemeHalfWidth = 1
    }
}
