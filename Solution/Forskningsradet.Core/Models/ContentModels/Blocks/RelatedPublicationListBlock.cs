﻿using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ContentModels.Contracts;
using Forskningsradet.Core.Models.ContentModels.Support;

namespace Forskningsradet.Core.Models.ContentModels.Blocks
{
    [ContentType(
        DisplayName = "Relaterte publikasjoner",
        Description = "Viser en liste av relaterte publikasjoner basert på kategorier.",
        GroupName = GroupNames.Blocks.RelatedContent,
        Order = 540,
        GUID = "6F5AFF4B-3A48-4CA9-B4D7-D9841A777FE6")]
    [ImageUrl(IconConstants.Thumbnails.RelatedPublicationListBlock)]
    public class RelatedPublicationListBlock : RelatedListBaseBlock, ICanBeRenderedInSidebar
    {
        public virtual int Year { get; set; }

        public override string ReactComponentName() => nameof(ReactModels.RelatedPublications);

        public string PartialReactComponentName() => ReactComponentName();
    }
}
