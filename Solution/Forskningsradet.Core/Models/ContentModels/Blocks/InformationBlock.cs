﻿using System.ComponentModel.DataAnnotations;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAnnotations;
using EPiServer.Shell.ObjectEditing;
using EPiServer.Web;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.EditorDescriptors;
using Forskningsradet.Core.Models.ContentModels.Support;

namespace Forskningsradet.Core.Models.ContentModels.Blocks
{
    [ContentType(
        DisplayName = "Informasjonsblokk",
        Description = "Designet for korte infotekster, lenk videre til andre innholdssider ved behov. Kan også brukes i høyremarger. Brukes i 1/2 eller 1/3 på landingssider. Aldri full bredde, og aldri i løpende tekst.",
        GroupName = GroupNames.Blocks.Common,
        Order = 110,
        GUID = "8D22C0F5-B93A-4FDD-AC0B-98096D5434E0")]
    [ImageUrl(IconConstants.Thumbnails.InformationBlock)]
    public class InformationBlock : BaseBlockData
    {
        [Display(
            Name = "Tittel",
            Description = "Brukes som blokkens tittel.",
            GroupName = GroupNames.Content,
            Order = 10)]
        [CultureSpecific]
        public virtual string Title { get; set; }

        [Display(
            Name = "Lenke",
            Description = "Lenke for tittelen på blokken",
            GroupName = GroupNames.Content,
            Order = 15)]
        public virtual Url Url { get; set; }

        [Display(
            Name = "Brødtekst",
            Description = "Brødteksten i blokken.",
            GroupName = GroupNames.Content,
            Order = 20)]
        [XhtmlStringRestrictContentTypes]
        [CultureSpecific]
        public virtual XhtmlString MainBody { get; set; }

        [Display(
            Name = "Knapp med lenke",
            Description = "Hvis fylt ut vises en knapp med lenke.",
            GroupName = GroupNames.Content,
            Order = 30)]
        public virtual EditLinkBlock Button { get; set; }

        [Display(
            Name = "Ikon",
            Description = "Valgfritt ikon som vises i hjørnet av blokken.",
            GroupName = GroupNames.Content,
            Order = 40)]
        [HideOnContentCreate]
        [UIHint(UIHint.Image)]
        public virtual ContentReference Icon { get; set; }

        [Display(
            Name = "Stil",
            Description = "Velg hvilket utseende blokken skal ha.",
            GroupName = GroupNames.Content,
            Order = 50)]
        [BackingType(typeof(PropertyNumber))]
        [EditorDescriptor(EditorDescriptorType = typeof(EnumEditorDescriptor<InformationBlockStyle>))]
        public virtual InformationBlockStyle Style { get; set; }

        public override string ReactComponentName()
            => nameof(ReactModels.InfoBlock);
    }

    public enum InformationBlockStyle
    {
        ThemeBlue = 1,
        ThemeOrange = 2,
        ThemeDarkBlue = 3,
    }
}

