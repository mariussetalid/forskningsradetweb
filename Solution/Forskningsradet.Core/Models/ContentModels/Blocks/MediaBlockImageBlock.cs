﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAnnotations;
using EPiServer.Web;
using Forskningsradet.Core.Models.ContentModels.Support;

namespace Forskningsradet.Core.Models.ContentModels.Blocks
{
    [ContentType(
        AvailableInEditMode = false,
        GUID = "6C3D57F5-5410-4578-97DE-3CB05A2ADBA5")]
    public class MediaBlockImageBlock : BaseBlockData
    {
        [Display(
            Name = "Bilde",
            Description = "Bilde vises kun når blokken brukes på landingsside")]
        [HideOnContentCreate]
        [UIHint(UIHint.Image)]
        public virtual ContentReference Image { get; set; }

        public override string ReactComponentName() => null;
    }
}
