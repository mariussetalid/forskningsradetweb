﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAnnotations;
using Forskningsradet.Core.Models.ContentModels.Support;

namespace Forskningsradet.Core.Models.ContentModels.Blocks
{
    [ContentType(
        DisplayName = "Kategoriliste",
        Description = "Blokk for å liste opp kategorier",
        GUID = "A7E97EAD-9179-41D1-8ED1-E54675419100",
        AvailableInEditMode = false)]
    public class CategoryListBlock : BlockData
    {
        [Display(
            Name = "Velg kategoriene som skal vises på siden",
            Description = "Kategorier som skal vises i listen",
            GroupName = GroupNames.Content,
            Order = 10)]
        public virtual CategoryList CategoryList { get; set; }
    }
}