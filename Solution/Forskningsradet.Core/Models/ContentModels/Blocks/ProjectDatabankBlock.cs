﻿using System.ComponentModel.DataAnnotations;
using EPiServer.DataAnnotations;
using EPiServer.Web;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ContentModels.Support;

namespace Forskningsradet.Core.Models.ContentModels.Blocks
{
    [ContentType(
        DisplayName = "Prosjektbanken-blokk",
        Description = "Blokk for visning av innhold fra Prosjektbanken.",
        GroupName = GroupNames.Blocks.Embed,
        Order = 320,
        GUID = "FD6B77EE-8294-46FB-9EF1-7DEAB59B878D")]
    [ImageUrl(IconConstants.Thumbnails.ProjectDatabankBlock)]
    public class ProjectDatabankBlock : EmbedBaseBlock
    {
        [Required]
        [Display(
            Name = "Src",
            Description = "Lenke til innholdet i Prosjektbanken som skal vises. Må starte med https://prosjektbanken.forskningsradet.no/",
            Prompt = EmbedSourceConstants.ProjectDatabank.ValidSrcStart + "..",
            Order = 20,
            GroupName = GroupNames.Content)]
        [UIHint(UIHint.Textarea)]
        public override string Src { get; set; }
    }
}
