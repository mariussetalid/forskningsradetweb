﻿using System.ComponentModel.DataAnnotations;
using EPiServer;
using EPiServer.DataAnnotations;
using EPiServer.Web;
using Forskningsradet.Core.Models.ContentModels.Support;

namespace Forskningsradet.Core.Models.ContentModels.Blocks
{
    [ContentType(
        DisplayName = "Lenke med tekst blokk",
        Description = "Brukes i portfølje content",
        GroupName = GroupNames.Blocks.Common,
        Order = 210,
        GUID = "2FDB7113-0316-4ADF-A68A-5E6B72BA083A")]
    public class LinkWithTextBlock : BaseBlockData
    {
        [Display(
            Name = "Lenketittel",
            Description = "Vises som tittelen på lenken.",
            GroupName = GroupNames.Content,
            Order = 10)]
        [CultureSpecific]
        public virtual string LinkTitle { get; set; }

        [Display(
            Name = "Lenke",
            Description = "Url for lenken.",
            GroupName = GroupNames.Content,
            Order = 20)]
        public virtual Url Link { get; set; }

        [Display(
            Name = "Tekst",
            Description = "Vil vises som tekst under lenkene.",
            GroupName = GroupNames.Content,
            Order = 30)]
        [CultureSpecific]
        [UIHint(UIHint.Textarea)]
        public virtual string Text { get; set; }

        public override string ReactComponentName() =>
            nameof(ReactModels.LinkWithText);
    }
}
