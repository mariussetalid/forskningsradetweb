﻿using System.ComponentModel.DataAnnotations;
using EPiServer.DataAnnotations;
using EPiServer.Web;
using Forskningsradet.Core.Models.ContentModels.Support;

namespace Forskningsradet.Core.Models.ContentModels.Blocks
{
    public abstract class EmbedBaseBlock : BaseBlockData
    {
        [Required]
        [Display(
            Name = "Alternativ tekst",
            Description = "Beskrivelse av innholdet. Nødvendig for å oppfylle UU krav",
            Order = 10,
            Prompt = "Beskrivelse av innholdet",
            GroupName = GroupNames.Content)]
        [CultureSpecific]
        public virtual string AlternativeText { get; set; }

        [Required]
        [Display(
            Name = "Src",
            Description = "Adressen det eksterne innholdet skal hentes fra.",
            Order = 20,
            GroupName = GroupNames.Content)]
        [UIHint(UIHint.Textarea)]
        public virtual string Src { get; set; }

        [Display(
            Name = "Bredde",
            Description = "Bredden blokken skal vises i, i piksler",
            Order = 30,
            GroupName = GroupNames.Content)]
        public virtual int Width { get; set; }

        [Display(
            Name = "Høyde",
            Description = "Høyden blokken skal vises i, i piksler",
            Order = 40,
            GroupName = GroupNames.Content)]
        public virtual int Height { get; set; }

        public override string ReactComponentName()
            => nameof(ReactModels.EmbedBlock);
    }
}