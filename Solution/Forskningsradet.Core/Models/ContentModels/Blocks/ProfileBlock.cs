﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAnnotations;
using EPiServer.Web;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ContentModels.Support;

namespace Forskningsradet.Core.Models.ContentModels.Blocks
{
    [ContentType(
        DisplayName = "Profilblokk",
        Description = "(Arrangement) Brukes i ContentArea for foredragsholdere.",
        GroupName = GroupNames.Blocks.Uncommon,
        Order = 450,
        GUID = "1727EB6C-4B91-46FE-988B-8B3489A21CA3")]
    [ImageUrl(IconConstants.Thumbnails.ProfileBlock)]
    public class ProfileBlock : BaseBlockData
    {
        [Display(
            Name = "Bilde",
            Description = "Valgfritt bilde av foredragsholder.",
            GroupName = GroupNames.Content,
            Order = 10)]
        [HideOnContentCreate]
        [UIHint(UIHint.Image)]
        public virtual ContentReference Image { get; set; }

        [Display(
            Name = "Navn",
            Description = "Navn på foredragsholder",
            GroupName = GroupNames.Content,
            Order = 20)]
        public virtual string ProfileName { get; set; }

        [Display(
            Name = "Navn som gjelder kun for dette språket",
            Description = "Fyll inn på den engelske versjonen av blokken ved behov.",
            GroupName = GroupNames.Content,
            Order = 25)]
        [CultureSpecific]
        public virtual string CultureSpecificProfileName { get; set; }

        [Display(
            Name = "Stillingstittel",
            Description = "Foredragsholderens stillingstittel.",
            GroupName = GroupNames.Content,
            Order = 40)]
        [CultureSpecific]
        public virtual string JobTitle { get; set; }

        [Display(
            Name = "Organisasjon",
            Description = "Organisasjonen foredragsholderen kommer ifra.",
            GroupName = GroupNames.Content,
            Order = 50)]
        public virtual string Company { get; set; }

        [Display(
            Name = "Organisasjon som gjelder kun for dette språket",
            Description = "Fyll inn på den engelske versjonen av blokken ved behov.",
            GroupName = GroupNames.Content,
            Order = 55)]
        [CultureSpecific]
        public virtual string CultureSpecificCompany { get; set; }

        [Display(
            Name = "Ingress",
            Description = "Ingressen som vises i blokken.",
            GroupName = GroupNames.Content,
            Order = 60)]
        [CultureSpecific]
        public virtual string MainIntro { get; set; }

        public override string ReactComponentName()
            => nameof(ReactModels.PersonBlock);
    }
}

