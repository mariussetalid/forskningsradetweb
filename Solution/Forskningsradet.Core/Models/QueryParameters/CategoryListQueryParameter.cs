namespace Forskningsradet.Core.Models.QueryParameters
{
    public class CategoryListQueryParameter : QueryParameterBase
    {
        public int[] Categories { get; set; }
        public int Page { get; set; }
    }
}