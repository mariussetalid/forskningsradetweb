﻿namespace Forskningsradet.Core.Models.QueryParameters
{
    public class GlossaryQueryParameter : QueryParameterBase
    {
        public string Query { get; set; }
    }
}