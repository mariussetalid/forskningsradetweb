using System.Collections.Generic;
using Forskningsradet.Core.Models.Validation;

namespace Forskningsradet.Core.Models.QueryParameters
{
    public class NewsletterRegistrationQueryParameter : QueryParameterBase
    {
        public List<NewsletterModelValidation> ModelValidation { get; set; }
    }
}