
namespace Forskningsradet.Core.Models.QueryParameters
{
    public class LargeDocumentQueryParameter : QueryParameterBase
    {
        public bool Download { get; set; }
    }
}