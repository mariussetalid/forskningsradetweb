﻿namespace Forskningsradet.Core.Models.QueryParameters
{
    public enum RenderMediaBlockOption
    {
        None = 0,
        MediaWithCaption = 1,
        MediaBlock = 2
    }
}
