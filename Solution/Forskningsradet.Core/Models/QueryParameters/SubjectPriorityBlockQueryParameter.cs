﻿using EPiServer.Core;

namespace Forskningsradet.Core.Models.QueryParameters
{
    public class SubjectPriorityBlockQueryParameter : QueryParameterBase
    {
        public PageReference PageReference { get; set; }
    }
}