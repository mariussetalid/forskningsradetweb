using Forskningsradet.Core.Models.SearchModels;

namespace Forskningsradet.Core.Models.QueryParameters
{
    public class EventListQueryParameter : QueryParameterBase
    {
        public TimeFrameFilter TimeFrame { get; set; }
        public int[] Subjects { get; set; }
        public int[] TargetGroups { get; set; }
        public int[] Categories { get; set; }
        public string[] Type { get; set; }
        public string[] Location { get; set; }
        public bool VideoOnly { get; set; }
    }
}