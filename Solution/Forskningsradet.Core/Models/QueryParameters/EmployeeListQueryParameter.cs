namespace Forskningsradet.Core.Models.QueryParameters
{
    public class EmployeeListQueryParameter : QueryParameterBase
    {
        public string Query { get; set; }
        public char? Letter { get; set; }
        public int? Page { get; set; }
    }
}