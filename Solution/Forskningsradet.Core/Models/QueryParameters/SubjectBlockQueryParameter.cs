
using EPiServer.Core;

namespace Forskningsradet.Core.Models.QueryParameters
{
    public class SubjectBlockQueryParameter : QueryParameterBase
    {
        public bool UseAlternativeDesign { get; set; }
        public PageReference PageReference { get; set; }
    }
}