﻿namespace Forskningsradet.Core.Models.QueryParameters
{
    public class RenderOptionQueryParameter : QueryParameterBase
    {
        public RenderWidthOption RenderWidthOption { get; set; }
        public RenderMediaBlockOption RenderMediaBlockOption { get; set; }
    }
}
