﻿namespace Forskningsradet.Core.Models.QueryParameters
{
    public enum RenderOption
    {
        None = 0,
        MediaBlock = 1,
        MediaWithCaption = 2,
        IsInSidebar = 3,
    }
}
