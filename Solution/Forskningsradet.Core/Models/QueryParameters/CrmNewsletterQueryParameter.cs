using System.Collections.Generic;

namespace Forskningsradet.Core.Models.QueryParameters
{
    public class CrmNewsletterQueryParameter : QueryParameterBase
    {
        public Dictionary<string, int> ValidSubjects { get; set; }
        public Dictionary<string, int> ValidTargetGroups { get; set; }
    }
}