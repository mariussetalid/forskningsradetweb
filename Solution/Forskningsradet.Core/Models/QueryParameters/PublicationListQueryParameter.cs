﻿namespace Forskningsradet.Core.Models.QueryParameters
{
    public class PublicationListQueryParameter : QueryParameterBase
    {
        public string Query { get; set; }
        public int Year { get; set; }
        public int[] Types { get; set; }
        public int Page { get; set; }
    }
}