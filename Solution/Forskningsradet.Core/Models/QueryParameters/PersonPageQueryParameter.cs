﻿namespace Forskningsradet.Core.Models.QueryParameters
{
    public class PersonPageQueryParameter : QueryParameterBase
    {
        public bool ShowPhoneNumber { get; set; }
    }
}
