
namespace Forskningsradet.Core.Models.QueryParameters
{
    public class SearchQueryParameter : QueryParameterBase
    {
        public string[] Subjects { get; }
        public string[] TargetGroups { get; }
        public string[] Types { get; }
        public string[] Categories { get; }
        public string Query { get; }
        public int From { get; }
        public int To { get; }
        public int Page { get; }

        public SearchQueryParameter(string[] subjects, string[] targetGroups, string[] types, string[] categories, string query, int? from, int? to, int? page)
        {
            Subjects = subjects;
            TargetGroups = targetGroups;
            Types = types;
            Categories = categories;
            Query = query;
            From = from ?? 0;
            To = to ?? 0;
            Page = page ?? 1;
        }
    }
}