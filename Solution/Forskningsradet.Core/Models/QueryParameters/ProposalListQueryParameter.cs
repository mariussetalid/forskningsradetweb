using Forskningsradet.Core.Models.SearchModels;

namespace Forskningsradet.Core.Models.QueryParameters
{
    public class ProposalListQueryParameter : QueryParameterBase
    {
        public TimeFrameFilter TimeFrame { get; set; }
        public string[] Subjects { get; set; }
        public string[] TargetGroups { get; set; }
        public int[] ApplicationTypes { get; set; }
        public string[] DeadlineTypes { get; set; }
    }
}