namespace Forskningsradet.Core.Models.QueryParameters
{
    public class PressReleaseQueryParameter : QueryParameterBase
    {
        public int Page { get; set; }
    }
}