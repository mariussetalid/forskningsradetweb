﻿namespace Forskningsradet.Core.Models.ViewModels.Media
{
    public class ImageFileViewModel
    {
        public string Url { get; set; }
        public string AltText { get; set; }
    }
}