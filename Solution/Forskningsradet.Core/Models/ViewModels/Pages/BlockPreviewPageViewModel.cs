﻿using Forskningsradet.Core.Models.ContentModels.Pages;
using ContentAreaPage = Forskningsradet.Core.Models.ReactModels.ContentAreaPage;

namespace Forskningsradet.Core.Models.ViewModels.Pages
{
    public class BlockPreviewPageViewModel : BasePageViewModel
    {
        public ContentAreaPage ContentAreaPage { get; }
        public ContentAreaPage SecondContentAreaPage { get; }
        public string[] FullRefreshProperties { get; }

        public BlockPreviewPageViewModel(BlockPreviewPage currentPage, ContentAreaPage contentAreaPage, ContentAreaPage secondContentAreaPage, string[] fullRefreshProperties = null) : base(currentPage)
        {
            ContentAreaPage = contentAreaPage;
            SecondContentAreaPage = secondContentAreaPage;
            FullRefreshProperties = fullRefreshProperties ?? new string[0];
        }
    }
}