﻿using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.ViewModels.Shared;

namespace Forskningsradet.Core.Models.ViewModels.Pages
{
    public class BasePageViewModel
    {
        public BasePageData CurrentBasePage { get; }
        public LayoutViewModel LayoutViewModel { get; set; }
        public MetadataViewModel MetadataViewModel { get; }

        public BasePageViewModel(BasePageData currentBasePage)
        {
            CurrentBasePage = currentBasePage;
            MetadataViewModel = new MetadataViewModel(currentBasePage);
        }
    }
}