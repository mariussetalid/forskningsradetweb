﻿using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.ViewModels.Contracts;

namespace Forskningsradet.Core.Models.ViewModels.Pages
{
    public class ReactPageViewModel : BasePageViewModel, IReactViewModel
    {
        public ReactModels.ReactComponent Model { get; }
        public string ReactComponentName { get; }
        public string[] FullRefreshProperties { get; }

        public ReactPageViewModel(EditorialPage currentPage, ReactModels.ReactComponent model, string[] fullRefreshProperties = null)
            : base(currentPage)
        {
            Model = model;
            ReactComponentName = currentPage.ReactComponentName();
            FullRefreshProperties = fullRefreshProperties ?? new string[0];
        }
    }
}