﻿using EPiServer.Core;
using Forskningsradet.Core.Extensions;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ContentModels.Pages;

namespace Forskningsradet.Core.Models.ViewModels.Shared
{
    public class MetadataViewModel
    {
        public BasePageData CurrentBasePage { get; }
        public SeoSettingsBlock SeoSettings { get; }
        public string Title { get; }
        public string FacebookAppId { get; set; }
        public string TwitterUser { get; set; }
        public string MainIntro { get; }
        public MetadataImageViewModel OpenGraphImage { get; set; }
        public MetadataIdioViewModel Idio { get; set; }

        public MetadataViewModel(BasePageData currentBasePage)
        {
            CurrentBasePage = currentBasePage;
            if(currentBasePage is EditorialPage page) 
            {
                SeoSettings = page.SeoSettings;
                Title = page.GetListTitle();
                MainIntro = page.GetListIntroOrMainIntro();
            }
        }
    }
}