﻿using System.Collections.Generic;
using Forskningsradet.Core.Models.ContentModels.Pages;

namespace Forskningsradet.Core.Models.ViewModels.Shared
{
    public class LayoutViewModel
    {
        public ReactModels.Header Header { get; set; }
        public ReactModels.Breadcrumbs Breadcrumbs { get; set; }
        public IList<ReactModels.Message> GlobalMessages { get; set; }
        public ReactModels.Footer Footer { get; set; }
        public bool HideFooter { get; set; }
        public bool EnableTrackingScripts { get; set; }
        public bool EnableApsisMarketingAutomationScript { get; set; }
        public string GoogleTagManagerId { get; set; }
        public string CanonicalUrl { get; set; }
        public ColorTheme ColorTheme { get; set; }

        public string FaviconIcoOverride { get; set; }
        public string FaviconPng16Override { get; set; }
        public string FaviconPng32Override { get; set; }
        public string FaviconPng96Override { get; set; }
        public string FaviconPng150Override { get; set; }
        public string FaviconPng180Override { get; set; }
        public string FaviconPng192Override { get; set; }
        public string FaviconPng512Override { get; set; }
        public string FaviconSvgOverride { get; set; }
        public bool UseFallbackIcons { get; set; }
    }
}