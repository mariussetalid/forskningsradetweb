﻿namespace Forskningsradet.Core.Models.ViewModels.Shared
{
    public class MetadataIdioViewModel
    {
        public string Subjects { get; set; }
        public string TargetGroups { get; set; }
        public string ApplicationType { get; set; }
        public string EventType { get; set; }
    }
}