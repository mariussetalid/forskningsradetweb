﻿using System.Drawing;

namespace Forskningsradet.Core.Models.ViewModels.Shared
{
    public class MetadataImageViewModel
    {
        public string Url { get; set; }
        public Size Size { get; set; }
    }
}