﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Forskningsradet.Core.Models.ViewModels.Forms
{
    public class NewsletterRegistrationFormModel
    {
        [Required(ErrorMessage = "RequiredErrorMessage")]
        [EmailAddress(ErrorMessage = "EmailAddressErrorMessage")]
        public string Email { get; set; }

        [Required(ErrorMessage = "RequiredErrorMessage")]
        [Compare(nameof(Email), ErrorMessage = "RepeatEmailAddressErrorMessage")]
        public string RepeatEmail { get; set; }

        public List<string> Subscriptions { get; set; }
    }
}
