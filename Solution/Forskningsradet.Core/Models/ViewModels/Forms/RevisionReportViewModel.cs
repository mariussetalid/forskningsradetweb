﻿using System;
using System.Globalization;

namespace Forskningsradet.Core.Models.ViewModels.Forms
{
    public class RevisionReportViewModel
    {
        public Guid PageGuid { get; set; }
        public string PageName { get; set; }
        public DateTime? PublishedDate { get; set; }
        public DateTime LastEdit { get; set; }
        public DateTime RevisionDate { get; set; }
        public string ChangedBy { get; set; }
        public CultureInfo Language { get; set; }
        public string PageType { get; set; }
        public string EditUrl { get; set; }
    }
}