﻿using System.Collections.Generic;

namespace Forskningsradet.Core.Models.ViewModels.Forms
{
    public class FileReportFileModel
    {
        public string Id { get; set; }
        public string FileName { get; set; }
        public string Link { get; set; }
        public string Info { get; set; }
        public int ReferenceCount { get; set; }
        public IEnumerable<string> Folders { get; set; }
        public string ParentId { get; set; }
    }
}
