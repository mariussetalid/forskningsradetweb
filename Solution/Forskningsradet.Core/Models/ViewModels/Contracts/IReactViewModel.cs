﻿
namespace Forskningsradet.Core.Models.ViewModels.Contracts
{
    public interface IReactViewModel
    {
        ReactModels.ReactComponent Model { get; }
        string ReactComponentName { get; }
        string[] FullRefreshProperties { get; }
    }
}
