﻿using Forskningsradet.Core.Models.ViewModels.Contracts;

namespace Forskningsradet.Core.Models.ViewModels.Blocks
{
    public class ReactPartialViewModel : IReactViewModel
    {
        public ReactModels.ReactComponent Model { get; }
        public string ReactComponentName { get; }
        public string[] FullRefreshProperties { get; }

        public ReactPartialViewModel(string reactComponentName, ReactModels.ReactComponent model, string[] fullRefreshProperties = null)
        {
            Model = model;
            ReactComponentName = reactComponentName;
            FullRefreshProperties = fullRefreshProperties ?? new string[0];
        }
    }
}