
namespace Forskningsradet.Core.Models.RequestModels
{
    public class CustomCategoriesSearchRequest : SearchRequestBase
    {
        public string[] Categories { get; }
        public int[] TopCategories { get; }

        public CustomCategoriesSearchRequest(string query, string[] categories, int[] topCategories, string[] types,
            int from, int to, int page, int pageSize, int pageRoot, bool skipTracking)
            : base(query, types, from, to, page, pageSize, pageRoot, skipTracking)
        {
            Categories = categories;
            TopCategories = topCategories;
        }
    }
}