﻿namespace Forskningsradet.Core.Models.RequestModels
{
    public class HistoricProposalRequest
    {
        public int Year { get; }
        public int[] Types { get; }
        public string Query { get; }
        public int PageRoot { get; }

        public HistoricProposalRequest(string query, int pageRoot, int year, int[] types)
        {
            Query = query;
            PageRoot = pageRoot;
            Year = year;
            Types = types;
        }
    }
}
