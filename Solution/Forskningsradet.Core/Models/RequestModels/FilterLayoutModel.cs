﻿using System.Collections.Generic;
using System.Linq;
using Forskningsradet.Core.Models.QueryParameters;
using Forskningsradet.Core.Models.SearchModels;

namespace Forskningsradet.Core.Models.RequestModels
{
    public class FilterLayoutModel
    {
        public FilterLayoutModelParameters Parameters { get; }
        public IList<CategoryGroup> CategoryGroups { get; }
        public ReactModels.ContentArea ContentArea { get; }
        public bool LeftSidePlacement { get; }
        public string FilterTitle { get; }
        public int ResultsCount { get; }

        public FilterLayoutModel(SearchQueryParameter parameters, IList<CategoryGroup> categoryGroups, string filterTitle, int resultsCount, bool showRangeSlider = false, bool leftSidePlacement = false)
        {
            Parameters = new FilterLayoutModelParameters
            {
                Subjects = parameters.Subjects,
                TargetGroups = parameters.TargetGroups,
                Types = parameters.Types,
                Categories = parameters.Categories,
                Query = parameters.Query,
                From = parameters.From,
                To = parameters.To,
                Page = parameters.Page,
                ShowRangeSlider = showRangeSlider
            };
            CategoryGroups = categoryGroups;
            LeftSidePlacement = leftSidePlacement;
            FilterTitle = filterTitle;
            ResultsCount = resultsCount;
        }

        public FilterLayoutModel(ProposalListQueryParameter parameters, IList<CategoryGroup> categoryGroups, ReactModels.ContentArea contentArea, string filterTitle, int resultsCount)
        {
            Parameters = new FilterLayoutModelParameters
            {
                Subjects = parameters.Subjects,
                TargetGroups = parameters.TargetGroups,
                TimeFrame = parameters.TimeFrame,
                ApplicationTypes = parameters.ApplicationTypes,
                DeadlineTypes = parameters.DeadlineTypes,
            };
            CategoryGroups = categoryGroups;
            ContentArea = contentArea;
            FilterTitle = filterTitle;
            ResultsCount = resultsCount;
        }

        public FilterLayoutModel(EventListQueryParameter parameters, IList<CategoryGroup> categoryGroups, string filterTitle, int resultsCount)
        {
            Parameters = new FilterLayoutModelParameters
            {
                Subjects = parameters.Subjects?.Select(x => x.ToString()).ToArray(),
                TargetGroups = parameters.TargetGroups?.Select(x => x.ToString()).ToArray(),
                Categories = parameters.Categories?.Select(x => x.ToString()).ToArray(),
                Types = parameters.Type,
                Location = parameters.Location,
                VideoOnly = parameters.VideoOnly,
                ShowVideoCheckbox = true,
            };
            CategoryGroups = categoryGroups;
            FilterTitle = filterTitle;
            ResultsCount = resultsCount;
        }

        public FilterLayoutModel(IList<CategoryGroup> categoryGroups, string[] categories, ReactModels.ContentArea contentArea, string filterTitle, int resultsCount)
        {
            CategoryGroups = categoryGroups;
            Parameters = new FilterLayoutModelParameters
            {
                Categories = categories
            };
            ContentArea = contentArea;
            FilterTitle = filterTitle;
            ResultsCount = resultsCount;
        }
    }
}
