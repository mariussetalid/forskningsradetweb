﻿using System.Collections.Generic;
using EPiServer.DataAbstraction;

namespace Forskningsradet.Core.Models.RequestModels
{
    public class PublicationSearchRequest : SearchRequestBase
    {
        public int Year { get; }
        public IList<Category> Categories { get; }
        public int[] PublicationTypes { get; }

        public PublicationSearchRequest(string query, int year, IList<Category> categories, int[] publicationTypes, string[] types, int from, int to, int page, int pageSize, int pageRoot)
            : base(query, types, from, to, page, pageSize, pageRoot)
        {
            Year = year;
            Categories = categories;
            PublicationTypes = publicationTypes;
        }
    }
}
