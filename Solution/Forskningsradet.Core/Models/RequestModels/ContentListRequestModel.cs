using EPiServer.Core;

namespace Forskningsradet.Core.Models.RequestModels
{
    public class ContentListRequestModel
    {
        public ContentReference CurrentPageLink { get; set; }
        public int[] PageCategories { get; set; }
        public int[] Categories { get; set; }
        public int[] TopCategories { get; set; }
        public int[] ContentTypes { get; set; }
        public int Take { get; set; }
        public int Page { get; set; }
        public int PageRoot { get; set; }
        public bool OnlyGetCategoryHomePages { get; set; }
    }
}