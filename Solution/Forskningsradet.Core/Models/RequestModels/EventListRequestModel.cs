using Forskningsradet.Core.Models.SearchModels;

namespace Forskningsradet.Core.Models.RequestModels
{
    public class EventListRequestModel
    {
        public TimeFrameFilter TimeFrame { get; }
        public int[] PageCategories { get; }
        public int[] Categories { get; }
        public int[] TopCategories { get; }
        public string[] Type { get; }
        public string[] Location { get; }
        public bool VideoOnly { get; }
        public int PageRoot { get; }
        public int Take { get; }
        public bool OnlyCustomCategories { get; }

        public EventListRequestModel(
            TimeFrameFilter timeFrame,
            int[] pageCategories,
            int[] categories,
            int[] topCategories,
            string[] type,
            string[] location,
            bool videoOnly,
            int pageRoot,
            int take,
            bool onlyCustomCategories
        )
        {
            TimeFrame = timeFrame;
            PageCategories = pageCategories;
            Categories = categories;
            TopCategories = topCategories;
            Type = type;
            Location = location;
            VideoOnly = videoOnly;
            PageRoot = pageRoot;
            Take = take;
            OnlyCustomCategories = onlyCustomCategories;
        }

        public EventListRequestModel(
            int[] categories,
            int pageRoot,
            int take
        )
        {
            TimeFrame = TimeFrameFilter.Future;
            PageCategories = new int[0];
            Categories = categories;
            TopCategories = new int[0];
            Type = new string[0];
            Location = new string[0];
            VideoOnly = false;
            PageRoot = pageRoot;
            Take = take;
            OnlyCustomCategories = false;
        }

        public EventListRequestModel(int pageRoot, int take)
        {
            PageRoot = pageRoot;
            Take = take;
            TimeFrame = TimeFrameFilter.Future;
            PageCategories = new int[0];
            Categories = new int[0];
            TopCategories = new int[0];
            Type = new string[0];
            Location = new string[0];
            VideoOnly = false;
            OnlyCustomCategories = false;
        }
    }
}