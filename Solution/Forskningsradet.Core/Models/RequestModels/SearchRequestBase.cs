
namespace Forskningsradet.Core.Models.RequestModels
{
    public abstract class SearchRequestBase
    {
        public string Query { get; }
        public string[] Types { get; }
        public int To { get; }
        public int From { get; }
        public int Page { get; }
        public int PageSize { get; }
        public int PageRoot { get; }
        public bool SkipTracking { get; }

        protected SearchRequestBase(string query, string[] types, int from, int to, int page, int pageSize, int pageRoot, bool skipTracking = false)
        {
            Query = query?.Trim();
            Types = types;
            From = from;
            To = to;
            Page = page;
            PageSize = pageSize;
            PageRoot = pageRoot;
            SkipTracking = skipTracking;
        }
    }
}