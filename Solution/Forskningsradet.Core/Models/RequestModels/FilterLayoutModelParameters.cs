﻿using Forskningsradet.Core.Models.SearchModels;

namespace Forskningsradet.Core.Models.RequestModels
{
    public class FilterLayoutModelParameters
    {
        public string[] Subjects { get; set; }
        public string[] TargetGroups { get; set; }
        public string[] Types { get; set; }
        public string[] Categories { get; set; }
        public string Query { get; set; }
        public int From { get; set; }
        public int To { get; set; }
        public int Page { get; set; }
        public TimeFrameFilter TimeFrame { get; set; }
        public int[] ApplicationTypes { get; set; }
        public string[] DeadlineTypes { get; set; }
        public string[] Location { get; set; }
        public bool VideoOnly { get; set; }
        public bool ShowRangeSlider { get; set; }
        public bool ShowVideoCheckbox { get; set; }
    }
}