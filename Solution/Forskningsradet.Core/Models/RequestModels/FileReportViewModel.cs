﻿using System.Collections.Generic;
using Forskningsradet.Core.Models.ViewModels.Forms;

namespace Forskningsradet.Core.Models.RequestModels
{
    public class FileReportViewModel
    {
        public IEnumerable<FileReportFileModel> Files { get; set; }
        public int Index { get; set; }
        public int Interval { get; set; }
        public int TotalMatching { get; set; }
        public long Time { get; set; }
    }
}
