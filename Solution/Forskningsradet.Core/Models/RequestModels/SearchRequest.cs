
namespace Forskningsradet.Core.Models.RequestModels
{
    public class SearchRequest : SearchRequestBase
    {
        public string[] Subjects { get; }
        public string[] TargetGroups { get; }

        public SearchRequest(string query, string[] subjects, string[] targetGroups, string[] types, int from, int to, int page, int pageSize, int pageRoot, bool skipTracking)
            : base(query, types, from, to, page, pageSize, pageRoot, skipTracking)
        {
            Subjects = subjects;
            TargetGroups = targetGroups;
        }
    }
}