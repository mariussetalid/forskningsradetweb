﻿using System;
using System.Collections.Generic;
using EPiServer.DataAbstraction;
using Forskningsradet.Common.Constants;

namespace Forskningsradet.Core.Models.RequestModels
{
    public class PublicationRequest
    {
        public int Year { get; }
        public int[] Types { get; }
        public string Query { get; }
        public int PageRoot { get; }
        public IList<Category> Categories { get; }
        public int Page { get; set; }
        public int PageSize { get; }
        public bool WithPaging  => PageSize > 0 && PageSize < SearchConstants.MaxPageSize;
        public bool UseGlobalSearchWithQuery { get; set; }

        public bool IsEmptyRequest =>
            Year <= 0
            && !(Types?.Length > 0)
            && string.IsNullOrEmpty(Query)
            && !(Categories?.Count > 0)
            && PageSize == 0;

        public PublicationRequest(string query, int pageRoot, int year, int[] types, int page = 0, int pageSize = 0, IList<Category> categories = null, bool useGlobalSearchWithQuery = false)
        {
            Query = query;
            PageRoot = pageRoot;
            Year = year;
            Types = types;
            Categories = categories;
            Page = Math.Max(page, 1);
            PageSize = pageSize > 0 ? pageSize : 0;
            UseGlobalSearchWithQuery = useGlobalSearchWithQuery;
        }
    }
}
