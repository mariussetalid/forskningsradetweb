﻿using EPiServer.Core;
using EPiServer.SpecializedProperties;

namespace Forskningsradet.Core.Models.RequestModels
{
    public class LargeDocumentModel
    {
        public bool ShowNumbers { get; }
        public int ChapterDepth { get; }
        public LinkItemCollection LinkItems { get; }
        public ContentReference MainPageReference { get; }
        public Node CurrentPageNode { get; }
        public Node LargeDocumentStructure { get; }

        public LargeDocumentModel(bool showNumbers, int chapterDepth, ContentReference mainPageReference, Node currentPageNode, Node largeDocumentStructure, LinkItemCollection linkItems = null)
        {
            ShowNumbers = showNumbers;
            ChapterDepth = chapterDepth;
            LinkItems = linkItems ?? new LinkItemCollection();
            MainPageReference = mainPageReference;
            CurrentPageNode = currentPageNode;
            LargeDocumentStructure = largeDocumentStructure;
        }
    }
}
