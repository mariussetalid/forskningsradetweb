﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ViewModels.Forms;

namespace Forskningsradet.Core.Models.RequestModels
{
    public class RevisionReportRequestModel
    {
        public IEnumerable<RevisionReportViewModel> Pages { get; set; }
        public string PageType { get; set; }
        public bool ShowNorwegianPages { get; set; }
        public bool ShowEnglishPages { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = DateTimeFormats.NorwegianDate)]
        [DataType(DataType.Date)]
        public DateTime? DateStart { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = DateTimeFormats.NorwegianDate)]
        [DataType(DataType.Date)]
        public DateTime? DateEnd { get; set; }
    }
}