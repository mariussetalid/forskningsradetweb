﻿using System.Collections.Generic;
using EPiServer.Core;

namespace Forskningsradet.Core.Models.RequestModels
{
    public class Node
    {
        public ContentReference ContentReference { get; }
        public int Depth { get; }
        public List<Node> Children { get; } = new List<Node>();
        public Node ParentNode { get; }

        public Node(Node parent, ContentReference reference, int depth)
        {
            ParentNode = parent;
            ContentReference = reference;
            Depth = depth;
        }

        public void AddChild(Node child) => Children.Add(child);

        public Node Find(ContentReference reference)
        {
            if (ContentReference == reference)
                return this;

            foreach (var child in Children)
            {
                if (child.Find(reference) is Node node)
                    return node;
            }

            return null;
        }

        public IEnumerable<ContentReference> GetAncestors()
        {
            var ancestors = new List<ContentReference> { ContentReference };
            var node = this;
            while (node.ParentNode != null)
            {
                ancestors.Add(node.ParentNode.ContentReference);
                node = node.ParentNode;
            }

            return ancestors;
        }
    }
}