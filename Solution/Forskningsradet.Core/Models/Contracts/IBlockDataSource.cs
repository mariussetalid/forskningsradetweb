namespace Forskningsradet.Core.Models.Contracts
{
    public interface IBlockDataSource
    {
        string Identity { get; }
        string Title { get; }
    }
}