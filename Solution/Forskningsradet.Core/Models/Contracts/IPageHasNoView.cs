﻿using EPiServer.Core;

namespace Forskningsradet.Core.Models.Contracts
{
    /// <summary>
    /// A <see cref="PageData"/> object implementing this interface will not be shown with
    /// a view, it will only be shown to editors in form mode.  
    /// </summary>
    public interface IPageHasNoView
    {

    }
}