﻿using EPiServer.Core;

namespace Forskningsradet.Core.Models.Contracts
{
    /// <summary>
    /// A <see cref="PageData"/> object implementing this interface will use the no view icon
    /// in the page tree.
    /// </summary>
    public interface IPageHasNoViewIcon
    {
    }
}