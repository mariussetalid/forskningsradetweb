namespace Forskningsradet.Core.Models.Contracts
{
    public interface IIdentifiable
    {
        string Id { get; }
    }
}