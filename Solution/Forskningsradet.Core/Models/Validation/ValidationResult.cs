using System.Collections.Generic;
using System.Linq;

namespace Forskningsradet.Core.Models.Validation
{
    public class ValidationResult
    {
        public bool HasErrors => ErrorMessages?.Any() ?? false;
        public IEnumerable<string> ErrorMessages { get; set; }
    }
}