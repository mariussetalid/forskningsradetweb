﻿using System.Collections.Generic;

namespace Forskningsradet.Core.Models.Validation
{
    public class NewsletterModelValidation
    {
        public string FieldName { get; set; }
        public string AttemptedValue { get; set; }
        public IEnumerable<string> Errors { get; set; }

    }
}