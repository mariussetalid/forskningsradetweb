﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Forskningsradet.Core.Models.Enums
{
    public enum HstsHeaderBehaviour
    {
        Skip = 0,
        Enable5Mins = 100,
        Enable1Week = 200,
        Enable1Month = 300,
        Enable2YearsWithPreload = 400,
        Disable = 1000
    }
}
