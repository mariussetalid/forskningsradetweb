﻿using System.Collections.Generic;
using EPiServer.Core;

namespace Forskningsradet.Core.Models
{
    public class BreadcrumbsBuildContext
    {
        public IEnumerable<PageData> ParentPages { get; set; }
        public IEnumerable<PageData> ChildPages { get; set; }
    }
}
