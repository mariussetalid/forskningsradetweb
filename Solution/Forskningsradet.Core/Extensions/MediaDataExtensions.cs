﻿using EPiServer.Core;

namespace Forskningsradet.Core.Extensions
{
    public static class MediaDataExtensions
    {
        const string SizeFormat = "0.### ";
        const string ByteFormat = "0 B";
        const string KB = "KB";
        const string MB = "MB";
        const string GB = "GB";
        private const int Gigabyte = 0x40000000;
        private const int Megabyte = 0x100000;
        private const int Kilobyte = 0x400;

        // Code is inspired by answer at https://stackoverflow.com/a/11124118
        public static string GetFileSize(this MediaData media)
        {
            var value = GetBytes(media);
            var absoluteValue = (value < 0 ? -value : value);
            string suffix;
            double readableSize;
            if (absoluteValue >= Gigabyte)
            {
                suffix = GB;
                readableSize = (value >> 20);
            }
            else if (absoluteValue >= Megabyte)
            {
                suffix = MB;
                readableSize = (value >> 10);
            }
            else if (absoluteValue >= Kilobyte)
            {
                suffix = KB;
                readableSize = value;
            }
            else
            {
                return value.ToString(ByteFormat);
            }
            readableSize = GetFractionalValue(readableSize);
            return readableSize.ToString(SizeFormat) + suffix;
        }

        private static double GetFractionalValue(double value) => value / 1024;

        private static long GetBytes(MediaData media)
        {
            if (media is null)
                return 0;

            using (var stream = media.BinaryData.OpenRead())
            {
                return stream.Length;
            }
        }
    }
}