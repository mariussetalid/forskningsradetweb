using System;
using System.Linq;
using EPiServer.Core;
using EPiServer.Find;
using EPiServer.Find.Cms;
using EPiServer.Find.UnifiedSearch;
using EPiServer.Logging;
using Forskningsradet.Core.Models.SearchModels;
using Microsoft.Data.Edm.Library.Expressions;

namespace Forskningsradet.Core.Extensions
{
    // Inspired by: https://www.brianweet.com/2017/03/17/handling-find-serviceexception.html
    public static class EpiserverFindExtensions
    {
        public static ITypeSearch<TResult> Apply<TResult>(this ITypeSearch<TResult> search, params Func<ITypeSearch<TResult>, ITypeSearch<TResult>>[] searchUpdateFunctions) 
            => (searchUpdateFunctions ?? Enumerable.Empty<Func<ITypeSearch<TResult>, ITypeSearch<TResult>>>()).Aggregate(search, (current, func) => func(current));

        public static UnifiedSearchResults GetResultSafe(this ITypeSearch<ISearchContent> search, HitSpecification hitSpecification = null)
        {
            UnifiedSearchResults searchResults;
            try
            {
                searchResults = search.GetResult(hitSpecification);
            }
            catch (Exception ex) when (ex is ClientException || ex is ServiceException)
            {
                LogError(ex);
                searchResults = new EmptyUnifiedSearchResult();
            }

            return searchResults;
        }

        public static SearchResults<TResult> GetResultSafe<TResult>(this ISearch<TResult> search)
        {
            SearchResults<TResult> contentResult;
            try
            {
                contentResult = search.GetResult();
            }
            catch (Exception ex) when (ex is ClientException || ex is ServiceException)
            {
                LogError(ex);
                contentResult = new EmptyResult<TResult>();
            }

            return contentResult;
        }

        public static IContentResult<TContentData> GetContentResultSafe<TContentData>(
            this ITypeSearch<TContentData> search,
            int cacheForSeconds = 60,
            bool cacheForEditorsAndAdmins = false) where TContentData : IContentData
        {
            IContentResult<TContentData> contentResult;
            try
            {
                contentResult = search.GetContentResult(cacheForSeconds, cacheForEditorsAndAdmins);
            }
            catch (Exception ex) when (ex is ClientException || ex is ServiceException)
            {
                LogError(ex);
                contentResult = new EmptyContentResult<TContentData>();
            }

            return contentResult;
        }

        public static PagesResult<TPageData> GetPagesResultSafe<TPageData>(
            this ITypeSearch<TPageData> search) where TPageData : PageData
        {
            PagesResult<TPageData> pagesResult;
            try
            {
                pagesResult = search.GetPagesResult();
            }
            catch (Exception ex) when (ex is ClientException || ex is ServiceException)
            {
                LogError(ex);
                pagesResult = new EmptyPagesResult<TPageData>();
            }

            return pagesResult;
        }

        private static void LogError(Exception ex)
        {
            var logger = LogManager.GetLogger(typeof(EpiserverFindExtensions));
            logger.Error("Could not retrieve data from Episerver Find, returning empty result", ex);
        }
    }
}