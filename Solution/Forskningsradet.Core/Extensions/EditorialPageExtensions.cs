﻿using System.Web;
using EPiServer.Core.Html;
using Forskningsradet.Core.Models.ContentModels.Pages;

namespace Forskningsradet.Core.Extensions
{
    public static class EditorialPageExtensions
    {
        public static string GetListTitle(this EditorialPage page) =>
            page != null
                ? page.ListTitle ?? GetTitle(page)
                : string.Empty;

        private static string GetTitle(EditorialPage page)
        {
            switch (page)
            {
                case SatelliteFrontPage satelliteFrontPage:
                    return satelliteFrontPage.Title ?? satelliteFrontPage.PageName;
                case ContentAreaPage contentAreaPage:
                    return contentAreaPage.Title ?? contentAreaPage.PageName;
                case LargeDocumentPageBase largeDocumentPageBase:
                    return largeDocumentPageBase.Title ?? largeDocumentPageBase.PageName;
                default:
                    return page.PageName;
            }
        }

        public static string GetListIntroOrMainIntro(this EditorialPage page, int maxTextLengthToReturn = 150)
        {
            switch (page)
            {
                case null:
                    return string.Empty;
                case ArticlePage articlePage:
                    return articlePage.ListIntro ?? articlePage.MainIntro ?? string.Empty;
                case ProposalPage proposalPage:
                    return proposalPage.ListIntro ?? TextIndexer.StripHtml(HttpUtility.HtmlDecode(proposalPage.Purpose?.ToString()), maxTextLengthToReturn);
                case InternationalProposalPage internationalProposalPage:
                    return internationalProposalPage.ListIntro ?? internationalProposalPage.TextBlock.Intro;
                case PressReleasePage pressReleasePage:
                    return pressReleasePage.ListIntro ?? pressReleasePage.Lead;
                default:
                    return page?.ListIntro ?? string.Empty;
            }
        }
    }
}