﻿using System;
using System.Text;
using System.Web;
using EPiServer;
using EPiServer.Core;
using EPiServer.Web.Routing;

namespace Forskningsradet.Core.Extensions
{
    public static class PageReferenceExtension
    {
        public static string GetExternalUrl(this PageReference pageReference, IUrlResolver urlResolver)
        {
            if (PageReference.IsNullOrEmpty(pageReference))
                return string.Empty;
            var internalUrl = urlResolver.GetUrl(pageReference);
            var url = new UrlBuilder(internalUrl);
            Global.UrlRewriteProvider?.ConvertToExternal(url, null, Encoding.UTF8);

            var externalUrl = HttpContext.Current is null
                ? UriSupport.AbsoluteUrlBySettings(url.ToString())
                : HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + url;

            return externalUrl;
        } 
    }
}
