﻿using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using Forskningsradet.Common.Constants;

namespace Forskningsradet.Core.Extensions
{
    public static class StringExtensions
    {
        public static string CamelCaseToWhiteSpaceSeperated(this string camelCase)
            => Regex.Replace(camelCase, "(\\B[A-Z])", " $1");

        public static string EnsureValidHtmlId(this string id)
        {
            if (!string.IsNullOrWhiteSpace(id))
            {
                id = id.Replace(" ", "");

                if (id[0] >= 'A' && id[0] <= 'Z' || id[0] >= 'a' && id[0] <= 'z')
                {
                    return HttpUtility.HtmlAttributeEncode(id);
                }
            }

            return null;
        }

        public static string EnsureValidEnglishWindowsFileName(this string fileName)
        {
            char[] norwegianLetters = { 'æ', 'ø', 'å', 'Æ', 'Ø', 'Å' };
            var result = string.Join("_", fileName.Split(norwegianLetters));
            return string.Concat(result.Split(Path.GetInvalidFileNameChars()));
        }

        public static string RemoveNorwegianStopwords(this string text) =>
            text is null ? string.Empty : string.Join(" ", text.Split(' ').Where(word => !SearchConstants.NorwegianStopwords.Contains(word.ToLowerInvariant())));
    }
}
