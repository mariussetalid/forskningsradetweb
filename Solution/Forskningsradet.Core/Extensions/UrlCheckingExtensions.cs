﻿using EPiServer;
using EPiServer.SpecializedProperties;
using Forskningsradet.Core.Services.Contracts;

namespace Forskningsradet.Core.Extensions
{
    public static class UrlCheckingExtensions
    {
        public static bool IsExternal(this Url url, IUrlCheckingService urlCheckingService) => urlCheckingService.CheckIsExternal(url?.ToString());

        public static bool IsExternal(this LinkItem linkItem, IUrlCheckingService urlCheckingService) => urlCheckingService.CheckIsExternal(linkItem?.Href);
    }
}