﻿using EPiServer;
using EPiServer.Web.Routing;

namespace Forskningsradet.Core.Extensions
{
    public static class EpiserverUrlExtensions
    {
        public static string GetUrl(this Url url, IUrlResolver urlResolver)
            => url != null
                ? url.Scheme == "mailto" ? url.ToString() : urlResolver.GetUrl(url.ToString())
                : null;
    }
}