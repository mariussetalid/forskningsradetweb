﻿using System.Web;
using System.Web.Mvc;
using EPiServer;
using EPiServer.Core;
using EPiServer.Web.Mvc.Html;

namespace Forskningsradet.Core.Extensions
{
    public static class UrlHelperExtensions
    {
        /// <summary>
        /// Convert contentreference to absolute Url
        /// </summary>
        public static string AbsoluteUrl(this UrlHelper helper, ContentReference content)
            => AbsoluteUrl(helper, helper.ContentUrl(content));

        /// <summary>
        /// Convert url to absolute
        /// </summary>
        public static string AbsoluteUrl(this UrlHelper helper, Url url)
            => url.IsAbsoluteUri
                ? url.OriginalString
                : AbsoluteUrl(helper, url.OriginalString);

        /// <summary>
        /// Convert relative url string to absolute
        /// </summary>
        public static string AbsoluteUrl(this UrlHelper helper, string relativeUrl)
        {
            var baseUrl = GetSiteBaseUrl();
            return $"{baseUrl}{relativeUrl}";
        }

        static string GetSiteBaseUrl()
        {
            var url = HttpContext.Current.Request.Url;
            var applicationPath = HttpContext.Current.Request.ApplicationPath.TrimEnd('/');
            return $"{url.Scheme}://{url.Authority}{applicationPath}";
        }
    }
}