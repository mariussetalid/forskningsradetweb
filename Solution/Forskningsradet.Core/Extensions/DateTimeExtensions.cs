﻿using System;
using System.Globalization;
using EPiServer.ServiceLocation;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Services.Contracts;

namespace Forskningsradet.Core.Extensions
{
    public static class DateTimeExtensions
    {
        private static readonly TimeZoneInfo NorwegianTimeZone = TimeZoneInfo.FindSystemTimeZoneById("W. Europe Standard Time");
        private static ILanguageContext LanguageContext => ServiceLocator.Current.GetInstance<ILanguageContext>();

        public static DateTime ToClosestDateUnspecified(this DateTime value)
        {
            var date = new DateTime(value.Year, value.Month, value.Day, 0, 0, 0, DateTimeKind.Unspecified);
            return value.Hour <= 12
                ? date
                : date.AddDays(1);
        }

        public static bool IsPast(this DateTime value) =>
            value.Date < DateTime.Today;

        public static DateTime ToDisplayDate(this DateTime value) =>
            TimeZoneInfo.ConvertTimeFromUtc(
                value.ToUniversalTime(),
                NorwegianTimeZone);

        public static string ToNorwegianDateString(this DateTime value) =>
            value.ToString(DateTimeFormats.NorwegianDate, new CultureInfo(LanguageConstants.NorwegianLanguageBranch));

        public static bool IsNorwegianSummerTime(this DateTime value) =>
            NorwegianTimeZone.IsDaylightSavingTime(value.ToDisplayDate());

        public static DateTime FromEpochDateTime(this long timeInMilliseconds) =>
            new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)
                .AddMilliseconds(timeInMilliseconds);

        public static long ToEpochDateTime(this DateTime dateTime) =>
            (long)(dateTime - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds;

        public static string DeadlineFormattedWithDaylightSavingNameOrStandardName(this DateTime value)
        {
            var displayDate = value.ToDisplayDate();
            var cultureInfo = LanguageContext.CurrentCulture;
            return $"{CultureSpecificDayMonthYear(displayDate)}, {displayDate.ToString("HH:mm", cultureInfo)}"
                + " "
                + (value.IsNorwegianSummerTime() ? DateTimeFormats.TimeZone.CEST : DateTimeFormats.TimeZone.CET);
        }

        public static string CultureSpecificDayMonthYear(this DateTime d)
        {
            var cultureInfo = LanguageContext.CurrentCulture;
            return cultureInfo.IsNorwegianCulture()
                ? $"{d.ToString(DateTimeFormats.NorwegianDayMonth, cultureInfo)} {d.Year}"
                : d.ToString(DateTimeFormats.EnglishDayMonthYear, cultureInfo);
        }

        public static string CultureSpecificDay(this DateTime value) =>
            LanguageContext.CurrentCulture.IsNorwegianCulture()
                ? value.ToString(DateTimeFormats.NorwegianDateDayLong)
                : value.ToString(DateTimeFormats.DateDayLong);

        private static bool IsNorwegianCulture(this CultureInfo culture) =>
                culture.TwoLetterISOLanguageName == new CultureInfo(LanguageConstants.NorwegianLanguageBranch).TwoLetterISOLanguageName;
    }
}