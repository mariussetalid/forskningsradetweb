﻿using System.Globalization;
using Forskningsradet.Common.Constants;

namespace Forskningsradet.Core.Extensions
{
    public static class IntegerExtensions
    {
        public static string ToLargeNumberFormat(this int number)
        {
            if (CultureInfo.CurrentCulture.Name == "nb-NO")
                return number.ToString(NumberFormats.LargeNumberFormat);
            else
            {
                var nfi = new NumberFormatInfo { NumberGroupSeparator = " " };
                return number.ToString(NumberFormats.LargeNumberFormat, nfi);
            }
        }
    }
}
