﻿using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.ServiceLocation;

namespace Forskningsradet.Core.Extensions
{
    public static class PageDataExtensions
    {
        public static string FriendlyPageTypeName(this PageData page)
            => page != null
                   && page.ContentTypeID > 0
                   && ServiceLocator.Current.GetInstance<IContentTypeRepository>().Load(page.ContentTypeID) is ContentType loadedType
                ? loadedType.DisplayName ?? string.Empty
                : page?.PageTypeName ?? string.Empty;
    }
}