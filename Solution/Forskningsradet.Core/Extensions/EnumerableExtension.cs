﻿using System.Collections.Generic;
using System.Linq;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ApiModels.Evurdering;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.SearchModels;

namespace Forskningsradet.Core.Extensions
{
    public static class EnumerableExtension
    {
        public static IEnumerable<ProposalBasePage> GetOrderedProposals(this IEnumerable<ProposalBasePage> proposalPages, TimeFrameFilter timeFrame)
        {
            var pagesInGroup = timeFrame == TimeFrameFilter.Future
                ? OrderFutureProposals()
                : OrderProposals();

            var upcomingDate = pagesInGroup.FirstOrDefault()?.DeadlineComputed;
            var result = new List<ProposalBasePage>();

            if (upcomingDate != null)
                result.AddRange(pagesInGroup.Where(x => x.DeadlineComputed == upcomingDate));

            result.AddRange(pagesInGroup.Where(x => !x.DeadlineComputed.HasValue));
            result.AddRange(pagesInGroup.Where(x => x.DeadlineComputed != upcomingDate && x.DeadlineComputed.HasValue));

            return result.OrderByProposalState();

            List<ProposalBasePage> OrderFutureProposals() =>
                proposalPages
                .OrderByDescending(x => x.DeadlineComputed.HasValue)
                .ThenBy(x => x.DeadlineComputed)
                .ThenByDescending(x => x.SortingNumberInList.HasValue)
                .ThenBy(x => x.SortingNumberInList)
                .ToList();

            List<ProposalBasePage> OrderProposals() =>
                proposalPages
                .OrderByDescending(x => x.DeadlineComputed.HasValue)
                .ThenByDescending(x => x.DeadlineComputed)
                .ThenByDescending(x => x.SortingNumberInList.HasValue)
                .ThenBy(x => x.SortingNumberInList)
                .ToList();
        }

        public static IEnumerable<FilterOption> OrderFilterOptons(this IEnumerable<FilterOption> options, string queryParameter, TimeFrameFilter? timeFrame = null)
        {
            if (queryParameter == QueryParameters.Deadlines)
                return OrderByDateDescending();
            else if (queryParameter == QueryParameters.DeadlineTypes)
                return OrderByDateThenByAlphabet();
            else
                return OrderAlphabetically();

            IEnumerable<FilterOption> OrderAlphabetically() =>
                options.OrderBy(x => x.Name);

            IEnumerable<FilterOption> OrderByDateDescending() =>
                options.OrderByDescending(x => x.Value);

            IEnumerable<FilterOption> OrderByDateThenByAlphabet()
            {
                var dates = options.Where(x => long.TryParse(x.Value, out _));
                dates = timeFrame == TimeFrameFilter.Future
                    ? dates.OrderByDescending(x => x.Value)
                    : dates.OrderBy(x => x.Value);
                var remaining = options.Where(x => !(long.TryParse(x.Value, out _))).OrderBy(x => x.Name);

                return dates.Concat(remaining);
            }
        }

        public static IEnumerable<ProposalPage> OrderRelatedProposals(this IEnumerable<ProposalPage> proposals) =>
            proposals
                .OrderByDescending(x => x.ProposalState == ProposalState.Active && x.DeadlineType == DeadlineType.Date)
                .ThenByDescending(x => x.ProposalState == ProposalState.Active && x.DeadlineType == DeadlineType.Continuous)
                .ThenByDescending(x => x.ProposalState == ProposalState.Planned && x.DeadlineType == DeadlineType.Date)
                .ThenByDescending(x => x.ProposalState == ProposalState.Planned && x.DeadlineType == DeadlineType.Continuous)
                .ThenByDescending(x => x.ProposalState == ProposalState.Planned && x.DeadlineType == DeadlineType.NotSet);

        private static IOrderedEnumerable<ProposalBasePage> OrderByProposalState(this IEnumerable<ProposalBasePage> list)
        {
            var sortOrder = new[]
            {
                (int)ProposalState.Active,
                (int)ProposalState.Cancelled,
                (int)ProposalState.Planned,
                (int)ProposalState.Completed,
                (int)ProposalState.Deleted
            };
            return list.OrderByDescending(x => sortOrder[(int)x.ProposalState]);
        }
    }
}
