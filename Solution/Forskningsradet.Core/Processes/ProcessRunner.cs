using System;
using System.Collections.Generic;
using System.Linq;

namespace Forskningsradet.Core.Processes
{
    public class ProcessRunner<T> : IProcess where T : Context
    {
        public int StepCount => _steps.Count();
        private readonly T _context;
        private readonly List<ProcessStep<T>> _steps;

        public ProcessRunner(T context)
        {
            _context = context;
            _steps = new List<ProcessStep<T>>();
        }

        public ProcessRunner<T> AddStep(ProcessStep<T> step)
        {
            step.Context = _context;
            _steps.Add(step);
            return this;
        }

        public bool HasStep(Type stepType) =>
            _steps.Any(s => s.GetType() == stepType);

        public ProcessStep<T> GetStep(int zeroBasedIndex) =>
            0 <= zeroBasedIndex && zeroBasedIndex < StepCount
            ? _steps[zeroBasedIndex]
            : null;

        public void Run()
        {
            _context.AddDebug("Starting process");

            foreach (var step in _steps)
            {
                var stepName = step.GetType().Name;
                _context.AddDebug($"Running {stepName}.");

                step.Run();

                if (_context.Abort)
                {
                    _context.AddDebug($"Aborting process at {stepName}.");
                    return;
                }

                _context.AddDebug($"{stepName} completed.");
            }

            _context.AddDebug("Completed process");
        }
    }
}