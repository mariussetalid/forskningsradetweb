namespace Forskningsradet.Core.Processes
{
    public abstract class ProcessStep<T> where T : Context
    {
        public abstract T Context { protected get; set; }

        public abstract void Run();
    }
}