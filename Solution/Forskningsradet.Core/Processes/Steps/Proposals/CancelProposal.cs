using Forskningsradet.Core.Processes.Contexts;

namespace Forskningsradet.Core.Processes.Steps.Proposals
{
    public class CancelProposal : ProcessStep<ProposalContext>
    {
        public override ProposalContext Context { protected get; set; }

        public override void Run() => Context.CurrentPage.ProposalAmount = string.Empty;
    }
}