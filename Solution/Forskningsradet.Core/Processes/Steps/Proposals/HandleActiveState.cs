using Forskningsradet.Core.Models.ApiModels.Evurdering;
using Forskningsradet.Core.Processes.Contexts;
using Forskningsradet.Core.Repositories.Contracts;

namespace Forskningsradet.Core.Processes.Steps.Proposals
{
    public class HandleActiveState : ProcessStep<ProposalContext>
    {
        private readonly IPageRepository _pageRepository;

        public HandleActiveState(IPageRepository pageRepository) =>
            _pageRepository = pageRepository;

        public override ProposalContext Context { protected get; set; }

        public override void Run()
        {
            var state = Context.OriginalPage.ProposalState;

            if (_pageRepository.IsPublished(Context.OriginalPage))
                Context.IsPublishable = IsPlanned(state) || IsContinuousAndActive(state);

            Context.CurrentPage.IsArchivable = IsArchivable(Context.Proposal.Status);
        }

        private static bool IsPlanned(ProposalState state) =>
            state == ProposalState.Planned;

        private bool IsContinuousAndActive(ProposalState state) =>
            Context.OriginalPage.DeadlineType == DeadlineType.Continuous && state == ProposalState.Active;

        private static bool IsArchivable(ProposalState state) =>
            state == ProposalState.Active || state == ProposalState.Cancelled;
    }
}