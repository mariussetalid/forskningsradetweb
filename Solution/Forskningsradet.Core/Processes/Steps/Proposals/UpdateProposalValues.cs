using System.Linq;
using EPiServer.Core;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ApiModels.Evurdering;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Processes.Contexts;
using Forskningsradet.Core.Repositories.Contracts;

namespace Forskningsradet.Core.Processes.Steps.Proposals
{
    public class UpdateProposalValues : ProcessStep<ProposalContext>
    {
        private readonly IPageRepository _pageRepository;

        public override ProposalContext Context { protected get; set; }

        public UpdateProposalValues(IPageRepository pageRepository)
        {
            _pageRepository = pageRepository;
        }

        public override void Run()
        {
            var language = new LanguageSelector(Context.LanguageBranch).Language;
            var proposalPage = Context.CurrentPage;
            var proposal = Context.Proposal;

            UpdateValuesFor(proposalPage);

            void UpdateValuesFor(ProposalPage page)
            {
                page.Name = proposal.GetTitle(language.Name);
                page.Activity = proposal.Program?.Title;
                page.ProposalState = proposal.Status;
                page.DeadlineType = proposal.DeadlineType;
                page.Deadline = proposal.ApplicationDeadline;
                page.ProposalAmount = MapProposalAmount(proposal.Amount);
                page.StartDate = proposal.StartDate;
                page.CaseResponsible = proposal.CaseResponsible;
                page.IsRff = proposal.IsRff;
                page.ProjectOutlineRequirement = proposal.ProjectOutlineRequirement;

                if (!(proposal.ApplicationType is ApplicationType applicationType))
                    return;

                page.MinApplicationAmount = MapAmountInThousands(applicationType.MinApplicationAmount);
                page.MaxApplicationAmount = MapAmountInThousands(applicationType.MaxApplicationAmount);
                page.MinProjectLength = applicationType.MinProjectLength ?? 0;
                page.MaxProjectLength = applicationType.MaxProjectLength ?? 0;
                page.EarliestProjectPeriodStart = applicationType.EarliestStartDate;
                page.LatestProjectPeriodStart = applicationType.LatestProjectPeriodStart;
                page.LatestProjectPeriodEnd = applicationType.LatestProjectPeriodEnd;
                page.ApplicationTypeReference = GetApplicationTypeReference(applicationType, Context.LanguageBranch);
                page.SpecificApplicationTypeId = applicationType.Id.ToString();
            }
        }

        private static string MapProposalAmount(double? proposalAmount) =>
            ((proposalAmount ?? 0) * 1000).ToString(NumberFormats.LargeNumberFormat);

        private static int MapAmountInThousands(int? amount) => (amount ?? 0) * 1000;

        private PageReference GetApplicationTypeReference(ApplicationType applicationType, string languageBranch)
        {
            var root = _pageRepository.GetCurrentStartPage()?.ApplicationTypeRoot;
            var page = _pageRepository.GetPageByExternalId<ApplicationTypePage>(applicationType.ApplicationId.ToString(), root, languageBranch);

            if (page != null)
                return page.PageLink;

            var pages = _pageRepository.CreatePageForAllLanguageBranches<ApplicationTypePage>(
                root,
                applicationType.ApplicationName,
                applicationType.ApplicationNameEnglish).ToList();

            foreach (var applicationTypePage in pages)
            {
                applicationTypePage.Id = applicationType.ApplicationId.ToString();
                _pageRepository.Save(applicationTypePage);
            }

            return pages.FirstOrDefault(p => p.Language.Name == languageBranch)?.PageLink;
        }
    }
}