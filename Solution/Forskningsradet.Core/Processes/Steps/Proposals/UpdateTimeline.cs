using System;
using System.Collections.Generic;
using System.Linq;
using Forskningsradet.Core.Models.ApiModels.Evurdering;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.ContentModels.PropertyList;
using Forskningsradet.Core.Processes.Contexts;
using Forskningsradet.Core.Services;

namespace Forskningsradet.Core.Processes.Steps.Proposals
{
    public class UpdateTimeline : ProcessStep<ProposalContext>
    {
        private readonly ILocalizationProvider _localizationProvider;

        public UpdateTimeline(ILocalizationProvider localizationProvider) =>
            _localizationProvider = localizationProvider;

        public override ProposalContext Context { protected get; set; }

        public override void Run()
        {
            UpdateTimelineItem(Context.Proposal.StartDate, TimeLineItemType.StartDate);
            UpdateTimelineItem(Context.Proposal.ApplicationDeadline, TimeLineItemType.Deadline);

            if (!(Context.Proposal.ApplicationType is ApplicationType applicationType))
                return;

            UpdateTimelineItem(applicationType.EarliestStartDate, TimeLineItemType.EarliestStartDate);
            UpdateTimelineItem(applicationType.LatestProjectPeriodStart, TimeLineItemType.LatestProjectPeriodStart);
            UpdateTimelineItem(applicationType.LatestProjectPeriodEnd, TimeLineItemType.LatestProjectPeriodEnd);
        }

        private void UpdateTimelineItem(DateTime? time, TimeLineItemType type)
        {
            var timelineBlock = Context.CurrentPage.Timeline;
            if (timelineBlock.TimeLineItems is null)
            {
                timelineBlock.Title = _localizationProvider.GetLabel(
                    $"{nameof(ProposalPage)}",
                    "ApplicationProcess");
                timelineBlock.TimeLineItems = new List<TimeLineItem>();
            }

            var existingItem = timelineBlock.TimeLineItems.SingleOrDefault(x => x.Type == type);

            if (!time.HasValue)
            {
                if (existingItem != null)
                    timelineBlock.TimeLineItems.Remove(existingItem);
            }
            else
            {
                if (existingItem != null)
                {
                    existingItem.DateTimes = new List<DateTime> { time.Value };
                    // This is needed to mark the property list as modified
                    timelineBlock.TimeLineItems.Remove(existingItem);
                    timelineBlock.TimeLineItems.Add(existingItem);
                }
                else
                {
                    timelineBlock.TimeLineItems.Add(new TimeLineItem
                    {
                        DateTimes = new List<DateTime> { time.Value },
                        Type = type,
                        Title = _localizationProvider.GetLabel(
                            $"property/enum/{nameof(TimeLineItemType)}",
                            type.ToString())
                    });
                }
            }
        }
    }
}