using Forskningsradet.Core.Processes.Contexts;
using Forskningsradet.Core.Repositories.Contracts;
using Forskningsradet.Core.Services.Contracts;

namespace Forskningsradet.Core.Processes.Steps.Proposals
{
    public class PublishProposal : ProcessStep<ProposalContext>
    {
        private readonly IPageRepository _pageRepository;
        private readonly IProposalValidationService _proposalValidationService;

        public override ProposalContext Context { protected get; set; }

        public PublishProposal(IPageRepository pageRepository, IProposalValidationService proposalValidationService)
        {
            _pageRepository = pageRepository;
            _proposalValidationService = proposalValidationService;
        }

        public override void Run()
        {
            var validationResult = _proposalValidationService.Validate(Context.CurrentPage);

            if (Context.IsPublishable && !validationResult.HasErrors)
            {
                _pageRepository.Save(Context.CurrentPage);
                return;
            }

            _pageRepository.SaveAsDraft(Context.CurrentPage);
        }
    }
}