using EPiServer.Core;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Processes.Contexts;
using Forskningsradet.Core.Repositories.Contracts;
using Forskningsradet.Core.Services.Contracts;

namespace Forskningsradet.Core.Processes.Steps.Proposals
{
    public class HandleActiveDraft : ProcessStep<ProposalContext>
    {
        private readonly IProposalPageProvider _proposalPageProvider;
        private readonly IPageRepository _pageRepository;

        public HandleActiveDraft(IProposalPageProvider proposalPageProvider, IPageRepository pageRepository)
        {
            _proposalPageProvider = proposalPageProvider;
            _pageRepository = pageRepository;
        }

        public override ProposalContext Context { protected get; set; }

        public override void Run()
        {
            if (GetCurrentlyPublishedVersionOfOriginalPage() is ProposalPage publishedVersion && publishedVersion != null)
                Context.OriginalPage = publishedVersion;
            Context.CurrentPage = _proposalPageProvider.CreateProposalPageVersion(Context.OriginalPage);
        }

        private ProposalPage GetCurrentlyPublishedVersionOfOriginalPage() =>
            _pageRepository.GetCurrentlyPublishedVersion<ProposalPage>(Context.OriginalPage.ContentLink.ToReferenceWithoutVersion(), Context.LanguageBranch);
    }
}