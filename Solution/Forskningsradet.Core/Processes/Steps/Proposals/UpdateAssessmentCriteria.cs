using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using EPiServer.Core;
using Forskningsradet.Core.Models.ApiModels.Evurdering;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Processes.Contexts;
using Forskningsradet.Core.Repositories.Contracts;

namespace Forskningsradet.Core.Processes.Steps.Proposals
{
    public class UpdateAssessmentCriteria : ProcessStep<ProposalContext>
    {
        private readonly IBlockRepository _blockRepository;

        public UpdateAssessmentCriteria(IBlockRepository blockRepository) => _blockRepository = blockRepository;

        public override ProposalContext Context { protected get; set; }

        public override void Run()
        {
            if (!(Context.Proposal.ApplicationType is ApplicationType applicationType))
                return;

            var language = new LanguageSelector(Context.LanguageBranch).Language;
            UpdateAssessmentCriteriaBlocks(applicationType, language);
        }

        private void UpdateAssessmentCriteriaBlocks(ApplicationType applicationType, CultureInfo language)
        {
            var criteria = applicationType.MainCriteria?.GroupBy(x => x.Id)
               .Select(x => x.FirstOrDefault(y => y.LanguageCode == language.Name) ?? x.First())
               .ToList() ?? new List<MainCriterion>();

            Context.CurrentPage.AssessmentCriteria = _blockRepository.UpdateBlocks<AssessmentCriterionBlock, MainCriterion>(
                criteria,
                Context.CurrentPage.AssessmentCriteria ?? new ContentArea(),
                Context.CurrentPage.ContentLink,
                language,
                UpdateAssessmentBlock);
        }

        private static void UpdateAssessmentBlock(AssessmentCriterionBlock block, MainCriterion criterion)
        {
            block.Id = criterion.Id.ToString();
            block.Title = criterion.Title;
            block.CriterionText = FormatCriterionText(criterion);
        }

        private static XhtmlString FormatCriterionText(MainCriterion criterion) =>
            new XhtmlString(criterion.Description?.Replace("\n", "<br>"));
    }
}