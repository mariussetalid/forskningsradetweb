using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using EPiServer.Core;
using Forskningsradet.Core.Models.ApiModels.Evurdering;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Processes.Contexts;
using Forskningsradet.Core.Repositories.Contracts;

namespace Forskningsradet.Core.Processes.Steps.Proposals
{
    public class UpdateSubjects : ProcessStep<ProposalContext>
    {
        private readonly IBlockRepository _blockRepository;

        public override ProposalContext Context { protected get; set; }

        public UpdateSubjects(
            IBlockRepository blockRepository)
        {
            _blockRepository = blockRepository;
        }

        public override void Run()
        {
            if (!(Context.Proposal.ApplicationType is ApplicationType applicationType))
                return;

            var language = new LanguageSelector(Context.LanguageBranch).Language;
            UpdateSubjectBlocks(applicationType, language);
        }

        private void UpdateSubjectBlocks(ApplicationType applicationType, CultureInfo language)
        {
            var subjects = applicationType.Subjects?
               .Select(x =>
               {
                   return new TranslatedSubject
                   {
                       Id = x.Code,
                       Title = x.GetTranslatedName(language.Name),
                       SubjectChildren = string.Join(", ",
                           GetTranslatedSubSubjects(x, language) ?? new string[0])
                   };
               })
               .Where(x => x != null && !string.IsNullOrWhiteSpace(x.Title))
               .ToList() ?? new List<TranslatedSubject>();

            Context.CurrentPage.SubjectBlocks = _blockRepository.UpdateBlocks<SubjectBlock, TranslatedSubject>(
                subjects,
                Context.CurrentPage.SubjectBlocks ?? new ContentArea(),
                Context.CurrentPage.ContentLink,
                language,
                UpdateSubjectBlock);
        }

        private void UpdateSubjectBlock(SubjectBlock block, TranslatedSubject subject)
        {
            block.SubjectCode = subject.Id;
            block.SubjectName = subject.Title;
            block.SubjectChildren = subject.SubjectChildren;
        }

        private static IEnumerable<string> GetTranslatedSubSubjects(Subject subject, CultureInfo language) =>
            subject.SubSubjects?.Select(y => y.GetTranslatedName(language.Name))
                .Where(x => x != null);
    }
}