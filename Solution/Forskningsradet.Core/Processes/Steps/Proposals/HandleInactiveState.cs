using Forskningsradet.Core.Processes.Contexts;
using Forskningsradet.Core.Repositories.Contracts;

namespace Forskningsradet.Core.Processes.Steps.Proposals
{
    public class HandleInactiveState : ProcessStep<ProposalContext>
    {
        private readonly IPageRepository _pageRepository;

        public HandleInactiveState(IPageRepository pageRepository) =>
            _pageRepository = pageRepository;

        public override ProposalContext Context { protected get; set; }

        public override void Run()
        {
            Context.IsPublishable = Context.OriginalPage != null && _pageRepository.IsPublished(Context.OriginalPage);
            Context.CurrentPage.IsArchivable = false;
        }
    }
}