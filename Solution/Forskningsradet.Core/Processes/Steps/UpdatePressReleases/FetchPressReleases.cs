using System.Threading.Tasks;
using Forskningsradet.Core.Processes.Contexts;
using Forskningsradet.ServiceAgents.Contracts;
using Forskningsradet.ServiceAgents.ResponseModels;

namespace Forskningsradet.Core.Processes.Steps.UpdatePressReleases
{
    public class FetchPressReleases : ProcessStep<UpdatePressReleasesContext>
    {
        private readonly INtbServiceAgent _ntbServiceAgent;

        public FetchPressReleases(INtbServiceAgent ntbServiceAgent) => _ntbServiceAgent = ntbServiceAgent;

        public override UpdatePressReleasesContext Context { protected get; set; }

        public override void Run()
        {
            int? nextPage = 0;

            while (nextPage != null)
            {
                var result = GetPressReleasesAsync(nextPage.Value);
                Context.AddDebug($"Received response from NTB with {result.Releases.Count} press releases.");

                foreach (var ntbPressRelease in result.Releases)
                {
                    Context.PressReleases.Add(ntbPressRelease);
                    Context.AddDebug($"Added press release with id {ntbPressRelease.Id}.");
                }

                nextPage = result.NextPage;
            }
            Context.AddInfo($"Received press releases: {Context.PressReleases.Count}");
        }

        private NtbPressReleases GetPressReleasesAsync(int nextPage)
        {
            var task = Task.Run(async () => await _ntbServiceAgent.GetPressReleases(nextPage));
            return task.Result;
        }
    }
}