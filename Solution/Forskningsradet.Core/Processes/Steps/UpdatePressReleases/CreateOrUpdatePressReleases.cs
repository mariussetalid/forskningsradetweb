using System;
using System.Diagnostics;
using System.Linq;
using Forskningsradet.Core.Models.ApiModels.Ntb;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Processes.Contexts;
using Forskningsradet.Core.Services.Contracts;
using Forskningsradet.ServiceAgents.ResponseModels;

namespace Forskningsradet.Core.Processes.Steps.UpdatePressReleases
{
    public class CreateOrUpdatePressReleases : ProcessStep<UpdatePressReleasesContext>
    {
        private readonly IPressReleasePageProvider _pressReleasePageProvider;

        public CreateOrUpdatePressReleases(IPressReleasePageProvider pressReleasePageProvider) =>
            _pressReleasePageProvider = pressReleasePageProvider;

        public override UpdatePressReleasesContext Context { protected get; set; }

        public override void Run()
        {
            if (Context.PressReleases.Count == 0)
            {
                Context.Abort = true;
                Context.AddError("No press releases in context. Aborting, press releases will not be updated.");
            }
            var createdCount = 0;
            var changedCount = 0;
            var sw = Stopwatch.StartNew();
            foreach (var pressRelease in Context.PressReleases.Select(Map))
            {
                var page = _pressReleasePageProvider.GetPageByExternalId(pressRelease.Id);
                if (page is null)
                {
                    var newPage = CreatePage(pressRelease);
                    UpdateValuesFor(pressRelease, newPage, Context.UpdateTime);
                    _pressReleasePageProvider.Publish(newPage);
                    createdCount++;
                }
                else if (HasChanged(pressRelease, page))
                {
                    var newVersion = _pressReleasePageProvider.CreatePageVersion(page);
                    UpdateValuesFor(pressRelease, newVersion, Context.UpdateTime);
                    _pressReleasePageProvider.Publish(newVersion);
                    changedCount++;
                }
            }
            sw.Stop();
            Context.AddInfo($"Time spent creating/updating: {sw.ElapsedMilliseconds} ms");

            Context.AddInfo($"Created press releases: {createdCount}");
            Context.AddInfo($"Updated press releases: {changedCount}");

        }

        private PressReleasePage CreatePage(PressRelease pressRelease)
        {
            var newPage = _pressReleasePageProvider.CreatePage(pressRelease);
            return _pressReleasePageProvider.CreatePageVersion(newPage);
        }

        private bool HasChanged(PressRelease pressRelease, PressReleasePage currentPage) =>
            pressRelease.Title != currentPage.Title
            || pressRelease.Lead != currentPage.Lead
            || pressRelease.Body != currentPage.Body
            || pressRelease.ImageCaption != currentPage.ImageCaption
            || pressRelease.ImageUrl != currentPage.ImageUrl
            || pressRelease.ImageThumbnailUrl != currentPage.ImageThumbnailUrl
            || pressRelease.Published != currentPage.ExternalPublishedAt;

        private static PressRelease Map(NtbPressRelease ntbPressRelease) =>
            new PressRelease
            {
                Id = ntbPressRelease.Id,
                Title = ntbPressRelease.Title,
                Lead = ntbPressRelease.Lead,
                Body = ntbPressRelease.Body,
                Published = ntbPressRelease.Published,
                ImageCaption = ntbPressRelease.MainImage?.Caption,
                ImageUrl = ntbPressRelease.MainImage?.Url,
                ImageThumbnailUrl = ntbPressRelease.MainImage?.ThumbnailUrl
            };

        private static void UpdateValuesFor(PressRelease pressRelease, PressReleasePage page, DateTime updateTime)
        {
            page.Title = pressRelease.Title;
            page.Lead = pressRelease.Lead;
            page.Body = pressRelease.Body;
            page.ImageCaption = pressRelease.ImageCaption;
            page.ImageUrl = pressRelease.ImageUrl;
            page.ImageThumbnailUrl = pressRelease.ImageThumbnailUrl;
            page.ExternalPublishedAt = pressRelease.Published;
            page.UpdatedAt = updateTime;
            page.StartPublish = page.ExternalPublishedAt;
        }
    }
}