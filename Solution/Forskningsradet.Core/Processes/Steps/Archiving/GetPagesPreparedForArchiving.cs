using System.Linq;
using Forskningsradet.Core.MessageBrokers;
using Forskningsradet.Core.Processes.Contexts;

namespace Forskningsradet.Core.Processes.Steps.Archiving
{
    public class GetPagesPreparedForArchiving : ProcessStep<ArchivingContext>
    {
        private readonly IArchiveMessageBroker _broker;

        public override ArchivingContext Context { protected get; set; }

        public GetPagesPreparedForArchiving(IArchiveMessageBroker broker)
            => _broker = broker;

        public override void Run()
        {
            var pages = _broker.ReceiveMessages().ToList();
            if (!pages.Any())
            {
                Context.Abort = true;
                Context.AddInfo("No pages prepared for archiving.");
                return;
            }

            Context.ArchivePageMessages.AddRange(pages);
        }
    }
}