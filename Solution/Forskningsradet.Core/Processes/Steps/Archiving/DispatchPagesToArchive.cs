using Forskningsradet.Core.Processes.Contexts;
using Forskningsradet.ServiceAgents.Archiving;

namespace Forskningsradet.Core.Processes.Steps.Archiving
{
    public class DispatchPagesToArchive : ProcessStep<ArchivingContext>
    {
        private readonly IArchivingServiceAgent _archivingServiceAgent;

        public DispatchPagesToArchive(IArchivingServiceAgent archivingServiceAgent) =>
            _archivingServiceAgent = archivingServiceAgent;

        public override ArchivingContext Context { protected get; set; }

        public override void Run()
        {
            foreach (var message in Context.ArchivePageMessages)
            {
                var result = _archivingServiceAgent.ArchivePage(MapToArchivingRequest(message.Entity)).Result;
                if (result is null)
                {
                    Context.AddError($"{message.Entity.JournalTitle} - Archiving service returned empty response.");
                    continue;
                }

                message.Entity.IsArchived = result.Result;

                if (result.Result)
                {
                    Context.AddInfo(result.Message);
                }
                else
                {
                    Context.AddError($"{message.Entity.JournalTitle} - {result.Message}");
                    Context.InvalidSignature = HasInvalidSignature(result.Message);
                }
            }
        }

        private static bool HasInvalidSignature(string message) =>
            message?.Contains("Det finnes ingen identiteter med gidkode") ?? false;

        private static ArchivingRequest MapToArchivingRequest(ArchivePage page) =>
            new ArchivingRequest
            {
                Sak = new Sak
                {
                    Id = page.CaseId,
                    Tittel = page.CaseTitle,
                    Type = page.Type,
                    Saksansvarlig = page.Responsible,
                    Aktiviteter = !string.IsNullOrEmpty(page.Activities)
                        ? new[] { page.Activities }
                        : new string[0]
                },
                JournalPost = new JournalPost
                {
                    Tittel = page.JournalTitle
                },
                Fil = new Fil
                {
                    Filnavn = page.FileName,
                    Filinnhold = page.FileContents
                }
            };
    }
}