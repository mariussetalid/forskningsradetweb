using Forskningsradet.Common.Constants;
using Forskningsradet.Core.MessageBrokers;
using Forskningsradet.Core.Processes.Contexts;

namespace Forskningsradet.Core.Processes.Steps.Archiving
{
    public class ResendFailedMessages : ProcessStep<ArchivingContext>
    {
        private readonly IArchiveMessageBroker _broker;

        public override ArchivingContext Context { protected get; set; }

        public ResendFailedMessages(IArchiveMessageBroker broker) =>
            _broker = broker;

        public override void Run()
        {
            foreach (var message in Context.ArchivePageMessages)
            {
                if (message.Entity.IsArchived)
                {
                    _broker.CompleteMessage(message.Token);
                }
                else if (Context.InvalidSignature)
                {
                    message.Entity.Responsible = ArchiveConstants.DefaultResponsible;
                    _broker.Send(message.Entity);
                }
            }
        }
    }
}