using System;
using System.Collections.Generic;
using System.Linq;
using Forskningsradet.Core.Models.ApiModels.DWH;
using Forskningsradet.Core.Models.ContentModels.PropertyList;
using Forskningsradet.Core.Processes.Contexts;
using GradeDistribution = Forskningsradet.Core.Models.ContentModels.PropertyList.GradeDistribution;
using SubjectSpecialist = Forskningsradet.Core.Models.ContentModels.PropertyList.SubjectSpecialist;

namespace Forskningsradet.Core.Processes.Steps.ApplicationResults
{
    public class UpdateApplicationResultPage : ProcessStep<ApplicationResultContext>
    {
        public override ApplicationResultContext Context { protected get; set; }

        public override void Run()
        {
            if (Context.Page is null || Context.ApplicationResult is null)
                return;

            Context.Page.AppliedAmount = ToIntString(Context.ApplicationResult.AppliedAmount);
            Context.Page.AwardedAmount = ToIntString(Context.ApplicationResult.AwardedAmount);
            Context.Page.NumberOfApplications = Context.ApplicationResult.ReceivedApplications.ToString();

            var approved = Context.ApplicationResult.ReceivedApplications - Context.ApplicationResult.RejectedApplications;
            Context.Page.NumberOfApprovedApplications = approved.ToString();

            Context.Page.GradeDistribution = MapGradeDistribution(Context.ApplicationResult.GradeDistributions);
            Context.Page.GrantedProjects = MapGrantedProjects(Context.ApplicationResult.Applications);
            Context.Page.SubjectSpecialists = MapSubjectSpecialists(Context.ApplicationResult.SubjectSpecialists);
        }

        private static string ToIntString(double dbl)
            => Convert.ToInt64(Math.Floor(dbl)).ToString();

        private static IList<GradeDistribution> MapGradeDistribution(IEnumerable<Models.ApiModels.DWH.GradeDistribution> gradeDistributions)
            => gradeDistributions?.Select(x => new GradeDistribution
            {
                Grade = x.Grade.ToString(),
                Count = x.Count.ToString()
            }).ToList();

        private static IList<GrantedProject> MapGrantedProjects(Application[] applications)
            => applications?.Select(x => new GrantedProject
            {
                Number = x.ProjectNumber.ToString(),
                Title = x.ProjectTitle,
                Organization = x.Organization
            }).ToList();

        private static IList<SubjectSpecialist> MapSubjectSpecialists(IEnumerable<Models.ApiModels.DWH.SubjectSpecialist> subjectSpecialists)
            => subjectSpecialists?.Select(x => new SubjectSpecialist
            {
                Name = x.Name,
                Organization = x.Country
            }).ToList();
    }
}