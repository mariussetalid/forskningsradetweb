using EPiServer.Core;
using EPiServer.Shell.Configuration;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Processes.Contexts;
using Forskningsradet.Core.Repositories.Contracts;

namespace Forskningsradet.Core.Processes.Steps.ApplicationResults
{
    public class ConnectResultToProposal : ProcessStep<ApplicationResultContext>
    {
        private readonly IPageRepository _pageRepository;

        public ConnectResultToProposal(IPageRepository pageRepository)
            => _pageRepository = pageRepository;

        public override ApplicationResultContext Context { protected get; set; }

        public override void Run()
        {
            if (Context.Page is null || Context.ApplicationResult is null)
                return;

            var proposalPage = _pageRepository.GetPageByExternalId<ProposalPage>(
                Context.ApplicationResult.ProposalId.ToString(),
                GetParentPage(),
                LanguageConstants.MasterLanguageBranch);

            if (proposalPage != null && proposalPage.ApplicationResult != Context.Page.PageLink)
            {
                var writablePage = (ProposalPage)proposalPage.CreateWritableClone();
                writablePage.ApplicationResult = Context.Page.PageLink;
                if (_pageRepository.IsPublished(writablePage))
                    _pageRepository.Save(writablePage);
                else
                    _pageRepository.SaveAsDraft(writablePage);
            }
        }

        private PageReference GetParentPage()
        {
            var parentPage = _pageRepository.GetCurrentStartPage()?.ProposalsPage;
            if (PageReference.IsNullOrEmpty(parentPage))
                throw new MissingConfigurationException("Proposal page import location has not been configured.");

            return parentPage;
        }
    }
}