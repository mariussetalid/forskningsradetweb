using System.Globalization;
using EPiServer.Core;
using EPiServer.Shell.Configuration;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ApiModels.DWH;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Processes.Contexts;
using Forskningsradet.Core.Repositories.Contracts;

namespace Forskningsradet.Core.Processes.Steps.ApplicationResults
{
    public class PrepareApplicationResultUpdate : ProcessStep<ApplicationResultContext>
    {
        private readonly IPageRepository _pageRepository;

        public PrepareApplicationResultUpdate(IPageRepository pageRepository)
            => _pageRepository = pageRepository;

        public override ApplicationResultContext Context { protected get; set; }

        public override void Run()
        {
            var parentPage = GetParentPage();

            var page = _pageRepository.GetPageByExternalId<ApplicationResultPage>(
                           Context.ApplicationResult.ProposalId.ToString(),
                           parentPage,
                           LanguageConstants.MasterLanguageBranch) ?? CreatePage(parentPage, Context.ApplicationResult);

            var writableClone = page.CreateWritableClone() as ApplicationResultPage;
            Context.Page = writableClone;
        }

        private PageReference GetParentPage()
        {
            var parentPage = _pageRepository.GetCurrentStartPage()?.ApplicationResultFolder;
            if (ContentReference.IsNullOrEmpty(parentPage))
                throw new MissingConfigurationException("Application result import location has not been configured.");

            return parentPage;
        }

        private ApplicationResultPage CreatePage(PageReference parentPage, ApplicationResult result)
        {
            var page = _pageRepository.Create<ApplicationResultPage>(parentPage, CultureInfo.GetCultureInfo(LanguageConstants.MasterLanguageBranch));
            page.Id = result.ProposalId.ToString();
            page.Name = result.ProposalName;
            _pageRepository.Save(page);

            TranslatePage(page);

            return page;
        }

        private void TranslatePage(ApplicationResultPage page)
        {
            var translatedPage = _pageRepository.CreateLanguageBranch<ApplicationResultPage>(page.PageLink, LanguageConstants.EnglishLanguageBranch);
            translatedPage.Name = page.PageName;
            _pageRepository.Save(translatedPage);
        }
    }
}