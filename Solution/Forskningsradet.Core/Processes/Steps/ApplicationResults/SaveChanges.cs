using Forskningsradet.Core.Processes.Contexts;
using Forskningsradet.Core.Repositories.Contracts;

namespace Forskningsradet.Core.Processes.Steps.ApplicationResults
{
    public class SaveChanges : ProcessStep<ApplicationResultContext>
    {
        private readonly IPageRepository _pageRepository;

        public SaveChanges(IPageRepository pageRepository)
            => _pageRepository = pageRepository;

        public override ApplicationResultContext Context { protected get; set; }

        public override void Run()
        {
            if (Context.Page != null)
                _pageRepository.Save(Context.Page);
        }
    }
}