using System;
using Forskningsradet.Core.Extensions;
using Forskningsradet.Core.MessageBrokers;
using Forskningsradet.Core.Processes.Contexts;

namespace Forskningsradet.Core.Processes.Steps.PrepareForArchiving
{
    public class SavePageForArchiving : ProcessStep<PrepareForArchivingContext>
    {
        private readonly IArchiveMessageBroker _broker;

        public SavePageForArchiving(IArchiveMessageBroker broker)
            => _broker = broker;

        public override PrepareForArchivingContext Context { protected get; set; }

        public override void Run()
        {
            var page = new ArchivePage
            {
                Id = Guid.NewGuid(),
                CaseId = Context.CaseId,
                CaseTitle = Context.Title,
                Type = Context.Type.ToString(),
                Responsible = Context.CaseResponsible,
                Activities = Context.Activity,
                JournalTitle = Context.JournalTitle,
                FileName = $"{Context.Title.EnsureValidEnglishWindowsFileName()}.pdf",
                FileContents = Context.PdfContent,
                IsArchived = false
            };

            _broker.Send(page);
        }
    }
}