using Forskningsradet.Core.Processes.Contexts;
using Forskningsradet.Core.Processes.Providers;

namespace Forskningsradet.Core.Processes.Steps.PrepareForArchiving
{
    public class GenerateArchivedPagePdf : ProcessStep<PrepareForArchivingContext>
    {
        private readonly IHtmlToPdfProvider _htmlToPdfProvider;

        public GenerateArchivedPagePdf(IHtmlToPdfProvider htmlToPdfProvider) =>
            _htmlToPdfProvider = htmlToPdfProvider;

        public override PrepareForArchivingContext Context { protected get; set; }

        public override void Run()
        {
            Context.PdfContent = _htmlToPdfProvider.GetPageAsArchivablePdf(Context.Page);

            if (string.IsNullOrEmpty(Context.PdfContent))
            {
                Context.Abort = true;
                Context.AddError($"ID:{Context.Page.ContentLink?.ID} - Unable to retrieve page as PDF.");
            }
        }
    }
}