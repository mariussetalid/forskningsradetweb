using System.Globalization;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ApiModels.Evurdering;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Processes.Contexts;
using Forskningsradet.Core.Processes.Providers;
using Forskningsradet.Core.Services;

namespace Forskningsradet.Core.Processes.Steps.PrepareForArchiving
{
    public class PopulateMetaData : ProcessStep<PrepareForArchivingContext>
    {
        private readonly IEpiPageVersionProvider _pageVersionProvider;
        private readonly ILocalizationProvider _localizationProvider;

        public PopulateMetaData(IEpiPageVersionProvider pageVersionProvider, ILocalizationProvider localizationProvider)
        {
            _pageVersionProvider = pageVersionProvider;
            _localizationProvider = localizationProvider;
        }

        public override PrepareForArchivingContext Context { protected get; set; }

        public override void Run()
        {
            if (Context.Page is null)
            {
                Context.Abort = true;
                Context.AddInfo("Page is null.");
                return;
            }

            Context.Type = GetCaseType();
            Context.CaseId = GetCaseId();
            Context.Title = GetTitle();
            Context.CaseResponsible = Context.Page.CaseResponsible;
            Context.Activity = GetActivity();
            Context.JournalTitle = GetJournalTitle();
        }

        private ArchiveCaseType GetCaseType() =>
            IsProposalPage()
                ? ArchiveCaseType.U
                : ArchiveCaseType.N;

        private string GetCaseId() =>
            IsProposalPage()
                ? ((ProposalPage)Context.Page).Id
                : Context.Page.PageLink.ID.ToString();

        private string GetTitle() =>
            IsProposalPage()
                ? GetProposalTitle()
                : $"{ArchiveConstants.WebContentPrefix} {GetPageTitle()}";

        private string GetProposalTitle()
        {
            var page = (ProposalPage)Context.Page;
            return page.DeadlineType == DeadlineType.Date
                ? $"{ArchiveConstants.ProposalPrefix} {GetPageTitle()} {page.DeadlineComputed:dd.MM.yyyy}"
                : $"{ArchiveConstants.ProposalPrefix} {GetPageTitle()}";
        }

        private string GetPageTitle() =>
            string.IsNullOrEmpty(Context.Page.ArchiveTitleOverride)
                ? Context.Page.Name
                : Context.Page.ArchiveTitleOverride;

        private string GetActivity() =>
            IsProposalPage()
                ? ((ProposalPage)Context.Page).Activity
                : null;

        private bool IsProposalPage() =>
            Context.Page.GetType().BaseType == typeof(ProposalPage);

        private string GetJournalTitle()
        {
            var version = _pageVersionProvider.GetPageVersion(Context.Page);

            if (!IsProposalPage())
                return $"{GetPageTitle()} - {version}";

            var state = _localizationProvider.GetEnumName(
                ((ProposalPage)Context.Page).ProposalState,
                CultureInfo.GetCultureInfo(LanguageConstants.MasterLanguageBranch)
                );
            return $"{GetPageTitle()} - {version} - {state}";
        }
    }
}
