using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Processes.Contexts;

namespace Forskningsradet.Core.Processes.Steps.PrepareForArchiving
{
    public class ValidatePageForArchiving : ProcessStep<PrepareForArchivingContext>
    {
        public override PrepareForArchivingContext Context { protected get; set; }

        public override void Run()
        {
            if (Context.Page is null
                || string.IsNullOrEmpty(Context.Page.Name)
                || string.IsNullOrEmpty(Context.Page.CaseResponsible)
                || !HasValidId())
            {
                Context.Abort = true;
            }
        }

        private bool HasValidId()
            => !(Context.Page is ProposalPage page) || !string.IsNullOrEmpty(page.Id);
    }
}