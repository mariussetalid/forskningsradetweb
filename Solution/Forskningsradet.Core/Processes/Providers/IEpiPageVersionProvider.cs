using EPiServer.Core;

namespace Forskningsradet.Core.Processes.Providers
{
    public interface IEpiPageVersionProvider
    {
        int GetPageVersion(PageData page);
    }
}