using System.Linq;
using EPiServer.Core;

namespace Forskningsradet.Core.Processes.Providers
{
    public class EpiPageVersionProvider : IEpiPageVersionProvider
    {
        private readonly IContentVersionRepository _repository;

        public EpiPageVersionProvider(IContentVersionRepository repository) => _repository = repository;

        public int GetPageVersion(PageData page)
            => _repository.List(new VersionFilter { ContentLink = page.ContentLink }, 0, int.MaxValue, out _)
                   .OrderByDescending(x => x.Saved)
                   .FirstOrDefault(x =>
                       x.LanguageBranch == ((ILocalizable)page).Language.Name)
                   ?.ContentLink.WorkID ?? 0;
    }
}