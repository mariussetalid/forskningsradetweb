using Forskningsradet.Core.Models.ContentModels.Pages;

namespace Forskningsradet.Core.Processes.Providers
{
    public interface IHtmlToPdfProvider
    {
        string GetPageAsArchivablePdf(EditorialPage page);
        byte[] GetPageAsDownloadablePdf(BasePageData page, string queryString = null);
    }
}