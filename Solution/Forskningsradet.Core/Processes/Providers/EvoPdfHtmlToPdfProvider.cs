﻿using System;
using System.Text;
using EPiServer;
using EPiServer.Core;
using EPiServer.Web.Routing;
using EvoPdf.HtmlToPdfClient;
using Forskningsradet.Core.Configuration;
using Forskningsradet.Core.Models.ContentModels.Pages;

namespace Forskningsradet.Core.Processes.Providers
{
    public class EvoPdfHtmlToPdfProvider : IHtmlToPdfProvider
    {
        private readonly IUrlResolver _urlResolver;
        private readonly IPdfConfiguration _pdfConfiguration;
        private const PdfStandardSubset ArchivablePdfSubset = PdfStandardSubset.Pdf_A_1b;

        public EvoPdfHtmlToPdfProvider(IUrlResolver urlResolver, IPdfConfiguration pdfConfiguration)
        {
            _urlResolver = urlResolver;
            _pdfConfiguration = pdfConfiguration;
        }

        public string GetPageAsArchivablePdf(EditorialPage page)
        {
            var outPdfBuffer = GetPageAsPdf(page, ArchivablePdfSubset);
            return Convert.ToBase64String(outPdfBuffer);
        }

        public byte[] GetPageAsDownloadablePdf(BasePageData page, string queryString = null) =>
            GetPageAsPdf(page, PdfStandardSubset.Full, true, queryString);

        private byte[] GetPageAsPdf(BasePageData page, PdfStandardSubset subset, bool includeHeader = false, string queryString = null)
        {
            var absoluteUrl = GetAbsoluteUrl(page.PageLink) + (queryString ?? string.Empty);
            var serverIp = _pdfConfiguration.ServerIp;
            var serverPort = _pdfConfiguration.Port;
            var htmlToPdfConverter = new HtmlToPdfConverter(serverIp, (uint)serverPort)
            {
                // Set HTML Viewer width in pixels which is the equivalent in converter of the browser window width
                HtmlViewerWidth = _pdfConfiguration.HtmlViewerWidth,
                // Seconds to wait for javascript to complete running
                ConversionDelay = subset == ArchivablePdfSubset
                    ? _pdfConfiguration.ArchiveConversionDelay
                    : _pdfConfiguration.ConversionDelay,
                PdfDocumentOptions = {
                    PdfStandardSubset = subset,
                    LeftMargin = 40,
                    RightMargin = 40,
                    TopMargin = 20,
                    BottomMargin = 20
                },
                PdfBookmarkOptions = { AutoBookmarksEnabled = true },
                LicenseKey = _pdfConfiguration.LicenseKey,
                MediaType = "print",
            };
            if (includeHeader)
                AddHeader(htmlToPdfConverter, absoluteUrl);

            return htmlToPdfConverter.ConvertUrl(absoluteUrl);
        }

        private static void AddHeader(HtmlToPdfConverter pdfConverter, string absoluteUrl)
        {
            pdfConverter.PdfDocumentOptions.ShowHeader = true;
            pdfConverter.PdfHeaderOptions.HeaderHeight = 20;
            pdfConverter.PdfHeaderOptions.ShowInFirstPage = true;
            pdfConverter.PdfHeaderOptions.ShowInOddPages = false;
            pdfConverter.PdfHeaderOptions.ShowInEvenPages = false;

            var html = $"<a href=\"{absoluteUrl}\">{absoluteUrl}</a>";
            var headerHtml = new HtmlToPdfElement(
                0,
                0,
                0,
                pdfConverter.PdfHeaderOptions.HeaderHeight,
                html,
                absoluteUrl,
                1024,
                0)
            {
                FitHeight = true
            };

            pdfConverter.PdfHeaderOptions.AddElement(headerHtml);
        }

        private string GetAbsoluteUrl(ContentReference page)
        {
            var url = _urlResolver.GetUrl(page);
            var urlBuilder = new UrlBuilder(url);

            Global.UrlRewriteProvider.ConvertToExternal(urlBuilder, null, Encoding.UTF8);
            return UriSupport.AbsoluteUrlBySettings(urlBuilder.ToString());
        }
    }
}