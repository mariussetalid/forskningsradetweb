using System;
using System.Collections.Generic;
using System.Text;
using Forskningsradet.Common.Logging;
using Forskningsradet.ServiceAgents.ResponseModels;

namespace Forskningsradet.Core.Processes.Contexts
{
    public class UpdatePressReleasesContext : Context
    {
        public UpdatePressReleasesContext(IIntegrationLogger logger) : base(logger)
        {
            PressReleases = new List<NtbPressRelease>();
            UpdateTime = DateTime.UtcNow;
        }

        public List<NtbPressRelease> PressReleases { get; }
        public DateTime UpdateTime;

        public override string GetProgressReport()
        {
            var sb = new StringBuilder();

            if (Abort)
            {
                sb.AppendLine("Process aborted.");
                sb.AppendLine("<br>");
            }

            foreach (var message in Progress)
            {
                sb.Append("<br>");
                sb.AppendLine(message);
            }

            return sb.ToString();
        }
    }
}