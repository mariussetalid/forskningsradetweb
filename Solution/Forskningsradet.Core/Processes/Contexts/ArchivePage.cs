using System;

namespace Forskningsradet.Core.Processes.Contexts
{
    public class ArchivePage
    {
        public Guid Id { get; set; }
        public string CaseId { get; set; }
        public string CaseTitle { get; set; }
        public string Type { get; set; }
        public string Responsible { get; set; }
        public string Activities { get; set; }
        public string JournalTitle { get; set; }
        public string FileName { get; set; }
        public string FileContents { get; set; }
        public bool IsArchived { get; set; }
    }
}