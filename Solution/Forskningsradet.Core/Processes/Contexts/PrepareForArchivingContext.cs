using Forskningsradet.Common.Logging;
using Forskningsradet.Core.Models.ContentModels.Pages;

namespace Forskningsradet.Core.Processes.Contexts
{
    public class PrepareForArchivingContext : Context
    {
        public EditorialPage Page { get; set; }

        public string CaseId { get; set; }
        public string Title { get; set; }
        public ArchiveCaseType Type { get; set; }
        public string CaseResponsible { get; set; }
        public string Activity { get; set; }
        public string PdfContent { get; set; }
        public string JournalTitle { get; set; }

        public PrepareForArchivingContext(IIntegrationLogger logger)
            : base(logger) { }

        public override string GetProgressReport()
            => string.Empty;
    }
}