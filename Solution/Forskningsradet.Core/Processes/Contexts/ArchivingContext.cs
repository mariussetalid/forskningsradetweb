using System.Collections.Generic;
using System.Linq;
using System.Text;
using Forskningsradet.Common.Logging;
using Forskningsradet.Core.MessageBrokers;

namespace Forskningsradet.Core.Processes.Contexts
{
    public class ArchivingContext : Context
    {
        public List<MessageEntity<ArchivePage>> ArchivePageMessages { get; } = new List<MessageEntity<ArchivePage>>();
        public bool InvalidSignature { get; set; }

        public ArchivingContext(IIntegrationLogger logger)
            : base(logger) { }

        public override string GetProgressReport()
        {
            var sb = new StringBuilder();

            if (Abort)
            {
                sb.AppendLine("Process aborted.");
                sb.AppendLine("<br>");
            }

            var archivedPages = ArchivePageMessages.Count(x => x.Entity.IsArchived);
            sb.AppendLine($"{archivedPages} of {ArchivePageMessages.Count} pages archived.");

            foreach (var message in Progress)
            {
                sb.Append("<br>");
                sb.AppendLine(message);
            }

            return sb.ToString();
        }
    }
}