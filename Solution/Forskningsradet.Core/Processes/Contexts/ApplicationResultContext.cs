using Forskningsradet.Common.Logging;
using Forskningsradet.Core.Models.ApiModels.DWH;
using Forskningsradet.Core.Models.ContentModels.Pages;

namespace Forskningsradet.Core.Processes.Contexts
{
    public class ApplicationResultContext : Context
    {
        public ApplicationResult ApplicationResult { get; set; }
        public ApplicationResultPage Page { get; set; }

        public ApplicationResultContext(IIntegrationLogger logger)
            : base(logger) { }

        public override string GetProgressReport()
            => string.Empty;
    }
}