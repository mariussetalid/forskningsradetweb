using Forskningsradet.Common.Logging;
using Forskningsradet.Core.Models.ApiModels.Evurdering;
using Forskningsradet.Core.Models.ContentModels.Pages;

namespace Forskningsradet.Core.Processes.Contexts
{
    public class ProposalContext : Context
    {
        public ProposalContext(IIntegrationLogger logger) : base(logger)
        {
        }

        public Proposal Proposal { get; set; }
        public ProposalPage OriginalPage { get; set; }
        public ProposalPage CurrentPage { get; set; }
        public string LanguageBranch { get; set; }
        public bool IsPublishable { get; set; }

        public override string GetProgressReport() => string.Empty;
    }
}