using System;
using System.Collections.Generic;
using Forskningsradet.Common.Logging;
using Forskningsradet.Core.Logging;

namespace Forskningsradet.Core.Processes
{
    public abstract class Context
    {
        private readonly IIntegrationLogger _logger;
        private readonly Guid _id = Guid.NewGuid();

        public bool Abort { get; set; }
        public List<string> Progress { get; } = new List<string>();

        protected Context(IIntegrationLogger logger)
            => _logger = logger;

        public abstract string GetProgressReport();

        public void AddError(string message)
            => AddProgress(LogLevel.Error, message);

        public void AddInfo(string message)
            => AddProgress(LogLevel.Information, message);

        public void AddDebug(string message)
            => AddProgress(LogLevel.Debug, message);

        private void AddProgress(LogLevel level, string message)
        {
            switch (level)
            {
                case LogLevel.Error:
                    if (_logger.IsEnabled(LogLevel.Error))
                    {
                        _logger.LogError("{id} - {message}", null, _id, message);
                        Progress.Add(message);
                    }
                    break;
                case LogLevel.Information:
                    if (_logger.IsEnabled(LogLevel.Information))
                    {
                        _logger.LogInfo("{id} - {message}", _id, message);
                        Progress.Add(message);
                    }
                    break;
                case LogLevel.Debug:
                    if (_logger.IsEnabled(LogLevel.Debug))
                    {
                        _logger.LogDebug("{id} - {message}", _id, message);
                        Progress.Add(message);
                    }
                    break;
            }
        }
    }
}