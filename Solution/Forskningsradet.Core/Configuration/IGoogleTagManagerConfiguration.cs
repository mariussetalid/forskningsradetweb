﻿namespace Forskningsradet.Core.Configuration
{
    public interface IGoogleTagManagerConfiguration
    {
        string GoogleTagManagerId { get; }
    }
}
