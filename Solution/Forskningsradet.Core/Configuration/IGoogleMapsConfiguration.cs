﻿namespace Forskningsradet.Core.Configuration
{
    public interface IGoogleMapsConfiguration
    {
        string GoogleMapsApiKey { get; }
    }
}
