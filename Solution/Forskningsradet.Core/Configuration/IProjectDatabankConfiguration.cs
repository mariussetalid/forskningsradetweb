﻿namespace Forskningsradet.Core.Configuration
{
    public interface IProjectDatabankConfiguration
    {
        string ProjectDatabankSearchUrl { get; }
    }
}