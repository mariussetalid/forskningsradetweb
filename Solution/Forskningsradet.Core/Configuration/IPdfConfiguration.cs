namespace Forskningsradet.Core.Configuration
{
    public interface IPdfConfiguration
    {
        string ServerIp { get; }
        int Port { get; }
        int HtmlViewerWidth { get; }
        int ConversionDelay { get; }
        int ArchiveConversionDelay { get; }
        string LicenseKey { get; }
    }
}