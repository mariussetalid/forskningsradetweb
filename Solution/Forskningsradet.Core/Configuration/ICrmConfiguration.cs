namespace Forskningsradet.Core.Configuration
{
    public interface ICrmConfiguration
    {
        string DynamicsAppId { get; }
        string DynamicsSecret { get; }
        string DynamicsUrl { get; }
    }
}