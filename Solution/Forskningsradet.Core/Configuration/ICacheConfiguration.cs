﻿namespace Forskningsradet.Core.Configuration
{
    public interface ICacheConfiguration
    {
        int? CacheEmployeeLettersInDays { get; }
    }
}