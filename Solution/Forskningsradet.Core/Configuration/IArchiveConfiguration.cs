namespace Forskningsradet.Core.Configuration
{
    public interface IArchiveConfiguration
    {
        string QueueEndpoint { get; }
        string ArchivingQueueName { get; }
    }
}