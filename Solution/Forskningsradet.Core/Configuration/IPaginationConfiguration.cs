namespace Forskningsradet.Core.Configuration
{
    public interface IPaginationConfiguration
    {
        int? EmployeeListPaginationSize { get; }
    }
}