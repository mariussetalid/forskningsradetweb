﻿using System;
using System.Collections.Generic;
using System.Globalization;
using EPiServer.Core;
using Forskningsradet.Core.Models.ContentModels.Pages;

namespace Forskningsradet.Core.Repositories.Contracts
{
    public interface IPageRepository
    {
        T Create<T>(ContentReference parentLink, CultureInfo language) where T : PageData;
        T CreateLanguageBranch<T>(ContentReference page, string languageBranch) where T : PageData;
        IEnumerable<T> CreatePageForAllLanguageBranches<T>(ContentReference parent, string pageName, string englishPageName = null) where T : PageData;
        void Save(PageData page);
        void SaveAsDraft(PageData page);
        void UnPublish(PageData page);
        void UnPublishAllLanguageBranches<T>(ContentReference contentLink) where T : PageData;
        bool IsPublished(PageData page);
        List<T> GetPublishedDescendants<T>(PageReference pageReference) where T : PageData;
        T GetClosestPage<T>(PageReference currentPageReference) where T : PageData;
        FrontPageBase GetClosestFrontPage(PageReference currentPageReference);
        FrontPageBase GetCurrentStartPageAsFrontPageBase();
        FrontPage GetCurrentStartPage();
        FrontPage GetStartPageFromWildcardHost();
        T GetPageByExternalId<T>(string id, PageReference parentLink, string languageBranch) where T : PageData;
        T GetCurrentlyPublishedVersion<T>(ContentReference contentReference, string languageBranch) where T : PageData;
        FolderPage GetOrCreateFolder(PageReference parent, string folderName, string languageBranch);
        IEnumerable<T> FilterPublished<T>(IEnumerable<T> pages) where T : PageData;
    }
}
