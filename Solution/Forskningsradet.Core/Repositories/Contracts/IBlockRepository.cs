using System;
using System.Collections.Generic;
using System.Globalization;
using EPiServer.Core;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.Contracts;

namespace Forskningsradet.Core.Repositories.Contracts
{
    public interface IBlockRepository
    {
        T GetWritableBlock<T>(ContentReference contentReference, CultureInfo language) where T : BaseBlockData;
        T CreateBlock<T>(ContentReference parentPage, CultureInfo language) where T : IContentData;
        void SaveBlock(BlockData block, string name);
        void AddBlockToContentArea(ContentArea contentArea, BaseBlockData block);
        void RemoveBlocksFromContentArea(ContentArea contentArea, IEnumerable<ContentAreaItem> items);
        ContentArea UpdateBlocks<T, TU>(
                IReadOnlyCollection<TU> source,
                ContentArea contentArea,
                ContentReference parentPage,
                CultureInfo language,
                Action<T, TU> setBlockProperties)
            where T : BaseBlockData, IIdentifiable
            where TU : IBlockDataSource;
    }
}