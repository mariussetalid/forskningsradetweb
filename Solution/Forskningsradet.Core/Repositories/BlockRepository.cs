using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAccess;
using EPiServer.Security;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.Contracts;
using Forskningsradet.Core.Repositories.Contracts;

namespace Forskningsradet.Core.Repositories
{
    public class BlockRepository : IBlockRepository
    {
        private readonly IContentRepository _contentRepository;
        private readonly IContentLoader _contentLoader;
        private readonly ContentAssetHelper _contentAssetHelper;

        public BlockRepository(IContentRepository contentRepository, IContentLoader contentLoader, ContentAssetHelper contentAssetHelper)
        {
            _contentRepository = contentRepository;
            _contentLoader = contentLoader;
            _contentAssetHelper = contentAssetHelper;
        }

        public T GetWritableBlock<T>(ContentReference contentReference, CultureInfo language) where T : BaseBlockData
        {
            var block = _contentLoader.Get<T>(contentReference, language);
            if (block is null)
            {
                var masterBlock = _contentLoader.Get<T>(contentReference, new CultureInfo(LanguageConstants.MasterLanguageBranch));
                if (masterBlock != null)
                    block = _contentRepository.CreateLanguageBranch<T>(contentReference, language);
            }

            return (T)block?.CreateWritableClone();
        }

        public T CreateBlock<T>(ContentReference parentPage, CultureInfo language) where T : IContentData
        {
            var assetFolder = _contentAssetHelper.GetOrCreateAssetFolder(parentPage);
            return _contentRepository.GetDefault<T>(assetFolder.ContentLink, language);
        }

        public void SaveBlock(BlockData block, string name)
        {
            var contentBlock = block as IContent;
            if (contentBlock != null)
            {
                contentBlock.Name = name;
                _contentRepository.Save(contentBlock, SaveAction.Publish, AccessLevel.NoAccess);
            }
        }

        public ContentArea UpdateBlocks<T, TU>(
                IReadOnlyCollection<TU> source,
                ContentArea contentArea,
                ContentReference parentPage,
                CultureInfo language,
                Action<T, TU> setBlockProperties)
            where T : BaseBlockData, IIdentifiable
            where TU : IBlockDataSource
        {
            var existing = new List<string>();
            var removed = new List<ContentAreaItem>();
            foreach (var item in contentArea.Items)
            {
                var block = GetWritableBlock<T>(item.ContentLink, language);
                var match = source.FirstOrDefault(x => x.Identity == block.Id);

                if (match != null)
                {
                    existing.Add(block.Id);
                    setBlockProperties(block, match);
                    SaveBlock(block, match.Title);
                }
                else
                {
                    removed.Add(item);
                }
            }

            RemoveBlocksFromContentArea(contentArea, removed);
            var missing = source.Where(x => !existing.Exists(y => y == x.Identity));

            foreach (var item in missing)
            {
                var block = CreateBlock<T>(parentPage, language);
                setBlockProperties(block, item);

                SaveBlock(block, item.Title);
                AddBlockToContentArea(contentArea, block);
            }

            SortItems<T, TU>(contentArea, source.ToList());

            return contentArea;
        }

        private void SortItems<T, TU>(ContentArea contentArea, List<TU> source)
            where T : BaseBlockData, IIdentifiable
            where TU : IBlockDataSource
        {
            var orderedItems = contentArea.Items
                .OrderBy(x => GetSortIndex<T, TU>(source, x))
                .ToList();

            contentArea.Items.Clear();
            foreach (var orderedItem in orderedItems)
            {
                contentArea.Items.Add(orderedItem);
            }
        }

        private int GetSortIndex<T, TU>(List<TU> source, ContentAreaItem x)
            where T : BaseBlockData, IIdentifiable
            where TU : IBlockDataSource
        {
            var block = _contentLoader.Get<T>(x.ContentLink);
            var sourceItem = source.FirstOrDefault(y => y.Identity == block.Id);
            return sourceItem != null
                ? source.IndexOf(sourceItem)
                : int.MaxValue;
        }

        public void AddBlockToContentArea(ContentArea contentArea, BaseBlockData block)
        {
            contentArea.Items.Add(new ContentAreaItem
            {
                ContentLink = ((IContent)block).ContentLink
            });
        }

        public void RemoveBlocksFromContentArea(ContentArea contentArea, IEnumerable<ContentAreaItem> items)
        {
            foreach (var item in items)
            {
                contentArea.Items.Remove(item);
            }
        }
    }
}