﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAccess;
using EPiServer.Filters;
using EPiServer.Logging;
using EPiServer.Security;
using EPiServer.Web;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Repositories.Contracts;

namespace Forskningsradet.Core.Repositories
{
    public class PageRepository : IPageRepository
    {
        private static readonly ILogger Log = LogManager.GetLogger();

        private readonly IContentRepository _contentRepository;
        private readonly IPublishedStateAssessor _publishedStateAssessor;
        private readonly IPageCriteriaQueryService _pageCriteriaQueryService;
        private readonly IContentVersionRepository _contentVersionRepository;
        private readonly ISiteDefinitionRepository _siteDefinitionRepository;

        public PageRepository(
            IContentRepository contentRepository,
            IPublishedStateAssessor publishedStateAssessor,
            IPageCriteriaQueryService pageCriteriaQueryService,
            IContentVersionRepository contentVersionRepository,
            ISiteDefinitionRepository siteDefinitionRepository)
        {
            _contentRepository = contentRepository;
            _publishedStateAssessor = publishedStateAssessor;
            _pageCriteriaQueryService = pageCriteriaQueryService;
            _contentVersionRepository = contentVersionRepository;
            _siteDefinitionRepository = siteDefinitionRepository;
        }

        public FrontPage GetCurrentStartPage()
            => GetCurrentStartPageAsFrontPageBase() as FrontPage;

        public FrontPageBase GetCurrentStartPageAsFrontPageBase()
            => ContentReference.IsNullOrEmpty(ContentReference.StartPage)
                ? null
                : _contentRepository.Get<FrontPageBase>(ContentReference.StartPage);

        public FrontPage GetStartPageFromWildcardHost()
        {
            var wildcardHostStartPageReference = _siteDefinitionRepository
                            .List()
                            .FirstOrDefault(x => x.Hosts.Any(h => h.IsWildcardHost()))
                            ?.StartPage;
            if (!ContentReference.IsNullOrEmpty(wildcardHostStartPageReference)
                && _contentRepository.Get<FrontPageBase>(wildcardHostStartPageReference) is FrontPage wildcardStartPage)
            {
                Log.Warning("Wildcard site FrontPage is used.");
                return wildcardStartPage;
            }

            return null;
        }

        public IEnumerable<T> FilterPublished<T>(IEnumerable<T> pages) where T : PageData
            => pages.Filter(new FilterPublished()).OfType<T>();

        public FolderPage GetOrCreateFolder(PageReference parent, string folderName, string languageBranch)
        {
            var folder = _contentRepository
                .GetChildren<FolderPage>(parent, new CultureInfo(languageBranch))
                .FirstOrDefault(x => x.PageName == folderName);

            if (folder != null)
                return folder;

            return CreatePageForAllLanguageBranches<FolderPage>(parent, folderName)
                .FirstOrDefault(x => x.Language.Name == languageBranch);
        }

        public T GetClosestPage<T>(PageReference currentPageReference) where T : PageData
        {
            if (currentPageReference == ContentReference.EmptyReference)
                return null;
            if (!_contentRepository.TryGet(currentPageReference, out PageData currentPage))
                return null;

            return currentPage as T ?? GetClosestPage<T>(currentPage.ParentLink);
        }

        public FrontPageBase GetClosestFrontPage(PageReference currentPageReference)
        {
            if (currentPageReference == ContentReference.EmptyReference)
                return GetCurrentStartPageAsFrontPageBase() ?? GetStartPageFromWildcardHost();
            var currentPage = _contentRepository.Get<PageData>(currentPageReference);
            return currentPage as FrontPageBase ?? GetClosestFrontPage(currentPage.ParentLink);
        }

        public T Create<T>(ContentReference parentLink, CultureInfo language) where T : PageData
            => _contentRepository.GetDefault<T>(parentLink, language);

        public T CreateLanguageBranch<T>(ContentReference page, string languageBranch) where T : PageData
            => _contentRepository.CreateLanguageBranch<T>(page, new LanguageSelector(languageBranch));

        public IEnumerable<T> CreatePageForAllLanguageBranches<T>(ContentReference parent, string pageName, string englishPageName = null) where T : PageData
        {
            var master = Create<T>(parent, new CultureInfo(LanguageConstants.NorwegianLanguageBranch));
            master.PageName = pageName;
            Save(master);

            var translated = CreateLanguageBranch<T>(master.ContentLink, LanguageConstants.EnglishLanguageBranch);
            translated.PageName = !string.IsNullOrEmpty(englishPageName) ? englishPageName : pageName;
            Save(translated);

            return new List<T> { master, translated };
        }

        public T GetPageByExternalId<T>(string id, PageReference parentLink, string languageBranch) where T : PageData
        {
            var criterias = new PropertyCriteriaCollection
            {
                new PropertyCriteria
                {
                    Name = "Id",
                    Type = PropertyDataType.String,
                    Condition = CompareCondition.Equal,
                    Value = id
                }
            };

            var page = _pageCriteriaQueryService.FindAllPagesWithCriteria(
                    parentLink,
                    criterias,
                    languageBranch,
                    new LanguageSelector(languageBranch))
                .OfType<T>()
                .SingleOrDefault();

            if (page is null)
                return null;

            var versions = _contentVersionRepository.List(page.PageLink);
            var currentVersion = versions
                .Where(x => x.LanguageBranch == languageBranch)
                .OrderByDescending(x => x.Saved)
                .First();

            return _contentRepository.Get<T>(currentVersion.ContentLink);
        }

        public T GetCurrentlyPublishedVersion<T>(ContentReference contentReference, string languageBranch) where T : PageData
        {
            var versions = _contentVersionRepository.List(contentReference);
            var currentVersion = versions
                .Where(x => x.LanguageBranch == languageBranch)
                .Where(x => x.Status == VersionStatus.Published)
                .FirstOrDefault();

            return currentVersion is null
                ? null
                : _contentRepository.Get<T>(currentVersion.ContentLink);
        }

        public void Save(PageData page)
            => _contentRepository.Save(page, SaveAction.Publish, AccessLevel.NoAccess);

        public void SaveAsDraft(PageData page)
            => _contentRepository.Save(page, SaveAction.Default, AccessLevel.NoAccess);

        public void UnPublish(PageData page)
        {
            var writableClone = page.CreateWritableClone();
            writableClone.StopPublish = DateTime.Now;
            Save(writableClone);
        }

        public void UnPublishAllLanguageBranches<T>(ContentReference contentLink) where T : PageData
        {
            var branches = _contentRepository.GetLanguageBranches<T>(contentLink);

            foreach (var branch in branches)
            {
                var writableClone = branch.CreateWritableClone();
                writableClone.StopPublish = DateTime.Now;
                Save(writableClone);
            }
        }

        public List<T> GetPublishedDescendants<T>(PageReference pageReference) where T : PageData
            => _contentRepository
                .GetDescendents(pageReference)
                .Select(x => _contentRepository.Get<T>(x))
                .Where(IsPublished)
                .ToList();

        public bool IsPublished(PageData page)
            => _publishedStateAssessor.IsPublished(page);
    }
}