﻿using EPiServer.Core;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.QueryParameters;
using Forskningsradet.Core.Models.ViewModels.Blocks;
using Forskningsradet.Core.Models.ViewModels.Contracts;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.ViewModelMappers.Partials
{
    public class PressReleasePageViewModelMapper : BasePartialViewModelMapperWithQuery<PressReleasePage>, IPartialViewModelMapper
    {
        private readonly IArticleBlockReactModelBuilder _articleBlockReactModelBuilder;

        public PressReleasePageViewModelMapper(IArticleBlockReactModelBuilder articleBlockReactModelBuilder) =>
            _articleBlockReactModelBuilder = articleBlockReactModelBuilder;

        protected override IReactViewModel GetPartialViewModelInternal(PressReleasePage currentContent, QueryParameterBase queryParameterBase) =>
            new ReactPartialViewModel(currentContent.PartialReactComponentName(), BuildReactModel(currentContent, queryParameterBase as RenderOptionQueryParameter ?? new RenderOptionQueryParameter()));

        public IReactViewModel GetPartialViewModel(IContentData currentContent) =>
            currentContent is PressReleasePage content
                ? GetPartialViewModelInternal(content, null)
                : null;

        private ReactModels.ReactComponent BuildReactModel(PressReleasePage currentContent, RenderOptionQueryParameter renderOption) =>
            _articleBlockReactModelBuilder.BuildReactModel(currentContent, renderOption.RenderWidthOption);
    }
}
