﻿using EPiServer.Core;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.QueryParameters;
using Forskningsradet.Core.Models.ViewModels.Blocks;
using Forskningsradet.Core.Models.ViewModels.Contracts;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.ViewModelMappers.Partials
{
    public abstract class ArticlePageViewModelMapper : BasePartialViewModelMapperWithQuery<ArticlePage>, IPartialViewModelMapper
    {
        private readonly IArticleBlockReactModelBuilder _articleBlockReactModelBuilder;

        protected ArticlePageViewModelMapper(IArticleBlockReactModelBuilder articleBlockReactModelBuilder) =>
            _articleBlockReactModelBuilder = articleBlockReactModelBuilder;

        protected override IReactViewModel GetPartialViewModelInternal(ArticlePage currentContent, QueryParameterBase queryParameterBase) =>
            new ReactPartialViewModel(currentContent.PartialReactComponentName(), BuildReactModel(currentContent, queryParameterBase as RenderOptionQueryParameter ?? new RenderOptionQueryParameter()));

        public IReactViewModel GetPartialViewModel(IContentData currentContent) =>
            currentContent is ArticlePage content
                ? GetPartialViewModelInternal(content, null)
                : null;

        private ReactModels.ReactComponent BuildReactModel(ArticlePage currentContent, RenderOptionQueryParameter renderOption) =>
            _articleBlockReactModelBuilder.BuildReactModel(currentContent, renderOption.RenderWidthOption);
    }
}
