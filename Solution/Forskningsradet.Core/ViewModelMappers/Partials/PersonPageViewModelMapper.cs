﻿using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.QueryParameters;
using Forskningsradet.Core.Models.ViewModels.Blocks;
using Forskningsradet.Core.Models.ViewModels.Contracts;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.ViewModelMappers.Partials
{
    public class PersonPageViewModelMapper : BasePartialViewModelMapperWithQuery<PersonPage>
    {
        private readonly IContactInfoReactModelBuilder _contactInfoReactModelBuilder;

        public PersonPageViewModelMapper(IContactInfoReactModelBuilder contactInfoReactModelBuilder) =>
            _contactInfoReactModelBuilder = contactInfoReactModelBuilder;

        protected override IReactViewModel GetPartialViewModelInternal(PersonPage page, QueryParameterBase parameters)
        {
            var viewModel = BuildPartialReactModel(page, parameters as PersonPageQueryParameter ?? new PersonPageQueryParameter());
            return new ReactPartialViewModel(page.ReactComponentName(), viewModel);
        }

        private ReactModels.ReactComponent BuildPartialReactModel(PersonPage page, PersonPageQueryParameter parameters) =>
            _contactInfoReactModelBuilder.BuildReactModel(page, null, parameters.ShowPhoneNumber, true);
    }
}
