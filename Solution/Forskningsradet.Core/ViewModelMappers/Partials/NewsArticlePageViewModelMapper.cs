﻿using Forskningsradet.Core.Builders.Contracts;

namespace Forskningsradet.Core.ViewModelMappers.Partials
{
    public class NewsArticlePageViewModelMapper : ArticlePageViewModelMapper
    {
        public NewsArticlePageViewModelMapper(IArticleBlockReactModelBuilder articleBlockReactModelBuilder) : base(articleBlockReactModelBuilder)
        {
        }
    }
}
