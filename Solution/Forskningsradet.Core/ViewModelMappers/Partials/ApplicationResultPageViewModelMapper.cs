using System.Collections.Generic;
using System.Linq;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.ContentModels.PropertyList;
using Forskningsradet.Core.Models.ReactModels;
using Forskningsradet.Core.Models.ViewModels.Blocks;
using Forskningsradet.Core.Models.ViewModels.Contracts;
using Forskningsradet.Core.Services;

namespace Forskningsradet.Core.ViewModelMappers.Partials
{
    public class ApplicationResultPageViewModelMapper : BasePartialViewModelMapper<ApplicationResultPage>
    {
        private readonly ILocalizationProvider _localizationProvider;

        public ApplicationResultPageViewModelMapper(ILocalizationProvider localizationProvider)
            => _localizationProvider = localizationProvider;

        protected override IReactViewModel GetViewModelForPartialInternal(ApplicationResultPage page)
            => new ReactPartialViewModel(page.ReactComponentName(), BuildReactComponent(page));

        private ReactComponent BuildReactComponent(ApplicationResultPage page)
            => new DescriptionListAndTables
            {
                DescriptionList = BuildDescriptionList(page),
                Tables = BuildTables(page).ToList()
            };

        private DescriptionListBlock BuildDescriptionList(ApplicationResultPage page)
            => new DescriptionListBlock
            {
                Items = BuildItems(page)
            };

        private IList<DescriptionListBlock_Items> BuildItems(ApplicationResultPage page)
            => new List<DescriptionListBlock_Items>
            {
                BuildItem(GetLabel("AppliedAmount"), page.AppliedAmount),
                BuildItem(GetLabel("AwardedAmount"), page.AwardedAmount),
                BuildItem(GetLabel("NumberOfApplications"), page.NumberOfApplications),
                BuildItem(GetLabel("NumberOfApprovedApplications"), page.NumberOfApprovedApplications)
            };

        private static DescriptionListBlock_Items BuildItem(string label, string text)
            => new DescriptionListBlock_Items
            {
                Label = label,
                Text = text
            };

        private IEnumerable<TableBlock> BuildTables(ApplicationResultPage page)
        {
            if (page.GradeDistribution != null)
                yield return BuildGradeDistribution(page.GradeDistribution);
            if (page.GrantedProjects != null)
                yield return BuildGrantedProjects(page.GrantedProjects);
            if (page.SubjectSpecialists != null)
                yield return BuildSubjectSpecialists(page.SubjectSpecialists);
        }

        private TableBlock BuildGradeDistribution(IEnumerable<GradeDistribution> gradeDistribution)
        {
            var sortedGrades = gradeDistribution
                .OrderBy(x => x.Grade)
                .ToList();

            var headers = new List<string> { GetLabel("Grade") };
            headers.AddRange(sortedGrades.Select(x => x.Grade));

            var row = new List<string> { GetLabel("Project") };
            row.AddRange(sortedGrades.Select(x => x.Count));

            return BuildTableBlock(
                GetLabel("GradeDistribution"),
                headers,
                new List<IList<string>> { row });
        }

        private TableBlock BuildGrantedProjects(IEnumerable<GrantedProject> grantedProjects)
        {
            var title = GetLabel("GrantedProjects");
            var headers = new[]
            {
                GetLabel("ProjectNumber"),
                GetLabel("ProjectTitle"),
                GetLabel("ProjectOrganization")
            };
            var rows = new List<IList<string>>();
            rows.AddRange(grantedProjects.Select(x => new List<string> { x.Number, x.Title, x.Organization }));
            return BuildTableBlock(title, headers, rows);
        }

        private TableBlock BuildSubjectSpecialists(IEnumerable<SubjectSpecialist> subjectSpecialists)
        {
            var title = GetLabel("SubjectSpecialists");
            var headers = new[]
            {
                GetLabel("Name"),
                GetLabel("Organization")
            };
            var rows = new List<IList<string>>();
            rows.AddRange(subjectSpecialists.Select(x => new List<string> { x.Name, x.Organization }));
            return BuildTableBlock(title, headers, rows);
        }

        private static TableBlock BuildTableBlock(string title, IList<string> headers, IList<IList<string>> rows)
            => new TableBlock
            {
                Title = title,
                Table = new Table
                {
                    Header = headers,
                    Rows = rows
                }
            };

        private string GetLabel(string key)
            => _localizationProvider.GetLabel(nameof(ApplicationResultPage), key);
    }
}