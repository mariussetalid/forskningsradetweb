﻿using Forskningsradet.Core.Builders.Contracts;

namespace Forskningsradet.Core.ViewModelMappers.Partials
{
    public class InformationArticlePageViewModelMapper : ArticlePageViewModelMapper
    {
        public InformationArticlePageViewModelMapper(IArticleBlockReactModelBuilder articleBlockReactModelBuilder) : base(articleBlockReactModelBuilder)
        {
        }
    }
}
