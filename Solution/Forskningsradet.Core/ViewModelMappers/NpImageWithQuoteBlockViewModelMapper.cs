﻿using EPiServer.Core;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ViewModels.Blocks;
using Forskningsradet.Core.Models.ViewModels.Contracts;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.ViewModelMappers
{
    public class NpImageWithQuoteBlockViewModelMapper : BasePartialViewModelMapper<NpImageWithQuoteBlock>
    {
        private readonly IQuotesBlockReactModelBuilder _quotesBlockReactModelBuilder;

        public NpImageWithQuoteBlockViewModelMapper(IQuotesBlockReactModelBuilder quotesBlockReactModelBuilder)
        {
            _quotesBlockReactModelBuilder = quotesBlockReactModelBuilder;
        }

        protected override IReactViewModel GetViewModelForPartialInternal(NpImageWithQuoteBlock currentBlock) => 
            new ReactPartialViewModel(currentBlock.ReactComponentName(), BuildReactComponent(currentBlock));

        private ReactModels.ReactComponent BuildReactComponent(NpImageWithQuoteBlock currentBlock)
        {
            var quotes = new ContentArea();
            quotes.Items.Add(new ContentAreaItem
            {
                ContentLink = ((IContent)currentBlock).ContentLink
            });

            var quoteListBlock = new NpQuotesBlock
            {
                Title = null,
                Quotes = quotes
            };

            return _quotesBlockReactModelBuilder.BuildReactModel(quoteListBlock);
        }    
    }
}
