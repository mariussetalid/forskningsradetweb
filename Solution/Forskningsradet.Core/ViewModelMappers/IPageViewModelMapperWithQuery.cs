﻿using EPiServer.Core;
using Forskningsradet.Core.Models.QueryParameters;
using Forskningsradet.Core.Models.ViewModels.Contracts;

namespace Forskningsradet.Core.ViewModelMappers
{
    public interface IPageViewModelMapperWithQuery
    {
        IReactViewModel GetPageViewModel(IContentData currentContent, QueryParameterBase parameters);
    }
}