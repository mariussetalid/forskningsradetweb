﻿using System;
using System.Collections.Generic;
using System.Linq;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.Web.Routing;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Extensions;
using Forskningsradet.Core.Facades.Contracts;
using Forskningsradet.Core.Models.ApiModels.Evurdering;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.QueryParameters;
using Forskningsradet.Core.Models.RequestModels;
using Forskningsradet.Core.Models.ViewModels.Blocks;
using Forskningsradet.Core.Models.ViewModels.Contracts;
using Forskningsradet.Core.Repositories.Contracts;
using Forskningsradet.Core.Services;
using Forskningsradet.Core.Services.Contracts;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.ViewModelMappers.Blocks
{
    public class RelatedProposalListBlockViewModelMapper : BasePartialViewModelMapperWithQuery<RelatedProposalListBlock>, IPartialViewModelMapper
    {
        private const string NumberFormat = "N0";

        private readonly IUrlResolver _urlResolver;
        private readonly IContentListService _contentListService;
        private readonly IContentTypeRepository<PageType> _pageTypeRepository;
        private readonly CategoryRepository _categoryRepository;
        private readonly IPageRepository _pageRepository;
        private readonly ILocalizationProvider _localizationProvider;
        private readonly ICategoryLocalizationProvider _categoryLocalizationProvider;
        private readonly IPageEditingAdapter _pageEditingAdapter;
        private readonly IRichTextReactModelBuilder _richTextReactModelBuilder;
        private readonly IMessageReactModelBuilder _messageReactModelBuilder;
        private readonly IDateCardDatesReactModelBuilder _dateCardDatesReactModelBuilder;
        private readonly IDateCardStatusHandler _dateCardStatusHandler;

        public RelatedProposalListBlockViewModelMapper(
            IUrlResolver urlResolver,
            IContentListService contentListService,
            IContentTypeRepository<PageType> pageTypeRepository,
            CategoryRepository categoryRepository,
            IPageRepository pageRepository,
            ILocalizationProvider localizationProvider,
            ICategoryLocalizationProvider categoryLocalizationProvider,
            IPageEditingAdapter pageEditingAdapter,
            IRichTextReactModelBuilder richTextReactModelBuilder,
            IMessageReactModelBuilder messageReactModelBuilder,
            IDateCardDatesReactModelBuilder dateCardDatesReactModelBuilder,
            IDateCardStatusHandler dateCardStatusHandler)
        {
            _urlResolver = urlResolver;
            _contentListService = contentListService;
            _pageTypeRepository = pageTypeRepository;
            _categoryRepository = categoryRepository;
            _pageRepository = pageRepository;
            _localizationProvider = localizationProvider;
            _categoryLocalizationProvider = categoryLocalizationProvider;
            _pageEditingAdapter = pageEditingAdapter;
            _richTextReactModelBuilder = richTextReactModelBuilder;
            _messageReactModelBuilder = messageReactModelBuilder;
            _dateCardDatesReactModelBuilder = dateCardDatesReactModelBuilder;
            _dateCardStatusHandler = dateCardStatusHandler;
        }

        protected override IReactViewModel GetPartialViewModelInternal(RelatedProposalListBlock currentContent, QueryParameterBase queryParameterBase)
        {
            var viewModel = BuildReactComponent(currentContent, queryParameterBase as RenderOptionQueryParameter ?? new RenderOptionQueryParameter());
            return new ReactPartialViewModel(currentContent.ReactComponentName(), viewModel);
        }

        public IReactViewModel GetPartialViewModel(IContentData currentContent) =>
            currentContent is RelatedProposalListBlock content
                ? GetPartialViewModelInternal(content, null)
                : null;

        private ReactModels.ReactComponent BuildReactComponent(RelatedProposalListBlock currentBlock, RenderOptionQueryParameter renderOption)
        {
            var proposals = GetProposals(BuildProposalListRequest(currentBlock));
            return new ReactModels.RelatedDates
            {
                RelatedContent = BuildRelatedContent(currentBlock),
                Dates = proposals.Select(proposalPage => MapProposal(proposalPage, renderOption)).ToList(),
                UsedInSidebar = renderOption.RenderWidthOption == RenderWidthOption.IsInSidebar
            };
        }

        private ReactModels.RelatedContent BuildRelatedContent(RelatedProposalListBlock block) =>
            new ReactModels.RelatedContent
            {
                OnPageEditing = _pageEditingAdapter.PageIsInEditMode()
                    ? new ReactModels.RelatedContent_OnPageEditing
                    {
                        Title = nameof(block.Title)
                    }
                    : null,
                EmptyList = BuildEmptyList(),
                Title = block.Title,
                Link = block.Button?.Link is null && !_pageEditingAdapter.PageIsInEditMode()
                    ? null
                    : new ReactModels.Link
                    {
                        OnPageEditing = _pageEditingAdapter.PageIsInEditMode()
                            ? nameof(block.Button)
                            : null,
                        Text = block.Button?.Text ?? GetLabel("GoToListButton"),
                        Url = BuildUrlWithQuery(block),
                        Id = block.Button?.AnchorId.EnsureValidHtmlId()
                    },
                Border = ReactModels.RelatedContent_Border.Bottom
            };

        private ReactModels.DateCard MapProposal(ProposalPage page, RenderOptionQueryParameter renderOption) =>
            new ReactModels.DateCard
            {
                Id = page.Id,
                MessageList = null,
                Title = page.GetListTitle(),
                Url = _urlResolver.GetUrl(page),
                Text = null,
                Status = renderOption.RenderWidthOption == RenderWidthOption.IsInSidebar ? null : BuildStatus(page),
                DateContainer = _dateCardDatesReactModelBuilder.BuildReactModel(page, GetLabel("Deadline"), GetDateDescription(page.DeadlineType)),
                Metadata = BuildProposalMetaData(page),
                Tags = BuildTags(page.Category, ListPageConstants.NumberOfVisibleTags),
                UsedInSidebar = renderOption.RenderWidthOption == RenderWidthOption.IsInSidebar
            };

        private string GetDateDescription(DeadlineType deadlineType)
        {
            if (deadlineType == DeadlineType.NotSet)
                return GetLabel("NotSetDeadline");
            if (deadlineType == DeadlineType.Continuous)
                return GetLabel("ContinuousDeadline");
            return null;
        }

        private ReactModels.DateCardStatus BuildStatus(ProposalPage page) =>
            _dateCardStatusHandler.GetCurrentStatus(page);

        private ReactModels.Metadata BuildProposalMetaData(ProposalPage page) =>
            new ReactModels.Metadata
            {
                Items = new List<ReactModels.Metadata_Items>
                {
                    new ReactModels.Metadata_Items
                    {
                        Label = GetLabel("ProjectSize"),
                        Text = string.Format(GetLabel("ProjectSizeFormat"),
                            page.MinApplicationAmount.ToString(NumberFormat),
                            page.MaxApplicationAmount.ToString(NumberFormat))
                    },
                    new ReactModels.Metadata_Items
                    {
                        Label = GetLabel("Duration"),
                        Text = string.Format(GetLabel("DurationFormat"), page.MinProjectLength, page.MaxProjectLength)
                    }
                }
            };

        private ReactModels.DateCardTags BuildTags(CategoryList categories, int numberOfVisibleItems) =>
            categories is null ? null
                : new ReactModels.DateCardTags
                {
                    Items = BuildTagList(categories),
                    Accordion = categories.Count - numberOfVisibleItems > 0
                        ? new ReactModels.Accordion
                        {
                            Guid = Guid.NewGuid().ToString(),
                            ExpandLabel = string.Format(GetLabel("ExpandTagsFormat"), categories.Count - numberOfVisibleItems),
                            CollapseLabel = GetLabel("CollapseTags")
                        }
                        : null,
                    NumberOfVisibleItems = numberOfVisibleItems
                };

        private IList<ReactModels.Link> BuildTagList(CategoryList categories) =>
            categories
                .Select(cat => _categoryRepository.Get(cat))
                .Select(x => new ReactModels.Link { Text = _categoryLocalizationProvider.GetCategoryName(x.ID) })
                .OrderBy(x => x.Text)
                .ToList();

        private string BuildUrlWithQuery(RelatedProposalListBlock block)
        {
            if (block.Button?.Link is null)
                return null;

            var categories = new List<string>();
            categories.AddRange(GetSubjectQueryStrings());
            categories.AddRange(GetTargetGroupQueryStrings());

            var query = categories.Any()
                ? "?" + string.Join("&", categories)
                : string.Empty;

            return block.Button?.Link.GetUrl(_urlResolver) is string url
                ? url + query
                : null;

            IEnumerable<string> GetSubjectQueryStrings() =>
                GetSelectedCategories(block)
                    .Where(x => x.Parent.Name == CategoryConstants.SubjectRoot)
                    .Select(x => $"{QueryParameters.Subject}={x.ID}");

            IEnumerable<string> GetTargetGroupQueryStrings() =>
                GetSelectedCategories(block)
                    .Where(x => x.Parent.Name == CategoryConstants.TargetGroupRoot)
                    .Select(x => $"{QueryParameters.TargetGroup}={x.ID}");
        }

        private ReactModels.EmptyList BuildEmptyList() =>
            new ReactModels.EmptyList { Text = GetLabel("EmptyProposalList") };

        private ContentListRequestModel BuildProposalListRequest(RelatedProposalListBlock block) =>
            new ContentListRequestModel
            {
                PageRoot = GetPageRoot(block),
                Take = block.NumberOfVisibleItems > 0 ? block.NumberOfVisibleItems : SearchConstants.DefaultRelatedContentSize,
                Categories = GetSelectedCategories(block)
                    .Select(x => x.ID)
                    .ToArray(),
                ContentTypes = GetDefaultPageTypeIds()
            };

        private int GetPageRoot(RelatedProposalListBlock block) =>
            block.PageRoot?.ID > 0
                ? block.PageRoot.ID
                : _pageRepository.GetClosestFrontPage(ContentReference.StartPage) is FrontPage frontPage &&
                  frontPage.ProposalsPage is PageReference listRoot
                    ? listRoot.ID
                    : 0;

        private int[] GetDefaultPageTypeIds() =>
            _pageTypeRepository.List()
                .Where(x => x.ModelType == typeof(ProposalPage))
                .Select(x => x.ID)
                .ToArray();

        private IEnumerable<Category> GetSelectedCategories(RelatedProposalListBlock block) =>
            (block as ICategorizable)?.Category.Select(x => _categoryRepository.Get(x)) ?? new List<Category>();

        private IEnumerable<ProposalPage> GetProposals(ContentListRequestModel request) =>
            _contentListService.Search<ProposalPage>(request)
                .Results
                .OfType<ProposalPage>()
                .Where(x => x.ProposalState == ProposalState.Active || x.ProposalState == ProposalState.Planned)
                .OrderRelatedProposals()
                .ToList();

        private string GetLabel(string key) => _localizationProvider.GetLabel(nameof(RelatedContentListBlock), key);
    }
}
