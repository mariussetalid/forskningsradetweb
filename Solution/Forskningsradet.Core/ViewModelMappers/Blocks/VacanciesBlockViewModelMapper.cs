﻿using System;
using System.Collections.Generic;
using System.Linq;
using EPiServer.Core;
using EPiServer.Web.Routing;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Extensions;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.ViewModels.Blocks;
using Forskningsradet.Core.Models.ViewModels.Contracts;
using Forskningsradet.Core.Repositories.Contracts;
using Forskningsradet.Core.Services;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.ViewModelMappers.Blocks
{
    public class VacanciesBlockViewModelMapper : BasePartialViewModelMapper<VacanciesBlock>
    {
        private readonly IUrlResolver _urlResolver;
        private readonly IPageRepository _pageRepository;
        private readonly IDateCardDatesReactModelBuilder _dateCardDatesReactModelBuilder;
        private readonly ILocalizationProvider _localizationProvider;

        public VacanciesBlockViewModelMapper(
            IUrlResolver urlResolver,
            IPageRepository pageRepository,
            IDateCardDatesReactModelBuilder dateCardDatesReactModelBuilder,
            ILocalizationProvider localizationProvider)
        {
            _urlResolver = urlResolver;
            _pageRepository = pageRepository;
            _dateCardDatesReactModelBuilder = dateCardDatesReactModelBuilder;
            _localizationProvider = localizationProvider;
        }

        protected override IReactViewModel GetViewModelForPartialInternal(VacanciesBlock currentBlock)
        {
            var reactComponent = BuildReactComponent(currentBlock);
            return new ReactPartialViewModel(currentBlock.ReactComponentName(), reactComponent);
        }

        private ReactModels.Vacancies BuildReactComponent(VacanciesBlock currentBlock) =>
            new ReactModels.Vacancies
            {
                Title = currentBlock.Title,
                Text = currentBlock.Text,
                Items = BuildItems().ToList(),
                Link = BuildEditLinkBlock(currentBlock.ButtonBlock),
                EmptyList = new ReactModels.EmptyList
                {
                    Text = GetLabel("EmptyList")
                }
            };

        private IEnumerable<ReactModels.DateCard> BuildItems()
        {
            var vacanciesPageReference = _pageRepository.GetCurrentStartPage()?.VacanciesPage;
            if (ContentReference.IsNullOrEmpty(vacanciesPageReference))
                return Enumerable.Empty<ReactModels.DateCard>();
            var vacanciesPages = _pageRepository.GetPublishedDescendants<VacancyPage>(vacanciesPageReference);
            var sortedVacancyPages = vacanciesPages.OrderBy(vacancyPage => vacancyPage.DueDate);
            return sortedVacancyPages.Select(MapVacancyPageToDateCard);
        }

        private ReactModels.DateCard MapVacancyPageToDateCard(VacancyPage page, int index) =>
            new ReactModels.DateCard
            {
                Id = index.ToString(),
                Title = page.Title,
                Url = page.HrManagerUri.GetUrl(_urlResolver),
                Text = page.MainIntro,
                DateContainer = _dateCardDatesReactModelBuilder.BuildReactModel(GetDates(page.DueDate), GetLabel("DueDate")),
                Metadata = new ReactModels.Metadata
                {
                    Items = BuildMetadataItems(page)
                }
            };

        private List<ReactModels.DateCardDates_Dates> GetDates(DateTime date) =>
            new List<ReactModels.DateCardDates_Dates>
                    {
                        new ReactModels.DateCardDates_Dates
                        {
                            Month = date.ToDisplayDate().ToString(DateTimeFormats.ShortMonth),
                            Day = date.ToDisplayDate().CultureSpecificDay()
                        }
                    };

        private List<ReactModels.Metadata_Items> BuildMetadataItems(VacancyPage page)
        {
            var labels = new List<ReactModels.Metadata_Items>();
            if (!string.IsNullOrWhiteSpace(page.WorkplaceLocation))
            {
                var workplaceLocation = BuildWorkPlaceLocationItem(page.WorkplaceLocation);
                labels.Add(workplaceLocation);
            }

            var dueDate = BuildDueDateItem(page.DueDate);
            labels.Add(dueDate);

            return labels;
        }

        private ReactModels.Metadata_Items BuildDueDateItem(DateTime pageDueDate) =>
            new ReactModels.Metadata_Items
            {
                Text = pageDueDate.ToDisplayDate().ToString("D"),
                Label = GetLabel("DueDate")
            };

        private ReactModels.Metadata_Items BuildWorkPlaceLocationItem(string pageWorkplaceLocation) =>
            new ReactModels.Metadata_Items
            {
                Text = pageWorkplaceLocation,
                Label = GetLabel("WorkPlace")
            };

        private ReactModels.Link BuildEditLinkBlock(EditLinkBlock editLinkBlock) =>
            new ReactModels.Link
            {
                Url = editLinkBlock.Link?.GetUrl(_urlResolver),
                Text = editLinkBlock.Text,
                Id = editLinkBlock.AnchorId.EnsureValidHtmlId()
            };

        private string GetLabel(string key) => _localizationProvider.GetLabel(nameof(VacanciesBlock), key);
    }
}
