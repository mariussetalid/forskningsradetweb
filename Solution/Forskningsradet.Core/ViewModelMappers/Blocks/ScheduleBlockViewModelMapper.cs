﻿using System.Collections.Generic;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Facades.Contracts;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ViewModels.Blocks;
using Forskningsradet.Core.Models.ViewModels.Contracts;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.ViewModelMappers.Blocks
{
    public class ScheduleBlockViewModelMapper : BasePartialViewModelMapper<ScheduleBlock>
    {
        private readonly IRichTextReactModelBuilder _richTextReactModelBuilder;
        private readonly IAccordionReactModelBuilder _accordionReactModelBuilder;
        private readonly IPageEditingAdapter _pageEditingAdapter;

        public ScheduleBlockViewModelMapper(IRichTextReactModelBuilder richTextReactModelBuilder, IAccordionReactModelBuilder accordionReactModelBuilder, IPageEditingAdapter pageEditingAdapter)
        {
            _richTextReactModelBuilder = richTextReactModelBuilder;
            _accordionReactModelBuilder = accordionReactModelBuilder;
            _pageEditingAdapter = pageEditingAdapter;
        }

        protected override IReactViewModel GetViewModelForPartialInternal(ScheduleBlock currentContent)
        {
            var reactComponent = BuildReactComponent(currentContent);
            return new ReactPartialViewModel(currentContent.ReactComponentName(), reactComponent);
        }

        private ReactModels.ReactComponent BuildReactComponent(ScheduleBlock currentBlock) =>
            new ReactModels.AccordionWithContentAreaList
            {
                Accordion = _accordionReactModelBuilder.BuildReactModel(true),
                Content = new List<ReactModels.AccordionWithContentArea>
                {
                    new ReactModels.AccordionWithContentArea
                    {
                        OnPageEditing = _pageEditingAdapter.PageIsInEditMode()
                        ? new ReactModels.AccordionWithContentArea_OnPageEditing
                        {
                            Title = nameof(currentBlock.Title)
                        }
                        : null,
                        Title = currentBlock.Title,
                        Content = new ReactModels.ContentArea
                        {
                            Blocks = new List<ReactModels.ContentAreaItem>
                            {
                                new ReactModels.ContentAreaItem
                                {
                                    ComponentName = nameof(ReactModels.Schedule),
                                    ComponentData = new ReactModels.Schedule
                                    {
                                        ProgramList = new List<ReactModels.Schedule_ProgramList>
                                        {
                                            new ReactModels.Schedule_ProgramList
                                            {
                                                Text = _richTextReactModelBuilder.BuildReactModel(currentBlock.Times, nameof(currentBlock.Times)),
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            };
    }
}
