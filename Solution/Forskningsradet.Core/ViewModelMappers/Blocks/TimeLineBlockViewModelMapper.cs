﻿using System;
using System.Collections.Generic;
using System.Linq;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Extensions;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ContentModels.PropertyList;
using Forskningsradet.Core.Models.ReactModels;
using Forskningsradet.Core.Models.ViewModels.Blocks;
using Forskningsradet.Core.Models.ViewModels.Contracts;
using Forskningsradet.Core.Services;

namespace Forskningsradet.Core.ViewModelMappers.Blocks
{
    public class TimeLineBlockViewModelMapper : BasePartialViewModelMapper<TimeLineBlock>
    {
        private readonly ILocalizationProvider _localizationProvider;
        private readonly IRichTextReactModelBuilder _richTextReactModelBuilder;

        public TimeLineBlockViewModelMapper(ILocalizationProvider localizationProvider, IRichTextReactModelBuilder richTextReactModelBuilder)
        {
            _localizationProvider = localizationProvider;
            _richTextReactModelBuilder = richTextReactModelBuilder;
        }

        protected override IReactViewModel GetViewModelForPartialInternal(TimeLineBlock currentBlock)
        {
            var reactModel = BuildReactModel(currentBlock);
            return new ReactPartialViewModel(currentBlock.ReactComponentName(), reactModel);
        }

        private TimelineBlock BuildReactModel(TimeLineBlock currentBlock)
        {
            var sortedTimeLine = SortTimeLine(currentBlock.TimeLineItems);
            var startIndex = GetStartIndex(sortedTimeLine);
            return new TimelineBlock
            {
                Title = currentBlock.Title,
                Items = BuildItems(sortedTimeLine, startIndex, GetProgressValueFromStartIndexToCurrentDay(sortedTimeLine, startIndex)),
                Labels = BuildLabels(),
                StartIndex = startIndex,
                EditorTheme = Timeline_EditorTheme.None
            };
        }

        private IList<TimeLineItem> SortTimeLine(IList<TimeLineItem> currentBlockTimeLineItems) =>
            currentBlockTimeLineItems is null
                ? new List<TimeLineItem>()
                : currentBlockTimeLineItems
                    .OrderBy(x => HasDate(x) ? GetDate(x) : DateTime.MaxValue)
                    .ToList();

        private IList<TimelineItem> BuildItems(IList<TimeLineItem> currentBlockTimeLineItems, int progressIndex, int progressValue) =>
            currentBlockTimeLineItems
                .Select((x, i) => i == progressIndex ? MapTimeLineItemToReactModel(x, progressValue) : MapTimeLineItemToReactModel(x, 0))
                .ToList();

        private TimelineItem MapTimeLineItemToReactModel(TimeLineItem timelineItem, int progress) =>
            new TimelineItem
            {
                Title = !string.IsNullOrEmpty(timelineItem.DateTitle)
                    ? timelineItem.DateTitle
                    : !HasDate(timelineItem) ? null : GetDate(timelineItem).ToClosestDateUnspecified().ToString(DateTimeFormats.TimelineDate),
                Text = !string.IsNullOrEmpty(timelineItem.MainBody?.ToString())
                    ? _richTextReactModelBuilder.BuildReactModel(timelineItem.MainBody, nameof(timelineItem.MainBody))
                    : null,
                SubTitle = timelineItem.Title,
                IsPastDate = IsPastOrMissingDate(timelineItem),
                Progress = progress
            };

        private int GetStartIndex(IList<TimeLineItem> sortedTimeline)
        {
            if (!sortedTimeline.Any())
                return 0;

            return sortedTimeline.Any(ItemWithFutureDate())
                ? LastIndexBeforeFutureDate()
                : LastIndex();

            int LastIndex() => sortedTimeline.Count() - 1;
            int LastIndexBeforeFutureDate() =>
                sortedTimeline
                .IndexOf(sortedTimeline.FirstOrDefault(ItemWithFutureDate()))
                - 1;

            Func<TimeLineItem, bool> ItemWithFutureDate() =>
                x => HasDate(x) && GetDate(x).Date.CompareTo(DateNow()) > 0;
        }

        private int GetProgressValueFromStartIndexToCurrentDay(IList<TimeLineItem> sortedTimeline, int startIndex)
        {
            if (!AllTimelineItemsHasDate() || !StartIndexIsValidIndex() || StartIndexIsLastItem())
                return 0;

            var closestPastDate = GetDate(sortedTimeline.ElementAt(startIndex)).Date;
            var nextDate = GetDate(sortedTimeline.ElementAt(startIndex + 1));
            var dateNow = DateNow();

            var daysBetween = (nextDate - closestPastDate).Days;
            var progressInDays = (dateNow - closestPastDate).Days;

            return daysBetween > 0 ? (100 * progressInDays / daysBetween) : 0;

            bool AllTimelineItemsHasDate() => sortedTimeline.All(HasDate);
            bool StartIndexIsValidIndex() => startIndex >= 0 && startIndex < sortedTimeline.Count;
            bool StartIndexIsLastItem() => startIndex == (sortedTimeline.Count - 1);

        }

        private static DateTime GetDate(TimeLineItem timelineItem) => timelineItem.DateTimes.FirstOrDefault();

        private static bool HasDate(TimeLineItem timelineItem) => timelineItem.DateTimes?.Count > 0;

        private static DateTime DateNow() => DateTime.UtcNow.ToDisplayDate().Date;

        private static bool IsPastOrMissingDate(TimeLineItem timelineItem) =>
            !HasDate(timelineItem) || GetDate(timelineItem).Date.CompareTo(DateNow()) <= 0;

        private Timeline_Labels BuildLabels() =>
            new Timeline_Labels
            {
                Next = GetLabel("Next"),
                Previous = GetLabel("Previous")
            };

        private string GetLabel(string key) => _localizationProvider.GetLabel(nameof(TimeLineBlock), key);
    }
}