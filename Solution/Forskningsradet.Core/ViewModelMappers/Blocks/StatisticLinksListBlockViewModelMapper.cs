﻿using System.Linq;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ViewModels.Blocks;
using Forskningsradet.Core.Models.ViewModels.Contracts;
using Forskningsradet.Core.Services.Contracts;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.ViewModelMappers.Blocks
{
    public class StatisticLinksListBlockViewModelMapper : BasePartialViewModelMapper<StatisticLinksListBlock>
    {
        private readonly IContentLoadHelper _contentLoadHelper;
        private readonly IImageReactModelBuilder _imageReactModelBuilder;
        private readonly ILinkReactModelBuilder _linkReactModelBuilder;

        public StatisticLinksListBlockViewModelMapper(IContentLoadHelper contentLoadHelper, IImageReactModelBuilder imageReactModelBuilder, ILinkReactModelBuilder linkReactModelBuilder)
        {
            _contentLoadHelper = contentLoadHelper;
            _imageReactModelBuilder = imageReactModelBuilder;
            _linkReactModelBuilder = linkReactModelBuilder;
        }

        protected override IReactViewModel GetViewModelForPartialInternal(StatisticLinksListBlock currentContent) =>
            new ReactPartialViewModel(currentContent.ReactComponentName(), BuildReactComponent(currentContent));

        private ReactModels.ReactComponent BuildReactComponent(StatisticLinksListBlock linksList) =>
             NullIfEmpty(new ReactModels.StatsLinkList
             {
                 Heading = linksList.Heading,
                 StatsLinks = _contentLoadHelper.GetFilteredItems<StatisticLinkBlock>(linksList.Links)
                    .Select(BuildLink)
                    .Where(x => x != null)
                    .ToList(),
                 AboutImage = _imageReactModelBuilder.BuildImage(linksList.AboutImage),
                 SeeMoreLink = _linkReactModelBuilder.BuildLink(linksList.SeeMoreLink)
             });

        private ReactModels.StatsLink BuildLink(StatisticLinkBlock link) =>
            link is null ? null : NullIfEmpty(new ReactModels.StatsLink
            {
                Image = _imageReactModelBuilder.BuildImage(link.Image),
                Title = link.Title,
                Link = _linkReactModelBuilder.BuildLink(link.Url, link.Text)
            });

        private ReactModels.StatsLinkList NullIfEmpty(ReactModels.StatsLinkList list) =>
            list.StatsLinks?.Any() != true && string.IsNullOrEmpty(list.Heading) && string.IsNullOrEmpty(list.AboutImage?.Src)
            ? null
            : list;

        private ReactModels.StatsLink NullIfEmpty(ReactModels.StatsLink link) =>
            link.Image is null && string.IsNullOrEmpty(link.Title) && link.Link is null
            ? null
            : link;
    }
}