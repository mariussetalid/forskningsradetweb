﻿using EPiServer;
using EPiServer.Web.Routing;
using Forskningsradet.Core.Facades.Contracts;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ContentModels.Media;
using Forskningsradet.Core.Models.ViewModels.Blocks;
using Forskningsradet.Core.Models.ViewModels.Contracts;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.ViewModelMappers.Blocks
{
    public class ImageBlockViewModelMapper : BasePartialViewModelMapper<ImageBlock>
    {
        private readonly IUrlResolver _urlResolver;
        private readonly IContentLoader _contentLoader;
        private readonly IPageEditingAdapter _pageEditingAdapter;

        public ImageBlockViewModelMapper(IUrlResolver urlResolver, IContentLoader contentLoader, IPageEditingAdapter pageEditingAdapter)
        {
            _urlResolver = urlResolver;
            _contentLoader = contentLoader;
            _pageEditingAdapter = pageEditingAdapter;
        }

        protected override IReactViewModel GetViewModelForPartialInternal(ImageBlock currentContent)
        {
            var reactComponent = BuildReactComponent(currentContent);
            return new ReactPartialViewModel(currentContent.ReactComponentName(), reactComponent);
        }

        private ReactModels.ReactComponent BuildReactComponent(ImageBlock currentBlock)
        {
            var imageFile = currentBlock.Image != null
                ? _contentLoader.Get<ImageFile>(currentBlock.Image)
                : null;
            return new ReactModels.ImageBlock
            {
                OnPageEditing = _pageEditingAdapter.PageIsInEditMode()
                    ? new ReactModels.ImageBlock_OnPageEditing
                    {
                        Title = nameof(currentBlock.Title)
                    }
                    : null,
                Title = currentBlock.Title,
                Image = imageFile != null || _pageEditingAdapter.PageIsInEditMode()
                    ? new ReactModels.Image
                    {
                        Alt = imageFile?.AltText ?? string.Empty,
                        Src = imageFile != null ? _urlResolver.GetUrl(imageFile.ContentLink) : string.Empty,
                        OnPageEditing = _pageEditingAdapter.PageIsInEditMode()
                            ? new ReactModels.Image_OnPageEditing
                            {
                                Image = nameof(currentBlock.Image)
                            }
                            : null
                    }
                    : null
            };
        }
    }
}