﻿using EPiServer.Web.Routing;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Extensions;
using Forskningsradet.Core.Facades.Contracts;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ReactModels;
using Forskningsradet.Core.Models.ViewModels.Blocks;
using Forskningsradet.Core.Models.ViewModels.Contracts;

namespace Forskningsradet.Core.ViewModelMappers.Blocks
{
    public class InformationBlockViewModelMapper : BasePartialViewModelMapper<InformationBlock>
    {
        private readonly IRichTextReactModelBuilder _richTextReactModelBuilder;
        private readonly IUrlResolver _urlResolver;
        private readonly IPageEditingAdapter _pageEditingAdapter;

        public InformationBlockViewModelMapper(IRichTextReactModelBuilder richTextReactModelBuilder, IUrlResolver urlResolver, IPageEditingAdapter pageEditingAdapter)
        {
            _richTextReactModelBuilder = richTextReactModelBuilder;
            _urlResolver = urlResolver;
            _pageEditingAdapter = pageEditingAdapter;
        }

        protected override IReactViewModel GetViewModelForPartialInternal(InformationBlock currentContent)
        {
            var reactComponent = BuildReactComponent(currentContent);
            return new ReactPartialViewModel(currentContent.ReactComponentName(), reactComponent);
        }

        private ReactComponent BuildReactComponent(InformationBlock currentBlock)
        {
            var imageFile = currentBlock.Icon?.GetAsImageFile();
            return new InfoBlock
            {
                OnPageEditing = _pageEditingAdapter.PageIsInEditMode()
                    ? new InfoBlock_OnPageEditing
                    {
                        Title = nameof(currentBlock.Title),
                    }
                    : null,
                Title = currentBlock.Title,
                Url = currentBlock.Url?.GetUrl(_urlResolver),
                Text = _richTextReactModelBuilder.BuildReactModel(currentBlock.MainBody, nameof(currentBlock.MainBody)),
                Cta = new Link
                {
                    OnPageEditing = _pageEditingAdapter.PageIsInEditMode()
                        ? nameof(currentBlock.Button)
                        : null,
                    Text = currentBlock.Button?.Text,
                    Url = currentBlock.Button?.Link?.GetUrl(_urlResolver),
                    Id = currentBlock.Button?.AnchorId.EnsureValidHtmlId()
                },
                Icon = imageFile != null || _pageEditingAdapter.PageIsInEditMode()
                    ? new Image
                    {
                        OnPageEditing = _pageEditingAdapter.PageIsInEditMode()
                            ? new Image_OnPageEditing
                            {
                                Image = nameof(currentBlock.Icon)
                            }
                            : null,
                        Alt = imageFile?.AltText ?? string.Empty,
                        Src = _urlResolver.GetUrl(imageFile?.ContentLink)
                    }
                    : null,
                EditorTheme = MapEditorTheme(currentBlock.Style)
            };
        }

        private static InfoBlock_EditorTheme MapEditorTheme(InformationBlockStyle style)
        {
            switch (style)
            {
                case InformationBlockStyle.ThemeBlue:
                    return InfoBlock_EditorTheme.Blue;
                case InformationBlockStyle.ThemeOrange:
                    return InfoBlock_EditorTheme.Orange;
                case InformationBlockStyle.ThemeDarkBlue:
                    return InfoBlock_EditorTheme.DarkBlue;
                default:
                    return InfoBlock_EditorTheme.None;
            }
        }
    }
}
