using System;
using System.Collections.Generic;
using System.Linq;
using EPiServer.Core;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.QueryParameters;
using Forskningsradet.Core.Models.ViewModels.Blocks;
using Forskningsradet.Core.Models.ViewModels.Contracts;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.ViewModelMappers.Blocks
{
    public class SubjectPriorityBlockViewModelMapper : BasePartialViewModelMapperWithQuery<SubjectPriorityBlock>, IPartialViewModelMapper
    {
        private readonly IRichTextReactModelBuilder _richTextReactModelBuilder;
        private readonly IContentAreaReactModelBuilder _contentAreaReactModelBuilder;
        private readonly IOptionsModalReactModelBuilder _optionsModalReactModelBuilder;

        public SubjectPriorityBlockViewModelMapper(
            IRichTextReactModelBuilder richTextReactModelBuilder,
            IContentAreaReactModelBuilder contentAreaReactModelBuilder,
            IOptionsModalReactModelBuilder optionsModalReactModelBuilder)
        {
            _richTextReactModelBuilder = richTextReactModelBuilder;
            _contentAreaReactModelBuilder = contentAreaReactModelBuilder;
            _optionsModalReactModelBuilder = optionsModalReactModelBuilder;
        }

        protected override IReactViewModel GetPartialViewModelInternal(SubjectPriorityBlock currentBlock, QueryParameterBase queryParameterBase)
        {
            var query = queryParameterBase as SubjectPriorityBlockQueryParameter ?? new SubjectPriorityBlockQueryParameter();
            return new ReactPartialViewModel(currentBlock.ReactComponentName(), BuildReactComponent(currentBlock, query));
        }

        public IReactViewModel GetPartialViewModel(IContentData currentContent) =>
          currentContent is SubjectPriorityBlock content
              ? GetPartialViewModelInternal(content, null)
              : null;

        private ReactModels.ReactComponent BuildReactComponent(SubjectPriorityBlock currentBlock, SubjectPriorityBlockQueryParameter query) =>
            new ReactModels.AccordionWithContentArea
            {
                Title = currentBlock.Title,
                IconWarning = GetIconWarningIfContainsMessage(BuildTextWithSidebarText(currentBlock)),
                Share = BuildShareModel(currentBlock.Title, currentBlock.AnchorId, query.PageReference),
                HtmlId = currentBlock.AnchorId,
                Content = BuildContent(currentBlock)
            };

        private ReactModels.IconWarning GetIconWarningIfContainsMessage(ReactModels.RichText text) =>
            text?.Blocks?.FirstOrDefault(y => y.ComponentName == nameof(ReactModels.Message))?.ComponentData is ReactModels.Message message
                ? new ReactModels.IconWarning { Theme = message.Theme }
                : null;

        private ReactModels.RichText BuildTextWithSidebarText(SubjectPriorityBlock currentBlock) =>
            _richTextReactModelBuilder.BuildReactModel(currentBlock.Text, nameof(currentBlock.Text));

        private ReactModels.OptionsModal BuildShareModel(string title, string anchorId, PageReference pageReference) =>
                 _optionsModalReactModelBuilder.BuildShareContent(pageReference, null, anchorId, title);

        private ReactModels.ContentArea BuildContent(SubjectPriorityBlock currentBlock)
        {
            var textWithSidebarComponent = new ReactModels.ContentAreaItem
            {
                Id = Guid.NewGuid().ToString(),
                ComponentName = nameof(ReactModels.TextWithSidebar),
                ComponentData = BuildTextWithSidebarWithoutTitle(currentBlock),
                OnPageEditing = null,
                Size = ReactModels.ContentAreaItem_Size.None
            };

            var content = new ReactModels.ContentArea();

            if (currentBlock.PortfolioContent is null)
            {
                content = _contentAreaReactModelBuilder.BuildReactModel(
                    new List<ReactModels.ContentAreaItem>
                    {
                            textWithSidebarComponent
                    }
                    , null);
            }
            else
            {
                content = BuildPortfolioContent(currentBlock.PortfolioContent, nameof(currentBlock.PortfolioContent));
                content.Blocks.Insert(0, textWithSidebarComponent);
            }
            return content;
        }

        private ReactModels.TextWithSidebar BuildTextWithSidebarWithoutTitle(SubjectPriorityBlock currentBlock) =>
            new ReactModels.TextWithSidebar
            {
                Title = null,
                Text = BuildTextWithSidebarText(currentBlock),
                Sidebar = currentBlock.RightBlockArea?.FilteredItems.Any() == true ? _contentAreaReactModelBuilder.BuildReactModel(currentBlock.RightBlockArea, nameof(currentBlock.RightBlockArea)) : null,
                HtmlId = currentBlock.AnchorId
            };

        private ReactModels.ContentArea BuildPortfolioContent(ContentArea portfolioContent, string propertyName)
        {
            var content = portfolioContent?.FilteredItems.Any() == true ? _contentAreaReactModelBuilder.BuildReactModel(portfolioContent, propertyName) : null;
            if (content is null)
                return null;

            foreach (ReactModels.ContentAreaItem item in content.Blocks.Where(e => e.ComponentData is ReactModels.PortfolioContent))
            {
                if (item.ComponentData is ReactModels.PortfolioContent p)
                    p.IsInsidePriorityBlock = true;
            }

            return content;
        }
    }
}