﻿using System.Collections.Generic;
using System.Linq;
using EPiServer.Core;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ViewModels.Blocks;
using Forskningsradet.Core.Models.ViewModels.Contracts;
using Forskningsradet.Core.Services.Contracts;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.ViewModelMappers.Blocks
{
    public class CampaignListBlockViewModelMapper : BasePartialViewModelMapper<CampaignListBlock>
    {
        private readonly IContentLoadHelper _contentLoadHelper;
        private readonly ICampaignBlockReactModelBuilder _campaignBlockReactModelBuilder;

        public CampaignListBlockViewModelMapper(IContentLoadHelper contentLoadHelper, ICampaignBlockReactModelBuilder campaignReactModelBuilder)
        {
            _contentLoadHelper = contentLoadHelper;
            _campaignBlockReactModelBuilder = campaignReactModelBuilder;
        }

        protected override IReactViewModel GetViewModelForPartialInternal(CampaignListBlock currentContent) =>
            new ReactPartialViewModel(currentContent.ReactComponentName(), BuildReactComponent(currentContent));

        private ReactModels.ReactComponent BuildReactComponent(CampaignListBlock block) =>
            new ReactModels.CampaignBlockList
            {
                List = BuildList(block.Campaigns)
            };

        private IList<ReactModels.CampaignBlock> BuildList(ContentArea campaigns) =>
            _contentLoadHelper.GetFilteredItems<CampaignBlock>(campaigns)
                .Select(BuildCampaign)
                .ToList();

        private ReactModels.CampaignBlock BuildCampaign(CampaignBlock campaignBlock) =>
            _campaignBlockReactModelBuilder.BuildReactModel(campaignBlock);
    }
}
