﻿using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ViewModels.Blocks;
using Forskningsradet.Core.Models.ViewModels.Contracts;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.ViewModelMappers.Blocks
{
    public class RichTextBlockViewModelMapper : BasePartialViewModelMapper<RichTextBlock>
    {
        private readonly IRichTextReactModelBuilder _richTextReactModelBuilder;

        public RichTextBlockViewModelMapper(IRichTextReactModelBuilder richTextReactModelBuilder) => _richTextReactModelBuilder = richTextReactModelBuilder;

        protected override IReactViewModel GetViewModelForPartialInternal(RichTextBlock currentContent)
        {
            var reactComponent = BuildReactComponent(currentContent);
            return new ReactPartialViewModel(currentContent.ReactComponentName(), reactComponent);
        }

        private ReactModels.ReactComponent BuildReactComponent(RichTextBlock currentBlock)
            => _richTextReactModelBuilder.BuildReactModel(currentBlock.MainBody, nameof(currentBlock.MainBody));
    }
}