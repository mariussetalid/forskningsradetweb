﻿using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Facades.Contracts;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ViewModels.Blocks;
using Forskningsradet.Core.Models.ViewModels.Contracts;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.ViewModelMappers.Blocks
{
    public class LinkListBlockViewModelMapper : BasePartialViewModelMapper<LinkListBlock>
    {
        private readonly ILinkListReactModelBuilder _linkListReactModelBuilder;
        private readonly IPageEditingAdapter _pageEditingAdapter;

        public LinkListBlockViewModelMapper(ILinkListReactModelBuilder linkListReactModelBuilder, IPageEditingAdapter pageEditingAdapter)
        {
            _linkListReactModelBuilder = linkListReactModelBuilder;
            _pageEditingAdapter = pageEditingAdapter;
        }

        protected override IReactViewModel GetViewModelForPartialInternal(LinkListBlock currentContent) =>
            new ReactPartialViewModel(currentContent.ReactComponentName(), BuildReactComponent(currentContent));

        private ReactModels.ReactComponent BuildReactComponent(LinkListBlock currentBlock) =>
            new ReactModels.InfoBlock
            {
                OnPageEditing = _pageEditingAdapter.PageIsInEditMode()
                    ? new ReactModels.InfoBlock_OnPageEditing
                    {
                        Title = nameof(currentBlock.Title)
                    }
                    : null,
                Title = currentBlock.Title,
                LinkList = _linkListReactModelBuilder.BuildReactModel(currentBlock.LinkItems, nameof(currentBlock.LinkItems))
            };
    }
}