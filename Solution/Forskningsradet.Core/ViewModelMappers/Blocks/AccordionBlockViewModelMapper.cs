using System;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Facades.Contracts;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ViewModels.Blocks;
using Forskningsradet.Core.Models.ViewModels.Contracts;
using Forskningsradet.Core.Services;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.ViewModelMappers.Blocks
{
    public class AccordionBlockViewModelMapper : BasePartialViewModelMapper<AccordionBlock>
    {
        private readonly IRichTextReactModelBuilder _richTextReactModelBuilder;
        private readonly ILocalizationProvider _localizationProvider;
        private readonly IPageEditingAdapter _pageEditingAdapter;

        public AccordionBlockViewModelMapper(IRichTextReactModelBuilder richTextReactModelBuilder, ILocalizationProvider localizationProvider, IPageEditingAdapter pageEditingAdapter)
        {
            _richTextReactModelBuilder = richTextReactModelBuilder;
            _localizationProvider = localizationProvider;
            _pageEditingAdapter = pageEditingAdapter;
        }

        protected override IReactViewModel GetViewModelForPartialInternal(AccordionBlock currentBlock)
            => new ReactPartialViewModel(currentBlock.ReactComponentName(), BuildReactComponent(currentBlock));

        private ReactModels.ReactComponent BuildReactComponent(AccordionBlock currentBlock)
            => new ReactModels.AccordionBlock
            {
                Text = _richTextReactModelBuilder.BuildReactModel(currentBlock.Text, nameof(currentBlock.Text)),
                Title = currentBlock.Title,
                Accordion = new ReactModels.Accordion
                {
                    InitiallyOpen = currentBlock.InitiallyOpen || _pageEditingAdapter.PageIsInEditMode() ,
                    CollapseLabel = GetLabel("ShowLess"),
                    ExpandLabel = GetLabel("ShowMore"),
                    Guid = Guid.NewGuid().ToString()

                },
                OnPageEditing = _pageEditingAdapter.PageIsInEditMode()
                    ? new ReactModels.AccordionBlock_OnPageEditing
                    {
                        Title = nameof(currentBlock.Title),
                    }
                    : null
            };

        private string GetLabel(string key)
            => _localizationProvider.GetLabel(nameof(AccordionBlock), key);
    }
}
