﻿using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ViewModels.Blocks;
using Forskningsradet.Core.Models.ViewModels.Contracts;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.ViewModelMappers.Blocks
{
    public class StructuredScheduleBlockViewModelMapper : BasePartialViewModelMapper<StructuredScheduleBlock>
    {
        private readonly IAccordionWithContentAreaListReactModelBuilder _accordionWithContentAreaListReactModelBuilder;

        public StructuredScheduleBlockViewModelMapper(IAccordionWithContentAreaListReactModelBuilder accordionWithContentAreaListReactModelBuilder)
        {
            _accordionWithContentAreaListReactModelBuilder = accordionWithContentAreaListReactModelBuilder;
        }

        protected override IReactViewModel GetViewModelForPartialInternal(StructuredScheduleBlock currentContent)
        {
            var reactComponent = BuildReactComponent(currentContent);
            return new ReactPartialViewModel(currentContent.ReactComponentName(), reactComponent);
        }

        private ReactModels.ReactComponent BuildReactComponent(StructuredScheduleBlock currentBlock) =>
            _accordionWithContentAreaListReactModelBuilder.BuildReactModel(currentBlock);
    }
}
