﻿using System;
using System.Collections.Generic;
using System.Linq;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.Web.Routing;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Extensions;
using Forskningsradet.Core.Facades.Contracts;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ContentModels.Media;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.Enums;
using Forskningsradet.Core.Models.QueryParameters;
using Forskningsradet.Core.Models.RequestModels;
using Forskningsradet.Core.Models.ViewModels.Blocks;
using Forskningsradet.Core.Models.ViewModels.Contracts;
using Forskningsradet.Core.Services;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.ViewModelMappers.Blocks
{
    public class RelatedContentListBlockViewModelMapper : BasePartialViewModelMapperWithQuery<RelatedContentListBlock>, IPartialViewModelMapper
    {
        private readonly IUrlResolver _urlResolver;
        private readonly IContentListService _contentListService;
        private readonly IContentLoader _contentLoader;
        private readonly IFluidImageReactModelBuilder _fluidImageReactModelBuilder;
        private readonly IPublicationReactModelBuilder _publicationReactModelBuilder;
        private readonly IContentTypeRepository<PageType> _pageTypeRepository;
        private readonly CategoryRepository _categoryRepository;
        private readonly ILocalizationProvider _localizationProvider;
        private readonly IPageEditingAdapter _pageEditingAdapter;
        private readonly IPageRouteHelper _pageRouteHelper;

        public RelatedContentListBlockViewModelMapper(
            IUrlResolver urlResolver,
            IContentListService contentListService,
            IContentLoader contentLoader,
            IFluidImageReactModelBuilder fluidImageReactModelBuilder,
            IPublicationReactModelBuilder publicationReactModelBuilder,
            IContentTypeRepository<PageType> pageTypeRepository,
            CategoryRepository categoryRepository,
            ILocalizationProvider localizationProvider,
            IPageEditingAdapter pageEditingAdapter, 
            IPageRouteHelper pageRouteHelper)
        {
            _urlResolver = urlResolver;
            _contentListService = contentListService;
            _contentLoader = contentLoader;
            _fluidImageReactModelBuilder = fluidImageReactModelBuilder;
            _publicationReactModelBuilder = publicationReactModelBuilder;
            _pageTypeRepository = pageTypeRepository;
            _categoryRepository = categoryRepository;
            _localizationProvider = localizationProvider;
            _pageEditingAdapter = pageEditingAdapter;
            _pageRouteHelper = pageRouteHelper;
        }

        protected override IReactViewModel GetPartialViewModelInternal(RelatedContentListBlock currentContent, QueryParameterBase queryParameterBase) =>
            new ReactPartialViewModel(PortfolioMode(currentContent) ? nameof(ReactModels.PortfolioContent) : currentContent.ReactComponentName(), BuildReactComponent(currentContent, queryParameterBase as RenderOptionQueryParameter ?? new RenderOptionQueryParameter()));

        public IReactViewModel GetPartialViewModel(IContentData currentContent) =>
            currentContent is RelatedContentListBlock content
                ? GetPartialViewModelInternal(content, null)
                : null;

        private bool PortfolioMode(RelatedContentListBlock block) => block.PortfolioMode || (block.Publications?.FilteredItems.Any() == true);

        private ReactModels.ReactComponent BuildReactComponent(RelatedContentListBlock currentBlock, RenderOptionQueryParameter renderOption)
        {
            var pages = GetPages(BuildContentListRequest(currentBlock), currentBlock.Publications);
            return PortfolioMode(currentBlock)
                ? BuildPortfolioContent()
                : BuildRelatedArticles();

            ReactModels.ReactComponent BuildPortfolioContent() =>
                new ReactModels.PortfolioContent
                {
                    Title = currentBlock.Title,
                    Content = new ReactModels.ContentArea
                    {
                       Blocks = new List<ReactModels.ContentAreaItem>
                       {
                           new ReactModels.ContentAreaItem
                           {
                               Id = "0",
                               ComponentName = currentBlock.ReactComponentName(),
                               ComponentData = BuildRelatedArticles(true)
                           }
                       }
                    }
                };

            ReactModels.RelatedArticles BuildRelatedArticles(bool portfolioMode = false) =>
                new ReactModels.RelatedArticles
                {
                    RelatedContent = BuildRelatedContent(currentBlock, portfolioMode ? null : currentBlock.Title, portfolioMode ? ReactModels.RelatedContent_Border.None : ReactModels.RelatedContent_Border.Bottom),
                    Articles = pages.Select(x => BuildReactItem(x, renderOption)).ToList(),
                    UsedInSidebar = renderOption.RenderWidthOption == RenderWidthOption.IsInSidebar,
                    IsGrid = portfolioMode,
                    IsPublication = portfolioMode && currentBlock.Publications?.FilteredItems.Any() == true
                };
        }

        private ReactModels.ArticleBlock BuildReactItem(BasePageData page, RenderOptionQueryParameter renderOption) =>
            page is EditorialPage editorialPage
            ? BuildReactItem(editorialPage, renderOption)
            : page is PublicationBasePage publication
                ? BuildReactItem(publication, renderOption)
                : null;

        private ReactModels.ArticleBlock BuildReactItem(EditorialPage page, RenderOptionQueryParameter renderOption) =>
            new ReactModels.ArticleBlock
            {
                Title = new ReactModels.Link
                {
                    Text = page.GetListTitle(),
                    Url = _urlResolver.GetUrl(page.ContentLink)
                },
                Text = null,
                Image = BuildImage(page),
                UsedInSidebar = renderOption.RenderWidthOption == RenderWidthOption.IsInSidebar,
                Published = new ReactModels.Published
                {
                    Type = GetLabel("Published"),
                    Date = page.StartPublish.HasValue ? FormatDateString(page.StartPublish.Value) : null
                }
            };

        private ReactModels.ArticleBlock BuildReactItem(PublicationBasePage page, RenderOptionQueryParameter renderOption)
        {
            var model = _publicationReactModelBuilder.BuildReactModel(page, PublicationMetadata.Small, HasListImage(page));
            return new ReactModels.ArticleBlock
            {
                Title = new ReactModels.Link
                {
                    Text = model.Title,
                    Url = model.Url ?? _urlResolver.GetUrl(page.ContentLink)
                },
                Text = model.Text,
                Icon = !HasListImage(page) ? model.Icon : null,
                Metadata = model.Metadata,
                DocumentImage = HasListImage(page) ? BuildDocumentImage(page.ListImage) : null,
                EventImage = new ReactModels.EventImage
                {
                    Background = ReactModels.EventImage_Background.None
                },
                UsedInSidebar = renderOption.RenderWidthOption == RenderWidthOption.IsInSidebar
            };
        }

        private bool HasListImage(PublicationBasePage page) =>
            !ContentReference.IsNullOrEmpty(page.ListImage);

        private ReactModels.FluidImage BuildImage(EditorialPage page) =>
            ContentReference.IsNullOrEmpty(page.ListImage)
                ? null
                : _fluidImageReactModelBuilder.BuildReactModel(_contentLoader.Get<ImageFile>(page.ListImage), null);

        private ReactModels.FluidImage BuildDocumentImage(ContentReference imageReference) =>
            _fluidImageReactModelBuilder.BuildReactModel(_contentLoader.Get<ImageFile>(imageReference), null);

        private ReactModels.RelatedContent BuildRelatedContent(RelatedContentListBlock block, string title, ReactModels.RelatedContent_Border borderStyle) =>
            new ReactModels.RelatedContent
            {
                OnPageEditing = _pageEditingAdapter.PageIsInEditMode()
                    ? new ReactModels.RelatedContent_OnPageEditing
                    {
                        Title = nameof(block.Title)
                    }
                    : null,
                EmptyList = BuildEmptyList(),
                Title = title,
                Link = block.Button?.Link is null && !_pageEditingAdapter.PageIsInEditMode()
                    ? null
                    : new ReactModels.Link
                    {
                        OnPageEditing = _pageEditingAdapter.PageIsInEditMode()
                            ? nameof(block.Button)
                            : null,
                        Text = block.Button?.Text ?? GetLabel("GoToListButton"),
                        Url = BuildUrl(block),
                        Id = block.Button?.AnchorId.EnsureValidHtmlId()
                    },
                Border = borderStyle
            };

        private string BuildUrl(RelatedContentListBlock block) => block.Button?.Link.GetUrl(_urlResolver);

        private ReactModels.EmptyList BuildEmptyList() => new ReactModels.EmptyList { Text = GetLabel("EmptyListLabel") };

        private ContentListRequestModel BuildContentListRequest(RelatedContentListBlock block) =>
            new ContentListRequestModel
            {
                CurrentPageLink = _pageRouteHelper.PageLink,
                PageRoot = GetPageRoot(block),
                Take = block.NumberOfVisibleItems > 0 ? block.NumberOfVisibleItems : SearchConstants.DefaultRelatedContentSize,
                Categories = GetSelectedCategories(block)
                    .Select(x => x.ID)
                    .ToArray(),
                ContentTypes = block.PageTypes?.Split(',').Select(int.Parse).ToArray() ?? GetDefaultPageTypeIds()
            };

        private static int GetPageRoot(RelatedContentListBlock block) =>
            block.PageRoot?.ID > 0
                ? block.PageRoot.ID
                : ContentReference.StartPage?.ID ?? 0;

        private int[] GetDefaultPageTypeIds() =>
            _pageTypeRepository.List()
                .Where(x => x.ModelType == typeof(ContentAreaPage)
                            || x.ModelType == typeof(InformationArticlePage)
                            || x.ModelType == typeof(NewsArticlePage)
                            || x.ModelType == typeof(PressReleasePage))
                .Select(x => x.ID)
                .ToArray();

        private IEnumerable<Category> GetSelectedCategories(RelatedContentListBlock block) =>
            (block as ICategorizable)?.Category.Select(x => _categoryRepository.Get(x)) ?? new List<Category>();

        private IEnumerable<BasePageData> GetPages(ContentListRequestModel request, ContentArea publications)
        {
            var pages = _contentListService.Search<EditorialPage>(request)
                .Results
                .OfType<BasePageData>()
                .OrderByDescending(x => x.StartPublish)
                .Take(request.Take);

            var publicationPages = publications?.FilteredItems?
                .Select(x => _contentLoader.Get<BasePageData>(x.ContentLink))
                .OrderByDescending(x => x.StartPublish)
                .Take(request.Take) ?? Enumerable.Empty<BasePageData>();

            if (publicationPages.Any())
                pages = publicationPages.Concat(pages);

            return pages.Take(request.Take).OrderByDescending(x => x.StartPublish);
        }

        private static string FormatDateString(DateTime dateTime) =>
            dateTime.ToDisplayDate().ToNorwegianDateString();

        private string GetLabel(string key) => _localizationProvider.GetLabel(nameof(RelatedContentListBlock), key);
    }
}
