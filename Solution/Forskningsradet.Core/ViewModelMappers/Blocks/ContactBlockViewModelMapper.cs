﻿using System.Collections.Generic;
using System.Linq;
using EPiServer;
using EPiServer.Core;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.ViewModels.Blocks;
using Forskningsradet.Core.Models.ViewModels.Contracts;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.ViewModelMappers.Blocks
{
    public class ContactBlockViewModelMapper : BasePartialViewModelMapper<ContactBlock>
    {
        private readonly IContentLoader _contentLoader;
        private readonly IContactInfoReactModelBuilder _contactInfoReactModelBuilder;

        public ContactBlockViewModelMapper(IContentLoader contentLoader, IContactInfoReactModelBuilder contactInfoReactModelBuilder)
        {
            _contentLoader = contentLoader;
            _contactInfoReactModelBuilder = contactInfoReactModelBuilder;
        }

        protected override IReactViewModel GetViewModelForPartialInternal(ContactBlock currentContent)
        {
            var viewModel = BuildReactComponent(currentContent);
            return new ReactPartialViewModel(currentContent.ReactComponentName(), viewModel);
        }

        private ReactModels.ReactComponent BuildReactComponent(ContactBlock currentContent) =>
            new ReactModels.ContactBlock
            {
                Title = currentContent.Title,
                Items = currentContent.Contacts?.FilteredItems != null
                    ? BuildContentArea(currentContent.Contacts.FilteredItems, currentContent.ShowPhoneNumber, currentContent.ShowJobTitle)
                    : null,
                OnPageEditing = BuildOnPageEditing(currentContent)
            };

        private ReactModels.ContentArea BuildContentArea(IEnumerable<ContentAreaItem> contactsFilteredItems, bool showPhoneNumber, bool showJobTitle) =>
            new ReactModels.ContentArea
            {
                Blocks = BuildItems(contactsFilteredItems, showPhoneNumber, showJobTitle)
            };

        private IList<ReactModels.ContentAreaItem> BuildItems(IEnumerable<ContentAreaItem> contactsFilteredItems, bool showPhoneNumber, bool showJobTitle) =>
            contactsFilteredItems?
                .Select((x, y) => BuildItem(x, y, showPhoneNumber, showJobTitle))
                .ToList();

        private ReactModels.ContentAreaItem BuildItem(ContentAreaItem item, int index, bool showPhoneNumber, bool showJobTitle)
        {
            var contactInfo = _contentLoader.TryGet(item.ContentLink, out PersonPage person)
                ? _contactInfoReactModelBuilder.BuildReactModel(person, null, showPhoneNumber, showJobTitle)
                : _contentLoader.TryGet(item.ContentLink, out NonPersonContactBlock nonPerson)
                    ? _contactInfoReactModelBuilder.BuildReactModel(nonPerson, null, showPhoneNumber)
                    : new ReactModels.ContactInfo();

            return new ReactModels.ContentAreaItem
            {
                Id = index.ToString(),
                ComponentName = nameof(ReactModels.ContactInfo),
                ComponentData = contactInfo
            };
        }

        private ReactModels.ContactBlock_OnPageEditing BuildOnPageEditing(ContactBlock currentContent) =>
            new ReactModels.ContactBlock_OnPageEditing
            {
                Title = nameof(currentContent.Title),
            };
    }
}
