﻿using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Facades.Contracts;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ReactModels;
using Forskningsradet.Core.Models.ViewModels.Blocks;
using Forskningsradet.Core.Models.ViewModels.Contracts;

namespace Forskningsradet.Core.ViewModelMappers.Blocks
{
    public class IframeBlockViewModelMapper : BasePartialViewModelMapper<IframeBlock>
    {
        private readonly IPageEditingAdapter _pageEditingAdapter;

        public IframeBlockViewModelMapper(IPageEditingAdapter pageEditingAdapter)
            => _pageEditingAdapter = pageEditingAdapter;

        protected override IReactViewModel GetViewModelForPartialInternal(IframeBlock currentBlock)
        {
            var reactComponent = BuildReactComponent(currentBlock);
            return new ReactPartialViewModel(currentBlock.ReactComponentName(), reactComponent);
        }

        private ReactComponent BuildReactComponent(IframeBlock currentBlock)
            => new EmbedBlock
            {
                OnPageEditing = _pageEditingAdapter.PageIsInEditMode()
                    ? nameof(currentBlock.Src)
                    : null,
                Title = currentBlock.AlternativeText,
                Src = BuildSource(currentBlock.Src),
                Width = currentBlock.Width > 0 ? currentBlock.Width : (int?)null,
                Height = currentBlock.Height > 0 ? currentBlock.Height : (int?)null
            };

        private static string BuildSource(string source)
            => source?.Trim() is string src && src.StartsWith(EmbedSourceConstants.Iframe.ValidSrcStart)
                ? src
                : null;
    }
}
