﻿using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ViewModels.Blocks;
using Forskningsradet.Core.Models.ViewModels.Contracts;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.ViewModelMappers.Blocks
{
    public class LocalMessageBlockViewModelMapper : BasePartialViewModelMapper<LocalMessageBlock>
    {
        private readonly IMessageReactModelBuilder _messageReactModelBuilder;

        public LocalMessageBlockViewModelMapper(IMessageReactModelBuilder messageReactModelBuilder)
        {
            _messageReactModelBuilder = messageReactModelBuilder;
        }

        protected override IReactViewModel GetViewModelForPartialInternal(LocalMessageBlock currentContent)
        {
            var reactComponent = BuildReactComponent(currentContent);
            return new ReactPartialViewModel(currentContent.ReactComponentName(), reactComponent);
        }

        private ReactModels.ReactComponent BuildReactComponent(LocalMessageBlock currentBlock) =>
            currentBlock.Text is null
                ? null
                : _messageReactModelBuilder.BuildReactModel(currentBlock.Text.ToString(), nameof(currentBlock.Text), currentBlock.Theme);
    }
}
