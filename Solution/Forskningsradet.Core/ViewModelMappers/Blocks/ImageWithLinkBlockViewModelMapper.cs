﻿using EPiServer.Web.Routing;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Extensions;
using Forskningsradet.Core.Facades.Contracts;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ViewModels.Blocks;
using Forskningsradet.Core.Models.ViewModels.Contracts;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.ViewModelMappers.Blocks
{
    public class ImageWithLinkBlockViewModelMapper : BasePartialViewModelMapper<ImageWithLinkBlock>
    {
        private readonly IUrlResolver _urlResolver;
        private readonly IFluidImageReactModelBuilder _fluidImageReactModelBuilder;
        private readonly IPageEditingAdapter _pageEditingAdapter;

        public ImageWithLinkBlockViewModelMapper(IUrlResolver urlResolver, IFluidImageReactModelBuilder fluidImageReactModelBuilder, IPageEditingAdapter pageEditingAdapter)
        {
            _urlResolver = urlResolver;
            _fluidImageReactModelBuilder = fluidImageReactModelBuilder;
            _pageEditingAdapter = pageEditingAdapter;
        }

        protected override IReactViewModel GetViewModelForPartialInternal(ImageWithLinkBlock currentBlock)
        {
            var reactComponent = BuildReactComponent(currentBlock);
            return new ReactPartialViewModel(currentBlock.ReactComponentName(), reactComponent);
        }

        private ReactModels.ReactComponent BuildReactComponent(ImageWithLinkBlock currentBlock)
        {
            return new ReactModels.ImageWithLink
            {
                Url = _urlResolver.GetUrl(currentBlock.EditLink?.Link?.GetUrl(_urlResolver)),
                Text = currentBlock.EditLink?.Text,
                Image = _fluidImageReactModelBuilder.BuildReactModel(currentBlock.Image?.GetAsImageFile(), nameof(currentBlock.Image)),
                OnPageEditing = _pageEditingAdapter.PageIsInEditMode()
                    ? new ReactModels.ImageWithLink_OnPageEditing
                    {
                        Text = nameof(currentBlock.EditLink) + "." + nameof(currentBlock.EditLink.Text),
                        Url = nameof(currentBlock.EditLink) + "." + nameof(currentBlock.EditLink.Link)
                    }
                    : null
            };
        }
    }
}