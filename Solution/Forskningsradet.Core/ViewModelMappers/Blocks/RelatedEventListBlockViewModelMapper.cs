﻿using System.Collections.Generic;
using System.Linq;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.Web.Routing;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Extensions;
using Forskningsradet.Core.Facades.Contracts;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.QueryParameters;
using Forskningsradet.Core.Models.RequestModels;
using Forskningsradet.Core.Models.ViewModels.Blocks;
using Forskningsradet.Core.Models.ViewModels.Contracts;
using Forskningsradet.Core.Services;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.ViewModelMappers.Blocks
{
    public class RelatedEventListBlockViewModelMapper : BasePartialViewModelMapperWithQuery<RelatedEventListBlock>, IPartialViewModelMapper
    {
        private readonly IUrlResolver _urlResolver;
        private readonly IEventListService _eventListService;
        private readonly CategoryRepository _categoryRepository;
        private readonly IDateCardReactModelBuilder _dateCardReactModelBuilder;
        private readonly ILocalizationProvider _localizationProvider;
        private readonly IPageEditingAdapter _pageEditingAdapter;

        public RelatedEventListBlockViewModelMapper(
            IUrlResolver urlResolver,
            IEventListService eventListService,
            CategoryRepository categoryRepository,
            IDateCardReactModelBuilder dateCardReactModelBuilder,
            ILocalizationProvider localizationProvider,
            IPageEditingAdapter pageEditingAdapter)
        {
            _urlResolver = urlResolver;
            _eventListService = eventListService;
            _categoryRepository = categoryRepository;
            _dateCardReactModelBuilder = dateCardReactModelBuilder;
            _localizationProvider = localizationProvider;
            _pageEditingAdapter = pageEditingAdapter;
        }

        protected override IReactViewModel GetPartialViewModelInternal(RelatedEventListBlock currentContent, QueryParameterBase queryParameterBase) =>
            new ReactPartialViewModel(ListUsesPortfolioMode(currentContent) ? nameof(ReactModels.PortfolioContent) : currentContent.ReactComponentName(), BuildReactComponent(currentContent, queryParameterBase as RenderOptionQueryParameter ?? new RenderOptionQueryParameter()));

        public IReactViewModel GetPartialViewModel(IContentData currentContent) =>
            currentContent is RelatedEventListBlock content
                ? GetPartialViewModelInternal(content, null)
                : null;

        private bool ListUsesPortfolioMode(RelatedEventListBlock block) => block.PortfolioMode;

        private ReactModels.ReactComponent BuildReactComponent(RelatedEventListBlock currentBlock, RenderOptionQueryParameter renderOption)
        {
            var renderOptionQueryParameter = new RenderOptionQueryParameter { RenderWidthOption = renderOption.RenderWidthOption};
            var events = GetEvents(BuildEventListRequestModel(currentBlock));

            return ListUsesPortfolioMode(currentBlock)
                ? BuildPortfolioContent()
                : BuildRelatedEvents();

            ReactModels.ReactComponent BuildPortfolioContent() =>
                new ReactModels.PortfolioContent
                {
                    Title = currentBlock.Title,
                    Content = new ReactModels.ContentArea
                    {
                        Blocks = new List<ReactModels.ContentAreaItem>
                       {
                           new ReactModels.ContentAreaItem
                           {
                               Id = "0",
                               ComponentName = currentBlock.ReactComponentName(),
                               ComponentData = BuildRelatedEvents(true)
                           }
                       }
                    }
                };

            ReactModels.RelatedDates BuildRelatedEvents(bool portfolioMode = false) =>
                new ReactModels.RelatedDates
                {
                    RelatedContent = BuildRelatedContent(currentBlock, portfolioMode ? null : currentBlock.Title, portfolioMode ? ReactModels.RelatedContent_Border.None : ReactModels.RelatedContent_Border.Bottom),
                    Dates = events.Select(x => _dateCardReactModelBuilder.BuildReactModel(x, renderOptionQueryParameter, false, portfolioMode)).ToList(),
                    UsedInSidebar = renderOption.RenderWidthOption == RenderWidthOption.IsInSidebar,
                    IsGrid = portfolioMode
                };
        }

        private ReactModels.RelatedContent BuildRelatedContent(RelatedEventListBlock block, string title, ReactModels.RelatedContent_Border borderStyle) =>
            new ReactModels.RelatedContent
            {
                OnPageEditing = _pageEditingAdapter.PageIsInEditMode()
                    ? new ReactModels.RelatedContent_OnPageEditing
                    {
                        Title = nameof(block.Title)
                    }
                    : null,
                EmptyList = BuildEmptyList(),
                Title = title,
                Link = block.Button?.Link != null || _pageEditingAdapter.PageIsInEditMode()
                    ? new ReactModels.Link
                    {
                        OnPageEditing = _pageEditingAdapter.PageIsInEditMode()
                            ? nameof(block.Button)
                            : null,
                        Text = block.Button?.Text ?? GetLabel("GoToEventsButton"),
                        Url = BuildUrlWithQuery(block),
                        Id = block.Button?.AnchorId.EnsureValidHtmlId()
                    }
                    : null,
                Border = borderStyle
            };

        private string BuildUrlWithQuery(RelatedEventListBlock block)
        {
            if (block.Button?.Link is null)
                return null;

            var categories = new List<string>();
            categories.AddRange(GetSubjectQueryStrings());
            categories.AddRange(GetTargetGroupQueryStrings());
            categories.AddRange(GetCategoryQueryStrings());

            var query = categories.Any()
                ? "?" + string.Join("&", categories)
                : string.Empty;

            return block.Button?.Link.GetUrl(_urlResolver) is string url
                ? url + query
                : null;

            IEnumerable<string> GetSubjectQueryStrings() =>
                GetSelectedCategories(block)
                    .Where(x => x.Parent.Name == CategoryConstants.SubjectRoot)
                    .Select(x => $"{QueryParameters.Subject}={x.ID}");

            IEnumerable<string> GetTargetGroupQueryStrings() =>
                GetSelectedCategories(block)
                    .Where(x => x.Parent.Name == CategoryConstants.TargetGroupRoot)
                    .Select(x => $"{QueryParameters.TargetGroup}={x.ID}");

            IEnumerable<string> GetCategoryQueryStrings() =>
                GetSelectedCategories(block)
                    .Where(x => x.Parent.Name != CategoryConstants.SubjectRoot && x.Parent.Name != CategoryConstants.TargetGroupRoot)
                    .Select(x => $"{QueryParameters.Categories}={x.ID}");
        }

        private ReactModels.EmptyList BuildEmptyList() => new ReactModels.EmptyList { Text = GetLabel("EmptyEventList") };

        private EventListRequestModel BuildEventListRequestModel(RelatedEventListBlock block) =>
            new EventListRequestModel(
                GetSelectedCategories(block)
                    .Select(x => x.ID)
                    .ToArray(),
                GetPageRoot(block),
                block.NumberOfVisibleItems > 0 ? block.NumberOfVisibleItems : SearchConstants.DefaultRelatedContentSize
            );

        private static int GetPageRoot(RelatedEventListBlock block) =>
            block.PageRoot?.ID > 0
                ? block.PageRoot.ID
                : ContentReference.StartPage?.ID ?? 0;

        private IEnumerable<Category> GetSelectedCategories(RelatedEventListBlock block) =>
            (block as ICategorizable)?.Category.Select(x => _categoryRepository.Get(x)) ?? new List<Category>();

        private IEnumerable<EventPage> GetEvents(EventListRequestModel requestModel) =>
            _eventListService.Search(requestModel).Results;

        private string GetLabel(string key) => _localizationProvider.GetLabel(nameof(RelatedContentListBlock), key);
    }
}
