﻿using Forskningsradet.Core.Facades.Contracts;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ReactModels;
using Forskningsradet.Core.Models.ViewModels.Blocks;
using Forskningsradet.Core.Models.ViewModels.Contracts;

namespace Forskningsradet.Core.ViewModelMappers.Blocks
{
    public class QuoteBlockViewModelMapper : BasePartialViewModelMapper<QuoteBlock>
    {
        private readonly IPageEditingAdapter _pageEditingAdapter;

        public QuoteBlockViewModelMapper(IPageEditingAdapter pageEditingAdapter)
            => _pageEditingAdapter = pageEditingAdapter;

        protected override IReactViewModel GetViewModelForPartialInternal(QuoteBlock currentContent)
        {
            var reactComponent = BuildReactComponent(currentContent);
            return new ReactPartialViewModel(currentContent.ReactComponentName(), reactComponent);
        }

        private ReactComponent BuildReactComponent(QuoteBlock currentBlock)
            => new Quote
            {
                OnPageEditing = _pageEditingAdapter.PageIsInEditMode()
                    ? new Quote_OnPageEditing
                    {
                        Text = nameof(currentBlock.Quote),
                        QuoteBy = nameof(currentBlock.QuoteBy)
                    }
                    : null,
                Text = currentBlock.Quote,
                QuoteBy = currentBlock.QuoteBy
            };
    }
}
