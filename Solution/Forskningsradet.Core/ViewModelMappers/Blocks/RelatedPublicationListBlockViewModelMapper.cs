﻿using System.Collections.Generic;
using System.Linq;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.Web.Routing;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Extensions;
using Forskningsradet.Core.Facades.Contracts;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.Enums;
using Forskningsradet.Core.Models.QueryParameters;
using Forskningsradet.Core.Models.RequestModels;
using Forskningsradet.Core.Models.ViewModels.Blocks;
using Forskningsradet.Core.Models.ViewModels.Contracts;
using Forskningsradet.Core.Services;
using Forskningsradet.Core.Services.Contracts;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.ViewModelMappers.Blocks
{
    public class RelatedPublicationListBlockViewModelMapper : BasePartialViewModelMapperWithQuery<RelatedPublicationListBlock>, IPartialViewModelMapper
    {
        private readonly IPublicationReactModelBuilder _publicationReactModelBuilder;
        private readonly IPublicationListService _publicationListService;
        private readonly CategoryRepository _categoryRepository;
        private readonly IPageEditingAdapter _pageEditingAdapter;
        private readonly ILocalizationProvider _localizationProvider;
        private readonly IUrlResolver _urlResolver;

        public RelatedPublicationListBlockViewModelMapper(
            IPublicationReactModelBuilder publicationReactModelBuilder,
            IPublicationListService publicationListService,
            CategoryRepository categoryRepository,
            IPageEditingAdapter pageEditingAdapter,
            ILocalizationProvider localizationProvider,
            IUrlResolver urlResolver)
        {
            _publicationReactModelBuilder = publicationReactModelBuilder;
            _publicationListService = publicationListService;
            _categoryRepository = categoryRepository;
            _pageEditingAdapter = pageEditingAdapter;
            _localizationProvider = localizationProvider;
            _urlResolver = urlResolver;
        }

        protected override IReactViewModel GetPartialViewModelInternal(RelatedPublicationListBlock currentContent, QueryParameterBase queryParameterBase) =>
            new ReactPartialViewModel(currentContent.ReactComponentName(), BuildReactComponent(currentContent, queryParameterBase as RenderOptionQueryParameter ?? new RenderOptionQueryParameter()));

        public IReactViewModel GetPartialViewModel(IContentData currentContent) =>
            currentContent is RelatedPublicationListBlock content
                ? GetPartialViewModelInternal(content, null)
                : null;

        private ReactModels.ReactComponent BuildReactComponent(RelatedPublicationListBlock currentBlock, RenderOptionQueryParameter renderOption)
        {
            var publications = GetPublications(BuildPublications(currentBlock));
            return new ReactModels.RelatedPublications
            {
                RelatedContent = BuildRelatedContent(currentBlock),
                UsedInSidebar = renderOption.RenderWidthOption == RenderWidthOption.IsInSidebar,
                Publications = publications.Select(x => _publicationReactModelBuilder.BuildReactModel(x, PublicationMetadata.Small, false)).ToList()
            };
        }

        private PublicationRequest BuildPublications(RelatedPublicationListBlock currentBlock)
        {
            var categories = (currentBlock as ICategorizable)?.Category
                                .Select(x => _categoryRepository.Get(x))
                                .ToList();

            return new PublicationRequest(
                                null,
                                currentBlock.PageRoot?.ID ?? 5,
                                currentBlock.Year,
                                null,
                                1,
                                currentBlock.NumberOfVisibleItems,
                                categories);
        }

        private ReactModels.RelatedContent BuildRelatedContent(RelatedPublicationListBlock block) =>
            new ReactModels.RelatedContent
            {
                OnPageEditing = _pageEditingAdapter.PageIsInEditMode()
                    ? new ReactModels.RelatedContent_OnPageEditing
                    {
                        Title = nameof(block.Title)
                    }
                    : null,
                EmptyList = BuildEmptyList(),
                Title = block.Title,
                Link = block.Button?.Link != null || _pageEditingAdapter.PageIsInEditMode()
                    ? new ReactModels.Link
                    {
                        OnPageEditing = _pageEditingAdapter.PageIsInEditMode()
                            ? nameof(block.Button)
                            : null,
                        Text = block.Button?.Text ?? GetLabel("GoToListButton"),
                        Url = BuildUrlWithQuery(block),
                        Id = block.Button?.AnchorId.EnsureValidHtmlId()
                    }
                    : null,
                Border = ReactModels.RelatedContent_Border.Bottom
            };

        private string BuildUrlWithQuery(RelatedPublicationListBlock block)
        {
            if (block.Button?.Link is null)
                return null;

            var categories = GetSelectedCategories(block, CategoryConstants.SubjectRoot)
                .Select(x => $"{QueryParameters.Subject}={x.ID}")
                .ToList();
            categories.AddRange(GetSelectedCategories(block, CategoryConstants.TargetGroupRoot)
                .Select(x => $"{QueryParameters.TargetGroup}={x.ID}")
                .ToList());
            categories.AddRange(GetSelectedCategories(block, null)
                .Select(x => $"{QueryParameters.Categories}={x.ID}")
                .ToList());

            var query = categories.Any()
                ? "?" + string.Join("&", categories)
                : string.Empty;

            return block.Button?.Link.GetUrl(_urlResolver) is string url
                ? url.Contains("?") ? url : url + query
                : null;
        }

        private IEnumerable<PublicationBasePage> GetPublications(PublicationRequest requestModel) =>
             _publicationListService.Search(requestModel)
                .Results
                .OrderByDescending(x => x.StartPublish);

        private ReactModels.EmptyList BuildEmptyList() =>
            new ReactModels.EmptyList { Text = GetLabel("EmptyPublicationList") };

        private string GetLabel(string key) =>
            _localizationProvider.GetLabel(nameof(RelatedContentListBlock), key);

        private IEnumerable<Category> GetSelectedCategories(RelatedPublicationListBlock block, string categoryRoot) =>
            (block as ICategorizable)?.Category
               .Select(x => _categoryRepository.Get(x))
               .Where(x => categoryRoot == null && x.Parent.Name != CategoryConstants.SubjectRoot && x.Parent.Name != CategoryConstants.TargetGroupRoot
                           || categoryRoot != null && x.Parent.Name == categoryRoot) ?? new List<Category>();
    }
}
