﻿using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ViewModels.Blocks;
using Forskningsradet.Core.Models.ViewModels.Contracts;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.ViewModelMappers.Blocks
{
    public class GlobalMessageBlockViewModelMapper : BasePartialViewModelMapper<GlobalMessageBlock>
    {
        private readonly IMessageReactModelBuilder _messageReactModelBuilder;

        public GlobalMessageBlockViewModelMapper(IMessageReactModelBuilder messageReactModelBuilder)
        {
            _messageReactModelBuilder = messageReactModelBuilder;
        }

        protected override IReactViewModel GetViewModelForPartialInternal(GlobalMessageBlock currentContent)
        {
            var reactComponent = BuildReactComponent(currentContent);
            return new ReactPartialViewModel(currentContent.ReactComponentName(), reactComponent);
        }

        private ReactModels.ReactComponent BuildReactComponent(GlobalMessageBlock currentBlock) =>
            currentBlock.Text is null
                ? null
                : _messageReactModelBuilder.BuildReactModel(currentBlock.Text.ToString(), nameof(currentBlock.Text), currentBlock.Theme);
    }
}
