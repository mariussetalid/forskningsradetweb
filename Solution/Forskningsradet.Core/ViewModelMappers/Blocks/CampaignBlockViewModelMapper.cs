﻿using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ViewModels.Blocks;
using Forskningsradet.Core.Models.ViewModels.Contracts;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.ViewModelMappers.Blocks
{
    public class CampaignBlockViewModelMapper : BasePartialViewModelMapper<CampaignBlock>
    {
        private readonly ICampaignBlockReactModelBuilder _campaignBlockReactModelBuilder;

        public CampaignBlockViewModelMapper(ICampaignBlockReactModelBuilder campaignBlockReactModelBuilder)
        {
            _campaignBlockReactModelBuilder = campaignBlockReactModelBuilder;
        }

        protected override IReactViewModel GetViewModelForPartialInternal(CampaignBlock currentContent)
        {
            var viewModel = BuildReactComponent(currentContent);
            return new ReactPartialViewModel(currentContent.ReactComponentName(), viewModel);
        }

        private ReactModels.ReactComponent BuildReactComponent(CampaignBlock currentBlock) =>
            _campaignBlockReactModelBuilder.BuildReactModel(currentBlock);
    }
}
