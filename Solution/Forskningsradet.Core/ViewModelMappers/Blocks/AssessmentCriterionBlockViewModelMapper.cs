using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ReactModels;
using Forskningsradet.Core.Models.ViewModels.Blocks;
using Forskningsradet.Core.Models.ViewModels.Contracts;

namespace Forskningsradet.Core.ViewModelMappers.Blocks
{
    public class AssessmentCriterionBlockViewModelMapper : BasePartialViewModelMapper<AssessmentCriterionBlock>
    {
        private readonly IRichTextReactModelBuilder _richTextReactModelBuilder;

        public AssessmentCriterionBlockViewModelMapper(IRichTextReactModelBuilder richTextReactModelBuilder)
            => _richTextReactModelBuilder = richTextReactModelBuilder;

        protected override IReactViewModel GetViewModelForPartialInternal(AssessmentCriterionBlock currentContent)
            => new ReactPartialViewModel(currentContent.ReactComponentName(), BuildReactModel(currentContent));

        private ReactComponent BuildReactModel(AssessmentCriterionBlock currentContent)
            => new Models.ReactModels.RichTextBlock
            {
                Title = currentContent.Title,
                HeadingLevelOffset = 1,
                Text = _richTextReactModelBuilder.BuildReactModel(
                    currentContent.CriterionText,
                    nameof(currentContent.CriterionText))
            };
    }
}