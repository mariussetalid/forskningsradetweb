using System.Collections.Generic;
using System.Linq;
using EPiServer;
using EPiServer.Core;
using EPiServer.Web.Routing;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Extensions;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ContentModels.Media;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.ViewModels.Blocks;
using Forskningsradet.Core.Models.ViewModels.Contracts;
using Forskningsradet.Core.Repositories.Contracts;
using Forskningsradet.Core.Services;
using Forskningsradet.Core.Services.Contracts;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.ViewModelMappers.Blocks
{
    public class FrontPageHeaderBlockViewModelMapper : BasePartialViewModelMapper<FrontPageHeaderBlock>
    {
        private readonly IPageRepository _pageRepository;
        private readonly IContentLoader _contentLoader;
        private readonly IUrlResolver _urlResolver;
        private readonly IUrlCheckingService _urlCheckingService;
        private readonly IFluidImageReactModelBuilder _fluidImageReactModelBuilder;
        private readonly ILocalizationProvider _localizationProvider;

        public FrontPageHeaderBlockViewModelMapper(
            IPageRepository pageRepository,
            IContentLoader contentLoader,
            IUrlResolver urlResolver,
            IUrlCheckingService urlCheckingService,
            IFluidImageReactModelBuilder fluidImageReactModelBuilder,
            ILocalizationProvider localizationProvider
            )
        {
            _pageRepository = pageRepository;
            _contentLoader = contentLoader;
            _urlResolver = urlResolver;
            _urlCheckingService = urlCheckingService;
            _fluidImageReactModelBuilder = fluidImageReactModelBuilder;
            _localizationProvider = localizationProvider;
        }

        protected override IReactViewModel GetViewModelForPartialInternal(FrontPageHeaderBlock currentBlock) =>
            new ReactPartialViewModel(currentBlock.ReactComponentName(), BuildReactComponent(currentBlock));

        private ReactModels.ReactComponent BuildReactComponent(FrontPageHeaderBlock currentBlock)
        {
            return new ReactModels.FrontpageHeader
            {
                Title = currentBlock.Title,
                Text = !string.IsNullOrEmpty(currentBlock.Text) ? currentBlock.Text : GetLabel("DefaultHeaderText"),
                Image = _fluidImageReactModelBuilder.BuildReactModel(currentBlock.Image, nameof(currentBlock.Image)),
                Links = BuildLinks(currentBlock).ToList(),
                Theme = GetTheme(currentBlock.Theme)
            };
        }

        private IEnumerable<ReactModels.Link> BuildLinks(FrontPageHeaderBlock block)
        {
            var mainUrl = block.MainButtonUrl?.GetUrl(_urlResolver);
            var mainText = block.MainButtonText;
            if (!string.IsNullOrEmpty(mainUrl) && !string.IsNullOrEmpty(mainText))
            {
                yield return new ReactModels.Link
                {
                    Text = mainText,
                    Url = mainUrl,
                    IsExternal = block.MainButtonUrl.IsExternal(_urlCheckingService),
                    IsPrimary = true
                };
            }

            var secondaryUrl = block.SecondaryButtonUrl?.GetUrl(_urlResolver);
            var secondaryText = block.SecondaryButtonText;
            if (!string.IsNullOrEmpty(secondaryUrl) && !string.IsNullOrEmpty(secondaryText))
            {
                yield return new ReactModels.Link
                {
                    Text = secondaryText,
                    Url = secondaryUrl,
                    IsExternal = block.SecondaryButtonUrl.IsExternal(_urlCheckingService)
                };
            }
        }

        private static ReactModels.FrontpageHeader_Theme GetTheme(FrontPageHeaderBlockTheme style)
        {
            switch (style)
            {
                case FrontPageHeaderBlockTheme.DarkBlue:
                    return ReactModels.FrontpageHeader_Theme.DarkBlue;
                case FrontPageHeaderBlockTheme.GreyBlue:
                    return ReactModels.FrontpageHeader_Theme.GreyBlue;
                case FrontPageHeaderBlockTheme.Purple:
                    return ReactModels.FrontpageHeader_Theme.Purple;
                default:
                    return ReactModels.FrontpageHeader_Theme.GreyBlue;
            }
        }

        private string GetLabel(string key) =>
            _localizationProvider.GetLabel(nameof(FrontPage), key);
    }
}