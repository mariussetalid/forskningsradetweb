using System;
using System.Collections.Generic;
using System.Linq;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Facades.Contracts;
using Forskningsradet.Core.Factories;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.QueryParameters;
using Forskningsradet.Core.Models.ViewModels.Blocks;
using Forskningsradet.Core.Models.ViewModels.Contracts;
using Forskningsradet.Core.Services;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.ViewModelMappers.Blocks
{
    public class SubjectBlockViewModelMapper : BasePartialViewModelMapperWithQuery<SubjectBlock>, IPartialViewModelMapper
    {
        private readonly IRichTextReactModelBuilder _richTextReactModelBuilder;
        private readonly IContentAreaReactModelBuilder _contentAreaReactModelBuilder;
        private readonly ILocalizationProvider _localizationProvider;
        private readonly ICategoryLocalizationProvider _categoryLocalizationProvider;
        private readonly CategoryRepository _categoryRepository;
        private readonly IPageEditingAdapter _pageEditingAdapter;
        private readonly IViewModelFactory _viewModelFactory;
        private readonly IContentLoader _contentLoader;

        public SubjectBlockViewModelMapper(
            IRichTextReactModelBuilder richTextReactModelBuilder,
            IContentAreaReactModelBuilder contentAreaReactModelBuilder,
            ILocalizationProvider localizationProvider,
            ICategoryLocalizationProvider categoryLocalizationProvider,
            CategoryRepository categoryRepository,
            IPageEditingAdapter pageEditingAdapter,
            IViewModelFactory viewModelFactory,
            IContentLoader contentLoader)
        {
            _richTextReactModelBuilder = richTextReactModelBuilder;
            _contentAreaReactModelBuilder = contentAreaReactModelBuilder;
            _localizationProvider = localizationProvider;
            _categoryLocalizationProvider = categoryLocalizationProvider;
            _categoryRepository = categoryRepository;
            _pageEditingAdapter = pageEditingAdapter;
            _viewModelFactory = viewModelFactory;
            _contentLoader = contentLoader;
        }

        protected override IReactViewModel GetPartialViewModelInternal(SubjectBlock currentBlock, QueryParameterBase queryParameterBase)
        {
            var query = queryParameterBase as SubjectBlockQueryParameter ?? new SubjectBlockQueryParameter();
            return new ReactPartialViewModel(query.UseAlternativeDesign ? currentBlock.ReactComponentName() : nameof(ReactModels.AccordionWithContentAreaOld), BuildReactComponent(currentBlock, query));
        }

        public IReactViewModel GetPartialViewModel(IContentData currentContent) =>
            currentContent is SubjectBlock content
                ? GetPartialViewModelInternal(content, new SubjectBlockQueryParameter { UseAlternativeDesign = true })
                : null;

        private ReactModels.ReactComponent BuildReactComponent(SubjectBlock currentBlock, SubjectBlockQueryParameter query)
        {
            var title = currentBlock.SubjectName;
            var htmlId = currentBlock.SubjectCode;
            if (string.IsNullOrEmpty(currentBlock.SubjectName))
            {
                var category = _categoryRepository.Get(currentBlock.Subject);
                if (category is null)
                    return null;

                title = _categoryLocalizationProvider.GetCategoryName(category.ID);
                htmlId = category.Name;
            }

            return query.UseAlternativeDesign
                ? BuildAccordionForAlternativeDesign()
                : BuildAccordionForOldDesign();

            ReactModels.ReactComponent BuildAccordionForOldDesign() =>
                new ReactModels.AccordionWithContentAreaOld
                {
                    Title = title,
                    Description = currentBlock.Description,
                    HtmlId = htmlId,
                    Text = currentBlock.SubjectChildren,
                    RichText = _richTextReactModelBuilder.BuildReactModel(currentBlock.SubjectChildrenDescription, nameof(currentBlock.SubjectChildrenDescription)),
                    Content = HasContent(currentBlock) ? BuildContent() : null,
                    Accordion = new ReactModels.Accordion
                    {
                        Guid = Guid.NewGuid().ToString(),
                        CollapseLabel = GetLabel("CollapseLabel"),
                        ExpandLabel = GetLabel("ExpandLabel"),
                        InitiallyOpen = false
                    },
                    OnPageEditing = _pageEditingAdapter.PageIsInEditMode()
                    ? new ReactModels.AccordionWithContentAreaOld_OnPageEditing
                    {
                        Title = nameof(currentBlock.Description)
                    }
                    : null
                };

            ReactModels.ReactComponent BuildAccordionForAlternativeDesign() =>
                new ReactModels.AccordionWithContentAreaList
                {
                    Title = title,
                    Description = currentBlock.Description,
                    HtmlId = htmlId,
                    Text = currentBlock.SubjectChildren,
                    RichText = _richTextReactModelBuilder.BuildReactModel(currentBlock.SubjectChildrenDescription, nameof(currentBlock.SubjectChildrenDescription)),
                    Content = HasContent(currentBlock) ? BuildAlternativeDesignContent() : null,
                    Accordion = new ReactModels.Accordion
                    {
                        Guid = Guid.NewGuid().ToString(),
                        CollapseLabel = GetLabel("CollapseLabel"),
                        ExpandLabel = GetLabel("ExpandLabel"),
                        InitiallyOpen = false
                    },
                    OnPageEditing = _pageEditingAdapter.PageIsInEditMode()
                    ? new ReactModels.AccordionWithContentAreaList_OnPageEditing
                    {
                        Title = nameof(currentBlock.Description)
                    }
                    : null
                };

            ReactModels.ContentArea BuildContent() =>
                _contentAreaReactModelBuilder.BuildReactModel(BuildContentAreaItems(), nameof(currentBlock.Priorities));

            IList<ReactModels.ContentAreaItem> BuildContentAreaItems() =>
                currentBlock.Priorities?.FilteredItems
                    .Select(x => _contentLoader.Get<IContent>(x.ContentLink))
                    .Select(BuildItem)
                    .Where(x => !(x is null))
                    .ToList();

            ReactModels.ContentAreaItem BuildItem(IContent item)
            {
                if (item is SubjectPriorityBlock t)
                {
                    return new ReactModels.ContentAreaItem
                    {
                        Id = Guid.NewGuid().ToString(),
                        ComponentData = BuildTextWithSidebar(t),
                        ComponentName = nameof(ReactModels.TextWithSidebar),
                        OnPageEditing = null,
                        Size = ReactModels.ContentAreaItem_Size.None
                    };
                }
                return null;
            }

            ReactModels.TextWithSidebar BuildTextWithSidebar(SubjectPriorityBlock block) =>
                new ReactModels.TextWithSidebar
                {
                    Text = _richTextReactModelBuilder.BuildReactModel(block.Text, null),
                    Title = block.Title,
                    HtmlId = block.AnchorId,
                    Sidebar = block.RightBlockArea?.FilteredItems.Any() == true ? _contentAreaReactModelBuilder.BuildReactModel(block.RightBlockArea, null) : null,
                };

            IList<ReactModels.AccordionWithContentArea> BuildAlternativeDesignContent() =>
                currentBlock.Priorities?.FilteredItems
                    .Select(x => _contentLoader.Get<IContent>(x.ContentLink))
                    .Select(BuildAccordionWithContentArea)
                    .Where(x => !(x is null))
                    .ToList() ?? new List<ReactModels.AccordionWithContentArea>();

            ReactModels.AccordionWithContentArea BuildAccordionWithContentArea(IContent item)
            {
                var viewModel = _viewModelFactory.GetPartialViewModel(item, new SubjectPriorityBlockQueryParameter { PageReference = query.PageReference });
                return viewModel.Model as ReactModels.AccordionWithContentArea;
            }
        }

        private bool HasContent(SubjectBlock currentBlock) =>
            currentBlock.Priorities?.FilteredItems.Count() > 0
            || currentBlock.SubjectChildren != null
            || currentBlock.SubjectChildrenDescription != null;

        private string GetLabel(string key) => _localizationProvider.GetLabel(nameof(SubjectBlock), key);
    }
}