﻿using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ViewModels.Blocks;
using Forskningsradet.Core.Models.ViewModels.Contracts;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.ViewModelMappers.Blocks
{
    public class ProcessBlockViewModelMapper : BasePartialViewModelMapper<ProcessBlock>
    {
        private readonly IProcessReactModelBuilder _processReactModelBuilder;

        public ProcessBlockViewModelMapper(IProcessReactModelBuilder processReactModelBuilder) =>
            _processReactModelBuilder = processReactModelBuilder;

        protected override IReactViewModel GetViewModelForPartialInternal(ProcessBlock currentBlock)
        {
            var reactModel = BuildReactModel(currentBlock);
            return new ReactPartialViewModel(currentBlock.ReactComponentName(), reactModel);
        }

        private ReactModels.Process BuildReactModel(ProcessBlock currentBlock) =>
            _processReactModelBuilder.BuildReactModel(currentBlock.Title, currentBlock.IntroText, currentBlock.ProcessItems, currentBlock.Style);
    }
}