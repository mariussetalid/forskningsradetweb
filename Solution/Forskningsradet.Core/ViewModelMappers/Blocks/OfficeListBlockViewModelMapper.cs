﻿using System.Collections.Generic;
using System.Linq;
using EPiServer.Web.Routing;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Facades.Contracts;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ContentModels.PropertyList;
using Forskningsradet.Core.Models.ViewModels.Blocks;
using Forskningsradet.Core.Models.ViewModels.Contracts;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.ViewModelMappers.Blocks
{
    public class OfficeListBlockViewModelMapper : BasePartialViewModelMapper<OfficeListBlock>
    {
        private readonly IUrlResolver _urlResolver;
        private readonly IContentAreaReactModelBuilder _contentAreaReactModelBuilder;
        private readonly IPageRouteHelper _pageRouteHelper;
        private readonly IPageEditingAdapter _pageEditingAdapter;

        public OfficeListBlockViewModelMapper(IUrlResolver urlResolver, IContentAreaReactModelBuilder contentAreaReactModelBuilder, IPageRouteHelper pageRouteHelper, IPageEditingAdapter pageEditingAdapter)
        {
            _urlResolver = urlResolver;
            _contentAreaReactModelBuilder = contentAreaReactModelBuilder;
            _pageRouteHelper = pageRouteHelper;
            _pageEditingAdapter = pageEditingAdapter;
        }

        protected override IReactViewModel GetViewModelForPartialInternal(OfficeListBlock currentBlock)
        {
            var reactComponent = BuildReactModel(currentBlock);
            return new ReactPartialViewModel(currentBlock.ReactComponentName(), reactComponent);
        }

        private ReactModels.OfficeListBlock BuildReactModel(OfficeListBlock currentBlock)
            => new ReactModels.OfficeListBlock
            {
                Links = BuildLinks(currentBlock),
                Offices = _contentAreaReactModelBuilder.BuildReactModel(currentBlock.Offices, nameof(currentBlock.Offices))
            };

        private IList<ReactModels.OfficeListBlock_Links> BuildLinks(OfficeListBlock currentBlock)
            => currentBlock.Links != null && currentBlock.Links.Any()
                ? MapLinks(currentBlock.Links)
                : new List<ReactModels.OfficeListBlock_Links>();

        private IList<ReactModels.OfficeListBlock_Links> MapLinks(IEnumerable<SortableLink> currentBlockLinks)
            => currentBlockLinks
                ?.OrderBy(x => x.SortOrder)
                .Select(MapToReactLinksItem)
                .ToList();

        private ReactModels.OfficeListBlock_Links MapToReactLinksItem(SortableLink link)
            => new ReactModels.OfficeListBlock_Links
            {
                Link = BuildLink(link),
                Id = link.SortOrder.ToString(),
                IsActive = IsLinkToCurrentPage(link)
            };

        private bool IsLinkToCurrentPage(SortableLink link)
            => link.Link
                .Equals(_pageRouteHelper.PageLink);

        private ReactModels.Link BuildLink(SortableLink link)
            => new ReactModels.Link
            {
                OnPageEditing = _pageEditingAdapter.PageIsInEditMode()
                        ? nameof(link.Title)
                        : null,
                Text = link.Title,
                Url = _urlResolver.GetUrl(link.Link)
            };
    }
}
