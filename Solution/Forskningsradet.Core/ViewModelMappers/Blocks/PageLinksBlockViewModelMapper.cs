﻿using System.Collections.Generic;
using System.Linq;
using EPiServer;
using EPiServer.Core;
using EPiServer.Web.Routing;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Facades.Contracts;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.ViewModels.Blocks;
using Forskningsradet.Core.Models.ViewModels.Contracts;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.ViewModelMappers.Blocks
{
    public class PageLinksBlockViewModelMapper : BasePartialViewModelMapper<PageLinksBlock>
    {
        private readonly IContentLoader _contentLoader;
        private readonly IUrlResolver _urlResolver;
        private readonly IContentAreaReactModelBuilder _contentAreaReactModelBuilder;
        private readonly IPageEditingAdapter _pageEditingAdapter;

        public PageLinksBlockViewModelMapper(IContentLoader contentLoader, IUrlResolver urlResolver, IContentAreaReactModelBuilder contentAreaReactModelBuilder, IPageEditingAdapter pageEditingAdapter)
        {
            _contentLoader = contentLoader;
            _urlResolver = urlResolver;
            _contentAreaReactModelBuilder = contentAreaReactModelBuilder;
            _pageEditingAdapter = pageEditingAdapter;
        }

        protected override IReactViewModel GetViewModelForPartialInternal(PageLinksBlock currentContent)
        {
            return ShouldRenderDefaultComponent()
                ? new ReactPartialViewModel(currentContent.ReactComponentName(), BuildReactComponent(currentContent))
                : new ReactPartialViewModel(nameof(ReactModels.PortfolioContent), BuildPortfolioReactComponent(currentContent));

            bool ShouldRenderDefaultComponent() =>
                currentContent.SecondPageGroup?.FilteredItems.Any() == true
                || !string.IsNullOrEmpty(currentContent.Text);
        }

        private ReactModels.ReactComponent BuildReactComponent(PageLinksBlock currentBlock) =>
            new ReactModels.PageLinksBlock
            {
                OnPageEditing = _pageEditingAdapter.PageIsInEditMode()
                    ? new ReactModels.PageLinksBlock_OnPageEditing
                    {
                        Title = nameof(currentBlock.Title),
                        Text = nameof(currentBlock.Text)
                    }
                    : null,
                Title = currentBlock.Title,
                Text = currentBlock.Text,
                HighlightedLinks = BuildContentArea(currentBlock.FirstPageGroup, nameof(currentBlock.FirstPageGroup)),
                LinkLists = new List<ReactModels.PageLinksBlock_LinkLists>
                {
                    new ReactModels.PageLinksBlock_LinkLists
                    {
                        Id = "0",
                        Links = BuildContentArea(currentBlock.SecondPageGroup, nameof(currentBlock.SecondPageGroup))
                    }
                }
            };

        private ReactModels.ReactComponent BuildPortfolioReactComponent(PageLinksBlock currentBlock) =>
            new ReactModels.PortfolioContent
            {
                Title = currentBlock.Title,
                Content = BuildContentArea(currentBlock.FirstPageGroup, nameof(currentBlock.FirstPageGroup)),
            };

        private ReactModels.ContentArea BuildContentArea(ContentArea contentArea, string propertyName)
        {
            return _contentAreaReactModelBuilder.BuildReactModel(BuildContentAreaItems(), propertyName);

            List<ReactModels.ContentAreaItem> BuildContentAreaItems() =>
                contentArea
                    ?.FilteredItems
                    .Select(BuildContentAreaItemComponent)
                    .Where(x => x != null)
                    .ToList() ?? new List<ReactModels.ContentAreaItem>();
        }

        private ReactModels.ContentAreaItem BuildContentAreaItemComponent(ContentAreaItem item, int index)
        {
            return new ReactModels.ContentAreaItem
            {
                ComponentData = BuildLinkWithText(_contentLoader.Get<EditorialPage>(item.ContentLink)),
                ComponentName = nameof(ReactModels.LinkWithText),
                Id = index.ToString(),
                Size = ReactModels.ContentAreaItem_Size.None
            };

            ReactModels.LinkWithText BuildLinkWithText(EditorialPage content) =>
                new ReactModels.LinkWithText
                {
                    Link = new ReactModels.Link { Url = _urlResolver.GetUrl(content.PageLink), Text = content.ListTitle ?? content.PageName },
                    Text = content.ListIntro
                };
        }
    }
}