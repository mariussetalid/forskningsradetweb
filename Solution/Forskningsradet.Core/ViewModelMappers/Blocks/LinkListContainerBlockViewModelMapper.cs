﻿using System;
using System.Collections.Generic;
using System.Linq;
using EPiServer;
using EPiServer.Core;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.ViewModels.Blocks;
using Forskningsradet.Core.Models.ViewModels.Contracts;
using Forskningsradet.Core.Services.Contracts;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.ViewModelMappers.Blocks
{
    public class LinkListContainerBlockViewModelMapper : BasePartialViewModelMapper<LinkListContainerBlock>
    {
        private readonly IContentLoader _contentLoader;
        private readonly ILinkListReactModelBuilder _linkListReactModelBuilder;
        private readonly IContentAreaReactModelBuilder _contentAreaReactModelBuilder;
        private readonly ILinkWithTextReactModelBuilder _linkWithTextReactModelBuilder;
        private readonly IPersonPageStringBuilder _personPageStringBuilder;
        private const int MaxLengthForNestedTitle = 50;

        public LinkListContainerBlockViewModelMapper(
            IContentLoader contentLoader,
            ILinkListReactModelBuilder linkListReactModelBuilder,
            IContentAreaReactModelBuilder contentAreaReactModelBuilder,
            ILinkWithTextReactModelBuilder linkWithTextReactModelBuilder,
            IPersonPageStringBuilder personPageStringBuilder)
        {
            _contentLoader = contentLoader;
            _linkListReactModelBuilder = linkListReactModelBuilder;
            _contentAreaReactModelBuilder = contentAreaReactModelBuilder;
            _linkWithTextReactModelBuilder = linkWithTextReactModelBuilder;
            _personPageStringBuilder = personPageStringBuilder;
        }

        protected override IReactViewModel GetViewModelForPartialInternal(LinkListContainerBlock currentContent) =>
            new ReactPartialViewModel(currentContent.ReactComponentName(), BuildReactComponent(currentContent));

        private ReactModels.ReactComponent BuildReactComponent(LinkListContainerBlock currentBlock) =>
            new ReactModels.PortfolioContent
            {
                Title = currentBlock.Title,
                Content = BuildContent(currentBlock),
                SingleColumn = currentBlock.SingleColumn
            };

        private ReactModels.ContentArea BuildContent(LinkListContainerBlock currentBlock)
        {
            if (ContentAreaHasItems(currentBlock.LinkList))
                return BuildContentAreaComponent(currentBlock.LinkList, BuildLinkListComponents, nameof(currentBlock.LinkList));

            if (ContentAreaHasItems(currentBlock.ContactList))
                return BuildContentAreaComponent(currentBlock.ContactList, BuildContactListComponents, nameof(currentBlock.ContactList));

            if (ContentAreaHasItems(currentBlock.LinkWithTextList))
                return BuildContentAreaComponent(currentBlock.LinkWithTextList, BuildLinkWithTextComponents, nameof(currentBlock.LinkWithTextList));

            return BuildDefaultContentAreaComponent();

            bool ContentAreaHasItems(ContentArea contentArea) =>
                contentArea != null && contentArea.FilteredItems.Any();

            ReactModels.ContentArea BuildContentAreaComponent(ContentArea contentArea, Func<IEnumerable<ContentAreaItem>, IEnumerable<ReactModels.ContentAreaItem>> MapToNestedContentAreaReactModels, string propertyName)
            {
                var nestedContentAreaItems = contentArea is null
                    ? new List<ReactModels.ContentAreaItem>()
                    : MapToNestedContentAreaReactModels(contentArea.FilteredItems).ToList();

                return _contentAreaReactModelBuilder.BuildReactModel(nestedContentAreaItems, propertyName);
            }

            ReactModels.ContentArea BuildDefaultContentAreaComponent()
            {

                var listWithDefaultLinkList = new List<ReactModels.ContentAreaItem>
                {
                    new ReactModels.ContentAreaItem
                    {
                        ComponentData = new ReactModels.LinkLists(),
                        ComponentName = nameof(ReactModels.LinkLists),
                    }
                };

                return _contentAreaReactModelBuilder.BuildReactModel(listWithDefaultLinkList, nameof(currentBlock.LinkList));
            }
        }

        private IEnumerable<ReactModels.ContentAreaItem> BuildLinkListComponents(IEnumerable<ContentAreaItem> items)
        {
            yield return new ReactModels.ContentAreaItem
            {
                ComponentData = BuildNestedLinkList(),
                ComponentName = nameof(ReactModels.LinkLists),
                Id = "0",
                Size = ReactModels.ContentAreaItem_Size.None
            };

            ReactModels.LinkLists BuildNestedLinkList() =>
                new ReactModels.LinkLists
                {
                    NestedLinks = items
                    .Select(x => _contentLoader.Get<LinkListBlock>(x.ContentLink))
                    .Select(x => _linkListReactModelBuilder.BuildNestedLinksReactModel(x.Title?.Length > MaxLengthForNestedTitle ? x.Title.Substring(0, MaxLengthForNestedTitle) : x.Title, x.LinkItems))
                    .ToList()
                };
        }

        private IEnumerable<ReactModels.ContentAreaItem> BuildContactListComponents(IEnumerable<ContentAreaItem> items)
        {
            yield return new ReactModels.ContentAreaItem
            {
                ComponentData = BuildContactList(),
                ComponentName = nameof(ReactModels.ContactList),
                Id = "0",
                Size = ReactModels.ContentAreaItem_Size.None
            };

            ReactModels.ContactList BuildContactList() =>
                new ReactModels.ContactList
                {
                    ContactGroups = items
                        .Select(x => _contentLoader.Get<ContactBlock>(x.ContentLink))
                        .Select(BuildContactListItem)
                        .ToList()
                };

            ReactModels.ContactListItem BuildContactListItem(ContactBlock contactBlock) =>
                new ReactModels.ContactListItem
                {
                    Title = contactBlock.Title,
                    ContactLists = BuildContactLists(contactBlock.Contacts?.FilteredItems, contactBlock.ShowPhoneNumber, contactBlock.ShowJobTitle)
                };

            IList<ReactModels.ContactListItem_ContactLists> BuildContactLists(IEnumerable<ContentAreaItem> contacts, bool showPhoneNumber, bool showJobTitle)
            {
                if (contacts is null)
                    return new List<ReactModels.ContactListItem_ContactLists>();

                return contacts.Select(x => BuildList(x, showPhoneNumber, showJobTitle))
                    .Where(x => x != null)
                    .ToList() ?? null;
            }

            ReactModels.ContactListItem_ContactLists BuildList(ContentAreaItem x, bool showPhoneNumber, bool showJobTitle)
            {
                if (_contentLoader.TryGet(x.ContentLink, out IContentData item))
                {
                    if (item is PersonPage page)
                        return BuildPersonPageLinks(page, showPhoneNumber, showJobTitle);

                    if (item is NonPersonContactBlock block)
                        return BuildNonPersonContactBlockLinks(block, showPhoneNumber);
                }

                return null;
            }
        }

        private ReactModels.ContactListItem_ContactLists BuildPersonPageLinks(PersonPage page, bool showPhoneNumber, bool showJobTitle)
        {
            var phone = !string.IsNullOrWhiteSpace(page.Mobile)
                ? page.Mobile
                : page.Phone;

            return BuildContactLinks(_personPageStringBuilder.BuildTitle(page), page.JobTitle, page.Email, phone, showPhoneNumber, showJobTitle);
        }

        private ReactModels.ContactListItem_ContactLists BuildNonPersonContactBlockLinks(NonPersonContactBlock block, bool showPhoneNumber) =>
            BuildContactLinks(block.Title, null, block.Email, block.Phone, showPhoneNumber, false);

        private ReactModels.ContactListItem_ContactLists BuildContactLinks(string name, string jobTitle, string email, string phone, bool showPhoneNumber, bool showJobTitle) =>
            new ReactModels.ContactListItem_ContactLists
            {
                Name = new ReactModels.ContactListItem_ContactLists_Name
                {
                    Text = name,
                    Position = showJobTitle && !string.IsNullOrEmpty(jobTitle) ? jobTitle : null
                },
                Email = !string.IsNullOrWhiteSpace(email)
                    ? new ReactModels.Link
                    {
                        Text = email,
                        Url = _personPageStringBuilder.BuildEmailUrlScheme(email)
                    }
                    : null,
                Phone = !string.IsNullOrWhiteSpace(phone) && showPhoneNumber
                    ? new ReactModels.Link
                    {
                        Text = phone,
                        Url = _personPageStringBuilder.BuildPhoneUrlScheme(phone)
                    }
                    : null
            };

        private IEnumerable<ReactModels.ContentAreaItem> BuildLinkWithTextComponents(IEnumerable<ContentAreaItem> items)
        {
            return items.Select(x => _contentLoader.Get<LinkWithTextBlock>(x.ContentLink))
                        .Select(BuildContentAreaItem);

            ReactModels.ContentAreaItem BuildContentAreaItem(LinkWithTextBlock item, int index) =>
                new ReactModels.ContentAreaItem
                {
                    ComponentData = BuildLinkWithText(item),
                    ComponentName = nameof(ReactModels.LinkWithText),
                    Id = index.ToString(),
                    Size = ReactModels.ContentAreaItem_Size.None
                };

            ReactModels.LinkWithText BuildLinkWithText(LinkWithTextBlock currentBlock) =>
                _linkWithTextReactModelBuilder.BuildReactModel(currentBlock.LinkTitle, currentBlock.Link, currentBlock.Text, null);
        }
    }
}