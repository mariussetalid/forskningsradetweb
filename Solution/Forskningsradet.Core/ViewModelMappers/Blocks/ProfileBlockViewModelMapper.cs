﻿using EPiServer;
using EPiServer.Web.Routing;
using Forskningsradet.Core.Facades.Contracts;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ContentModels.Media;
using Forskningsradet.Core.Models.ViewModels.Blocks;
using Forskningsradet.Core.Models.ViewModels.Contracts;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.ViewModelMappers.Blocks
{
    public class ProfileBlockViewModelMapper : BasePartialViewModelMapper<ProfileBlock>
    {
        private readonly IUrlResolver _urlResolver;
        private readonly IContentLoader _contentLoader;
        private readonly IPageEditingAdapter _pageEditingAdapter;

        public ProfileBlockViewModelMapper(IUrlResolver urlResolver, IContentLoader contentLoader, IPageEditingAdapter pageEditingAdapter)
        {
            _urlResolver = urlResolver;
            _contentLoader = contentLoader;
            _pageEditingAdapter = pageEditingAdapter;
        }

        protected override IReactViewModel GetViewModelForPartialInternal(ProfileBlock currentContent)
        {
            var reactComponent = BuildReactComponent(currentContent);
            return new ReactPartialViewModel(currentContent.ReactComponentName(), reactComponent);
        }

        private ReactModels.ReactComponent BuildReactComponent(ProfileBlock currentBlock)
        {
            var imageFile = currentBlock.Image != null
                ? _contentLoader.Get<ImageFile>(currentBlock.Image)
                : null;
            return new ReactModels.PersonBlock
            {
                OnPageEditing = _pageEditingAdapter.PageIsInEditMode()
                    ? new ReactModels.PersonBlock_OnPageEditing
                    {
                        Company = nameof(currentBlock.Company),
                        JobTitle = nameof(currentBlock.JobTitle),
                        Name = nameof(currentBlock.ProfileName),
                        Text = nameof(currentBlock.MainIntro)
                    }
                    : null,
                Image = imageFile != null || _pageEditingAdapter.PageIsInEditMode()
                    ? new ReactModels.Image
                    {
                        Alt = imageFile?.AltText ?? string.Empty,
                        Src = imageFile != null ? _urlResolver.GetUrl(imageFile.ContentLink) : string.Empty,
                        OnPageEditing = _pageEditingAdapter.PageIsInEditMode()
                            ? new ReactModels.Image_OnPageEditing
                            {
                                Image = nameof(currentBlock.Image)
                            }
                            : null
                    }
                    : null,
                Company = !string.IsNullOrEmpty(currentBlock.CultureSpecificCompany) ? currentBlock.CultureSpecificCompany : currentBlock.Company,
                JobTitle = currentBlock.JobTitle,
                Name = !string.IsNullOrEmpty(currentBlock.CultureSpecificProfileName) ? currentBlock.CultureSpecificProfileName : currentBlock.ProfileName,
                Text = currentBlock.MainIntro
            };
        }
    }
}
