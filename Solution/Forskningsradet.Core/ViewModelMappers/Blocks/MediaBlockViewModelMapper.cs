﻿using EPiServer;
using EPiServer.Core;
using EPiServer.Web.Routing;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Extensions;
using Forskningsradet.Core.Facades.Contracts;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ContentModels.Media;
using Forskningsradet.Core.Models.QueryParameters;
using Forskningsradet.Core.Models.ViewModels.Blocks;
using Forskningsradet.Core.Models.ViewModels.Contracts;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.ViewModelMappers.Blocks
{
    public class MediaBlockViewModelMapper : BasePartialViewModelMapperWithQuery<MediaBlock>, IPartialViewModelMapper
    {
        private readonly IFluidImageReactModelBuilder _fluidImageReactModelBuilder;
        private readonly IContentLoader _contentLoader;
        private readonly IPageEditingAdapter _pageEditingAdapter;
        private readonly IUrlResolver _urlResolver;

        public MediaBlockViewModelMapper(IFluidImageReactModelBuilder fluidImageReactModelBuilder, IContentLoader contentLoader, IPageEditingAdapter pageEditingAdapter, IUrlResolver urlResolver)
        {
            _fluidImageReactModelBuilder = fluidImageReactModelBuilder;
            _contentLoader = contentLoader;
            _pageEditingAdapter = pageEditingAdapter;
            _urlResolver = urlResolver;
        }

        protected override IReactViewModel GetPartialViewModelInternal(MediaBlock currentContent, QueryParameterBase queryParameterBase) =>
            (queryParameterBase as RenderOptionQueryParameter)?.RenderMediaBlockOption == RenderMediaBlockOption.MediaWithCaption
                ? new ReactPartialViewModel(nameof(ReactModels.MediaWithCaption), BuildMediaWithCaptionReactModel(currentContent))
                : new ReactPartialViewModel(nameof(ReactModels.MediaBlock), BuildMediaBlockReactComponent(currentContent));

        public IReactViewModel GetPartialViewModel(IContentData currentContent) =>
            currentContent is MediaBlock content
                ? GetPartialViewModelInternal(content, null)
                : null;

        private ReactModels.MediaWithCaption BuildMediaWithCaptionReactModel(MediaBlock currentBlock)
        {
            var reactComponent = new ReactModels.MediaWithCaption
            {
                Title = currentBlock.Title,
                Url = currentBlock.Url.GetUrl(_urlResolver),
                Caption = currentBlock.Caption,
                OnPageEditing = _pageEditingAdapter.PageIsInEditMode()
                            ? new ReactModels.MediaWithCaption_OnPageEditing
                            {
                                Title = nameof(currentBlock.Title),
                                Caption = nameof(currentBlock.Caption)
                            }
                            : null
            };

            if (currentBlock.ImageBlock.Image != null)
                reactComponent.Image = GetImage(currentBlock.ImageBlock);
            else if (!(string.IsNullOrEmpty(currentBlock.VideoBlock.UrlOrEmbedCode)))
                reactComponent.YouTubeVideo = GetYoutubeVideo(currentBlock.VideoBlock);
            else if (!(string.IsNullOrEmpty(currentBlock.EmbedBlock.Src)))
                reactComponent.Embed = GetEmbed(currentBlock.EmbedBlock);

            return reactComponent;
        }

        private ReactModels.MediaBlock BuildMediaBlockReactComponent(MediaBlock currentBlock)
        {
            var reactComponent = new ReactModels.MediaBlock
            {
                Title = currentBlock.Title,
                Url = currentBlock.Url?.GetUrl(_urlResolver),
                OnPageEditing = _pageEditingAdapter.PageIsInEditMode()
                            ? new ReactModels.MediaBlock_OnPageEditing { Title = nameof(currentBlock.Title) }
                            : null,
            };

            if (currentBlock.ImageBlock.Image != null)
                reactComponent.Image = _fluidImageReactModelBuilder.BuildReactModel(GetImageFile(currentBlock.ImageBlock.Image), nameof(currentBlock.ImageBlock.Image));
            else if (!(string.IsNullOrEmpty(currentBlock.VideoBlock.UrlOrEmbedCode)))
                reactComponent.YouTubeVideo = GetYoutubeVideo(currentBlock.VideoBlock);
            else if (!(string.IsNullOrEmpty(currentBlock.EmbedBlock.Src)))
                reactComponent.Embed = GetEmbed(currentBlock.EmbedBlock);

            return reactComponent;
        }

        private ReactModels.Image GetImage(MediaBlockImageBlock currentBlock)
        {
            var imageFile = currentBlock.Image != null
                ? _contentLoader.Get<ImageFile>(currentBlock.Image)
                : null;
            return new ReactModels.Image
            {
                Alt = imageFile?.AltText ?? string.Empty,
                Src = imageFile is null ? string.Empty : _urlResolver.GetUrl(imageFile.ContentLink),
                OnPageEditing = _pageEditingAdapter.PageIsInEditMode()
                            ? new ReactModels.Image_OnPageEditing
                            {
                                Image = nameof(currentBlock.Image)
                            }
                            : null
            };
        }

        private ReactModels.YoutubeVideo GetYoutubeVideo(VideoBlock block) =>
            new ReactModels.YoutubeVideo
            {
                UrlOrEmbed = block.UrlOrEmbedCode
            };

        private ReactModels.EmbedBlock GetEmbed(MediaBlockEmbedBlock block) =>
            new ReactModels.EmbedBlock
            {
                Title = block.AlternativeText,
                Src = BuildSource(block.Src),
                Width = block.Width > 0 ? block.Width : (int?)null,
                Height = block.Height > 0 ? block.Height : (int?)null,
                OnPageEditing = _pageEditingAdapter.PageIsInEditMode()
                                ? nameof(block.Src)
                                : null,
            };

        private ImageFile GetImageFile(ContentReference image) =>
            image is null
                ? null
                : _contentLoader.Get<ImageFile>(image);

        private static string BuildSource(string src)
        {
            src = src?.Trim() ?? string.Empty;

            if (src.StartsWith(EmbedSourceConstants.Tableau.ValidSrcStart))
                src = $"{src}{EmbedSourceConstants.Tableau.SrcSuffix}";

            return src.StartsWith(EmbedSourceConstants.Tableau.ValidSrcStart)
                        || src.StartsWith(EmbedSourceConstants.Vbrick.ValidSrcStart)
                        || src.StartsWith(EmbedSourceConstants.ProjectDatabank.ValidSrcStart)
                        || src.StartsWith(EmbedSourceConstants.Facebook.ValidSrcStart)
                        || src.StartsWith(EmbedSourceConstants.Instagram.ValidSrcStart)
                    ? src
                    : null;
        }
    }
}
