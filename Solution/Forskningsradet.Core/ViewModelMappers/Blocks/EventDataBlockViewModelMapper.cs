﻿using System;
using System.Collections.Generic;
using System.Linq;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ViewModels.Blocks;
using Forskningsradet.Core.Models.ViewModels.Contracts;
using Forskningsradet.Core.Services;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.ViewModelMappers.Blocks
{
    public class EventDataBlockViewModelMapper : BasePartialViewModelMapper<EventDataBlock>
    {
        private readonly ILocalizationProvider _localizationProvider;

        public EventDataBlockViewModelMapper(ILocalizationProvider localizationProvider) =>
            _localizationProvider = localizationProvider;

        protected override IReactViewModel GetViewModelForPartialInternal(EventDataBlock currentContent)
        {
            var reactComponent = BuildReactComponent(currentContent);
            return new ReactPartialViewModel(currentContent.ReactComponentName(), reactComponent);
        }

        private ReactModels.ReactComponent BuildReactComponent(EventDataBlock currentContent)
        {
            var items = new List<ReactModels.EventData_Items>();

            if (currentContent.WhenList != null && currentContent.WhenList.Any(x => !string.IsNullOrEmpty(x)))
                items.Add(BuildEventDataItem("EventDataWhenTitle", currentContent.WhenList.ToArray()));

            if (!string.IsNullOrEmpty(currentContent.Where))
                items.Add(BuildEventDataItem("EventDataWhereTitle", currentContent.Where));

            if (currentContent.Type != EventType.None)
                items.Add(BuildEventDataItem("EventDataTypeTitle", _localizationProvider.GetEnumName(currentContent.Type)));

            if (!string.IsNullOrEmpty(currentContent.TargetGroup))
                items.Add(BuildEventDataItem("EventDataTargetGroupTitle", currentContent.TargetGroup));

            if (!string.IsNullOrEmpty(currentContent.Price))
                items.Add(BuildEventDataItem("EventDataPriceTitle", currentContent.Price));

            if (!string.IsNullOrEmpty(currentContent.Deadline))
                items.Add(BuildEventDataItem("EventDataDeadlineTitle", currentContent.Deadline));

            if (!string.IsNullOrEmpty(currentContent.Next))
                items.Add(BuildEventDataItem("EventDataNextTitle", currentContent.Next));

            return new ReactModels.EventData
            {
                Items = items
            };
        }

        private ReactModels.EventData_Items BuildEventDataItem(string labelKey, params string[] text) =>
            new ReactModels.EventData_Items
            {
                Label = GetEventDataLabel(labelKey),
                Text = BuildEventDataText(text)
            };

        private string GetEventDataLabel(string labelKey) =>
            _localizationProvider.GetLabel(nameof(ReactModels.EventPage), labelKey);

        private static IList<IList<string>> BuildEventDataText(params string[] data) =>
            data?.Select(x => (IList<string>)x?.Split(new[] { "\n" }, StringSplitOptions.RemoveEmptyEntries).ToList() ?? new List<string>())
                .ToList() ?? new List<IList<string>>();
    }
}
