﻿using EPiServer.Web.Routing;
using Forskningsradet.Core.Extensions;
using Forskningsradet.Core.Facades.Contracts;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ViewModels.Blocks;
using Forskningsradet.Core.Models.ViewModels.Contracts;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.ViewModelMappers.Blocks
{
    public class NumberBlockViewModelMapper : BasePartialViewModelMapper<NumberBlock>
    {
        private readonly IUrlResolver _urlResolver;
        private readonly IPageEditingAdapter _pageEditingAdapter;

        public NumberBlockViewModelMapper(IUrlResolver urlResolver, IPageEditingAdapter pageEditingAdapter)
        {
            _urlResolver = urlResolver;
            _pageEditingAdapter = pageEditingAdapter;
        }

        protected override IReactViewModel GetViewModelForPartialInternal(NumberBlock currentContent)
        {
            var reactComponent = BuildReactComponent(currentContent);
            return new ReactPartialViewModel(currentContent.ReactComponentName(), reactComponent);
        }

        private ReactModels.ReactComponent BuildReactComponent(NumberBlock currentBlock)
            => new ReactModels.NumberBlock
            {
                OnPageEditing = _pageEditingAdapter.PageIsInEditMode()
                    ? new ReactModels.NumberBlock_OnPageEditing
                    {
                        Description = nameof(currentBlock.Suffix),
                        Number = nameof(currentBlock.Number),
                        Text = nameof(currentBlock.Text),
                        Url = nameof(currentBlock.Link)
                    }
                    : null,
                Description = currentBlock.Suffix,
                Number = !string.IsNullOrEmpty(currentBlock.CultureSpecificNumber) ? currentBlock.CultureSpecificNumber : currentBlock.Number,
                Text = currentBlock.Text,
                Url = currentBlock.Link?.GetUrl(_urlResolver)
            };
    }
}
