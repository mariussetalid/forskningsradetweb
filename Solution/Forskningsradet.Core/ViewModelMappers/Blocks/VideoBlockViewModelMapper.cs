﻿using Forskningsradet.Core.Facades.Contracts;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ViewModels.Blocks;
using Forskningsradet.Core.Models.ViewModels.Contracts;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.ViewModelMappers.Blocks
{
    public class VideoBlockViewModelMapper : BasePartialViewModelMapper<VideoBlock>
    {
        private readonly IPageEditingAdapter _pageEditingAdapter;

        public VideoBlockViewModelMapper(IPageEditingAdapter pageEditingAdapter) =>
            _pageEditingAdapter = pageEditingAdapter;

        protected override IReactViewModel GetViewModelForPartialInternal(VideoBlock currentBlock)
        {
            var reactComponent = BuildReactComponent(currentBlock);
            return new ReactPartialViewModel(currentBlock.ReactComponentName(), reactComponent);
        }

        private ReactModels.ReactComponent BuildReactComponent(VideoBlock videoBlock) =>
            new ReactModels.YoutubeVideo
            {
                OnPageEditing = _pageEditingAdapter.PageIsInEditMode()
                ? nameof(videoBlock.UrlOrEmbedCode)
                : null,
                UrlOrEmbed = videoBlock.UrlOrEmbedCode
            };
    }
}
