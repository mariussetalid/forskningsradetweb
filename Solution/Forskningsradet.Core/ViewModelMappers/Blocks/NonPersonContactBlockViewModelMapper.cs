﻿using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ViewModels.Blocks;
using Forskningsradet.Core.Models.ViewModels.Contracts;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.ViewModelMappers.Blocks
{
    public class NonPersonContactBlockViewModelMapper : BasePartialViewModelMapper<NonPersonContactBlock>
    {
        private readonly IContactInfoReactModelBuilder _contactInfoReactModelBuilder;

        public NonPersonContactBlockViewModelMapper(IContactInfoReactModelBuilder contactInfoReactModelBuilder) =>
            _contactInfoReactModelBuilder = contactInfoReactModelBuilder;

        protected override IReactViewModel GetViewModelForPartialInternal(NonPersonContactBlock currentContent)
        {
            var viewModel = BuildPartialReactModel(currentContent);
            return new ReactPartialViewModel(currentContent.ReactComponentName(), viewModel);
        }

        private ReactModels.ReactComponent BuildPartialReactModel(NonPersonContactBlock page) =>
            _contactInfoReactModelBuilder.BuildReactModel(page, null, true);
    }
}
