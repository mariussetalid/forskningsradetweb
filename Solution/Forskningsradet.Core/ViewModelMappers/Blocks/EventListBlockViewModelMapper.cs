﻿using System.Collections.Generic;
using System.Linq;
using EPiServer;
using EPiServer.Core;
using EPiServer.Web.Routing;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Extensions;
using Forskningsradet.Core.Facades.Contracts;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.RequestModels;
using Forskningsradet.Core.Models.ViewModels.Blocks;
using Forskningsradet.Core.Models.ViewModels.Contracts;
using Forskningsradet.Core.Services;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.ViewModelMappers.Blocks
{
    public class EventListBlockViewModelMapper : BasePartialViewModelMapper<EventListBlock>
    {
        private readonly IUrlResolver _urlResolver;
        private readonly IEventListService _eventListService;
        private readonly IDateCardReactModelBuilder _dateCardReactModelBuilder;
        private readonly ILocalizationProvider _localizationProvider;
        private readonly IPageEditingAdapter _pageEditingAdapter;

        public EventListBlockViewModelMapper(IUrlResolver urlResolver, IEventListService eventListService, IDateCardReactModelBuilder dateCardReactModelBuilder, ILocalizationProvider localizationProvider, IPageEditingAdapter pageEditingAdapter)
        {
            _urlResolver = urlResolver;
            _eventListService = eventListService;
            _dateCardReactModelBuilder = dateCardReactModelBuilder;
            _localizationProvider = localizationProvider;
            _pageEditingAdapter = pageEditingAdapter;
        }

        protected override IReactViewModel GetViewModelForPartialInternal(EventListBlock currentContent) =>
            new ReactPartialViewModel(currentContent.ReactComponentName(), BuildReactComponent(currentContent));

        private ReactModels.ReactComponent BuildReactComponent(EventListBlock currentBlock) =>
            new ReactModels.FutureEvents
            {
                Title = currentBlock.Title,
                Text = currentBlock.Description,
                Link = currentBlock.Button?.Link != null || _pageEditingAdapter.PageIsInEditMode()
                    ? new ReactModels.Link
                    {
                        OnPageEditing = _pageEditingAdapter.PageIsInEditMode()
                            ? nameof(currentBlock.Button)
                            : null,
                        Text = currentBlock.Button?.Text ?? GetLabel("GoToEventsButton"),
                        Url = GetUrl(currentBlock.Button?.Link),
                        Id = currentBlock.Button?.AnchorId.EnsureValidHtmlId()
                    }
                    : null,
                Items = GetEvents(currentBlock.PageRoot)
            };

        private List<ReactModels.DateCard> GetEvents(PageReference pageRoot) =>
            _eventListService.Search(new EventListRequestModel(GetPageRoot(pageRoot), 3))
                .Results
                .Select(x => _dateCardReactModelBuilder.BuildReactModel(x))
                .ToList();

        private static int GetPageRoot(PageReference pageRoot) =>
            pageRoot?.ID > 0
                ? pageRoot.ID
                : ContentReference.StartPage?.ID ?? 0;

        private string GetUrl(Url url) => url.GetUrl(_urlResolver);

        private string GetLabel(string key) => _localizationProvider.GetLabel(nameof(EventListPage), key);
    }
}
