﻿using System.Collections.Generic;
using System.Linq;
using EPiServer;
using EPiServer.Web.Routing;
using Forskningsradet.Core.Facades.Contracts;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ContentModels.PropertyList;
using Forskningsradet.Core.Models.ViewModels.Blocks;
using Forskningsradet.Core.Models.ViewModels.Contracts;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.ViewModelMappers.Blocks
{
    public class OfficeBlockViewModelMapper : BasePartialViewModelMapper<OfficeBlock>
    {
        private readonly IUrlResolver _urlResolver;
        private readonly IPageEditingAdapter _pageEditingAdapter;

        public OfficeBlockViewModelMapper(IUrlResolver urlResolver, IPageEditingAdapter pageEditingAdapter)
        {
            _urlResolver = urlResolver;
            _pageEditingAdapter = pageEditingAdapter;
        }

        protected override IReactViewModel GetViewModelForPartialInternal(OfficeBlock currentBlock)
        {
            var reactModel = BuildReactModel(currentBlock);
            return new ReactPartialViewModel(currentBlock.ReactComponentName(), reactModel);
        }

        private ReactModels.OfficeBlock BuildReactModel(OfficeBlock currentBlock)
            => new ReactModels.OfficeBlock
            {
                Title = currentBlock.Title,
                Info = MapOfficeInfoToReactModel(currentBlock.Information)
            };

        private IList<ReactModels.OfficeBlockInfo> MapOfficeInfoToReactModel(IList<OfficeInformation> officeBlockInformation)
            => officeBlockInformation
                ?.Select(MapOfficeBlockInfoToReactComponent)
                .ToList();

        private ReactModels.OfficeBlockInfo MapOfficeBlockInfoToReactComponent(OfficeInformation informationEntry)
            => new ReactModels.OfficeBlockInfo
            {
                Title = informationEntry.Title,
                Text = informationEntry.Text,
                MoreText = informationEntry.MoreText,
                Link = informationEntry.Link != null
                        ? MapLinkToReactModel(informationEntry.Link, informationEntry.LinkText, nameof(informationEntry.LinkText))
                        : null
            };

        private ReactModels.Link MapLinkToReactModel(Url linkUrl, string linkText, string propertyName)
            => new ReactModels.Link
            {
                Text = linkText ?? linkUrl.OriginalString,
                Url = linkUrl.ToString().StartsWith("mailto:") ? linkUrl.ToString() : _urlResolver.GetUrl(linkUrl.ToString()),
                OnPageEditing = _pageEditingAdapter.PageIsInEditMode()
                    ? propertyName
                    : null
            };
    }
}
