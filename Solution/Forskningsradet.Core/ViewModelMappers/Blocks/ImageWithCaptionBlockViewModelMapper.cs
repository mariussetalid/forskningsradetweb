﻿using EPiServer;
using EPiServer.Web.Routing;
using Forskningsradet.Core.Facades.Contracts;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ContentModels.Media;
using Forskningsradet.Core.Models.ReactModels;
using Forskningsradet.Core.Models.ViewModels.Blocks;
using Forskningsradet.Core.Models.ViewModels.Contracts;

namespace Forskningsradet.Core.ViewModelMappers.Blocks
{
    public class ImageWithCaptionBlockViewModelMapper : BasePartialViewModelMapper<ImageWithCaptionBlock>
    {
        private readonly IUrlResolver _urlResolver;
        private readonly IPageEditingAdapter _pageEditingAdapter;
        private readonly IContentLoader _contentLoader;

        public ImageWithCaptionBlockViewModelMapper(IUrlResolver urlResolver, IPageEditingAdapter pageEditingAdapter, IContentLoader contentLoader)
        {
            _urlResolver = urlResolver;
            _pageEditingAdapter = pageEditingAdapter;
            _contentLoader = contentLoader;
        }

        protected override IReactViewModel GetViewModelForPartialInternal(ImageWithCaptionBlock currentBlock)
        {
            var reactModel = BuildReactModel(currentBlock);
            return new ReactPartialViewModel(currentBlock.ReactComponentName(), reactModel, BuildFullRefreshProperties());
        }

        private ReactComponent BuildReactModel(ImageWithCaptionBlock currentBlock)
        {
            var imageFile = currentBlock.Image != null
                ? _contentLoader.Get<ImageFile>(currentBlock.Image)
                : null;
            return new ImageWithCaption
            {
                Title = currentBlock.Title,
                Caption = currentBlock.Caption,
                Image = imageFile != null || _pageEditingAdapter.PageIsInEditMode()
                    ? new Image
                    {
                        OnPageEditing = _pageEditingAdapter.PageIsInEditMode()
                            ? new Image_OnPageEditing
                            {
                                Image = nameof(currentBlock.Image)
                            }
                            : null,
                        Alt = imageFile?.AltText ?? string.Empty,
                        Src = _urlResolver.GetUrl(imageFile?.ContentLink)
                    }
                    : null,
                EditorTheme = MapEditorTheme(currentBlock.Style),
                OnPageEditing = _pageEditingAdapter.PageIsInEditMode()
                    ? new ImageWithCaption_OnPageEditing
                    {
                        Caption = nameof(currentBlock.Caption)
                    }
                    : null
            };
        }

        private string[] BuildFullRefreshProperties()
            => new[]
            {
                nameof(ImageWithCaptionBlock.Image),
                nameof(ImageWithCaptionBlock.Caption),
            };

        private static ImageWithCaption_EditorTheme MapEditorTheme(ImageBlockStyle style)
        {
            switch (style)
            {
                case ImageBlockStyle.ThemeHalfWidth:
                    return ImageWithCaption_EditorTheme.HalfWidth;
                default:
                    return ImageWithCaption_EditorTheme.None;
            }
        }
    }
}
