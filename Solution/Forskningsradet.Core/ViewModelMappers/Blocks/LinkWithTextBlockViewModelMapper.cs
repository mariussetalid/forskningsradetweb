﻿using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ViewModels.Blocks;
using Forskningsradet.Core.Models.ViewModels.Contracts;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.ViewModelMappers.Blocks
{
    public class LinkWithTextBlockViewModelMapper : BasePartialViewModelMapper<LinkWithTextBlock>
    {
        private readonly ILinkWithTextReactModelBuilder _linkWithTextReactModelBuilder;

        public LinkWithTextBlockViewModelMapper(ILinkWithTextReactModelBuilder linkWithTextReactModelBuilder) =>
            _linkWithTextReactModelBuilder = linkWithTextReactModelBuilder;

        protected override IReactViewModel GetViewModelForPartialInternal(LinkWithTextBlock currentContent) =>
            new ReactPartialViewModel(currentContent.ReactComponentName(), BuildReactComponent(currentContent));

        private ReactModels.ReactComponent BuildReactComponent(LinkWithTextBlock currentBlock)
        {
            return _linkWithTextReactModelBuilder.BuildReactModel(currentBlock.LinkTitle, currentBlock.Link, currentBlock.Text, BuildOnPageEditingComponent());

            ReactModels.LinkWithText_OnPageEditing BuildOnPageEditingComponent() =>
                new ReactModels.LinkWithText_OnPageEditing
                {
                    Link = nameof(currentBlock.Link),
                    Text = nameof(currentBlock.Text)
                };
        }
    }
}