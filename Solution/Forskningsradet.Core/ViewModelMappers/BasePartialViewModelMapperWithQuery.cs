﻿using EPiServer.Core;
using Forskningsradet.Core.Models.QueryParameters;
using Forskningsradet.Core.Models.ViewModels.Contracts;

namespace Forskningsradet.Core.ViewModelMappers
{
    public abstract class BasePartialViewModelMapperWithQuery<T> : IPartialViewModelMapperWithQuery
        where T : IContentData
    {
        public IReactViewModel GetPartialViewModel(IContentData currentContent, QueryParameterBase parameters)
            => currentContent is T content
                ? GetPartialViewModelInternal(content, parameters)
                : null;

        protected abstract IReactViewModel GetPartialViewModelInternal(T page, QueryParameterBase parameters);
    }
}