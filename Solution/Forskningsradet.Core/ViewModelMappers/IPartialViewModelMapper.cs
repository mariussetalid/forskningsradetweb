﻿using EPiServer.Core;
using Forskningsradet.Core.Models.ViewModels.Contracts;

namespace Forskningsradet.Core.ViewModelMappers
{
    public interface IPartialViewModelMapper
    {
        IReactViewModel GetPartialViewModel(IContentData currentContent);
    }
}