﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.Globalization;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Extensions;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.ViewModels.Contracts;
using Forskningsradet.Core.Models.ViewModels.Pages;
using Forskningsradet.Core.Services;
using Forskningsradet.Core.Services.Contracts;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.ViewModelMappers.Pages
{
    public class InternationalProposalPageViewModelMapper : BasePageViewModelMapper<InternationalProposalPage>
    {
        private readonly IRichTextReactModelBuilder _richTextReactModelBuilder;
        private readonly IOptionsModalReactModelBuilder _optionsModalReactModelBuilder;
        private readonly ILocalizationProvider _localizationProvider;
        private readonly IContentRepository _contentRepository;
        private readonly CategoryRepository _categoryRepository;
        private readonly ICategoryLocalizationProvider _categoryLocalizationProvider;
        private readonly IContentLoader _contentLoader;
        private readonly IArticleHeaderReactModelBuilder _articleHeaderReactModelBuilder;
        private readonly IProposalContactReactModelBuilder _proposalContactReactModelBuilder;
        private readonly IDateCardStatusHandler _dateCardStatusHandler;

        public InternationalProposalPageViewModelMapper(
            IRichTextReactModelBuilder richTextReactModelBuilder,
            IOptionsModalReactModelBuilder optionsModalReactModelBuilder,
            ILocalizationProvider localizationProvider,
            IContentRepository contentRepository,
            CategoryRepository categoryRepository,
            ICategoryLocalizationProvider categoryLocalizationProvider,
            IContentLoader contentLoader,
            IArticleHeaderReactModelBuilder articleHeaderReactModelBuilder,
            IProposalContactReactModelBuilder proposalContactReactModelBuilder,
            IDateCardStatusHandler dateCardStatusHandler)
        {
            _richTextReactModelBuilder = richTextReactModelBuilder;
            _optionsModalReactModelBuilder = optionsModalReactModelBuilder;
            _localizationProvider = localizationProvider;
            _contentRepository = contentRepository;
            _categoryRepository = categoryRepository;
            _categoryLocalizationProvider = categoryLocalizationProvider;
            _contentLoader = contentLoader;
            _articleHeaderReactModelBuilder = articleHeaderReactModelBuilder;
            _proposalContactReactModelBuilder = proposalContactReactModelBuilder;
            _dateCardStatusHandler = dateCardStatusHandler;
        }

        protected override IReactViewModel GetViewModelForPageInternal(InternationalProposalPage page) =>
            new ReactPageViewModel(page, BuildInternationalProposalPageReactModel(page));

        private ReactModels.ReactComponent BuildInternationalProposalPageReactModel(InternationalProposalPage page) =>
            new ReactModels.ProposalPage
            {
                Title = page.Name,
                IsInternational = true,
                Header = _articleHeaderReactModelBuilder.BuildReactModelWithoutShare(page),
                StatusList = BuildStatusList(page),
                MetadataLeft = BuildProposalData(BuildLeftMetadata(page)),
                MetadataRight = BuildProposalData(BuildRightMetadata(page)),
                ContactLabel = GetLabel("ContactPerson"),
                Contact = GetContact(page.ContactPerson),
                DescriptionTitle = page.TextBlock.Title,
                DescriptionIngress = page.TextBlock.Intro,
                DescriptionText = _richTextReactModelBuilder.BuildReactModel(page.TextBlock.RichText, nameof(page.TextBlock) + "." + nameof(page.TextBlock.RichText)),
                Share = _optionsModalReactModelBuilder.BuildShareContent(page)
            };

        private IList<ReactModels.DateCardStatus> BuildStatusList(InternationalProposalPage page) =>
            new List<ReactModels.DateCardStatus> { _dateCardStatusHandler.GetCurrentStatus(page) };

        private ReactModels.ProposalContact GetContact(PageReference pageReference)
        {
            if (pageReference is null)
                return null;

            var personPage = _contentLoader.Get<PersonPage>(pageReference);
            return _proposalContactReactModelBuilder.BuildReactModel(personPage, false, true);
        }

        private IEnumerable<ReactModels.ProposalData_Items> BuildRightMetadata(InternationalProposalPage page)
        {
            var categoriesString = GetCategoriesStringList(page.Category);
            if (categoriesString != string.Empty)
                yield return BuildProposalDataItem(GetLabel("Thematics"), categoriesString);
        }

        private IEnumerable<ReactModels.ProposalData_Items> BuildLeftMetadata(InternationalProposalPage page)
        {
            if (GetPage<ApplicationTypePage>(page.ApplicationTypeReference) is ApplicationTypePage applicationTypepage)
                yield return BuildProposalDataItem(GetLabel("ApplicationType"), applicationTypepage.Name);

            if (!string.IsNullOrEmpty(page.Program))
                yield return BuildProposalDataItem(GetLabel("Program"), page.Program);

            if (page.ActivationDate.HasValue && !(page.ActivationDate.Value.IsPast()))
                yield return BuildProposalDataItem(
                    page.ActivationDateDisplayName ?? GetLabel("ActivationDate"),
                    GetActivationDateText(page.ActivationDate.Value, page.ActivationTimeUnit));

            if (!(page.FirstDeadline is null))
                yield return BuildProposalDataItem(
                    page.FirstDeadlineDisplayName ?? GetLabel("FirstDeadline"),
                    page.FirstDeadline.Value.DeadlineFormattedWithDaylightSavingNameOrStandardName());

            if (!(page.Deadline is null))
                yield return BuildProposalDataItem(
                    page.DeadlineDisplayName ?? GetLabel("SecondDeadline"),
                    page.Deadline.Value.DeadlineFormattedWithDaylightSavingNameOrStandardName());
        }

        private string GetActivationDateText(DateTime dateTime, TimeUnit timeUnit)
        {
            switch (timeUnit)
            {
                case TimeUnit.Month:
                    return dateTime.ToDisplayDate().ToString(DateTimeFormats.MonthYear);
                case TimeUnit.Date:
                    return dateTime.ToDisplayDate().ToString(DateTimeFormats.ProposalDeadlineDate);
                case TimeUnit.Week:
                    return string.Format(GetLabel("WeekFormat"), GetWeekNumber(), dateTime.Year);
                default:
                    return dateTime.ToDisplayDate().ToString(DateTimeFormats.ProposalDeadlineDate);
            }

            int GetWeekNumber()
            {
                var culture = CultureInfo.CurrentCulture;
                return culture.Calendar.GetWeekOfYear(dateTime, culture.DateTimeFormat.CalendarWeekRule, culture.DateTimeFormat.FirstDayOfWeek);
            }
        }

        private string GetCategoriesStringList(CategoryList categoryList) =>
            string.Join(", ",
                categoryList
                    .Select(x => _categoryRepository.Get(x))
                    .Select(cat => _categoryLocalizationProvider.GetCategoryName(cat.ID)));

        private static ReactModels.ProposalData BuildProposalData(IEnumerable<ReactModels.ProposalData_Items> items) =>
            new ReactModels.ProposalData { Items = items.ToList() };

        private static ReactModels.ProposalData_Items BuildProposalDataItem(string label, string text) =>
            new ReactModels.ProposalData_Items
            {
                Label = label,
                Text = text
            };

        private T GetPage<T>(ContentReference page) where T : PageData
        {
            if (page is null)
                return null;

            var currentLanguage = ContentLanguage.PreferredCulture.Name;
            return _contentRepository.Get<T>(page, new CultureInfo(currentLanguage));
        }

        private string GetLabel(string key) =>
            _localizationProvider.GetLabel(nameof(InternationalProposalPage), key);
    }
}
