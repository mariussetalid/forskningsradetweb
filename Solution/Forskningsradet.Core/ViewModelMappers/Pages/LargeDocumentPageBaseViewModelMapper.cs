﻿using System.Collections.Generic;
using System.Linq;
using EPiServer;
using EPiServer.Core;
using EPiServer.Web.Routing;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.QueryParameters;
using Forskningsradet.Core.Models.RequestModels;
using Forskningsradet.Core.Models.ViewModels.Contracts;
using Forskningsradet.Core.Models.ViewModels.Pages;
using Forskningsradet.Core.Repositories.Contracts;
using Forskningsradet.Core.Services;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.ViewModelMappers.Pages
{
    public class LargeDocumentPageBaseViewModelMapper : BasePageViewModelMapperWithQuery<LargeDocumentPageBase>
    {
        private readonly IUrlResolver _urlResolver;
        private readonly IContentLoader _contentLoader;
        private readonly ILargeDocumentService _largeDocumentService;
        private readonly IPageRepository _pageRepository;
        private readonly IArticleHeaderReactModelBuilder _articleHeaderReactModelBuilder;
        private readonly ITableOfContentsReactModelBuilder _tableOfContentsReactModelBuilder;
        private readonly IRichTextReactModelBuilder _richTextReactModelBuilder;
        private readonly ILocalizationProvider _localizationProvider;
        private readonly IOptionsModalReactModelBuilder _optionsModalReactModelBuilder;

        public LargeDocumentPageBaseViewModelMapper(
            IUrlResolver urlResolver,
            IContentLoader contentLoader,
            ILargeDocumentService largeDocumentService,
            IPageRepository pageRepository,
            IArticleHeaderReactModelBuilder articleHeaderReactModelBuilder,
            ITableOfContentsReactModelBuilder tableOfContentsReactModelBuilder,
            IRichTextReactModelBuilder richTextReactModelBuilder,
            ILocalizationProvider localizationProvider,
            IOptionsModalReactModelBuilder optionsModalReactModelBuilder)
        {
            _urlResolver = urlResolver;
            _contentLoader = contentLoader;
            _largeDocumentService = largeDocumentService;
            _pageRepository = pageRepository;
            _articleHeaderReactModelBuilder = articleHeaderReactModelBuilder;
            _tableOfContentsReactModelBuilder = tableOfContentsReactModelBuilder;
            _richTextReactModelBuilder = richTextReactModelBuilder;
            _localizationProvider = localizationProvider;
            _optionsModalReactModelBuilder = optionsModalReactModelBuilder;
        }

        protected override IReactViewModel GetViewModelForPageInternal(LargeDocumentPageBase page, QueryParameterBase parameters) =>
             new ReactPageViewModel(page, BuildPageReactModel(page, parameters as LargeDocumentQueryParameter ?? new LargeDocumentQueryParameter()));

        private ReactModels.LargeDocumentsPage BuildPageReactModel(LargeDocumentPageBase page, LargeDocumentQueryParameter parameters)
        {
            var mainPage = page as LargeDocumentPage ?? GetMainPage(page.ParentLink);
            var model = BuildModel(mainPage, parameters, page.ContentLink, _largeDocumentService.GetTree(mainPage.ContentLink.ToReferenceWithoutVersion()));

            return page is LargeDocumentPage || parameters.Download
                ? MapPage(mainPage, model, parameters.Download)
                : MapPage(page, mainPage, model);
        }

        private LargeDocumentModel BuildModel(LargeDocumentPage page, LargeDocumentQueryParameter parameters, ContentReference currentPageReference, Node largeDocumentStructure) =>
            new LargeDocumentModel(
                page.ShowNumbered,
                parameters.Download ? 0 : page.DisplayDepth,
                page.ContentLink.ToReferenceWithoutVersion(),
                largeDocumentStructure.Find(currentPageReference.ToReferenceWithoutVersion()) ?? largeDocumentStructure,
                largeDocumentStructure,
                page.LinkItems
                );

        private LargeDocumentPage GetMainPage(PageReference link) =>
            _pageRepository.GetClosestPage<LargeDocumentPage>(link);

        private ReactModels.LargeDocumentsPage MapPage(LargeDocumentPage page, LargeDocumentModel model, bool download) =>
            new ReactModels.LargeDocumentsPage
            {
                Title = page.Title ?? page.PageName,
                Ingress = page.MainIntro,
                Header = _articleHeaderReactModelBuilder.BuildReactModel(page),
                Footer = MapPageFooter(page),
                Content = MapContent(page, model, download),
                TableOfContents = _tableOfContentsReactModelBuilder.BuildReactModel(model),
                PageNavigation = MapPageNavigation(model)
            };

        private ReactModels.LargeDocumentsPage MapPage(LargeDocumentPageBase page, LargeDocumentPage mainPage, LargeDocumentModel model) =>
            new ReactModels.LargeDocumentsPage
            {
                Title = mainPage.Title ?? mainPage.PageName,
                Header = _articleHeaderReactModelBuilder.BuildReactModel(page, mainPage),
                Footer = MapPageFooter(page),
                Content = MapContent(page, model),
                TableOfContents = _tableOfContentsReactModelBuilder.BuildReactModel(model),
                PageNavigation = MapPageNavigation(model)
            };

        private ReactModels.RichText MapContent(LargeDocumentPageBase page, LargeDocumentModel model, bool download = false)
        {
            var currentlyAboveViewDepth = model.CurrentPageNode.Depth < model.ChapterDepth;
            if (currentlyAboveViewDepth)
            {
                return new ReactModels.RichText
                {
                    Blocks = BuildChapterPage(page, new List<LargeDocumentPageBase>(), download:download).ToList()
                };
            }

            var chapters = _largeDocumentService.GetChapters(model)
                .Select(x => _contentLoader.Get<LargeDocumentPageBase>(x))
                .ToList();
            return new ReactModels.RichText
            {
                Blocks = BuildChapterPage(page, chapters, download:download).ToList()
            };
        }

        private IEnumerable<ReactModels.ContentAreaItem> BuildChapterPage(LargeDocumentPageBase page, List<LargeDocumentPageBase> chapters, int headingLevelOffset = 0, bool download = false)
        {
            yield return new ReactModels.ContentAreaItem
            {
                ComponentName = nameof(ReactModels.RichTextBlock),
                ComponentData = new ReactModels.RichTextBlock
                {
                    HtmlId = page.ContentLink.ID.ToString(),
                    Text = GetRichText(page, download),
                    HeadingLevelOffset = headingLevelOffset,
                    Title = page is LargeDocumentPage largeDocumentPage
                            ? largeDocumentPage.IntroTitle ?? largeDocumentPage.Title ?? largeDocumentPage.PageName
                            : page.Title ?? page.PageName
                }
            };

            foreach (var chapter in chapters)
            {
                yield return new ReactModels.ContentAreaItem
                {
                    ComponentName = nameof(ReactModels.RichText),
                    ComponentData = BuildChapter(chapter, headingLevelOffset + 1, download)
                };
            }
        }

        private ReactModels.RichText GetRichText(LargeDocumentPageBase page, bool download)
        {
            var richText = _richTextReactModelBuilder.BuildReactModel(page.MainBody, nameof(page.MainBody));
            if (download)
            {
                richText.Blocks = richText.Blocks?
                    .Where(x => !(x.ComponentData is ReactModels.EmbedBlock emb && SourceIsTableauOrNotSet(emb.Src)))
                    .ToList();
            }

            return richText;

            bool SourceIsTableauOrNotSet(string src) =>
                string.IsNullOrEmpty(src)
                || src.StartsWith(EmbedSourceConstants.Tableau.ValidSrcStart);
        }

        private ReactModels.RichText BuildChapter(LargeDocumentPageBase page, int headingLevelOffset, bool download) =>
            new ReactModels.RichText
            {
                Blocks = BuildChapterItems(page, headingLevelOffset, download).ToList()
            };

        private IEnumerable<ReactModels.ContentAreaItem> BuildChapterItems(LargeDocumentPageBase page, int headingLevelOffset, bool download)
        {
            var children = _largeDocumentService.GetChildren(page.PageLink)
                .Select(x => _contentLoader.Get<LargeDocumentPageBase>(x))
                .ToList();

            yield return new ReactModels.ContentAreaItem
            {
                ComponentName = nameof(ReactModels.RichTextBlock),
                ComponentData = new ReactModels.RichTextBlock
                {
                    HtmlId = page.ContentLink.ID.ToString(),
                    Title = page.Title ?? page.PageName,
                    Text = GetRichText(page, download),
                    HeadingLevelOffset = headingLevelOffset
                }
            };

            foreach (var child in children)
            {
                yield return new ReactModels.ContentAreaItem
                {
                    ComponentName = nameof(ReactModels.RichText),
                    ComponentData = BuildChapter(child, headingLevelOffset + 1, download)
                };
            }
        }

        private ReactModels.PageFooter MapPageFooter(LargeDocumentPageBase page) =>
            new ReactModels.PageFooter
            {
                Share = _optionsModalReactModelBuilder.BuildShareContent(page)
            };

        private ReactModels.PageNavigation MapPageNavigation(LargeDocumentModel model) =>
            new ReactModels.PageNavigation
            {
                Previous = _urlResolver.GetUrl(_largeDocumentService.GetPreviousPage(model)) is string previousUrl
                    ? new ReactModels.Link { Text = GetLabel("Previous"), Url = previousUrl }
                    : null,
                Next = _urlResolver.GetUrl(_largeDocumentService.GetNextPage(model)) is string nextUrl
                    ? new ReactModels.Link { Text = GetLabel("Next"), Url = nextUrl }
                    : null
            };

        private string GetLabel(string key) =>
            _localizationProvider.GetLabel(nameof(LargeDocumentPage), key);
    }
}