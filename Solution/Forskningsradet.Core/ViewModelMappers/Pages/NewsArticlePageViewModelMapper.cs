﻿using System;
using EPiServer.Web.Routing;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Facades.Contracts;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Services;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.ViewModelMappers.Pages
{
    public class NewsArticlePageViewModelMapper : ArticlePageViewModelMapper<NewsArticlePage>
    {
        private readonly IOptionsModalReactModelBuilder _optionsModalReactModelBuilder;

        public NewsArticlePageViewModelMapper(
            IContentAreaReactModelBuilder contentAreaReactModelBuilder,
            IRichTextReactModelBuilder richTextReactModelBuilder,
            ILocalizationProvider localizationProvider,
            IPageEditingAdapter pageEditingAdapter,
            ISocialMediaLinkListReactModelBuilder socialMediaLinkListReactModelBuilder,
            ITagLinkListReactModelBuilder tagLinkListReactModelBuilder,
            IUrlResolver urlResolver,
            IContentListService contentListService,
            IOptionsModalReactModelBuilder optionsModalReactModelBuilder)
            : base(contentAreaReactModelBuilder, richTextReactModelBuilder, localizationProvider, pageEditingAdapter, socialMediaLinkListReactModelBuilder, tagLinkListReactModelBuilder, urlResolver, contentListService)
        {
            _optionsModalReactModelBuilder = optionsModalReactModelBuilder;
        }

        protected override ReactModels.ArticleHeader BuildHeader(NewsArticlePage currentPage)
            => new ReactModels.ArticleHeader
            {
                Accordion = new ReactModels.Accordion
                {
                    CollapseLabel = GetLabel("Collapse"),
                    ExpandLabel = GetLabel("Expand"),
                    Guid = Guid.NewGuid().ToString()
                },
                Byline = BuildByline(currentPage),
                Download = BuildDownloadLink(currentPage),
                Share = _optionsModalReactModelBuilder.BuildShareContent(currentPage)
            };

        protected override ReactModels.PageFooter BuildFooter(NewsArticlePage currentPage)
            => null;

    }
}
