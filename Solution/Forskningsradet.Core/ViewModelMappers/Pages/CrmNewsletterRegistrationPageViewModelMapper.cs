﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Helpers;
using EPiServer.Web.Routing;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.QueryParameters;
using Forskningsradet.Core.Models.SearchModels;
using Forskningsradet.Core.Models.ViewModels.Contracts;
using Forskningsradet.Core.Models.ViewModels.Forms;
using Forskningsradet.Core.Models.ViewModels.Pages;
using Forskningsradet.Core.Services;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.ViewModelMappers.Pages
{
    public class CrmNewsletterRegistrationPageViewModelMapper : BasePageViewModelMapperWithQuery<CrmNewsletterRegistrationPage>
    {
        private readonly IRichTextReactModelBuilder _richTextReactModelBuilder;
        private readonly IContentAreaReactModelBuilder _contentAreaReactModelBuilder;
        private readonly IMessageReactModelBuilder _messageReactModelBuilder;
        private readonly IOptionsModalReactModelBuilder _optionsModalReactModelBuilder;
        private readonly IUrlResolver _urlResolver;
        private readonly ILocalizationProvider _localizationProvider;

        private const string FormMethodPost = "POST";

        public CrmNewsletterRegistrationPageViewModelMapper(
            IRichTextReactModelBuilder richTextReactModelBuilder,
            IContentAreaReactModelBuilder contentAreaReactModelBuilder,
            IMessageReactModelBuilder messageReactModelBuilder,
            IOptionsModalReactModelBuilder optionsModalReactModelBuilder,
            IUrlResolver urlResolver,
            ILocalizationProvider localizationProvider)
        {
            _richTextReactModelBuilder = richTextReactModelBuilder;
            _contentAreaReactModelBuilder = contentAreaReactModelBuilder;
            _messageReactModelBuilder = messageReactModelBuilder;
            _optionsModalReactModelBuilder = optionsModalReactModelBuilder;
            _urlResolver = urlResolver;
            _localizationProvider = localizationProvider;
        }

        protected override IReactViewModel GetViewModelForPageInternal(CrmNewsletterRegistrationPage currentPage, QueryParameterBase parameters) =>
            new ReactPageViewModel(currentPage, BuildReactModel(currentPage, parameters as CrmNewsletterQueryParameter ?? new CrmNewsletterQueryParameter()));

        private ReactModels.NewsletterCrmRegistrationPage BuildReactModel(CrmNewsletterRegistrationPage currentPage, CrmNewsletterQueryParameter parameters) =>
            new ReactModels.NewsletterCrmRegistrationPage
            {
                RegistrationPage = new ReactModels.NewsletterRegistrationPage
                {
                    Title = currentPage.Title ?? currentPage.PageName,
                    RichText = _richTextReactModelBuilder.BuildReactModel(currentPage.MainIntro, nameof(currentPage.MainIntro)),
                    Sidebar = _contentAreaReactModelBuilder.BuildReactModel(currentPage.RightBlockArea, nameof(currentPage.RightBlockArea)),
                    CheckboxGroups = BuildCheckboxGroups(parameters).ToList(),
                    Form = new ReactModels.Form
                    {
                        Method = FormMethodPost,
                        Endpoint = _urlResolver.GetUrl(currentPage.ContentLink),
                        SubmitButtonText = GetLabel("Register")
                    },
                    SubmitButton = new ReactModels.Button
                    {
                        Text = GetLabel("Register")
                    },
                    InputFields = BuildInputFields().ToList(),
                    InputTitle = GetLabel("EmailFieldsTitle"),
                    HtmlString = CreateAntiForgeryToken(),
                    Message = BuildMessage(currentPage.ValidationMessage, nameof(currentPage.ValidationMessage)),
                    Footer = BuildFooter(currentPage)
                }
            };

        private IEnumerable<ReactModels.CheckboxGroup> BuildCheckboxGroups(CrmNewsletterQueryParameter parameters)
        {
            yield return new ReactModels.CheckboxGroup
            {
                Title = _localizationProvider.GetEnumName(FilterGroupType.TargetGroup) + "*",
                Options = BuildOptionsFromCategories(CategoryConstants.TargetGroupRoot, QueryParameters.TargetGroup, parameters.ValidTargetGroups)
            };

            yield return new ReactModels.CheckboxGroup
            {
                Title = _localizationProvider.GetEnumName(FilterGroupType.Subject),
                Options = BuildOptionsFromCategories(CategoryConstants.SubjectRoot, QueryParameters.Subject, parameters.ValidSubjects)
            };

            IList<ReactModels.Checkbox> BuildOptionsFromCategories(string categoryRoot, string queryParameterName, Dictionary<string, int> validValues) =>
                validValues
                ?.Select(x => new ReactModels.Checkbox
                {
                    Name = queryParameterName,
                    Label = x.Key,
                    Value = x.Value.ToString(),
                }
                )?.ToList() ?? new List<ReactModels.Checkbox>();
        }

        private IEnumerable<ReactModels.InputEmail> BuildInputFields()
        {
            yield return BuildEmailInputFromParameters(nameof(NewsletterRegistrationFormModel.Email));

            ReactModels.InputEmail BuildEmailInputFromParameters(string fieldName)
            {
                return new ReactModels.InputEmail
                {
                    Name = fieldName,
                    Label = GetLabel(fieldName),
                    ValidationError = GetLabel("EmailAddressErrorMessage")
                };
            }
        }

        private static string CreateAntiForgeryToken() => AntiForgery.GetHtml().ToHtmlString();

        private ReactModels.Message BuildMessage(string validationMessage, string propertyName)
        {
            var text = string.IsNullOrEmpty(validationMessage)
                ? GetLabel("ValidationMessage")
                : validationMessage;

            return _messageReactModelBuilder.BuildReactModel(text, propertyName);
        }

        private ReactModels.PageFooter BuildFooter(CrmNewsletterRegistrationPage currentPage) =>
            new ReactModels.PageFooter
            {
                Share = _optionsModalReactModelBuilder.BuildShareContent(currentPage)
            };

        private string GetLabel(string key) =>
            _localizationProvider.GetLabel(nameof(MailChimpNewsletterRegistrationPage), key);
    }
}