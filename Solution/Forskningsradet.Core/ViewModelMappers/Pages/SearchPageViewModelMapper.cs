using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Castle.Core.Internal;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.Find.UnifiedSearch;
using EPiServer.Web.Routing;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Builders;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Configuration;
using Forskningsradet.Core.Extensions;
using Forskningsradet.Core.Models.ApiModels.Evurdering;
using Forskningsradet.Core.Models.ContentModels.Contracts;
using Forskningsradet.Core.Models.ContentModels.Media;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.QueryParameters;
using Forskningsradet.Core.Models.RequestModels;
using Forskningsradet.Core.Models.SearchModels;
using Forskningsradet.Core.Models.ViewModels.Contracts;
using Forskningsradet.Core.Models.ViewModels.Pages;
using Forskningsradet.Core.Repositories.Contracts;
using Forskningsradet.Core.Services;
using Forskningsradet.Core.Services.Contracts;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.ViewModelMappers.Pages
{
    public class SearchPageViewModelMapper : BasePageViewModelMapperWithQuery<SearchPage>
    {
        private readonly ISearchService _searchService;
        private readonly IUrlResolver _urlResolver;
        private readonly IContentLoader _contentLoader;
        private readonly IPageRepository _pageRepository;
        private readonly IProjectDatabankConfiguration _projectDatabankConfiguration;
        private readonly FilterLayoutReactModelBuilder _filterLayoutReactModelBuilder;
        private readonly IFluidImageReactModelBuilder _fluidImageReactModelBuilder;
        private readonly IDateCardStatusHandler _dateCardStatusHandler;
        private readonly ICategoryFilterService _categoryFilterService;
        private readonly CategoryRepository _categoryRepository;
        private readonly ICategoryLocalizationProvider _categoryLocalizationProvider;
        private readonly IDateCardTagsReactModelBuilder _dateCardTagsReactModelBuilder;
        private readonly ILocalizationProvider _localizationProvider;
        private readonly IPaginationReactModelBuilder _paginationReactModelBuilder;
        private readonly ISearchApiUrlResolver _searchApiUrlResolver;
        private readonly ISearchTrackingService _searchTrackingService;

        public SearchPageViewModelMapper(
            ISearchService searchService,
            IUrlResolver urlResolver,
            IContentLoader contentLoader,
            IPageRepository pageRepository,
            IProjectDatabankConfiguration projectDataBankConfiguration,
            FilterLayoutReactModelBuilder filterLayoutReactModelBuilder,
            IFluidImageReactModelBuilder fluidImageReactModelBuilder,
            IDateCardStatusHandler dateCardStatusHandler,
            ICategoryFilterService categoryFilterService,
            CategoryRepository categoryRepository,
            ICategoryLocalizationProvider categoryLocalizationProvider,
            IDateCardTagsReactModelBuilder dateCardTagsReactModelBuilder,
            ILocalizationProvider localizationProvider,
            IPaginationReactModelBuilder paginationReactModelBuilder,
            ISearchApiUrlResolver searchApiUrlResolver,
            ISearchTrackingService searchTrackingService)
        {
            _searchService = searchService;
            _urlResolver = urlResolver;
            _contentLoader = contentLoader;
            _pageRepository = pageRepository;
            _projectDatabankConfiguration = projectDataBankConfiguration;
            _filterLayoutReactModelBuilder = filterLayoutReactModelBuilder;
            _fluidImageReactModelBuilder = fluidImageReactModelBuilder;
            _dateCardStatusHandler = dateCardStatusHandler;
            _categoryFilterService = categoryFilterService;
            _categoryRepository = categoryRepository;
            _categoryLocalizationProvider = categoryLocalizationProvider;
            _dateCardTagsReactModelBuilder = dateCardTagsReactModelBuilder;
            _localizationProvider = localizationProvider;
            _paginationReactModelBuilder = paginationReactModelBuilder;
            _searchApiUrlResolver = searchApiUrlResolver;
            _searchTrackingService = searchTrackingService;
        }

        protected override IReactViewModel GetViewModelForPageInternal(SearchPage currentPage, QueryParameterBase parameters) =>
            new ReactPageViewModel(currentPage, BuildReactModel(currentPage, parameters as SearchQueryParameter ?? new SearchQueryParameter(null, null, null, null, null, null, null, null)));

        private ReactModels.SearchPage BuildReactModel(SearchPage currentPage, SearchQueryParameter parameters)
        {
            var result = _searchService.Search(BuildSearchRequest(parameters, currentPage));
            var listPageUrl = _urlResolver.GetUrl(currentPage);
            var externalEndpoint = !string.IsNullOrEmpty(currentPage.ExternalSearchEndpoint)
                ? currentPage.ExternalSearchEndpoint
                : _projectDatabankConfiguration.ProjectDatabankSearchUrl;

            var reactModel = new ReactModels.SearchPage
            {
                Title = currentPage.PageName,
                EmptyList = BuildEmptyListElement(parameters, result.TotalMatching),
                Results = EmptyRequest(parameters) ? null : BuildSearchResults(parameters.Query, result, currentPage)?.ToList(),
                FilterLayout = BuildFilterLayout(parameters, result.Categories, currentPage.ShowRangeSlider, currentPage.LeftSideFilterPlacement, currentPage.FilterTitle, result.TotalMatching),
                ResultDescription = BuildResultDescription(parameters, result.TotalMatching, SearchConstants.SearchPageSize),
                Form = BuildForm(listPageUrl),
                Search = BuildSearch(parameters, result, externalEndpoint, currentPage.EnableExternalSearch, currentPage.SpellCheck?.IsDisabled(result.TotalMatching) == true),
                FetchFilteredResultsEndpoint = _searchApiUrlResolver.GetSearchUrl(),
                Pagination = EmptyRequest(parameters) ? null : BuildPagination(parameters, result.TotalMatching, parameters.Page, SearchConstants.SearchPageSize, listPageUrl)
            };
            return reactModel;
        }

        private IEnumerable<ReactModels.SearchResult> BuildSearchResults(string query, SearchPageResults result, SearchPage page)
        {
            var categoryGroups = result.Categories?.Where(x => x.Category != null).ToList() ?? new List<CategoryGroup>();
            var categoriesForDateCardTags = GetCategoriesForDateCardTags(page).ToList();
            var queryTrackResult = _searchTrackingService.TrackQuery(query, result.Results.Count());
            return result.Results?.Select((x, index) => new ReactModels.SearchResult
            {
                Url = x.Url,
                TrackUrl = queryTrackResult is null ? default : _searchApiUrlResolver.GetSearchHitTrackUrl(_searchTrackingService.GetTrackHitRequest(query, queryTrackResult, x.OriginalObjectGetter() as IContent, index)),
                Title = x.Title,
                DescriptiveTitle = GetDocumentTitle(x),
                Text = GetSearchHitText(x),
                Metadata = new ReactModels.Metadata
                {
                    Items = BuildMetadataList(x, categoryGroups, page).ToList()
                },
                Image = BuildImage(x),
                Icon = BuildDocumentIcon(x),
                StatusList = GetStatusList(x).ToList(),
                ThemeTags = BuildThemeTags(x, categoriesForDateCardTags, ListPageConstants.NumberOfVisibleTags)
            });

            string GetSearchHitText(UnifiedSearchHit x) =>
                GetSearchResultType(x.TypeName) != SearchResultType.Person
                    ? x.Excerpt
                    : GetStringFromMetadata(x, SearchConstants.MetaDataJobTitle);
        }

        private string GetDocumentTitle(UnifiedSearchHit hit)
        {
            if (hit.OriginalObjectGetter() is LargeDocumentChapterPage documentChapter)
            {
                return _pageRepository.GetClosestPage<LargeDocumentPage>(documentChapter.ParentLink).GetListTitle() is string documentTitle
                    ? $"{documentTitle}:"
                    : null;
            }
            return null;
        }

        private ReactModels.DocumentIcon BuildDocumentIcon(UnifiedSearchHit searchHit)
        {
            if (searchHit.OriginalObjectGetter.Invoke() is PublicationPage)
                return new ReactModels.DocumentIcon { IconTheme = ReactModels.DocumentIcon_IconTheme.Pdf };

            if (searchHit.OriginalObjectGetter.Invoke() is GenericMedia genericMedia)
            {
                switch (genericMedia.FileExtension)
                {
                    case ".pdf":
                        return new ReactModels.DocumentIcon { IconTheme = ReactModels.DocumentIcon_IconTheme.Pdf };
                    case ".doc":
                    case ".docx":
                        return new ReactModels.DocumentIcon { IconTheme = ReactModels.DocumentIcon_IconTheme.Word };
                    case ".xls":
                    case ".xlsx":
                        return new ReactModels.DocumentIcon { IconTheme = ReactModels.DocumentIcon_IconTheme.Excel };
                    case ".ppt":
                    case ".pptx":
                        return new ReactModels.DocumentIcon { IconTheme = ReactModels.DocumentIcon_IconTheme.Ppt };
                    default:
                        return new ReactModels.DocumentIcon { IconTheme = ReactModels.DocumentIcon_IconTheme.Fallback };
                }
            }

            return null;
        }

        private ReactModels.DateCardTags BuildThemeTags(UnifiedSearchHit searchHit, IList<Category> categoriesForDateCardTags, int numberOfVisibleTags) =>
            searchHit.OriginalObjectGetter.Invoke() is PageData pageData
                ? _dateCardTagsReactModelBuilder.BuildReactModel(new CategoryList(
                    categoriesForDateCardTags
                    .Where(x => pageData.Category.MemberOf(x.ID))
                    .Select(x => x.ID)), numberOfVisibleTags)
                : null;

        private IEnumerable<Category> GetCategoriesForDateCardTags(SearchPage searchPage) =>
            !UseCustomCategories(searchPage)
                ? _categoryFilterService.GetDefaultCategoriesForLinkTags()
                : _categoryFilterService.GetCategoriesForLinkTags(searchPage.FilterCategoryList, searchPage.TagCategoryRoot);

        private IEnumerable<ReactModels.DateCardStatus> GetStatusList(UnifiedSearchHit hit)
        {
            return hit.OriginalObjectGetter.Invoke() is ProposalPage proposalPage
                ? _dateCardStatusHandler.BuildDateCardStatusList(proposalPage)
                : new List<ReactModels.DateCardStatus>();
        }

        private ReactModels.FluidImage BuildImage(UnifiedSearchHit hit)
        {
            return hit.OriginalObjectGetter.Invoke() is EditorialPage editorialPage
                   && !ContentReference.IsNullOrEmpty(editorialPage.ListImage)
                ? _fluidImageReactModelBuilder.BuildReactModel(_contentLoader.Get<ImageFile>(editorialPage.ListImage), null)
                : null;
        }

        private IEnumerable<ReactModels.Metadata_Items> BuildMetadataList(UnifiedSearchHit hit, List<CategoryGroup> categoryGroups, SearchPage page)
        {
            foreach (var tag in BuildTags(hit, page))
                yield return tag;

            foreach (var metadata in BuildMetadata(hit))
                yield return metadata;

            if (!UseCustomCategories(page))
                yield break;

            if (!(hit.OriginalObjectGetter.Invoke() is ICategorizable categorizable))
                yield break;

            var categoryIdsInHit = GetAllowedCategoryIdsInHit(categorizable, GetAllowedFilterCategories(page).ToList());

            var categoryGroupsVisibleInMetadata = categoryGroups
                .Where(x => page.FilterCategoryList.Contains(x.Category.ID)
                            && !(page.TagCategoryRoot != null && page.TagCategoryRoot.Contains(x.Category.ID)));
            foreach (var categoryGroup in categoryGroupsVisibleInMetadata)
            {
                var namesOfAllowedCategoriesInGroup = categoryGroup.Category.Categories
                    .Where(c => categoryIdsInHit.Contains(c.ID))
                    .Select(c => _categoryLocalizationProvider.GetCategoryName(c.ID))
                    .ToList();

                if (namesOfAllowedCategoriesInGroup.Any())
                {
                    yield return BuildMetadataItem(
                        _categoryLocalizationProvider.GetCategoryName(categoryGroup.Category.ID),
                        string.Join(", ", namesOfAllowedCategoriesInGroup));
                }
            }
        }

        private IEnumerable<ReactModels.Metadata_Items> BuildTags(UnifiedSearchHit hit, SearchPage page)
        {
            if (!UseCustomCategories(page))
            {
                yield return BuildMetadataItem(GetLabel("ContentTypeLabel"), GetPageTypeName(hit.TypeName));
            }
            else
            {
                if (!(hit.OriginalObjectGetter.Invoke() is ICategorizable categorizable))
                    yield break;

                var categoryIdsInHit = GetAllowedCategoryIdsInHit(categorizable, GetAllowedTagCategories(page).ToList());
                foreach (var categoryId in categoryIdsInHit)
                {
                    yield return BuildMetadataItem(GetLabel("ContentTypeLabel"), _categoryLocalizationProvider.GetCategoryName(categoryId));
                }
            }
        }

        private static bool UseCustomCategories(SearchPage page) =>
            page.FilterCategoryList?.Count > 0;

        private static IEnumerable<int> GetAllowedCategoryIdsInHit(ICategorizable hitWithCategories, IReadOnlyCollection<int> allowedCategoryIds) =>
            hitWithCategories.Category.MemberOfAny(allowedCategoryIds)
                ? hitWithCategories.Category.Where(allowedCategoryIds.Contains)
                : Enumerable.Empty<int>();

        private IEnumerable<int> GetAllowedTagCategories(SearchPage page) =>
            page.TagCategoryRoot?.ToArray()
                .Select(x => _categoryRepository.Get(x))
                .SelectMany(x => x.Categories.Select(c => c.ID))
                .ToList() ?? Enumerable.Empty<int>();

        private IEnumerable<int> GetAllowedFilterCategories(SearchPage page) =>
            page.FilterCategoryList.ToArray()
                .Select(x => _categoryRepository.Get(x))
                .SelectMany(x => x.Categories.Select(c => c.ID))
                .ToList();

        private string GetPageTypeName(string searchResultTypeName)
        {
            var enumValue = GetSearchResultType(searchResultTypeName);
            return _localizationProvider.GetEnumName(enumValue);
        }

        private static SearchResultType GetSearchResultType(string searchResultTypeName) =>
            Enum.TryParse(searchResultTypeName, out SearchResultType searchResultType)
                ? searchResultType
                : SearchResultType.None;

        private static string BuildDateTimeText(DateTime? start, DateTime? end) =>
            start?.ToDisplayDate() is DateTime startDate
                ? startDate.ToNorwegianDateString()
                  + (end?.ToDisplayDate() is DateTime endDate && startDate.ToString("M") != endDate.ToString("M")
                      ? " - " + endDate.ToNorwegianDateString()
                      : string.Empty)
                : null;

        private IEnumerable<ReactModels.Metadata_Items> BuildMetadata(UnifiedSearchHit hit)
        {
            var type = GetSearchResultType(hit.TypeName);
            switch (type)
            {
                case SearchResultType.Event:
                    yield return BuildDateTimeText(GetDateFromMetadata(hit, SearchConstants.MetaDataStartDate), GetDateFromMetadata(hit, "EndDate")) is string dateTimeText
                        ? new ReactModels.Metadata_Items
                        {
                            Label = GetLabel("EventDataWhenTitle"),
                            Text = dateTimeText,
                            IsDisabled = GetDateFromMetadata(hit, "EndDate") is DateTime dateTime
                                ? dateTime.IsPast()
                                : false
                        }
                        : new ReactModels.Metadata_Items
                        {
                            Label = GetLabel("Published"),
                            Text = hit.PublishDate?.ToNorwegianDateString()
                        };
                    break;
                case SearchResultType.Proposal:
                    yield return GetDateFromMetadata(hit, SearchConstants.MetaDataDeadline)?.ToDisplayDate().ToNorwegianDateString() is string date
                        ? new ReactModels.Metadata_Items
                        {
                            Label = GetLabel("Deadline"),
                            Text = date,
                            IsDisabled = GetDateFromMetadata(hit, SearchConstants.MetaDataDeadline) is DateTime time
                                ? time.IsPast()
                                : false
                        }
                        : new ReactModels.Metadata_Items
                        {
                            Label = GetLabel("Deadline"),
                            Text = Enum.TryParse(GetStringFromMetadata(hit, SearchConstants.MetaDataDeadlineType), out DeadlineType deadlineType)
                                ? deadlineType == DeadlineType.Continuous
                                    ? GetLabel("ContinuousDeadline")
                                    : null
                                : null
                        };
                    break;
                case SearchResultType.Person:
                    yield return BuildMetadataItem(
                        GetLabel("DepartmentLabel"),
                        GetStringFromMetadata(hit, SearchConstants.MetaDataDepartment));

                    var email = GetStringFromMetadata(hit, SearchConstants.MetaDataEmail);
                    if (!email.IsNullOrEmpty())
                        yield return BuildMetadataItem(
                            GetLabel("EmailLabel"),
                            email,
                            "mailto:" + email);
                    break;
                case SearchResultType.Publication:
                    if (GetIntFromMetadata(hit, SearchConstants.MetaDataPublicationType) is int publicationType && publicationType >= 0)
                        yield return BuildMetadataItem(
                            GetLabel("PublicationTypeLabel"),
                            GetPublicationTypeString(publicationType)
                            );
                    if (GetIntFromMetadata(hit, SearchConstants.MetaDataNumberOfPages) is int numberOfPages && numberOfPages > 0)
                        yield return BuildMetadataItem(
                            GetLabel("NumberOfPagesLabel"),
                            numberOfPages.ToString()
                            );
                    if (GetStringFromMetadata(hit, SearchConstants.MetaDataAuthor) is string author && !string.IsNullOrEmpty(author))
                        yield return BuildMetadataItem(
                            GetLabel("AuthorLabel"),
                            author
                            );
                    if (GetIntFromMetadata(hit, SearchConstants.MetaDataYear) is int year && year > 0)
                        yield return BuildMetadataItem(
                            GetLabel("YearLabel"),
                            year.ToString()
                            );
                    if (GetStringFromMetadata(hit, SearchConstants.MetaDataIsbn) is string isbn && !string.IsNullOrEmpty(isbn))
                        yield return BuildMetadataItem(
                            GetLabel("IsbnLabel"),
                            isbn);
                    if (GetDocumentFileInfo(GetIntFromMetadata(hit, SearchConstants.MetaDataAttachmentInfo)) is string documentInfo && !string.IsNullOrEmpty(documentInfo))
                        yield return BuildMetadataItem(
                            GetLabel("DocumentLabel"),
                            documentInfo);
                    break;
                default:
                    yield return BuildMetadataItem(
                        GetLabel("Published"),
                        hit.PublishDate?.ToDisplayDate().ToNorwegianDateString());
                    break;
            }
        }

        private static DateTime? GetDateFromMetadata(UnifiedSearchHit hit, string key) =>
            hit.MetaData?.Keys.Contains(key) ?? false
                ? hit.MetaData[key].StringValue is var value && !string.IsNullOrWhiteSpace(value) && DateTime.TryParse(value, new CultureInfo(LanguageConstants.NorwegianLanguageBranch), DateTimeStyles.None, out var result)
                    ? result
                    : (DateTime?)null
                : null;

        private static string GetStringFromMetadata(UnifiedSearchHit hit, string key) =>
            hit.MetaData?.Keys.Contains(key) ?? false
                ? hit.MetaData[key].StringValue
                : null;

        private static int GetIntFromMetadata(UnifiedSearchHit hit, string key) =>
            hit.MetaData?.Keys.Contains(key) ?? false
                ? hit.MetaData[key].StringValue is var value && !string.IsNullOrWhiteSpace(value) && int.TryParse(value, out int result)
                    ? result
                    : -1
                : -1;

        private static ReactModels.Metadata_Items BuildMetadataItem(string label, string text, string url = null) =>
            new ReactModels.Metadata_Items
            {
                Label = label,
                Text = text,
                Url = url
            };

        private string GetPublicationTypeString(int publicationTypeInt)
        {
            if (Enum.IsDefined(typeof(PublicationType), publicationTypeInt)
                && Enum.TryParse(publicationTypeInt.ToString(), out PublicationType publicationType))
                return _localizationProvider.GetEnumName(publicationType);

            return string.Empty;
        }

        private string GetDocumentFileInfo(int contentId)
        {
            if (contentId <= 0)
                return string.Empty;

            var reference = new ContentReference(contentId);
            if (_contentLoader.TryGet<MediaData>(reference, out var content)
                && content is IContentMediaMetaData mediaData)
            {
                return $"{mediaData.FileExtension} ({mediaData.FileSize})";
            }

            return string.Empty;
        }

        private string BuildResultDescription(SearchQueryParameter parameters, int totalHits, int pageSize)
        {
            if (EmptyRequest(parameters))
                return null;

            var currentPage = parameters.Page;
            return totalHits > 0
                ? string.Format(
                    GetLabel("ResultDescriptionFormat"),
                    (currentPage - 1) * pageSize + 1,
                    Math.Min(currentPage * pageSize, totalHits),
                    totalHits)
                : null;
        }

        private ReactModels.EmptyList BuildEmptyListElement(SearchQueryParameter parameters, int totalMatching) =>
            totalMatching == 0 || EmptyRequest(parameters)
                ? new ReactModels.EmptyList
                {
                    Text = GetLabel("EmptySearch")
                }
                : null;

        private ReactModels.FilterLayout BuildFilterLayout(SearchQueryParameter parameters, IEnumerable<CategoryGroup> categories, bool showRangeSlider, bool leftSidePlacement, string filterTitle, int resultsCount) =>
            _filterLayoutReactModelBuilder.BuildReactModel(new FilterLayoutModel(parameters, categories?.ToList() ?? new List<CategoryGroup>(), filterTitle, resultsCount, showRangeSlider, leftSidePlacement));

        private ReactModels.Form BuildForm(string endpoint) =>
            new ReactModels.Form
            {
                Endpoint = endpoint,
                SubmitButtonText = GetLabel("SubmitButtonText")
            };

        private static bool EmptyRequest(SearchQueryParameter p) =>
            string.IsNullOrWhiteSpace(p.Query)
            && !(p.Subjects?.Length > 0)
            && !(p.TargetGroups?.Length > 0)
            && !(p.Types?.Length > 0)
            && !(p.Categories?.Length > 0);

        private ReactModels.Search BuildSearch(SearchQueryParameter parameters, SearchPageResults result, string externalEndpoint, bool enableExternalSearch, bool disableSpellCheck) =>
            new ReactModels.Search
            {
                Input = new ReactModels.TextInput
                {
                    Name = QueryParameters.SearchQuery,
                    Label = QueryParameters.SearchQuery,
                    Placeholder = GetLabel("SearchPlaceholder"),
                    Value = parameters.Query
                },
                ResultsDescription = !EmptyRequest(parameters)
                    ? string.Format(GetLabel("InternalSearchDescriptionFormat"), result.TotalMatching)
                    : GetLabel("EmptySearchDescription"),
                ExternalResultsLabel = !string.IsNullOrWhiteSpace(parameters.Query) && enableExternalSearch
                    ? GetLabel("ExternalSearchDescription")
                    : string.Empty,
                ExternalResultsEndpoint = !string.IsNullOrWhiteSpace(parameters.Query) && enableExternalSearch
                    ? string.Format(externalEndpoint, parameters.Query)
                    : null,
                Submit = new ReactModels.Button { Text = GetLabel("SearchButtonText") },
                SearchSuggestions = !disableSpellCheck
                    ? BuildSpellCheckSuggestions(parameters.Query)
                    : null
            };

        private ReactModels.SearchSuggestions BuildSpellCheckSuggestions(string query)
        {
            var suggestions = _searchService.GetDidYouMeanSuggestions(query);
            return suggestions?.Count > 0
                ? new ReactModels.SearchSuggestions
                {
                    Label = GetLabel("SpellCheckSuggestionsLabel"),
                    Suggestions = suggestions
                }
                : null;
        }

        private ReactModels.Pagination BuildPagination(SearchQueryParameter parameters, int totalPages, int currentPage, int pageSize, string listPageUrl) =>
            _paginationReactModelBuilder.BuildReactModel(BuildCurrentQueryString(parameters), currentPage, totalPages, pageSize, listPageUrl);

        private static string BuildCurrentQueryString(SearchQueryParameter parameters) =>
            string.Join("&", BuildQueryParameters(parameters));

        private static IEnumerable<string> BuildQueryParameters(SearchQueryParameter parameters)
        {
            if (parameters.Query != null)
            {
                yield return $"{QueryParameters.SearchQuery}={parameters.Query}";
            }

            if (parameters.Subjects != null)
            {
                foreach (var subject in parameters.Subjects)
                    yield return $"{QueryParameters.Subject}={subject}";
            }

            if (parameters.TargetGroups != null)
            {
                foreach (var targetGroup in parameters.TargetGroups)
                    yield return $"{QueryParameters.TargetGroup}={targetGroup}";
            }

            if (parameters.Types != null)
            {
                foreach (var type in parameters.Types)
                    yield return $"{QueryParameters.ResultType}={type}";
            }

            if (parameters.Categories != null)
            {
                foreach (var category in parameters.Categories)
                    yield return $"{QueryParameters.Categories}={category}";
            }

            if (parameters.From > 0 && parameters.To > 0)
            {
                yield return $"{QueryParameters.YearFrom}={parameters.From}";
                yield return $"{QueryParameters.YearTo}={parameters.To}";
            }
        }

        private static SearchRequestBase BuildSearchRequest(SearchQueryParameter query, SearchPage page) =>
            page.FilterCategoryList?.Count > 0
                ? (SearchRequestBase)new CustomCategoriesSearchRequest(
                    query.Query,
                    query.Categories,
                    page.FilterCategoryList.ToArray(),
                    query.Types,
                    query.From,
                    query.To,
                    query.Page,
                    SearchConstants.SearchPageSize,
                    page.PageRoot?.ToReferenceWithoutVersion().ID ?? 0, true)
                : new SearchRequest(
                    query.Query,
                    query.Subjects,
                    query.TargetGroups,
                    query.Types,
                    query.From,
                    query.To,
                    query.Page,
                    SearchConstants.SearchPageSize,
                    page.PageRoot?.ToReferenceWithoutVersion().ID ?? 0, true);

        private string GetLabel(string key) => _localizationProvider.GetLabel(nameof(SearchPage), key);
    }
}