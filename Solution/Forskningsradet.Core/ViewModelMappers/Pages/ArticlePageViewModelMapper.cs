﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Routing;
using EPiServer.Globalization;
using EPiServer.Web.Routing;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Extensions;
using Forskningsradet.Core.Facades.Contracts;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.QueryParameters;
using Forskningsradet.Core.Models.RequestModels;
using Forskningsradet.Core.Models.ViewModels.Contracts;
using Forskningsradet.Core.Models.ViewModels.Pages;
using Forskningsradet.Core.Services;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.ViewModelMappers.Pages
{
    public abstract class ArticlePageViewModelMapper<T> : BasePageViewModelMapper<T> where T : ArticlePage
    {
        private readonly IRichTextReactModelBuilder _richTextReactModelBuilder;
        private readonly ILocalizationProvider _localizationProvider;
        private readonly IPageEditingAdapter _pageEditingAdapter;
        private readonly ISocialMediaLinkListReactModelBuilder _socialMediaLinkListReactModelBuilder;
        private readonly IContentAreaReactModelBuilder _contentAreaReactModelBuilder;
        private readonly ITagLinkListReactModelBuilder _tagLinkListReactModelBuilder;
        private readonly IUrlResolver _urlResolver;
        private readonly IContentListService _contentListService;
        private static string By => nameof(By);
        private static string TranslatedBy => nameof(TranslatedBy);
        private static string Published => nameof(Published);
        private static string Changed => nameof(Changed);
        private static string Download => nameof(Download);

        protected ArticlePageViewModelMapper(
            IContentAreaReactModelBuilder contentAreaReactModelBuilder,
            IRichTextReactModelBuilder richTextReactModelBuilder,
            ILocalizationProvider localizationProvider,
            IPageEditingAdapter pageEditingAdapter,
            ISocialMediaLinkListReactModelBuilder socialMediaLinkListReactModelBuilder,
            ITagLinkListReactModelBuilder tagLinkListReactModelBuilder,
            IUrlResolver urlResolver,
            IContentListService contentListService)
        {
            _contentAreaReactModelBuilder = contentAreaReactModelBuilder;
            _richTextReactModelBuilder = richTextReactModelBuilder;
            _localizationProvider = localizationProvider;
            _pageEditingAdapter = pageEditingAdapter;
            _socialMediaLinkListReactModelBuilder = socialMediaLinkListReactModelBuilder;
            _tagLinkListReactModelBuilder = tagLinkListReactModelBuilder;
            _urlResolver = urlResolver;
            _contentListService = contentListService;
        }

        protected sealed override IReactViewModel GetViewModelForPageInternal(T currentPage)
        {
            var viewModel = BuildReactModel(currentPage);
            return new ReactPageViewModel(currentPage, viewModel);
        }

        protected abstract ReactModels.ArticleHeader BuildHeader(T currentPage);

        protected abstract ReactModels.PageFooter BuildFooter(T currentPage);

        protected ReactModels.Byline BuildByline(ArticlePage currentPage) =>
            new ReactModels.Byline
            {
                Items = BuildBylineItems(currentPage).ToList()
            };

        protected ReactModels.SocialMediaLinkList BuildSocialMediaLinkList(T currentPage) =>
            _socialMediaLinkListReactModelBuilder.BuildReactModel(currentPage.PageLink);

        protected ReactModels.Link BuildDownloadLink(T currentPage) =>
            new ReactModels.Link
            {
                Url = BuildDownloadUrl(currentPage),
                Text = GetLabel(Download)
            };

        protected string GetLabel(string key) => _localizationProvider.GetLabel(nameof(ArticlePage), key);

        private ReactModels.ReactComponent BuildReactModel(T currentPage) =>
            new ReactModels.ArticlePage
            {
                OnPageEditing = _pageEditingAdapter.PageIsInEditMode()
                        ? BuildOnPageEditingReactModel(currentPage)
                        : null,
                Ingress = currentPage.MainIntro,
                Title = currentPage.PageName,
                Content = BuildMainContent(currentPage),
                Footer = BuildFooter(currentPage),
                Header = BuildHeader(currentPage),
                Sidebar = _contentAreaReactModelBuilder.BuildReactModel(currentPage.RightBlockArea,
                                                                        nameof(currentPage.RightBlockArea),
                                                                        new RenderOptionQueryParameter { RenderWidthOption = RenderWidthOption.IsInSidebar,
                                                                                                         RenderMediaBlockOption = RenderMediaBlockOption.MediaWithCaption })
            };

        private ReactModels.ArticlePage_OnPageEditing BuildOnPageEditingReactModel(T currentPage) =>
            new ReactModels.ArticlePage_OnPageEditing
            {
                Title = nameof(currentPage.PageName),
                Ingress = nameof(currentPage.MainIntro)
            };

        private ReactModels.RichText BuildMainContent(T currentPage)
        {
            var richText = _richTextReactModelBuilder.BuildReactModel(currentPage.MainBody,
                                                                        nameof(currentPage.MainBody));

            if (!currentPage.ShowCategories)
                return richText;

            var contentItem = new ReactModels.ContentAreaItem
            {
                ComponentName = nameof(ReactModels.TagLinkList),
                ComponentData = _tagLinkListReactModelBuilder.BuildReactModel(currentPage.Category, null, GetCategoryHomePages(), true, false)
            };
            richText.Blocks.Add(contentItem);

            return richText;
        }

        private IReadOnlyList<ContentAreaPage> GetCategoryHomePages() =>
            _contentListService.Search<ContentAreaPage>(new ContentListRequestModel { OnlyGetCategoryHomePages = true, Take = SearchConstants.MaxPageSize })
                ?.Results
                .OfType<ContentAreaPage>()
                .ToList() ?? new List<ContentAreaPage>();

        private string BuildDownloadUrl(T currentPage) =>
            _urlResolver.GetUrl(
                currentPage.ContentLink,
                ContentLanguage.PreferredCulture.Name,
                new UrlResolverArguments
                {
                    RouteValues = new RouteValueDictionary(new { action = "Download" })
                });

        private IEnumerable<ReactModels.Byline_Items> BuildBylineItems(ArticlePage currentPage)
        {
            if (!string.IsNullOrWhiteSpace(currentPage.Author) || _pageEditingAdapter.PageIsInEditMode())
                yield return BuildBylineText(By, currentPage.Author, nameof(ArticlePage.Author));

            if (!string.IsNullOrWhiteSpace(currentPage.TranslatedBy) || _pageEditingAdapter.PageIsInEditMode())
                yield return BuildBylineText(TranslatedBy, currentPage.TranslatedBy, nameof(ArticlePage.TranslatedBy));

            if (currentPage.StartPublish is null)
            {
                yield return BuildChangedByline(currentPage);
            }
            else if (currentPage.Changed.Date.Equals(currentPage.StartPublish.Value.Date))
            {
                yield return BuildPublishedByline(currentPage);
            }
            else
            {
                yield return BuildPublishedByline(currentPage);
                yield return BuildChangedByline(currentPage);
            }
        }

        private ReactModels.Byline_Items BuildChangedByline(ArticlePage currentPage) =>
            BuildBylineDateTime(Changed, currentPage.Changed);

        private ReactModels.Byline_Items BuildPublishedByline(ArticlePage currentPage) =>
            BuildBylineDateTime(Published, currentPage.StartPublish.GetValueOrDefault());

        private ReactModels.Byline_Items BuildBylineText(string label, string text, string propertyName) =>
            BuildBylineItem(BuildLocalizedFormatString(label, text), propertyName);

        private ReactModels.Byline_Items BuildBylineDateTime(string label, DateTime datetime) =>
            BuildBylineItem(BuildLocalizedFormatString(label, FormatDateString(datetime)), null);

        private ReactModels.Byline_Items BuildBylineItem(string text, string propertyName) =>
            new ReactModels.Byline_Items
            {
                Text = text,
                OnPageEditing = _pageEditingAdapter.PageIsInEditMode()
                        ? propertyName
                        : null
            };

        private string BuildLocalizedFormatString(string key, string parameter) =>
            !string.IsNullOrWhiteSpace(parameter)
                ? $"{GetLabel(key)} {parameter}"
                : null;

        private static string FormatDateString(DateTime dateTime) =>
            dateTime.ToDisplayDate().ToNorwegianDateString();
    }
}
