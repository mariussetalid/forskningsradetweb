﻿using System.Collections.Generic;
using System.Linq;
using EPiServer;
using EPiServer.Web.Routing;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Extensions;
using Forskningsradet.Core.Facades.Contracts;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.ViewModels.Contracts;
using Forskningsradet.Core.Models.ViewModels.Pages;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.ViewModelMappers.Pages
{
    public class SatelliteFrontPageViewModelMapper : BasePageViewModelMapper<SatelliteFrontPage>
    {
        private readonly IContentAreaReactModelBuilder _contentAreaReactModelBuilder;
        private readonly IFluidImageReactModelBuilder _fluidImageReactModelBuilder;
        private readonly IPageEditingAdapter _pageEditingAdapter;
        private readonly IContentLoader _contentLoader;
        private readonly IUrlResolver _urlResolver;

        public SatelliteFrontPageViewModelMapper(IContentAreaReactModelBuilder contentAreaReactModelBuilder, IFluidImageReactModelBuilder fluidImageReactModelBuilder, IPageEditingAdapter pageEditingAdapter, IContentLoader contentLoader, IUrlResolver urlResolver)
        {
            _contentAreaReactModelBuilder = contentAreaReactModelBuilder;
            _fluidImageReactModelBuilder = fluidImageReactModelBuilder;
            _pageEditingAdapter = pageEditingAdapter;
            _contentLoader = contentLoader;
            _urlResolver = urlResolver;
        }

        protected override IReactViewModel GetViewModelForPageInternal(SatelliteFrontPage currentPage)
        {
            var reactComponent = BuildReactComponent(currentPage);
            return new ReactPageViewModel(currentPage, reactComponent);
        }

        private ReactModels.SatelliteFrontpage BuildReactComponent(SatelliteFrontPage page) =>
            new ReactModels.SatelliteFrontpage
            {
                SatelliteHeader = BuildHeader(page),
                Content = _contentAreaReactModelBuilder.BuildReactModel(page.MainContent, nameof(page.MainContent))
            };

        private ReactModels.SatelliteHeader BuildHeader(SatelliteFrontPage page)
        {
            var imageFile = page.Image?.GetAsImageFile();
            return new ReactModels.SatelliteHeader
            {
                Title = page.Title ?? page.PageName,
                Ingress = page.ListIntro,
                Image = imageFile != null || _pageEditingAdapter.PageIsInEditMode() ? _fluidImageReactModelBuilder.BuildReactModel(imageFile, nameof(page.Image)) : null,
                Links = GetLinks(page.LinkBlock1, page.LinkBlock2, page.LinkBlock3).ToList(),
                Text = page.Text,
                TextColor = page.TextStyle,
                OnPageEditing = new ReactModels.SatelliteHeader_OnPageEditing
                {
                    Title = nameof(page.Title),
                    Ingress = nameof(page.ListIntro)
                }
            };
        }

        private IEnumerable<ReactModels.Link> GetLinks(params EditPageLinkBlock[] blocks) =>
            blocks
            .Where(x => !(x.Link is null))
            .Select(x => GetLink(x));

        private ReactModels.Link GetLink(EditPageLinkBlock linkBlock) =>
            new ReactModels.Link
            {
                Text = linkBlock.Text ?? _contentLoader.Get<EditorialPage>(linkBlock.Link).GetListTitle(),
                Url = _urlResolver.GetUrl(linkBlock.Link)
            };
    }
}
