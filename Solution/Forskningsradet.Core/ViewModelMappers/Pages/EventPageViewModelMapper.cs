﻿using System;
using System.Collections.Generic;
using System.Linq;
using EPiServer;
using EPiServer.Core;
using EPiServer.Web.Routing;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Extensions;
using Forskningsradet.Core.Facades.Contracts;
using Forskningsradet.Core.Factories;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ContentModels.Media;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.ViewModels.Contracts;
using Forskningsradet.Core.Models.ViewModels.Pages;
using Forskningsradet.Core.Repositories.Contracts;
using Forskningsradet.Core.Services;
using Forskningsradet.Core.Services.Contracts;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.ViewModelMappers.Pages
{
    public class EventPageViewModelMapper : BasePageViewModelMapper<EventPage>
    {
        private readonly IContentAreaReactModelBuilder _contentAreaReactModelBuilder;
        private readonly IRichTextReactModelBuilder _richTextReactModelBuilder;
        private readonly IDateCardDatesReactModelBuilder _dateCardDatesReactModelBuilder;
        private readonly IFluidImageReactModelBuilder _fluidImageReactModelBuilder;
        private readonly IAccordionWithContentAreaListReactModelBuilder _accordionWithContentAreaListReactModelBuilder;
        private readonly IViewModelFactory _viewModelFactory;
        private readonly IUrlResolver _urlResolver;
        private readonly IUrlCheckingService _urlCheckingService;
        private readonly IContentLoader _contentLoader;
        private readonly ILocalizationProvider _localizationProvider;
        private readonly IPageEditingAdapter _pageEditingAdapter;
        private readonly IOptionsModalReactModelBuilder _optionsModalReactModelBuilder;
        private readonly IPageRepository _pageRepository;

        public EventPageViewModelMapper(
            IContentAreaReactModelBuilder contentAreaReactModelBuilder,
            IRichTextReactModelBuilder richTextReactModelBuilder,
            IDateCardDatesReactModelBuilder dateCardDatesReactModelBuilder,
            IFluidImageReactModelBuilder fluidImageReactModelBuilder,
            IAccordionWithContentAreaListReactModelBuilder accordionWithContentAreaListReactModelBuilder,
            IViewModelFactory viewModelFactory,
            IUrlResolver urlResolver,
            IUrlCheckingService urlCheckingService,
            IContentLoader contentLoader,
            ILocalizationProvider localizationProvider,
            IPageEditingAdapter pageEditingAdapter,
            IOptionsModalReactModelBuilder optionsModalReactModelBuilder,
            IPageRepository pageRepository)
        {
            _contentAreaReactModelBuilder = contentAreaReactModelBuilder;
            _richTextReactModelBuilder = richTextReactModelBuilder;
            _dateCardDatesReactModelBuilder = dateCardDatesReactModelBuilder;
            _fluidImageReactModelBuilder = fluidImageReactModelBuilder;
            _accordionWithContentAreaListReactModelBuilder = accordionWithContentAreaListReactModelBuilder;
            _viewModelFactory = viewModelFactory;
            _urlResolver = urlResolver;
            _urlCheckingService = urlCheckingService;
            _contentLoader = contentLoader;
            _localizationProvider = localizationProvider;
            _pageEditingAdapter = pageEditingAdapter;
            _optionsModalReactModelBuilder = optionsModalReactModelBuilder;
            _pageRepository = pageRepository;
        }

        protected override IReactViewModel GetViewModelForPageInternal(EventPage eventPage) =>
            new ReactPageViewModel(eventPage, BuildEventPageReactModel(eventPage), BuildFullRefreshProperties());

        private ReactModels.EventPage BuildEventPageReactModel(EventPage page) =>
            new ReactModels.EventPage
            {
                OnPageEditing = _pageEditingAdapter.PageIsInEditMode()
                    ? new ReactModels.EventPage_OnPageEditing { Title = nameof(page.PageName) }
                    : null,
                Title = (page.Canceled ? GetLabel("CanceledEventTitlePrefix") : "") + page.PageName,
                Menu = BuildEventMenu(page),
                Metadata = BuildEventData(page, nameof(page.EventData)),
                RegistrationLink = page.Canceled ? null : BuildRegistrationLink(page, nameof(page.RegistrationLink)),
                Labels = GetEventLabels(page),
                Links = BuildLinks(page),
                RichText = _richTextReactModelBuilder.BuildReactModel(page.MainBody, nameof(page.MainBody)),
                Speakers = BuildContentArea(page.SpeakersContentArea, nameof(page.SpeakersContentArea)),
                Schedule = BuildScheduleContentArea(page.ScheduleContentArea, nameof(page.ScheduleContentArea)),
                ContactInfo = BuildContentArea(page.ContactContentArea, nameof(page.ContactContentArea)),
                Share = _optionsModalReactModelBuilder.BuildShareContent(page),
                DateContainer = _dateCardDatesReactModelBuilder.BuildReactModel(page, nameof(page.Image)),
                EventImage = BuildEventImage(page),
                Media = BuildMedia(page.EventData)
            };

        private ReactModels.DateCardMedia BuildMedia(EventDataBlock eventData)
        {
            return new ReactModels.DateCardMedia
            {
                Items = BuildItems().ToList()
            };

            IEnumerable<ReactModels.DateCardMedia_Items> BuildItems()
            {
                if (eventData?.Type == EventType.Digital)
                    yield return new ReactModels.DateCardMedia_Items
                    {
                        Icon = ReactModels.DateCardMedia_Items_Icon.Camera,
                        Text = eventData?.RecordingWillBeAvailable == true ? GetLabel("StreamedAndRecordedEvent") : GetLabel("StreamedEvent")
                    };
            }
        }

        private ReactModels.ContentArea BuildContentArea(ContentArea contentArea, string propertyName) =>
            contentArea?.FilteredItems.Any() ?? _pageEditingAdapter.PageIsInEditMode()
                ? _contentAreaReactModelBuilder.BuildReactModel(contentArea, propertyName)
                : null;

        ReactModels.ContentArea BuildScheduleContentArea(ContentArea contentArea, string propertyName)
        {
            var scheduleBlocks = contentArea?.FilteredItems
                .Select(x => _contentLoader.Get<IContent>(x.ContentLink))
                .OfType<StructuredScheduleBlock>() ?? new List<StructuredScheduleBlock>();

            return !scheduleBlocks.Any()
                ? BuildContentArea(contentArea, propertyName)
                : CreateItemInContentArea(
                    _accordionWithContentAreaListReactModelBuilder.BuildReactModel(scheduleBlocks.ToArray()),
                    nameof(ReactModels.AccordionWithContentAreaList));

            ReactModels.ContentArea CreateItemInContentArea(ReactModels.ReactComponent component, string componentName) =>
                _contentAreaReactModelBuilder.BuildReactModel(
                    new List<ReactModels.ContentAreaItem>
                    {
                        new ReactModels.ContentAreaItem
                        {
                            Id = "0",
                            ComponentName = componentName,
                            ComponentData = component
                        }
                    },
                    propertyName);
        }

        private string[] BuildFullRefreshProperties() =>
            new[]
            {
                nameof(EventPage.EventData),
                nameof(EventPage.RegistrationLink),
            };

        private ReactModels.StickyMenuOnTabs BuildEventMenu(EventPage page) =>
            new ReactModels.StickyMenuOnTabs
            {
                Title = GetLabel("ShortcutsTitle"),
                NavGroups = new List<ReactModels.StickyMenuOnTabs_NavGroups>
                {
                    new ReactModels.StickyMenuOnTabs_NavGroups
                    {
                        Links = new ReactModels.StickyMenuOnTabs_NavGroups_Links
                        {
                            Items = BuildMenuItems(page).ToList()
                        }
                    }
                }
            };

        private IEnumerable<ReactModels.StickyMenuOnTabsItem> BuildMenuItems(EventPage page)
        {
            yield return BuildItem(page.MainBodyTitle, "AboutTitle");

            if (ContentAreaHasItems(page.SpeakersContentArea))
                yield return BuildItem(page.SpeakersTitle, "SpeakersTitle");

            if (ContentAreaHasItems(page.ScheduleContentArea))
                yield return BuildItem(page.ScheduleTitle, "ScheduleTitle");

            if (ContentAreaHasItems(page.ContactContentArea))
                yield return BuildItem(string.Empty, "ContactTitle");

            ReactModels.StickyMenuOnTabsItem BuildItem(string title, string fallbackLabelKey) =>
                new ReactModels.StickyMenuOnTabsItem
                {
                    Link = BuildStickyMenuLink(title, fallbackLabelKey)
                };
        }

        private ReactModels.StickyMenuOnTabsItem_Link BuildStickyMenuLink(string title, string fallbackLabelKey) =>
            new ReactModels.StickyMenuOnTabsItem_Link
            {
                Text = GetEventLabelTitle(title, fallbackLabelKey),
                Url = $"#{fallbackLabelKey}"
            };

        private ReactModels.EventPage_Labels GetEventLabels(EventPage page)
        {
            return new ReactModels.EventPage_Labels
            {
                About = BuildLabel(page.MainBodyTitle, "AboutTitle"),
                Speakers = BuildLabel(page.SpeakersTitle, "SpeakersTitle"),
                Schedule = BuildLabel(page.ScheduleTitle, "ScheduleTitle"),
                Contact = BuildLabel(string.Empty, "ContactTitle")
            };

            ReactModels.Label BuildLabel(string title, string fallbackLabelKey) =>
                new ReactModels.Label
                {
                    Title = GetEventLabelTitle(title, fallbackLabelKey),
                    HtmlId = fallbackLabelKey
                };
        }

        private string GetEventLabelTitle(string title, string fallbackLabelKey) =>
            !string.IsNullOrEmpty(title) ? title : GetLabel(fallbackLabelKey);

        private ReactModels.EventData BuildEventData(EventPage page, string propertyName)
        {
            var data = page.EventData;
            var eventData = _viewModelFactory.GetPartialViewModel(data)?.Model as ReactModels.EventData ?? new ReactModels.EventData();
            eventData.OnPageEditing = _pageEditingAdapter.PageIsInEditMode()
                ? new ReactModels.EventData_OnPageEditing { Items = propertyName }
                : null;

            if (EditorFieldForTimesIsEmptyAndShouldUseEventStartAndEndDate(data?.WhenList)
                && BuildDateTimeText(page.StartDate, page.EndDate) is string dateTimeText)
            {
                eventData.Items.Insert(0, new ReactModels.EventData_Items
                {
                    Label = GetLabel("EventDataWhenTitle"),
                    Text = new List<IList<string>>
                    {
                        new List<string>
                        {
                            dateTimeText
                        }
                    }
                });
            }

            return eventData;
        }

        private static bool EditorFieldForTimesIsEmptyAndShouldUseEventStartAndEndDate(IList<string> editorSpecifiedTimes) =>
            editorSpecifiedTimes is null || editorSpecifiedTimes.All(string.IsNullOrWhiteSpace);

        private string BuildDateTimeText(DateTime? start, DateTime? end) =>
            start?.ToDisplayDate() is DateTime startDate
                ? startDate.ToString(GetLabel(nameof(DateTimeFormats.EventDateTimeFull)))
                  + (end?.ToDisplayDate() is DateTime endDate
                      ? " - " + (startDate.ToString("M") == endDate.ToString("M")
                            ? endDate.ToString(DateTimeFormats.EventTime)
                            : endDate.ToString(GetLabel(nameof(DateTimeFormats.EventDateTimeFull))))
                      : string.Empty)
                : null;

        private ReactModels.Link BuildRegistrationLink(EventPage page, string propertyName) =>
            new ReactModels.Link
            {
                OnPageEditing = _pageEditingAdapter.PageIsInEditMode()
                    ? propertyName
                    : null,
                Text = !string.IsNullOrEmpty(page.RegistrationLink?.ToString())
                    ? GetLabel("RegistrationText")
                    : null,
                Url = GetRegistrationUrl(page),
                IsExternal = page.RegistrationLink.IsExternal(_urlCheckingService)
            };

        private string GetRegistrationUrl(EventPage page) =>
            CheckIfPastEvent(page)
                ? null
                : GetUrl(page.RegistrationLink);

        private List<ReactModels.EventPageLink> BuildLinks(EventPage page) =>
            new List<ReactModels.EventPageLink>
            {
               !page.Canceled && !page.HideCalendarLink && page.StartDate != null && page.EndDate != null && !page.EndDate.Value.IsPast()
                    ? new ReactModels.EventPageLink {Text = GetLabel("AddToCalendar"), Url = $"?{QueryParameters.DownloadCalendarQuery}=true", Icon = ReactModels.EventPageLink_Icon.Calendar}
                    : null,
                GetUrl(page.VideoLink) is string videoUrl
                    ? new ReactModels.EventPageLink
                    {
                        Text = CheckIfPastEvent(page) ? GetLabel("VideoTextPastEvent") : GetLabel("VideoText"),
                        Url = videoUrl,
                        Icon = ReactModels.EventPageLink_Icon.Video
                    }
                    : null,
            }
            .Where(x => x != null)
            .ToList();

        private ReactModels.EventImage BuildEventImage(EventPage eventPage)
        {
            var imageLink = eventPage.Canceled
                ? TryGetCancelledEventImageLink() ?? eventPage.Image
                : eventPage.Image;

            return new ReactModels.EventImage
            {
                Image = imageLink is null
                    ? null
                    : _fluidImageReactModelBuilder.BuildReactModel(_contentLoader.Get<ImageFile>(imageLink), null),
                Background = (ReactModels.EventImage_Background)eventPage.EventImageStyle
            };
        }

        private ContentReference TryGetCancelledEventImageLink() =>
            _pageRepository.GetCurrentStartPageAsFrontPageBase()?.CanceledEventImage;

        private bool ContentAreaHasItems(ContentArea contentArea) =>
            contentArea?.FilteredItems.Any() == true;

        private static bool CheckIfPastEvent(EventPage page) =>
            page.ComputedEventEndDate?.IsPast() ?? false;

        private string GetUrl(Url url) =>
            url.GetUrl(_urlResolver);

        private string GetLabel(string key) =>
            _localizationProvider.GetLabel(nameof(EventPage), key);
    }
}