using System;
using System.Collections.Generic;
using System.Linq;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.Web.Routing;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Builders;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Extensions;
using Forskningsradet.Core.Factories;
using Forskningsradet.Core.Models.ApiModels.Evurdering;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.QueryParameters;
using Forskningsradet.Core.Models.RequestModels;
using Forskningsradet.Core.Models.SearchModels;
using Forskningsradet.Core.Models.ViewModels.Contracts;
using Forskningsradet.Core.Models.ViewModels.Pages;
using Forskningsradet.Core.Services;
using Forskningsradet.Core.Services.Contracts;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.ViewModelMappers.Pages
{
    public class ProposalListPageViewModelMapper : BasePageViewModelMapperWithQuery<ProposalListPage>
    {
        private readonly IProposalService _proposalService;
        private readonly ILocalizationProvider _localizationProvider;
        private readonly IUrlResolver _urlResolver;
        private readonly IRichTextReactModelBuilder _richTextReactModelBuilder;
        private readonly IContentAreaReactModelBuilder _contentAreaReactModelBuilder;
        private readonly IDateCardDatesReactModelBuilder _dateCardDatesReactModelBuilder;
        private readonly IDateCardStatusHandler _dateCardStatusHandler;
        private readonly FilterLayoutReactModelBuilder _filterLayoutReactModelBuilder;
        private readonly ICategoryLocalizationProvider _categoryLocalizationProvider;
        private readonly CategoryRepository _categoryRepository;
        private readonly IMessageReactModelBuilder _messageReactModelBuilder;
        private readonly IContentLoader _contentLoader;
        private readonly IViewModelFactory _viewModelFactory;
        private readonly ISearchApiUrlResolver _searchApiUrlResolver;

        public ProposalListPageViewModelMapper(
            IProposalService proposalService,
            ILocalizationProvider localizationProvider,
            IUrlResolver urlResolver,
            IRichTextReactModelBuilder richTextReactModelBuilder,
            IContentAreaReactModelBuilder contentAreaReactModelBuilder,
            IDateCardDatesReactModelBuilder dateCardDatesReactModelBuilder,
            IDateCardStatusHandler dateCardStatusHandler,
            FilterLayoutReactModelBuilder filterLayoutReactModelBuilder,
            ICategoryLocalizationProvider categoryLocalizationProvider,
            CategoryRepository categoryRepository,
            IMessageReactModelBuilder messageReactModelBuilder,
            IContentLoader contentLoader,
            IViewModelFactory viewModelFactory,
            ISearchApiUrlResolver searchApiUrlResolver)
        {
            _proposalService = proposalService;
            _localizationProvider = localizationProvider;
            _urlResolver = urlResolver;
            _richTextReactModelBuilder = richTextReactModelBuilder;
            _contentAreaReactModelBuilder = contentAreaReactModelBuilder;
            _dateCardDatesReactModelBuilder = dateCardDatesReactModelBuilder;
            _dateCardStatusHandler = dateCardStatusHandler;
            _filterLayoutReactModelBuilder = filterLayoutReactModelBuilder;
            _categoryLocalizationProvider = categoryLocalizationProvider;
            _categoryRepository = categoryRepository;
            _messageReactModelBuilder = messageReactModelBuilder;
            _contentLoader = contentLoader;
            _viewModelFactory = viewModelFactory;
            _searchApiUrlResolver = searchApiUrlResolver;
        }

        protected override IReactViewModel GetViewModelForPageInternal(ProposalListPage page, QueryParameterBase parameters) =>
            new ReactPageViewModel(page, BuildProposalListPageReactModel(page, parameters as ProposalListQueryParameter));

        private ReactModels.ReactComponent BuildProposalListPageReactModel(ProposalListPage page, ProposalListQueryParameter parameters)
        {
            var results = _proposalService.Search(
                page.PageRoot ?? page.ContentLink,
                parameters.TimeFrame,
                parameters.Subjects,
                parameters.TargetGroups,
                parameters.ApplicationTypes,
                parameters.DeadlineTypes);

            return new ReactModels.ProposalListPage
            {
                Title = page.PageName,
                TopFilter = BuildTopFilter(parameters.TimeFrame),
                Form = BuildForm(page),
                Groups = BuildProposalGroups(results.Results, page.NumberOfVisibleItems, parameters.TimeFrame),
                FilterLayout = BuildFilterLayout(parameters, results.Categories, page, results.TotalMatching),
                EmptyList = BuildEmptyList(results.TotalMatching),
                SectionTitle = BuildSectionTitle(page, parameters.TimeFrame),
                FetchFilteredResultsEndpoint = _searchApiUrlResolver.GetSearchProposalUrl(),
            };
        }

        private string BuildSectionTitle(ProposalListPage page, TimeFrameFilter timeFrame)
        {
            if (timeFrame == TimeFrameFilter.Future)
            {
                return page.CurrentSectionTitle;
            }
            else if (timeFrame == TimeFrameFilter.Past)
            {
                return page.ClosedSectionTitle;
            }
            else if (timeFrame == TimeFrameFilter.Result)
            {
                return page.ResultSectionTitle;
            }
            else
            {
                return page.CurrentSectionTitle;
            };
        }

        private ReactModels.FilterLayout BuildFilterLayout(ProposalListQueryParameter parameters, IEnumerable<CategoryGroup> categories, ProposalListPage page, int resultsCount) =>
            _filterLayoutReactModelBuilder.BuildReactModel(
                new FilterLayoutModel(
                    parameters,
                    categories.ToList(),
                    _contentAreaReactModelBuilder.BuildReactModel(page.RightBlockArea, nameof(page.RightBlockArea)), page.FilterTitle, resultsCount));

        private ReactModels.EmptyList BuildEmptyList(int totalMatching) =>
            totalMatching == 0
                ? new ReactModels.EmptyList
                {
                    Text = GetLabel("EmptyListLabel")
                }
                : null;

        private IList<ReactModels.ProposalGroup> BuildProposalGroups(IEnumerable<ApplicationTypeGroup> groups, int visibleItemsInGroup, TimeFrameFilter timeFrame) =>
            groups.Select(group =>
                new ReactModels.ProposalGroup
                {
                    Title = GetProposalGroupsTitle(group),
                    Subtitle = group.SubTitle,
                    Text = _richTextReactModelBuilder.BuildReactModel(group.Text, nameof(group.Text)),
                    NumberOfVisibleItems = visibleItemsInGroup,
                    Accordion = BuildAccordion(visibleItemsInGroup, group.Pages.Count()),
                    Proposals = BuildProposals(group.Pages, timeFrame)
                }
            ).ToList();

        private string GetProposalGroupsTitle(ApplicationTypeGroup group) =>
            $"{group.Title} ({group.Pages.Count()} {(group.Pages.Count() == 1 ? GetLabel("ProposalInGroup") : GetLabel("ProposalsInGroup"))})";

        private ReactModels.Accordion BuildAccordion(int visiblePageLimit, int pageCount) =>
            pageCount > visiblePageLimit
                ? new ReactModels.Accordion
                {
                    Guid = Guid.NewGuid().ToString(),
                    InitiallyOpen = false,
                    CollapseLabel = GetLabel("CollapseLabel"),
                    ExpandLabel = string.Format(GetLabel("ExpandLabel"), pageCount - visiblePageLimit)
                }
                : null;

        private IList<ReactModels.DateCard> BuildProposals(IEnumerable<ProposalBasePage> pages, TimeFrameFilter timeFrame) =>
            pages
                .GetOrderedProposals(timeFrame)
                .Select(page =>
                    new ReactModels.DateCard
                    {
                        Id = page.Id,
                        MessageList = GetMessages(page).ToList(),
                        Title = page.GetListTitle(),
                        Url = page.DisableLink ? null : _urlResolver.GetUrl(page),
                        Text = page.GetListIntroOrMainIntro(350),
                        Status = BuildStatus(page),
                        DateContainer = _dateCardDatesReactModelBuilder.BuildReactModel(page, GetDatesTitle(page), GetDateDescription(page), page.ProposalState == ProposalState.Planned),
                        Metadata = GetProposalMetaData(page),
                        Tags = BuildTags(page.Category, ListPageConstants.NumberOfVisibleTags)
                    }).ToList();

        private string GetDatesTitle(ProposalBasePage page)
        {
            return page is InternationalProposalPage international
                ? GetInternationalDatesTitle()
                : GetRegularDatesTitle();

            string GetInternationalDatesTitle()
            {
                if (international.FirstDeadline.HasValue && !international.FirstDeadline.Value.IsPast())
                    return GetLabel("Deadline1");
                if (international.FirstDeadline.HasValue && international.Deadline.HasValue && !international.Deadline.Value.IsPast())
                    return GetLabel("Deadline2");

                return GetRegularDatesTitle();
            }

            string GetRegularDatesTitle() => GetLabel("Deadline");
        }

        private IEnumerable<ReactModels.Message> GetMessages(ProposalBasePage proposalBasePage)
        {
            if (proposalBasePage.CancellationText != null)
                yield return _messageReactModelBuilder.BuildReactModel(proposalBasePage.CancellationText?.ToString(), null);

            if (proposalBasePage is ProposalPage proposalpage && proposalpage.ListMessages != null && proposalpage.ListMessages.FilteredItems.Any())
                yield return _viewModelFactory.GetPartialViewModel(_contentLoader.Get<MessageBaseBlock>(proposalpage.ListMessages.FilteredItems.First().ContentLink)).Model as ReactModels.Message;
        }

        private string GetDateDescription(ProposalBasePage page)
        {
            if (page is ProposalPage proposalPage)
            {
                var deadlineType = proposalPage.DeadlineType;

                if (deadlineType == DeadlineType.NotSet)
                    return GetLabel("NotSetDeadline");
                if (deadlineType == DeadlineType.Continuous)
                    return GetLabel("ContinuousDeadline");
                return null;
            }
            return null;
        }

        private ReactModels.DateCardStatus BuildStatus(ProposalBasePage proposalBasePage) =>
            _dateCardStatusHandler.GetCurrentStatus(proposalBasePage);

        private ReactModels.DateCardTags BuildTags(CategoryList categories, int numberOfVisibleItems) =>
            categories.IsEmpty
                ? null
                : new ReactModels.DateCardTags
                {
                    Items = BuildTagList(categories),
                    Accordion = categories.Count - numberOfVisibleItems > 0
                        ? new ReactModels.Accordion
                        {
                            Guid = Guid.NewGuid().ToString(),
                            ExpandLabel = string.Format(GetLabel("ExpandTagsFormat"),
                                categories.Count - numberOfVisibleItems),
                            CollapseLabel = GetLabel("CollapseTags")
                        }
                        : null,
                    NumberOfVisibleItems = numberOfVisibleItems
                };

        private IList<ReactModels.Link> BuildTagList(CategoryList categories) =>
            categories
                .Select(cat => _categoryRepository.Get(cat))
                .Where(cat => cat.Parent.Name == CategoryConstants.SubjectRoot)
                .Select(x => new ReactModels.Link { Text = _categoryLocalizationProvider.GetCategoryName(x.ID) })
                .ToList();

        private ReactModels.Metadata GetProposalMetaData(ProposalBasePage proposalBasePage)
        {
            if (proposalBasePage is ProposalPage proposalPage)
                return BuildProposalMetaData(proposalPage);
            if (proposalBasePage is InternationalProposalPage internationalProposalPage)
                return BuildProposalMetaData(internationalProposalPage);
            return null;
        }

        private ReactModels.Metadata BuildProposalMetaData(ProposalPage page) =>
            new ReactModels.Metadata
            {
                Items = new List<ReactModels.Metadata_Items>
                {
                    !string.IsNullOrEmpty(page.ProposalAmount) && page.ProposalAmount != "0"
                        ? new ReactModels.Metadata_Items
                        {
                            Label = GetLabel("Amount"),
                            Text = string.Format(GetLabel("NokFormat"), page.ProposalAmount)
                        }
                        : null,
                    page.MinApplicationAmount > 0 && page.MaxApplicationAmount > 0
                        ? new ReactModels.Metadata_Items
                        {
                            Label = GetLabel("ProjectSize"),
                            Text = string.Format(GetLabel("ProjectSizeFormat"),
                                page.MinApplicationAmount.ToLargeNumberFormat(),
                                page.MaxApplicationAmount.ToLargeNumberFormat())
                        }
                        : null,
                    page.MinProjectLength > 0 && page.MaxProjectLength > 0
                        ? new ReactModels.Metadata_Items
                        {
                            Label = GetLabel("Duration"),
                            Text = string.Format(GetLabel("DurationFormat"),
                                page.MinProjectLength,
                                page.MaxProjectLength)
                        }
                        : null
                }.Where(x => x != null).ToList()
            };

        private ReactModels.Metadata BuildProposalMetaData(InternationalProposalPage page) =>
            new ReactModels.Metadata
            {
                Items = new List<ReactModels.Metadata_Items>
                {
                    page.ActivationDate.HasValue && !page.ActivationDate.Value.IsPast()
                        ? new ReactModels.Metadata_Items
                        {
                            Label = string.IsNullOrEmpty(page.ActivationDateDisplayName) ? GetLabel("ActivationDate") : page.ActivationDateDisplayName,
                            Text = page.ActivationDate.Value.ToDisplayDate().ToString(DateTimeFormats.ProposalDeadlineDate)
                        }
                        : null,
                    page.FirstDeadline.HasValue
                        ? new ReactModels.Metadata_Items
                        {
                            Label = string.IsNullOrEmpty(page.FirstDeadlineDisplayName) ? GetLabel("Deadline1") : page.FirstDeadlineDisplayName,
                            Text = page.FirstDeadline.Value.ToDisplayDate().ToString(DateTimeFormats.ProposalDeadlineDate)
                        }
                        : null,
                    page.Deadline.HasValue
                        ? new ReactModels.Metadata_Items
                        {
                            Label = string.IsNullOrEmpty(page.DeadlineDisplayName) ? GetLabel("Deadline2") : page.DeadlineDisplayName,
                            Text = page.Deadline.Value.ToDisplayDate().ToString(DateTimeFormats.ProposalDeadlineDate)
                        }
                        : null,
                    string.IsNullOrEmpty(page.Program)
                        ? null
                        : new ReactModels.Metadata_Items
                        {
                            Label = GetLabel("Program"),
                            Text = page.Program
                        }
                }.Where(x => x != null).ToList()
            };

        private ReactModels.TopFilter BuildTopFilter(TimeFrameFilter timeframe) =>
            new ReactModels.TopFilter
            {
                Timeframe = GetStatus(timeframe),
                Options = new List<ReactModels.TopFilter_Options>
                {
                    new ReactModels.TopFilter_Options
                    {
                        Link = new ReactModels.Link
                        {
                            Text = GetLabel("StatusFilterCurrent"),
                            Url = $"?{QueryParameters.TimeFrame}={((int) TimeFrameFilter.Future).ToString()}"
                        },
                        IsCurrent = timeframe == TimeFrameFilter.Future
                    },
                    new ReactModels.TopFilter_Options
                    {
                        Link = new ReactModels.Link
                        {
                            Text = GetLabel("StatusFilterCompleted"),
                            Url = $"?{QueryParameters.TimeFrame}={((int) TimeFrameFilter.Past).ToString()}"
                        },
                        IsCurrent = timeframe == TimeFrameFilter.Past
                    },
                    new ReactModels.TopFilter_Options
                    {
                        Link = new ReactModels.Link
                        {
                            Text = GetLabel("StatusFilterResult"),
                            Url = $"?{QueryParameters.TimeFrame}={((int) TimeFrameFilter.Result).ToString()}"
                        },
                        IsCurrent = timeframe == TimeFrameFilter.Result
                    }
                }
            };

        private ReactModels.TopFilter_Timeframe GetStatus(TimeFrameFilter timeframe) =>
            new ReactModels.TopFilter_Timeframe
            {
                Id = QueryParameters.TimeFrame,
                Value = (int)timeframe,
                Name = QueryParameters.TimeFrame
            };

        private ReactModels.Form BuildForm(IContent page) =>
            new ReactModels.Form { Endpoint = _urlResolver.GetUrl(page) };

        private string GetLabel(string key) => _localizationProvider.GetLabel(nameof(ProposalListPage), key);
    }
}