﻿using System.Collections.Generic;
using System.Linq;
using EPiServer;
using EPiServer.Core;
using EPiServer.Web.Routing;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Configuration;
using Forskningsradet.Core.Extensions;
using Forskningsradet.Core.Facades.Contracts;
using Forskningsradet.Core.Models.ContentModels.Media;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.ContentModels.PropertyList;
using Forskningsradet.Core.Models.QueryParameters;
using Forskningsradet.Core.Models.RequestModels;
using Forskningsradet.Core.Models.ViewModels.Contracts;
using Forskningsradet.Core.Models.ViewModels.Pages;
using Forskningsradet.Core.Services;
using Forskningsradet.Core.Services.Contracts;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.ViewModelMappers.Pages
{
    public class ContentAreaPageViewModelMapper : BasePageViewModelMapper<ContentAreaPage>
    {
        private readonly IContentAreaReactModelBuilder _contentAreaReactModelBuilder;
        private readonly IFluidImageReactModelBuilder _fluidImageReactModelBuilder;
        private readonly IContentLoader _contentLoader;
        private readonly IContentListService _contentListService;
        private readonly IPageEditingAdapter _pageEditingAdapter;
        private readonly IGoogleMapsConfiguration _googleMapsConfiguration;
        private readonly ITagLinkListReactModelBuilder _tagLinkListReactModelBuilder;
        private readonly IRichTextReactModelBuilder _richTextReactModelBuilder;
        private readonly ILocalizationProvider _localizationProvider;
        private readonly IUrlResolver _urlResolver;
        private readonly IUrlCheckingService _urlCheckingService;

        public ContentAreaPageViewModelMapper(
            IContentAreaReactModelBuilder contentAreaReactModelBuilder,
            IFluidImageReactModelBuilder fluidImageReactModelBuilder,
            IContentLoader contentLoader,
            IContentListService contentListService,
            IPageEditingAdapter pageEditingAdapter,
            IGoogleMapsConfiguration googleMapsConfiguration,
            ITagLinkListReactModelBuilder tagLinkListReactModelBuilder,
            IRichTextReactModelBuilder richTextReactModelBuilder,
            ILocalizationProvider localizationProvider,
            IUrlResolver urlResolver,
            IUrlCheckingService urlCheckingService)
        {
            _contentAreaReactModelBuilder = contentAreaReactModelBuilder;
            _fluidImageReactModelBuilder = fluidImageReactModelBuilder;
            _contentLoader = contentLoader;
            _contentListService = contentListService;
            _pageEditingAdapter = pageEditingAdapter;
            _googleMapsConfiguration = googleMapsConfiguration;
            _tagLinkListReactModelBuilder = tagLinkListReactModelBuilder;
            _richTextReactModelBuilder = richTextReactModelBuilder;
            _localizationProvider = localizationProvider;
            _urlResolver = urlResolver;
            _urlCheckingService = urlCheckingService;
        }

        protected override IReactViewModel GetViewModelForPageInternal(ContentAreaPage contentAreaPage) =>
            new ReactPageViewModel(contentAreaPage, BuildLandingPageReactModel(contentAreaPage));

        private ReactModels.ContentAreaPage BuildLandingPageReactModel(ContentAreaPage page)
            => new ReactModels.ContentAreaPage
            {
                PageHeader = BuildPageHeader(page),
                Content = _contentAreaReactModelBuilder.BuildReactModel(page.MainContent, nameof(page.MainContent),
                                                                        new RenderOptionQueryParameter { RenderMediaBlockOption = RenderMediaBlockOption.MediaBlock })
            };

        private IReadOnlyList<ContentAreaPage> GetCategoryHomePages() =>
            _contentListService.Search<ContentAreaPage>(new ContentListRequestModel { OnlyGetCategoryHomePages = true, Take = SearchConstants.MaxPageSize })
                ?.Results
                .OfType<ContentAreaPage>()
                .ToList() ?? new List<ContentAreaPage>();

        private ReactModels.PageHeader BuildPageHeader(ContentAreaPage page) =>
            page.Markers != null && page.Markers.Any()
                ? BuildMapPageHeader(page)
                : BuildImagePageHeader(page);

        private ReactModels.PageHeader BuildImagePageHeader(ContentAreaPage page)
        {
            var imageFile = GetImageFile(page.Image);
            return new ReactModels.PageHeader
            {
                Title = page.TitleWithCustomSeparator ?? page.Title ?? page.PageName,
                Image = imageFile is null && !_pageEditingAdapter.PageIsInEditMode()
                    ? null
                    : _fluidImageReactModelBuilder.BuildReactModel(imageFile, nameof(page.Image)),
                Tags = BuildTagList(page),
                Ingress = page.MainIntro,
                OnPageEditing = _pageEditingAdapter.PageIsInEditMode()
                    ? BuildOnPageEditing(page)
                    : null
            };
        }

        private ReactModels.TagLinkList BuildTagList(ContentAreaPage content) =>
            content.IsCategoryHomePage
                ? _tagLinkListReactModelBuilder.BuildReactModel(GetCategoryList(content), content.HomePageForCategories, GetCategoryHomePages(), false, false)
                : content.ShowCategories
                    ? _tagLinkListReactModelBuilder.BuildReactModel(content.Category, null, GetCategoryHomePages(), false, false)
                    : null;

        private static CategoryList GetCategoryList(ContentAreaPage page) =>
            page.CategoryListBlock?.CategoryList ?? page.HomePageForCategories;

        private ReactModels.PageHeader BuildMapPageHeader(ContentAreaPage page) =>
            new ReactModels.PageHeader
            {
                Title = page.Title ?? page.PageName,
                Image = null,
                Ingress = page.MainIntro,
                OnPageEditing = _pageEditingAdapter.PageIsInEditMode()
                    ? BuildOnPageEditing(page)
                    : null,
                Map = BuildMap(page.Markers)
            };

        private ReactModels.PageHeader_OnPageEditing BuildOnPageEditing(ContentAreaPage page) =>
            new ReactModels.PageHeader_OnPageEditing
            {
                Title = nameof(page.Title),
                Ingress = nameof(page.MainIntro)
            };

        private ReactModels.Map BuildMap(IEnumerable<MapMarker> markers) =>
            new ReactModels.Map
            {
                Markers = markers.Select(MapMarkersToReactModels).ToList(),
                GoogleMapsAPIKey = _googleMapsConfiguration.GoogleMapsApiKey,
                Labels = BuildMapLabels()
            };

        private ReactModels.Map_Labels BuildMapLabels() =>
            new ReactModels.Map_Labels
            {
                ZoomIn = _localizationProvider.GetLabel(nameof(ContentAreaPage), "ZoomIn"),
                ZoomOut = _localizationProvider.GetLabel(nameof(ContentAreaPage), "ZoomOut")
            };

        private ReactModels.Map_Markers MapMarkersToReactModels(MapMarker marker) =>
            new ReactModels.Map_Markers
            {
                Name = marker.Name,
                Latitude = marker.Latitude,
                Longitude = marker.Longitude,
                Popup = PopupPropertiesArePresent(marker)
                    ? BuildPopUp(marker)
                    : null
            };

        private static bool PopupPropertiesArePresent(MapMarker marker) =>
            marker.Address != null || marker.Link != null || !string.IsNullOrWhiteSpace(marker.PopUpTitle);

        private ReactModels.MapPopup BuildPopUp(MapMarker marker) =>
            new ReactModels.MapPopup
            {
                Title = marker.PopUpTitle,
                Link = marker.Link is null ? null : BuildLink(marker.Link, marker.LinkText),
                Address = marker.Address is null ? null : _richTextReactModelBuilder.BuildReactModel(marker.Address, nameof(marker.Address))
            };

        private ReactModels.Link BuildLink(Url markerUrl, string markerText) =>
            new ReactModels.Link
            {
                Text = !string.IsNullOrWhiteSpace(markerText)
                    ? markerText
                    : markerUrl.ToString(),
                Url = _urlResolver.GetUrl(markerUrl.ToString()),
                IsExternal = markerUrl.IsExternal(_urlCheckingService)
            };

        private ImageFile GetImageFile(ContentReference reference) =>
            reference is null ? null : _contentLoader.Get<ImageFile>(reference);
    }
}
