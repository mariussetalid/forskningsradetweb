﻿using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.ViewModels.Contracts;
using Forskningsradet.Core.Models.ViewModels.Pages;
using Forskningsradet.Core.Services;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.ViewModelMappers.Pages
{
    public class ApsisNewsletterRegistrationPageViewModelMapper : BasePageViewModelMapper<ApsisNewsletterRegistrationPage>
    {
        private readonly IRichTextReactModelBuilder _richTextReactModelBuilder;
        private readonly IContentAreaReactModelBuilder _contentAreaReactModelBuilder;
        private readonly ILocalizationProvider _localizationProvider;
        private readonly IOptionsModalReactModelBuilder _optionsModalReactModelBuilder;

        public ApsisNewsletterRegistrationPageViewModelMapper(
            IRichTextReactModelBuilder richTextReactModelBuilder,
            IContentAreaReactModelBuilder contentAreaReactModelBuilder,
            ILocalizationProvider localizationProvider,
            IOptionsModalReactModelBuilder optionsModalReactModelBuilder)
        {
            _richTextReactModelBuilder = richTextReactModelBuilder;
            _contentAreaReactModelBuilder = contentAreaReactModelBuilder;
            _localizationProvider = localizationProvider;
            _optionsModalReactModelBuilder = optionsModalReactModelBuilder;
        }

        protected override IReactViewModel GetViewModelForPageInternal(ApsisNewsletterRegistrationPage currentPage) =>
            new ReactPageViewModel(currentPage, BuildReactModel(currentPage));

        private ReactModels.ResponsiveIframePage BuildReactModel(ApsisNewsletterRegistrationPage currentPage) =>
            new ReactModels.ResponsiveIframePage
            {
                Title = currentPage.Title ?? currentPage.PageName,
                RichText = _richTextReactModelBuilder.BuildReactModel(currentPage.MainIntro, nameof(currentPage.MainIntro)),
                Sidebar = _contentAreaReactModelBuilder.BuildReactModel(currentPage.RightBlockArea, nameof(currentPage.RightBlockArea)),
                Iframe = BuildResponsiveIframe(currentPage),
                Footer = BuildFooter(currentPage)
            };

        private ReactModels.ResponsiveIframe BuildResponsiveIframe(ApsisNewsletterRegistrationPage page) =>
            new ReactModels.ResponsiveIframe
            {
                Title = GetLabel("RegistrationTitle"),
                Src = "form"
            };

        private ReactModels.PageFooter BuildFooter(ApsisNewsletterRegistrationPage currentPage) =>
            new ReactModels.PageFooter
            {
                Share = _optionsModalReactModelBuilder.BuildShareContent(currentPage)
            };

        private string GetLabel(string key) =>
            _localizationProvider.GetLabel(nameof(ApsisNewsletterRegistrationPage), key);
    }
}