using System;
using System.Collections.Generic;
using System.Linq;
using EPiServer.Web.Routing;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Configuration;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.QueryParameters;
using Forskningsradet.Core.Models.SearchModels;
using Forskningsradet.Core.Models.ViewModels.Contracts;
using Forskningsradet.Core.Models.ViewModels.Pages;
using Forskningsradet.Core.Services;
using Forskningsradet.Core.Services.Contracts;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.ViewModelMappers.Pages
{
    public class EmployeeListPageViewModelMapper : BasePageViewModelMapperWithQuery<EmployeeListPage>
    {
        private readonly IContentAreaReactModelBuilder _contentAreaReactModelBuilder;
        private readonly IEmployeeService _employeeService;
        private readonly IUrlResolver _urlResolver;
        private readonly ILocalizationProvider _localizationProvider;
        private readonly IPaginationConfiguration _paginationConfiguration;

        public EmployeeListPageViewModelMapper(IContentAreaReactModelBuilder contentAreaReactModelBuilder,
            IEmployeeService employeeService,
            IUrlResolver urlResolver,
            ILocalizationProvider localizationProvider,
            IPaginationConfiguration paginationConfiguration)
        {
            _contentAreaReactModelBuilder = contentAreaReactModelBuilder;
            _employeeService = employeeService;
            _urlResolver = urlResolver;
            _localizationProvider = localizationProvider;
            _paginationConfiguration = paginationConfiguration;
        }

        protected override IReactViewModel GetViewModelForPageInternal(EmployeeListPage employeeListPage, QueryParameterBase parameters)
            => new ReactPageViewModel(employeeListPage, BuildEmployeeListPageReactModel(employeeListPage, parameters as EmployeeListQueryParameter));

        private ReactModels.ReactComponent BuildEmployeeListPageReactModel(EmployeeListPage page, EmployeeListQueryParameter parameters)
        {
            var listPageUrl = _urlResolver.GetUrl(page);
            var currentPage = parameters.Page ?? 1;
            var filterByLetter = parameters.Letter.HasValue;
            var pageSize = filterByLetter
                ? SearchConstants.MaxPageSize
                : _paginationConfiguration.EmployeeListPaginationSize ?? SearchConstants.DefaultPageSize;

            var results = _employeeService.Search(parameters.Query, parameters.Letter, currentPage, pageSize);
            var letters = _employeeService.GetAllLetters().ToList();
            var visibleLetters = filterByLetter
                ? letters
                : results.Letters;

            return new ReactModels.GroupedSearchPage
            {
                Title = page.PageName,
                Results = BuildResults(results.Results),
                Navigation = BuildLetters(letters, visibleLetters, listPageUrl, parameters.Query),
                ResultsDescription = BuildResultDescription(currentPage, results.TotalMatching, pageSize, filterByLetter),
                Form = BuildForm(listPageUrl),
                Search = BuildSearch(parameters.Query),
                Pagination = BuildPagination(parameters, results.TotalMatching, currentPage, pageSize, listPageUrl),
                FilterLayout = BuildFilterLayout(page),
                EmptyList = results.TotalMatching == 0 ? BuildEmptyList() : null
            };
        }

        private string BuildResultDescription(int currentPage, int totalPages, int pageSize, bool filterByLetter)
            => totalPages > 0
                ? filterByLetter
                    ? GetLabel("ResultDescriptionLetterSearch")
                    : string.Format(
                        GetLabel("ResultDescription"),
                        (currentPage - 1) * pageSize + 1,
                        Math.Min(currentPage * pageSize, totalPages),
                        totalPages)
                : null;

        private ReactModels.Pagination BuildPagination(EmployeeListQueryParameter parameters, int totalPages, int currentPage, int pageSize, string listPageUrl)
        {
            if (parameters.Letter.HasValue || totalPages == 0)
                return null;

            var pageCount = (totalPages + pageSize - 1) / pageSize;
            var pagination = new ReactModels.Pagination
            {
                Pages = new List<ReactModels.Pagination_Pages>(),
                Title = GetLabel("PaginationTitle")
            };

            if (currentPage != 1 && pageCount > 1)
            {
                pagination.Pages.Add(BuildPaginationNavigationLink(listPageUrl, GetLabel("PaginationPrevious"),
                    GetLabel("PaginationPreviousPage"), parameters.Query, currentPage - 1));
            }

            for (var i = 1; i <= pageCount; i++)
            {
                pagination.Pages.Add(new ReactModels.Pagination_Pages
                {
                    IsCurrent = i == currentPage,
                    Label = string.Format(GetLabel("PaginationPage"), i),
                    Link = new ReactModels.Link
                    {
                        Text = i.ToString(),
                        Url = BuildPaginationUrl(listPageUrl, i, parameters.Query)
                    }
                });
            }

            if (currentPage != pageCount && pageCount > 1)
            {
                pagination.Pages.Add(BuildPaginationNavigationLink(listPageUrl, GetLabel("PaginationNext"),
                    GetLabel("PaginationNextPage"), parameters.Query, currentPage + 1));
            }

            return pagination;
        }

        private static ReactModels.Pagination_Pages BuildPaginationNavigationLink(string listPageUrl, string linkText, string label, string query, int pageIndex)
            => new ReactModels.Pagination_Pages
            {
                IsCurrent = false,
                Label = label,
                Link = new ReactModels.Link
                {
                    Text = linkText,
                    Url = BuildPaginationUrl(listPageUrl, pageIndex, query)
                }
            };

        private static string BuildPaginationUrl(string listPageUrl, int pageIndex, string query)
            => !string.IsNullOrEmpty(query)
                ? $"{listPageUrl}?{QueryParameters.SearchQuery}={query}&{QueryParameters.Page}={pageIndex}"
                : $"{listPageUrl}?{QueryParameters.Page}={pageIndex}";

        private IList<ReactModels.SearchResultGroup> BuildResults(IEnumerable<EmployeeSearchHit> employees)
            => employees.GroupBy(e => e.Letter,
                (key, employee) =>
                    new ReactModels.SearchResultGroup
                    {
                        Title = key,
                        Results = BuildContentArea(employee)
                    }).ToList();

        private ReactModels.ContentArea BuildContentArea(IEnumerable<EmployeeSearchHit> employees)
            => new ReactModels.ContentArea
            {
                Blocks = employees.Select(BuildContentAreaItem).ToList()
            };

        private ReactModels.ContentAreaItem BuildContentAreaItem(EmployeeSearchHit employee, int index)
            => new ReactModels.ContentAreaItem
            {
                Id = index.ToString(),
                ComponentName = nameof(ReactModels.EmployeeSearchResult),
                ComponentData = new ReactModels.EmployeeSearchResult
                {
                    Title = employee.Name,
                    SubTitle = employee.JobTitle,
                    Texts = new List<string> { employee.Department },
                    LabeledLinks = new List<ReactModels.EmployeeSearchResult_LabeledLinks>
                    {
                        new ReactModels.EmployeeSearchResult_LabeledLinks
                        {
                            Label = GetLabel("EmailLabel"),
                            Link = new ReactModels.Link
                            {
                                Text = employee.Email,
                                Url = $"mailto:{employee.Email}"
                            }
                        }
                    }
                }
            };

        private static IList<ReactModels.GroupedSearchPageLink> BuildLetters(
            IEnumerable<string> letters,
            IEnumerable<string> visibleLetters, string url, string query)
            => letters
                .Select(x =>
                    new ReactModels.GroupedSearchPageLink
                    {
                        Link = new ReactModels.Link
                        {
                            Text = x,
                            Url = visibleLetters is null || visibleLetters.Any(y => y == x)
                                ? $"{url}?{QueryParameters.SearchQuery}={query}&{QueryParameters.Letter}={x}"
                                : null
                        }
                    }).ToList();

        private static ReactModels.Form BuildForm(string url)
            => new ReactModels.Form { Endpoint = url };

        private ReactModels.FilterLayout BuildFilterLayout(GroupedSearchPage page)
            => new ReactModels.FilterLayout
            {
                ContentArea = _contentAreaReactModelBuilder.BuildReactModel(page.RightBlockArea, nameof(page.RightBlockArea)),
            };

        private ReactModels.Search BuildSearch(string query)
            => new ReactModels.Search
            {
                Input = new ReactModels.SearchInput
                {
                    Name = QueryParameters.SearchQuery,
                    Placeholder = GetLabel("SearchPlaceholder"),
                    Value = query
                },
                Submit = new ReactModels.Button
                {
                    Text = GetLabel("SearchButton")
                }
            };

        private ReactModels.EmptyList BuildEmptyList()
            => new ReactModels.EmptyList { Text = GetLabel("EmptyListLabel") };

        private string GetLabel(string key)
            => _localizationProvider.GetLabel(nameof(EmployeeListPage), key);
    }
}