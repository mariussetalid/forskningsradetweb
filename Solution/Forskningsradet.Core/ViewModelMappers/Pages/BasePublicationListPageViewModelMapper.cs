﻿using System.Collections.Generic;
using System.Linq;
using EPiServer.Web.Routing;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.Enums;
using Forskningsradet.Core.Models.QueryParameters;
using Forskningsradet.Core.Models.RequestModels;
using Forskningsradet.Core.Models.SearchModels;
using Forskningsradet.Core.Models.ViewModels.Contracts;
using Forskningsradet.Core.Models.ViewModels.Pages;
using Forskningsradet.Core.Services;
using Forskningsradet.Core.Services.Contracts;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.ViewModelMappers.Pages
{
    public abstract class BasePublicationListPageViewModelMapper<T> : BasePageViewModelMapperWithQuery<T>
        where T : PublicationListPage
    {
        protected readonly IPublicationListService _publicationsService;
        protected readonly IPublicationReactModelBuilder _publicationReactModelBuilder;
        protected readonly ILocalizationProvider _localizationProvider;
        protected readonly IUrlResolver _urlResolver;

        protected BasePublicationListPageViewModelMapper(IPublicationListService publicationsService, IPublicationReactModelBuilder publicationReactModelBuilder, ILocalizationProvider localizationProvider, IUrlResolver urlResolver)
        {
            _publicationsService = publicationsService;
            _publicationReactModelBuilder = publicationReactModelBuilder;
            _localizationProvider = localizationProvider;
            _urlResolver = urlResolver;
        }

        protected override IReactViewModel GetViewModelForPageInternal(T page, QueryParameterBase parameters)
        {
            var queryParameters = parameters as PublicationListQueryParameter;
            var viewModel = BuildViewModel(page, queryParameters);
            return new ReactPageViewModel(page, viewModel);
        }

        protected PublicationRequest MapQueryParameterToRequestModel(PublicationListQueryParameter queryParameter, T currentPage, int pageSize = SearchConstants.MaxPageSize) =>
            new PublicationRequest(
                queryParameter.Query,
                currentPage.PageRoot?.ID ?? currentPage.ContentLink.ID,
                queryParameter.Year,
                queryParameter.Types,
                queryParameter.Page,
                pageSize
                );

        protected static string BuildCurrentQueryString(PublicationRequest request) =>
            string.Join("&", BuildQueryParameters(request));

        protected static IEnumerable<string> BuildQueryParameters(PublicationRequest request, int? customYear = null)
        {
            if (request.Query != null)
                yield return $"{QueryParameters.SearchQuery}={request.Query}";

            var year = customYear ?? request.Year;
            if (year > 0)
                yield return $"{QueryParameters.Year}={year}";

            if (request.Types != null)
            {
                foreach (var type in request.Types)
                {
                    yield return $"{QueryParameters.ResultType}={type}";
                }
            }
        }

        protected IList<ReactModels.GroupedSearchPageLink> BuildNavigation(IList<FilterOption> years, List<ReactModels.SearchResultGroup> resultGroups, string listPageUrl, PublicationRequest request)
        {
            var emptyQuery = request.Query is null || string.IsNullOrEmpty(request.Query.Trim());
            var emptyType = request.Types is null || request.Types.Length == 0;
            var validYear = request.Year > 0 && years.Any(x => x.Value == request.Year.ToString());
            var currentYear = validYear
                ? request.Year.ToString()
                : request.Year <= 0
                    ? years.FirstOrDefault()?.Value ?? string.Empty
                    : string.Empty;

            if (validYear || (emptyQuery && emptyType))
            {
                return years.Select(x =>
                        new ReactModels.GroupedSearchPageLink
                        {
                            IsCurrent = x.Value == currentYear.ToString(),
                            Link = new ReactModels.Link
                            {
                                Text = x.Name,
                                Url = BuildNavigationUrl(listPageUrl, int.Parse(x.Value), request)
                            }
                        })
                    .ToList();
            }

            return years.Select(x =>
                    new ReactModels.GroupedSearchPageLink
                    {
                        IsCurrent = false,
                        Link = new ReactModels.Link
                        {
                            Text = x.Name,
                            Url = request.WithPaging || resultGroups.Any(y => y.Title == x.Name)
                                ? BuildNavigationUrl(listPageUrl, int.Parse(x.Value), request)
                                : null
                        }
                    })
                .ToList();
        }

        protected IEnumerable<ReactModels.SearchResultGroup> BuildResultGroups(List<PublicationBasePage> publications)
        {
            var groupedPublications = publications
                .GroupBy(x => x.Year)
                .ToList();
            foreach (var group in groupedPublications.OrderByDescending(x => x.Key))
            {
                var year = group.Key;
                var sortedPublications = SortPublications(group.ToList());
                yield return new ReactModels.SearchResultGroup
                {
                    Title = year.ToString(),
                    HtmlId = year.ToString(),
                    Results = BuildContentAreaComponent(sortedPublications)
                };
            }
        }

        protected ReactModels.ContentArea BuildContentAreaComponent(List<PublicationBasePage> publications) =>
             new ReactModels.ContentArea
             {
                 Blocks = publications
                    .Select(BuildItem)
                    .ToList()
             };

        protected ReactModels.Search BuildSearch(string query) =>
            new ReactModels.Search
            {
                Input = new ReactModels.TextInput
                {
                    Name = QueryParameters.SearchQuery,
                    Label = QueryParameters.SearchQuery,
                    Placeholder = GetLabel("Search"),
                    Value = query
                },
                Submit = new ReactModels.Button
                {
                    Text = GetLabel("Search"),
                }
            };

        protected ReactModels.EmptyList BuildEmptyListElement(int totalMatching) =>
            totalMatching == 0
                ? new ReactModels.EmptyList
                {
                    Text = GetLabel("EmptySearch")
                }
                : null;

        protected ReactModels.FilterLayout BuildFilterLayout(T currentPage, PublicationRequest request, PublicationListSearchResults results) =>
            new ReactModels.FilterLayout
            {
                Filters = BuildFilters(currentPage, request, results),
            };

        protected abstract ReactModels.Filters BuildFilters(T currentPage, PublicationRequest request, PublicationListSearchResults results);

        protected abstract List<PublicationBasePage> SortPublications(List<PublicationBasePage> publications);

        protected string GetLabel(string key) =>
            _localizationProvider.GetLabel(nameof(PublicationListPage), key);

        private ReactModels.ReactComponent BuildViewModel(T currentPage, PublicationListQueryParameter queryParameters)
        {
            var searchRequestModel = MapQueryParameterToRequestModel(queryParameters, currentPage);
            var results = _publicationsService.Search(searchRequestModel);
            var allYears = results.Years.ToList();
            var searchResultGroups = BuildResultGroups(results.Results.ToList()).ToList();
            var listPageUrl = _urlResolver.GetUrl(currentPage.ContentLink);

            return new ReactModels.PublicationsPage
            {
                Title = currentPage.PageName,
                EmptyList = BuildEmptyListElement(results.TotalMatching),
                Form = new ReactModels.Form
                {
                    Endpoint = listPageUrl,
                    SubmitButtonText = GetLabel("SubmitButtonText")
                },
                Results = searchResultGroups,
                FilterLayout = BuildFilterLayout(currentPage, searchRequestModel, results),
                Search = BuildSearch(queryParameters.Query),
                Navigation = BuildNavigation(
                    allYears,
                    searchResultGroups,
                    listPageUrl,
                    searchRequestModel)
            };
        }

        private static string BuildNavigationUrl(string listPageUrl, int year, PublicationRequest currentRequest) =>
            listPageUrl + "?" + string.Join("&", BuildQueryParameters(currentRequest, year));

        private ReactModels.ContentAreaItem BuildItem(PublicationBasePage publication, int index) =>
            new ReactModels.ContentAreaItem
            {
                Id = index.ToString(),
                ComponentName = nameof(ReactModels.SearchResult),
                ComponentData = _publicationReactModelBuilder.BuildReactModel(publication, PublicationMetadata.Standard, false)
            };
    }
}