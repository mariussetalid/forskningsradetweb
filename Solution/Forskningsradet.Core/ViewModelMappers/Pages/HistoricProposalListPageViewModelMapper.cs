﻿using System.Collections.Generic;
using System.Linq;
using EPiServer.Web.Routing;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.QueryParameters;
using Forskningsradet.Core.Models.RequestModels;
using Forskningsradet.Core.Models.SearchModels;
using Forskningsradet.Core.Models.ViewModels.Contracts;
using Forskningsradet.Core.Models.ViewModels.Pages;
using Forskningsradet.Core.Services;
using Forskningsradet.Core.Services.Contracts;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.ViewModelMappers.Pages
{
    public class HistoricProposalListPageViewModelMapper : BasePublicationListPageViewModelMapper<HistoricProposalListPage>
    {
        public HistoricProposalListPageViewModelMapper(
            IPublicationListService publicationsService,
            IPublicationReactModelBuilder publicationReactModelBuilder,
            ILocalizationProvider localizationProvider,
            IUrlResolver urlResolver)
            : base(publicationsService, publicationReactModelBuilder, localizationProvider, urlResolver)
        {
        }

        protected override IReactViewModel GetViewModelForPageInternal(HistoricProposalListPage page, QueryParameterBase parameters)
        {
            var queryParameters = parameters as PublicationListQueryParameter;
            var viewModel = BuildViewModel(page, queryParameters);
            return new ReactPageViewModel(page, viewModel);
        }

        protected override ReactModels.Filters BuildFilters(HistoricProposalListPage currentPage, PublicationRequest request, PublicationListSearchResults results) =>
            new ReactModels.Filters();

        protected override List<PublicationBasePage> SortPublications(List<PublicationBasePage> publications) =>
            publications.OrderByDescending(x => x.StartPublish).ToList();

        private ReactModels.ReactComponent BuildViewModel(HistoricProposalListPage currentPage, PublicationListQueryParameter queryParameters)
        {
            var searchRequestModel = MapQueryParameterToRequestModel(queryParameters, currentPage);
            var results = _publicationsService.Search(searchRequestModel);
            var allYears = results.Years.ToList();
            var searchResultGroups = BuildResultGroups(results.Results.ToList()).ToList();
            var listPageUrl = _urlResolver.GetUrl(currentPage.ContentLink);

            return new ReactModels.PublicationsPage
            {
                Title = currentPage.PageName,
                EmptyList = BuildEmptyListElement(results.TotalMatching),
                Form = new ReactModels.Form
                {
                    Endpoint = listPageUrl,
                    SubmitButtonText = GetLabel("SubmitButtonText")
                },
                Results = searchResultGroups,
                Search = BuildSearch(queryParameters.Query),
                Navigation = BuildNavigation(
                    allYears,
                    searchResultGroups,
                    listPageUrl,
                    searchRequestModel)
            };
        }

        private HistoricProposalRequest MapQueryParameterToRequestModel(PublicationListQueryParameter queryParameter, PublicationListPage currentPage) =>
            new HistoricProposalRequest(
                queryParameter.Query,
                currentPage.PageRoot?.ID ?? currentPage.ContentLink.ID,
                queryParameter.Year,
                queryParameter.Types);

        private IList<ReactModels.GroupedSearchPageLink> BuildNavigation(IList<FilterOption> years, List<ReactModels.SearchResultGroup> resultGroups, string listPageUrl, HistoricProposalRequest request) =>
            BuildNavigation(years, resultGroups, listPageUrl, new PublicationRequest(request.Query, request.PageRoot, request.Year, request.Types));
    }
}
