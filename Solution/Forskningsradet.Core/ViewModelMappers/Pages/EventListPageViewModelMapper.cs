﻿using System.Collections.Generic;
using System.Linq;
using EPiServer.Core;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Builders;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.QueryParameters;
using Forskningsradet.Core.Models.RequestModels;
using Forskningsradet.Core.Models.SearchModels;
using Forskningsradet.Core.Models.ViewModels.Contracts;
using Forskningsradet.Core.Models.ViewModels.Pages;
using Forskningsradet.Core.Services;
using Forskningsradet.Core.Services.Contracts;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.ViewModelMappers.Pages
{
    public class EventListPageViewModelMapper : BasePageViewModelMapperWithQuery<EventListPage>
    {
        private readonly IEventListService _eventListService;
        private readonly FilterLayoutReactModelBuilder _filterLayoutReactModelBuilder;
        private readonly IDateCardReactModelBuilder _dateCardReactModelBuilder;
        private readonly ILocalizationProvider _localizationProvider;
        private readonly ISearchApiUrlResolver _searchApiUrlResolver;

        public EventListPageViewModelMapper(IEventListService eventListService, FilterLayoutReactModelBuilder filterLayoutReactModelBuilder, IDateCardReactModelBuilder dateCardReactModelBuilder, ILocalizationProvider localizationProvider, ISearchApiUrlResolver searchApiUrlResolver)
        {
            _eventListService = eventListService;
            _filterLayoutReactModelBuilder = filterLayoutReactModelBuilder;
            _dateCardReactModelBuilder = dateCardReactModelBuilder;
            _localizationProvider = localizationProvider;
            _searchApiUrlResolver = searchApiUrlResolver;
        }

        protected override IReactViewModel GetViewModelForPageInternal(EventListPage currentPage, QueryParameterBase parameters) =>
            new ReactPageViewModel(currentPage, BuildReactModel(currentPage, parameters as EventListQueryParameter ?? new EventListQueryParameter()));

        private ReactModels.EventListPage BuildReactModel(EventListPage currentPage, EventListQueryParameter parameters)
        {
            var result = _eventListService.Search(BuildEventListRequestModel(currentPage, parameters));
            var events = result.Results.Select(x => _dateCardReactModelBuilder.BuildReactModel(x, skipTags: currentPage.HideEventTags));

            return new ReactModels.EventListPage
            {
                Title = currentPage.PageName,
                EmptyList = BuildEmptyListElement(),
                Events = events.ToList(),
                TopFilter = BuildTopFilter(parameters.TimeFrame),
                FilterLayout = BuildFilterLayout(parameters, result.Categories, currentPage.FilterTitle, result.TotalMatching),
                Form = BuildForm(),
                FetchFilteredResultsEndpoint = _searchApiUrlResolver.GetSearchEventUrl(),
            };
        }

        private static EventListRequestModel BuildEventListRequestModel(EventListPage page, EventListQueryParameter parameters) =>
            new EventListRequestModel
            (
                parameters.TimeFrame,
                page.Category?.ToArray() ?? new int[0],
                (parameters.Categories ?? new int[0]).Concat(parameters.Subjects ?? new int[0]).Concat(parameters.TargetGroups ?? new int[0]).ToArray(),
                page.FilterCategoryList?.ToArray() ?? new int[0],
                parameters.Type,
                parameters.Location,
                parameters.VideoOnly,
                GetPageRoot(page),
                0,
                page.FilterCategoryList?.Count > 0);

        private static int GetPageRoot(EventListPage page) =>
            page.PageRoot?.ID > 0
                ? page.PageRoot.ID
                : ContentReference.StartPage?.ID ?? 0;

        private ReactModels.EmptyList BuildEmptyListElement() =>
            new ReactModels.EmptyList
            {
                Text = GetLabel("EmptyEventList")
            };

        private ReactModels.FilterLayout BuildFilterLayout(EventListQueryParameter parameters, IEnumerable<CategoryGroup> categories, string filterTitle, int resultsCount) =>
            _filterLayoutReactModelBuilder.BuildReactModel(new FilterLayoutModel(parameters, categories.ToList(), filterTitle, resultsCount));

        private ReactModels.Form BuildForm() =>
            new ReactModels.Form
            {
                Endpoint = "",
                SubmitButtonText = GetLabel("SubmitButtonText")
            };

        private ReactModels.TopFilter BuildTopFilter(TimeFrameFilter timeFrame) =>
            new ReactModels.TopFilter
            {
                Title = GetLabel("TopFilterTitle"),
                Timeframe = BuildTimeFrame(timeFrame),
                Options = new List<ReactModels.TopFilter_Options>
                {
                    new ReactModels.TopFilter_Options
                    {
                        Link = new ReactModels.Link
                        {
                            Text = GetLabel("OptionFuture"),
                            Url = $"?{QueryParameters.TimeFrame}={((int) TimeFrameFilter.Future).ToString()}"
                        },
                        IsCurrent = timeFrame == TimeFrameFilter.Future
                    },
                    new ReactModels.TopFilter_Options
                    {
                        Link = new ReactModels.Link
                        {
                            Text = GetLabel("OptionPast"),
                            Url = $"?{QueryParameters.TimeFrame}={((int) TimeFrameFilter.Past).ToString()}"
                        },
                        IsCurrent = timeFrame == TimeFrameFilter.Past
                    }
                }
            };

        private ReactModels.TopFilter_Timeframe BuildTimeFrame(TimeFrameFilter timeFrame) =>
            new ReactModels.TopFilter_Timeframe
            {
                Id = QueryParameters.TimeFrame,
                Value = (int)timeFrame,
                Name = QueryParameters.TimeFrame
            };

        private string GetLabel(string key) => _localizationProvider.GetLabel(nameof(EventListPage), key);
    }
}