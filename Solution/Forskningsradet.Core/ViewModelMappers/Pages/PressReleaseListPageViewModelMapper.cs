using System.Collections.Generic;
using System.Linq;
using EPiServer.Core;
using EPiServer.Web.Routing;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Extensions;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.QueryParameters;
using Forskningsradet.Core.Models.RequestModels;
using Forskningsradet.Core.Models.ViewModels.Contracts;
using Forskningsradet.Core.Models.ViewModels.Pages;
using Forskningsradet.Core.Services;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.ViewModelMappers.Pages
{
    public class PressReleaseListPageViewModelMapper : BasePageViewModelMapperWithQuery<PressReleaseListPage>
    {
        private readonly IContentListService _contentListService;
        private readonly IPaginationReactModelBuilder _paginationReactModelBuilder;
        private readonly IContentAreaReactModelBuilder _contentAreaReactModelBuilder;
        private readonly IOptionsModalReactModelBuilder _optionsModalReactModelBuilder;
        private readonly IFluidImageReactModelBuilder _fluidImageReactModelBuilder;
        private readonly IUrlResolver _urlResolver;
        private readonly ILocalizationProvider _localizationProvider;

        public PressReleaseListPageViewModelMapper(
            IContentListService contentListService,
            IPaginationReactModelBuilder paginationReactModelBuilder,
            IContentAreaReactModelBuilder contentAreaReactModelBuilder,
            IOptionsModalReactModelBuilder optionsModalReactModelBuilder,
            IFluidImageReactModelBuilder fluidImageReactModelBuilder,
            IUrlResolver urlResolver,
            ILocalizationProvider localizationProvider)
        {
            _contentListService = contentListService;
            _paginationReactModelBuilder = paginationReactModelBuilder;
            _contentAreaReactModelBuilder = contentAreaReactModelBuilder;
            _optionsModalReactModelBuilder = optionsModalReactModelBuilder;
            _fluidImageReactModelBuilder = fluidImageReactModelBuilder;
            _urlResolver = urlResolver;
            _localizationProvider = localizationProvider;
        }

        protected override IReactViewModel GetViewModelForPageInternal(PressReleaseListPage currentPage, QueryParameterBase parameters)
        {
            var viewModel = BuildViewModel(currentPage, parameters as PressReleaseQueryParameter);
            return new ReactPageViewModel(currentPage, viewModel);
        }

        private ReactModels.ArticlePage BuildViewModel(PressReleaseListPage currentPage, PressReleaseQueryParameter parameters)
        {
            var result = _contentListService.Search<PressReleasePage>(BuildRequestModel(currentPage, parameters));
            var pressReleasePages = result
                .Results
                .OfType<PressReleasePage>()
                .ToList();

            return new ReactModels.ArticlePage
            {
                Title = currentPage.PageName,
                Ingress = pressReleasePages.Count == 0
                    ? GetLabel("NotAvailable")
                    : null,
                Sidebar = _contentAreaReactModelBuilder.BuildReactModel(
                    currentPage.RightBlockArea,
                    nameof(currentPage.RightBlockArea)),
                Content = new ReactModels.RichText
                {
                    Blocks = BuildPressReleaseList(currentPage, pressReleasePages, parameters, result.TotalMatching).ToList()
                }
            };
        }

        private static ContentListRequestModel BuildRequestModel(PressReleaseListPage page, PressReleaseQueryParameter parameters) =>
            new ContentListRequestModel
            {
                PageRoot = page.ContentLink.ID,
                Page = parameters.Page,
                Take = ListPageConstants.PressReleases.ItemsPerPage
            };

        private IEnumerable<ReactModels.ContentAreaItem> BuildPressReleaseList(PressReleaseListPage currentPage, List<PressReleasePage> pressReleases, PressReleaseQueryParameter parameters, int totalPages)
        {
            foreach (var pressRelease in pressReleases)
            {
                yield return new ReactModels.ContentAreaItem
                {
                    ComponentName = nameof(ReactModels.RelatedArticles),
                    ComponentData = new ReactModels.RelatedArticles
                    {
                        Articles = new List<ReactModels.ArticleBlock>
                        {
                            new ReactModels.ArticleBlock
                            {
                                Title = new ReactModels.Link
                                {
                                    Text = pressRelease.GetListTitle(),
                                    Url = _urlResolver.GetUrl(pressRelease)
                                },
                                Published = new ReactModels.Published
                                {
                                    Date = pressRelease.ExternalPublishedAt?.ToString(DateTimeFormats.ProposalDeadlineDate),
                                    Type = GetLabel("Published")
                                },
                                Image = GetListImageOrThumbnail(pressRelease),
                                Text = pressRelease.GetListIntroOrMainIntro()
                            }
                        },
                        RelatedContent = new ReactModels.RelatedContent
                        {
                            Border = ReactModels.RelatedContent_Border.Top
                        }
                    }
                };
            }

            yield return new ReactModels.ContentAreaItem
            {
                ComponentName = nameof(ReactModels.Pagination),
                ComponentData = _paginationReactModelBuilder.BuildReactModel(
                    string.Empty,
                    parameters.Page,
                    totalPages,
                    ListPageConstants.PressReleases.ItemsPerPage,
                    _urlResolver.GetUrl(currentPage))
            };
        }

        private ReactModels.FluidImage GetListImageOrThumbnail(PressReleasePage page)
        {
            if (!ContentReference.IsNullOrEmpty(page.ListImage))
                return _fluidImageReactModelBuilder.BuildReactModel(page.ListImage, null);

            return !string.IsNullOrEmpty(page.ImageThumbnailUrl)
                ? new ReactModels.FluidImage
                {
                    Src = page.ImageThumbnailUrl,
                    Alt = page.ImageCaption
                }
                : null;
        }

        private string GetLabel(string key) =>
            _localizationProvider.GetLabel(nameof(PressReleaseListPage), key);
    }
}