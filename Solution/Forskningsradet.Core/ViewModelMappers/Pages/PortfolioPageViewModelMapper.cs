﻿using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Facades.Contracts;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.QueryParameters;
using Forskningsradet.Core.Models.ViewModels.Contracts;
using Forskningsradet.Core.Models.ViewModels.Pages;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.ViewModelMappers.Pages
{
    public class PortfolioPageViewModelMapper : BasePageViewModelMapper<PortfolioPage>
    {
        private readonly ILinkListReactModelBuilder _linkListReactModelBuilder;
        private readonly IContentAreaReactModelBuilder _contentAreaReactModelBuilder;
        private readonly IPageEditingAdapter _pageEditingAdapter;

        public PortfolioPageViewModelMapper(
            ILinkListReactModelBuilder linkListReactModelBuilder,
            IContentAreaReactModelBuilder contentAreaReactModelBuilder,
            IPageEditingAdapter pageEditingAdapter)
        {
            _linkListReactModelBuilder = linkListReactModelBuilder;
            _contentAreaReactModelBuilder = contentAreaReactModelBuilder;
            _pageEditingAdapter = pageEditingAdapter;
        }

        protected override IReactViewModel GetViewModelForPageInternal(PortfolioPage page) =>
            new ReactPageViewModel(page, BuildLandingPageReactModel(page));

        private ReactModels.ContentAreaPage BuildLandingPageReactModel(PortfolioPage page) =>
            new ReactModels.ContentAreaPage
            {
                PageHeader = BuildPageHeader(page),
                Content = _contentAreaReactModelBuilder.BuildReactModel(page.MainContent, nameof(page.MainContent),
                    new RenderOptionQueryParameter { RenderMediaBlockOption = RenderMediaBlockOption.MediaBlock })
            };

        private ReactModels.PageHeader BuildPageHeader(PortfolioPage page) =>
            new ReactModels.PageHeader
            {
                Title = page.Title ?? page.PageName,
                Ingress = page.MainIntro,
                PortfolioLinks = _linkListReactModelBuilder.BuildReactModel(page.LinkItems, null).Items,
                OnPageEditing = _pageEditingAdapter.PageIsInEditMode()
                    ? BuildOnPageEditing(page)
                    : null,
            };

        private ReactModels.PageHeader_OnPageEditing BuildOnPageEditing(PortfolioPage page) =>
            new ReactModels.PageHeader_OnPageEditing
            {
                Title = nameof(page.Title),
                Ingress = nameof(page.MainIntro)
            };
    }
}
