﻿using EPiServer.Core;
using Forskningsradet.Core.Models.QueryParameters;
using Forskningsradet.Core.Models.ViewModels.Contracts;

namespace Forskningsradet.Core.ViewModelMappers.Pages
{
    public abstract class BasePageViewModelMapperWithQuery<T> : IPageViewModelMapperWithQuery
        where T : IContentData
    {
        public IReactViewModel GetPageViewModel(IContentData currentContent, QueryParameterBase parameters)
            => currentContent is T content
                ? GetViewModelForPageInternal(content, parameters)
                : null;

        protected abstract IReactViewModel GetViewModelForPageInternal(T page, QueryParameterBase parameters);
    }
}