﻿using System.Collections.Generic;
using System.Linq;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.Web.Routing;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Builders;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Extensions;
using Forskningsradet.Core.Models.ContentModels.Media;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.QueryParameters;
using Forskningsradet.Core.Models.RequestModels;
using Forskningsradet.Core.Models.SearchModels;
using Forskningsradet.Core.Models.ViewModels.Contracts;
using Forskningsradet.Core.Models.ViewModels.Pages;
using Forskningsradet.Core.Services;
using Forskningsradet.Core.Services.Contracts;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.ViewModelMappers.Pages
{
    public class CategoryListPageViewModelMapper : BasePageViewModelMapperWithQuery<CategoryListPage>
    {
        private readonly IContentListService _contentListService;
        private readonly IContentLoader _contentLoader;
        private readonly IUrlResolver _urlResolver;
        private readonly FilterLayoutReactModelBuilder _filterLayoutReactModelBuilder;
        private readonly ITagLinkListReactModelBuilder _tagLinkListReactModelBuilder;
        private readonly IFluidImageReactModelBuilder _fluidImageReactModelBuilder;
        private readonly IContentAreaReactModelBuilder _contentAreaReactModelBuilder;
        private readonly IPaginationReactModelBuilder _paginationReactModelBuilder;
        private readonly IContentTypeRepository<PageType> _pageTypeRepository;
        private readonly ISearchApiUrlResolver _searchApiUrlResolver;
        private readonly ICategoryFilterService _categoryFilterService;
        private readonly CategoryRepository _categoryRepository;
        private readonly ICategoryLocalizationProvider _categoryLocalizationProvider;
        private readonly ILocalizationProvider _localizationProvider;

        public CategoryListPageViewModelMapper(IContentListService contentListService, IContentLoader contentLoader, IUrlResolver urlResolver, FilterLayoutReactModelBuilder filterLayoutReactModelBuilder, ITagLinkListReactModelBuilder tagLinkListReactModelBuilder, IFluidImageReactModelBuilder fluidImageReactModelBuilder, IContentAreaReactModelBuilder contentAreaReactModelBuilder, IPaginationReactModelBuilder paginationReactModelBuilder, IContentTypeRepository<PageType> PageTypeRepository, ISearchApiUrlResolver searchApiUrlResolver, ICategoryFilterService categoryFilterService, CategoryRepository categoryRepository, ICategoryLocalizationProvider categoryLocalizationProvider, ILocalizationProvider localizationProvider)
        {
            _contentListService = contentListService;
            _contentLoader = contentLoader;
            _urlResolver = urlResolver;
            _filterLayoutReactModelBuilder = filterLayoutReactModelBuilder;
            _tagLinkListReactModelBuilder = tagLinkListReactModelBuilder;
            _fluidImageReactModelBuilder = fluidImageReactModelBuilder;
            _contentAreaReactModelBuilder = contentAreaReactModelBuilder;
            _paginationReactModelBuilder = paginationReactModelBuilder;
            _pageTypeRepository = PageTypeRepository;
            _searchApiUrlResolver = searchApiUrlResolver;
            _categoryFilterService = categoryFilterService;
            _categoryRepository = categoryRepository;
            _categoryLocalizationProvider = categoryLocalizationProvider;
            _localizationProvider = localizationProvider;
        }

        protected override IReactViewModel GetViewModelForPageInternal(CategoryListPage currentPage, QueryParameterBase parameters) =>
            new ReactPageViewModel(currentPage, BuildReactModel(currentPage, parameters as CategoryListQueryParameter ?? new CategoryListQueryParameter()));

        private ReactModels.CategoryListPage BuildReactModel(CategoryListPage page, CategoryListQueryParameter parameters)
        {
            var result = _contentListService.Search<EditorialPage>(BuildContentListRequestModel(page, parameters));

            return new ReactModels.CategoryListPage
            {
                Title = string.IsNullOrEmpty(page.Title) ? page.PageName : page.Title,
                EmptyList = BuildEmptyListElement(),
                Groups = BuildItems(result.Results, page).ToList(),
                FilterLayout = BuildFilterLayout(parameters, result.Categories, page, result.TotalMatching),
                Form = BuildForm(),
                Pagination = BuildPagination(parameters, result.TotalMatching, _urlResolver.GetUrl(page)),
                FetchFilteredResultsEndpoint = _searchApiUrlResolver.GetSearchCategoryUrl()
            };
        }

        IEnumerable<ReactModels.ArticleBlock> BuildItems(IEnumerable<EditorialPage> items, CategoryListPage currentPage)
        {
            var categoriesForTag = GetCategoriesForTag().ToList();
            var categoriesForLinkTags = GetCategoriesForLinkTags().ToList();
            var categoryHomePages = GetCategoryHomePages();
            foreach (var page in items)
            {
                yield return new ReactModels.ArticleBlock
                {
                    Title = new ReactModels.Link
                    {
                        Url = _urlResolver.GetUrl(page.PageLink),
                        Text = page.GetListTitle()
                    },
                    Text = page.GetListIntroOrMainIntro(),
                    Tags = BuildTags(page.Category),
                    LinkTags = BuildLinkTags(page.Category),
                    Published = !page.StartPublish.HasValue ? null
                        : new ReactModels.Published
                        {
                            Type = GetLabel("Published"),
                            Date = page.StartPublish.Value.ToDisplayDate().ToString(DateTimeFormats.ProposalDeadlineDate)
                        },
                    Image = BuildImage(page)
                };
            }

            IEnumerable<Category> GetCategoriesForTag()
            {
                var top = currentPage.TagCategoryRoot?
                              .Select(x => _categoryRepository.Get(x))
                              .ToList() ?? new List<Category>();
                var children = top
                    .SelectMany(x => x.Categories)
                    .Distinct()
                    .ToList();
                return top
                    .Union(children);
            }

            ReactModels.TagList BuildTags(CategoryList categories)
            {
                return new ReactModels.TagList
                {
                    Tags = categoriesForTag
                        .Where(x => categories.MemberOf(x.ID))
                        .Select(x => new ReactModels.TagList_Tags
                        {
                            Text = _categoryLocalizationProvider.GetCategoryName(x.ID)
                        })
                        .Take(currentPage.ShowMultipleTags ? categoriesForTag.Count : 1)
                        .ToList()
                };
            }

            IEnumerable<Category> GetCategoriesForLinkTags() =>
                _categoryFilterService.GetCategoriesForLinkTags(currentPage.FilterCategoryList, currentPage.TagCategoryRoot);

            ReactModels.TagLinkList BuildLinkTags(CategoryList categories)
            {
                var categoryList = new CategoryList(
                    categoriesForLinkTags
                        .Where(x => categories.MemberOf(x.ID))
                        .Select(x => x.ID)
                        .ToList()
                );
                return _tagLinkListReactModelBuilder.BuildReactModel(categoryList, new CategoryList(), categoryHomePages, true, true);
            }
        }

        private ReactModels.FluidImage BuildImage(EditorialPage page) =>
            page.ListImage is null
                ? null
                : _fluidImageReactModelBuilder.BuildReactModel(_contentLoader.Get<ImageFile>(page.ListImage), null);

        private IReadOnlyList<ContentAreaPage> GetCategoryHomePages() =>
            _contentListService.Search<ContentAreaPage>(new ContentListRequestModel { OnlyGetCategoryHomePages = true, Take = SearchConstants.MaxPageSize })
                ?.Results
                .OfType<ContentAreaPage>()
                .ToList() ?? new List<ContentAreaPage>();

        private ContentListRequestModel BuildContentListRequestModel(CategoryListPage page, CategoryListQueryParameter parameters)
        {
            return new ContentListRequestModel
            {
                TopCategories = GetFilterCategoryListOrDefaultTopCategories(),
                PageCategories = page.Category?.ToArray() ?? new int[0],
                Categories = parameters.Categories,
                ContentTypes = string.IsNullOrEmpty(page.PageTypes)
                    ? GetDefaultPageTypeIds()
                    : page.PageTypes.Split(',').Select(int.Parse).ToArray(),
                PageRoot = GetPageRoot(page),
                Page = parameters.Page,
                Take = SearchConstants.SearchPageSize
            };

            int[] GetFilterCategoryListOrDefaultTopCategories()
            {
                var list = page.FilterCategoryList?.ToArray() ?? new int[0];
                return list.Length > 0
                    ? list
                    : new[]
                    {
                        _categoryRepository.Get(CategoryConstants.SubjectRoot).ID,
                        _categoryRepository.Get(CategoryConstants.TargetGroupRoot).ID
                    };
            }
        }

        private int[] GetDefaultPageTypeIds() =>
            _pageTypeRepository.List()
                .Where(x => x.ModelType == typeof(ContentAreaPage)
                            || x.ModelType == typeof(InformationArticlePage)
                            || x.ModelType == typeof(NewsArticlePage)
                            || x.ModelType == typeof(PressReleasePage))
                .Select(x => x.ID)
                .ToArray();

        private static int GetPageRoot(CategoryListPage page) =>
            page.PageRoot?.ID > 0
                ? page.PageRoot.ID
                : ContentReference.StartPage?.ID ?? 0;

        private ReactModels.EmptyList BuildEmptyListElement() =>
            new ReactModels.EmptyList
            {
                Text = GetLabel("EmptyCategoryList")
            };

        private ReactModels.FilterLayout BuildFilterLayout(CategoryListQueryParameter parameters, IEnumerable<CategoryGroup> categories, CategoryListPage page, int resultsCount) =>
            _filterLayoutReactModelBuilder.BuildReactModel(
                new FilterLayoutModel(
                    categories.ToList(),
                    parameters.Categories?.Select(x => x.ToString()).ToArray(),
                    _contentAreaReactModelBuilder.BuildReactModel(page.RightBlockArea, nameof(page.RightBlockArea)),
                    page.FilterTitle,
                    resultsCount)
            );

        private ReactModels.Form BuildForm() =>
            new ReactModels.Form
            {
                Endpoint = "",
                SubmitButtonText = GetLabel("SubmitButtonText")
            };

        private ReactModels.Pagination BuildPagination(CategoryListQueryParameter parameters, int totalPages, string listPageUrl)
        {
            return _paginationReactModelBuilder.BuildReactModel(BuildCurrentQueryString(), parameters.Page, totalPages, SearchConstants.SearchPageSize, listPageUrl);

            string BuildCurrentQueryString() =>
                string.Join("&", BuildQueryParameters());

            IEnumerable<string> BuildQueryParameters()
            {
                if (parameters.Categories is null)
                    yield break;

                foreach (var category in parameters.Categories)
                    yield return $"{QueryParameters.Categories}={category}";
            }
        }

        private string GetLabel(string key) => _localizationProvider.GetLabel(nameof(CategoryListPage), key);
    }
}