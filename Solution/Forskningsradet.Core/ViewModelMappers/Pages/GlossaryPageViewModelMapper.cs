﻿using System.Collections.Generic;
using System.Linq;
using EPiServer.Web.Routing;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.QueryParameters;
using Forskningsradet.Core.Models.SearchModels;
using Forskningsradet.Core.Models.ViewModels.Contracts;
using Forskningsradet.Core.Models.ViewModels.Pages;
using Forskningsradet.Core.Services;
using Forskningsradet.Core.Services.Contracts;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.ViewModelMappers.Pages
{
    public class GlossaryPageViewModelMapper : BasePageViewModelMapperWithQuery<GlossaryPage>
    {
        private readonly IGlossaryService _glossaryService;
        private readonly IContentAreaReactModelBuilder _contentAreaReactModelBuilder;
        private readonly IUrlResolver _urlResolver;
        private readonly ILocalizationProvider _localizationProvider;

        public GlossaryPageViewModelMapper(IGlossaryService glossaryService, IContentAreaReactModelBuilder contentAreaReactModelBuilder, IUrlResolver urlResolver, ILocalizationProvider localizationProvider)
        {
            _glossaryService = glossaryService;
            _contentAreaReactModelBuilder = contentAreaReactModelBuilder;
            _urlResolver = urlResolver;
            _localizationProvider = localizationProvider;
        }

        protected override IReactViewModel GetViewModelForPageInternal(GlossaryPage glossaryPage, QueryParameterBase parameters)
            => new ReactPageViewModel(glossaryPage, BuildGlossaryPageReactModel(glossaryPage, parameters as GlossaryQueryParameter));

        private ReactModels.GroupedSearchPage BuildGlossaryPageReactModel(GlossaryPage page, GlossaryQueryParameter parameters)
        {
            var query = parameters?.Query?.Trim();
            var result = _glossaryService.GetWordsMatchingQuery(page, query);
            return new ReactModels.GroupedSearchPage()
            {
                Title = page.PageName,
                Results = BuildWordList(result.Results?.ToList()).ToList(),
                Navigation = BuildLetters(result),
                Form = BuildForm(page),
                Search = BuildSearch(query),
                FilterLayout = BuildFilterLayout(page),
                EmptyList = BuildEmptyList()
            };
        }

        private IEnumerable<ReactModels.SearchResultGroup> BuildWordList(List<WordPage> words)
        {
            foreach (var grouping in words.GroupBy(x => x.Letter))
            {
                var letter = grouping.Key.ToUpper();
                yield return new ReactModels.SearchResultGroup
                {
                    Title = letter,
                    HtmlId = letter,
                    Results = BuildContentAreaComponent(grouping.ToList())
                };
            }
        }

        private ReactModels.ContentArea BuildContentAreaComponent(List<WordPage> words)
            => new ReactModels.ContentArea
            {
                Blocks = words.Select(BuildItem).ToList()
            };

        private ReactModels.ContentAreaItem BuildItem(WordPage word, int index)
            => new ReactModels.ContentAreaItem
            {
                Id = index.ToString(),
                ComponentName = nameof(ReactModels.SearchResult),
                ComponentData = new ReactModels.SearchResult
                {
                    Title = word.PageName,
                    Text = word.MainIntro
                }
            };

        private List<ReactModels.GroupedSearchPageLink> BuildLetters(GlossarySearchResults results)
        {
            return results.Letters.Select(x =>
                    new ReactModels.GroupedSearchPageLink
                    {
                        IsCurrent = false,
                        Link = new ReactModels.Link
                        {
                            Text = x,
                            Url = results.Results.Any(y => y.Letter == x)
                                ? $"#{x}"
                                : null
                        }
                    })
                .ToList();
        }

        private ReactModels.Form BuildForm(GlossaryPage page)
            => new ReactModels.Form
            {
                Endpoint = _urlResolver.GetUrl(page.ContentLink),
            };

        private ReactModels.Search BuildSearch(string query)
            => new ReactModels.Search
            {
                Input = new ReactModels.TextInput
                {
                    Name = Common.Constants.QueryParameters.SearchQuery,
                    Placeholder = GetLabel("SearchPlaceholder"),
                    Value = query ?? ""
                },
                Submit = new ReactModels.Button
                {
                    Text = GetLabel("SearchButtonText"),
                }
            };

        private ReactModels.FilterLayout BuildFilterLayout(GroupedSearchPage page)
            => new ReactModels.FilterLayout
            {
                ContentArea = _contentAreaReactModelBuilder.BuildReactModel(page.RightBlockArea, nameof(page.RightBlockArea)),
            };

        private ReactModels.EmptyList BuildEmptyList()
            => new ReactModels.EmptyList { Text = GetLabel("EmptyListLabel") };

        private string GetLabel(string key)
            => _localizationProvider.GetLabel(nameof(GlossaryPage), key);
    }
}