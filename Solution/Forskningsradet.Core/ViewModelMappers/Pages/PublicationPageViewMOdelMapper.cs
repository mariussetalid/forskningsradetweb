﻿using System.Collections.Generic;
using EPiServer.Core;
using EPiServer.Web.Routing;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Facades.Contracts;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.Enums;
using Forskningsradet.Core.Models.ViewModels.Contracts;
using Forskningsradet.Core.Models.ViewModels.Pages;
using Forskningsradet.Core.Services;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.ViewModelMappers.Pages
{
    public class PublicationPageViewModelMapper : BasePageViewModelMapper<PublicationPage>
    {
        private readonly ILocalizationProvider _localizationProvider;
        private readonly IUrlResolver _urlResolver;
        private readonly IPageEditingAdapter _pageEditingAdapter;
        private readonly IPublicationReactModelBuilder _publicationReactModelBuilder;

        public PublicationPageViewModelMapper(ILocalizationProvider localizationProvider, IUrlResolver urlResolver, IPageEditingAdapter pageEditingAdapter, IPublicationReactModelBuilder publicationReactModelBuilder)
        {
            _localizationProvider = localizationProvider;
            _urlResolver = urlResolver;
            _pageEditingAdapter = pageEditingAdapter;
            _publicationReactModelBuilder = publicationReactModelBuilder;
        }

        protected override IReactViewModel GetViewModelForPageInternal(PublicationPage currentPage) =>
            new ReactPageViewModel(new InformationArticlePage(), BuildPublicationPageReactModel(currentPage));

        private ReactModels.ArticlePage BuildPublicationPageReactModel(PublicationPage page) =>
            new ReactModels.ArticlePage
            {
                Sidebar = ContentReference.IsNullOrEmpty(page.Attachment) ? null : BuildSideBar(page),
                Content = new ReactModels.RichText
                {
                    Blocks = new List<ReactModels.ContentAreaItem>
                    {
                        new ReactModels.ContentAreaItem
                        {
                            ComponentName = nameof(ReactModels.SearchResult),
                            ComponentData = _publicationReactModelBuilder.BuildReactModel(page, PublicationMetadata.Large, false)
                        },
                    }
                },
                OnPageEditing = _pageEditingAdapter.PageIsInEditMode()
                    ? new ReactModels.ArticlePage_OnPageEditing { Title = nameof(page.Subtitle) }
                    : null
            };

        private ReactModels.ContentArea BuildSideBar(PublicationPage page) =>
            new ReactModels.ContentArea
            {
                Blocks = new List<ReactModels.ContentAreaItem>
                {
                    new ReactModels.ContentAreaItem
                    {
                        ComponentName = nameof(ReactModels.LinkListBlock),
                        ComponentData = new ReactModels.LinkListBlock
                        {
                            Title = _localizationProvider.GetLabel(nameof(PublicationListPage), "Download"),
                            Links = new ReactModels.LinkList
                            {
                                Items = new List<ReactModels.Link>
                                {
                                    new ReactModels.Link {
                                        Url = _urlResolver.GetUrl(page.Attachment),
                                        Text = page.Name,
                                        OnPageEditing = _pageEditingAdapter.PageIsInEditMode() ? nameof(page.Attachment) : null
                                    }
                                }
                            }
                        }
                    }
                }
            };
    }
}
