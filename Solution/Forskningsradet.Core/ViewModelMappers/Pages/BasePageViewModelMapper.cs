﻿using EPiServer.Core;
using Forskningsradet.Core.Models.ViewModels.Contracts;

namespace Forskningsradet.Core.ViewModelMappers.Pages
{
    public abstract class BasePageViewModelMapper<T> : IPageViewModelMapper where T : IContentData
    {
        public IReactViewModel GetPageViewModel(IContentData currentContent)
            => currentContent is T content
                ? GetViewModelForPageInternal(content)
                : null;

        protected abstract IReactViewModel GetViewModelForPageInternal(T currentContent);
    }
}