﻿using System.Collections.Generic;
using System.Linq;
using EPiServer;
using EPiServer.Core;
using EPiServer.Web.Routing;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Extensions;
using Forskningsradet.Core.Facades.Contracts;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ContentModels.Media;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.ContentModels.PropertyList;
using Forskningsradet.Core.Models.ViewModels.Contracts;
using Forskningsradet.Core.Models.ViewModels.Pages;
using Forskningsradet.Core.Services;
using Forskningsradet.Core.Services.Contracts;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.ViewModelMappers.Pages
{
    public class NpMethodPageViewModelMapper : BasePageViewModelMapper<NpMethodPage>
    {
        private readonly IUrlResolver _urlResolver;
        private readonly IUrlCheckingService _urlCheckingService;
        private readonly IPageEditingAdapter _pageEditingAdapter;
        private readonly IContentLoader _contentLoader;
        private readonly IQuotesBlockReactModelBuilder _quoteListReactModelBuilder;
        private readonly ILocalizationProvider _localizationProvider;

        private const string StepLabelKeyFormat = "Step{0}";

        public NpMethodPageViewModelMapper(
            IUrlResolver urlResolver,
            IUrlCheckingService urlCheckingService,
            IPageEditingAdapter pageEditingAdapter,
            IContentLoader contentLoader,
            IQuotesBlockReactModelBuilder quoteListBlockReactModelBuilder,
            ILocalizationProvider localizationProvider
            )
        {
            _urlResolver = urlResolver;
            _urlCheckingService = urlCheckingService;
            _pageEditingAdapter = pageEditingAdapter;
            _contentLoader = contentLoader;
            _quoteListReactModelBuilder = quoteListBlockReactModelBuilder;
            _localizationProvider = localizationProvider;
        }

        protected override IReactViewModel GetViewModelForPageInternal(NpMethodPage currentPage) =>
            new ReactPageViewModel(currentPage, BuildReactModel(currentPage));

        private ReactModels.NpMethod BuildReactModel(NpMethodPage page) =>
            new ReactModels.NpMethod
            {
                Title = page.Title ?? page.PageName,
                Ingress = page.MainIntro,
                Link = BuildLink(page),
                Menu = BuildSteps(page.Steps).ToList(),
                StepsContent = BuildStepsContent(page)
            };

        private ReactModels.Link BuildLink(NpMethodPage page)
        {
            return page.EditLink?.Link != null || _pageEditingAdapter.PageIsInEditMode()
                ? new ReactModels.Link
                {
                    Text = page.EditLink?.Text,
                    Url = BuildUrl(),
                    OnPageEditing = _pageEditingAdapter.PageIsInEditMode()
                        ? nameof(page.EditLink)
                        : null,
                    IsExternal = page.EditLink?.Link.IsExternal(_urlCheckingService) == true
                }
                : null;

            string BuildUrl() =>
                page.EditLink?.Link.GetUrl(_urlResolver) is string url
                    ? url
                    : null;
        }

        private IEnumerable<ReactModels.Step> BuildSteps(IEnumerable<NpStepBlock> steps)
        {
            return steps.Select((x, i) => BuildStep(x.Title, GetLabel(string.Format(StepLabelKeyFormat, i + 1))));

            ReactModels.Step BuildStep(string text, string fallback) =>
                new ReactModels.Step
                {
                    Text = !string.IsNullOrEmpty(text)
                        ? text
                        : fallback
                };
        }

        private ReactModels.StepsContent BuildStepsContent(NpMethodPage page)
        {
            return new ReactModels.StepsContent
            {
                Close = new ReactModels.StepsContent_Close
                {
                    TextDesktop = !string.IsNullOrEmpty(page.CloseTextDesktop) ? page.CloseTextDesktop : GetLabel("CloseDesktop"),
                    TextMobile = !string.IsNullOrEmpty(page.CloseTextMobile) ? page.CloseTextMobile : GetLabel("CloseMobile")
                },
                Steps = page.Steps
                    .Select((step, i) => BuildStepContent(GetLabel(string.Format(StepLabelKeyFormat, i + 1)), step))
                    .ToList()
            };

            ReactModels.StepsContent_Steps BuildStepContent(string stepLabel, NpStepBlock step) =>
            new ReactModels.StepsContent_Steps
            {
                VideoBlock = BuildVideoBlock(stepLabel, step.Video?.UrlOrEmbedCode, step.Title, step.Intro),
                StepsBlock = BuildStepsBlock(step.ImageListBlock),
                QuotesBlock = _quoteListReactModelBuilder.BuildReactModel(step.QuotesBlock)
            };

            ReactModels.VideoBlock BuildVideoBlock(string stepLabel, string urlOrEmbedcode, string title, string intro) =>
                new ReactModels.VideoBlock
                {
                    Step = stepLabel,
                    Title = title,
                    Text = intro,
                    PlayText = GetLabel("PlayVideoText"),
                    Video = !string.IsNullOrEmpty(urlOrEmbedcode)
                    ? new ReactModels.YoutubeVideo
                    {
                        UrlOrEmbed = urlOrEmbedcode
                    }
                    : null
                };

            ReactModels.StepsBlock BuildStepsBlock(NpStepImagesBlock block) =>
                block?.ImageList?.Any() != true
                ? null
                : new ReactModels.StepsBlock
                {
                    Pages = BuildStepsBlockPages(block.ImageList).ToList()
                };
        }

        private IEnumerable<ReactModels.StepsBlock_Pages> BuildStepsBlockPages(IList<NpStepImageListItem> list)
        {
            foreach (var item in list)
            {
                yield return new ReactModels.StepsBlock_Pages
                {
                    Title = item.Title,
                    Text = item.Ingress,
                    ButtonText = item.ButtonText,
                    Image = BuildImage(item.Image)
                };
            }
        }

        private ReactModels.Image BuildImage(ContentReference image)
        {
            if (!_contentLoader.TryGet(image, out ImageFile imageFile))
            {
                return new ReactModels.Image
                {
                    Alt = string.Empty,
                    Src = string.Empty
                };
            }

            return new ReactModels.Image
            {
                Alt = imageFile.AltText ?? string.Empty,
                Src = _urlResolver.GetUrl(imageFile.ContentLink) is string url && !string.IsNullOrEmpty(url)
                    ? url
                    : string.Empty,
            };
        }

        private string GetLabel(string key) =>
            _localizationProvider.GetLabel(nameof(NpMethodPage), key);
    }
}
