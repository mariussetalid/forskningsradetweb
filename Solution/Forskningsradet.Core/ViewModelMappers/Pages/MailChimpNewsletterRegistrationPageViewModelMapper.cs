﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Helpers;
using EPiServer.Web.Routing;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.QueryParameters;
using Forskningsradet.Core.Models.Validation;
using Forskningsradet.Core.Models.ViewModels.Contracts;
using Forskningsradet.Core.Models.ViewModels.Forms;
using Forskningsradet.Core.Models.ViewModels.Pages;
using Forskningsradet.Core.Services;
using Forskningsradet.Core.Services.Contracts;
using Forskningsradet.ServiceAgents.MailChimp.Models;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.ViewModelMappers.Pages
{
    public class MailChimpNewsletterRegistrationPageViewModelMapper : BasePageViewModelMapperWithQuery<MailChimpNewsletterRegistrationPage>
    {
        private readonly IRichTextReactModelBuilder _richTextReactModelBuilder;
        private readonly IContentAreaReactModelBuilder _contentAreaReactModelBuilder;
        private readonly IMessageReactModelBuilder _messageReactModelBuilder;
        private readonly ISocialMediaLinkListReactModelBuilder _socialMediaLinkListReactModelBuilder;
        private readonly IMailChimpService _mailChimpService;
        private readonly IUrlResolver _urlResolver;
        private readonly ILocalizationProvider _localizationProvider;

        private const string FormMethodPost = "POST";
        private const string EmailFieldsTitle = "EmailFieldsTitle";
        private const string Register = "Register";
        private const string Validation = "Validation";
        private const char Separator = ',';

        public MailChimpNewsletterRegistrationPageViewModelMapper(
            IRichTextReactModelBuilder richTextReactModelBuilder,
            IContentAreaReactModelBuilder contentAreaReactModelBuilder,
            IMessageReactModelBuilder messageReactModelBuilder,
            ISocialMediaLinkListReactModelBuilder socialMediaLinkListReactModelBuilder,
            IMailChimpService mailChimpService,
            IUrlResolver urlResolver,
            ILocalizationProvider localizationProvider
            )
        {
            _richTextReactModelBuilder = richTextReactModelBuilder;
            _contentAreaReactModelBuilder = contentAreaReactModelBuilder;
            _messageReactModelBuilder = messageReactModelBuilder;
            _socialMediaLinkListReactModelBuilder = socialMediaLinkListReactModelBuilder;
            _mailChimpService = mailChimpService;
            _urlResolver = urlResolver;
            _localizationProvider = localizationProvider;
        }

        protected override IReactViewModel GetViewModelForPageInternal(MailChimpNewsletterRegistrationPage currentPage, QueryParameterBase parameters) =>
            new ReactPageViewModel(currentPage, BuildReactModel(currentPage, parameters as NewsletterRegistrationQueryParameter ?? new NewsletterRegistrationQueryParameter()));

        private ReactModels.NewsletterRegistrationPage BuildReactModel(MailChimpNewsletterRegistrationPage currentPage, NewsletterRegistrationQueryParameter parameters)
        {
            if (currentPage.MailChimpApiKey is null || currentPage.MailChimpListId is null)
                throw new ArgumentException("Mailchimp settings cannot be null.");

            var groups = AsyncHelpers.RunSync(() => _mailChimpService.GetGroups(currentPage.MailChimpApiKey, currentPage.MailChimpListId));
            var attemptedSubscriptions = GetAttemptedSubscriptions(parameters.ModelValidation);
            return new ReactModels.NewsletterRegistrationPage
            {
                Title = currentPage.PageName,
                RichText = _richTextReactModelBuilder.BuildReactModel(currentPage.MainIntro, nameof(currentPage.MainIntro)),
                Sidebar = _contentAreaReactModelBuilder.BuildReactModel(currentPage.RightBlockArea, nameof(currentPage.RightBlockArea)),
                CheckboxGroups = BuildCheckBoxGroups(groups, attemptedSubscriptions),
                Form = new ReactModels.Form
                {
                    Method = FormMethodPost,
                    Endpoint = _urlResolver.GetUrl(currentPage.ContentLink),
                    SubmitButtonText = GetLabel(Register)
                },
                SubmitButton = new ReactModels.Button
                {
                    Text = GetLabel(Register)
                },
                InputFields = BuildInputFieldsItems(parameters).ToList(),
                InputTitle = GetLabel(EmailFieldsTitle),
                HtmlString = CreateAntiForgeryToken(),
                Message = BuildMessage(currentPage.ValidationMessage, parameters.ModelValidation),
                Footer = BuildFooter(currentPage)
            };
        }

        private IEnumerable<ReactModels.InputEmail> BuildInputFieldsItems(NewsletterRegistrationQueryParameter parameters)
        {
            yield return BuildEmailInputFromParameters(parameters, nameof(NewsletterRegistrationFormModel.Email));
            yield return BuildEmailInputFromParameters(parameters, nameof(NewsletterRegistrationFormModel.RepeatEmail));
        }

        private ReactModels.InputEmail BuildEmailInputFromParameters(NewsletterRegistrationQueryParameter parameters, string fieldName)
        {
            return BuildInput(GetModelValidation());

            ReactModels.InputEmail BuildInput(NewsletterModelValidation validationModel) =>
                new ReactModels.InputEmail
                {
                    Name = fieldName,
                    Label = GetLabel(fieldName),
                    Value = GetModelRawValue(validationModel),
                    ValidationError = GetLabel(validationModel?.Errors?.FirstOrDefault())
                };

            NewsletterModelValidation GetModelValidation() =>
                parameters?.ModelValidation?
                .FirstOrDefault(x => x.FieldName.Equals(fieldName));

            string GetModelRawValue(NewsletterModelValidation validationModel) =>
                validationModel?.AttemptedValue;
        }

        private List<string> GetAttemptedSubscriptions(IEnumerable<NewsletterModelValidation> parametersModel)
        {
            var subscriptionsModelField = parametersModel?.FirstOrDefault(x =>
                x.FieldName.Equals(nameof(NewsletterRegistrationFormModel.Subscriptions)));
            return subscriptionsModelField is null
                ? new List<string>()
                : ExtractSubscriptionIds(subscriptionsModelField.AttemptedValue);
        }

        private static List<string> ExtractSubscriptionIds(string attemptedValue) =>
            attemptedValue?.Split(Separator).ToList() ?? new List<string>();

        private ReactModels.Message BuildMessage(string validationText, IEnumerable<NewsletterModelValidation> fields)
        {
            var text = string.IsNullOrEmpty(validationText)
                ? GetLabel(Validation)
                : validationText;

            var fieldsWithError = fields?
                .Where(x => x.Errors.Any())
                .Select(x => GetLabel(x.FieldName))
                .ToList() ?? new List<string>();

            return fieldsWithError.Any()
                ? _messageReactModelBuilder.BuildReactModel($"{text}<br />{string.Join("<br />", fieldsWithError)}", null)
                : null;
        }

        private static string CreateAntiForgeryToken() => AntiForgery.GetHtml().ToHtmlString();

        private ReactModels.PageFooter BuildFooter(MailChimpNewsletterRegistrationPage currentPage) =>
            new ReactModels.PageFooter();

        private static IList<ReactModels.CheckboxGroup> BuildCheckBoxGroups(IEnumerable<Group> groups, List<string> attemptedSubscriptions) =>
            groups.Select(group => new ReactModels.CheckboxGroup
            {
                Title = group.Title,
                Options = BuildCheckBoxes(group.Children, attemptedSubscriptions)
            }).ToList();

        private static IList<ReactModels.Checkbox> BuildCheckBoxes(IEnumerable<Group> groupChildren, List<string> attemptedSubscriptions) =>
            groupChildren.Select(group => new ReactModels.Checkbox
            {
                Name = nameof(NewsletterRegistrationFormModel.Subscriptions),
                Value = group.Id,
                Label = group.Title,
                Checked = attemptedSubscriptions.Contains(group.Id)
            }).ToList();

        private string GetLabel(string key) =>
            _localizationProvider.GetLabel(nameof(MailChimpNewsletterRegistrationPage), key);
    }
}