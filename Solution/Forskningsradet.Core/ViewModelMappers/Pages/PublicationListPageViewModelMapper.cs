﻿using System;
using System.Collections.Generic;
using System.Linq;
using EPiServer.Find.UnifiedSearch;
using EPiServer.Web.Routing;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Builders;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.Enums;
using Forskningsradet.Core.Models.QueryParameters;
using Forskningsradet.Core.Models.RequestModels;
using Forskningsradet.Core.Models.SearchModels;
using Forskningsradet.Core.Models.ViewModels.Contracts;
using Forskningsradet.Core.Models.ViewModels.Pages;
using Forskningsradet.Core.Services;
using Forskningsradet.Core.Services.Contracts;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.ViewModelMappers.Pages
{
    public class PublicationListPageViewModelMapper : BasePublicationListPageViewModelMapper<PublicationListPage>
    {
        private readonly FilterGroupReactModelBuilder _filterGroupReactModelBuilder;
        private readonly ISearchApiUrlResolver _searchApiUrlResolver;
        private readonly IPaginationReactModelBuilder _paginationReactModelBuilder;

        public PublicationListPageViewModelMapper(
            IPublicationListService publicationsService,
            IPublicationReactModelBuilder publicationReactModelBuilder,
            ILocalizationProvider localizationProvider,
            IUrlResolver urlResolver,
            ISearchApiUrlResolver searchApiUrlResolver,
            FilterGroupReactModelBuilder filterGroupReactModelBuilder,
            IPaginationReactModelBuilder paginationReactModelBuilder)
            : base(publicationsService, publicationReactModelBuilder, localizationProvider, urlResolver)
        {
            _filterGroupReactModelBuilder = filterGroupReactModelBuilder;
            _paginationReactModelBuilder = paginationReactModelBuilder;
            _searchApiUrlResolver = searchApiUrlResolver;
        }

        protected override IReactViewModel GetViewModelForPageInternal(PublicationListPage page, QueryParameterBase parameters)
        {
            var queryParameters = parameters as PublicationListQueryParameter;
            var viewModel = BuildViewModel(page, queryParameters);
            return new ReactPageViewModel(page, viewModel);
        }

        protected override ReactModels.Filters BuildFilters(PublicationListPage currentPage, PublicationRequest request, PublicationListSearchResults results) =>
            new ReactModels.Filters
            {
                Title = currentPage.FilterTitle,
                MobileTitle = currentPage.FilterTitle ?? GetLabel("FilterTitle"),
                Labels = new ReactModels.Filters_Labels
                {
                    Reset = GetLabel("Reset"),
                    ShowResults = string.Format(GetLabel("ShowResultsFormat"), results.TotalMatching)
                },
                Items = new List<ReactModels.FilterGroup>
                {
                    BuildFilterGroup(GetLabel("FilterTitleType"), QueryParameters.ResultType, results.Types.ToList(), request.Types ?? new int[0]),
                }
            };

        protected override List<PublicationBasePage> SortPublications(List<PublicationBasePage> publications) =>
            publications.OrderBy(x => x.PageName).ToList();

        private ReactModels.ReactComponent BuildViewModel(PublicationListPage currentPage, PublicationListQueryParameter queryParameters)
        {
            var pageSize = !string.IsNullOrWhiteSpace(queryParameters.Query) && currentPage.IncludeContentOfAttachmentWhenSearching
                ? SearchConstants.SearchPageSize
                : SearchConstants.MaxPageSize;
            var searchRequestModel = MapQueryParameterToRequestModel(queryParameters, currentPage, pageSize);
            searchRequestModel.UseGlobalSearchWithQuery = currentPage.IncludeContentOfAttachmentWhenSearching;
            var results = _publicationsService.Search(searchRequestModel);
            var allYears = results.Years.ToList();
            var searchResultGroups = results.SearchPageResults is null
                ? BuildResultGroups(results.Results.ToList()).ToList()
                : BuildResultGroups(results.SearchPageResults).ToList();
            var listPageUrl = _urlResolver.GetUrl(currentPage.ContentLink);

            return new ReactModels.PublicationsPage
            {
                Title = currentPage.PageName,
                EmptyList = BuildEmptyListElement(results.TotalMatching),
                Form = new ReactModels.Form
                {
                    Endpoint = listPageUrl,
                    SubmitButtonText = GetLabel("SubmitButtonText")
                },
                Results = searchResultGroups,
                FilterLayout = BuildFilterLayout(currentPage, searchRequestModel, results),
                Search = BuildSearch(queryParameters.Query),
                Navigation = BuildNavigation(
                    allYears,
                    searchResultGroups,
                    listPageUrl,
                    searchRequestModel),
                FetchFilteredResultsEndpoint = _searchApiUrlResolver.GetSearchPublicationUrl(),
                Pagination = searchRequestModel.WithPaging
                    ? BuildPagination(searchRequestModel, results.TotalMatching, queryParameters.Page, pageSize, listPageUrl)
                    : null
            };
        }

        private IEnumerable<ReactModels.SearchResultGroup> BuildResultGroups(SearchPageResults results)
        {
            var groupedHits = results.Results
                .ToList()
                .GroupBy(x => GetPublicationFromHit(x)?.Year ?? 0)
                .ToList();

            foreach (var group in groupedHits.OrderByDescending(x => x.Key))
            {
                var year = group.Key;
                var sortedHits = group.OrderBy(x => GetPublicationFromHit(x)?.PageName ?? string.Empty).ToList();
                yield return new ReactModels.SearchResultGroup
                {
                    Title = year.ToString(),
                    HtmlId = year.ToString(),
                    Results = BuildContentAreaComponent(sortedHits)
                };
            }
        }

        private ReactModels.ContentArea BuildContentAreaComponent(IEnumerable<UnifiedSearchHit> hits)
        {
            return new ReactModels.ContentArea
            {
                Blocks = hits.Select(BuildItem).ToList()
            };

            ReactModels.ContentAreaItem BuildItem(UnifiedSearchHit hit, int index) =>
               new ReactModels.ContentAreaItem
               {
                   Id = index.ToString(),
                   ComponentName = nameof(ReactModels.SearchResult),
                   ComponentData = _publicationReactModelBuilder.BuildReactModel(GetPublicationFromHit(hit), PublicationMetadata.Standard, false, hit.Excerpt)
               };
        }

        private PublicationBasePage GetPublicationFromHit(UnifiedSearchHit x) => (x.OriginalObjectGetter() as PublicationBasePage);

        private ReactModels.FilterGroup BuildFilterGroup(string title, string optionName, List<FilterOption> options, int[] selection) =>
            _filterGroupReactModelBuilder.BuildReactModel(
                BuildAccordion(selection.Any()),
                title,
                optionName,
                options,
                selection);

        private ReactModels.Accordion BuildAccordion(bool initiallyOpen) =>
            new ReactModels.Accordion
            {
                CollapseLabel = GetLabel("Collapse"),
                ExpandLabel = GetLabel("Expand"),
                Guid = Guid.NewGuid().ToString(),
                InitiallyOpen = initiallyOpen
            };

        private ReactModels.Pagination BuildPagination(PublicationRequest publicationRequest, int totalPages, int currentPage, int pageSize, string listPageUrl) =>
            _paginationReactModelBuilder.BuildReactModel(BuildCurrentQueryString(publicationRequest), currentPage, totalPages, pageSize, listPageUrl);
    }
}