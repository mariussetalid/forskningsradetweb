using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.Globalization;
using EPiServer.Web.Routing;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Configuration;
using Forskningsradet.Core.Extensions;
using Forskningsradet.Core.Facades.Contracts;
using Forskningsradet.Core.Factories;
using Forskningsradet.Core.Models.ApiModels.Evurdering;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.QueryParameters;
using Forskningsradet.Core.Models.ViewModels.Contracts;
using Forskningsradet.Core.Models.ViewModels.Pages;
using Forskningsradet.Core.Repositories.Contracts;
using Forskningsradet.Core.Services;
using Forskningsradet.Core.Services.Contracts;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.ViewModelMappers.Pages
{
    public class ProposalPageViewModelMapper : BasePageViewModelMapper<ProposalPage>
    {
        private readonly IProposalService _proposalService;
        private readonly IRichTextReactModelBuilder _richTextReactModelBuilder;
        private readonly ILocalizationProvider _localizationProvider;
        private readonly IContentRepository _contentRepository;
        private readonly IPageRepository _pageRepository;
        private readonly IUrlResolver _urlResolver;
        private readonly IProposalPageConfiguration _proposalPageConfiguration;
        private readonly IViewModelFactory _viewModelFactory;
        private readonly IContentAreaReactModelBuilder _contentAreaReactModelBuilder;
        private readonly IPageEditingAdapter _pageEditingAdapter;
        private readonly IContentLoader _contentLoader;
        private readonly ICategoryLocalizationProvider _categoryLocalizationProvider;
        private readonly IPersonPageStringBuilder _personPageStringBuilder;
        private readonly IOptionsModalReactModelBuilder _optionsModalReactModelBuilder;
        private readonly IDateCardStatusHandler _dateCardStatusHandler;
        private readonly IProposalContactReactModelBuilder _proposalContactReactModelBuilder;
        private readonly IArticleHeaderReactModelBuilder _articleHeaderReactModelBuilder;
        private readonly IDownloadListReactModelBuilder _downloadListReactModelBuilder;
        private readonly CategoryRepository _categoryRepository;

        public ProposalPageViewModelMapper(
            IProposalService proposalService,
            IRichTextReactModelBuilder richTextReactModelBuilder,
            ILocalizationProvider localizationProvider,
            IContentRepository contentRepository,
            IPageRepository pageRepository,
            IUrlResolver urlResolver,
            IProposalPageConfiguration proposalPageConfiguration,
            IViewModelFactory viewModelFactory,
            IContentAreaReactModelBuilder contentAreaReactModelBuilder,
            IPageEditingAdapter pageEditingAdapter,
            IContentLoader contentLoader,
            ICategoryLocalizationProvider categoryLocalizationProvider,
            CategoryRepository categoryRepository,
            IPersonPageStringBuilder personPageStringBuilder,
            IOptionsModalReactModelBuilder optionsModalReactModelBuilder,
            IDateCardStatusHandler dateCardStatusHandler,
            IProposalContactReactModelBuilder proposalContactReactModelBuilder,
            IArticleHeaderReactModelBuilder articleHeaderReactModelBuilder,
            IDownloadListReactModelBuilder downloadListReactModelBuilder)
        {
            _proposalService = proposalService;
            _richTextReactModelBuilder = richTextReactModelBuilder;
            _localizationProvider = localizationProvider;
            _contentRepository = contentRepository;
            _pageRepository = pageRepository;
            _urlResolver = urlResolver;
            _proposalPageConfiguration = proposalPageConfiguration;
            _viewModelFactory = viewModelFactory;
            _contentAreaReactModelBuilder = contentAreaReactModelBuilder;
            _pageEditingAdapter = pageEditingAdapter;
            _contentLoader = contentLoader;
            _categoryLocalizationProvider = categoryLocalizationProvider;
            _categoryRepository = categoryRepository;
            _personPageStringBuilder = personPageStringBuilder;
            _optionsModalReactModelBuilder = optionsModalReactModelBuilder;
            _dateCardStatusHandler = dateCardStatusHandler;
            _proposalContactReactModelBuilder = proposalContactReactModelBuilder;
            _articleHeaderReactModelBuilder = articleHeaderReactModelBuilder;
            _downloadListReactModelBuilder = downloadListReactModelBuilder;
        }

        protected override IReactViewModel GetViewModelForPageInternal(ProposalPage page) =>
            new ReactPageViewModel(page, BuildProposalPageReactModel(page));

        private ReactModels.ReactComponent BuildProposalPageReactModel(ProposalPage page)
        {
            var applyButton = BuildApplyButton(page);

            var model = new ReactModels.ProposalPage
            {
                Title = page.Name,
                Message = BuildMessage(page),
                DescriptionTitle = GetLabel("PurposeTitle"),
                DescriptionText = _richTextReactModelBuilder.BuildReactModel(page.Purpose, nameof(page.Purpose)),
                ApplyButton = applyButton,
                MetadataLeft = BuildProposalData(BuildLeftMetadata(page)),
                MetadataRight = BuildProposalData(BuildRightMetadata(page)),
                ContactLabel = GetLabel("Contact"),
                Contact = BuildContact(page.Contact),
                Timeline = BuildTimeline(page.Timeline),
                Tabs = BuildTabs(page, applyButton),
                Download = _optionsModalReactModelBuilder.BuildDownloadContent(page),
                Share = _optionsModalReactModelBuilder.BuildShareContent(page),
                StatusList = _dateCardStatusHandler.BuildDateCardStatusList(page).ToList(),
                Header = _articleHeaderReactModelBuilder.BuildReactModelWithoutShare(page),
            };
            return model;
        }

        private static ReactModels.ProposalData BuildProposalData(IEnumerable<ReactModels.ProposalData_Items> items) =>
            new ReactModels.ProposalData { Items = items.ToList() };

        private IEnumerable<ReactModels.ProposalData_Items> BuildLeftMetadata(ProposalPage page)
        {
            if (GetPage<ApplicationTypePage>(page.ApplicationTypeReference) is ApplicationTypePage applicationType)
                yield return BuildApplicationType(applicationType);

            if (page.ProjectOutlineRequirement != ProjectOutlineRequirement.None && page.ProjectOutline != null)
                yield return BuildProjectOutline(page.ProjectOutline, page.ProjectOutlineRequirement);

            if (page.ChildProjectOutlines?.Any() ?? false)
                yield return BuildChildProjectOutlines(page.ChildProjectOutlines);

            yield return BuildDeadline(page.DeadlineComputed, page.DeadlineType);

            var sortedSubjects = _proposalService.GetSortedSubjects(page).ToList();
            if (sortedSubjects.Any())
                yield return BuildSubjects(sortedSubjects);

            yield return BuildTargetGroups(page.Category);
        }

        private IEnumerable<ReactModels.ProposalData_Items> BuildRightMetadata(ProposalPage page)
        {
            if (page.MinApplicationAmount > 0 && page.MaxApplicationAmount > 0)
                yield return BuildProjectSize(page.MinApplicationAmount, page.MaxApplicationAmount);

            if (!string.IsNullOrEmpty(page.ProposalAmount) && page.ProposalAmount != "0")
                yield return BuildAmount(page);

            if (page.MinProjectLength > 0 && page.MaxProjectLength > 0)
                yield return BuildDuration(page.MinProjectLength, page.MaxProjectLength);

            if (page.UpcomingProposalReference != null)
                yield return BuildNextProposalLink(page.UpcomingProposalReference);
        }

        private ReactModels.Message BuildMessage(ProposalPage page) =>
            page.Messages != null && page.Messages.FilteredItems.Any()
                ? _viewModelFactory.GetPartialViewModel(
                    _contentLoader.Get<MessageBaseBlock>(page.Messages.FilteredItems.First().ContentLink)).Model as ReactModels.Message
                : null;

        private ReactModels.Tabs BuildTabs(ProposalPage page, ReactModels.Link applyButton)
        {
            var items = BuildTabItems(page, applyButton).ToList();
            var id = items.Any()
                     && !PageReference.IsNullOrEmpty(page.ApplicationResult)
                     && (page.DeadlineComputed?.IsPast() ?? false)
                ? items.Last().Guid
                : null;
            return new ReactModels.Tabs
            {
                Items = items,
                ActiveTab = id,
                StickyMenuOnTabs = page.UseAlternativeSubjectBlockPresentation
            };
        }

        private IEnumerable<ReactModels.Tabs_Items> BuildTabItems(ProposalPage page, ReactModels.Link applyButton)
        {
            yield return BuildTab(GetLabel("ProposalTabLabel"), page.UseAlternativeSubjectBlockPresentation ? BuildProposalInfo() : BuildProposalInfoOldDesign());

            if (page.TableauContent?.FilteredItems.Any() ?? false)
                yield return BuildTab(GetLabel("ApplicationProcessingTabLabel"), BuildApplicationProcessing());

            if (page.ApplicationResult != null)
                yield return BuildTab(GetLabel("ApplicationResultsTabLabel"), BuildApplicationResults());

            ReactModels.TabContent BuildProposalInfo() =>
                BuildTabContent(BuildProposalInfoTabContent(page, applyButton), BuildMenu(page));

            ReactModels.TabContent BuildProposalInfoOldDesign() =>
                BuildTabContent(BuildProposalInfoTabContentOldDesign(page, applyButton), BuildMenu(page));

            ReactModels.TabContent BuildApplicationProcessing() =>
                BuildTabContent(BuildApplicationProcessingTabContent(page), null);

            ReactModels.TabContent BuildApplicationResults() =>
                BuildTabContent(BuildApplicationResultsTabContent(page), null);
        }

        private static ReactModels.Tabs_Items BuildTab(string title, ReactModels.TabContent tabContent)
            => new ReactModels.Tabs_Items
            {
                Name = title,
                Guid = Guid.NewGuid().ToString(),
                Content = tabContent
            };

        private static ReactModels.TabContent BuildTabContent(IEnumerable<ReactModels.TabContentSection> sections, ReactModels.StickyMenu menu)
            => new ReactModels.TabContent
            {
                Menu = menu,
                ContentSections = sections.ToList()
            };

        private IEnumerable<ReactModels.TabContentSection> BuildProposalInfoTabContentOldDesign(ProposalPage page, ReactModels.Link applyButton)
        {
            yield return new ReactModels.TabContentSection
            {
                Title = GetLabel("AboutTitle"),
                Ingress = _richTextReactModelBuilder.BuildReactModel(page.About, nameof(page.About)),
                Process = BuildProcess(page.ProcessContentArea, nameof(page.ProcessContentArea)),
                Texts = BuildTexts(page)
            };

            yield return new ReactModels.TabContentSection
            {
                ContentDescription = BuildSubjectDescription(),
                Content = BuildSubjectBlocksContentArea(page.SubjectBlocks, nameof(page.SubjectBlocks), page.UseAlternativeSubjectBlockPresentation, page.PageLink)
            };

            yield return new ReactModels.TabContentSection
            {
                Application = applyButton is null ? null : BuildApplicationLink(page, applyButton),
                MoreTexts = BuildMoreTexts(page),
                DownloadList = _downloadListReactModelBuilder.BuildTemplatesList(page),
                MoreContent = _contentAreaReactModelBuilder.BuildReactModel(page.Content, nameof(page.Content))
            };

            ReactModels.RichTextBlock BuildSubjectDescription() =>
                !string.IsNullOrEmpty(page.RelevantSubjectsText?.ToString())
                || page.SubjectBlocks?.Count > 0
                || _pageEditingAdapter.PageIsInEditMode()
                    ? new ReactModels.RichTextBlock
                    {
                        Title = GetLabel("RelevantSubjectsTitle"),
                        Text = _richTextReactModelBuilder.BuildReactModel(
                            page.RelevantSubjectsText,
                            nameof(page.RelevantSubjectsText))
                    }
                    : null;
        }

        private IEnumerable<ReactModels.TabContentSection> BuildProposalInfoTabContent(ProposalPage page, ReactModels.Link applyButton)
        {
            yield return new ReactModels.TabContentSection
            {
                Title = GetLabel("AboutTitle"),
                HtmlId = "AboutNavigationTitle",
                Ingress = _richTextReactModelBuilder.BuildReactModel(page.About, nameof(page.About)),
                Process = BuildProcess(page.ProcessContentArea, nameof(page.ProcessContentArea)),
                Texts = BuildTexts(page)
            };

            var shouldBuildSubjectSectionDescription = ShouldBuildSubjectDescription();
            yield return new ReactModels.TabContentSection
            {
                Title = !shouldBuildSubjectSectionDescription ? null : GetLabel("RelevantSubjectsTitle"),
                HtmlId = "SubjectsNavigationTitle",
                Ingress = !shouldBuildSubjectSectionDescription ? null : _richTextReactModelBuilder.BuildReactModel(page.RelevantSubjectsText, nameof(page.RelevantSubjectsText)),
                Content = BuildSubjectBlocksContentArea(page.SubjectBlocks, nameof(page.SubjectBlocks), page.UseAlternativeSubjectBlockPresentation, page.PageLink)
            };

            yield return new ReactModels.TabContentSection
            {
                Title = page.UseAlternativeSubjectBlockPresentation ? GetLabel("PracticalInfoNavigationTitle") : null,
                HtmlId = "PracticalInfoNavigationTitle",
                Application = applyButton is null ? null : BuildApplicationLink(page, applyButton),
                MoreTexts = BuildMoreTexts(page),
                DownloadList = _downloadListReactModelBuilder.BuildTemplatesList(page),
                MoreContent = _contentAreaReactModelBuilder.BuildReactModel(page.Content, nameof(page.Content))
            };

            bool ShouldBuildSubjectDescription() =>
                !string.IsNullOrEmpty(page.RelevantSubjectsText?.ToString())
                || page.SubjectBlocks?.Count > 0
                || _pageEditingAdapter.PageIsInEditMode();
        }

        private ReactModels.Process BuildProcess(ContentArea contentArea, string contentAreaName) =>
            _contentAreaReactModelBuilder.BuildReactModel(contentArea, contentAreaName).Blocks?.FirstOrDefault()?.ComponentData as ReactModels.Process ?? null;

        private ReactModels.TabContentSection_Application BuildApplicationLink(ProposalPage page, ReactModels.Link applyButton) =>
            new ReactModels.TabContentSection_Application
            {
                Title = applyButton.Text,
                Text = !string.IsNullOrEmpty(page.ApplicationText) ? page.ApplicationText : string.Format(GetLabel("ApplicationTextFormat"), page.Name),
                Link = applyButton
            };

        private ReactModels.ContentArea BuildSubjectBlocksContentArea(ContentArea subjectBlockContentArea, string contentAreaName, bool useAlternativePresentation, PageReference pageReference)
        {
            return new ReactModels.ContentArea
            {
                OnPageEditing = !_pageEditingAdapter.PageIsInEditMode() ? null : new ReactModels.ContentArea_OnPageEditing { Name = contentAreaName },
                Blocks = subjectBlockContentArea?.FilteredItems.Select((x, y) => BuildSubjectBlocksContentAreaItem(_contentLoader.Get<IContent>(x.ContentLink), y)).ToList()
            };

            ReactModels.ContentAreaItem BuildSubjectBlocksContentAreaItem(IContent item, int index)
            {
                var viewModel = _viewModelFactory.GetPartialViewModel(item, new SubjectBlockQueryParameter { UseAlternativeDesign = useAlternativePresentation, PageReference = pageReference });
                return new ReactModels.ContentAreaItem
                {
                    Id = index.ToString(),
                    ComponentName = viewModel.ReactComponentName,
                    ComponentData = viewModel.Model
                };
            }
        }

        private ReactModels.ContentAreaItem_OnPageEditing BuildContentAreaItemOnPageEditingComponent(string contentName, string contentId)
            => _pageEditingAdapter.PageIsInEditMode()
                ? new ReactModels.ContentAreaItem_OnPageEditing { ContentName = contentName, ContentId = contentId }
                : null;

        private IEnumerable<ReactModels.TabContentSection> BuildApplicationProcessingTabContent(ProposalPage page)
        {
            yield return new ReactModels.TabContentSection
            {
                Title = GetLabel("AboutApplicationProcessingTitle"),
                Ingress = _richTextReactModelBuilder.BuildReactModel(page.ApplicationProcessingText, nameof(page.ApplicationProcessingText)),
                Content = _contentAreaReactModelBuilder.BuildReactModel(page.TableauContent, nameof(page.TableauContent))
            };
        }

        private IEnumerable<ReactModels.TabContentSection> BuildApplicationResultsTabContent(ProposalPage page)
        {
            var applicationResult = _contentLoader.Get<ApplicationResultPage>(page.ApplicationResult);

            yield return new ReactModels.TabContentSection
            {
                Title = GetLabel("AboutApplicationResultsTitle"),
                Ingress = _richTextReactModelBuilder.BuildReactModel(page.ApplicationResultsText, nameof(page.ApplicationResultsText)),
                Content = _viewModelFactory.GetPartialViewModel(applicationResult) is IReactViewModel viewModel
                    ? new ReactModels.ContentArea
                    {
                        Blocks = new List<ReactModels.ContentAreaItem>
                        {
                            new ReactModels.ContentAreaItem
                            {
                                Id = Guid.NewGuid().ToString(),
                                ComponentName = viewModel.ReactComponentName,
                                ComponentData = viewModel.Model
                            }
                        }
                    }
                    : null
            };
        }

        private IList<ReactModels.RichTextBlock> BuildTexts(ProposalPage page)
        {
            var richTextBlocks = BuildTextList(
                BuildText(
                    page.IntendedApplicants,
                    "IntendedApplicantsTitle",
                    nameof(page.IntendedApplicants)),
                BuildText(
                    page.ProjectParticipants,
                    "ProjectParticipantsTitle",
                    nameof(page.ProjectParticipants)),
                BuildText(
                    page.Subject,
                    "SubjectTitle",
                    nameof(page.Subject))
            ).ToList();

            if (!string.IsNullOrEmpty(page.ArchivingOfDataText?.ToString()))
            {
                richTextBlocks.Add(BuildText(
                    page.ArchivingOfDataText,
                    "ArchivingOfDataTitle",
                    nameof(page.ArchivingOfDataText)));
            }

            if (!string.IsNullOrEmpty(page.AdditionalText?.ToString()))
            {
                richTextBlocks.Add(new ReactModels.RichTextBlock
                {
                    Title = page.AdditionalTextTitle,
                    HtmlId = nameof(page.AdditionalTextTitle),
                    Text = _richTextReactModelBuilder.BuildReactModel(page.AdditionalText, nameof(page.AdditionalText))
                });
            }
            return richTextBlocks;
        }

        private IList<ReactModels.RichTextBlock> BuildMoreTexts(ProposalPage page)
        {
            var texts = BuildTextList(
                BuildText(
                    page.DesignRequirements,
                    "DesignRequirementsTitle",
                    nameof(page.DesignRequirements)),
                BuildText(
                    page.AssessmentCriteriaText,
                    "AssessmentCriteriaTitle",
                    nameof(page.AssessmentCriteriaText))).ToList();

            var assessmentCriterionBlocks = _contentAreaReactModelBuilder.BuildReactModel(
                page.AssessmentCriteria, nameof(page.AssessmentCriteria));

            var assessmentCriteria = assessmentCriterionBlocks?.Blocks?
                .Select(x => x.ComponentData as ReactModels.RichTextBlock)
                .Where(x => x != null);

            if (assessmentCriteria != null)
                texts.AddRange(assessmentCriteria);

            texts.AddRange(
                BuildTextList(BuildText(
                    page.GeneralAssessmentCriteria,
                    null,
                    nameof(page.GeneralAssessmentCriteria)),
                BuildText(
                    page.Evaluation,
                    "EvaluationTitle",
                    nameof(page.Evaluation))));

            return texts.ToList();
        }

        private static IEnumerable<ReactModels.RichTextBlock> BuildTextList(
            params ReactModels.RichTextBlock[] richTextBlocks)
            => richTextBlocks.Where(x => x != null);

        private ReactModels.RichTextBlock BuildText(XhtmlString richText, string labelName, string onPageEditName)
            => !string.IsNullOrEmpty(richText?.ToString()) || _pageEditingAdapter.PageIsInEditMode()
                ? new ReactModels.RichTextBlock
                {
                    Title = GetLabel(labelName),
                    HtmlId = labelName,
                    Text = _richTextReactModelBuilder.BuildReactModel(richText, onPageEditName)
                }
                : null;

        private ReactModels.Timeline BuildTimeline(TimeLineBlock timeline)
            => timeline.TimeLineItems?.Count > 0
                ? _viewModelFactory.GetPartialViewModel(timeline)?.Model as ReactModels.Timeline ?? new ReactModels.Timeline()
                : null;

        private ReactModels.Link BuildApplyButton(ProposalPage page) =>
            page.ProposalState == ProposalState.Active
                ? new ReactModels.Link
                {
                    Text = GetLabel("ApplyButtonText"),
                    Url = string.Format(
                        _proposalPageConfiguration.MyPageUrl,
                        page.SpecificApplicationTypeId,
                        page.Language.Name)
                }
                : null;

        private ReactModels.ProposalContact BuildContact(ContactBlock contactBlock)
        {
            var contentReference = contactBlock
                    .Contacts
                    ?.FilteredItems
                    ?.FirstOrDefault()
                    ?.ContentLink;

            if (contentReference is null)
                return null;

            if (_contentLoader.TryGet<PersonPage>(contentReference, out var personPage))
                return _proposalContactReactModelBuilder.BuildReactModel(personPage, contactBlock.ShowPhoneNumber, contactBlock.ShowJobTitle);

            if (_contentLoader.TryGet<NonPersonContactBlock>(contentReference, out var nonPersonContactBlock))
                return _proposalContactReactModelBuilder.BuildReactModel(nonPersonContactBlock, contactBlock.ShowPhoneNumber);

            return null;
        }

        private ReactModels.ProposalData_Items BuildApplicationType(ApplicationTypePage applicationType)
        {
            if (applicationType.ArticlePageReference is null)
                return BuildProposalDataItem(GetLabel("ApplicationType"), applicationType.Name);

            var articleUrl = _urlResolver.GetUrl(applicationType.ArticlePageReference);
            return BuildProposalDataItem(GetLabel("ApplicationType"), applicationType.Name, articleUrl);
        }

        private ReactModels.ProposalData_Items BuildProjectOutline(
            ContentReference projectOutline,
            ProjectOutlineRequirement projectOutlineRequirement)
        {
            var page = GetPage<ProposalPage>(projectOutline);
            return BuildProposalDataItem(GetProjectOutlineLabel(projectOutlineRequirement), page.Name, _urlResolver.GetUrl(page));
        }

        private string GetProjectOutlineLabel(ProjectOutlineRequirement projectOutlineRequirement) =>
            projectOutlineRequirement == ProjectOutlineRequirement.Mandatory
                ? GetLabel("MandatoryProjectOutline")
                : GetLabel("ProjectOutline");

        private ReactModels.ProposalData_Items BuildChildProjectOutlines(IEnumerable<ContentReference> childProjectOutlines)
        {
            var links = childProjectOutlines.Select(x =>
            {
                var page = GetPage<ProposalPage>(x);
                return new ReactModels.Link
                {
                    Text = page.Name,
                    Url = _urlResolver.GetUrl(page)
                };
            }).ToList();

            return BuildProposalDataItem(GetLabel("ProjectOutlineFor"), links);
        }

        private ReactModels.ProposalData_Items BuildDeadline(DateTime? deadline, DeadlineType deadlineType) =>
            BuildProposalDataItem(
                GetLabel("Deadline"),
                deadline != null
                    ? deadline.Value.DeadlineFormattedWithDaylightSavingNameOrStandardName()
                    : deadlineType == DeadlineType.NotSet
                        ? GetLabel("NotSetDeadline")
                        : GetLabel("ContinuousDeadline")
                );

        private ReactModels.ProposalData_Items BuildProjectSize(int minApplicationAmount, int maxApplicationAmount)
            => BuildProposalDataItem(
                GetLabel("ProjectSize"),
                string.Format(GetLabel("ProjectSizeFormat"),
                    minApplicationAmount.ToLargeNumberFormat(),
                    maxApplicationAmount.ToLargeNumberFormat()));

        private ReactModels.ProposalData_Items BuildDuration(int minProjectLength, int maxProjectLength)
            => BuildProposalDataItem(GetLabel("Duration"),
                string.Format(GetLabel("DurationFormat"), minProjectLength, maxProjectLength));

        private ReactModels.ProposalData_Items BuildAmountItem(ProposalPage page) =>
            BuildProposalDataItem(
                GetLabel("Amount"),
                string.Format(GetLabel("NokFormat"), page.ProposalAmount));

        private ReactModels.ProposalData_Items BuildAmount(ProposalPage page)
        {
            var proposalsPage = _pageRepository.GetCurrentStartPage()?.ProposalsPage;
            var amountItem = BuildAmountItem(page);

            amountItem.Text = !string.IsNullOrEmpty(page.ProposalAmountText)
                ? $"{amountItem.Text}. {page.ProposalAmountText}"
                : amountItem.Text;

            amountItem.Links = !string.IsNullOrEmpty(page.ProposalAmountUrlText)
                               && proposalsPage != null
                               && page.ApplicationTypeReference != null
                               && !page.IsRff
                ? new List<ReactModels.Link>
                {
                    new ReactModels.Link
                    {
                        Text = page.ProposalAmountUrlText,
                        Url = $"{_urlResolver.GetUrl(proposalsPage)}?{QueryParameters.ApplicationTypes}={page.ApplicationTypeReference.ID}"
                    }
                }
                : null;

            return amountItem;
        }

        private ReactModels.ProposalData_Items BuildSubjects(List<SubjectBlock> subjects)
        {
            var subjectLinks = subjects
                .Select(c =>
                {
                    string anchorLink;
                    string name;

                    (name, anchorLink) = GetLinkFromSubject(c);

                    if (name is null || anchorLink is null)
                        return null;

                    return BuildLink(name, anchorLink);
                })
                .Where(x => x != null)
                .ToList();

            return BuildProposalDataItem(GetLabel("Subjects"), subjectLinks);

            (string, string) GetLinkFromSubject(SubjectBlock subjectBlock)
            {
                if (!string.IsNullOrEmpty(subjectBlock.SubjectName))
                {
                    return (subjectBlock.SubjectName, $"#{subjectBlock.SubjectCode}");
                }
                else
                {
                    var category = _categoryRepository.Get(subjectBlock.Subject);
                    if (category?.Parent.Name == CategoryConstants.SubjectRoot)
                        return (_categoryLocalizationProvider.GetCategoryName(category.ID), $"#{category.Name}");
                    else
                        return (null, null);
                }
            }
        }

        private ReactModels.ProposalData_Items BuildTargetGroups(CategoryList categories)
        {
            var targetGroups = categories.Select(c =>
            {
                var category = _categoryRepository.Get(c);
                return category.Parent.Name == CategoryConstants.TargetGroupRoot
                    ? _categoryLocalizationProvider.GetCategoryName(category.ID)
                    : null;
            }).Where(x => x != null);

            return BuildProposalDataItem(GetLabel("TargetGroups"), string.Join(", ", targetGroups));
        }

        private ReactModels.ProposalData_Items BuildNextProposalLink(ContentReference upcomingProposal)
        {
            var page = _contentRepository.Get<ProposalPage>(upcomingProposal);
            var url = _urlResolver.GetUrl(page);

            return BuildProposalDataItem(
                GetLabel("UpcomingProposal"),
                page.DeadlineComputed?.ToDisplayDate().ToString(DateTimeFormats.ProposalDeadlineDate),
                url);
        }

        private static ReactModels.ProposalData_Items BuildProposalDataItem(string label, string text)
            => new ReactModels.ProposalData_Items
            {
                Label = label,
                Text = text
            };

        private static ReactModels.ProposalData_Items BuildProposalDataItem(string label, string text, string url)
            => new ReactModels.ProposalData_Items
            {
                Label = label,
                Links = new List<ReactModels.Link>
                {
                    new ReactModels.Link
                    {
                        Text = text,
                        Url = url
                    }
                }
            };

        private static ReactModels.ProposalData_Items BuildProposalDataItem(string label, IList<ReactModels.Link> links)
            => new ReactModels.ProposalData_Items
            {
                Label = label,
                Links = links
            };

        private ReactModels.StickyMenu BuildMenu(ProposalPage page)
            => new ReactModels.StickyMenu
            {
                Title = GetLabel("ShortcutsTitle"),
                Accordion = new ReactModels.Accordion
                {
                    Guid = Guid.NewGuid().ToString(),
                    CollapseLabel = GetLabel("CollapseLabel"),
                    ExpandLabel = GetLabel("ExpandLabel"),
                    InitiallyOpen = true
                },
                NavGroups = BuildNavigationGroups(page).ToList()
            };

        private IEnumerable<ReactModels.StickyMenu_NavGroups> BuildNavigationGroups(ProposalPage page)
        {
            var aboutGroup = BuildNavigationGroup(
                BuildTitleLink("AboutNavigationTitle"),
                BuildAboutLinks(page).ToList());

            if (aboutGroup != null)
                yield return aboutGroup;

            var subjectsGroup = BuildNavigationGroup(
                BuildTitleLink("SubjectsNavigationTitle"),
                BuildSubjectsLinks(page).ToList());

            if (subjectsGroup != null)
                yield return subjectsGroup;

            var practicalInfoGroup = BuildNavigationGroup(
                BuildTitleLink("PracticalInfoNavigationTitle"),
                BuildPracticalInfoLinks(page).ToList());

            if (practicalInfoGroup != null)
                yield return practicalInfoGroup;

            ReactModels.Link BuildTitleLink(string labelName) =>
                BuildLink(GetLabel(labelName), $"#{labelName}");
        }

        private static ReactModels.StickyMenu_NavGroups BuildNavigationGroup(ReactModels.Link titleLink, List<ReactModels.StickyMenuItem> links) =>
            links.Any()
                ? new ReactModels.StickyMenu_NavGroups
                {
                    TitleLink = titleLink,
                    Links = new ReactModels.StickyMenu_NavGroups_Links
                    {
                        Items = links
                    }
                }
                : null;

        private IEnumerable<ReactModels.StickyMenuItem> BuildAboutLinks(ProposalPage page)
        {
            yield return BuildStickyMenuItem("IntendedApplicantsTitle");
            yield return BuildStickyMenuItem("ProjectParticipantsTitle");
            yield return BuildStickyMenuItem("SubjectTitle");

            if (!string.IsNullOrEmpty(page.ArchivingOfDataText?.ToString()))
                yield return BuildStickyMenuItem("ArchivingOfDataTitle");

            if (!string.IsNullOrEmpty(page.AdditionalText?.ToString()))
                yield return BuildStickyMenuItemLink(page.AdditionalTextTitle, $"#{nameof(page.AdditionalTextTitle)}");
        }

        private IEnumerable<ReactModels.StickyMenuItem> BuildSubjectsLinks(ProposalPage page)
        {
            if (page.SubjectBlocks is null)
                yield break;

            foreach (var item in page.SubjectBlocks.FilteredItems)
            {
                var block = _contentLoader.Get<SubjectBlock>(item.ContentLink);
                if (block is null)
                    continue;

                string name = block.SubjectName;
                string linkAnchor = block.SubjectCode;
                if (string.IsNullOrEmpty(block.SubjectName))
                {
                    var category = _categoryRepository.Get(block.Subject);
                    if (category is null)
                        continue;

                    name = _categoryLocalizationProvider.GetCategoryName(category.ID);
                    linkAnchor = category.Name;
                }

                var subjectPriorityBlocks = block.Priorities?.FilteredItems.Select(x => _contentLoader.Get<SubjectPriorityBlock>(x.ContentLink));
                yield return BuildStickyMenuItemLink(name, $"#{linkAnchor}", subjectPriorityBlocks);
            }
        }

        private IEnumerable<ReactModels.StickyMenuItem> BuildPracticalInfoLinks(ProposalPage page)
        {
            if (!string.IsNullOrEmpty(page.DesignRequirements?.ToString()))
                yield return BuildStickyMenuItem("DesignRequirementsTitle");
            if (!string.IsNullOrEmpty(page.AssessmentCriteriaText?.ToString()))
                yield return BuildStickyMenuItem("AssessmentCriteriaTitle");
            if (!string.IsNullOrEmpty(page.Evaluation?.ToString()))
                yield return BuildStickyMenuItem("EvaluationTitle");
        }

        private ReactModels.StickyMenuItem BuildStickyMenuItem(string labelName) =>
            BuildStickyMenuItemLink(GetLabel(labelName), $"#{labelName}");

        private static ReactModels.Link BuildLink(string text, string url) =>
            new ReactModels.Link
            {
                Text = text,
                Url = url
            };

        private ReactModels.StickyMenuItem BuildStickyMenuItemLink(string text, string url, IEnumerable<SubjectPriorityBlock> priorityBlocks = null)
        {
            var stickyMenuItem = new ReactModels.StickyMenuItem
            {
                Link = new ReactModels.StickyMenuItem_Link
                {
                    Text = text,
                    Url = url
                },
            };

            if (priorityBlocks != null)
            {
                stickyMenuItem.SubLinks = new ReactModels.StickyMenuItem_SubLinks
                {
                    Accordion = new ReactModels.Accordion
                    {
                        CollapseLabel = GetLabel("CollapseLabel"),
                        ExpandLabel = GetLabel("ExpandLabel"),
                        Guid = Guid.NewGuid().ToString(),
                        InitiallyOpen = false
                    },
                    Links = new ReactModels.StickyMenuItem_SubLinks_Links
                    {
                        Items = priorityBlocks.Select(x => new ReactModels.StickyMenuItemSublink { Text = x.Title, Url = $"#{x.AnchorId}" }).ToList()
                    }
                };
            }

            return stickyMenuItem;
        }

        private string GetLabel(string key)
            => _localizationProvider.GetLabel(nameof(ProposalPage), key);

        private T GetPage<T>(ContentReference page) where T : PageData
        {
            if (page is null)
                return null;

            var currentLanguage = ContentLanguage.PreferredCulture.Name;
            return _contentRepository.Get<T>(page, new CultureInfo(currentLanguage));
        }
    }
}