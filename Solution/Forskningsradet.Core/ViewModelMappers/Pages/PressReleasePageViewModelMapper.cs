﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Routing;
using EPiServer.Globalization;
using EPiServer.Web.Routing;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.ViewModels.Contracts;
using Forskningsradet.Core.Models.ViewModels.Pages;
using Forskningsradet.Core.Services;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.ViewModelMappers.Pages
{
    public class PressReleasePageViewModelMapper : BasePageViewModelMapper<PressReleasePage>
    {
        private readonly IUrlResolver _urlResolver;
        private readonly IOptionsModalReactModelBuilder _optionsModalReactModelBuilder;
        private readonly ILocalizationProvider _localizationProvider;

        public PressReleasePageViewModelMapper(
            IUrlResolver urlResolver,
            IOptionsModalReactModelBuilder optionsModalReactModelBuilder,
            ILocalizationProvider localizationProvider)
        {
            _urlResolver = urlResolver;
            _optionsModalReactModelBuilder = optionsModalReactModelBuilder;
            _localizationProvider = localizationProvider;
        }

        protected override IReactViewModel GetViewModelForPageInternal(PressReleasePage page) =>
            new ReactPageViewModel(page, BuildReactModel(page));

        private ReactModels.ArticlePage BuildReactModel(PressReleasePage page) =>
            new ReactModels.ArticlePage
            {
                Title = page.Title,
                Ingress = page.Lead,
                Header = BuildArticleHeader(page),
                Content = new ReactModels.RichText
                {
                    Blocks = BuildPressRelease(page).ToList()
                }
            };

        private ReactModels.ArticleHeader BuildArticleHeader(PressReleasePage currentPage) =>
            new ReactModels.ArticleHeader
            {
                Accordion = new ReactModels.Accordion
                {
                    CollapseLabel = GetLabel("Collapse"),
                    ExpandLabel = GetLabel("Expand"),
                    Guid = Guid.NewGuid().ToString()
                },
                Byline = BuildByline(currentPage),
                Download = BuildDownloadLink(currentPage),
                Share = _optionsModalReactModelBuilder.BuildShareContent(currentPage)
            };

        private ReactModels.Byline BuildByline(PressReleasePage page) =>
            new ReactModels.Byline
            {
                Items = new List<ReactModels.Byline_Items>
                    {
                        new ReactModels.Byline_Items
                        {
                            Text = GetLabel("PressRelease")
                        },
                        page.ExternalPublishedAt is null ? null
                        : new ReactModels.Byline_Items
                        {
                            Text = $"{GetLabel("Published")} {page.ExternalPublishedAt?.ToString(DateTimeFormats.TimelineDate)}"
                        }
                    }
            };

        private ReactModels.Link BuildDownloadLink(PressReleasePage page) =>
            new ReactModels.Link
            {
                Url = BuildDownloadUrl(page),
                Text = GetLabel("Download")
            };

        private string BuildDownloadUrl(PressReleasePage page) =>
            _urlResolver.GetUrl(
                page.ContentLink,
                ContentLanguage.PreferredCulture.Name,
                new UrlResolverArguments
                {
                    RouteValues = new RouteValueDictionary(new { action = "Download" })
                });

        private static IEnumerable<ReactModels.ContentAreaItem> BuildPressRelease(PressReleasePage pressRelease)
        {
            if (!string.IsNullOrEmpty(pressRelease.ImageUrl))
            {
                yield return new ReactModels.ContentAreaItem
                {
                    Id = PressReleaseConstants.ImageContentAreaItemId,
                    ComponentName = nameof(ReactModels.ImageWithCaption),
                    ComponentData = new ReactModels.ImageWithCaption
                    {
                        Caption = pressRelease.ImageCaption,
                        Image = new ReactModels.Image
                        {
                            Src = pressRelease.ImageUrl,
                            Alt = pressRelease.ImageCaption
                        }
                    }
                };
            }

            yield return new ReactModels.ContentAreaItem
            {
                Id = PressReleaseConstants.BodyContentAreaItemId,
                ComponentName = nameof(ReactModels.HtmlString),
                ComponentData = new ReactModels.HtmlString
                {
                    Text = pressRelease.Body
                }
            };
        }

        private string GetLabel(string key) =>
            _localizationProvider.GetLabel(nameof(PressReleaseListPage), key);
    }
}
