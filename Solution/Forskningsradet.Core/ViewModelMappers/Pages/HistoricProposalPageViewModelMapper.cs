﻿using System.Collections.Generic;
using EPiServer;
using EPiServer.Core;
using EPiServer.Web.Routing;
using Forskningsradet.Core.Facades.Contracts;
using Forskningsradet.Core.Factories;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.ViewModels.Contracts;
using Forskningsradet.Core.Models.ViewModels.Pages;
using Forskningsradet.Core.Services;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.ViewModelMappers.Pages
{
    public class HistoricProposalPageViewModelMapper : BasePageViewModelMapper<HistoricProposalPage>
    {
        private readonly IContentLoader _contentLoader;
        private readonly IViewModelFactory _viewModelFactory;
        private readonly IUrlResolver _urlResolver;
        private readonly ILocalizationProvider _localizationProvider;
        private readonly IPageEditingAdapter _pageEditingAdapter;

        public HistoricProposalPageViewModelMapper(
            IContentLoader contentLoader,
            IViewModelFactory viewModelFactory,
            IUrlResolver urlResolver,
            ILocalizationProvider localizationProvider,
            IPageEditingAdapter pageEditingAdapter)
        {
            _contentLoader = contentLoader;
            _viewModelFactory = viewModelFactory;
            _urlResolver = urlResolver;
            _localizationProvider = localizationProvider;
            _pageEditingAdapter = pageEditingAdapter;
        }

        protected override IReactViewModel GetViewModelForPageInternal(HistoricProposalPage currentPage) =>
            new ReactPageViewModel(new InformationArticlePage(), BuildHistoricProposalPageReactModel(currentPage), BuildFullRefreshProperties());

        private ReactModels.ArticlePage BuildHistoricProposalPageReactModel(HistoricProposalPage page)
        {
            var applicationResult = !ContentReference.IsNullOrEmpty(page.ApplicationResultPage)
                ? _contentLoader.Get<ApplicationResultPage>(page.ApplicationResultPage)
                : null;

            return new ReactModels.ArticlePage
            {
                Title = page.ApplicationResultTitle ?? applicationResult?.PageName ?? page.PageName,
                Sidebar = BuildSideBar(page),
                Content = new ReactModels.RichText
                {
                    Blocks = new List<ReactModels.ContentAreaItem>
                    {
                        new ReactModels.ContentAreaItem
                        {
                            ComponentName = nameof(ReactModels.DescriptionListAndTables),
                            ComponentData = BuildApplicationResultContent(applicationResult)
                        }
                    },
                    OnPageEditing = _pageEditingAdapter.PageIsInEditMode()
                        ? new ReactModels.ContentArea_OnPageEditing
                        {
                            Name = nameof(page.ApplicationResultPage)
                        }
                        : null
                },
                OnPageEditing = _pageEditingAdapter.PageIsInEditMode()
                    ? new ReactModels.ArticlePage_OnPageEditing { Title = nameof(page.ApplicationResultTitle) }
                    : null
            };
        }

        private ReactModels.ContentArea BuildSideBar(HistoricProposalPage page) =>
            new ReactModels.ContentArea
            {
                Blocks = new List<ReactModels.ContentAreaItem>
                {
                    new ReactModels.ContentAreaItem
                    {
                        ComponentName = nameof(ReactModels.LinkListBlock),
                        ComponentData = new ReactModels.LinkListBlock
                        {
                            Title = GetLabel("Download"),
                            Links = new ReactModels.LinkList
                            {
                                Items = new List<ReactModels.Link>
                                {
                                    new ReactModels.Link { Url = _urlResolver.GetUrl(page) + "/Download", Text = page.PageName, OnPageEditing = _pageEditingAdapter.PageIsInEditMode() ? nameof(page.Attachment) : null}
                                }
                            }
                        }
                    }
                }
            };

        private ReactModels.DescriptionListAndTables BuildApplicationResultContent(ApplicationResultPage page) =>
            page != null && _viewModelFactory.GetPartialViewModel(page)?.Model is ReactModels.DescriptionListAndTables content
                ? content
                : new ReactModels.DescriptionListAndTables();

        private string[] BuildFullRefreshProperties() =>
            new[]
            {
                nameof(HistoricProposalPage.PageName),
                nameof(HistoricProposalPage.ApplicationResultTitle),
                nameof(HistoricProposalPage.ApplicationResultPage)
            };

        private string GetLabel(string key) =>
            _localizationProvider.GetLabel(nameof(HistoricProposalPage), key);
    }
}