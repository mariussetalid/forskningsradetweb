﻿using System.Collections.Generic;
using System.Linq;
using EPiServer;
using EPiServer.Core;
using EPiServer.SpecializedProperties;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Factories;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.ViewModels.Contracts;
using Forskningsradet.Core.Models.ViewModels.Pages;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.ViewModelMappers.Pages
{
    public class FrontPageViewModelMapper : BasePageViewModelMapper<FrontPage>
    {
        private readonly IContentAreaReactModelBuilder _contentAreaReactModelBuilder;
        private readonly ILinkListReactModelBuilder _linkListReactModelBuilder;
        private readonly IContentLoader _contentLoader;
        private readonly IViewModelFactory _viewModelFactory;

        public FrontPageViewModelMapper(
            IContentAreaReactModelBuilder contentAreaReactModelBuilder,
            IContentLoader contentLoader,
            IViewModelFactory viewModelFactory,
            ILinkListReactModelBuilder linkListReactModelBuilder)
        {
            _contentAreaReactModelBuilder = contentAreaReactModelBuilder;
            _contentLoader = contentLoader;
            _viewModelFactory = viewModelFactory;
            _linkListReactModelBuilder = linkListReactModelBuilder;
        }

        protected override IReactViewModel GetViewModelForPageInternal(FrontPage currentPage)
        {
            var reactComponent = BuildReactComponent(currentPage);
            return new ReactPageViewModel(currentPage, reactComponent, BuildFullRefreshProperties());
        }

        private ReactModels.Frontpage BuildReactComponent(FrontPage page) =>
            new ReactModels.Frontpage
            {
                Header = BuildPageHeader(page),
                Content = _contentAreaReactModelBuilder.BuildReactModel(page.MainContent, nameof(page.MainContent)),
                LinkList = IsNullOrEmpty(page.LinkList) ? null : _linkListReactModelBuilder.BuildReactModel(page.LinkList, nameof(page.LinkList))
            };

        private ReactModels.FrontpageHeader BuildPageHeader(FrontPage page)
        {
            var emptyHeader = new ReactModels.FrontpageHeader { Links = new List<ReactModels.Link>() };
            if (page.TopContent?.FilteredItems.Any() != true)
                return emptyHeader;

            var headerBlock = _contentLoader.Get<IContentData>(page.TopContent.FilteredItems.First().ContentLink);
            return _viewModelFactory.GetPartialViewModel(headerBlock)?.Model as ReactModels.FrontpageHeader ?? emptyHeader;
        }

        private bool IsNullOrEmpty(LinkItemCollection linkList) =>
            linkList is null || linkList.Any() == false;

        private string[] BuildFullRefreshProperties() =>
            new[]
            {
                nameof(FrontPage.TopContent),
            };
    }
}
