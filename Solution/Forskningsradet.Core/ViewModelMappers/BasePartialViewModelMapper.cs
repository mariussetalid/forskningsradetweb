﻿using EPiServer.Core;
using Forskningsradet.Core.Models.ViewModels.Contracts;

namespace Forskningsradet.Core.ViewModelMappers
{
    public abstract class BasePartialViewModelMapper<T> : IPartialViewModelMapper where T : IContentData
    {
        public IReactViewModel GetPartialViewModel(IContentData currentContent)
            => currentContent is T content
                ? GetViewModelForPartialInternal(content)
                : null;

        protected abstract IReactViewModel GetViewModelForPartialInternal(T currentContent);
    }
}