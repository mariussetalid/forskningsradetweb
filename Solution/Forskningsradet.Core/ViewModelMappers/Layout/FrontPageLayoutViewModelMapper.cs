﻿using System.Linq;
using EPiServer;
using EPiServer.Core;
using EPiServer.Web.Routing;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Configuration;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Services;
using Forskningsradet.Core.Services.Contracts;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.ViewModelMappers.Layout
{
    public class FrontPageLayoutViewModelMapper : BaseLayoutViewModelMapper<FrontPage>
    {
        public FrontPageLayoutViewModelMapper(
            IUrlResolver urlResolver,
            IUrlCheckingService urlCheckingService,
            IPageStructureService pageStructureService,
            IContentLoader contentLoader,
            IContentFilterService contentFilterService,
            IFooterReactModelBuilder footerReactModelBuilder,
            IBreadcrumbsReactModelBuilder breadcrumbsReactModelBuilder,
            IGoogleTagManagerConfiguration googleTagManagerConfiguration,
            ILocalizationProvider localizationProvider,
            IMessageReactModelBuilder messageReactModelBuilder,
            IContentRepository contentRepository,
            IPublishedStateAssessor publishedStateAssessor)
        : base(
            urlResolver,
            urlCheckingService,
            pageStructureService,
            contentLoader,
            contentFilterService,
            footerReactModelBuilder,
            breadcrumbsReactModelBuilder,
            googleTagManagerConfiguration,
            localizationProvider,
            messageReactModelBuilder,
            contentRepository,
            publishedStateAssessor
            )
        {
        }

        protected override ReactModels.Header BuildHeader(FrontPage frontPage, PageReference currentPageReference) =>
            new ReactModels.Header
            {
                GlobalSearch = frontPage.SearchPage is null ? null : BuildGlobalSearchModel(frontPage.SearchPage),
                LinkToHome = BuildLinkToHome(frontPage),
                LinkList = BuildLinkList(frontPage, currentPageReference).ToList(),
                PriorityLink = BuildPriorityLink(frontPage),
                MenuText = GetLabel("MenuLabel"),
                Menu = frontPage.MainMenuRoot is null ? null : BuildMenuViewModel(frontPage.MainMenuRoot),
                Logo = BuildLogoViewModel(frontPage),
                TabMenu = BuildTabMenu(),
                Satellite = null
            };
    }
}
