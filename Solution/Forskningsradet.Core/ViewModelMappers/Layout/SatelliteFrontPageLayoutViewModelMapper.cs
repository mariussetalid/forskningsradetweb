﻿using System.Linq;
using EPiServer;
using EPiServer.Core;
using EPiServer.Web.Routing;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Configuration;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Repositories.Contracts;
using Forskningsradet.Core.Services;
using Forskningsradet.Core.Services.Contracts;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.ViewModelMappers.Layout
{
    public class SatelliteFrontPageLayoutViewModelMapper : BaseLayoutViewModelMapper<SatelliteFrontPage>
    {
        private readonly IPageRepository _pageRepository;

        public SatelliteFrontPageLayoutViewModelMapper(
            IUrlResolver urlResolver,
            IUrlCheckingService urlCheckingService,
            IPageStructureService pageStructureService,
            IContentLoader contentLoader,
            IContentFilterService contentFilterService,
            IFooterReactModelBuilder footerReactModelBuilder,
            IBreadcrumbsReactModelBuilder breadcrumbsReactModelBuilder,
            IGoogleTagManagerConfiguration googleTagManagerConfiguration,
            ILocalizationProvider localizationProvider,
            IPageRepository pageRepository,
            IMessageReactModelBuilder messageReactModelBuilder,
            IContentRepository contentRepository,
            IPublishedStateAssessor publishedStateAssessor)
        : base(
            urlResolver,
            urlCheckingService,
            pageStructureService,
            contentLoader,
            contentFilterService,
            footerReactModelBuilder,
            breadcrumbsReactModelBuilder,
            googleTagManagerConfiguration,
            localizationProvider,
            messageReactModelBuilder,
            contentRepository,
            publishedStateAssessor
            ) => _pageRepository = pageRepository;

        protected override ReactModels.Header BuildHeader(SatelliteFrontPage frontPage, PageReference currentPageReference) =>
            new ReactModels.Header
            {
                GlobalSearch = frontPage.SearchPage is null ? null : BuildGlobalSearchModel(frontPage.SearchPage),
                LinkToHome = BuildLinkToHome(frontPage),
                LinkList = BuildLinkList(frontPage, currentPageReference).ToList(),
                PriorityLink = BuildPriorityLink(frontPage),
                MenuText = GetLabel("MenuLabel"),
                Menu = frontPage.MainMenuRoot is null ? null : BuildMenuViewModel(frontPage.MainMenuRoot),
                Logo = BuildLogoViewModel(frontPage),
                TabMenu = BuildTabMenu(),
                Satellite = BuildSatelliteHeader(frontPage)
            };

        private ReactModels.Header_Satellite BuildSatelliteHeader(SatelliteFrontPage frontPage)
        {
            var startPage = _pageRepository.GetCurrentStartPage() ?? _pageRepository.GetStartPageFromWildcardHost();
            var startPageUrl = startPage is null ? "#" : UrlResolver.GetUrl(startPage);
            return new ReactModels.Header_Satellite
            {
                Header = new ReactModels.SimpleHeader
                {
                    Url = startPageUrl,
                    Text = GetLabel("PlanetName")
                },
                IsVisible = frontPage.ShowTopBanner
            };
        }
    }
}
