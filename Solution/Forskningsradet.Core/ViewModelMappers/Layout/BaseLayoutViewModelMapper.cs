﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using EPiServer;
using EPiServer.Cms.Shell;
using EPiServer.Core;
using EPiServer.Web.Routing;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Configuration;
using Forskningsradet.Core.Extensions;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ContentModels.Media;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.ViewModels.Shared;
using Forskningsradet.Core.Services;
using Forskningsradet.Core.Services.Contracts;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Core.ViewModelMappers.Layout
{
    public abstract class BaseLayoutViewModelMapper<T> : ILayoutViewModelMapper where T : FrontPageBase
    {
        protected readonly IUrlResolver UrlResolver;
        private readonly IUrlCheckingService _urlCheckingService;
        private readonly IPageStructureService _pageStructureService;
        private readonly IContentLoader _contentLoader;
        private readonly IContentFilterService _contentFilterService;
        private readonly IFooterReactModelBuilder _footerReactModelBuilder;
        private readonly IBreadcrumbsReactModelBuilder _breadcrumbsReactModelBuilder;
        private readonly IGoogleTagManagerConfiguration _googleTagManagerConfiguration;
        private readonly ILocalizationProvider _localizationProvider;
        private readonly IMessageReactModelBuilder _messageReactModelBuilder;
        private readonly IContentRepository _contentRepository;
        private readonly IPublishedStateAssessor _publishedStateAssessor;

        protected BaseLayoutViewModelMapper(
            IUrlResolver urlResolver,
            IUrlCheckingService urlCheckingService,
            IPageStructureService pageStructureService,
            IContentLoader contentLoader,
            IContentFilterService contentFilterService,
            IFooterReactModelBuilder footerReactModelBuilder,
            IBreadcrumbsReactModelBuilder breadcrumbsReactModelBuilder,
            IGoogleTagManagerConfiguration googleTagManagerConfiguration,
            ILocalizationProvider localizationProvider,
            IMessageReactModelBuilder messageReactModelBuilder,
            IContentRepository contentRepository,
            IPublishedStateAssessor publishedStateAssessor)
        {
            UrlResolver = urlResolver;
            _urlCheckingService = urlCheckingService;
            _pageStructureService = pageStructureService;
            _contentLoader = contentLoader;
            _contentFilterService = contentFilterService;
            _footerReactModelBuilder = footerReactModelBuilder;
            _breadcrumbsReactModelBuilder = breadcrumbsReactModelBuilder;
            _googleTagManagerConfiguration = googleTagManagerConfiguration;
            _localizationProvider = localizationProvider;
            _messageReactModelBuilder = messageReactModelBuilder;
            _contentRepository = contentRepository;
            _publishedStateAssessor = publishedStateAssessor;
        }

        public LayoutViewModel GetViewModel(FrontPageBase closestFrontPage, PageReference currentPageReference) =>
            closestFrontPage is T frontPage
                ? BuildLayoutViewModel(frontPage, _contentLoader.TryGet(currentPageReference, out PageData currentPage) ? currentPage : null, currentPageReference)
                : null;

        protected LayoutViewModel BuildLayoutViewModel(T currentContent, PageData currentPage, PageReference currentPageReference) =>
            new LayoutViewModel
            {
                Header = BuildHeader(currentContent, currentPageReference),
                Breadcrumbs = _breadcrumbsReactModelBuilder.BuildReactModel(currentContent, currentPageReference),
                GlobalMessages = BuildGlobalMessages(currentContent, currentPageReference),
                Footer = BuildFooter(currentContent),
                HideFooter = HideFooter(currentPage),
                EnableTrackingScripts = EnableTrackingScripts(),
                EnableApsisMarketingAutomationScript = GetApsisMarketingAutomationScriptEnabled(currentContent),
                GoogleTagManagerId = GetGoogleTagManagerId(currentContent) ?? _googleTagManagerConfiguration.GoogleTagManagerId,
                CanonicalUrl = currentPageReference.GetExternalUrl(UrlResolver),
                ColorTheme = GetColorTheme(currentContent),
                FaviconIcoOverride = string.IsNullOrWhiteSpace(GetImageUrl(currentContent.FaviconIcoOverride)) ? null : GetImageUrl(currentContent.FaviconIcoOverride),
                FaviconPng16Override = string.IsNullOrWhiteSpace(GetImageUrl(currentContent.FaviconPng16Override)) ? null : GetImageUrl(currentContent.FaviconPng16Override),
                FaviconPng32Override = string.IsNullOrWhiteSpace(GetImageUrl(currentContent.FaviconPng32Override)) ? null : GetImageUrl(currentContent.FaviconPng32Override),
                FaviconPng96Override = string.IsNullOrWhiteSpace(GetImageUrl(currentContent.FaviconPng96Override)) ? null : GetImageUrl(currentContent.FaviconPng96Override),
                FaviconPng150Override = string.IsNullOrWhiteSpace(GetImageUrl(currentContent.FaviconPng150Override)) ? null : GetImageUrl(currentContent.FaviconPng150Override),
                FaviconPng180Override = string.IsNullOrWhiteSpace(GetImageUrl(currentContent.FaviconPng180Override)) ? null : GetImageUrl(currentContent.FaviconPng180Override),
                FaviconPng192Override = string.IsNullOrWhiteSpace(GetImageUrl(currentContent.FaviconPng192Override)) ? null : GetImageUrl(currentContent.FaviconPng192Override),
                FaviconPng512Override = string.IsNullOrWhiteSpace(GetImageUrl(currentContent.FaviconPng512Override)) ? null : GetImageUrl(currentContent.FaviconPng512Override),
                FaviconSvgOverride = string.IsNullOrWhiteSpace(GetImageUrl(currentContent.FaviconSvgOverride)) ? null : GetImageUrl(currentContent.FaviconSvgOverride),
                UseFallbackIcons = currentContent.UseFallbackIcons
            };

        private ColorTheme GetColorTheme(FrontPageBase currentContent)
        {
            if (currentContent is SatelliteFrontPage satelliteFrontPage)
                return satelliteFrontPage.ColorTheme;

            return ColorTheme.Forskningsradet;
        }

        private bool HideFooter(PageData currentPage) =>
            currentPage is NpMethodPage;

        private bool EnableTrackingScripts()
        {
            var environment = ConfigurationManager.AppSettings["episerver:EnvironmentName"];

            if (new[] { "Integration", "Preproduction", "Production" }.Contains(environment))
            {
                // Tracking scripts are enabled in all DXC environments
                return true;
            }

            return false;
        }

        private static string GetGoogleTagManagerId(T closestFrontPage) =>
            closestFrontPage.GoogleTagManagerId is var gtmId && !string.IsNullOrEmpty(gtmId)
                ? gtmId
                : null;

        private static bool GetApsisMarketingAutomationScriptEnabled(T closestFrontPage) =>
            closestFrontPage.UseApsisMarketingAutomationScript;

        protected abstract ReactModels.Header BuildHeader(T closestFrontPage, PageReference currentPageReference);

        protected IList<ReactModels.Message> BuildGlobalMessages(FrontPageBase closestFrontPage, PageReference currentPageReference)
        {
            var blocks = GetBlocks(closestFrontPage, currentPageReference);
            return blocks
                .Select(x => _messageReactModelBuilder.BuildReactModel(x.Text?.ToString(), null, x.Theme))
                .ToList();
        }

        protected ReactModels.Footer BuildFooter(T closestFrontPage) =>
            _footerReactModelBuilder.BuildReactModel(closestFrontPage.Footer, nameof(closestFrontPage.Footer));

        protected ReactModels.Link BuildLinkToHome(FrontPageBase settingsPage) =>
            new ReactModels.Link
            {
                Url = UrlResolver.GetUrl(settingsPage.ContentLink),
                Text = GetLabel("HomeButtonLabel")
            };

        protected IEnumerable<ReactModels.Link> BuildLinkList(FrontPageBase frontPageBase, PageReference currentPageReference)
        {
            if (TryGetLanguageLink(currentPageReference) is ReactModels.Link languageLink)
                yield return languageLink;

            if (frontPageBase.HeaderLinks?.Any() == true)
                foreach (var headerLink in frontPageBase.HeaderLinks)
                    yield return new ReactModels.Link
                    {
                        Url = UrlResolver.GetUrl(headerLink.Href),
                        Text = headerLink.Text,
                        IsExternal = headerLink.IsExternal(_urlCheckingService)
                    };
        }

        protected ReactModels.Link BuildPriorityLink(FrontPageBase frontPageBase) =>
            frontPageBase.LoginUrl is null ? null
            : new ReactModels.Link
            {
                Url = frontPageBase.LoginUrl.GetUrl(UrlResolver),
                Text = !string.IsNullOrEmpty(frontPageBase.LoginUrlText) ? frontPageBase.LoginUrlText : GetLabel("Login"),
                IsExternal = frontPageBase.LoginUrlIsExternal
            };

        protected ReactModels.GlobalSearch BuildGlobalSearchModel(PageReference searchPageReference)
        {
            var searchUrl = !ContentReference.IsNullOrEmpty(searchPageReference)
                ? UrlResolver.GetUrl(searchPageReference)
                : "/";
            return new ReactModels.GlobalSearch
            {
                Button = new ReactModels.Button { Text = GetLabel("Search") },
                Input = new ReactModels.TextInput { Name = QueryParameters.SearchQuery, Placeholder = GetLabel("SearchPlaceholder") },
                CloseButtonText = GetLabel("CloseSearchButtonLabel"),
                Form = new ReactModels.Form
                {
                    Endpoint = searchUrl
                },
                Endpoint = searchUrl
            };
        }

        protected ReactModels.Link TryGetLanguageLink(ContentReference currentPageLink) =>
            !ContentReference.IsNullOrEmpty(currentPageLink) &&
            _contentRepository.GetLanguageBranches<PageData>(currentPageLink)
                .Where(_publishedStateAssessor.IsPublished)
                .FirstOrDefault(x => !Equals(x.Language, CultureInfo.CurrentUICulture)) is PageData secondLanguagePageVersion
                ? new ReactModels.Link
                {
                    Url = UrlResolver.GetUrl(secondLanguagePageVersion),
                    Text = GetLabel(secondLanguagePageVersion.Language.Name == "en" ? "English" : "Norwegian")
                }
                : null;

        protected ReactModels.Header_Logo BuildLogoViewModel(FrontPageBase frontPage)
        {
            var mainLogoIsNotSetWhichMustMeanThatTheyDoNotHaveAnyLogoReady = frontPage.MainLogo is null;
            if (mainLogoIsNotSetWhichMustMeanThatTheyDoNotHaveAnyLogoReady)
                return null;
            return frontPage.CultureSpecificLogo is null && frontPage.CultureSpecificWhiteLogo is null
                ? null
                : new ReactModels.Header_Logo
                {
                    Default = GetImageUrl(frontPage.CultureSpecificLogo),
                    White = GetImageUrl(frontPage.CultureSpecificWhiteLogo)
                };
        }

        protected ReactModels.TabMenu BuildTabMenu() =>
            new ReactModels.TabMenu
            {
                OpenMenuLabel = GetLabel("OpenMenu"),
                MainContentLink = new ReactModels.Link
                {
                    Text = GetLabel("GoToContent"),
                    Url = "#main"
                }
            };

        protected ReactModels.Menu BuildMenuViewModel(PageReference menuRoot) =>
            new ReactModels.Menu
            {
                CloseButtonText = GetLabel("CloseButtonText"),
                NavigationGroups = menuRoot is null
                    ? new List<ReactModels.NavigationGroup>()
                    : BuildMenuGroupsFrom(menuRoot).ToList()
            };

        private IEnumerable<ReactModels.NavigationGroup> BuildMenuGroupsFrom(PageReference menuRoot) =>
            _pageStructureService.GetFilteredChildPages<MenuStructurePage>(menuRoot, true)
                .Select(page => new ReactModels.NavigationGroup
                {
                    Title = _pageStructureService.GetMenuPageTitle(page),
                    Items = BuildMenuItemsFrom(page.PageLink).ToList()
                });

        private IEnumerable<ReactModels.NavigationGroup_Items> BuildMenuItemsFrom(PageReference menuElement) =>
            _pageStructureService.GetFilteredChildPages<MenuStructurePage>(menuElement, true)
                .Select(page => new ReactModels.NavigationGroup_Items
                {
                    LinkList = BuildNavigationListFrom(page),
                    Link = BuildLinkListFrom(page.PageLink).Any()
                        ? null
                        : GetLink(page)
                });

        private ReactModels.NavigationList BuildNavigationListFrom(MenuStructurePage page) =>
            new ReactModels.NavigationList
            {
                Title = _pageStructureService.GetMenuPageTitle(page),
                Accordion = new ReactModels.Accordion
                {
                    CollapseLabel = GetLabel("HideLinksButtonLabel"),
                    ExpandLabel = GetLabel("ShowLinksButtonLabel"),
                    Guid = Guid.NewGuid().ToString()
                },
                LinkList = new ReactModels.LinkList
                {
                    Items = BuildLinkListFrom(page.PageLink).ToList()
                }
            };

        private IEnumerable<ReactModels.Link> BuildLinkListFrom(PageReference menuItem) =>
            _pageStructureService.GetFilteredChildPages<MenuStructurePage>(menuItem, true)
                .Select(GetLink);

        private ReactModels.Link GetLink(MenuStructurePage page)
        {
            string url;
            bool isExternal = false;
            if (!ContentReference.IsNullOrEmpty(page.LinkToPage))
                url = UrlResolver.GetUrl(page.LinkToPage);
            else
            {
                url = page.UrlLink?.GetUrl(UrlResolver) ?? string.Empty;
                isExternal = page.UrlLink.IsExternal(_urlCheckingService);
            }

            return new ReactModels.Link
            {
                Url = url,
                IsExternal = isExternal,
                Text = _pageStructureService.GetMenuPageTitle(page)
            };
        }

        private string GetImageUrl(ContentReference contentReference)
        {
            return GetImageFile() is ImageFile image
                ? UrlResolver.GetUrl(image.ContentLink)
                : string.Empty;

            ImageFile GetImageFile() =>
                contentReference is null ? null : _contentLoader.Get<ImageFile>(contentReference);
        }

        private IEnumerable<MessageBaseBlock> GetBlocks(FrontPageBase frontPage, PageReference currentPageReference)
        {
            var blocks = GetLocalAndSharedBlocks();

            return currentPageReference == frontPage.ContentLink
                ? blocks
                : blocks.Where(x => !x.FrontPageOnly);

            IEnumerable<MessageBaseBlock> GetLocalAndSharedBlocks()
            {
                var list = ContentReference.IsNullOrEmpty(frontPage.GlobalSharedMessages)
                    ? new List<GlobalMessageBlock>()
                    : _contentLoader.GetChildren<GlobalMessageBlock>(frontPage.GlobalSharedMessages)
                        .Where(x => x.IsSharedMessage)
                        .ToList();

                var sharedBlocks = _contentFilterService.FilterBlocks(list).Cast<MessageBaseBlock>();
                var localBlocks = frontPage.GlobalMessages?.FilteredItems
                                    .Select(x => _contentLoader.Get<MessageBaseBlock>(x.ContentLink)) ?? new List<MessageBaseBlock>();

                return sharedBlocks.Concat(localBlocks);
            }
        }

        protected string GetLabel(string key) => _localizationProvider.GetLabel(nameof(FrontPage), key);
    }
}
