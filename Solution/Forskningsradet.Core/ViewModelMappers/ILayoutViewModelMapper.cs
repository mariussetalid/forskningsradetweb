﻿using EPiServer.Core;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.ReactModels;
using Forskningsradet.Core.Models.ViewModels.Shared;

namespace Forskningsradet.Core.ViewModelMappers
{
    public interface ILayoutViewModelMapper
    {
        LayoutViewModel GetViewModel(FrontPageBase currentContent, PageReference currentPageReference);
    }
}
