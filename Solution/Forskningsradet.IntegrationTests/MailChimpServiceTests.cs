﻿using Forskningsradet.Core.Services;
using Forskningsradet.ServiceAgents.MailChimp;
using Newtonsoft.Json;
using Xunit;
using Xunit.Abstractions;

namespace Forskningsradet.IntegrationTests
{
    public class MailChimpServiceTests
    {
        private readonly ITestOutputHelper _output;
        private readonly MailChimpService _service;

        // Insert secrets temporarily to test integration
        private string _mailChimpApiKey = "";
        private string _mailChimpListId = "";

        public MailChimpServiceTests(ITestOutputHelper output)
        {
            _output = output;
            _service = new MailChimpService(new MailChimpServiceAgent());
        }

        [Fact(Skip = "Requires secrets to run")]
        public void GetGroupsTest()
        {
            var groups = _service.GetGroups(_mailChimpApiKey, _mailChimpListId);
            _output.WriteLine(JsonConvert.SerializeObject(groups, Formatting.Indented));
        }
    }
}