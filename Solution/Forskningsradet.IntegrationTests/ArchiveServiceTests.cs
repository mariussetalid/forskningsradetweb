using FakeItEasy;
using Forskningsradet.Common.Logging;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Services;
using Forskningsradet.Core.Services.Contracts;
using Xunit;

namespace Forskningsradet.IntegrationTests
{
    public class ArchiveServiceTests
    {
        private readonly ArchiveService _archiveService;
        private const string ValidPageName = "name";
        private const string ValidCaseResponsible = "signature";

        public ArchiveServiceTests()
        {
            _archiveService = new ArchiveService(A.Fake<IProcessFactory>(), A.Fake<IIntegrationLogger>());
        }
        
        [Fact]
        public void Run_WhenPageTypeIsSupported_ShouldReturnArchivable()
        {
            var content = A.Fake<EditorialPage>();
            content.Name = ValidPageName;
            content.CaseResponsible = ValidCaseResponsible;
            content.IsArchivable = true;
            
            var result = _archiveService.Archive(content);
            
            Assert.True(result.Archivable);
        }
        
        [Fact]
        public void Run_WhenPageTypeIsNotSupported_ShouldReturnNonArchivable()
        {
            var content = A.Fake<FolderPage>();
            
            var result = _archiveService.Archive(content);
            
            Assert.False(result.Archivable);
        }
        
        [Fact]
        public void Run_WhenPageIsNotArchivable_ShouldReturnNonArchivable()
        {
            var content = A.Fake<EditorialPage>();
            content.Name = ValidPageName;
            content.CaseResponsible = ValidCaseResponsible;
            content.IsArchivable = false;
            
            var result = _archiveService.Archive(content);
            
            Assert.False(result.Archivable);
        }
    }
}