﻿using System.Threading.Tasks;
using Forskningsradet.ServiceAgents.MailChimp;
using Newtonsoft.Json;
using Xunit;
using Xunit.Abstractions;

namespace Forskningsradet.IntegrationTests
{
    public class MailChimpServiceAgentTests
    {
        private readonly ITestOutputHelper _output;
        private readonly MailChimpServiceAgent _service;

        // Insert secrets temporarily to test integration
        private string _mailChimpApiKey = "";
        private string _mailChimpListId = "";

        public MailChimpServiceAgentTests(ITestOutputHelper output)
        {
            _output = output;
            _service = new MailChimpServiceAgent();
        }

        [Fact(Skip = "Requires secrets to run")]
        public async Task GetGroupsTest()
        {
            var groups = await _service.GetGroups(_mailChimpApiKey, _mailChimpListId);

            _output.WriteLine(JsonConvert.SerializeObject(groups, Formatting.Indented));
        }

        [Fact(Skip = "Requires secrets to run")]
        public async Task SubscribeTest()
        {
            await _service.Subscribe("test1@ostensjo.net", new[] { "738053cd45", "e78a4dd5d5" }, _mailChimpApiKey, _mailChimpListId);
        }
    }
}