using FakeItEasy;
using Forskningsradet.Common.Logging;
using Forskningsradet.ServiceAgents;
using Forskningsradet.Web.Configuration;
using Xunit;

namespace Forskningsradet.IntegrationTests
{
    public class NtbServiceAgentTests
    {
        [Fact]
        public void GetPressReleases_ShouldReturnResult()
        {
            var agent = new NtbServiceAgent(new SiteConfiguration(), A.Fake<IIntegrationLogger>());
            
            var result = agent.GetPressReleases().Result;
            
            Assert.NotNull(result);
            Assert.NotEmpty(result.Releases);
        }
    }
}