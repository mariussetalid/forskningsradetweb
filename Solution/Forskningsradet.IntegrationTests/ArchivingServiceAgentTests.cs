﻿using FakeItEasy;
using Forskningsradet.Common.Logging;
using Forskningsradet.ServiceAgents;
using Forskningsradet.ServiceAgents.Archiving;
using Forskningsradet.Web.Configuration;
using Xunit;

namespace Forskningsradet.IntegrationTests
{
    public class ArchivingServiceAgentTests
    {
        [Fact]
        public void ArchivePage_WithValidRequest_ShouldReturnPositiveResult()
        {
            var request = new ArchivingRequest
            {
                Sak = new Sak
                {
                    Id = "N123",
                    Tittel = "Testintegrasjon CMS",
                    Type = "N",
                    Saksansvarlig = "jag",
                    Aktiviteter = new[] {"test"}
                },
                JournalPost = new JournalPost
                {
                    Tittel = "Testintegrasjon CMS v1"
                },
                Fil = new Fil
                {
                    Filnavn = "Testintegrasjon CMS.pdf",
                    Filinnhold = GetEmptyPdfAsBase64()
                }
            };
            var agent = new ArchivingServiceAgent(new SiteConfiguration(), A.Fake<IIntegrationLogger>());
            
            var result = agent.ArchivePage(request).Result;
            
            Assert.True(result.Result);
        }

        private static string GetEmptyPdfAsBase64()
            => @"JVBERi0xLjYNJeLjz9MNCjE1IDAgb2JqIDw8L0xpbmVhcml6ZWQgMS9MIDc3MTMvTyAx
Ny9FIDIxMjMvTiAxL1QgNzM2Ni9IIFsgNDc2IDE2NF0+Pg1lbmRvYmoNICAgICAgICAg
ICAgICAgICAgICAgDQp4cmVmDQoxNSA5DQowMDAwMDAwMDE2IDAwMDAwIG4NCjAwMDAw
MDA4MDUgMDAwMDAgbg0KMDAwMDAwMTA0OSAwMDAwMCBuDQowMDAwMDAxMjU0IDAwMDAw
IG4NCjAwMDAwMDE0NTUgMDAwMDAgbg0KMDAwMDAwMTgwOCAwMDAwMCBuDQowMDAwMDAy
MDQ3IDAwMDAwIG4NCjAwMDAwMDA2NDAgMDAwMDAgbg0KMDAwMDAwMDQ3NiAwMDAwMCBu
DQp0cmFpbGVyDQo8PC9TaXplIDI0L1ByZXYgNzM1NS9YUmVmU3RtIDY0MC9Sb290IDE2
IDAgUi9JbmZvIDYgMCBSL0lEWzwwNjQ4NUI3M0VFNjg3NTU2MzE2QTI4ODkyREU4Q0E3
MD48NkU0ODc0MUEzNDBFOTc0NTk3MkM3OEFCQkYzOTMwMkY+XT4+DQpzdGFydHhyZWYN
CjANCiUlRU9GDQogICAgDQoyMyAwIG9iajw8L0xlbmd0aCA3NS9DIDg1L0ZpbHRlci9G
bGF0ZURlY29kZS9JIDEwNy9MIDY5L1MgMzg+PnN0cmVhbQ0KeNpiYGBgZWBgLmUAAhYj
BlTACBJk4GhQQBJjhWIGBh8GTof5SasMwBwmBgbOQgjNYAHRyr4cQjOegmtmY2CQtoMa
fBMgwABrPwdFDQplbmRzdHJlYW0NZW5kb2JqDTIyIDAgb2JqPDwvTGVuZ3RoIDIwL0Zp
bHRlci9GbGF0ZURlY29kZS9XWzEgMSAxXS9JbmRleFs3IDhdL0RlY29kZVBhcm1zPDwv
Q29sdW1ucyAzL1ByZWRpY3RvciAxMj4+L1NpemUgMTUvVHlwZS9YUmVmPj5zdHJlYW0N
CnjaYmJiZGBiYGDEhQECDAAB9wAbDQplbmRzdHJlYW0NZW5kb2JqDTE2IDAgb2JqPDwv
TWFya0luZm88PC9MZXR0ZXJzcGFjZUZsYWdzIDAvTWFya2VkIHRydWU+Pi9NZXRhZGF0
YSA1IDAgUi9QaWVjZUluZm88PC9NYXJrZWRQREY8PC9MYXN0TW9kaWZpZWQoRDoyMDA3
MDEzMDEzMjUzNyk+Pj4+L1BhZ2VzIDQgMCBSL1N0cnVjdFRyZWVSb290IDcgMCBSL1R5
cGUvQ2F0YWxvZy9MYW5nKEVOLUdCKS9MYXN0TW9kaWZpZWQoRDoyMDA3MDEzMDEzMjUz
NykvUGFnZUxhYmVscyAyIDAgUj4+DWVuZG9iag0xNyAwIG9iajw8L0Nyb3BCb3hbMCAw
IDU5NSA4NDJdL1BhcmVudCA0IDAgUi9TdHJ1Y3RQYXJlbnRzIDAvQ29udGVudHMgMTgg
MCBSL1JvdGF0ZSAwL01lZGlhQm94WzAgMCA1OTUgODQyXS9SZXNvdXJjZXM8PC9Gb250
PDwvVFQwIDE5IDAgUj4+L1Byb2NTZXRbL1BERi9UZXh0XS9FeHRHU3RhdGU8PC9HUzAg
MjEgMCBSPj4+Pi9UeXBlL1BhZ2U+Pg1lbmRvYmoNMTggMCBvYmo8PC9MZW5ndGggMTMy
L0ZpbHRlci9GbGF0ZURlY29kZT4+c3RyZWFtDQpIiTSNsQrCQBBE+/2KKZPC3FwgnoGQ
IrlTFAIBtxMrQbEIiBFEv95NYfXeFI9xI5rGDf0+gmjbLvYQ4iYed4jbHc1n6VScKuGh
V1mxIFlBL/jb21roDM+FX1v6hC+NXFATodoU9ZplgE5yytKUh+zx+mCMW+RnPUhSSYN9
/wQYAMGlH3oNCmVuZHN0cmVhbQ1lbmRvYmoNMTkgMCBvYmo8PC9TdWJ0eXBlL1RydWVU
eXBlL0ZvbnREZXNjcmlwdG9yIDIwIDAgUi9MYXN0Q2hhciAxMjEvV2lkdGhzWzI1MCAw
IDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAg
MCAwIDAgMCAwIDAgMCAwIDAgNzIyIDYxMSA1NTYgMCAwIDAgMCAwIDAgMCAwIDAgNTU2
IDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAg
MCAwIDAgNzc4IDAgMCA1MDAgMCAwIDAgMjc4IDAgMCAwIDAgNTAwXS9CYXNlRm9udC9U
aW1lc05ld1JvbWFuUFNNVC9GaXJzdENoYXIgMzIvRW5jb2RpbmcvV2luQW5zaUVuY29k
aW5nL1R5cGUvRm9udD4+DWVuZG9iag0yMCAwIG9iajw8L1N0ZW1WIDgyL0ZvbnROYW1l
L1RpbWVzTmV3Um9tYW5QU01UL0ZvbnRTdHJldGNoL05vcm1hbC9Gb250V2VpZ2h0IDQw
MC9GbGFncyAzNC9EZXNjZW50IC0yMTYvRm9udEJCb3hbLTU2OCAtMzA3IDIwMDAgMTAw
N10vQXNjZW50IDg5MS9Gb250RmFtaWx5KFRpbWVzIE5ldyBSb21hbikvQ2FwSGVpZ2h0
IDY1Ni9YSGVpZ2h0IDAvVHlwZS9Gb250RGVzY3JpcHRvci9JdGFsaWNBbmdsZSAwPj4N
ZW5kb2JqDTIxIDAgb2JqPDwvT1BNIDEvT1AgZmFsc2Uvb3AgZmFsc2UvVHlwZS9FeHRH
U3RhdGUvU0EgZmFsc2UvU00gMC4wMj4+DWVuZG9iag0xIDAgb2JqPDwvRmlyc3QgNTEv
TGVuZ3RoIDM2NS9GaWx0ZXIvRmxhdGVEZWNvZGUvTiA4L1R5cGUvT2JqU3RtPj5zdHJl
YW0NCnjaVFFNb8IwDP0rPm6HzQkMlUmoEp8S4lMUtAPiEMB01UpShXSCfz8nXQUcEsfO
s9+zHYGANkgh4BPkh+AXyBY/JTQjNg1ofkYgm9CS/h9ajQg6HZxwjoAVLpUl7daWiNOf
A3O6ugndQOLK5DRThS/tIetbQZg4Wx4CbmWMw36uLpeAkR4Tx4GD+UJNiIJNMKGDw6nS
6ctw/tbdvAbcvDxftsJrZNAuhMY6zzQl34qZRllaWsKBNUVfFbW7KJ2HYFIojUlZkL0c
bFa4OrB/dNeLLp8+n1GwLD77IVPW39xpz1xxkP3i3NizynGJQ33UxhEH+Bpxj3dvo49k
7+wjq84Ukrn+2BOM8Uk601fhqt3A4DdTD4qrHKh7cmRBvIsgp5tnKbfhlHW44IndvNgv
m7lMpzNzJJza9b5K7NHJWAqZAT9mddoFn3t48HzduxvH22o9O1bQr/uegOB1tf/XVQ8j
BRlVav8EGACWWr+MDQplbmRzdHJlYW0NZW5kb2JqDTIgMCBvYmo8PC9OdW1zWzAgMyAw
IFJdPj4NZW5kb2JqDTMgMCBvYmo8PC9TL0Q+Pg1lbmRvYmoNNCAwIG9iajw8L0NvdW50
IDEvVHlwZS9QYWdlcy9LaWRzWzE3IDAgUl0+Pg1lbmRvYmoNNSAwIG9iajw8L1N1YnR5
cGUvWE1ML0xlbmd0aCA0Mzc0L1R5cGUvTWV0YWRhdGE+PnN0cmVhbQ0KPD94cGFja2V0
IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4KPHg6eG1w
bWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iMy4xLTcwMiI+CiAg
IDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1y
ZGYtc3ludGF4LW5zIyI+CiAgICAgIDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSJ1
dWlkOjNiMTdiNGM2LTBkMGItNDExNi04NjkxLTgyODA1NmQ1ZGMwZiIKICAgICAgICAg
ICAgeG1sbnM6cGRmPSJodHRwOi8vbnMuYWRvYmUuY29tL3BkZi8xLjMvIj4KICAgICAg
ICAgPHBkZjpQcm9kdWNlcj5BY3JvYmF0IERpc3RpbGxlciA2LjAgKFdpbmRvd3MpPC9w
ZGY6UHJvZHVjZXI+CiAgICAgIDwvcmRmOkRlc2NyaXB0aW9uPgogICAgICA8cmRmOkRl
c2NyaXB0aW9uIHJkZjphYm91dD0idXVpZDozYjE3YjRjNi0wZDBiLTQxMTYtODY5MS04
MjgwNTZkNWRjMGYiCiAgICAgICAgICAgIHhtbG5zOnBkZng9Imh0dHA6Ly9ucy5hZG9i
ZS5jb20vcGRmeC8xLjMvIj4KICAgICAgICAgPHBkZng6U291cmNlTW9kaWZpZWQ+RDoy
MDA3MDEzMDEzMjUyMDwvcGRmeDpTb3VyY2VNb2RpZmllZD4KICAgICAgPC9yZGY6RGVz
Y3JpcHRpb24+CiAgICAgIDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSJ1dWlkOjNi
MTdiNGM2LTBkMGItNDExNi04NjkxLTgyODA1NmQ1ZGMwZiIKICAgICAgICAgICAgeG1s
bnM6eGFwPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIj4KICAgICAgICAgPHhh
cDpDcmVhdGVEYXRlPjIwMDctMDEtMzBUMTM6MjU6MjVaPC94YXA6Q3JlYXRlRGF0ZT4K
ICAgICAgICAgPHhhcDpDcmVhdG9yVG9vbD5BY3JvYmF0IFBERk1ha2VyIDYuMCBmb3Ig
V29yZDwveGFwOkNyZWF0b3JUb29sPgogICAgICAgICA8eGFwOk1vZGlmeURhdGU+MjAw
Ny0xMi0wN1QxMzo1MDo0OSswMTowMDwveGFwOk1vZGlmeURhdGU+CiAgICAgICAgIDx4
YXA6TWV0YWRhdGFEYXRlPjIwMDctMTItMDdUMTM6NTA6NDkrMDE6MDA8L3hhcDpNZXRh
ZGF0YURhdGU+CiAgICAgIDwvcmRmOkRlc2NyaXB0aW9uPgogICAgICA8cmRmOkRlc2Ny
aXB0aW9uIHJkZjphYm91dD0idXVpZDozYjE3YjRjNi0wZDBiLTQxMTYtODY5MS04Mjgw
NTZkNWRjMGYiCiAgICAgICAgICAgIHhtbG5zOnhhcE1NPSJodHRwOi8vbnMuYWRvYmUu
Y29tL3hhcC8xLjAvbW0vIj4KICAgICAgICAgPHhhcE1NOkRvY3VtZW50SUQ+dXVpZDo4
MDU4YWE1NC0zNjkxLTRmYmEtOGY3NS0yZmJkMDU1NGViNDI8L3hhcE1NOkRvY3VtZW50
SUQ+CiAgICAgICAgIDx4YXBNTTpJbnN0YW5jZUlEPnV1aWQ6NGUyYTMxMzgtMTAwMS00
MDFmLTg2N2QtNjg2NzliMzM3ZDA0PC94YXBNTTpJbnN0YW5jZUlEPgogICAgICAgICA8
eGFwTU06VmVyc2lvbklEPgogICAgICAgICAgICA8cmRmOlNlcT4KICAgICAgICAgICAg
ICAgPHJkZjpsaT4xPC9yZGY6bGk+CiAgICAgICAgICAgIDwvcmRmOlNlcT4KICAgICAg
ICAgPC94YXBNTTpWZXJzaW9uSUQ+CiAgICAgIDwvcmRmOkRlc2NyaXB0aW9uPgogICAg
ICA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0idXVpZDozYjE3YjRjNi0wZDBiLTQx
MTYtODY5MS04MjgwNTZkNWRjMGYiCiAgICAgICAgICAgIHhtbG5zOmRjPSJodHRwOi8v
cHVybC5vcmcvZGMvZWxlbWVudHMvMS4xLyI+CiAgICAgICAgIDxkYzpmb3JtYXQ+YXBw
bGljYXRpb24vcGRmPC9kYzpmb3JtYXQ+CiAgICAgICAgIDxkYzp0aXRsZT4KICAgICAg
ICAgICAgPHJkZjpBbHQ+CiAgICAgICAgICAgICAgIDxyZGY6bGkgeG1sOmxhbmc9Ingt
ZGVmYXVsdCI+RW1wdHkgUERGPC9yZGY6bGk+CiAgICAgICAgICAgIDwvcmRmOkFsdD4K
ICAgICAgICAgPC9kYzp0aXRsZT4KICAgICAgICAgPGRjOmNyZWF0b3I+CiAgICAgICAg
ICAgIDxyZGY6U2VxPgogICAgICAgICAgICA8L3JkZjpTZXE+CiAgICAgICAgIDwvZGM6
Y3JlYXRvcj4KICAgICAgPC9yZGY6RGVzY3JpcHRpb24+CiAgICAgIDxyZGY6RGVzY3Jp
cHRpb24gcmRmOmFib3V0PSJ1dWlkOjNiMTdiNGM2LTBkMGItNDExNi04NjkxLTgyODA1
NmQ1ZGMwZiIKICAgICAgICAgICAgeG1sbnM6cGhvdG9zaG9wPSJodHRwOi8vbnMuYWRv
YmUuY29tL3Bob3Rvc2hvcC8xLjAvIj4KICAgICAgICAgPHBob3Rvc2hvcDpoZWFkbGlu
ZT4KICAgICAgICAgICAgPHJkZjpTZXE+CiAgICAgICAgICAgICAgIDxyZGY6bGkvPgog
ICAgICAgICAgICA8L3JkZjpTZXE+CiAgICAgICAgIDwvcGhvdG9zaG9wOmhlYWRsaW5l
PgogICAgICA8L3JkZjpEZXNjcmlwdGlvbj4KICAgPC9yZGY6UkRGPgo8L3g6eG1wbWV0
YT4KICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
IAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
CiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAK
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAog
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAg
ICAgICAgICAgIAo8P3hwYWNrZXQgZW5kPSJ3Ij8+DQplbmRzdHJlYW0NZW5kb2JqDTYg
MCBvYmo8PC9DcmVhdGlvbkRhdGUoRDoyMDA3MDEzMDEzMjUyNVopL0NyZWF0b3IoQWNy
b2JhdCBQREZNYWtlciA2LjAgZm9yIFdvcmQpL1Byb2R1Y2VyKEFjcm9iYXQgRGlzdGls
bGVyIDYuMCBcKFdpbmRvd3NcKSkvTW9kRGF0ZShEOjIwMDcxMjA3MTM1MDQ5KzAxJzAw
JykvU291cmNlTW9kaWZpZWQoRDoyMDA3MDEzMDEzMjUyMCkvVGl0bGUoRW1wdHkgUERG
KT4+DWVuZG9iag14cmVmDQowIDE1DQowMDAwMDAwMDAwIDY1NTM1IGYNCjAwMDAwMDIx
MjMgMDAwMDAgbg0KMDAwMDAwMjU4MSAwMDAwMCBuDQowMDAwMDAyNjE0IDAwMDAwIG4N
CjAwMDAwMDI2MzcgMDAwMDAgbg0KMDAwMDAwMjY4OCAwMDAwMCBuDQowMDAwMDA3MTM4
IDAwMDAwIG4NCjAwMDAwMDAwMDAgNjU1MzUgZg0KMDAwMDAwMDAwMCA2NTUzNSBmDQow
MDAwMDAwMDAwIDY1NTM1IGYNCjAwMDAwMDAwMDAgNjU1MzUgZg0KMDAwMDAwMDAwMCA2
NTUzNSBmDQowMDAwMDAwMDAwIDY1NTM1IGYNCjAwMDAwMDAwMDAgNjU1MzUgZg0KMDAw
MDAwMDAwMCA2NTUzNSBmDQp0cmFpbGVyDQo8PC9TaXplIDE1Pj4NCnN0YXJ0eHJlZg0K
MTE2DQolJUVPRg0K";
    }
}
