﻿using System.Collections.Generic;

namespace Forskningsradet.ServiceAgents.Contracts
{
    public interface IDynamicsCrmServiceAgent
    {
        Dictionary<string, int> GetTopics();
        Dictionary<string, int> GetGroups();
        bool Subscribe(string email, int[] subjects, int[] targetGroups, string clientId, string clientSecret, string apiUrl);
    }
}