using System.Threading.Tasks;
using Forskningsradet.ServiceAgents.ResponseModels;

namespace Forskningsradet.ServiceAgents.Contracts
{
    public interface INtbServiceAgent
    {
        Task<NtbPressReleases> GetPressReleases(int page = 1);
    }
}