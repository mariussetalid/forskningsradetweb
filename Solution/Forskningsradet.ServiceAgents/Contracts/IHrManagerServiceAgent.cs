﻿using System.Threading.Tasks;
using Forskningsradet.ServiceAgents.ResponseModels;

namespace Forskningsradet.ServiceAgents.Contracts
{
    public interface IHrManagerServiceAgent
    {
        Task<Vacancy> GetVacancy(int id);
        Task<Vacancies> GetVacancies();
    }
}
