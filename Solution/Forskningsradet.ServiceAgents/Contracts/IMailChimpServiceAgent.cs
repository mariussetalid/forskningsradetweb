﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Forskningsradet.ServiceAgents.MailChimp.Models;

namespace Forskningsradet.ServiceAgents.Contracts
{
    public interface IMailChimpServiceAgent
    {
        Task<List<Group>> GetGroups(string mailChimpApiKey, string mailChimpListId);
        Task<bool> Subscribe(string email, string[] interestIds, string mailChimpApiKey, string mailChimpListId);
    }
}