﻿namespace Forskningsradet.ServiceAgents.Contracts
{
    public interface IHrManagerServiceAgentConfiguration
    {
        string HrManagerBaseUri { get; }
        string HrManagerVacanciesUri { get; }
        string HrManagerVacancyUri { get; }
    }
}
