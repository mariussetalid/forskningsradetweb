﻿namespace Forskningsradet.ServiceAgents.Contracts
{
    public interface IArchivingServiceAgentConfiguration
    {
        string ArchivingServiceBaseUri { get; }
        string ArchivingServiceSaveWebContentUri { get; }
    }
}