﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Forskningsradet.ServiceAgents.Contracts;
using Forskningsradet.ServiceAgents.MailChimp.Models;
using MailChimp.Net;
using MailChimp.Net.Models;

namespace Forskningsradet.ServiceAgents.MailChimp
{
    public class MailChimpServiceAgent : IMailChimpServiceAgent
    {
        public async Task<List<Group>> GetGroups(string mailChimpApiKey, string mailChimpListId)
        {
            var manager = new MailChimpManager(mailChimpApiKey);

            var groups = await manager.InterestCategories.GetAllAsync(mailChimpListId);

            var retval = groups.Select(x => new Group { Id = x.Id, Title = x.Title }).ToList();

            foreach (var grp in retval)
            {
                var subGroups = await manager.Interests.GetAllAsync(mailChimpListId, grp.Id);

                grp.Children = subGroups.Select(x => new Group { Id = x.Id, Title = x.Name }).ToList();
            }

            return retval;
        }

        public async Task<bool> Subscribe(string email, string[] interestIds, string mailChimpApiKey, string mailChimpListId)
        {
            var manager = new MailChimpManager(mailChimpApiKey);
            var member = new Member
            {
                EmailAddress = email,
                Status = Status.Pending
            };
            var interests = (await GetGroups(mailChimpApiKey, mailChimpListId)).SelectMany(x => x.Children);

            member.Interests = BuildInterests(interestIds, interests);

            await manager.Members.AddOrUpdateAsync(mailChimpListId, member).ConfigureAwait(false);
            return true;
        }

        private static Dictionary<string, bool> BuildInterests(IReadOnlyCollection<string> interestIds, IEnumerable<Group> interests) =>
            interests.ToDictionary(
                i => i.Id,
                i => interestIds.Count == 0 || interestIds.Any(x => x == i.Id));
    }
}