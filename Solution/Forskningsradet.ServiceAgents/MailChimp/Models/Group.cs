﻿using System.Collections.Generic;

namespace Forskningsradet.ServiceAgents.MailChimp.Models
{
    public class Group
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public IList<Group> Children { get; set; }
    }
}