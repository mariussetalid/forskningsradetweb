using System.Threading.Tasks;
using Forskningsradet.Common.Logging;
using Forskningsradet.ServiceAgents.Contracts;
using Forskningsradet.ServiceAgents.ResponseModels;

namespace Forskningsradet.ServiceAgents
{
    public class NtbServiceAgent : BaseServiceAgent, INtbServiceAgent
    {
        private const string PageParamName = "page";
        
        public NtbServiceAgent(INtbServiceAgentConfiguration configuration, IIntegrationLogger logger)
            : base(configuration.NtbUri, logger)
        {
        }

        public async Task<NtbPressReleases> GetPressReleases(int page = 1) =>
            await GetWithParameterAsync<NtbPressReleases>(PageParamName, page.ToString()).ConfigureAwait(false);
    }
}