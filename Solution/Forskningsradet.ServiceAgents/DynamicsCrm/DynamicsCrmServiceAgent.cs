﻿using System;
using System.Collections.Generic;
using System.Linq;
using Forskningsradet.ServiceAgents.Contracts;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Tooling.Connector;

namespace Forskningsradet.ServiceAgents.DynamicsCrm
{
    public class DynamicsCrmServiceAgent : IDynamicsCrmServiceAgent
    {
        private const string CrmEntityName = "trcn_newslettersubscription";
        private const string CrmEmailFieldName = "trcn_emailaddress1";
        private const string CrmTopicFieldName = "trcn_newslettertopic";
        private const string CrmGroupFieldName = "trcn_group";

        private readonly Dictionary<string, int> _topics =
            new Dictionary<string, int>
            {
                { "Demokrati, styring og fornyelse", 100000000 },
                { "Energi, transport og lavutslipp", 100000001 },
                { "Forskningspolitikk", 100000002 },
                { "Global utvikling", 100000003 },
                { "Hav", 100000005 },
                { "Helse", 100000006 },
                { "Humaniora og samfunnsvitenskap", 100000007 },
                { "Industri og tjenestenæringer", 100000008 },
                { "Klima- og polarforskning", 100000009 },
                { "Landbasert mat, miljø og bioressurser", 100000010 },
                { "Livsvitenskap", 100000011 },
                { "Teknologi, naturvitenskap og matematikk", 100000012 },
                { "Petroleum", 100000013 },
                { "Utdanning og kompetanse", 100000014 },
                { "Velferd, kultur og samfunn", 100000015 }
            };

        private readonly Dictionary<string, int> _groups =
            new Dictionary<string, int>
            {
                { "Forskningsorganisasjon", 100000000 },
                { "Næringsliv", 100000001 },
                { "Offentlig sektor", 100000002 }
            };

        public Dictionary<string, int> GetTopics() =>
            _topics;

        public Dictionary<string, int> GetGroups() =>
            _groups;

        public bool Subscribe(string email, int[] subjects, int[] targetGroups, string clientId, string clientSecret, string apiUrl)
        {
            var topicValues = new OptionSetValueCollection();
            topicValues.AddRange(CleanupAndGetValues(subjects, _topics));

            var groupValues = new OptionSetValueCollection();
            groupValues.AddRange(CleanupAndGetValues(targetGroups, _groups));

            if (!topicValues.Any() && !groupValues.Any())
                return false;

            var entity = new Entity(CrmEntityName);
            entity[CrmEmailFieldName] = email;

            if (topicValues.Any())
                entity[CrmTopicFieldName] = topicValues;

            if (groupValues.Any())
                entity[CrmGroupFieldName] = groupValues;

            var entityId = Guid.Empty;
            using (var service = Connect(clientId, clientSecret, apiUrl))
            {
                entityId = service.Create(entity);
            }

            return entityId != Guid.Empty;
        }

        private IEnumerable<OptionSetValue> CleanupAndGetValues(int[] input, Dictionary<string, int> dictionary) =>
            Cleanup(input ?? new int[0])
            .Where(x => dictionary.ContainsValue(x))
            .Select(x => new OptionSetValue(x));

        private IEnumerable<int> Cleanup(int[] array) =>
            array.Where(x => x > 0)
            .Distinct();

        private static CrmServiceClient Connect(string clientId, string clientSecret, string apiUrl) =>
            new CrmServiceClient($@"AuthType=ClientSecret;url={apiUrl};ClientId={clientId};ClientSecret={clientSecret}");
    }
}