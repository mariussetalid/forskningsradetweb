﻿using System;
using System.Threading.Tasks;
using Forskningsradet.Common.Logging;
using Newtonsoft.Json;
using RestSharp;

namespace Forskningsradet.ServiceAgents
{
    public abstract class BaseServiceAgent
    {
        private readonly IIntegrationLogger _logger;
        private readonly Uri _baseUri;
        
        protected BaseServiceAgent(string baseUri, IIntegrationLogger logger)
        {
            _logger = logger;
            _baseUri = new Uri(baseUri);
        }

        protected async Task<T> GetAsync<T>(string relativeUri)
            => await ExecuteAsync<T>(new RestRequest(relativeUri, Method.GET));
        
        protected async Task<T> GetWithParameterAsync<T>(string parameterName, string parameterValue)
        {
            var request = new RestRequest(Method.GET);
            request.AddParameter(parameterName, parameterValue);
            return await ExecuteAsync<T>(request);
        }

        protected async Task<T> PostAsync<T>(string relativeUri, object body)
            => await ExecuteAsync<T>(BuildPostRequest(relativeUri, body));

        private async Task<T> ExecuteAsync<T>(IRestRequest request)
        {
            var client = new RestClient(_baseUri);
            var response = await client.ExecuteAsync(request);
            
            if (response.ErrorException != null)
                _logger.LogError("Request resulted in an error", response.ErrorException);
            
            return JsonConvert.DeserializeObject<T>(response.Content);
        }
        
        private static RestRequest BuildPostRequest(string relativeUri, object body)
        {
            var bodyAsJson = JsonConvert.SerializeObject(body);
            var request = new RestRequest(relativeUri, Method.POST) {RequestFormat = DataFormat.Json};
            request.AddParameter("application/json; charset=utf-8", bodyAsJson, ParameterType.RequestBody);
            
            return request;
        }
    }
}
