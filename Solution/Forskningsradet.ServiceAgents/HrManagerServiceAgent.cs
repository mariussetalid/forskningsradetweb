﻿using System.Threading.Tasks;
using Forskningsradet.Common.Logging;
using Forskningsradet.ServiceAgents.Contracts;
using Forskningsradet.ServiceAgents.ResponseModels;

namespace Forskningsradet.ServiceAgents
{
    public class HrManagerServiceAgent : BaseServiceAgent, IHrManagerServiceAgent
    {
        private readonly IHrManagerServiceAgentConfiguration _configuration;

        public HrManagerServiceAgent(IHrManagerServiceAgentConfiguration configuration, IIntegrationLogger logger)
            : base(configuration.HrManagerBaseUri, logger)
            => _configuration = configuration;

        public async Task<Vacancy> GetVacancy(int id)
            => await GetAsync<Vacancy>(string.Format(_configuration.HrManagerVacancyUri, id));

        public async Task<Vacancies> GetVacancies()
            => await GetAsync<Vacancies>(_configuration.HrManagerVacanciesUri);
    }
}
