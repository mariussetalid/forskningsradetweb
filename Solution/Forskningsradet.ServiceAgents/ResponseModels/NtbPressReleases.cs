using System.Collections.Generic;

namespace Forskningsradet.ServiceAgents.ResponseModels
{
    public class NtbPressReleases
    {
        public NtbPressReleases()
        {
            Releases = new List<NtbPressRelease>();
        }
        
        public List<NtbPressRelease> Releases { get; set; }
        public int? NextPage { get; set; }
        public int TotalCount { get; set; }
    }
}