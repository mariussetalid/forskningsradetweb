﻿using System;

namespace Forskningsradet.ServiceAgents.ResponseModels
{
    public class VacancyItem
    {
        public string AdvertisementUrl { get; set; }
        public DateTime ApplicationDue { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public string ShortDescription { get; set; }
    }
}
