using System;
using Newtonsoft.Json;

namespace Forskningsradet.ServiceAgents.ResponseModels
{
    public class NtbPressRelease
    {
        public long Id { get; set; }
        public string Title { get; set; }
        [JsonProperty(PropertyName = "leadtext")]
        public string Lead { get; set; }
        public string Body { get; set; }
        public DateTime Published { get; set; }
        public NtbImage MainImage { get; set; }
    }
}