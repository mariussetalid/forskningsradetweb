using Newtonsoft.Json;

namespace Forskningsradet.ServiceAgents.ResponseModels
{
    public class NtbImage
    {
        public string Url { get; set; }
        [JsonProperty(PropertyName = "thumbnail_square")]
        public string ThumbnailUrl { get; set; }
        public string Caption { get; set; }
    }
}