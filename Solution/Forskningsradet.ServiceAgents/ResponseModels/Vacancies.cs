﻿using System.Collections.Generic;

namespace Forskningsradet.ServiceAgents.ResponseModels
{
    public class Vacancies
    {
        public string CustomerAlias { get; set; }
        public string CustomerName { get; set; }
        public List<VacancyItem> Items { get; set; }
        public int PositionCountCustomer { get; set; }
        public int PositionCountList { get; set; }
        public int PositionCountSearch { get; set; }
        public int PositionCountSkipped { get; set; }
    }
}
