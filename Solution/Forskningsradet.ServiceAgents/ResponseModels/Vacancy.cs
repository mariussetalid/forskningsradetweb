﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Forskningsradet.ServiceAgents.ResponseModels
{
    [DebuggerDisplay("Id = {Id}")]
    public class Vacancy
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string AdvertisementUrl { get; set; }
        public string AdvertisementUrlSecure { get; set; }
        public List<Advertisement> Advertisements { get; set; }
        public DateTime ApplicationDue { get; set; }
        public object PositionLocationTree { get; set; }
        public string ShortDescription { get; set; }
        public string WorkPlace { get; set; }
    }

    public class Advertisement
    {
        public string Content { get; set; }
        public string Description { get; set; }
        public int Id { get; set; }
        public object ImageUrl { get; set; }
        public object ImageUrlSecure { get; set; }
        public string Introduction { get; set; }
        public Video Video { get; set; }
    }

    public class Video
    {
        public string Embed { get; set; }
        public string Url { get; set; }
    }
}
