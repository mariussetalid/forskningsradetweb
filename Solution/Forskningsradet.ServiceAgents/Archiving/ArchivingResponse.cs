namespace Forskningsradet.ServiceAgents.Archiving
{
    public class ArchivingResponse
    {
        public bool Result { get; set; }
        public string Message { get; set; }
    }
}