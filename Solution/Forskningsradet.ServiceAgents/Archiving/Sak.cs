using Newtonsoft.Json;

namespace Forskningsradet.ServiceAgents.Archiving
{
    public class Sak
    {
        [JsonProperty("ID")]
        public string Id { get; set; }
        public string Tittel { get; set; }
        public string Type { get; set; }
        public string Saksansvarlig { get; set; }
        public string[] Aktiviteter { get; set; }
    }
}