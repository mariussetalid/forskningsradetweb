using System.Threading.Tasks;

namespace Forskningsradet.ServiceAgents.Archiving
{
    public interface IArchivingServiceAgent
    {
        Task<ArchivingResponse> ArchivePage(ArchivingRequest request);
    }
}