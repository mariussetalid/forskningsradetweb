using Newtonsoft.Json;

namespace Forskningsradet.ServiceAgents.Archiving
{
    public class ArchivingRequest
    {
        [JsonProperty("sak")]
        public Sak Sak { get; set; }
        
        [JsonProperty("jp")]
        public JournalPost JournalPost { get; set; }
        
        [JsonProperty("fil")]
        public Fil Fil { get; set; }
    }
}