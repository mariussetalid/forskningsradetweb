﻿using System.Threading.Tasks;
using Forskningsradet.Common.Logging;
using Forskningsradet.ServiceAgents.Archiving;
using Forskningsradet.ServiceAgents.Contracts;

namespace Forskningsradet.ServiceAgents
{
    public class ArchivingServiceAgent : BaseServiceAgent, IArchivingServiceAgent
    {
        private readonly IArchivingServiceAgentConfiguration _configuration;
        
        public ArchivingServiceAgent(IArchivingServiceAgentConfiguration configuration, IIntegrationLogger logger)
            : base(configuration.ArchivingServiceBaseUri, logger) => _configuration = configuration;

        public async Task<ArchivingResponse> ArchivePage(ArchivingRequest request)
            => await PostAsync<ArchivingResponse>(_configuration.ArchivingServiceSaveWebContentUri, request);
    }
}