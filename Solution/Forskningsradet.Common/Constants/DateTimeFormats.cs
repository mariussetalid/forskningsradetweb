namespace Forskningsradet.Common.Constants
{
    public static class DateTimeFormats
    {
        public const string NorwegianDate = "dd.MM.yyyy";
        public const string DateDayLong = "dd";
        public const string NorwegianDateDayLong = "dd.";
        public const string EnglishDayMonthYear = "d MMMM yyyy";
        public const string NorwegianDayMonth = "M";
        public const string EventDateTimeFull = "d. MMMM yyyy HH:mm";
        public const string EventTime = "HH:mm";
        public const string ProposalDeadlineDate = "dd. MMMM yyyy";
        public const string TimelineDate = "dd MMM yyyy";
        public const string ShortMonth = "MMM";
        public const string ShortMonthYear = "MMM yyyy";
        public const string MonthYear = "MMMM yyyy";

        public static class TimeZone
        {
            public const string CEST = "CEST";
            public const string CET = "CET";
        }
    }
}