﻿namespace Forskningsradet.Common.Constants
{
    public static class ShareConstants
    {
        public const string Facebook = "Facebook";
        public const string Twitter = "Twitter";
        public const string Linkedin = "LinkedIn";
        public const string Mail = "Mail";

        public const string FacebookUrlFormat = "https://www.facebook.com/sharer.php?u={0}";
        public const string TwitterUrlFormat = "https://twitter.com/intent/tweet?text={0}&url={1}";
        public const string LinkedinUrlFormat = "https://www.linkedin.com/sharing/share-offsite/?url={0}";
        public const string MailUrlFormat = "mailto:?subject={0}&body={1}";
    }
}
