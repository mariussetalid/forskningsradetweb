﻿namespace Forskningsradet.Common.Constants
{
    public static class ContentAreaTags
    {
        public const string FullWidth = "fullwidth";
        public const string TwoThirdsWidth = "TwoThirdsWidth";
        public const string HalfWidth = "halfwidth";
        public const string OneThirdWidth = "onethirdwidth";
        public const string NoRenderer = "norenderer";
    }
}
