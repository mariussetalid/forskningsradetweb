namespace Forskningsradet.Common.Constants
{
    public static class EmbedSourceConstants
    {
        public static class ProjectDatabank
        {
            public const string ValidSrcStart = "https://prosjektbanken.forskningsradet.no/";
        }

        public static class Vbrick
        {
            public const string ValidSrcStart = "https://videoportal.rcn.no/";
        }

        public static class Tableau
        {
            public const string ValidSrcStart = "https://public.tableau.com/views";
            public const string SrcSuffix = "&:showVizHome=no";
        }

        public static class Facebook
        {
            public const string ValidSrcStart = "https://facebook.com/video/";
        }

        public static class Instagram
        {
            public const string ValidSrcStart = "https://instagram.com/p/";
        }

        public static class Iframe
        {
            public const string ValidSrcStart = "https://";
        }
    }
}