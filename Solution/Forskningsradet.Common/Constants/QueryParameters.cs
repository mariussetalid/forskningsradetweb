﻿namespace Forskningsradet.Common.Constants
{
    public static class QueryParameters
    {
        public const string SearchQuery = "q";
        public const string DownloadCalendarQuery = "downloadCalendar";
        public const string Letter = "letter";
        public const string Page = "page";
        public const string TimeFrame = "timeframe";
        public const string Year = "year";
        public const string YearFrom = "from";
        public const string YearTo = "to";
        public const string EventType = "type";
        public const string ResultType = "type";
        public const string Location = "location";
        public const string Video = "video";
        public const string Subject = "subjects";
        public const string TargetGroup = "targetGroups";
        public const string Categories = "categories";
        public const string Deadlines = "deadlines";
        public const string ApplicationTypes = "applicationTypes";
        public const string DeadlineTypes = "deadlineTypes";
        public const string Download = "download";
        public const string Id = "id";
    }
}
