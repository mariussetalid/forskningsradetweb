namespace Forskningsradet.Common.Constants
{
    public static class ListPageConstants
    {
        public const int NumberOfVisibleTags = 15;

        public static class PressReleases
        {
            public const int ItemsPerPage = 10;
        }
    }
}