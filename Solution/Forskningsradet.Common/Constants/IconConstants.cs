﻿namespace Forskningsradet.Common.Constants
{
    public static class IconConstants
    {
        private const string Path = "/static/img/thumbnails/";

        public static class Thumbnails
        {
            public const string ListPage = Path + "liste_enkel.png";
            public const string ListPageWithVisualElement = Path + "liste_enkel_m_visuelt_element.png";
            public const string ListWithSearchAndFilterAndGroups = Path + "liste_m_sok_og_alfabet.png";
            public const string ListWithSearchAndFilter = Path + "liste_m_sok_og_filter.png";
            public const string ListWithFilter = Path + "liste_med_filter.png";
            public const string FolderPage = Path + "mappeside_mork.png";
            public const string ContentAreaPage = Path + "landingsside.png";
            public const string EventPage = Path + "arrangementsside.png";
            public const string InformationArticlePage = Path + "innholdsside.png";
            public const string MenuPage = Path + "menypunkt.png";
            public const string NewsArticlePage = Path + "innholdsside_nyhet.png";
            public const string NewsletterPage = Path + "nyhetsbrev.png";
            public const string FrontPage = Path + "forside.png";
            public const string LargeDocumentMainPage = Path + "store_dok_kapittelside.png";
            public const string LargeDocumentChapterPage = Path + "store_dok_innholdsside.png";
            public const string ProposalPage = Path + "utlysningsside.png";
            public const string PersonPage = Path + "personside.png";
            public const string PublicationPage = Path + "publikasjon.png";
            public const string RssPage = Path + "RSS-feed.png";
            public const string SettingsPage = Path + "tannhjul.png";
            public const string WordPage = Path + "ordliste.png";

            public const string AccordionBlock = Path + "faktablokk.png";
            public const string CampaignBlock = Path + "kampanjeblokk.png";
            public const string IframeBlock = Path + "i-frame_generell.png";
            public const string ImageWithCaptionBlock = Path + "bildeblokk_m_bildetekst.png";
            public const string InformationBlock = Path + "infoblokk.png";
            public const string LinkListBlock = Path + "lenkelisteblokk.png";
            public const string NumberBlock = Path + "stort_tall_blokk.png";
            public const string PageLinksBlock = Path + "temainnganger.png";
            public const string ProfileBlock = Path + "profilblokk.png";
            public const string ProjectDatabankBlock = Path + "i-frame_prosjektbanken.png";
            public const string QuoteBlock = Path + "sitatblokk.png";
            public const string RelatedContentListBlock = Path + "relatert_artikkel_blokk.png";
            public const string RelatedEventListBlock = Path + "relatert_arr_blokk.png";
            public const string RelatedProposalListBlock = Path + "relatert_utlysning_blokk.png";
            public const string RelatedPublicationListBlock = Path + "relatert_publ_blokk.png";
            public const string RichTextBlock = Path + "fritekstblokk.png";
            public const string ScheduleBlock = Path + "agendablokk.png";
            public const string SubjectPriorityBlock = Path + "tekst_m_blokkmarg.png";
            public const string TableauBlock = Path + "i-frame_tableau.png";
            public const string TimeLineBlock = Path + "tidslinjeblokk.png";
            public const string VbrickBlock = Path + "i-frame_v-brick.png";
            public const string VideoBlock = Path + "i-frame_youtube.png";
            public const string ImageWithLinkBlock = Path + "lenkeblokk_m_bilde.png";
            public const string EventListBlock = Path + "listeblokk_m_dato.png";
            public const string ContactBlock = Path + "kontaktblokk.png";
        }

        public static class CssClasses
        {
            public const string Blank = "blank";
            public const string Blocks = "blokker";
            public const string Folder = "mappe";
            public const string List = "side_liste";
            public const string Menu = "menypunkt";
            public const string NoView = "utenvisning";
            public const string Rss = "rssfeed";
            public const string Settings = "tannhjul";
            public const string StartPage = "hus";
            public const string Text = "tekst";
        }
    }
}
