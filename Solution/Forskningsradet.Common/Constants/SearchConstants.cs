namespace Forskningsradet.Common.Constants
{
    public static class SearchConstants
    {
        public const int DefaultRelatedContentSize = 10;
        public const int DefaultPageSize = 20;
        public const int SearchPageSize = 10;
        public const int MaxPageSize = 1000;
        public const int ThresholdForFirstAndLastDirectPaginationLinks = 10;
        public const string MetaDataJobTitle = "JobTitle";
        public const string MetaDataDepartment = "Department";
        public const string MetaDataEmail = "Email";
        public const string MetaDataStartDate = "StartDate";
        public const string MetaDataEndDate = "EndDate";
        public const string MetaDataDeadline = "Deadline";
        public const string MetaDataDeadlineType = "DeadlineType";
        public const string MetaDataPublicationType = "PublicationType";
        public const string MetaDataNumberOfPages = "NumberOfPages";
        public const string MetaDataAuthor = "Author";
        public const string MetaDataYear = "Year";
        public const string MetaDataIsbn = "Isbn";
        public const string MetaDataAttachmentInfo = "AttachmentInfo";
        public const string PreTagForHighlights = "<mark>";
        public const string PostTagForHighlights = "</mark>";

        public static class EmployeeSearch
        {
            public const string EmployedResourceType = "A";
        }

        public static class EventListFilter
        {
            public const string CheckboxVideoValue = "true";
        }

        public static class ProposalSearch
        {
            public const string TestActivity = "TEST";
        }

        public static string[] NorwegianStopwords = new string[] { "av", "den", "det", "ei", "en", "er", "et", "i", "og", "til" };
    }
}