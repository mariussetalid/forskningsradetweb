using System;
using Forskningsradet.Core.Logging;
using log4net;
using Serilog;
using Serilog.Events;

namespace Forskningsradet.Common.Logging
{
    public interface IIntegrationLogger
    {
        void LogError(string message, Exception exception, params object[] propertyValues);
        void LogWarning(string message, params object[] propertyValues);
        void LogInfo(string message, params object[] propertyValues);
        void LogDebug(string message, params object[] propertyValues);
        bool IsEnabled(LogLevel logLevel);
    }
    
    public class IntegrationLogger : IIntegrationLogger
    {
        private readonly ILoggingConfiguration _configuration;
        private ILog Logger;

        public IntegrationLogger(Type type, ILoggingConfiguration configuration)
        {
            _configuration = configuration;
            InitializeLogger(type);
        }

        private void InitializeLogger(Type type) =>
            Logger = !_configuration.UseSerilog
                ? LogManager.GetLogger(type)
                : null;

        public void LogError(string message, Exception exception, params object[] propertyValues)
        {
            if(_configuration.UseSerilog)
                Log.Error(exception, message, propertyValues);
            else
                LogWithLog4Net(LogLevel.Error, message, new {values = propertyValues}, exception);
        }

        public void LogWarning(string message, params object[] propertyValues)
        {
            if(_configuration.UseSerilog)
                Log.Warning(message, propertyValues);
            else
                LogWithLog4Net(LogLevel.Warning, message, new {values = propertyValues});
        }

        public void LogInfo(string message, params object[] propertyValues)
        {
            if(_configuration.UseSerilog)
                Log.Information(message, propertyValues);
            else
                LogWithLog4Net(LogLevel.Information, message, new {values = propertyValues});
        }

        public void LogDebug(string message, params object[] propertyValues)
        {
            if(_configuration.UseSerilog)
                Log.Debug(message, propertyValues);
            else
                LogWithLog4Net(LogLevel.Debug, message, new {values = propertyValues});
        }

        public bool IsEnabled(LogLevel logLevel)
        {
            return _configuration.UseSerilog
                ? Log.IsEnabled(MapLogLevel(logLevel))
                : IsEnabledWithLog4Net(logLevel);
        }

        private void LogWithLog4Net(LogLevel level, string message, object obj, Exception exception = null)
        {
            if (Logger is null)
                Logger = LogManager.GetLogger(typeof(IntegrationLogger));
            
            LogicalThreadContext.Properties["CustomObject"] = obj;
            
            switch (level)
            {
                case LogLevel.Error:
                    Logger.Error(message, exception);
                    break;
                case LogLevel.Warning:
                    Logger.Warn(message, exception);
                    break;
                case LogLevel.Information:
                    Logger.Info(message, exception);
                    break;
                case LogLevel.Debug:
                    Logger.Debug(message, exception);
                    break;
            }
            
            LogicalThreadContext.Properties["CustomObject"] = null;
        }
        
        private bool IsEnabledWithLog4Net(LogLevel logLevel)
        {
            switch (logLevel)
            {
                case LogLevel.Error:
                    return Logger.IsErrorEnabled;
                case LogLevel.Warning:
                    return Logger.IsWarnEnabled;
                case LogLevel.Information:
                    return Logger.IsInfoEnabled;
                case LogLevel.Debug:
                    return Logger.IsDebugEnabled;
                default:
                    return Logger.IsErrorEnabled;
            }
        }

        private static LogEventLevel MapLogLevel(LogLevel logLevel)
        {
            switch (logLevel)
            {
                case LogLevel.Error:
                    return LogEventLevel.Error;
                case LogLevel.Warning:
                    return LogEventLevel.Warning;
                case LogLevel.Information:
                    return LogEventLevel.Information;
                case LogLevel.Debug:
                    return LogEventLevel.Debug;
                default:
                    return LogEventLevel.Error;
            }
        }
    }
}