namespace Forskningsradet.Common.Logging
{
    public interface ILoggingConfiguration
    {
        bool UseSerilog { get; }
    }
}