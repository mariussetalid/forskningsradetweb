namespace Forskningsradet.Core.Logging
{
    public enum LogLevel
    {
        Error,
        Warning,
        Information,
        Debug
    }
}