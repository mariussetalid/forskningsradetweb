using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Reflection;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using System.Web.Http.ModelBinding.Binders;
using System.Web.Http.Routing;
using Forskningsradet.Core.Behavior;
using Forskningsradet.Web.Infrastructure;
using Microsoft.Web.Http.Routing;
using Swashbuckle.Application;

namespace Forskningsradet.Web
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            var constraintResolver = new DefaultInlineConstraintResolver {ConstraintMap = {["apiVersion"] = typeof(ApiVersionRouteConstraint)}};
            
            config.MapHttpAttributeRoutes(constraintResolver);
            config.Formatters.JsonFormatter.SupportedMediaTypes
                .Add(new MediaTypeHeaderValue("text/html"));

            var provider = new SimpleModelBinderProvider(typeof(GetContactPersons.Query), new ContactPersonsQueryModelBinder());
            config.Services.Insert(typeof(ModelBinderProvider), 0, provider);

            var environment = ConfigurationManager.AppSettings["episerver:EnvironmentName"];
            var environmentIsDevOrProd = new[] { "Integration", "Production" }.Contains(environment);
            if (!environmentIsDevOrProd)
            {
                config.AddApiVersioning();

                var apiExplorer = config.AddVersionedApiExplorer(
                    options =>
                    {
                        options.GroupNameFormat = "'v'VVV"; // "'v'major[.minor][-status]"
                        options.SubstituteApiVersionInUrl = true;
                    });

                config.EnableSwagger(
                        "{apiVersion}/swagger",
                        swagger =>
                        {
                            swagger.MultipleApiVersions(
                                (apiDescription, version) => apiDescription.GetGroupName() == version,
                                info =>
                                {
                                    foreach (var group in apiExplorer.ApiDescriptions.OrderByDescending(x => x.ApiVersion))
                                    {
                                        var description = "Integration API's.";

                                        if (group.IsDeprecated)
                                        {
                                            description += " This API version has been deprecated.";
                                        }

                                        info.Version(group.Name, $"Forskningsrådet Web API {group.ApiVersion}")
                                            .Description(description);
                                    }
                                });

                            // Integrate xml comments
                            swagger.IncludeXmlComments(XmlCommentsFilePath);

                            swagger.BasicAuth("basic");

                            // Excludes episerver find api's
                            swagger.DocumentFilter<LocalApiDocumentFilter>();
                            swagger.ResolveConflictingActions(a => a.First());
                        })
                    .EnableSwaggerUi(c =>
                    {
                        c.EnableDiscoveryUrlSelector();
                        c.InjectJavaScript(Assembly.GetAssembly(typeof(WebApiConfig)), "Forskningsradet.Web.Static.Scripts.basic-auth.js");
                    });
            }
        }

        private static string XmlCommentsFilePath
        {
            get
            {
                var basePath = System.AppDomain.CurrentDomain.RelativeSearchPath;
                var fileName = typeof(Startup).GetTypeInfo().Assembly.GetName().Name + ".xml";
                return Path.Combine(basePath, fileName);
            }
        }
    }
}