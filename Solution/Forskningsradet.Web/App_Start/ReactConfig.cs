using EPiServer.Framework;
using EPiServer.Framework.Initialization;
using JavaScriptEngineSwitcher.ChakraCore;
using JavaScriptEngineSwitcher.Core;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using React;
using React.TinyIoC;
using React.Web.TinyIoC;
using System.Collections.Generic;
using System.IO;
using System.Web;

namespace Forskningsradet.Web
{
    [InitializableModule]
    [ModuleDependency(typeof(EPiServer.Web.InitializationModule))]
    public class ReactConfig : IInitializableModule
    {
        public void Initialize(InitializationEngine context)
        {
            Initializer.Initialize(AsPerRequestSingleton);

            Manifest = ReactManifest.Load("~/Frontend/dist/", "manifest.json");

            JsEngineSwitcher.Current.EngineFactories.Clear();
            JsEngineSwitcher.Current.EngineFactories.AddChakraCore();
            JsEngineSwitcher.Current.DefaultEngineName = ChakraCoreJsEngine.EngineName;

            ReactSiteConfiguration.Configuration
                .SetJsonSerializerSettings(new JsonSerializerSettings
                {
                    //ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                    NullValueHandling = NullValueHandling.Ignore,
                    ContractResolver = new CamelCasePropertyNamesContractResolver(),
                    Converters = new List<JsonConverter> { new StringEnumConverter() }
                })
                .SetLoadBabel(false)
                .SetLoadReact(false)
                .AddScriptWithoutTransform(Manifest.ServerJs);
        }

        public static ReactManifest Manifest { get; private set; }

        private static TinyIoCContainer.RegisterOptions AsPerRequestSingleton(TinyIoCContainer.RegisterOptions registerOptions)
        {
            return TinyIoCContainer.RegisterOptions.ToCustomLifetimeManager(
                registerOptions,
                new HttpContextLifetimeProvider(),
                "per request singleton"
            );
        }

        public void Uninitialize(InitializationEngine context) { }

        public class ReactManifest
        {
            public string ForskningsradetCss { get; }

            public string RffCss { get; }

            public string NpCss { get; }

            public string KildenCss { get; }

            public string ServerJs { get; }

            public string VendorJs { get; }

            public string ClientJs { get; }

            protected internal static ReactManifest Load(string relativeRoot, string manifestFilename)
            {
                var manifestJson = File.ReadAllText(HttpContext.Current.Server.MapPath(Path.Combine(relativeRoot, manifestFilename)));
                var manifest = JsonConvert.DeserializeObject<Dictionary<string, string>>(manifestJson);

                return new ReactManifest(
                    BuildPath("server.js"),
                    BuildPath("client.js"),
                    BuildPath("vendor.js"),
                    BuildPath("colorThemeForskningsradet.css"),
                    BuildPath("colorThemeRff.css"),
                    BuildPath("colorThemeNp.css"),
                    BuildPath("colorThemeKilden.css"));

                string BuildPath(string filename) =>
                    Path.Combine(relativeRoot, manifest[filename]);
            }

            private ReactManifest(string serverJs, string clientJs, string vendorJs, string forskningsradetCss, string rffCss, string npCss, string kildenCss)
            {
                ServerJs = serverJs;
                ClientJs = clientJs;
                VendorJs = vendorJs;
                ForskningsradetCss = forskningsradetCss;
                RffCss = rffCss;
                NpCss = npCss;
                KildenCss = kildenCss;
            }
        }
    }
}