﻿using System;
using System.IdentityModel.Claims;
using System.Web.Helpers;
using System.Web.Http;
using System.Web.Mvc;
using EPiServer.Logging;
using EPiServer.ServiceLocation;
using Forskningsradet.Core.Logging;

namespace Forskningsradet.Web
{
    public class EPiServerApplication : EPiServer.Global
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            AntiForgeryConfig.UniqueClaimTypeIdentifier = ClaimTypes.NameIdentifier;
        }
        
        protected void Application_Error(object sender, EventArgs e)
            => ServiceLocator.Current.GetInstance<ILogger>().Log(Level.Error, "Caught error in global.asax.", Server.GetLastError());
        
        protected void Application_BeginRequest(object sender, EventArgs e)
            => log4net.LogicalThreadContext.Properties["CorrelationId"] = new CorrelationId();
    }
}