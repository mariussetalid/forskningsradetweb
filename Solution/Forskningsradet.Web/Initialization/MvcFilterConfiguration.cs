using System.Web.Mvc;
using EPiServer.Framework;
using EPiServer.Framework.Initialization;
using EPiServer.ServiceLocation;
using Forskningsradet.Web.Business.ActionFilters;

namespace Forskningsradet.Web.Initialization
{
    /// <summary>
    /// Module for adding MVC action filters.
    /// </summary>
    [ModuleDependency(typeof(EPiServer.Web.InitializationModule))]
    public class MvcFilterConfiguration : IInitializableModule
    {
        public void Initialize(InitializationEngine context)
        {
            GlobalFilters.Filters.Add(ServiceLocator.Current.GetInstance<FillBaseViewModelFilter>());
            GlobalFilters.Filters.Add(ServiceLocator.Current.GetInstance<HttpHeadersFilter>());
        }

        public void Uninitialize(InitializationEngine context)
        {
        }
    }
}