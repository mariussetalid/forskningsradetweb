using System.Web.Mvc;
using EPiServer.Framework;
using EPiServer.Framework.Initialization;
using System.Web.Routing;

namespace Forskningsradet.Web.Initialization
{
    [ModuleDependency(typeof(EPiServer.Web.InitializationModule))]
    public class MvcRoutesInitialization : IInitializableModule
    {
        public void Initialize(InitializationEngine context)
        {
            RegisterRoutes(RouteTable.Routes);
        }

        public void RegisterRoutes(RouteCollection routes)
        {
            routes.MapRoute(
                name: null,
                url: "AdminTools/import-content",
                defaults: new {controller = "ImportContentTool", action = "Index"});
            
            routes.MapRoute(
                name: "ProposalRoute",
                url: "utlysning/{id}/{language}",
                defaults: new {controller = "Proposal", action = "Index", language = UrlParameter.Optional});
            
            routes.MapRoute(
                name: "NotFoundRoute",
                url: "error/404",
                defaults: new { controller = "ErrorPage", action = "NotFound" });

            routes.MapRoute(
                name: "ErrorRoute",
                url: "error/500",
                defaults: new { controller = "ErrorPage", action = "Error" });

            routes.MapRoute(
                name: "GoneRoute",
                url: "error/410",
                defaults: new { Controller = "ErrorPage", action = "Gone" });
        }

        public void Uninitialize(InitializationEngine context)
        {
        }
    }
}