﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EPiServer.Framework;
using EPiServer.Framework.Blobs;
using EPiServer.Framework.Initialization;
using EPiServer.ServiceLocation;

namespace Forskningsradet.Web.Initialization
{
    [InitializableModule]
    [ModuleDependency(typeof(FrameworkInitialization))]
    public class DeveloperSpecificBlobProviderInitialization : IConfigurableModule
    {
        private const string DeveloperBlobsProviderPostfix = "-dev-blobs";

        public void ConfigureContainer(ServiceConfigurationContext context)
        {
            context.Services.Configure<BlobOptions>(o =>
            {
                var developerSpecificProviderName = Environment.MachineName + DeveloperBlobsProviderPostfix;
                foreach (var blobProviderBuilder in o.Providers.Values.ToList())
                {
                    if (blobProviderBuilder.Name.Equals(developerSpecificProviderName, StringComparison.InvariantCultureIgnoreCase))
                    {
                        o.DefaultProvider = blobProviderBuilder.Name;
                    }
                    else if (blobProviderBuilder.Name.EndsWith(DeveloperBlobsProviderPostfix, StringComparison.InvariantCultureIgnoreCase))
                    {
                        o.Providers.Remove(blobProviderBuilder.Name);
                    }
                }
            });
        }

        public void Initialize(InitializationEngine context) { }
        public void Uninitialize(InitializationEngine context) { }
    }

}