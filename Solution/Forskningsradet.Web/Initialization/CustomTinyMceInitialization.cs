﻿using EPiServer.Cms.TinyMce.Core;
using EPiServer.Framework;
using EPiServer.Framework.Initialization;
using EPiServer.ServiceLocation;

namespace Forskningsradet.Web.Initialization
{
    [ModuleDependency(typeof(TinyMceInitialization))]
    public class CustomTinyMceInitialization : IConfigurableModule
    {
        public void Initialize(InitializationEngine context)
        {
        }

        public void Uninitialize(InitializationEngine context)
        {
        }

        public void ConfigureContainer(ServiceConfigurationContext context) 
            => context.Services.Configure<TinyMceConfiguration>(config =>
                {
                    config.Default()
                        .AddPlugin("table")
                        .AddPlugin("anchor")
                        .Toolbar(DefaultValues.Toolbar + " | anchor | undo redo | table")
                        .AddSetting("table_appearance_options", false)
                        .AddSetting("table_default_attributes", new {})
                        .AddSetting("table_default_styles", new{})
                        .AddSetting("table_advtab", false)
                        .AddSetting("table_cell_advtab", false)
                        .AddSetting("table_row_advtab", false)
                        .AddSetting("invalid_elements", "h1,script,iframe");
                });
    }
}