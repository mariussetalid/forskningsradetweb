using System.Web;
using System.Web.Http;
using EPiServer.Framework;
using EPiServer.Framework.Initialization;
using EPiServer.ServiceLocation;
using Forskningsradet.Common.Logging;
using Forskningsradet.Core.Configuration;
using Forskningsradet.Core.Initialization;
using Forskningsradet.Core.Services;
using Forskningsradet.Core.Services.Contracts;
using Forskningsradet.ServiceAgents.Contracts;
using Forskningsradet.Web.Business.DependencyResolver;
using Forskningsradet.Web.Configuration;
using Forskningsradet.Web.Controllers.Api;
using StructureMap.Web;

namespace Forskningsradet.Web.Initialization
{
    /// <summary>
    /// Original code taken from Episerver template project. 
    /// </summary>
    [InitializableModule]
    [ModuleDependency(new[] { typeof(DependencyResolverConfiguration) })]
    public class DependencyResolverInitialization : IConfigurableModule
    {
        public void ConfigureContainer(ServiceConfigurationContext context)
        {
            var container = context.StructureMap();

            container.Configure(x =>
            {
                x.AddRegistry<AdGroupsToRolesMapperRegistry>();
                x.AddRegistry<MediatorRegistry>();
                x.For<IApiKeyStore>().HybridHttpOrThreadLocalScoped().Use<SimpleApiKeyStore>();
            });

            context.Services.AddSingleton<IHrManagerServiceAgentConfiguration, SiteConfiguration>();
            context.Services.AddSingleton<IArchivingServiceAgentConfiguration, SiteConfiguration>();
            context.Services.AddSingleton<IArchiveConfiguration, SiteConfiguration>();
            context.Services.AddSingleton<IApsisConfiguration, SiteConfiguration>();
            context.Services.AddSingleton<ICrmConfiguration, SiteConfiguration>();
            context.Services.AddSingleton<IGoogleMapsConfiguration, SiteConfiguration>();
            context.Services.AddSingleton<IGoogleTagManagerConfiguration, SiteConfiguration>();
            context.Services.AddSingleton<IPaginationConfiguration, SiteConfiguration>();
            context.Services.AddSingleton<ICacheConfiguration, SiteConfiguration>();
            context.Services.AddSingleton<IPdfConfiguration, SiteConfiguration>();
            context.Services.AddSingleton<IProposalPageConfiguration, SiteConfiguration>();
            context.Services.AddSingleton<IProjectDatabankConfiguration, SiteConfiguration>();
            context.Services.AddSingleton<ILoggingConfiguration, SiteConfiguration>();
            context.Services.AddSingleton<INtbServiceAgentConfiguration, SiteConfiguration>();
            context.Services.AddHttpContextOrThreadScoped<IArchiveService, ArchiveService>();
            context.Services.AddHttpContextOrThreadScoped<ISearchApiUrlResolver, SearchApiUrlResolver>();
            context.Services.AddHttpContextScoped<HttpRequestBase>(locator => new HttpRequestWrapper(HttpContext.Current.Request));
        }

        public void Initialize(InitializationEngine context)
        {
            var dependencyResolver = new ServiceLocatorDependencyResolver(context.Locate.Advanced);
            System.Web.Mvc.DependencyResolver.SetResolver(dependencyResolver);
            GlobalConfiguration.Configuration.DependencyResolver = dependencyResolver;
        }

        public void Uninitialize(InitializationEngine context)
        {
        }

        public void Preload(string[] parameters)
        {
        }
    }
}
