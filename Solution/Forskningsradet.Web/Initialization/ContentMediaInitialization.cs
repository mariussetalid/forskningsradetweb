using System.IO;
using EPiServer;
using EPiServer.Core;
using EPiServer.Framework;
using EPiServer.Framework.Initialization;
using EPiServer.ServiceLocation;
using Forskningsradet.Core.Extensions;
using Forskningsradet.Core.Models.ContentModels.Contracts;

namespace Forskningsradet.Web.Initialization
{
    /// <summary>
    /// Original code inspired by https://www.luminary.com/blog/image-metadata-svg-thumbnails-episerver-dxc. 
    /// </summary>
    [InitializableModule]
    [ModuleDependency(typeof(EPiServer.Web.InitializationModule))]
    public class ContentMediaInitialization : IInitializableModule
    {
        public void Initialize(InitializationEngine context)
        {
            var eventRegistry = ServiceLocator.Current.GetInstance<IContentEvents>();

            eventRegistry.CreatingContent += OnCreatingContent;
            eventRegistry.SavingContent += OnSavingContent;
        }

        public void Uninitialize(InitializationEngine context)
        {
            var eventRegistry = ServiceLocator.Current.GetInstance<IContentEvents>();

            eventRegistry.CreatingContent -= OnCreatingContent;
            eventRegistry.SavingContent -= OnSavingContent;
        }

        public void OnCreatingContent(object sender, ContentEventArgs e)
        {
            if (e.Content is MediaData media)
            {
                if (media is IContentMediaMetaData data)
                {
                    data.FileSize = media.GetFileSize();
                    data.FileExtension = Path.GetExtension(media.Name);
                }
            }
        }

        public void OnSavingContent(object sender, ContentEventArgs e)
        {
            if (e.Content is MediaData media)
            {
                if (media is IContentMediaMetaData data)
                {
                    data.FileSize = media.GetFileSize();
                    data.FileExtension = Path.GetExtension(media.Name);
                }
            }
        }
    }
}
