using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using EPiServer.Find.UnifiedSearch;

namespace Forskningsradet.Web.Initialization
{
    // Based on: https://world.episerver.com/forum/developer-forum/EPiServer-Search/Thread-Container/2018/10/unifiedsearchregistry-throws-loaderexceptions-with-missing-assemblies/
    public static class CustomUnifiedSearchRegistryExtension
    {
        public static UnifiedSearchTypeRulesBuilder<T> ForInstanceOfWithErrorHandling<T>(this IUnifiedSearchRegistry registry)
        {
            var type = typeof(T);
            var assemblies = (IEnumerable<Assembly>) AppDomain.CurrentDomain.GetAssemblies();
            var assembliesWhere = assemblies.Where(x => !x.IsDynamicAssembly()).ToList();

            var listOfGetTypes = new List<Type>();
            foreach (var assembly in assembliesWhere)
            {
                try
                {
                    var getMany = assembly.GetTypes() as IEnumerable<Type>;
                    listOfGetTypes.AddRange(getMany);
                }
                catch(ReflectionTypeLoadException reflectionTypeLoadException)
                {
                    // Some assemblies could not be loaded - These are null in Types property.
                    // https://docs.microsoft.com/en-us/dotnet/api/system.reflection.assembly.gettypes?view=netframework-4.7.2
                    var typesWhichCouldBeLoaded = reflectionTypeLoadException.Types.Where(x => x != null);
                    listOfGetTypes.AddRange(typesWhichCouldBeLoaded);
                }
                catch (Exception)
                {
                    // ignored
                }
            }

            var assembliesSelectManyWhere = listOfGetTypes.Where(x =>
            {
                if (type.IsAssignableFrom(x))
                {
                    return !registry.Contains(x);
                }
                return false;
            });

            foreach (var assemblyType in assembliesSelectManyWhere)
            {
                registry.Add(assemblyType);
            }

            return new UnifiedSearchTypeRulesBuilder<T>(registry);
        }
    }
}