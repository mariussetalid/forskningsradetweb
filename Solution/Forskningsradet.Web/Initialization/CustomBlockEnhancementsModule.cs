﻿using EPiServer.Framework;
using EPiServer.Framework.Initialization;
using EPiServer.Labs.BlockEnhancements;
using EPiServer.ServiceLocation;
using EPiServer.Shell.ObjectEditing.EditorDescriptors;

namespace Forskningsradet.Web.Initialization
{
    /// <summary>
    /// See more here: https://github.com/episerver/episerver.labs.blockenhancements#configuring-enabled-features
    /// </summary>
    [InitializableModule]
    [ModuleDependency(typeof(FrameworkInitialization))]
    public class CustomBlockEnhancementsModule : IConfigurableModule
    {
        public void ConfigureContainer(ServiceConfigurationContext context)
        {
            context.Services.Configure<BlockEnhancementsOptions>(options =>
            {
                options.PublishWithLocalContentItems = true;
                options.ContentDraftView = false; // Disabled because does not working good with EPiServer.CMS.UI v 11.32.1
                options.InlinePublish = true;
                options.StatusIndicator = true;
                options.ContentAreaSettings = new ContentAreaSettings
                {
                    ContentAreaBrowse = true,
                    ContentAreaEditorDescriptorBehavior = EditorDescriptorBehavior.OverrideDefault
                };
                options.InlineTranslate = true;
            });
        }

        public void Initialize(InitializationEngine context) { }

        public void Uninitialize(InitializationEngine context) { }
    }
}