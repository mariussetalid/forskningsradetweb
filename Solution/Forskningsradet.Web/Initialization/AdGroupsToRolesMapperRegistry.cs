﻿using Creuna.AzureAD;
using Creuna.AzureAD.Configuration;
using Creuna.AzureAD.Configuration.ConfigFile;
using Creuna.AzureAD.Configuration.Episerver;
using Creuna.AzureAD.Contracts;
using Creuna.AzureAD.Utils.FeatureToggles;
using StructureMap;

namespace Forskningsradet.Web.Initialization
{
    public class AdGroupsToRolesMapperRegistry : Registry
    {
        public AdGroupsToRolesMapperRegistry()
        {
            if (!new FeatureToggleDisableAzureAD().FeatureEnabled)
            {

                if (new FeatureToggleForseJsonConfig().FeatureEnabled)
                {
                    JsonConfigAdGroupsToRolesMapper();
                }
                else
                {
                    EpiserverAdGroupsToRolesMapper();
                }
            }
        }

        private void JsonConfigAdGroupsToRolesMapper()
        {
            CommonComponents();

            For<AzureAdSecurityConfigurationFileProvider>().Singleton();
            For<IAzureAdSecuritySettingsProvider>().Singleton().Use(ctx => ctx.GetInstance<AzureAdSecurityConfigurationFileProvider>());
            For<ICustomVirtualRolesProvider>().Singleton().Use(ctx => ctx.GetInstance<AzureAdSecurityConfigurationFileProvider>());
        }

        private void EpiserverAdGroupsToRolesMapper()
        {
            CommonComponents();

            For<AzureAdSecurityConfigurationFileProvider>().Singleton();
            For<AzureAdSecurityEpiserverProvider>().Singleton().Use<AzureAdSecurityEpiserverProvider>()
                .Ctor<IAzureAdSecuritySettingsProvider>().Is<AzureAdSecurityConfigurationFileProvider>();
            For<IAzureAdSecuritySettingsProvider>().Singleton().Use(ctx => ctx.GetInstance<AzureAdSecurityEpiserverProvider>());
            For<ICustomVirtualRolesProvider>().Singleton().Use(ctx => ctx.GetInstance<AzureAdSecurityEpiserverProvider>());
        }

        private void CommonComponents()
        {
            For<IIdentityUpdater>().Singleton().Use<AdGroupsToRolesIdentityUpdater>();
            For<ICustomVirtualRolesWatcher>().Singleton().Use<RolesWatcher>();
        }
    }
}