using System.Linq;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAccess;
using EPiServer.Framework;
using EPiServer.Framework.Initialization;
using EPiServer.Security;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.Validation;
using Forskningsradet.Core.Services;
using Forskningsradet.Core.Services.Contracts;

namespace Forskningsradet.Web.Initialization
{
    [InitializableModule]
    [ModuleDependency(typeof(EPiServer.Web.InitializationModule))]
    public class PublishEventInitialization : IInitializableModule
    {
        private IProposalValidationService _proposalValidationService;
        private IArchiveService _archiveService;
        private IArchiveValidationService _archiveValidationService;
        private IContentRepository _contentRepository;

        public void Initialize(InitializationEngine context)
        {
            var events = context.Locate.ContentEvents();
            events.PublishingContent += ValidateContent;
            events.PublishingContent += ValidateArchiving;
            events.PublishedContent += ArchiveContent;
            
            _proposalValidationService = context.Locate.Advanced.GetInstance<IProposalValidationService>();
            _archiveService = context.Locate.Advanced.GetInstance<IArchiveService>();
            _archiveValidationService = context.Locate.Advanced.GetInstance<IArchiveValidationService>();
            _contentRepository = context.Locate.Advanced.GetInstance<IContentRepository>();
        }
        
        public void Uninitialize(InitializationEngine context)
        {
            var events = context.Locate.ContentEvents();
            events.PublishingContent -= ValidateContent;
            events.PublishingContent -= ValidateArchiving;
            events.PublishedContent -= ArchiveContent;
        }
        
        private void ValidateContent(object sender, ContentEventArgs e)
        {
            if (e.Content is ProposalPage page)
            {
                var validationResult = _proposalValidationService.Validate(page);
                ApplyValidationResult(e, validationResult);
            }
            else if (e.Content is MediaBlock mediablock)
            {
                if (mediablock.EmbedBlock.Src != null && mediablock.EmbedBlock.AlternativeText is null)
                {
                    e.CancelAction = true;
                    e.CancelReason = "Hvis Embed lenke skal brukes s� m� alternativ tekst v�re fyllt ut (UU-krav)";
                }
            }
            else if (e.Content is StructuredScheduleBlock scheduleBlock)
            {
                if (scheduleBlock.ProgramList?.Any() == true)
                {
                    if (scheduleBlock.ProgramList.Any(x => !x.Time.Contains(":")))
                    {
                        e.CancelAction = true;
                        e.CancelReason = "All times in the program list must be on the format [hh:mm]. One or more times does not contain ':'.";
                    }
                    var lastElementIsNotPauseActivity = !scheduleBlock.ProgramList.Last().IsBreak;
                    if (lastElementIsNotPauseActivity)
                    {
                        e.CancelAction = true;
                        e.CancelReason = "The last element in the program list must be a pause activity.";
                    }
                }
                else
                {
                    e.CancelAction = true;
                    e.CancelReason = "Program list must contain at least one activity.";
                }
            }
            else if (e.Content is RelatedContentListBlock relatedContentListBlock)
            {
                if (!relatedContentListBlock.PortfolioMode && relatedContentListBlock.Publications?.FilteredItems.Any() == true)
                {
                    e.CancelAction = true;
                    e.CancelReason = "Hvis publikasjoner skal brukes m� Portef�ljevisning v�re skrudd p�.";
                }
            }
        }

        private void ValidateArchiving(object sender, ContentEventArgs e)
        {
            if (e.Content is EditorialPage page)
            {
                var validationResult = _archiveValidationService.Validate(page);
                ApplyValidationResult(e, validationResult);
            }
        }

        private static void ApplyValidationResult(ContentEventArgs e, ValidationResult validationResult)
        {
            e.CancelAction = validationResult.HasErrors;
            e.CancelReason = !string.IsNullOrEmpty(e.CancelReason)
                ? $"{e.CancelReason}. {string.Join(". ", validationResult.ErrorMessages)}"
                : string.Join(". ", validationResult.ErrorMessages);
        }

        private void ArchiveContent(object sender, ContentEventArgs e)
        {
            var result = _archiveService.Archive(e.Content);
            
            if (result.Archivable && !result.Success)
            {
                e.CancelAction = true;
                e.CancelReason = result.Message;
            }
        }
    }
}