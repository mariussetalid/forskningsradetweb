using System.Collections.Generic;
using EPiServer.Core;
using EPiServer.Find;
using EPiServer.Find.ClientConventions;
using EPiServer.Find.Cms;
using EPiServer.Find.Cms.Conventions;
using EPiServer.Find.Framework;
using EPiServer.Framework;
using EPiServer.Framework.Initialization;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ContentModels.Media;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.SearchModels;

namespace Forskningsradet.Web.Initialization
{
    [ModuleDependency(typeof(EPiServer.Find.Cms.Module.IndexingModule))]
    public class FindInitialization : IInitializableModule
    {
        public void Initialize(InitializationEngine context)
        {
            var contentIndexer = ContentIndexer.Instance;
            contentIndexer.ContentBatchSize = 37;
            contentIndexer.Conventions.ForInstancesOf<BaseBlockData>().IndexingInContentAreas(x => true);
            contentIndexer.Conventions.ForInstancesOf<ImageFile>().ShouldIndex(x => false);
            contentIndexer.Conventions.ForInstancesOf<RssPage>().ShouldIndex(x => false);
            contentIndexer.Conventions.ForInstancesOf<ApplicationResultPage>().ShouldIndex(x => false);

            var searchClient = SearchClient.Instance;
            searchClient.Conventions.ForInstancesOf<IContentMedia>().ExcludeField(x => x.SearchAttachment());

            var registry = searchClient.Conventions.UnifiedSearchRegistry;

            registry
                .ForInstanceOfWithErrorHandling<GenericMedia>()
                .ProjectTypeNameFrom(x => ((int)SearchResultType.File).ToString());

            registry
                .ForInstanceOfWithErrorHandling<PersonPage>()
                .ProjectUrlFrom<PersonPage>(x => null)
                .ProjectMetaDataFrom(x => new Dictionary<string, IndexValue>
                {
                    {SearchConstants.MetaDataJobTitle, x.JobTitle},
                    {SearchConstants.MetaDataDepartment, x.Department},
                    {SearchConstants.MetaDataEmail, x.Email}
                })
                .AlwaysApplyFilter(f => f
                    .BuildFilter<PersonPage>()
                    .And(x => x.ResourceType.Match(SearchConstants.EmployeeSearch.EmployedResourceType)));

            registry
                .ForInstanceOfWithErrorHandling<EventPage>()
                .ProjectMetaDataFrom(x => new Dictionary<string, IndexValue>
                {
                    {SearchConstants.MetaDataStartDate, x.ComputedEventDateAsString},
                    {SearchConstants.MetaDataEndDate, x.ComputedEventEndDateAsString}
                });

            registry
                .ForInstanceOfWithErrorHandling<ProposalPage>()
                .ProjectMetaDataFrom(x => new Dictionary<string, IndexValue>
                {
                    {SearchConstants.MetaDataDeadline, x.DeadlineComputedAsString},
                    {SearchConstants.MetaDataDeadlineType, ((int)x.DeadlineType).ToString()}
                });

            registry
                .ForInstanceOfWithErrorHandling<VacancyPage>()
                .ProjectUrlFrom<VacancyPage>(x => x.HrManagerUri.ToString());

            registry
                .ForInstanceOfWithErrorHandling<EditorialPage>()
                .ProjectTypeNameFrom(x => ((int)x.ComputedSearchResultType).ToString());

            registry
                .ForInstanceOfWithErrorHandling<ArticlePage>()
                .ProjectHighlightedExcerptUsing<ArticlePage>(x => y => $"{y.MainIntro} {y.MainBody}");

            registry
                .ForInstanceOfWithErrorHandling<PressReleasePage>()
                .ProjectHighlightedExcerptUsing<PressReleasePage>(x => y => $"{y.Lead}");

            registry
                .ForInstanceOfWithErrorHandling<VacancyPage>()
                .ProjectTypeNameFrom(x => ((int)SearchResultType.None).ToString());

            registry
                .ForInstanceOfWithErrorHandling<PublicationPage>()
                .ProjectTypeNameFrom(x => ((int)SearchResultType.Publication).ToString())
                .ProjectExcerptUsing<PublicationPage>(x => y => y.SearchSummary)
                .ProjectMetaDataFrom(x => new Dictionary<string, IndexValue>
                {
                    {SearchConstants.MetaDataPublicationType, ((int)x.PublicationType).ToString()},
                    {SearchConstants.MetaDataNumberOfPages, x.NumberOfPages.ToString()},
                    {SearchConstants.MetaDataAuthor, x.Author},
                    {SearchConstants.MetaDataYear, x.Year.ToString()},
                    {SearchConstants.MetaDataIsbn, x.Isbn},
                    {SearchConstants.MetaDataAttachmentInfo, x.HasAttachment ? x.Attachment.ID.ToString() : string.Empty}
                });

            registry
                .ForInstanceOfWithErrorHandling<HistoricProposalPage>()
                .ProjectTypeNameFrom(x => ((int)SearchResultType.None).ToString());

            registry
                .ForInstanceOfWithErrorHandling<WordPage>()
                .ProjectUrlFrom<WordPage>(x => null)
                .ProjectTypeNameFrom(x => ((int)SearchResultType.None).ToString());
        }

        public void Uninitialize(InitializationEngine context)
        {

        }
    }
}