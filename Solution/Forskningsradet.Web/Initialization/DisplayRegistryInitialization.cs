﻿using EPiServer.Framework;
using EPiServer.Framework.Initialization;
using EPiServer.ServiceLocation;
using EPiServer.Web;
using Forskningsradet.Common.Constants;

namespace Forskningsradet.Web.Initialization
{
    [ModuleDependency(typeof(EPiServer.Web.InitializationModule))]
    public class DisplayRegistryInitialization : IInitializableModule
    {
        public void Initialize(InitializationEngine context)
        {
            if (context.HostType == HostType.WebApplication)
            {
                var options = ServiceLocator.Current.GetInstance<DisplayOptions>();
                options
                    .Add("wide", "/displayoptions/wide", ContentAreaTags.FullWidth, "Utfallende", "epi-icon__layout--full")
                    .Add("full", "/displayoptions/full", ContentAreaTags.NoRenderer, "Full bredde (standard)", "epi-icon__layout--full")
                    .Add("twothirds", "/displayoptions/twothirds", ContentAreaTags.TwoThirdsWidth, "2/3 bredde", "epi-icon__layout--two-third") //TODO: check if this icon exist
                    .Add("half", "/displayoptions/half", ContentAreaTags.HalfWidth, "Halv bredde", "epi-icon__layout--half")
                    .Add("narrow", "/displayoptions/narrow", ContentAreaTags.OneThirdWidth, "1/3 bredde", "epi-icon__layout--one-third");
            }
        }

        public void Uninitialize(InitializationEngine context)
        {
        }
    }
}