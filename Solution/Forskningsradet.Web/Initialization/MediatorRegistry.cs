using Forskningsradet.Core.Behavior;
using MediatR;
using StructureMap;

namespace Forskningsradet.Web.Initialization
{
    public class MediatorRegistry : Registry
    {
        public MediatorRegistry()
        {
            Scan(scanner =>
                {
                    scanner.AssemblyContainingType<GetContactPerson>();
                    scanner.ConnectImplementationsToTypesClosing(typeof(IRequestHandler<,>));
                    scanner.ConnectImplementationsToTypesClosing(typeof(INotificationHandler<>));
                });
            For<ServiceFactory>().Use<ServiceFactory>(ctx => ctx.GetInstance);
            For<IMediator>().Use<Mediator>();
        }
    }
}