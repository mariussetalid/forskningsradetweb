﻿using System.Collections.Generic;
using System.Web.Mvc;
using Forskningsradet.Core.HttpHeaders;
using Forskningsradet.Core.HttpHeaders.HstsHeader;

namespace Forskningsradet.Web.Business.ActionFilters
{
    public class HttpHeadersFilter : IResultFilter
    {
        private readonly List<IHttpHeaderRenderer> _httpHeaderRenderers;

        public HttpHeadersFilter(HstsHeaderRenderer hstsHeaderRenderer)
            => _httpHeaderRenderers = new List<IHttpHeaderRenderer> { hstsHeaderRenderer };

        public void OnResultExecuting(ResultExecutingContext filterContext)
        {
            // do nothing here.
        }

        public void OnResultExecuted(ResultExecutedContext filterContext)
        {
            foreach (var headerRenderer in _httpHeaderRenderers)
                headerRenderer.RenderHeader(filterContext.HttpContext.Response);

        }
    }
}