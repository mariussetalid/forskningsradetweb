using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Extensions;
using Forskningsradet.Core.Factories;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.ViewModels.Pages;
using Forskningsradet.Core.Models.ViewModels.Shared;
using Forskningsradet.Core.Repositories.Contracts;
using Forskningsradet.Core.Services.Contracts;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Web.Business.ActionFilters
{
    /// <summary>
    /// Filter that intercepts all page requests and fills common data in the base model so
    /// that pages do not have to duplicate this functionality.
    /// </summary>
    public class FillBaseViewModelFilter : IResultFilter
    {
        private readonly IPageRepository _pageRepository;
        private readonly IViewModelFactory _viewModelFactory;
        private readonly UrlHelper _urlHelper;
        private readonly CategoryRepository _categoryRepository;
        private readonly IImageMetadataService _imageMetadataService;

        public FillBaseViewModelFilter(IPageRepository pageRepository, IViewModelFactory viewModelFactory, UrlHelper urlHelper, CategoryRepository categoryRepository, IImageMetadataService imageMetadataService)
        {
            _pageRepository = pageRepository;
            _viewModelFactory = viewModelFactory;
            _urlHelper = urlHelper;
            _categoryRepository = categoryRepository;
            _imageMetadataService = imageMetadataService;
        }

        public void OnResultExecuting(ResultExecutingContext filterContext)
        {
            if (!(filterContext.Controller.ViewData.Model is BasePageViewModel viewModel))
                return;
            FillLayoutProperties(viewModel);
        }

        private void FillLayoutProperties(BasePageViewModel basePageViewModel)
        {
            var currentPageReference = basePageViewModel.CurrentBasePage.PageLink;
            if (_pageRepository.GetClosestFrontPage(currentPageReference) is FrontPageBase currentFrontPage)
            {
                basePageViewModel.LayoutViewModel = BuildLayout(currentFrontPage, currentPageReference);
                basePageViewModel.MetadataViewModel.OpenGraphImage = BuildOpenGraphImage(basePageViewModel.MetadataViewModel.SeoSettings, currentFrontPage);
                basePageViewModel.MetadataViewModel.FacebookAppId = currentFrontPage.SeoFacebookAppId;
                basePageViewModel.MetadataViewModel.TwitterUser = currentFrontPage.SeoTwitterSite;
            }
            basePageViewModel.MetadataViewModel.Idio = basePageViewModel.CurrentBasePage.Category is CategoryList currentCategories
                ? new MetadataIdioViewModel
                {
                    Subjects = GetCategories(currentCategories, CategoryConstants.SubjectRoot),
                    TargetGroups = GetCategories(currentCategories, CategoryConstants.TargetGroupRoot),
                    EventType = (basePageViewModel.CurrentBasePage as EventPage)?.ComputedEventType,
                    ApplicationType = (basePageViewModel.CurrentBasePage as ProposalBasePage)?.ApplicationTypeReference?.ID.ToString()
                }
                : new MetadataIdioViewModel();
        }

        public void OnResultExecuted(ResultExecutedContext filterContext)
        { }

        private MetadataImageViewModel BuildOpenGraphImage(SeoSettingsBlock seoSettings, FrontPageBase currentFrontPage)
        {
            var imageUrl = seoSettings?.OpenGraphImage != null
                ? _urlHelper.AbsoluteUrl(seoSettings.OpenGraphImage)
                : ContentReference.IsNullOrEmpty(currentFrontPage.FallbackSeoImage)
                    ? string.Empty
                    : _urlHelper.AbsoluteUrl(currentFrontPage.FallbackSeoImage);

            return string.IsNullOrEmpty(imageUrl)
                ? null
                : new MetadataImageViewModel
                {
                    Url = imageUrl,
                    Size = _imageMetadataService.TryGetImageSize(imageUrl)
                };
        }

        private string GetCategories(CategoryList categoryList, string parentCategory)
        {
            if (categoryList is null)
                return string.Empty;

            var categories = categoryList
                .Select(x => _categoryRepository.Get(x))
                .Where(x => x.Parent.Name == parentCategory)
                .Select(x => x.Name);

            return categories.Any()
                ? string.Join(",", categories)
                : string.Empty;
        }

        private LayoutViewModel BuildLayout(FrontPageBase currentFrontPage, PageReference currentPageReference) =>
            _viewModelFactory.GetLayoutViewModel(currentFrontPage, currentPageReference);
    }
}
