﻿using EPiServer.DataAbstraction;
using EPiServer.ServiceLocation;
using EPiServer.Web;
using EPiServer.Web.Mvc;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.Contracts;

namespace Forskningsradet.Web.Business.Rendering
{
    /// <summary>
    /// Starting point for custom template actions. 
    /// Original code taken from Episerver template project. 
    /// </summary>
    [ServiceConfiguration(typeof(IViewTemplateModelRegistrator))]
    public class TemplateCoordinator : IViewTemplateModelRegistrator
    {
        public const string BlockFolder = "~/Views/Blocks/";
        public const string PageFolder = "~/Views/Pages/";

        public static void OnTemplateResolved(object sender, TemplateResolverEventArgs args)
        {
            //Disable DefaultPageController for page types that shouldn't have any renderer as pages
            if (args.ItemToRender is IPageHasNoView)
            {
                args.SelectedTemplate = null;
            }
        }

        /// <summary>
        /// Registers renderers/templates which are not automatically discovered, 
        /// i.e. partial views whose names does not match a content type's name.
        /// </summary>
        /// <remarks>
        /// Using only partial views instead of controllers for blocks and page partials
        /// has performance benefits as they will only require calls to RenderPartial instead of
        /// RenderAction for controllers.
        /// Registering partial views as templates this way also enables specifying tags and 
        /// that a template supports all types inheriting from the content type/model type.
        /// </remarks>
        public void Register(TemplateModelCollection viewTemplateModelRegistrator)
        {
            viewTemplateModelRegistrator.Add(typeof(BaseBlockData), new TemplateModel
            {
                Path = "~/Views/Shared/ReactBlockView.cshtml",
                Inherit = true
            });
        }
    }
}