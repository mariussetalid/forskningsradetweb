﻿using System.Linq;
using System.Web.Mvc;

namespace Forskningsradet.Web.Business.Rendering
{
    /// <summary>
    /// Extends the Razor view engine to include the folders ~/Views/Blocks/ and ~/Views/Pages/
    /// when looking for views.
    /// Original code taken from Episerver template project. 
    /// </summary>
    public class SiteViewEngine : RazorViewEngine
    {
        private static readonly string[] AdditionalPartialViewFormats = {
            TemplateCoordinator.BlockFolder + "{0}.cshtml"
        };

        private static readonly string[] AdditionalPageViewFormats = {
            TemplateCoordinator.PageFolder + "{0}.cshtml"
        };

        public SiteViewEngine()
        {
            ViewLocationFormats = ViewLocationFormats.Union(AdditionalPageViewFormats).ToArray();
            PartialViewLocationFormats = PartialViewLocationFormats.Union(AdditionalPartialViewFormats).ToArray();
        }
    }
}