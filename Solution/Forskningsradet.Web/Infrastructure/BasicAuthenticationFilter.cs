using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Principal;
using System.Text;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using EPiServer.Security;
using EPiServer.ServiceLocation;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Services;

namespace Forskningsradet.Web.Infrastructure
{
    public class BasicAuthenticationFilter : AuthorizationFilterAttribute
    {
        private const string Realm = "forskningsradet.no";
        
        public Injected<IApiKeyStore> ApiKeyStore { private get; set; }

        public override void OnAuthorization(HttpActionContext context)
        {
            var request = context.Request;
            var authHeader = request.Headers.Authorization;
            if (authHeader != null && authHeader.Scheme == "Basic")
            {
                var credentials = ParseAuthenticationHeader(authHeader);

                if (credentials is null)
                {
                    Challenge(context);
                    return;
                }

                if (!AuthenticateUser(credentials))
                {
                    Challenge(context);
                    return;
                }
            }
            else
            {
                Challenge(context);
                return;
            }
            
            base.OnAuthorization(context);
        }

        private static NetworkCredential ParseAuthenticationHeader(AuthenticationHeaderValue header)
        {
            NetworkCredential credentials;
            try
            {
                var encoding = Encoding.GetEncoding("iso-8859-1");
                var credentialString = encoding.GetString(Convert.FromBase64String(header.Parameter));

                var separator = credentialString.IndexOf(':');
                var username = credentialString.Substring(0, separator);
                var password = credentialString.Substring(separator + 1);
                credentials = new NetworkCredential(username, password);
            }
            catch (Exception)
            {
                return null;
            }

            return credentials;
        }
        
        private bool AuthenticateUser(NetworkCredential credentials)
        {
            var secret = ApiKeyStore.Service.FindSecretByUsername(credentials.UserName);

            if (string.IsNullOrEmpty(secret) || secret != credentials.Password)
                return false;
            
            var identity = new GenericIdentity(credentials.UserName);
            var principal = new GenericPrincipal(identity, new []{EpiserverRoleConstants.ProposalAdmin});
            PrincipalInfo.CurrentPrincipal = principal;
            
            return true;
        }
        
        private static void Challenge(HttpActionContext context)
        {
            context.Response = context.Request.CreateResponse(HttpStatusCode.Unauthorized);
            context.Response.Headers.Add("WWW-Authenticate", $"Basic realm=\"{Realm}\"");
        }
    }
}