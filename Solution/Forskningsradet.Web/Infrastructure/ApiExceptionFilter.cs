using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Web.Http.Filters;
using EPiServer.ServiceLocation;
using EPiServer.Shell.Configuration;
using Forskningsradet.Common.Logging;

namespace Forskningsradet.Web.Infrastructure
{
    public class ApiExceptionFilter : ExceptionFilterAttribute 
    {
        public Injected<IIntegrationLogger> IntegrationLogger { private get; set; }
        
        public override void OnException(HttpActionExecutedContext context)
        {
            if (context.Exception is MissingConfigurationException)
            {
                context.Response = GetResponseMessage(HttpStatusCode.ServiceUnavailable, context.Exception.Message);
                IntegrationLogger.Service.LogError(context.Exception.Message, context.Exception);
            }
            else if (context.Exception != null)
            {
                context.Response = GetResponseMessage(HttpStatusCode.InternalServerError, context.Exception.Message);
                IntegrationLogger.Service.LogError(context.Exception.Message, context.Exception);
            }
        }
        
        private static HttpResponseMessage GetResponseMessage(HttpStatusCode code, string reason)
            => new HttpResponseMessage(code) {ReasonPhrase = Regex.Replace(reason, @"\t|\n|\r", "")};
    }
}