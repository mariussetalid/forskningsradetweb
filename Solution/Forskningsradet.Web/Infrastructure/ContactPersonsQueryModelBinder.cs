using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.ModelBinding;
using Forskningsradet.Core.Behavior;
using Forskningsradet.Core.Models.ApiModels.Idm;

namespace Forskningsradet.Web.Infrastructure
{
    public class ContactPersonsQueryModelBinder : IModelBinder
    {
        private const string QueryFilterParam = "_queryFilter";

        public bool BindModel(HttpActionContext actionContext, ModelBindingContext bindingContext)
        {
            if (bindingContext.ModelType != typeof(GetContactPersons.Query))
            {
                return false;
            }

            var query = actionContext.Request.RequestUri.Query;
            var contactPersonsQuery = new GetContactPersons.Query();

            if (string.IsNullOrEmpty(query) && !query.Contains(QueryFilterParam))
            {
                bindingContext.Model = contactPersonsQuery;
                return true;
            }

            query = HttpUtility.UrlDecode(query);

            var possibleIds = GetPossibleIds(query);

            foreach (var possibleId in possibleIds)
            {
                var externalId = new IdmContactPersonId(possibleId);

                if (externalId.IsValid)
                {
                    contactPersonsQuery.SearchTerms.Add(externalId.HasResourceId ? externalId.ResourceId.ToString() : externalId.Guid.ToString());
                }
            }

            bindingContext.Model = contactPersonsQuery;
            return true;
        }

        private static IEnumerable<string> GetPossibleIds(string query) =>
            Regex.Matches(query, "\"([^\"]*)\"")
                .Cast<Match>()
                .Select(m => m.Groups[1].ToString());
    }
}