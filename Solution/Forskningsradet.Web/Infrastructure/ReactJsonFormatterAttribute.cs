﻿using System;
using System.Net.Http.Formatting;
using System.Web.Http.Controllers;
using React;

namespace Forskningsradet.Web.Infrastructure
{
    public class ReactJsonFormatterAttribute : Attribute, IControllerConfiguration
    {
        public void Initialize(HttpControllerSettings settings, HttpControllerDescriptor descriptor)
        {
            settings.Formatters.Clear();
            JsonMediaTypeFormatter jsonFormatter = new JsonMediaTypeFormatter
            {
                SerializerSettings = ReactSiteConfiguration.Configuration.JsonSerializerSettings
            };

            settings.Formatters.Add(jsonFormatter);
        }
    }
}