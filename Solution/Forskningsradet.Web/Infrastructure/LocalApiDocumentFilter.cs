using System.Linq;
using System.Web.Http.Description;
using Swashbuckle.Swagger;

namespace Forskningsradet.Web.Infrastructure
{
    public class LocalApiDocumentFilter : IDocumentFilter
    {
        public void Apply(SwaggerDocument swaggerDoc, SchemaRegistry schemaRegistry, IApiExplorer apiExplorer)
        {
            swaggerDoc.paths = swaggerDoc.paths
                .Where(entry => entry.Key.ToLower().StartsWith("/api"))
                .ToDictionary(entry => entry.Key, entry => entry.Value);
        }
    }
}