# Forskningsrådet frontend

You need Node.js, [yarn](https://yarnpkg.com/lang/en/), Python and `windows-build-tools` or XCode command line tools in order to build the frontend.

`yarn` in root dir

`yarn dev` for webpack dev server

`yarn build` to build for production

## Folder structure

The production website is served from the parent folder of this folder. When building for production, asset paths are prefixed with the path '/Frontend/dist' in order for the paths to be correct in production. This means that if the web root, the Frontend folder or the build folder (Frontend/dist) is moved or renamed, the 'env.prefix' option in the `build` script in `package.json` has to be updated to reflect the correct path.

## Dependencies

The build server is at the time of writing set up to run `yarn audit` before building this codebase, which means if there are known vulnerabilities in `node_modules` these _have_ to be fixed before new code can be deployed to production.

## CSS naming

This project uses a BEM-like naming convention. The differences compared to BEM are:

- Double dash instead of double underscore. (Jumping between words in BEM using a keyboard is hard because the double underscore is not considered a separator of words)
- [ABEM](https://css-tricks.com/abem-useful-adaptation-bem/) syntax for modifiers.

It looks like this:

```
.block--element .-modifier

<div class="block--element -modifier">
```

Standard BEM:

```
.block__element--modifier
```

## Dependencies

The build server runs `yarn install --production` and `yarn build`, which means anything that's directly or indirectly required to run `yarn build` _must_ be a `dependency` in `package.json`. Use `devDependencies` for excluding modules from the build server (like `pa11y-ci` which crashes the build server).

## Accessibility tests

The npm script `test:wcag` requires that `build:static` has been run before running the tests. On unix or unix-like systems you can do this:

```
yarn build:static && yarn test:wcag
```

## ContentArea

This project renders Episerver content areas with the `ContentArea` React component. This component has a manually authored list of supported blocks. This means that in order for a React component to be rendered using the `ContentArea` component, you have to add that component in `components/content-area/supported-blocks.js`.

**IMPORTANT**: Make sure that you don't add any component that imports `ContentArea` to the `supported-blocks` list! Doing so will result in an infinite loop of imports (`Component` imports `ContentArea`, which imports `Components`, which imports `ContentArea` and so on), which `Webpack` will "fix" for you by setting the imported `ContentArea` to `undefined` and rendering will crash.

## C# class generator

This project uses [@creuna/prop-types-csharp-webpack-plugin](https://github.com/Creuna-Oslo/prop-types-csharp-webpack-plugin). You might notice that most React components have a `propTypesMeta` property. This property is used by the csharp plugin. Read more about `propTypesMeta` in the [docs](https://github.com/Creuna-Oslo/prop-types-csharp-webpack-plugin#readme).

C# classes are built to `Solution/Forskningsradet.Core/Models/ReactModels`

### Working with propTypes

In order to avoid duplicate definitions it's preferable to structure your render methods in a certain way. Consider the following example:

```jsx
import ComponentB from 'component-b';

const ComponentA = ({ someProp, anotherProp }) => <ComponentB someProp={someProp} anotherProp={anotherProp}>

ComponentA.propTypes = {
  someProp: PropTypes.string,
  anotherProp: PropTypes.string
};
```

When generating C# classes for `ComponentA` and `ComponentB` from the above example, both classes will have definitions for `someProp` and `anotherProp`:

```cs
public class ComponentA
{
  public string SomeProp { get; set; }
  public string AnotherProp { get; set; }
}

public class ComponentB
{
  public string SomeProp { get; set; }
  public string AnotherProp { get; set; }
}
```

Duplicating definitions like this is not ideal, both because it's nice to avoid duplicate code and because we don't want to have to maintain the definition in two separate places. In order to fix this, we can change `ComponentA` to look like this:

```jsx
import ComponentB from 'component-b';

const ComponentA = ({ componentB }) => <ComponentB {...componentB}>

ComponentA.propTypes = {
  componentB: PropTypes.exact(ComponentB.propTypes)
};
```

```cs
public class ComponentA
{
  public ComponentB ComponentB { get; set; }
}

public class ComponentB
{
  public string SomeProp { get; set; }
  public string AnotherProp { get; set; }
}
```

Now, in both C# and React, `ComponentA` doesn't have to know anything about what data `ComponentB` needs. `ComponentB` can be refactored without having to update `ComponentA`. Yay!
