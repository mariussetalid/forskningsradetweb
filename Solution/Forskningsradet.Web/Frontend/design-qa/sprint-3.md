Alfabetliste:

- ~~Litt thight mellom radene med bokstaver~~
- ~~Alfabetet skal være midt mellom søk og bokstavliste~~
- ~~32px over og under bokstavelementet~~
- ~~Margin på alle h1:  
  margin-top: 60px;
  margin-bottom: 24px;~~
- ~~Ansattliste skal ikke ha lenker~~
- ~~Samme avstand på ordlista som på ansattlista~~
- ~~Gjenbruke streken mellom bokstavene på disse:
  Nærlingsliv og teknologi
  Avdeling for mobilisering og regional utvikling~~
  - ~~Navn på ansatte skal være 24px , alt lignende skal også være 24px~~

Søk:

- ~~margin-top: 24px; på .search-results~~

Brødsmule:

- ~~Fonten på "vis brødsmuler": 14px~~

Logo:

- ~~Har fått feil størrelse~~

Blokker:

- ~~32px marg på alle blokker~~
- ~~Kampanjeblokk/felt: skriftstørrelse 24px~~
- ~~Cta: 14px på knappen~~

Karriere:

- ~~På ledige stillinger i forskningsrådet-tittelen
  margin-bottom: 0;
  margin-top: 44px;~~
- ~~date-card--title: midstilt. størrelse 18px~~

Font:

- Ny mixin. 30px på desktop, 24px på mobil **Lager denne når den skal brukes**
