# Design QA etter Sprint 1

### Header

- Header vokser med skjermen
- ~~Lenke blå på krysset i søket~~
- ~~Litt mer mellomrom mellom søk og ikon~~
- ~~Litt mykere skygge under menyen~~
- ~~Skygge på menyen (både desktop og mobil)~~

### Meny

- ~~Fjerne underlinje fra hover i meny, ha fargeforandring i stedet #AFB4BD~~
- Hovereffekt på pila i menyen (de som har undermeny) **Dette så ganske corny ut, må dobbeltsjekkes**
- ~~Hånd på alle menyelemenetene~~

- "Lukk meny" i stedet for skjul

### Arrangementer:

- ~~Litt mindre luft mellom tittel og header~~
- ~~Litt mer mellom "vis" og tittel~~
- Legge inn designet for aktiv/inaktiv **Forandrer ikke dette før vi vet helt sikkert**
- ~~Gjennomført skal stå i 14px ikke 16px~~
- ~~Mellom måned og dato: litt mer avstand, typ 3px~~
- Litt mer mellom orange strek og måned/dato **Må dobbeltsjekkes med Anne**
- ~~Når man har huket av ting i filterlista: svart tekst~~
- ~~Orange linje skal 32px, og 20px avstand~~
- ~~Filter: når "alle" er valgt eller mouseover, gjør teksten grå~~
  - ~~Mørkere bakgrunn på hovereffekt: #E8ECEE~~
- ~~Arrangmentsortering på tid: 10px mellom strek og tekst i stedet for 8px, litt lavere strek. 16px høyde (fiks dette i brødsmulestien også). Streken skal alltid være: #d2dade~~
- ~~Mobilfilter: flytte nærmere toppen~~

#### Brødsmulesti:

- ~~Vis brødsmulesti og Lukk brødsmulesti~~

Hvis man ikke sender med lenke på tags: inaktive

### Arrangement

- ~~Litt mindre avstand mellom ikon og lenke~~
- Ikke tabell **Bestemte oss sammen med designer for å beholde tabell for å unngå kodekompleksitet**
- ~~Litt mindre avstand mellom hver del i tabellen~~
- ~~Ikke margin på event-data~~
- ~~Mellom foredragsholdere og bilder av foredragsholdere, litt tettere~~
- ~~Programliste: h3: padding-bottom: 16px dl: padding-top 16px~~

### Artikkel

- Litt mer spacing mellom header og tittel (gjennomgående på alle sider)
- ~~Forfatter skillelinje: følge samme mal som brødsmule~~
- ~~Ikke tvunget brekk etter to på fporfatterlista~~
- ~~5px margin-top på <p> etter en <h3> i løpende tekst~~
- ~~Gå gjennom alle orange streker i blokkene~~
- ~~Infoblokker på mobil: 16px <p>~~
- ~~Accordion: Runde.~~
- Accordion: Mer padding i bunnen. **Ser merkelig ut, dobbeltsjekk med designer**
- Call to action: flytt teksten -1px for at det skal se midtstilt ut. Litt mer padding på sidene. **Dette blir litt for flisespikkete?**
- ~~Løpende tekst: svart tekst på hover~~
- Mellom some-ikoner og linjen i bunnen: 10px avstand **Flisespikk**
- ~~Mer avstand mellom call to action-knapp og tekst i cta-blokk~~

### Infoside

- ~~Blokk med H2 og orange strek i stedet for "Relatert innhold" på egen linje~~
- ~~related-content--content: <p> skal ikke ha egen padding-bottom~~
- ~~Ha en small-image-block i stedet for image-with-caption~~
  - ~~Denne skal ikke ha padding på høyre side i mobilversjon~~

#### Generelt

- Gå gjennom alle knapper og legg på hover
