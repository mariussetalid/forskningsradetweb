### Søkesiden

- Bruk avstandene til arrangement-tittelen overalt på siden
- ~~Feil margin rundt søkeresultater (søkeresultatene skal følge bredden til søket)~~
- ~~Gjøre om søkeresultatet-tekst til en riktekst, slik at søkeordet kan være i bold~~
- Den valgte siden i pagineringen skal være i bold og med sort tekst
- Bruk-knappen skal ha margin-top: 32px **Neste hopp er 44px, sjekk med designer**
