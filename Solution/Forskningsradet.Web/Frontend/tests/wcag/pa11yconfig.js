const pagePaths = require('../../source/static-site/pages/paths');
const path = require('path');

module.exports = {
  urls: pagePaths.map(pagePath =>
    path.join(__dirname, '..', '..', 'dist', pagePath, 'index.html')
  )
};
