import test from 'ava';

import doesContain from '../../source/js/utils/doesContain';

test('Checks if value is in array', t => {
  t.true(doesContain(['asd', 'burger', 'hei'], 'hei'));
  t.true(doesContain(['asd', false, 'hei'], false));
  t.true(doesContain(['asd', 'false', 'hei', 2], 2));

  t.false(doesContain(['asd', 'burger', 2], '2'));
});

test('Checks if value is equal to value', t => {
  t.true(doesContain('hei', 'hei'));
  t.true(doesContain(false, false));
  t.true(doesContain(2, 2));

  t.false(doesContain(false, true));
  t.false(doesContain('a', 'A'));
  t.false(doesContain('asd', 'dsa'));
  t.false(doesContain(2, '2'));
});
