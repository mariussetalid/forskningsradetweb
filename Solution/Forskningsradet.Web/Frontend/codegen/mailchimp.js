/* eslint-env node */
/* eslint-disable no-console */
const fs = require('fs');
const path = require('path');

const dist = path.resolve(__dirname, '..', 'dist');

const maybeCopyFile = (filePath, pattern, fileName) => {
  if (pattern.test(filePath)) {
    const content = fs.readFileSync(path.join(dist, filePath));
    fs.writeFileSync(path.join(dist, fileName), content);
    console.log(`'${fileName}' written.`);
  }
};

fs.readdirSync(dist).forEach(filePath => {
  maybeCopyFile(filePath, /style\.\w+\.css$/, 'mailchimp-style.css');
  maybeCopyFile(filePath, /logo\.\w+\.png$/, 'logo.png');
});
