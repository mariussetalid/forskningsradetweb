import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
// NOTE: levelOffset is intended to be used by the backend only, as a way to describe the semantic relationships between blocks without having to know what the actual heading levels are.

const Heading = ({ className, children, id, level, levelOffset }) => {
  const HeadingTag = `h${level + levelOffset}`;

  return (
    <HeadingTag className={cn('heading', className)} id={id}>
      {children}
    </HeadingTag>
  );
};

Heading.propTypes = {
  className: PropTypes.string,
  children: PropTypes.node,
  id: PropTypes.string,
  level: PropTypes.number,
  levelOffset: PropTypes.number
};

Heading.defaultProps = {
  level: 2,
  levelOffset: 0
};

Heading.propTypesMeta = 'exclude';

export default Heading;
