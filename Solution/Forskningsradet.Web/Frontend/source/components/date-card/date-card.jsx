import React from 'react';
import PropTypes from 'prop-types';

import cn from 'classnames';
import propTypeTheme from 'utils/prop-type-theme';

import DateCardMedia from './date-card-media';
import DateCardStatus from './date-card-status';
import DateCardTags from './date-card-tags';
import DateCardDates from './date-card-dates';
import Heading from 'components/heading';
import LinkWrapper from 'components/link-wrapper';
import Metadata from 'components/metadata';
import Message from 'components/message';

const themes = {
  rightAlignedTags: '-theme-right-aligned-tags',
  noPadding: '-theme-no-padding',
  noBorder: '-theme-no-border',
  sidebar: '-theme-sidebar',
  vertical: '-theme-vertical'
};

const DateCard = ({
  dateContainer,
  headingLevel,
  media,
  metadata,
  metadataTheme,
  messageList,
  showMedia,
  subtitle,
  status,
  tags,
  theme,
  title,
  text,
  url,
  usedInSidebar,
  isLink
}) => {
  const { OuterLink, InnerLink } = LinkWrapper(isLink, url);
  const { dates = [] } = dateContainer;
  return (
    <div
      className={cn('date-card', theme, {
        '-without-date': !dates.length
      })}
    >
      <OuterLink
        className={cn('date-card__grid date-card__grid-primary', {
          '-is-past-date': dateContainer.isPastDate,
          '-is-planned': dateContainer.isPlanned,
          '-without-date': !dates.length
        })}
      >
        <div className="date-card--title">
          <InnerLink>
            {title && (
              <Heading className="date-card--heading" level={headingLevel}>
                {title}
              </Heading>
            )}
            {subtitle && (
              <Heading
                className="date-card--sub-heading"
                level={headingLevel + 1}
              >
                {subtitle}
              </Heading>
            )}
          </InnerLink>
        </div>

        {!!dates.length && (
          <DateCardDates
            {...dateContainer}
            theme={
              theme.indexOf('-theme-vertical') !== -1
                ? DateCardDates.themes.big
                : ''
            }
          />
        )}
        <div className="date-card--content">
          {messageList.map(message => (
            <div key={message.text.text} className="date-card--message">
              <Message {...message}></Message>
            </div>
          ))}
          {metadata && (
            <div className="date-card--metadata">
              <Metadata
                theme={usedInSidebar ? Metadata.themes.vertical : metadataTheme}
                {...metadata}
              />
            </div>
          )}
          {showMedia && media && <DateCardMedia {...media} />}
          {text && <p className="date-card--text">{text}</p>}
        </div>
      </OuterLink>
      {(status || tags) && (
        <div className="date-card__grid date-card__grid-secondary">
          {status && (
            <DateCardStatus
              centered={!usedInSidebar}
              theme={
                (dateContainer.isPlanned && DateCardStatus.themes.isPlanned) ||
                (dateContainer.isPastDate && DateCardStatus.themes.isCompleted)
              }
              {...status}
            />
          )}

          {tags && <DateCardTags {...tags} />}
        </div>
      )}
    </div>
  );
};

DateCard.propTypes = {
  dateContainer: PropTypes.exact(DateCardDates.propTypes),
  messageList: PropTypes.arrayOf(PropTypes.exact(Message.propTypes)),
  headingLevel: PropTypes.number,
  metadata: PropTypes.exact(Metadata.propTypes),
  metadataTheme: PropTypes.string,
  // Disable eslint warning of unused propType because 'id' is used as 'key' when rendering lists of DateCard
  // eslint-disable-next-line
  id: PropTypes.string.isRequired,
  media: PropTypes.exact(DateCardMedia.propTypes),
  showMedia: PropTypes.bool,
  status: PropTypes.exact(DateCardStatus.propTypes),
  subtitle: PropTypes.string,
  tags: PropTypes.exact(DateCardTags.propTypes),
  text: PropTypes.string,
  theme: propTypeTheme(themes),
  title: PropTypes.string,
  url: PropTypes.string,
  usedInSidebar: PropTypes.bool,
  isLink: PropTypes.bool
};

DateCard.propTypesMeta = {
  headingLevel: 'exclude',
  metadataTheme: 'exclude',
  showMedia: 'exclude',
  theme: 'exclude',
  isLink: 'exclude'
};

DateCard.defaultProps = {
  headingLevel: 2,
  messageList: [],
  showMedia: true,
  dateContainer: {},
  theme: ''
};

DateCard.themes = themes;

export default DateCard;
