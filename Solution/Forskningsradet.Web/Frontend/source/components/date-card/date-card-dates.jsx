/* eslint-disable react/no-multi-comp */
import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import EventImage from 'components/event-image';
import propTypeTheme from 'js/utils/prop-type-theme';
import HeightOfChild from 'components/height-of-child';
import Icon from 'components/icon';

const themes = {
  big: '-theme-big'
};

const hasSameValue = (dates, key) =>
  dates.length === 2 && dates[0][key] === dates[1][key];

const DateCardDates = ({
  datesTitle,
  dates,
  datesDescription,
  isPastDate,
  isInternational,
  isPlanned,
  labels,
  eventImage,
  className,
  theme,
  setHeight
}) => {
  const setParentHeight = height => {
    if (!height) return;
    setHeight(`${height}px`);
  };

  const singleYear = hasSameValue(dates, 'year');
  const singleMonth = hasSameValue(dates, 'month') && singleYear;
  return (
    <div
      className={cn('date-card-dates', className, theme, {
        '-single-period': singleMonth || singleYear,
        '-light-text': eventImage,
        '-is-past-date': isPastDate,
        '-is-planned': isPlanned
      })}
      ref={node => node && setHeight && setParentHeight(node.clientHeight)}
    >
      {eventImage && (
        <EventImage
          {...eventImage}
          darkFilter={eventImage.image && !!eventImage.image.src}
        />
      )}
      {datesTitle && (
        <div className="date-card-dates--description-top">{datesTitle}</div>
      )}
      {!!dates.length && (
        <div className="date-card-dates--dates">
          {dates.map((date, index) => (
            <div className="date-card-dates--date" key={date.day + date.month}>
              <div className="date-card-dates--day">{date.day}</div>
              {((singleMonth && !!index) || !singleMonth) && (
                <HeightOfChild>
                  {Child => (
                    <Child
                      className={cn('date-card-dates--month', {
                        '-single-period': singleMonth
                      })}
                    >
                      {date.month}
                    </Child>
                  )}
                </HeightOfChild>
              )}
              {((singleYear && !!index) || !singleYear) && (
                <HeightOfChild includeWidth>
                  {Child => (
                    <Child
                      className={cn('date-card-dates--year', {
                        '-single-period': singleYear
                      })}
                    >
                      {date.year}
                    </Child>
                  )}
                </HeightOfChild>
              )}
            </div>
          ))}
        </div>
      )}
      {datesDescription && (
        <div className="date-card-dates--description">{datesDescription}</div>
      )}
      {isPastDate && (
        <div className="date-card-dates--past-date">{labels.pastDate}</div>
      )}
      {isInternational && (
        <div className="date-card-dates--international">
          <Icon
            className="date-card-dates--international-icon"
            name="globe"
          ></Icon>
        </div>
      )}
      {!isPlanned && !isInternational && (
        <div className="date-card-dates--bar" />
      )}
    </div>
  );
};

DateCardDates.propTypes = {
  dates: PropTypes.arrayOf(
    PropTypes.exact({
      day: PropTypes.string,
      month: PropTypes.string,
      year: PropTypes.string
    })
  ),
  datesDescription: PropTypes.string,
  datesTitle: PropTypes.string,
  isPastDate: PropTypes.bool,
  isInternational: PropTypes.bool,
  isPlanned: PropTypes.bool,
  className: PropTypes.string,
  eventImage: PropTypes.exact(EventImage.propTypes),
  theme: propTypeTheme(themes),
  setHeight: PropTypes.func,
  labels: PropTypes.exact({
    pastDate: PropTypes.string
  })
};

DateCardDates.defaultProps = {
  dates: [],
  labels: {}
};

DateCardDates.propTypesMeta = {
  setHeight: 'exclude',
  theme: 'exclude'
};
DateCardDates.themes = themes;

export default DateCardDates;
