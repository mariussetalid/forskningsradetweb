import React from 'react';
import PropTypes from 'prop-types';

import OPE from 'js/on-page-editing';

import Button from 'components/button';
import EmptyList from 'components/empty-list';
import Link from 'components/link';
import cn from 'classnames';
import Heading from 'components/heading';
import AutoGrid from 'components/auto-grid';

const borders = {
  top: '-border-top',
  bottom: '-border-bottom'
};

const RelatedContent = ({
  children,
  emptyList,
  link,
  onPageEditing,
  title,
  border,
  isGrid
}) => {
  const gridLinkThemes = [Button.themes.orangeOutline, Button.themes.big];
  return (
    <div className={cn('related-content', { '-is-grid': isGrid })}>
      {title && (
        <Heading
          className="related-content--title"
          {...OPE(onPageEditing.title)}
        >
          {title}
        </Heading>
      )}
      {React.Children.count(children) > 0 ? (
        <AutoGrid isList={!isGrid}>
          {AutoGridItem =>
            React.Children.map(children, child => (
              <AutoGridItem
                className={cn(
                  null,
                  { 'related-content--item': !isGrid },
                  borders[border]
                )}
              >
                {child}
              </AutoGridItem>
            ))
          }
        </AutoGrid>
      ) : (
        <EmptyList {...emptyList} />
      )}

      {link && (
        <div className="related-content--link">
          <Link
            className="related-content--link-inner"
            theme={isGrid ? gridLinkThemes : Button.themes.orangeOutline}
            useButtonStyles
            {...link}
          />
        </div>
      )}
    </div>
  );
};

RelatedContent.propTypes = {
  children: PropTypes.node,
  emptyList: PropTypes.exact(EmptyList.propTypes),
  link: PropTypes.exact(Link.propTypes),
  onPageEditing: PropTypes.exact({
    title: PropTypes.string
  }),
  title: PropTypes.string,
  border: PropTypes.oneOf(Object.keys(borders)),
  isGrid: PropTypes.bool
};

RelatedContent.defaultProps = {
  border: 'bottom',
  onPageEditing: {}
};

RelatedContent.propTypesMeta = {
  isGrid: 'exclude'
};
export default RelatedContent;
