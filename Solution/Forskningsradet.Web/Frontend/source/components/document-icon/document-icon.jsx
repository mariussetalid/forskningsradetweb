import React from 'react';
import PropTypes from 'prop-types';

import cn from 'classnames';

import csv from 'images/csv.svg';
import fallback from 'images/doc-fallback.svg';
import excel from 'images/excel.svg';
import image from 'images/image.svg';
import pdf from 'images/pdf.svg';
import ppt from 'images/ppt.svg';
import word from 'images/word.svg';

import csvHover from 'images/csv-hover.svg';
import fallbackHover from 'images/doc-fallback-hover.svg';
import excelHover from 'images/excel-hover.svg';
import imageHover from 'images/image-hover.svg';
import pdfHover from 'images/pdf-hover.svg';
import pptHover from 'images/ppt-hover.svg';
import wordHover from 'images/word-hover.svg';

import csvWhite from 'images/csv-white.svg';
import fallbackWhite from 'images/doc-fallback-white.svg';
import excelWhite from 'images/excel-white.svg';
import imageWhite from 'images/image-white.svg';
import pdfWhite from 'images/pdf-white.svg';
import pptWhite from 'images/ppt-white.svg';
import wordWhite from 'images/word-white.svg';

const sizes = {
  xsmall: '-size-xsmall',
  small: '-size-small',
  large: '-size-large',
  xlarge: '-size-xlarge'
};

const iconThemes = {
  word: { icon: word, hover: wordHover, white: wordWhite },
  pdf: { icon: pdf, hover: pdfHover, white: pdfWhite },
  ppt: { icon: ppt, hover: pptHover, white: pptWhite },
  csv: { icon: csv, hover: csvHover, white: csvWhite },
  excel: { icon: excel, hover: excelHover, white: excelWhite },
  image: { icon: image, hover: imageHover, white: imageWhite },
  fallback: { icon: fallback, hover: fallbackHover, white: fallbackWhite }
};

const types = {
  hover: 'hover',
  icon: 'icon',
  white: 'white'
};

const DocumentIcon = ({ iconTheme, size, type }) => {
  const icon = iconThemes[iconTheme][type || 'icon'];
  const backgroundImage = { backgroundImage: `url(${icon})` };
  return (
    <div className="document-icon">
      <div
        className={cn('document-icon__icon', iconTheme, size)}
        style={backgroundImage}
      ></div>
    </div>
  );
};

DocumentIcon.propTypes = {
  iconTheme: PropTypes.oneOf(Object.keys(iconThemes)),
  size: PropTypes.oneOf(Object.values(sizes)),
  type: PropTypes.oneOf(Object.values(types))
};

DocumentIcon.defaultProps = {
  iconTheme: 'fallback'
};

DocumentIcon.propTypesMeta = {
  size: 'exclude',
  type: 'exclude'
};

DocumentIcon.sizes = sizes;
DocumentIcon.types = types;

export default DocumentIcon;
