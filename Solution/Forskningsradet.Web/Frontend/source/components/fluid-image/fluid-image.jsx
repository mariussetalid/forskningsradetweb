import React from 'react';
import PropTypes from 'prop-types';

import cn from 'classnames';

import OPE from 'js/on-page-editing';

import getNewSrc from 'js/responsive-images';

const FluidImage = ({
  alt,
  className,
  focusPoint,
  initialSize,
  onPageEditing,
  src,
  darkFilter
}) => {
  const container = React.useRef();
  const [currentSrc, setSrc] = React.useState(getNewSrc(src, initialSize));

  React.useEffect(() => {
    setSrc(getNewSrc(src, container.current.offsetWidth, initialSize));
  });

  const darkGradient = darkFilter
    ? 'linear-gradient( rgba(0, 0, 0, 0.4), rgba(0, 0, 0, 0.4)),'
    : '';

  return (
    <div
      className={cn('fluid-image', className)}
      ref={container}
      {...OPE(onPageEditing === null ? null : onPageEditing.image)}
    >
      <div
        className={cn('fluid-image--image')}
        style={{
          backgroundImage: `${darkGradient} url(${currentSrc})`,
          backgroundPosition: focusPoint
            ? `${focusPoint.x}% ${focusPoint.y}%`
            : 'center'
        }}
      />
      <img src={currentSrc} alt={alt} />
    </div>
  );
};

FluidImage.propTypes = {
  alt: PropTypes.string.isRequired,
  className: PropTypes.string,
  focusPoint: PropTypes.exact({
    x: PropTypes.number.isRequired,
    y: PropTypes.number.isRequired
  }),
  initialSize: PropTypes.number,
  onPageEditing: PropTypes.exact({
    image: PropTypes.string
  }),
  src: PropTypes.string.isRequired,
  darkFilter: PropTypes.bool
};

FluidImage.propTypesMeta = {
  className: 'exclude',
  darkFilter: 'exclude'
};

FluidImage.defaultProps = {
  initialSize: 200,
  onPageEditing: {}
};

export default FluidImage;
