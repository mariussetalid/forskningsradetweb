import React from 'react';
import PropTypes from 'prop-types';

import OPE from 'js/on-page-editing';

import ArticleHeader from 'components/article-header';
import ContentArea from 'components/content-area';
import ContentWithSidebar from 'components/content-with-sidebar';
import RichText from 'components/rich-text';
import PageFooter from 'components/page-footer';

const ArticlePage = ({
  content,
  footer,
  header,
  ingress,
  onPageEditing,
  sidebar,
  title
}) => (
  <React.Fragment>
    <ContentWithSidebar>
      <ContentWithSidebar.Content>
        {title && (
          <h1 data-epi-type="title" {...OPE(onPageEditing.title)}>
            {title}
          </h1>
        )}
      </ContentWithSidebar.Content>
      <ContentWithSidebar.Sidebar />
    </ContentWithSidebar>

    <ContentWithSidebar>
      <ContentWithSidebar.Content>
        <div data-epi-type="content">
          {(ingress || onPageEditing.ingress) && (
            <div
              className="article-page--ingress"
              {...OPE(onPageEditing.ingress)}
            >
              <p>{ingress}</p>
            </div>
          )}
          <ArticleHeader {...header} />
          <RichText {...content} />
        </div>
      </ContentWithSidebar.Content>

      <ContentWithSidebar.Sidebar className="article-page--sidebar">
        <ContentArea enableElementSizing={false} {...sidebar} />
      </ContentWithSidebar.Sidebar>
    </ContentWithSidebar>

    <ContentWithSidebar>
      <ContentWithSidebar.Content>
        <div className="article-page--footer" data-epi-type="content">
          <PageFooter {...footer} />
        </div>
      </ContentWithSidebar.Content>
    </ContentWithSidebar>
  </React.Fragment>
);

ArticlePage.propTypes = {
  content: PropTypes.exact(RichText.propTypes),
  footer: PropTypes.exact(PageFooter.propTypes),
  header: PropTypes.exact(ArticleHeader.propTypes),
  ingress: PropTypes.string,
  onPageEditing: PropTypes.exact({
    ingress: PropTypes.string,
    title: PropTypes.string
  }),
  sidebar: PropTypes.exact(ContentArea.propTypes),
  title: PropTypes.string
};

ArticlePage.defaultProps = {
  onPageEditing: {}
};

export default ArticlePage;
