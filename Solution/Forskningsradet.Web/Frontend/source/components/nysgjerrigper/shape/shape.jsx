import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import Icon from 'components/icon/icon';

import { colors } from 'js/utils/np-colors';

const types = {
  rectangle: 'rectangle',
  circle: 'circle',
  snakeH: 'snake-horizontal',
  snakeV: 'snake-vertical',
  longH: 'long-horizontal',
  longV: 'long-vertical'
};

const Shape = ({ type, color, className }) => {
  return (
    <Icon
      className={cn('shape', className)}
      style={{ color }}
      name={`np-${type}`}
      fill
    />
  );
};

Shape.propTypes = {
  type: PropTypes.oneOf(Object.values(types)),
  color: PropTypes.oneOf(Object.values(colors)),
  className: PropTypes.string
};

Shape.propTypesMeta = 'exclude';

Shape.colors = colors;
Shape.types = types;

export default Shape;
