import React from 'react';
import PropTypes from 'prop-types';

import Accordion from 'components/accordion';
import ContentContainer from 'components/content-container';
import Icon from 'components/icon';
import Link from 'components/link';

const Breadcrumbs = ({ accordion, currentPage, items, root }) => {
  return (
    <Accordion showButtonText={true} {...accordion}>
      {({ Button, Collapse }) => (
        <nav className="breadcrumbs" data-epi-type="content">
          <ContentContainer className="breadcrumbs--header">
            <Button className="breadcrumbs--toggle">
              <Icon name="house" fill={true} />
            </Button>
          </ContentContainer>

          <Collapse className="breadcrumbs--collapse">
            <ContentContainer element="ul" theme={ContentContainer.themes.wide}>
              <li>
                <a className="breadcrumbs--root" href={root.url}>
                  <Icon name="house" fill={true} />
                  <span>{root.text}</span>
                </a>
              </li>

              {items.map(item => (
                <li key={item.url || item.text}>
                  <Link {...item} />
                </li>
              ))}

              {currentPage && <li key={currentPage}>{currentPage}</li>}
            </ContentContainer>
          </Collapse>
        </nav>
      )}
    </Accordion>
  );
};

Breadcrumbs.propTypes = {
  accordion: PropTypes.exact(Accordion.propTypes),
  currentPage: PropTypes.string,
  items: PropTypes.arrayOf(PropTypes.exact(Link.propTypes)),
  root: PropTypes.exact(Link.propTypes)
};

Breadcrumbs.defaultProps = {
  items: [],
  root: {}
};

export default Breadcrumbs;
