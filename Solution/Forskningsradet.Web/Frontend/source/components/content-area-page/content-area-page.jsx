import React from 'react';
import PropTypes from 'prop-types';

import ContentArea from 'components/content-area';
import ContentContainer from 'components/content-container';
import HtmlString from 'components/html-string';
import PageHeader from 'components/page-header';
import RichTextBlock from 'components/rich-text-block';

const narrowTextProps = {
  HtmlString: { theme: HtmlString.themes.narrow },
  RichTextBlock: { theme: RichTextBlock.themes.narrow }
};

const ContentAreaPage = ({ content, pageHeader }) => (
  <div className="content-area-page">
    <PageHeader {...pageHeader} />
    <ContentContainer className="content-area-page--content">
      <ContentArea
        additionalComponentProps={{
          ...narrowTextProps,
          // NOTE: RichText is just a proxy for ContentArea, so themes need to be applied using additionalComponentProps
          RichText: { additionalComponentProps: narrowTextProps }
        }}
        {...content}
      />
    </ContentContainer>
  </div>
);

ContentAreaPage.propTypes = {
  content: PropTypes.exact(ContentArea.propTypes),
  pageHeader: PropTypes.exact(PageHeader.propTypes)
};

export default ContentAreaPage;
