import React, { useState, useContext, useEffect } from 'react';
import PropTypes from 'prop-types';

import Button from 'components/button';
import Checkbox from 'components/form-elements/checkbox';
import FilterGroup from './filter-group';
import Form from 'components/form';
import RangeSlider from 'components/range-slider';
import useIsScreenSize from 'js/hooks/use-is-screen-size';
import Spinner from 'components/spinner';

const Filters = ({
  checkboxes,
  items,
  labels,
  rangeFilter,
  fetchFilteredResults,
  title,
  close,
  mobileTitle,
  isLoading
}) => {
  const [isReseting, setIsReseting] = useState();
  const [isMounted, setIsMounted] = useState();
  const { submitForm } = useContext(Form.Context);
  const [searchQueryParameter, setSearchQueryParameter] = useState(
    getInitialSearchQueryParameter(items)
  );
  const isDesktop = useIsScreenSize('lg');

  const setSearchQueryParameterHelper = (name, value, checked) => {
    setSearchQueryParameter(prevState => ({
      ...prevState,
      [name]: checked
        ? prevState[name].concat(value)
        : prevState[name].filter(item => item !== value)
    }));
  };

  function getInitialSearchQueryParameter(items) {
    let result = {};
    items.map(item => {
      item.options.map(option => {
        let target = option.name;
        if (!(target in result)) {
          result[target] = [];
        }
        if (option.checked) {
          result[target] = result[target].concat(option.value);
        }
      });
    });
    return result;
  }

  useEffect(() => {
    isMounted && fetchFilteredResults(searchQueryParameter);
  }, [searchQueryParameter]);

  useEffect(() => {
    setIsMounted(true);
  }, []);

  useEffect(() => {
    !isLoading && isReseting && setIsReseting(false);
  }, [isLoading]);

  const deselectItems = () => {
    setIsReseting(true);
    setSearchQueryParameter(state =>
      Object.keys(state).reduce(
        (acc, key) => ({
          ...acc,
          [key]: []
        }),
        {}
      )
    );
  };

  return (
    <div className="filters">
      {!isDesktop ? (
        <Button
          className="filters--close"
          onClick={close}
          iconBeforeChildren="x-straight"
        >
          <span className="filters--close-text">{mobileTitle}</span>
        </Button>
      ) : (
        title && <div className="filters--title">{title}</div>
      )}
      <div className="filters--filters">
        {items &&
          items.map(item => (
            <FilterGroup
              {...item}
              key={item.title}
              setSearchQueryParameterHelper={setSearchQueryParameterHelper}
              isDesktop={isDesktop}
            />
          ))}

        <Button className="filters--deselect-button" onClick={deselectItems}>
          {labels.reset}
        </Button>
        {checkboxes &&
          checkboxes.map(checkbox => (
            <Checkbox
              className="filters--checkbox"
              key={checkbox.name}
              onChange={submitForm}
              theme={Checkbox.themes.gray}
              {...checkbox}
            />
          ))}

        {rangeFilter && (
          <RangeSlider className="filters--range" {...rangeFilter} />
        )}
      </div>
      {!isDesktop && labels.showResults && (
        <div className="filters--show-results">
          <Button
            onClick={close}
            theme={[Button.themes.fill, Button.themes.big]}
          >
            {labels.showResults}
          </Button>
        </div>
      )}

      <Spinner isActive={isReseting} />
    </div>
  );
};

Filters.propTypes = {
  checkboxes: PropTypes.arrayOf(PropTypes.exact(Checkbox.propTypes)),
  items: PropTypes.arrayOf(PropTypes.exact(FilterGroup.propTypes)),
  labels: PropTypes.exact({
    showResults: PropTypes.string,
    reset: PropTypes.string
  }),
  rangeFilter: PropTypes.exact(RangeSlider.propTypes),
  fetchFilteredResults: PropTypes.func,
  title: PropTypes.string,
  close: PropTypes.func,
  mobileTitle: PropTypes.string,
  isLoading: PropTypes.bool
};

Filters.defaultProps = {
  checkboxes: [],
  items: [],
  labels: {}
};

Filters.propTypesMeta = {
  isMounted: 'exclude',
  isLoading: 'exclude'
};

export default Filters;
