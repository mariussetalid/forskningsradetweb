import React from 'react';
import PropTypes from 'prop-types';
import Link from 'components/link';

const PageNavigation = ({ previous, next }) => (
  <div className="page-navigation">
    {previous && (
      <Link
        className="page-navigation--icon-left"
        iconBeforeChildren="long-arrow"
        iconClassName="page-navigation--icon"
        theme={[Link.themes.smallText]}
        {...previous}
      />
    )}
    {next && (
      <Link
        className="page-navigation--icon-right"
        icon="long-arrow"
        iconClassName="page-navigation--icon"
        theme={[Link.themes.smallText]}
        {...next}
      />
    )}
  </div>
);

PageNavigation.propTypes = {
  previous: PropTypes.shape(Link.propTypes),
  next: PropTypes.shape(Link.propTypes)
};

export default PageNavigation;
