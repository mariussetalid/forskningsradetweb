import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';

import responsiveValue from 'js/utils/responsive-value';
import resizeHook from 'js/hooks/resize';

import Carousel from 'components/carousel';
import Button from 'components/button';
import ProcessItem from './process-item';
import HeightOfChild from 'components/height-of-child';

let breakpoints = {
  '0px': 1,
  '510px': 2,
  '768px': 3,
  '1600px': 4
};

const ProcessCarousel = ({ items, oldStyle, hasSideBar }) => {
  if (oldStyle || hasSideBar)
    breakpoints = {
      '0px': 1,
      '510px': 2,
      '768px': 3
    };

  const [numberOfSlidesToShow, setNumberOfSlidesToShow] = useState(3);

  const updateNumberOfSlides = () => {
    const slidesToShow = Math.min(items.length, responsiveValue(breakpoints));
    setNumberOfSlidesToShow(slidesToShow);
  };

  resizeHook(updateNumberOfSlides);

  useEffect(() => {
    updateNumberOfSlides();
  }, [hasSideBar]);

  return (
    <HeightOfChild
      className={cn('process-carousel', {
        '-more-than-three': numberOfSlidesToShow > 3 && !oldStyle
      })}
    >
      {Child => (
        <Carousel
          numberOfSlidesToShow={numberOfSlidesToShow}
          nextButton={(next, hasNext) => (
            <Button
              className="process-carousel__button process-carousel__button-next"
              disabled={!hasNext}
              icon="small-arrow-right"
              onClick={next}
            />
          )}
          previousButton={(prev, hasPrev) => (
            <Button
              className="process-carousel__button process-carousel__button-prev"
              disabled={!hasPrev}
              icon="small-arrow-right"
              onClick={prev}
            />
          )}
        >
          {items.map((item, index) => (
            <Child
              className="process-carousel--item"
              key={item.title || item.text || index}
            >
              <ProcessItem key={item.text} {...item} index={index} />
            </Child>
          ))}
        </Carousel>
      )}
    </HeightOfChild>
  );
};

ProcessCarousel.propTypes = {
  items: PropTypes.arrayOf(PropTypes.exact(ProcessItem.propTypes)),
  oldStyle: PropTypes.bool,
  hasSideBar: PropTypes.bool
};

ProcessCarousel.propTypesMeta = {
  oldStyle: 'exclude',
  hasSideBar: 'exclude'
};

export default ProcessCarousel;
