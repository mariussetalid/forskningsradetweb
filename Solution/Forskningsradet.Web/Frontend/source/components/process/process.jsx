import React, { useEffect, useRef, useState } from 'react';
import PropTypes from 'prop-types';

import Heading from 'components/heading';
import ProcessItem from './process-item';
import ProcessCarousel from './process-carousel';
import AutoGrid from 'components/auto-grid';

const Process = ({ title, introText, isCarousel, items, oldStyle }) => {
  const processRef = useRef();
  const [hasSideBar, setHasSideBar] = useState();
  const hasSideBarParent = element => {
    const isSideBar = element =>
      element.classList[0] === 'content-with-sidebar' &&
      !element.className.includes('left');
    if (element)
      return isSideBar(element) || hasSideBarParent(element.parentElement);
  };

  useEffect(() => {
    const hasSideBar = hasSideBarParent(processRef.current);
    setHasSideBar(hasSideBar);
  }, []);

  return (
    <div className="process" ref={processRef}>
      {title && <Heading>{title}</Heading>}
      {introText && <p>{introText}</p>}
      {items.length &&
        (isCarousel ? (
          <ProcessCarousel
            items={items}
            oldStyle={oldStyle}
            hasSideBar={hasSideBar}
          />
        ) : (
          <AutoGrid>
            {AutoGridItem =>
              items.map((item, index) => (
                <AutoGridItem key={item.title || item.text || index}>
                  <ProcessItem key={item.text} {...item} index={index} />
                </AutoGridItem>
              ))
            }
          </AutoGrid>
        ))}
    </div>
  );
};

Process.defaultProps = {
  items: []
};

Process.propTypes = {
  title: PropTypes.string,
  introText: PropTypes.string,
  isCarousel: PropTypes.bool,
  items: PropTypes.arrayOf(PropTypes.exact(ProcessItem.propTypes)),
  oldStyle: PropTypes.bool
};

ProcessCarousel.propTypesMeta = {
  oldStyle: 'exclude'
};

export default Process;
