import React from 'react';

import Timeline from 'components/timeline';

const TimelineBlock = props => (
  <Timeline
    numberOfSlidesToShow={{
      '0px': 1,
      '510px': 2,
      '768px': 3
    }}
    theme={Timeline.themes.block}
    {...props}
  />
);

TimelineBlock.propTypes = Timeline.propTypes;

export default TimelineBlock;
