import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

const InputControlled = ({ checked, label, name, onChange, value }) => {
  const [isChecked, setIsChecked] = useState(!!checked);

  const updateChecked = e => {
    setIsChecked(state => !state);
    onChange(e);
  };

  useEffect(() => {
    if (checked === undefined) return;
    setIsChecked(checked);
  }, [checked]);

  const id = `checkbox-${name}-${label.replace(/ /g, '_')}`;

  return (
    <>
      <input
        type="checkbox"
        id={id}
        name={name}
        onChange={updateChecked}
        value={value}
        checked={isChecked}
      />
    </>
  );
};

InputControlled.propTypes = {
  checked: PropTypes.bool,
  label: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  onChange: PropTypes.func,
  value: PropTypes.string
};

InputControlled.propTypesMeta = 'exclude';

export default InputControlled;
