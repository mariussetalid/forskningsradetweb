import React from 'react';
import PropTypes from 'prop-types';

import cn from 'classnames';

import propTypeTheme from 'utils/prop-type-theme';

import Icon from 'components/icon';
import InputControlled from './input-controlled';
import InputUncontrolled from './input-uncontrolled';
// NOTE: 'default' is a theme because these styles are used on mailchimp, and we have no control over the checkbox markup there
const themes = {
  default: '-theme-default',
  gray: '-theme-gray'
};

const Checkbox = ({
  className,
  checked,
  label,
  name,
  onChange,
  theme,
  value,
  isControlled
}) => {
  const id = `checkbox-${name}-${label.replace(/ /g, '_')}`;

  const Input = isControlled ? InputControlled : InputUncontrolled;

  return (
    <div className={cn('checkbox', theme, className)}>
      <Input
        id={id}
        name={name}
        label={label}
        onChange={onChange}
        value={value}
        checked={checked}
      />
      <label htmlFor={id}>
        <span>{label}</span>
        <Icon fill className="checkbox--checkmark" name="checkmark" />
      </label>
    </div>
  );
};

Checkbox.propTypes = {
  checked: PropTypes.bool,
  className: PropTypes.string,
  label: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  onChange: PropTypes.func,
  theme: propTypeTheme(themes),
  value: PropTypes.string,
  isControlled: PropTypes.bool
};

Checkbox.defaultProps = {
  label: '',
  theme: themes.default
};

Checkbox.propTypesMeta = {
  className: 'exclude',
  theme: 'exclude',
  isControlled: 'exclude'
};

Checkbox.themes = themes;

export default Checkbox;
