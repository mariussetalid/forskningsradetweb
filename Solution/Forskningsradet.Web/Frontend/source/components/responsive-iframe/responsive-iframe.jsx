import React, { useState } from 'react';
import PropTypes from 'prop-types';

import ContentContainer from 'components/content-container';

const ResponsiveIframe = ({ src, title }) => {
  const [height, setHeight] = useState(700);

  const getHeight = event => {
    setHeight(event.currentTarget.contentWindow.document.body.scrollHeight);
  };

  return (
    <ContentContainer className="responsive-iframe">
      <iframe
        className="responsive-iframe--iframe"
        onLoad={getHeight}
        src={src}
        title={title}
        height={`${height}px`}
      />
    </ContentContainer>
  );
};

ResponsiveIframe.propTypes = {
  src: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired
};

export default ResponsiveIframe;
