import React, { useState } from 'react';
import PropTypes from 'prop-types';

import cn from 'classnames';

import OPE from 'js/on-page-editing';

import Button from 'components/button';
import FluidImage from 'components/fluid-image';
import Link from 'components/link';
import Ref from 'components/ref';
const themes = {
  darkBlue: '-theme-dark-blue'
};
const CampaignBlock = ({
  cta,
  editorTheme,
  image,
  onPageEditing,
  title,
  isColumn
}) => {
  const [lowerHalfHeight, setLowerHalfHeight] = useState();

  const inlineStyle = isColumn ? { height: `${lowerHalfHeight}px` } : undefined;

  return (
    <div
      className={cn('campaign-block', editorTheme, { '-is-column': isColumn })}
    >
      {image && (
        <div className="campaign-block--image" style={inlineStyle}>
          <FluidImage {...image} />
        </div>
      )}
      <Ref
        className="campaign-block--text"
        refValue={setLowerHalfHeight}
        refKey={'offsetHeight'}
      >
        {title && (
          <h2 className="campaign-block--title" {...OPE(onPageEditing.title)}>
            {title}
          </h2>
        )}
        <Link
          theme={
            editorTheme === themes.darkBlue
              ? [Button.themes.orangeOutline, Button.themes.white]
              : [Button.themes.fill]
          }
          useButtonStyles
          {...cta}
        />
      </Ref>
    </div>
  );
};

CampaignBlock.propTypes = {
  cta: PropTypes.exact(Link.propTypes),
  editorTheme: PropTypes.oneOf(Object.values(themes)),
  image: PropTypes.exact(FluidImage.propTypes),
  onPageEditing: PropTypes.exact({
    title: PropTypes.string
  }),
  title: PropTypes.string.isRequired,
  isColumn: PropTypes.bool
};

CampaignBlock.defaultProps = {
  onPageEditing: {}
};

CampaignBlock.propTypesMeta = {
  isColumn: 'exclude'
};

export default CampaignBlock;
