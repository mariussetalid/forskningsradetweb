import React from 'react';
import PropTypes from 'prop-types';

import OPE from 'js/on-page-editing';

import ContentArea from 'components/content-area';

const ContactBlock = ({ items, onPageEditing, title }) => (
  <div className="contact-block">
    {title && <h2 {...OPE(onPageEditing.title)}>{title}</h2>}
    <ContentArea {...items} />
  </div>
);

ContactBlock.propTypes = {
  items: PropTypes.exact(ContentArea.propTypes),
  onPageEditing: PropTypes.exact({
    title: PropTypes.string
  }),
  title: PropTypes.string
};

ContactBlock.defaultProps = {
  items: [],
  onPageEditing: {}
};

export default ContactBlock;
