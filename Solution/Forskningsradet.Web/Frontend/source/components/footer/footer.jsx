import React from 'react';
import PropTypes from 'prop-types';

import Button from 'components/button';
import ContentContainer from 'components/content-container';
import HtmlString from 'components/html-string';
import Link from 'components/link';
import RichText from 'components/rich-text';
import SocialMediaLinkList from 'components/social-media-link-list';

const Footer = ({
  contactInfo,
  infoLinks,
  linkLists,
  newsLetter,
  socialMedia
}) => (
  <footer className="footer">
    <ContentContainer
      className="footer--content"
      theme={ContentContainer.themes.wide}
    >
      <div className="footer--contact-info">
        {contactInfo && (
          <RichText
            additionalComponentProps={{
              HtmlString: { theme: HtmlString.themes.white }
            }}
            {...contactInfo}
          />
        )}
        <Link
          className="footer--newsletter-link"
          theme={[Button.themes.orangeOutline, Button.themes.white]}
          useButtonStyles
          {...newsLetter}
        />
      </div>
      {linkLists &&
        linkLists.map(linkList => (
          <div className="footer--link-list" key={linkList.id}>
            {linkList.title && <h2>{linkList.title}</h2>}
            <ul>
              {linkList.links.map(link => (
                <li key={link.text}>
                  <Link theme={[Link.themes.white]} {...link} />
                </li>
              ))}
            </ul>
          </div>
        ))}
      <div className="footer--info-links">
        <SocialMediaLinkList
          className="footer--social-media-link-list"
          theme={SocialMediaLinkList.themes.white}
          {...socialMedia}
        />
        {infoLinks && !!infoLinks.length && (
          <ul>
            {infoLinks.map(link => (
              <li key={link.text}>
                <Link theme={Link.themes.white} {...link} />
              </li>
            ))}
          </ul>
        )}
      </div>
    </ContentContainer>
  </footer>
);

Footer.propTypes = {
  contactInfo: PropTypes.exact(RichText.propTypes),
  infoLinks: PropTypes.arrayOf(PropTypes.exact(Link.propTypes)),
  linkLists: PropTypes.arrayOf(
    PropTypes.exact({
      id: PropTypes.number,
      title: PropTypes.string,
      links: PropTypes.arrayOf(PropTypes.exact(Link.propTypes))
    })
  ),
  newsLetter: PropTypes.exact(Link.propTypes),
  socialMedia: PropTypes.exact(SocialMediaLinkList.propTypes)
};
export default Footer;
