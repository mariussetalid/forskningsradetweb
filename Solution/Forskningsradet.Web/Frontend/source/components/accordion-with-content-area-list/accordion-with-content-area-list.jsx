import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

import OPE from 'js/on-page-editing';

import Accordion from 'components/accordion';
import RichText from 'components/rich-text';
import AccordionWithContentArea from 'components/accordion-with-content-area';

const AccordionWithContentAreaList = ({
  accordion,
  content,
  description,
  htmlId,
  onPageEditing,
  richText,
  text,
  themeTags,
  title
}) => {
  const [anchorHashList, setAnchorHashList] = useState(undefined);

  const addAnchorHash = anchorHash =>
    setAnchorHashList((state = []) => [anchorHash, ...state]);

  const removeAnchorHash = anchorHash =>
    setAnchorHashList(state => state.filter(hash => hash !== anchorHash));

  const effectDependencies = !anchorHashList || anchorHashList.length;
  useEffect(() => {
    if (anchorHashList === undefined) {
      setAnchorHashList([]);
      return;
    }

    const hash = anchorHashList[0] || ' ';
    history.replaceState(undefined, undefined, hash);
  }, [effectDependencies]);

  return (
    <div className="accordion-with-content-area-list">
      <div className="accordion-with-content-area-list-header">
        {title && (
          <h2 id={htmlId} {...OPE(onPageEditing.title)}>
            {title}
          </h2>
        )}
        {description && <p>{description}</p>}
        {themeTags && !!themeTags.length && (
          <div className="accordion-with-content-area-list--tag-list">
            {themeTags.map((tag, index) => (
              <span
                className="accordion-with-content-area-list--tag-list-item"
                key={tag + index}
              >
                {tag}
              </span>
            ))}
          </div>
        )}
        {text && !themeTags && <p>{text}</p>}

        {richText && <RichText {...richText} />}
      </div>
      {content &&
        content.map(acc => (
          <AccordionWithContentArea
            key={acc.title}
            {...acc}
            accordion={accordion}
            onPageEditing={onPageEditing}
            addAnchorHash={addAnchorHash}
            removeAnchorHash={removeAnchorHash}
          />
        ))}
    </div>
  );
};

AccordionWithContentAreaList.propTypes = {
  accordion: PropTypes.exact(Accordion.propTypes),
  content: PropTypes.arrayOf(
    PropTypes.exact(AccordionWithContentArea.propTypes)
  ),
  description: PropTypes.string,
  htmlId: PropTypes.string,
  onPageEditing: PropTypes.exact({
    title: PropTypes.string
  }),
  richText: PropTypes.exact(RichText.propTypes),
  title: PropTypes.string,
  themeTags: PropTypes.arrayOf(PropTypes.string),
  text: PropTypes.string
};

AccordionWithContentAreaList.defaultProps = {
  onPageEditing: {}
};

export default AccordionWithContentAreaList;
