import React, { useState } from 'react';
import PropTypes from 'prop-types';

import Link from 'components/link';
import Heading from 'components/heading';
import Icon from 'components/icon';
import DocumentIcon from 'components/document-icon';

import cn from 'classnames';

const themes = {
  narrow: '-theme-narrow'
};

const DownloadList = ({ groups, downloadAllText, downloadAllUrl, theme }) => {
  const [highlightedItem, sethighLightedItem] = useState();
  return (
    <div className={cn('download-list', theme)}>
      {groups.map((group, index) => (
        <div className="download-list__group" key={group.heading + index}>
          {group.heading && (
            <Heading className="download-list__heading">
              {group.heading}
            </Heading>
          )}

          <ul>
            {group.items.map(item => (
              <li
                key={item.text}
                onMouseEnter={() => sethighLightedItem(item.text)}
                onFocus={() => sethighLightedItem(item.text)}
                onMouseLeave={() => sethighLightedItem('')}
              >
                <Link className="download-list__item" url={item.url}>
                  <DocumentIcon
                    size={
                      theme === themes.narrow
                        ? DocumentIcon.sizes.small
                        : undefined
                    }
                    iconTheme={item.iconTheme}
                    type={
                      highlightedItem === item.text
                        ? DocumentIcon.types.hover
                        : undefined
                    }
                  ></DocumentIcon>
                  <div className="download-list__item-text">{item.text}</div>
                  <Icon
                    className="download-list__download-icon"
                    name="download-circle"
                  ></Icon>
                </Link>
              </li>
            ))}
          </ul>
        </div>
      ))}
      <div>
        {downloadAllText && downloadAllUrl && (
          <div className="download-list__download-all">
            <Link icon="download-small" url={downloadAllUrl}>
              {downloadAllText}
            </Link>
          </div>
        )}
      </div>
    </div>
  );
};

DownloadList.propTypes = {
  groups: PropTypes.arrayOf(
    PropTypes.exact({
      heading: PropTypes.string,
      items: PropTypes.arrayOf(
        PropTypes.exact({
          text: PropTypes.string,
          iconTheme: DocumentIcon.propTypes.iconTheme,
          url: PropTypes.string
        })
      )
    })
  ),
  downloadAllText: PropTypes.string,
  downloadAllUrl: PropTypes.string,
  theme: PropTypes.oneOf(Object.values(themes))
};

DownloadList.defaultProps = {
  groups: []
};

DownloadList.propTypesMeta = {
  theme: 'exclude'
};

DownloadList.themes = themes;

export default DownloadList;
