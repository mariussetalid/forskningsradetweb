import { useState, useEffect, useRef } from 'react';
import exposedValues from 'js/utils/exposed-values';

// Sends ID with open() func for every htmlId within content (links)
// open() has access to latest isOpen bool from Accordion through reference
const useExpose = content => {
  const isOpenRef = useRef();
  const [isFirstRender, setIsFirstRender] = useState(true);

  const updateIsOpen = isOpen => {
    isOpenRef.current = isOpen;
  };

  const expose = toggle => {
    const open = () => {
      if (!isOpenRef.current) toggle();
    };
    if (!content || !content.blocks) return;
    const htmlIds = content.blocks.map(block => block.componentData.htmlId);
    htmlIds.map(id => exposedValues.addValue(id, open));
  };

  useEffect(() => {
    setIsFirstRender(false);
  });

  return [isFirstRender, updateIsOpen, expose];
};
export default useExpose;
