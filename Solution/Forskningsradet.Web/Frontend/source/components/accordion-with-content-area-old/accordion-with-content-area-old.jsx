import React from 'react';
import PropTypes from 'prop-types';

import cn from 'classnames';

import OPE from 'js/on-page-editing';

import Accordion from 'components/accordion';
import ContentArea from 'components/content-area';
import Icon from 'components/icon';
import RichText from 'components/rich-text';

import useExpose from './use-expose';

const AccordionWithContentAreaOld = ({
  accordion,
  content,
  description,
  htmlId,
  onPageEditing,
  richText,
  text,
  title
}) => {
  const isEditMode = Object.keys(onPageEditing).length > 0;
  const [isFirstRender, updateIsOpen, expose] = useExpose(content);
  const isTargetedByAnchor = () =>
    htmlId && '#' + htmlId === window.location.hash;

  return content ? (
    <Accordion {...accordion} expandOnMount={() => isTargetedByAnchor()}>
      {({ Collapse, Button, isOpen, toggle }) => {
        updateIsOpen(isOpen);
        if (isFirstRender) expose(toggle);
        return (
          <div className="accordion-with-content-area-old">
            <div
              className="accordion-with-content-area-old--header"
              onClick={isEditMode ? () => {} : toggle}
            >
              <div>
                {title && (
                  <h3 id={htmlId} {...OPE(onPageEditing.title)}>
                    {title}
                  </h3>
                )}
                {description && <p>{description}</p>}
              </div>

              <Button className="accordion-with-content-area-old--button">
                <Icon
                  className={cn('accordion-with-content-area-old--icon', {
                    '-is-open': isOpen
                  })}
                  name="small-arrow-down"
                />
              </Button>
            </div>

            <Collapse>
              <div className="accordion-with-content-area-old--content">
                {text && <p>{text}</p>}
                {richText && <RichText {...richText} />}
                <ContentArea
                  additionalComponentProps={{
                    TextWithSidebar: { headingLevel: 4 }
                  }}
                  className="accordion-with-content-area-old--content-area"
                  {...content}
                />
              </div>
            </Collapse>
          </div>
        );
      }}
    </Accordion>
  ) : (
    <div className="accordion-with-content-area-old" aria-disabled>
      <div className="accordion-with-content-area-old--header accordion-with-content-area-old--header--disabled">
        <div>
          {title && (
            <h3 id={htmlId} {...OPE(onPageEditing.title)}>
              {title}
            </h3>
          )}
          {description && <p>{description}</p>}
        </div>
      </div>
    </div>
  );
};

AccordionWithContentAreaOld.propTypes = {
  accordion: PropTypes.exact(Accordion.propTypes),
  content: PropTypes.exact(ContentArea.propTypes),
  description: PropTypes.string,
  htmlId: PropTypes.string,
  onPageEditing: PropTypes.exact({
    title: PropTypes.string
  }),
  richText: PropTypes.exact(RichText.propTypes),
  title: PropTypes.string,
  text: PropTypes.string
};

AccordionWithContentAreaOld.defaultProps = {
  onPageEditing: {}
};

export default AccordionWithContentAreaOld;
