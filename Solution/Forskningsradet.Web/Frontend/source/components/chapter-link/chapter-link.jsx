import React from 'react';
import PropTypes from 'prop-types';

import Link from 'components/link';

const ChapterLink = ({ chapterNumber, hasDash, isCurrent, level, link }) => (
  <Link
    className="chapter-link"
    theme={isCurrent && [Link.themes.black, Link.themes.bold]}
    {...link}
  >
    {chapterNumber && (
      <span
        className="chapter-link--number"
        style={{ width: `${15 * level}px` }}
      >
        {chapterNumber}
      </span>
    )}

    {hasDash && (
      <span
        className="chapter-link--dash"
        style={{ width: `${5 * level}px` }}
      />
    )}
  </Link>
);

ChapterLink.propTypes = {
  chapterNumber: PropTypes.string,
  hasDash: PropTypes.bool,
  isCurrent: PropTypes.bool,
  link: PropTypes.exact(Link.propTypes),
  level: PropTypes.number
};

ChapterLink.defaultProp = {
  level: 1
};

ChapterLink.propTypesMeta = {
  level: 'exclude'
};

export default ChapterLink;
