import React from 'react';
import PropTypes from 'prop-types';

import propTypeTheme from 'utils/prop-type-theme';

import cn from 'classnames';

import Accordion from 'components/accordion';
import ChapterLink from 'components/chapter-link';

const themes = {
  outlineButton: '-theme-outline-button'
};

const ChapterNavigationList = ({
  accordion,
  link,
  isCurrent,
  level,
  linkOrLinkList,
  theme
}) => {
  return (
    <Accordion {...accordion}>
      {({ Button, Collapse, isOpen, toggle }) => (
        <div className="chapter-navigation-list">
          <div className="chapter-navigation-list--header">
            <ChapterLink isCurrent={isCurrent} level={level} {...link} />
            <Button
              icon="small-arrow-down"
              className={cn('chapter-navigation-list--toggle', theme, {
                '-is-active': isOpen
              })}
              onClick={toggle}
            />
          </div>

          <Collapse className="chapter-navigation-list--content">
            <ul className="link-list">
              {linkOrLinkList.map(({ link, linkOrLinkList }, index) => {
                if (link) {
                  return (
                    <li key={link.chapterNumber + String(index)}>
                      <ChapterLink level={level + 1} {...link} />
                    </li>
                  );
                }

                if (linkOrLinkList) {
                  return (
                    <li
                      className="-has-button"
                      key={linkOrLinkList.title + String(index)}
                    >
                      <ChapterNavigationList
                        level={level + 1}
                        theme={themes.outlineButton}
                        {...linkOrLinkList}
                      />
                    </li>
                  );
                }
              })}
            </ul>
          </Collapse>
        </div>
      )}
    </Accordion>
  );
};

ChapterNavigationList.propTypes = {
  accordion: PropTypes.exact(Accordion.propTypes),
  link: PropTypes.exact(ChapterLink.propTypes),
  isCurrent: PropTypes.bool,
  level: PropTypes.number,
  theme: propTypeTheme(themes)
};

// PropTypes is mutated because linkOrLinkList refers to ChapterNavgiationList
ChapterNavigationList.propTypes.linkOrLinkList = PropTypes.arrayOf(
  PropTypes.exact({
    linkOrLinkList: PropTypes.exact(ChapterNavigationList.propTypes),
    link: PropTypes.exact(ChapterLink.propTypes)
  })
).isRequired;

ChapterNavigationList.defaultProps = {
  level: 1,
  linkOrLinkList: []
};

ChapterNavigationList.propTypesMeta = {
  level: 'exclude',
  theme: 'exclude'
};

ChapterNavigationList.themes = themes;

export default ChapterNavigationList;
