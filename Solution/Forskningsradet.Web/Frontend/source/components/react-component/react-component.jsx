const ReactComponent = {};

// Empty propTypes needed in order for ReactComponent.cs to be generated.
ReactComponent.propTypes = {};

// This component exists in order to generate ReactComponent.cs, and to be able to reference this class in propTypes. The ReactComponent C# class is used in order to have a type that matches any React component view model. It is used in ContentArea.jsx to describe 'componentData'.
// 'ReactComponent' is used as the base class in all generated .cs-files (configurerd in CSharpWebpackPlugin options in webpack.config.js)
export default ReactComponent;
