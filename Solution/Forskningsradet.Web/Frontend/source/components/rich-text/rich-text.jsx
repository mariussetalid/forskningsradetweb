import React from 'react';

import ContentArea from 'components/content-area';

const RichText = props => (
  <ContentArea enableElementSizing={false} {...props} />
);

RichText.propTypes = ContentArea.propTypes;

export default RichText;
