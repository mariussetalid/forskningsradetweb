import React from 'react';
import PropTypes from 'prop-types';

import cn from 'classnames';

import Button from 'components/button';
import ContentContainer from 'components/content-container';
import FluidImage from 'components/fluid-image';
import Link from 'components/link';
import Map from 'components/map';
import TagLinkList from 'components/tag-link-list';

import OPE from 'js/on-page-editing';

const hyphenate = text => text.replace(/_/g, '&shy;');

const PageHeader = ({
  image,
  ingress,
  links,
  map,
  onPageEditing,
  tags,
  title,
  portfolioLinks
}) => (
  <div className="page-header">
    {(image || map || links) && (
      <div className={cn('page-header--top', { '-has-image': image || map })}>
        {map && <Map {...map} />}
        {image && <FluidImage {...image} />}
        {links && (
          <ContentContainer className="page-header--links-container">
            <div className="page-header--links" {...OPE(onPageEditing.links)}>
              {links.map(link => (
                <Link
                  className="page-header--link"
                  key={link.url}
                  useButtonStyles={true}
                  theme={[Button.themes.fill, Button.themes.big]}
                  {...link}
                />
              ))}
            </div>
          </ContentContainer>
        )}
      </div>
    )}
    <div className="page-header--title" {...OPE(onPageEditing.title)}>
      {title && (
        <h1 dangerouslySetInnerHTML={{ __html: hyphenate(title) }}></h1>
      )}
    </div>
    <ContentContainer>
      <div {...OPE(onPageEditing.ingress)}>
        {ingress && (
          <p
            className={cn('page-header--ingress', {
              '-has-portfolio-links': portfolioLinks
            })}
          >
            {ingress}
          </p>
        )}
        {portfolioLinks && !!portfolioLinks.length && (
          <div className="page-header--portfolio-links">
            {portfolioLinks.map((link, index) => {
              const theme = !index
                ? [Button.themes.fill, Button.themes.big]
                : [Button.themes.orangeOutline, Button.themes.big];
              return (
                <Link
                  key={link.text}
                  className="page-header--portfolio-link"
                  {...link}
                  theme={theme}
                  useButtonStyles
                />
              );
            })}
          </div>
        )}
        {tags && <TagLinkList {...tags} />}
      </div>
    </ContentContainer>
  </div>
);

PageHeader.propTypes = {
  image: PropTypes.exact(FluidImage.propTypes),
  ingress: PropTypes.string,
  links: PropTypes.arrayOf(PropTypes.exact(Link.propTypes)),
  portfolioLinks: PropTypes.arrayOf(PropTypes.exact(Link.propTypes)),
  map: PropTypes.exact(Map.propTypes),
  onPageEditing: PropTypes.exact({
    title: PropTypes.string,
    ingress: PropTypes.string,
    links: PropTypes.string
  }),
  title: PropTypes.string.isRequired,
  tags: PropTypes.exact(TagLinkList.propTypes)
};

PageHeader.defaultProps = {
  onPageEditing: {}
};

export default PageHeader;
