import React from 'react';
import PropTypes from 'prop-types';

import cn from 'classnames';

import HtmlString from 'components/html-string';
import IconWarning from 'components/icon-warning';

const Message = ({ text, theme }) => (
  <div className={cn('message', theme)}>
    <div className="message--content">
      <IconWarning theme={theme} size={IconWarning.sizes.wide} />
      <div className={cn('message--text')}>
        <HtmlString {...text} />
      </div>
    </div>
  </div>
);

Message.propTypes = {
  text: PropTypes.exact(HtmlString.propTypes),
  theme: IconWarning.propTypes.theme
};

Message.themes = IconWarning.themes;

export default Message;
