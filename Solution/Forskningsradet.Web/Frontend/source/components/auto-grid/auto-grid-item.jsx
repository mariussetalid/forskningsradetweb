import React from 'react';
import cn from 'classnames';
import PropTypes from 'prop-types';

const AutoGridItem = (minWidth, gutter, gridWidth) => {
  const GridItem = ({ children, className }) => {
    const columns = Math.floor(gridWidth / (minWidth + gutter));

    const style = minWidth
      ? {
          minWidth: `${minWidth}px`,
          width: `${100 / columns}%`,
          borderWidth: `0 ${gutter}px ${gutter}px 0`
        }
      : {};

    return (
      <li className={cn('auto-grid__item', className)} style={style}>
        {children}
      </li>
    );
  };

  GridItem.propTypes = {
    className: PropTypes.string,
    children: PropTypes.node
  };

  return GridItem;
};

AutoGridItem.propTypes = 'exclude';

export default AutoGridItem;
