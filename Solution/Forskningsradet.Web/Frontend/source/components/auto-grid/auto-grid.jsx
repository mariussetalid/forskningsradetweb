/*
Component for creating flexible grid that where amount of grid columns
adjust to fit as many columns without column width going below minWidth.
(exactly like 'display grid' with 'auto-fill' but works for IE).
*/

import React, { useState } from 'react';
import cn from 'classnames';
import PropTypes from 'prop-types';
import resize from 'js/hooks/resize';
import IEWrapper from 'js/HOC/IEWrapper';
import AutoGridItem from './auto-grid-item';

const AutoGridLogic = (minWidth, gutter) => {
  const [width, setWidth] = useState(0);

  const style = {
    gridTemplateColumns: `repeat(auto-fill, minmax(${minWidth}px, 1fr))`,
    padding: `${gutter}px 0 0 ${gutter}px`,
    margin: `-${gutter}px`
  };

  const setGridWidth = node => {
    if (!node) return;
    if (node.offsetWidth === width) return;
    setWidth(node.offsetWidth);
  };

  resize(setGridWidth);

  return [style, width, setGridWidth];
};

const AutoGrid = ({
  children = () => {},
  isList,
  minWidth = 250,
  gutter = 24,
  IELogic
}) => {
  if (isList) {
    minWidth = null;
    gutter = null;
  }
  const defaultStyle = {
    gridTemplateColumns: `repeat(auto-fill, minmax(${minWidth}px, 1fr))`
  };

  const [style = defaultStyle, width, setGridWidth] = IELogic(
    AutoGridLogic,
    minWidth,
    gutter
  );

  const ItemComponent = AutoGridItem(minWidth, gutter, width);

  return (
    <ul
      className={cn('auto-grid', { '-theme-grid': minWidth })}
      style={style}
      ref={setGridWidth}
    >
      {children(ItemComponent)}
    </ul>
  );
};

AutoGrid.propTypes = {
  children: PropTypes.func,
  minWidth: PropTypes.number,
  gutter: PropTypes.number,
  IELogic: PropTypes.func,
  isList: PropTypes.bool
};

AutoGrid.propTypesMeta = 'exclude';

export default IEWrapper(AutoGrid);
