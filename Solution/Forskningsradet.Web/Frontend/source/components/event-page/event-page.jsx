import React, { useState } from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import OPE from 'js/on-page-editing';

import Button from 'components/button';
import ContentArea from 'components/content-area';
import ContentContainer from 'components/content-container';
import StickyMenuOnTabs from 'components/sticky-menu-on-tabs';
import ContentWithSidebar from 'components/content-with-sidebar';
import EventData from './event-data';
import EventPageLink from './event-page-link';
import Link from 'components/link';
import RichText from 'components/rich-text';
import OptionsModal from 'components/options-modal';
import DateCardDates from 'components/date-card/date-card-dates';
import DateCardMedia from 'components/date-card/date-card-media';
import EventImage from 'components/event-image';
import Label from 'components/label';

const EventPage = ({
  contactInfo,
  metadata,
  labels,
  links,
  onPageEditing,
  registrationLink,
  eventImage,
  richText,
  schedule,
  speakers,
  title,
  share,
  dateContainer,
  media,
  menu
}) => {
  const [dateHeight, setDateHeight] = useState();
  const { image = {} } = eventImage;
  if (dateContainer.isPastDate) image.src = '';
  return (
    <div className="event-page">
      <div className="event-page--header">
        <div className="event-page--header-left">
          <div
            className={cn('event-page--image-container', {
              '-default-image': !image.src,
              '-is-past-date': dateContainer.isPastDate
            })}
            style={{ minHeight: dateHeight }}
          >
            {dateContainer.dates && (
              <DateCardDates
                className={cn('event-page--dates', {
                  '-dark-filter': eventImage.image && !!eventImage.image.src
                })}
                {...dateContainer}
                theme={!image.src && DateCardDates.themes.big}
                setHeight={setDateHeight}
              />
            )}
            <EventImage {...eventImage} />
          </div>
        </div>
        <div className="event-page--header-text">
          {title && (
            <h1
              data-epi-type="title"
              className="event-page--header-title"
              {...OPE(onPageEditing.title)}
            >
              {title}
            </h1>
          )}
          <div data-epi-type="content">
            <EventData {...metadata} />
            <DateCardMedia {...media} />
            <div className="event-page--registration">
              <Link
                theme={[
                  Button.themes.fill,
                  Button.themes.link,
                  Button.themes.big
                ]}
                useButtonStyles={true}
                {...registrationLink}
              />
            </div>
          </div>
        </div>
      </div>
      <ContentContainer
        theme={ContentContainer.themes.wide}
        className="event-page--content-container"
      >
        <ContentWithSidebar
          theme={ContentWithSidebar.themes.sidebarLeft}
          className="event-page--content-with-sidebar"
          useContentContainer={false}
        >
          <ContentWithSidebar.Content className="event-page--content">
            <div className="event-page--links">
              {links.map(link => (
                <EventPageLink key={link.url} {...link} />
              ))}
              {share && (
                <OptionsModal
                  theme={OptionsModal.themes.isProposalOrEvent}
                  {...share}
                />
              )}
            </div>
            {richText && (
              <div className="event-page--text" data-epi-type="content">
                {labels.about && <Label {...labels.about} />}
                <RichText {...richText} />
              </div>
            )}
            {speakers && (
              <div className="event-page--speakers" data-epi-type="content">
                {labels.speakers && <Label {...labels.speakers} />}
                <ContentArea {...speakers} />
              </div>
            )}
            {schedule && (
              <div className="event-page--schedule" data-epi-type="content">
                {labels.schedule && <Label {...labels.schedule} />}
                <ContentArea
                  className="event-page--schedule-content"
                  {...schedule}
                />
              </div>
            )}
            {contactInfo && (
              <div className="event-page--contact" id={labels.contact.htmlId}>
                <ContentArea
                  enableElementSizing={false}
                  theme={ContentArea.themes.twoColumns}
                  {...contactInfo}
                />
              </div>
            )}
          </ContentWithSidebar.Content>

          <ContentWithSidebar.Sidebar className="event-page--sidebar">
            <StickyMenuOnTabs {...menu} className="event-page--menu" />
          </ContentWithSidebar.Sidebar>
        </ContentWithSidebar>
      </ContentContainer>
    </div>
  );
};

EventPage.propTypes = {
  contactInfo: PropTypes.exact(ContentArea.propTypes),
  labels: PropTypes.exact({
    about: PropTypes.exact(Label.propTypes),
    contact: PropTypes.exact(Label.propTypes),
    speakers: PropTypes.exact(Label.propTypes),
    schedule: PropTypes.exact(Label.propTypes)
  }),
  links: PropTypes.arrayOf(PropTypes.exact(EventPageLink.propTypes)),
  metadata: PropTypes.exact(EventData.propTypes),
  onPageEditing: PropTypes.exact({
    title: PropTypes.string
  }),
  speakers: PropTypes.exact(ContentArea.propTypes),
  richText: PropTypes.exact(RichText.propTypes),
  registrationLink: PropTypes.exact(Link.propTypes),
  schedule: PropTypes.exact(ContentArea.propTypes),
  title: PropTypes.string,
  share: PropTypes.exact(OptionsModal.propTypes),
  eventImage: PropTypes.exact(EventImage.propTypes),
  dateContainer: PropTypes.exact(DateCardDates.propTypes),
  media: PropTypes.exact(DateCardMedia.propTypes),
  menu: PropTypes.exact(StickyMenuOnTabs.propTypes)
};

EventPage.defaultProps = {
  labels: {},
  links: [],
  onPageEditing: {},
  dateContainer: {},
  eventImage: {}
};

export default EventPage;
