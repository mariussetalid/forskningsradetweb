import React from 'react';
import PropTypes from 'prop-types';

import Button from 'components/button';
import DateCard from 'components/date-card';
import Link from 'components/link';

const FutureEvents = ({ items, link, title, text }) => (
  <div className="future-events">
    {title && <h2 className="future-events--title">{title}</h2>}
    <p>{text}</p>
    <div className="future-events--list">
      {items.map(event => (
        <DateCard
          headingLevel={3}
          key={event.id}
          showMedia={false}
          theme={[DateCard.themes.rightAlignedTags]}
          {...event}
        />
      ))}
    </div>
    <Link
      theme={[Button.themes.big, Button.themes.fill]}
      useButtonStyles={true}
      {...link}
    />
  </div>
);

FutureEvents.propTypes = {
  items: PropTypes.arrayOf(PropTypes.exact(DateCard.propTypes)),
  link: PropTypes.exact(Link.propTypes),
  title: PropTypes.string,
  text: PropTypes.string
};

FutureEvents.defaultProps = {
  items: []
};
export default FutureEvents;
