import React from 'react';
import PropTypes from 'prop-types';

import cn from 'classnames';

// Checks whether a string represents a number, to allow for special styling of numbers-as-text
const isNumber = (string = '') => !isNaN(Number(string.replace(/\s/g, '')));

const Table = ({ footer, header, rows }) => (
  <div className="table">
    <table>
      {header.length > 0 && (
        <thead>
          <tr>
            {header.map(column => (
              <th key={column}>{column}</th>
            ))}
          </tr>
        </thead>
      )}
      {rows.length > 0 && (
        <tbody>
          {rows.map((row = []) => (
            <tr key={row}>
              {row.map((column, index) => (
                <td
                  className={cn({ '-is-number': isNumber(column) })}
                  key={column + index}
                >
                  {column}
                </td>
              ))}
            </tr>
          ))}
        </tbody>
      )}
      {footer.length > 0 && (
        <tfoot>
          <tr>
            {footer.map(column => (
              <td key={column}>{column}</td>
            ))}
          </tr>
        </tfoot>
      )}
    </table>
  </div>
);

Table.propTypes = {
  footer: PropTypes.arrayOf(PropTypes.string),
  header: PropTypes.arrayOf(PropTypes.string),
  rows: PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.string))
};

Table.defaultProps = {
  footer: [],
  header: [],
  rows: []
};

export default Table;
