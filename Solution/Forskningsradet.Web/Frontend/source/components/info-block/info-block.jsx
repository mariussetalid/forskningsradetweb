import React from 'react';
import PropTypes from 'prop-types';

import cn from 'classnames';

import Button from 'components/button';
import Image from 'components/image';
import Link from 'components/link';
import LinkList from 'components/link-list';
import RichText from 'components/rich-text';

import OPE from 'js/on-page-editing';
import propTypeTheme from 'js/utils/prop-type-theme';

const themes = {
  blue: '-theme-blue',
  darkBlue: '-theme-dark-blue',
  orange: '-theme-orange'
};

const InfoBlock = ({
  cta,
  editorTheme,
  icon,
  linkList,
  onPageEditing,
  title,
  text,
  theme,
  url
}) => {
  const titleElement = url ? (
    <Link
      iconBeforeChildren="small-arrow-right"
      iconClassName="info-block--title-icon"
      url={url}
    >
      {title}
    </Link>
  ) : (
    title
  );

  // The fallback theme can't be handled with defaultProps because the theme is controlled by both 'theme' and 'editorTheme', which means you could get unexpected combinations of themes.
  const fallbackTheme = !theme && !editorTheme ? themes.blue : '';

  return (
    <div className={cn('info-block', theme, editorTheme, fallbackTheme)}>
      {icon && (
        <Image className="info-block--icon" responsive={false} {...icon} />
      )}
      {titleElement && (
        <h2 className={cn({ '-has-link': url })} {...OPE(onPageEditing.title)}>
          {titleElement}
        </h2>
      )}
      {text && <RichText className="info-block--text" {...text} />}

      {cta && (
        <Link
          className="info-block--cta-button"
          theme={Button.themes.fill}
          useButtonStyles={true}
          {...cta}
        />
      )}

      {linkList && <LinkList {...linkList} withIcon={true} />}
    </div>
  );
};

InfoBlock.propTypes = {
  cta: PropTypes.exact(Link.propTypes),
  editorTheme: PropTypes.oneOf(Object.values(themes)),
  icon: PropTypes.exact(Image.propTypes),
  linkList: PropTypes.exact(LinkList.propTypes),
  onPageEditing: PropTypes.exact({
    title: PropTypes.string
  }),
  title: PropTypes.string,
  text: PropTypes.exact(RichText.propTypes),
  theme: propTypeTheme(themes),
  url: PropTypes.string
};

InfoBlock.propTypesMeta = {
  theme: 'exclude'
};

InfoBlock.defaultProps = {
  onPageEditing: {}
};

InfoBlock.themes = themes;

export default InfoBlock;
