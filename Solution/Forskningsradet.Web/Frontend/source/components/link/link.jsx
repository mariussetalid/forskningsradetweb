import React from 'react';
import PropTypes from 'prop-types';

import cn from 'classnames';

import Button from 'components/button';
import Icon from 'components/icon';

import OPE from 'js/on-page-editing';
import propTypeTheme from 'js/utils/prop-type-theme';

import api from 'js/api-helper';

const themes = {
  black: '-theme-black',
  bold: '-theme-bold',
  fullWidth: '-theme-full-width',
  inlineIcon: '-theme-inline-icon',
  orangeIcon: '-theme-orange-icon',
  small: '-theme-small',
  tag: '-theme-tag',
  orangeTag: '-theme-orange-tag',
  underline: '-theme-underline',
  white: '-theme-white',
  portfolio: '-theme-portfolio'
};

const split = str => {
  if (typeof str === 'string' && str.includes('///'))
    return (
      <span>
        {str.split('///').map(line => (
          <span className="link-line" key={line}>
            {line}
          </span>
        ))}
      </span>
    );
  return str;
};
// NOTE: If link receives a Button theme, it inherits styles from the Button component instead of Link
const Link = ({
  attributes,
  children,
  className,
  icon,
  iconBeforeChildren,
  iconClassName,
  id,
  onClick,
  onPageEditing,
  text,
  theme,
  url,
  trackUrl,
  useButtonStyles,
  label,
  noHoverEffect,
  openInNewWindow,
  isExternal
}) => {
  const [role, setRole] = React.useState(null);

  React.useEffect(() => {
    setRole(onClick ? 'button' : null);
  }, [onClick]);

  const handleOnClick = e => {
    if (trackUrl) {
      var defaultBehaviour = e.ctrlKey;
      if (!defaultBehaviour) {
        e.preventDefault();
      }
      api.execute(trackUrl, {}).then(_response => {
        if (!defaultBehaviour && url) {
          window.location.href = url;
        }
      });
    }
  };

  const Element = url ? 'a' : 'span';
  const baseClassName = useButtonStyles ? Button.className : 'link';

  const showExternalIcon = isExternal && url;
  const externalIcon = showExternalIcon
    ? {
        name: 'tiny-arrow',
        className: 'external-link-icon'
      }
    : {};
  const SpanIfExternal = showExternalIcon ? 'span' : React.Fragment;

  const newWindowProps = openInNewWindow
    ? { target: '_blank', rel: 'noopener norefferer' }
    : {};

  return (
    <Element
      className={cn(baseClassName, className, theme, {
        '-has-children': !!text || !!children,
        '-has-icon-left': iconBeforeChildren,
        '-has-icon-right': icon,
        '-has-hover-effect': !noHoverEffect,
        '-is-external-link': showExternalIcon
      })}
      id={id}
      href={url || null} // The 'null' makes sure that the 'href' attribute is not written to the DOM when 'url' is an empty string
      onClick={onClick || handleOnClick}
      role={role}
      aria-label={label}
      {...newWindowProps}
      {...attributes}
      {...OPE(onPageEditing)}
    >
      <SpanIfExternal>
        {iconBeforeChildren && (
          <Icon className={iconClassName} name={iconBeforeChildren} />
        )}
        {children}
        {split(text)}
        {showExternalIcon && <Icon {...externalIcon} />}
        {icon && <Icon className={iconClassName} name={icon} />}
      </SpanIfExternal>
    </Element>
  );
};

Link.propTypes = {
  attributes: PropTypes.object,
  children: PropTypes.node,
  className: PropTypes.string,
  icon: PropTypes.string,
  iconBeforeChildren: PropTypes.string,
  iconClassName: PropTypes.string,
  id: PropTypes.string,
  onClick: PropTypes.func,
  onPageEditing: PropTypes.string,
  text: PropTypes.string,
  theme: propTypeTheme(Object.assign({}, Button.themes, themes)),
  url: PropTypes.string,
  trackUrl: PropTypes.string,
  useButtonStyles: PropTypes.bool,
  label: PropTypes.string,
  noHoverEffect: PropTypes.bool,
  isExternal: PropTypes.bool,
  openInNewWindow: PropTypes.bool,
  isPrimary: PropTypes.bool // This is only to add it to the CS model
};

Link.propTypesMeta = {
  attributes: 'exclude',
  className: 'exclude',
  icon: 'exclude',
  iconBeforeChildren: 'exclude',
  iconClassName: 'exclude',
  theme: 'exclude',
  useButtonStyles: 'exclude',
  noHoverEffect: 'exclude',
  openInNewWindow: 'exclude'
};

Link.themes = themes;

export default Link;
