import React from 'react';
import PropTypes from 'prop-types';

import cn from 'classnames';

import getNewSrc from 'js/responsive-images';

import OPE from 'js/on-page-editing';

const Image = ({
  alt,
  className,
  initialSize,
  onPageEditing,
  responsive,
  src
}) => {
  const image = React.useRef();
  const initialSrc = responsive ? getNewSrc(src, initialSize) : src;
  const [currentSrc, setSrc] = React.useState(initialSrc);

  React.useEffect(() => {
    const parentNode = image.current.parentElement;

    if (parentNode && responsive) {
      setSrc(getNewSrc(src, parentNode.offsetWidth, initialSize));
    }
  });

  return (
    <img
      className={cn('image', className)}
      src={currentSrc}
      alt={alt}
      ref={image}
      {...OPE(onPageEditing.image)}
    />
  );
};

Image.propTypes = {
  alt: PropTypes.string,
  className: PropTypes.string,
  initialSize: PropTypes.number,
  onPageEditing: PropTypes.exact({
    image: PropTypes.string
  }),
  responsive: PropTypes.bool,
  src: PropTypes.string
};

Image.propTypesMeta = {
  className: 'exclude',
  initialSize: 'exclude',
  responsive: 'exclude'
};

Image.defaultProps = {
  initialSize: 300,
  onPageEditing: {},
  responsive: true
};

export default Image;
