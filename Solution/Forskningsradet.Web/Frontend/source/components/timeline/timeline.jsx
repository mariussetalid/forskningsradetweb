import React from 'react';
import PropTypes from 'prop-types';

import 'intersection-observer';
import cn from 'classnames';
import debounce from 'lodash/debounce';

import responsiveValue from 'js/utils/responsive-value';

import Button from '../button';
import Carousel from '../carousel';
import ContentContainer from '../content-container';
import RichText from '../rich-text';
import TimelineItem from './timeline-item';
import Icon from 'components/icon';

// NOTE: Editor themes are separated from 'themes' in order to control which themes are exposed to the backend.
const editorThemes = {
  gray: '-theme-gray'
};

const themes = {
  ...editorThemes,
  block: '-theme-block'
};

class Timeline extends React.Component {
  static propTypes = {
    editorTheme: PropTypes.oneOf(Object.values(editorThemes)),
    labels: PropTypes.exact({
      next: PropTypes.string,
      previous: PropTypes.string
    }),
    numberOfSlidesToShow: PropTypes.object,
    items: PropTypes.arrayOf(PropTypes.exact(TimelineItem.propTypes)),
    startIndex: PropTypes.number,
    title: PropTypes.string,
    theme: PropTypes.oneOf(Object.values(themes))
  };

  static propTypesMeta = {
    numberOfSlidesToShow: 'exclude',
    startIndex: 'int?',
    theme: 'exclude'
  };

  static defaultProps = {
    labels: {},
    numberOfSlidesToShow: {
      '0px': 1,
      '510px': 2,
      '768px': 3,
      '1024px': 4
    },
    items: []
  };

  state = {
    isMounted: false,
    numberOfSlidesToShow: 1,
    isVisible: false
  };

  ref = React.createRef();

  setNumberOfSlidesToShow = debounce(
    () => {
      const numberOfSlidesToShow = responsiveValue(
        this.props.numberOfSlidesToShow
      );

      this.setState({ numberOfSlidesToShow });
    },
    200,
    { leading: true }
  );

  componentDidMount() {
    this.setNumberOfSlidesToShow();
    this.setState({ isMounted: true });
    window.addEventListener('resize', this.setNumberOfSlidesToShow);
    const observer = new IntersectionObserver(
      ([entry]) => {
        if (entry.isIntersecting) {
          this.setState({ isVisible: true });
          observer.disconnect();
        }
      },
      { root: null, rootMargin: '0px', threshold: 0.5 }
    );
    if (this.ref.current) {
      observer.observe(this.ref.current);
    }
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.setNumberOfSlidesToShow);
  }

  render() {
    const { labels } = this.props;

    const themeList = [this.props.theme, this.props.editorTheme];

    return (
      <React.Fragment>
        <ContentContainer className="timeline-print">
          {this.props.title && <h2>{this.props.title}</h2>}
          {this.props.items.map(item => (
            <div
              className="timeline-print--item"
              key={item.title || item.subTitle}
            >
              {item.title && (
                <h3 className="timeline-print--title">{item.title}</h3>
              )}
              {item.subTitle && (
                <h4 className="timeline-print--sub-title">{item.subTitle}</h4>
              )}
              <RichText {...item.text} />
            </div>
          ))}
        </ContentContainer>
        <div
          ref={this.ref}
          id="timeline"
          className={cn('timeline', themeList, {
            '-is-mounted': this.state.isMounted
          })}
        >
          <ContentContainer theme={ContentContainer.themes.wide}>
            {this.props.title && <h2>{this.props.title}</h2>}
          </ContentContainer>
          <ContentContainer
            className="timeline--content"
            theme={ContentContainer.themes.wide}
          >
            <div className="timeline--track">
              {this.state.isVisible && (
                <Carousel
                  numberOfSlidesToShow={this.state.numberOfSlidesToShow}
                  startIndex={this.props.startIndex}
                  nextButton={(next, hasNext) => (
                    <Button
                      className="timeline--next"
                      disabled={!hasNext}
                      onClick={next}
                      title={labels.next}
                    >
                      <Icon
                        className="timeline--next-icon"
                        fill
                        name="open-arrow-circle"
                      ></Icon>
                    </Button>
                  )}
                  previousButton={(previous, hasPrevious) => (
                    <Button
                      className="timeline--previous"
                      disabled={!hasPrevious}
                      onClick={previous}
                      title={labels.previous}
                    >
                      <Icon
                        className="timeline--previous-icon"
                        fill
                        name="open-arrow-circle"
                      ></Icon>
                    </Button>
                  )}
                >
                  {this.props.items.map((item, index) => {
                    const delay = 500 * (index - this.props.startIndex + 1);
                    return (
                      <TimelineItem
                        key={item.title + item.subTitle}
                        delay={delay}
                        isLast={index === this.props.items.length - 1}
                        {...item}
                      />
                    );
                  })}
                </Carousel>
              )}
            </div>
          </ContentContainer>
        </div>
      </React.Fragment>
    );
  }
}

Timeline.themes = themes;

export default Timeline;
