import React from 'react';
import PropTypes from 'prop-types';

import Link from 'components/link';

const EmployeeSearchResult = ({ labeledLinks, subTitle, texts, title }) => (
  <div className="employee-search-result">
    {title && <h3>{title}</h3>}

    {subTitle && <h4>{subTitle}</h4>}

    {texts.length > 0 && (
      <ul className="employee-search-result--texts">
        {texts.map(text => (
          <li key={text}>{text}</li>
        ))}
      </ul>
    )}

    {labeledLinks.length > 0 && (
      <ul className="employee-search-result--links">
        {labeledLinks.map(({ label, link }) => (
          <li key={label}>
            <span>{label}: </span>
            <Link {...link} />
          </li>
        ))}
      </ul>
    )}
  </div>
);

EmployeeSearchResult.propTypes = {
  labeledLinks: PropTypes.arrayOf(
    PropTypes.exact({
      label: PropTypes.string,
      link: PropTypes.exact(Link.propTypes)
    })
  ),
  subTitle: PropTypes.string,
  texts: PropTypes.arrayOf(PropTypes.string),
  title: PropTypes.string.isRequired
};

EmployeeSearchResult.defaultProps = {
  texts: [],
  labeledLinks: []
};

export default EmployeeSearchResult;
