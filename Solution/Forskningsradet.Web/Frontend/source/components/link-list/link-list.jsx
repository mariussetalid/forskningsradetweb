import React from 'react';
import PropTypes from 'prop-types';

import cn from 'classnames';

import OPE from 'js/on-page-editing';
import propTypeTheme from 'js/utils/prop-type-theme';

import Link from 'components/link';
import NestedLink from 'components/nested-link';

const themes = {
  border: '-theme-border',
  portfolio: '-theme-portfolio',
  frontpageLinks: '-theme-frontpage-links'
};

const LinkList = ({
  className,
  items,
  nestedLinks,
  linkTheme,
  onPageEditing,
  theme,
  withIcon
}) => {
  const iconData = withIcon
    ? {
        iconBeforeChildren: 'small-arrow-right',
        theme: ['-theme-orange-icon', '-theme-inline-icon'].concat(linkTheme)
      }
    : {};
  const isPortfolio = linkTheme.indexOf(Link.themes.portfolio) !== -1;
  return (
    <ul
      className={cn('link-list', className, theme, {
        'link-list--with-icon': withIcon && !isPortfolio
      })}
      {...OPE(onPageEditing.items)}
    >
      {nestedLinks &&
        nestedLinks.map((nestedLink, index) => (
          <NestedLink
            key={nestedLink.url + String(index)}
            {...{ item: nestedLink, iconData, linkTheme }}
          />
        ))}
      {items &&
        items.map((link, index) => (
          <li key={link.url + String(index)}>
            <Link theme={linkTheme} {...iconData} {...link} />
          </li>
        ))}
    </ul>
  );
};

LinkList.propTypes = {
  className: PropTypes.string,
  items: PropTypes.arrayOf(PropTypes.exact(Link.propTypes)),
  nestedLinks: PropTypes.arrayOf(NestedLink.propTypes.item),
  linkTheme: propTypeTheme(Link.themes),
  onPageEditing: PropTypes.exact({
    items: PropTypes.string
  }),
  theme: propTypeTheme(themes),
  withIcon: PropTypes.bool
};

LinkList.propTypesMeta = {
  className: 'exclude',
  linkTheme: 'exclude',
  theme: 'exclude',
  withIcon: 'exclude',
  attributes: 'exclude',
  icon: 'exclude',
  iconBeforeChildren: 'exclude',
  iconClassName: 'exclude',
  useButtonStyles: 'exclude'
};

LinkList.defaultProps = {
  items: [],
  onPageEditing: {},
  linkTheme: []
};

LinkList.themes = themes;

export default LinkList;
