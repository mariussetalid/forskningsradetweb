import React from 'react';
import PropTypes from 'prop-types';

import FluidImage from 'components/fluid-image';
import background1 from 'images/event-background.svg';
import background2 from 'images/event-background2.svg';
import background3 from 'images/event-background3.svg';

import background1grey from 'images/event-background-grey.svg';
import background2grey from 'images/event-background2-grey.svg';
import background3grey from 'images/event-background3-grey.svg';

const normalBackgrounds = {
  background1,
  background2,
  background3
};

const greyBackgrounds = {
  background1grey,
  background2grey,
  background3grey
};

let availableIndices = [0, 1, 2];

const getBackground = backgrounds => {
  const randomIndex = Math.floor(Math.random() * availableIndices.length);
  const backgroundIndex = availableIndices[randomIndex];
  const randomBackground = Object.values(backgrounds)[backgroundIndex];

  availableIndices.splice(randomIndex, 1);
  if (availableIndices.length === 0) availableIndices = [0, 1, 2];

  return randomBackground;
};

const EventImage = ({ image = {}, background, darkFilter, greyFilter }) => {
  const backgrounds = greyFilter ? greyBackgrounds : normalBackgrounds;
  const eventImage = {
    src: image.src || backgrounds[background] || getBackground(backgrounds),
    alt: image.alt || 'background'
  };
  return (
    <div className="event-image">
      <FluidImage {...eventImage} darkFilter={darkFilter} />
    </div>
  );
};

EventImage.propTypes = {
  image: PropTypes.exact(FluidImage.propTypes),
  background: PropTypes.oneOf(Object.keys(normalBackgrounds)),
  darkFilter: PropTypes.bool,
  greyFilter: PropTypes.bool
};

EventImage.propTypesMeta = {
  darkFilter: 'exclude',
  greyFilter: 'exclude',
  theme: 'exclude'
};

export default EventImage;
