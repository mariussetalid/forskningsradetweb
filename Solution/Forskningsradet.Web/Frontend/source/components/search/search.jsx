import React from 'react';
import PropTypes from 'prop-types';

import api from 'js/api-helper';

import Button from 'components/button';
import Link from 'components/link';
import TextInput from 'components/form-elements/text-input';
import SearchSuggestions from 'components/search-suggestions';

const Search = ({
  externalResultsLabel,
  externalResultsEndpoint,
  input,
  resultsDescription,
  submit,
  searchSuggestions
}) => {
  const inputElement = React.useRef();
  const [hasText, setHasText] = React.useState(false);
  const [externalResultsCount, setExternalResultsCount] = React.useState(0);
  const [externalResultsUrl, setExternalResultsUrl] = React.useState('');

  const onInputChange = event => {
    setHasText(event.target.value.length > 0);
  };

  const removeText = () => {
    inputElement.current.value = '';
    inputElement.current.focus();

    setHasText(false);
  };

  React.useEffect(() => {
    if (externalResultsEndpoint) {
      api.get(externalResultsEndpoint).then(({ Hits, url }) => {
        setExternalResultsCount(Hits);
        setExternalResultsUrl(url);
      });
    }
  }, [externalResultsEndpoint]);

  return (
    <div className="search">
      <div className="search--input-wrapper">
        <div className="search--field">
          <TextInput
            className="search--input"
            type="search"
            onChange={onInputChange}
            ref={inputElement}
            theme={[TextInput.themes.rounded, TextInput.themes.hiddenLabel]}
            {...input}
          />
          {hasText && (
            <Button className="search--clear" icon="x" onClick={removeText} />
          )}
        </div>

        <Button
          className="search--submit"
          type="submit"
          theme={[Button.themes.fill, Button.themes.big]}
          {...submit}
        />
      </div>

      <div className="search--results">
        {resultsDescription}
        {externalResultsUrl && (
          <Link
            icon="tiny-arrow"
            iconClassName="search--results-icon"
            url={externalResultsUrl}
            text={`${externalResultsCount} ${externalResultsLabel}`}
          />
        )}
        {searchSuggestions && <SearchSuggestions {...searchSuggestions} />}
      </div>
    </div>
  );
};

Search.propTypes = {
  externalResultsLabel: PropTypes.string,
  externalResultsEndpoint: PropTypes.string,
  input: PropTypes.exact(TextInput.propTypes),
  resultsDescription: PropTypes.string,
  submit: PropTypes.exact(Button.propTypes),
  searchSuggestions: PropTypes.exact(SearchSuggestions.propTypes)
};

export default Search;
