import React from 'react';
import PropTypes from 'prop-types';

import cn from 'classnames';
import scrollingElement from '@creuna/utils/scrolling-element';

import TabContent from './tab-content';
import TabList from './tab-list';

const tabId = id => `tab-${id}`;
const contentId = id => `content-${id}`;

// NOTE: 'aria-role="tablist"' is intentionally not used for this component.
// Read about why here: https://simplyaccessible.com/article/danger-aria-tabs/

const Tabs = ({ activeTab, items, stickyMenuOnTabs }) => {
  const initialId = activeTab || (items[0] || {}).guid;
  const [currentTab, setCurrentTab] = React.useState(initialId);
  const [currentTabIndex, setCurrentTabIndex] = React.useState(0);
  const [isMounted, setIsMounted] = React.useState(false);

  const setActiveTab = id => e => {
    // NOTE: The anchor link scroll is prevented because it's intrusive for mouse user. Focus is set manually in React.useEffect below.
    e.preventDefault();

    setCurrentTab(id);
    setCurrentTabIndex(items.findIndex(item => item.guid === id));
  };

  React.useEffect(() => {
    // NOTE: This callback is called for the initial render, resulting in the first open tab receiving focus without user interaction. This is not nice, so this flag is used to prevent focus before interaction.
    if (!isMounted) {
      setIsMounted(true);
      return;
    }
    // NOTE: Manually setting scroll position after focus in order to prevent scroll consistently across browsers
    const content = document.getElementById(contentId(currentTab));
    const scrollPos = scrollingElement.scrollTop;
    content && content.focus();
    scrollingElement.scrollTop = scrollPos;
  }, [currentTab]);

  return (
    <div className="tabs">
      <TabList useOldStyle={stickyMenuOnTabs}>
        {items.map(({ guid, name }, index) => {
          const isCurrent = guid === currentTab;
          return (
            <TabList.Item
              key={guid}
              isCurrent={isCurrent}
              length={items.length}
              index={index}
              currentIndex={currentTabIndex}
            >
              <a
                aria-current={isCurrent}
                href={`#${contentId(guid)}`}
                id={tabId(guid)}
                onClick={setActiveTab(guid)}
              >
                {name}
              </a>
            </TabList.Item>
          );
        })}
      </TabList>
      <div className="tabs--content">
        {items.map(tab => {
          const isCurrent = tab.guid === currentTab;

          return (
            <div
              aria-hidden={!isCurrent}
              aria-labelledby={tabId(tab.guid)}
              id={contentId(tab.guid)}
              key={tab.name}
              className={cn('tabs--content-tab', { '-inactive': !isCurrent })}
              tabIndex={isCurrent ? -1 : null}
            >
              <TabContent
                {...tab.content}
                stickyMenuOnTabs={stickyMenuOnTabs}
              />
            </div>
          );
        })}
      </div>
    </div>
  );
};

Tabs.propTypes = {
  activeTab: PropTypes.string,
  items: PropTypes.arrayOf(
    PropTypes.exact({
      guid: PropTypes.string.isRequired,
      content: PropTypes.exact(TabContent.propTypes),
      name: PropTypes.string.isRequired
    })
  ),
  stickyMenuOnTabs: PropTypes.bool
};

Tabs.defaultProps = {
  items: []
};

export default Tabs;
