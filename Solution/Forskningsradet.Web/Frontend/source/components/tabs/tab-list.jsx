/* eslint-disable react/no-multi-comp */
import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';

import ContentContainer from 'components/content-container';

const getZIndex = (length, index, currentIndex) => {
  if (index === currentIndex) return length;
  if (index < currentIndex) return index;
  return length - index;
};

const TabList = ({ children, className, useOldStyle }) => {
  return (
    <ContentContainer
      className={cn('tab-list', className)}
      theme={useOldStyle ? ContentContainer.themes.medium : undefined}
    >
      <ul>{children}</ul>
    </ContentContainer>
  );
};
TabList.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string,
  useOldStyle: PropTypes.bool
};

TabList.propTypesMeta = 'exclude';

TabList.Item = ({ children, isCurrent, length, index, currentIndex }) => (
  <li
    className={cn({ '-is-active': isCurrent })}
    style={{
      zIndex: getZIndex(length, index, currentIndex)
    }}
  >
    {children}
  </li>
);

TabList.Item.propTypes = {
  children: PropTypes.node,
  isCurrent: PropTypes.bool,
  length: PropTypes.number,
  index: PropTypes.number,
  currentIndex: PropTypes.number
};
export default TabList;
