/* eslint-disable react/no-multi-comp */

import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';

import ContentContainer from 'components/content-container';
import ContentWithSidebar from 'components/content-with-sidebar';
import StickyMenu from 'components/sticky-menu';
import StickyMenuOnTabs from 'components/sticky-menu-on-tabs';
import TabContentSection from 'components/tab-content-section';

const TabContent = ({ menu, contentSections, stickyMenuOnTabs }) => {
  const oldStyle = !stickyMenuOnTabs ? 'tab-content--old-style' : '';

  const tabContent = (
    <React.Fragment>
      {contentSections &&
        contentSections.map((contentSection, index) => {
          if (contentSection.process && !!oldStyle)
            contentSection = {
              ...contentSection,
              process: { ...contentSection.process, oldStyle: true }
            };
          return <TabContentSection {...contentSection} key={index} />;
        })}
    </React.Fragment>
  );

  const className = cn('tab-content', oldStyle);

  return !menu ? (
    <ContentContainer
      theme={ContentContainer.themes.medium}
      className={className}
    >
      {tabContent}
    </ContentContainer>
  ) : (
    <ContentContainer
      className={cn(className, 'tab-content__container')}
      theme={ContentContainer.themes.wide}
    >
      <ContentWithSidebar
        theme={ContentWithSidebar.themes.sidebarLeft}
        className="tab-content__grid"
        useContentContainer={false}
      >
        <ContentWithSidebar.Content className="tab-content__content">
          {tabContent}
        </ContentWithSidebar.Content>
        <ContentWithSidebar.Sidebar>
          {stickyMenuOnTabs ? (
            <StickyMenuOnTabs {...menu} includeHeader />
          ) : (
            <StickyMenu {...menu} />
          )}
        </ContentWithSidebar.Sidebar>
      </ContentWithSidebar>
    </ContentContainer>
  );
};

TabContent.propTypes = {
  contentSections: PropTypes.arrayOf(
    PropTypes.exact(TabContentSection.propTypes)
  ),
  menu: PropTypes.exact(StickyMenu.propTypes),
  stickyMenuOnTabs: PropTypes.bool
};

TabContent.propTypesMeta = {
  stickyMenuOnTabs: 'exclude'
};

TabContent.defaultProps = {};

export default TabContent;
