import React from 'react';
import PropTypes from 'prop-types';

import Image from 'components/image';
import RichText from 'components/rich-text';

import logo from '../../assets/images/logo.png';
import downtime from '../../assets/images/downtime.svg';

const ErrorPage = ({ downtime, errorCode, linkUrl, logo, text }) => (
  <div className="error-page">
    {!errorCode && (
      <Image
        responsive={false}
        className="error-page--downtime"
        {...downtime}
      />
    )}
    {errorCode && <div className="error-page--code">{errorCode}</div>}
    <RichText className="error-page--text" {...text} />
    <a href={linkUrl}>
      <Image responsive={false} className="error-page--logo" {...logo} />
    </a>
  </div>
);

ErrorPage.propTypes = {
  downtime: PropTypes.exact(Image.propTypes),
  errorCode: PropTypes.string,
  linkUrl: PropTypes.string,
  logo: PropTypes.exact(Image.propTypes),
  text: PropTypes.exact(RichText.propTypes)
};

ErrorPage.defaultProps = {
  logo: { src: logo, alt: 'Forskningsrådet logo' },
  downtime: { src: downtime, alt: 'Et øyeblikk' }
};

export default ErrorPage;
