import React from 'react';
import PropTypes from 'prop-types';

import Heading from 'components/heading';
import Table from 'components/table';

const TableBlock = ({ headingLevel, table, title }) => (
  <div className="table-block">
    {title && (
      <Heading className="table-block--title" level={headingLevel}>
        {title}
      </Heading>
    )}
    <Table {...table} />
  </div>
);

TableBlock.propTypes = {
  headingLevel: PropTypes.number,
  table: PropTypes.exact(Table.propTypes).isRequired,
  title: PropTypes.string.isRequired
};

TableBlock.propTypesMeta = {
  headingLevel: 'exclude'
};

TableBlock.defaultProps = {
  headingLevel: 2
};

export default TableBlock;
