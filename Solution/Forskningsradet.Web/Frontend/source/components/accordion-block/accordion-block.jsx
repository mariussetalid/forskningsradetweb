import React from 'react';
import PropTypes from 'prop-types';

import cn from 'classnames';

import OPE from 'js/on-page-editing';

import Accordion from 'components/accordion';
import Icon from 'components/icon';
import RichText from 'components/rich-text';

const AccordionBlock = ({ accordion, onPageEditing, title, text }) => {
  const isEditMode = Object.keys(onPageEditing).length > 0;

  return (
    <Accordion {...accordion}>
      {({ Collapse, Button, isOpen, toggle }) => (
        <div className="accordion-block">
          <div
            className="accordion-block--header"
            onClick={isEditMode ? () => {} : toggle}
          >
            <h2 {...OPE(onPageEditing.title)}>
              {title}
              <Button>
                <Icon
                  className={cn('accordion-block--icon', {
                    '-is-open': isOpen
                  })}
                  name="small-arrow-down"
                />
              </Button>
            </h2>
          </div>

          <Collapse>
            <div className="accordion-block--content">
              <RichText {...text} />
            </div>
          </Collapse>
        </div>
      )}
    </Accordion>
  );
};

AccordionBlock.propTypes = {
  accordion: PropTypes.exact(Accordion.propTypes),
  onPageEditing: PropTypes.exact({
    title: PropTypes.string
  }),

  title: PropTypes.string,
  text: PropTypes.exact(RichText.propTypes)
};

AccordionBlock.defaultProps = {
  onPageEditing: {}
};

export default AccordionBlock;
