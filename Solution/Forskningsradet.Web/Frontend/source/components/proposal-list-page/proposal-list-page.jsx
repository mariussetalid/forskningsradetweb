import React from 'react';
import PropTypes from 'prop-types';

import ContentWithSidebar from 'components/content-with-sidebar';
import EmptyList from 'components/empty-list';
import FilterLayout from 'components/filter-layout';
import Form from 'components/form';
import ProposalGroup from './proposal-group';
import TopFilter from 'components/top-filter';
import TabList from 'components/tabs/tab-list';
import Link from 'components/link';
import Heading from 'components/heading';
import Spinner from 'components/spinner';
import useFetchFilteredResults from 'js/hooks/use-fetch-filtered-results';

const ProposalListPage = ({
  emptyList,
  filterLayout,
  form,
  groups,
  title,
  sectionTitle,
  topFilter,
  fetchFilteredResultsEndpoint
}) => {
  const [
    isLoading,
    {
      emptyList: emptyListState,
      filterLayout: filterLayoutState,
      groups: groupsState
    },
    fetchResults
  ] = useFetchFilteredResults(
    { emptyList, filterLayout, groups },
    fetchFilteredResultsEndpoint,
    form.endpoint,
    topFilter.timeframe
  );

  return (
    <Form className="proposal-list-page" showSubmitButton={false} {...form}>
      <input
        type="hidden"
        id={topFilter.timeframe.id}
        name={topFilter.timeframe.name}
        value={topFilter.timeframe.value}
      />
      <ContentWithSidebar>{title && <h1>{title}</h1>}</ContentWithSidebar>
      <FilterLayout
        className="proposal-list-page__hide-underline-overflow"
        {...filterLayoutState}
        includeBottomBorder={false}
        fetchFilteredResults={fetchResults}
        topContent={
          topFilter ? (
            <TabList className="proposal-list-page__tab-list">
              {topFilter.options.map(({ isCurrent, link }, index) => (
                <TabList.Item
                  key={link.url}
                  isCurrent={isCurrent}
                  length={topFilter.options.length}
                  index={index}
                  currentIndex={topFilter.options.findIndex(
                    ({ isCurrent }) => isCurrent
                  )}
                >
                  <Link theme={isCurrent ? Link.themes.black : []} {...link} />
                </TabList.Item>
              ))}
            </TabList>
          ) : null
        }
        isLoading={isLoading}
      >
        <React.Fragment>
          {sectionTitle && <Heading>{sectionTitle}</Heading>}
          {groupsState && groupsState.length > 0 ? (
            groupsState.map((group, index) => (
              <ProposalGroup key={group.title + String(index)} {...group} />
            ))
          ) : (
            <EmptyList {...emptyListState} />
          )}
          <Spinner isActive={isLoading} />
        </React.Fragment>
      </FilterLayout>
    </Form>
  );
};

ProposalListPage.propTypes = {
  emptyList: PropTypes.exact(EmptyList.propTypes),
  filterLayout: PropTypes.exact(FilterLayout.propTypes),
  form: PropTypes.exact(Form.propTypes),
  groups: PropTypes.arrayOf(PropTypes.exact(ProposalGroup.propTypes)),
  title: PropTypes.string,
  topFilter: PropTypes.exact(TopFilter.propTypes),
  sectionTitle: PropTypes.string,
  fetchFilteredResultsEndpoint: PropTypes.string
};

ProposalListPage.defaultProps = {
  groups: []
};

export default ProposalListPage;
