import React from 'react';
import PropTypes from 'prop-types';

import ArticleHeader from 'components/article-header';
import ContentWithSidebar from 'components/content-with-sidebar';
import PageNavigation from 'components/page-navigation';
import PageFooter from 'components/page-footer';
import TableOfContents from 'components/table-of-contents';
import RichText from 'components/rich-text';

const LargeDocumentsPage = ({
  content,
  footer,
  header,
  ingress,
  pageNavigation,
  tableOfContents,
  title
}) => (
  <ContentWithSidebar
    theme={ContentWithSidebar.themes.sidebarLeft}
    className="large-documents-page"
  >
    <ContentWithSidebar.Content>
      {title && <h1>{title}</h1>}
      <div className="large-documents-page--ingress">
        <p>{ingress}</p>
      </div>
      <ArticleHeader {...header} />
      <RichText {...content} />
      {pageNavigation && <PageNavigation {...pageNavigation} />}
      <div className="large-documents-page--footer">
        <PageFooter {...footer} />
      </div>
    </ContentWithSidebar.Content>
    <ContentWithSidebar.Sidebar>
      <TableOfContents {...tableOfContents} />
    </ContentWithSidebar.Sidebar>
  </ContentWithSidebar>
);

LargeDocumentsPage.propTypes = {
  content: PropTypes.exact(RichText.propTypes),
  footer: PropTypes.exact(PageFooter.propTypes),
  header: PropTypes.exact(ArticleHeader.propTypes),
  ingress: PropTypes.string,
  pageNavigation: PropTypes.exact(PageNavigation.propTypes),
  tableOfContents: PropTypes.exact(TableOfContents.propTypes),
  title: PropTypes.string
};

export default LargeDocumentsPage;
