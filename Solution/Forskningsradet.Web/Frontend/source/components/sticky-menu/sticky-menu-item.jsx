import React from 'react';
import PropTypes from 'prop-types';

import cn from 'classnames';
import Accordion from 'components/accordion';

import Link from 'components/link';
import StickyMenuItemSublink from './sticky-menu-item-sublink';

const StickyMenuItem = ({ link, subLinks }) => {
  if (!subLinks)
    return (
      <li>
        <Link {...link} />
      </li>
    );
  else
    return (
      <Accordion {...subLinks.accordion}>
        {({ Button: AccordionButton, Collapse, isOpen }) => {
          return (
            <React.Fragment>
              <li className="sticky-menu-item__link-with-sublinks">
                <Link {...link} />

                <div>
                  <AccordionButton
                    icon="small-arrow-down"
                    className={cn('sticky-menu-item--toggle-sub-links', {
                      '-is-active': isOpen
                    })}
                  />
                </div>
              </li>

              <Collapse>
                <ul>
                  {subLinks.links &&
                    subLinks.links.items.map(item => (
                      <StickyMenuItemSublink
                        key={item.url}
                        {...item}
                        isAccordionOpen={isOpen}
                      />
                    ))}
                </ul>
              </Collapse>
            </React.Fragment>
          );
        }}
      </Accordion>
    );
};

StickyMenuItem.propTypes = {
  link: PropTypes.shape({
    text: PropTypes.string,
    url: PropTypes.string
  }),
  subLinks: PropTypes.shape({
    accordion: PropTypes.exact(Accordion.propTypes),
    links: PropTypes.exact({
      items: PropTypes.arrayOf(PropTypes.exact(StickyMenuItemSublink.propTypes))
    })
  })
};

export default StickyMenuItem;
