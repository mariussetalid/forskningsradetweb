import React from 'react';

import Link from 'components/link';
import exposedValues from 'js/utils/exposed-values';

const stickyMenuItemSublink = ({ text, url }) => {
  const anchorId = url.slice(1);

  const openAccordion = e => {
    const linkTarget = e.target.href;
    const open = exposedValues.getValue(anchorId);
    if (open) open();
    //Wait for accordion opening animation
    setTimeout(() => {
      window.location.href = linkTarget;
    }, 700);
  };

  return (
    <li>
      <span>- </span>
      <Link text={text} url={url} onClick={openAccordion}></Link>
    </li>
  );
};

stickyMenuItemSublink.propTypes = Link.propTypes;

export default stickyMenuItemSublink;
