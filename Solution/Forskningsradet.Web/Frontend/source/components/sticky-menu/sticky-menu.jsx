import React from 'react';
import PropTypes from 'prop-types';

// Hacky loading because stickyfilljs directly references 'window', which crashes server side rendering.
import isRunningOnClient from '@creuna/utils/is-running-on-client';
const stickyPolyfill = isRunningOnClient ? require('stickyfilljs') : {};
import Accordion from 'components/accordion';
import Icon from 'components/icon';
import StickyMenuItem from './sticky-menu-item';
import Link from 'components/link';

const StickyMenu = ({ accordion, navGroups, title }) => {
  const element = React.useRef();

  React.useEffect(() => {
    stickyPolyfill.addOne(element.current);
  }, []);

  return (
    <div className="sticky-menu">
      <Accordion
        expandOnMount={windowWidth => windowWidth >= 1024}
        {...accordion}
      >
        {({ Button: AccordionButton, Collapse, isOpen, toggle }) => (
          <div className="sticky-menu--content" ref={element}>
            <div className="sticky-menu--header">
              {title && <h3 onClick={toggle}>{title}</h3>}
              <AccordionButton className="sticky-menu--toggle">
                <Icon
                  className="sticky-menu--toggle-icon"
                  name={isOpen ? 'x-circle' : 'plus-circle'}
                  fill
                />
              </AccordionButton>
            </div>

            <Collapse>
              <nav>
                {navGroups.map(({ titleLink = {}, links }) => (
                  <React.Fragment key={title}>
                    {titleLink.text && (
                      <Link
                        className="sticky-menu--title-link"
                        {...titleLink}
                      />
                    )}
                    <ul>
                      {!!links &&
                        links.items.map(item => (
                          <StickyMenuItem {...item} key={item.link.url} />
                        ))}
                    </ul>
                  </React.Fragment>
                ))}
              </nav>
            </Collapse>
          </div>
        )}
      </Accordion>
    </div>
  );
};

StickyMenu.propTypes = {
  accordion: PropTypes.exact(Accordion.propTypes),
  navGroups: PropTypes.arrayOf(
    PropTypes.exact({
      titleLink: PropTypes.exact(Link.propTypes),
      links: PropTypes.shape({
        items: PropTypes.arrayOf(PropTypes.exact(StickyMenuItem.propTypes))
          .isRequired
      })
    })
  ),
  title: PropTypes.string
};

StickyMenu.defaultProps = {
  navGroups: []
};

export default StickyMenu;
