/* eslint-disable no-console */
const shouldLog = process.env.NODE_ENV === 'development';
const noop = () => {};

export const log = shouldLog ? console.log : noop;
export const warn = shouldLog ? console.warn : noop;
export const error = shouldLog ? console.error : noop;
