const doesContain = (arr, item) =>
  Array.isArray(arr) ? arr.indexOf(item) > -1 : arr === item;

export default doesContain;
