export default () =>
  typeof process !== 'undefined' &&
  process.env &&
  process.env.NODE_ENV === 'development';
