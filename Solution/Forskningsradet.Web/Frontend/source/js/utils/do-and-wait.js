const doAndWait = (func, delay) => {
  func();
  return new Promise(res => setTimeout(() => res(), delay));
};

export default doAndWait;
