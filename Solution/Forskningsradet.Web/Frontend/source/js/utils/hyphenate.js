export default text => ({
  dangerouslySetInnerHTML: { __html: text.replace(/_/g, '&shy;') }
});
