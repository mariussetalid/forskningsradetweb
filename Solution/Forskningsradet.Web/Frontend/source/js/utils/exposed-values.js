//A module to expose value from one component to another outside of react.

const exposedValues = (function () {
  const values = {};

  return {
    addValue: (key, value) => {
      if (key && !values[key]) values[key] = value;
    },
    getValue: key => {
      return values[key];
    }
  };
})();

export default exposedValues;
