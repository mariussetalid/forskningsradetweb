/* eslint-disable no-console */

// NOTE: This file runs 'axe', a web accessibility checker. Errors are reported in the browser console.

// NOTE: axe adds itself to window
import 'axe-core/axe.min.js';
const { axe } = window;

// NOTE: See here for more options: https://github.com/dequelabs/axe-core/blob/develop/doc/API.md#options-parameter
const options = {
  runOnly: ['wcag2a', 'wcag2aa'],
  restoreScroll: true
};

const formatSummary = text =>
  text
    .split('\n')
    .slice(1) // Remove first line (because it just says 'fix this')
    .map(line => line.replace(/^ +/, '')) // Remove leading spaces
    .join('\n');

const formatMessage = (ruleId, summary) =>
  `WCAG ERROR: ${ruleId}\n\n${formatSummary(summary)}\n\n`;

axe.run(options).then(({ violations }) => {
  violations.forEach(({ id, nodes }) => {
    nodes.forEach(({ failureSummary, target }) => {
      target.forEach(selector => {
        console.error(
          formatMessage(id, failureSummary),
          document.querySelector(selector)
        );
      });
    });
  });
});
