/* HOC for separating logic for IE.
   Passes function to the Component argument used for executing a function only for IE browser
*/
import React, { useState, useEffect } from 'react';

const IEWrapper = Comp => props => {
  const [isDOMLoaded, setIsDOMLoaded] = useState(false);

  useEffect(() => setIsDOMLoaded(true), []);

  const IELogic = (func, ...args) => {
    const isIEBrowser = window.document.documentMode;
    if (isIEBrowser) return func(...args);
    return func().map(() => undefined);
  };

  return isDOMLoaded ? <Comp IELogic={IELogic} {...props} /> : null;
};

export default IEWrapper;
