import isRunningOnClient from '@creuna/utils/is-running-on-client';

const transformBase = '?transform=DownFit&width=';
const breakpointResolution = 100;

function getNewSrc(originalSrc, imageNodeWidth, currentImageWidth = 0) {
  const physicalPixelWidth = isRunningOnClient
    ? imageNodeWidth * window.devicePixelRatio
    : imageNodeWidth;

  // Return image resized up to closest 100px relative to imageNodeWidth
  const adjustedWidth =
    Math.ceil(physicalPixelWidth / breakpointResolution) * breakpointResolution;

  return (
    originalSrc + transformBase + Math.max(adjustedWidth, currentImageWidth)
  );
}

export default getNewSrc;
