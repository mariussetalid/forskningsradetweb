import { useState } from 'react';
import api from 'js/api-helper';
import { error } from 'js/log';
import toQueryString from 'js/utils/to-query-string';

const useFetchFilteredResults = (
  initialData,
  endpoint,
  currentPage,
  urlQuery
) => {
  const [isLoading, setIsLoading] = useState(false);
  const [data, setData] = useState(initialData);

  const fetchFilteredResults = searchQueryParameter => {
    setIsLoading(true);
    const parameterString = toQueryString(searchQueryParameter);
    if (urlQuery) {
      const queryObject = { [urlQuery.name]: urlQuery.value };
      searchQueryParameter = Object.assign(queryObject, searchQueryParameter);
    }

    const urlQueryString = urlQuery
      ? `?${urlQuery.name}=${urlQuery.value}`
      : '?';

    const urlEndpoint = currentPage
      .concat(urlQueryString)
      .concat(parameterString);

    api
      .execute(endpoint, searchQueryParameter, 'post')
      .then(response => {
        setData(response);
        window.history.pushState('', '', urlEndpoint);
        setIsLoading(false);
      })
      .catch(e => error('Got error when requesting data: ', e));
  };
  return [isLoading, data, fetchFilteredResults];
};

export default useFetchFilteredResults;
