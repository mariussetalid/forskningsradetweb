import { useState } from 'react';
import resizeHook from 'js/hooks/resize';

const screens = {
  md: '(min-width: 768px)',
  lg: '(min-width: 1024px)'
};

const useIsScreenSize = (screenSize = 'lg') => {
  const [isScreenSize, setIsScreenSize] = useState(true);

  const screen = screens[screenSize];

  const onResize = () => {
    setIsScreenSize(window.matchMedia(screen).matches);
  };

  resizeHook(onResize);

  return isScreenSize;
};

export default useIsScreenSize;
