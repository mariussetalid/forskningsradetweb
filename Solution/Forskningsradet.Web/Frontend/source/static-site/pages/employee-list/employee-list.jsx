/*
name: Ansattliste
group: Informasjon
*/
import React from 'react';
import Layout from '../../layout';

import content from './employee-list.js';

import GroupedSearchPage from 'components/grouped-search-page';

const EmployeeList = () => (
  <Layout>
    <GroupedSearchPage {...content} />
  </Layout>
);

export default EmployeeList;
