import utils from '../../content-area-utils';

export default {
  errorCode: '404',
  linkUrl: '/frontpage',
  text: utils.RichText(
    "<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut <a href='#'>labore</a> et dolore.</p>"
  )
};
