/*
name: Feilside - 404
*/
import React from 'react';
import Layout from '../../layout';

import content from './error-404.js';

import ErrorPage from 'components/error-page';

const Error404 = () => (
  <Layout showHeader={false} showBreadcrumbs={false}>
    <ErrorPage {...content} />
  </Layout>
);

export default Error404;
