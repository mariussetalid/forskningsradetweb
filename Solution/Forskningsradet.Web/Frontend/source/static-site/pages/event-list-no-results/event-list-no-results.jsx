/*
name: Tom listeside
group: Arrangementer
*/
import React from 'react';
import Layout from '../../layout';

import content from './event-list-no-results.js';

import EventListPage from 'components/event-list-page';

const EventListNoResults = () => (
  <Layout>
    <EventListPage {...content} />
  </Layout>
);

export default EventListNoResults;
