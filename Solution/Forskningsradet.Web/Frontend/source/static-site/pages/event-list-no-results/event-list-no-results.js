import content from '../event-list/event-list.json';

export default Object.assign({}, content, {
  events: []
});
