/*
name: Knapper
*/

import React from 'react';
import Layout from '../../layout';

import Button from 'components/button';
import ContentContainer from 'components/content-container';
import Link from 'components/link';

const t = Button.themes;

const darkBlue = '#1c445a';

/* eslint-disable react/prop-types */
const DarkWrapper = ({ children }) => (
  <span
    style={{ display: 'inline-block', padding: 20, backgroundColor: darkBlue }}
  >
    {children}
  </span>
);

/* eslint-disable react/no-multi-comp */
const MockLink = props => <Link useButtonStyles={true} url="/" {...props} />;

/* eslint-disable react/no-multi-comp */
const Buttons = () => (
  <Layout>
    <ContentContainer theme={ContentContainer.themes.wide}>
      <style>{`hr { margin: 20px 0; }`}</style>

      <h2>Med button-element</h2>
      <hr />
      <Button>Ingen tema</Button>
      <Button theme={t.big}>Big</Button>
      <Button theme={t.link}>Link</Button>
      <Button theme={t.small}>Small</Button>
      <Button theme={t.uppercase}>Uppercase</Button>
      <DarkWrapper>
        <Button theme={t.white}>White</Button>
      </DarkWrapper>

      <hr />
      <div>
        <Button theme={t.fill}>Fill</Button>
        <Button theme={[t.fill, t.big]}>Fill + Big</Button>
        <Button theme={[t.fill, t.small]}>Fill + Small</Button>
        <Button theme={[t.fill, t.uppercase]}>Fill + Uppercase</Button>
        <Button disabled theme={t.fill}>
          Fill disabled
        </Button>
      </div>

      <hr />
      <div>
        <Button theme={t.outline}>Outline</Button>
        <Button theme={[t.outline, t.big]}>Outline + Big</Button>
        <Button theme={[t.outline, t.small]}>Outline + Small</Button>
        <Button theme={[t.outline, t.uppercase]}>Outline + Uppercase</Button>
        <Button disabled theme={t.outline}>
          Outline disabled
        </Button>
        <div>
          <DarkWrapper>
            <Button theme={[t.outline, t.white]}>Outline + White</Button>
          </DarkWrapper>
          <DarkWrapper>
            <Button disabled theme={[t.outline, t.white]}>
              Outline + White, disabled
            </Button>
          </DarkWrapper>
        </div>
      </div>

      <hr />
      <div>
        <Button theme={t.orangeOutline}>OrangeOutline</Button>
        <Button theme={[t.orangeOutline, t.big]}>OrangeOutline + Big</Button>
        <Button theme={[t.orangeOutline, t.small]}>
          OrangeOutline + Small
        </Button>
        <Button theme={[t.orangeOutline, t.uppercase]}>
          OrangeOutline + Uppercase
        </Button>
        <Button disabled theme={t.orangeOutline}>
          OrangeOutline disabled
        </Button>
        <div>
          <DarkWrapper>
            <Button theme={[t.orangeOutline, t.white]}>
              OrangeOutline + White
            </Button>
          </DarkWrapper>
          <DarkWrapper>
            <Button disabled theme={[t.orangeOutline, t.white]}>
              OrangeOutline + White, disabled
            </Button>
          </DarkWrapper>
        </div>
      </div>

      <h2 style={{ marginTop: 80 }}>Med a-element</h2>

      <hr />
      <div>
        <MockLink theme={t.fill} text="Fill" />
        <MockLink theme={[t.fill, t.big]} text="Fill + Big" />
        <MockLink theme={[t.fill, t.small]} text="Fill + Small" />
        <MockLink theme={[t.fill, t.uppercase]} text="Fill + Uppercase" />
        <MockLink theme={t.fill} text="Fill uten URL" url={undefined} />
      </div>

      <hr />
      <div>
        <MockLink theme={t.outline} text="Outline" />
        <MockLink theme={[t.outline, t.big]} text="Outline + Big" />
        <MockLink theme={[t.outline, t.small]} text="Outline + Small" />
        <MockLink theme={[t.outline, t.uppercase]} text="Outline + Uppercase" />
        <MockLink theme={t.outline} text="Outline uten URL" url={undefined} />
        <div>
          <DarkWrapper>
            <MockLink theme={[t.outline, t.white]} text="Outline + White" />
          </DarkWrapper>
          <DarkWrapper>
            <MockLink
              disabled
              theme={[t.outline, t.white]}
              text="Outline + White, uten URL"
              url={undefined}
            />
          </DarkWrapper>
        </div>
      </div>

      <hr />
      <div>
        <MockLink theme={t.orangeOutline} text="OrangeOutline" />
        <MockLink theme={[t.orangeOutline, t.big]} text="OrangeOutline + Big" />
        <MockLink
          theme={[t.orangeOutline, t.small]}
          text="OrangeOutline + Small"
        />
        <MockLink
          theme={[t.orangeOutline, t.uppercase]}
          text="OrangeOutline + Uppercase"
        />
        <MockLink
          theme={t.orangeOutline}
          text="OrangeOutline uten URL"
          url={undefined}
        />
        <div>
          <DarkWrapper>
            <MockLink
              theme={[t.orangeOutline, t.white]}
              text="OrangeOutline + White"
            />
          </DarkWrapper>
          <DarkWrapper>
            <MockLink
              theme={[t.orangeOutline, t.white]}
              text="OrangeOutline + White, uten URL"
              url={undefined}
            />
          </DarkWrapper>
        </div>
      </div>
    </ContentContainer>
  </Layout>
);

export default Buttons;
