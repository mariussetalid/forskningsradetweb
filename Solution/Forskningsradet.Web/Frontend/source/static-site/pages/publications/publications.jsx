/*
group: CMS-maler
name: Publikasjoner
*/

import React from 'react';
import Layout from '../../layout';

import PublicationsPage from 'components/publications-page';

import content from './publications.js';

const Publications = () => (
  <Layout>
    <PublicationsPage {...content} />
  </Layout>
);

export default Publications;
