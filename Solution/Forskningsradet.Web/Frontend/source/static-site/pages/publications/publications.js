import utils from '../../content-area-utils';

export default {
  title: 'Publikasjoner',
  form: { endpoint: '/publications' },
  resultsDescription: 'Viser 1-10 av 340 treff',
  fetchFilteredResultsEndpoint:
    '/static-site/api/filtered-publications-result.json',
  search: {
    input: {
      label: 'Søk',
      name: 'search',
      placeholder: 'Søk'
    },
    submit: {
      text: 'Søk'
    },
    resultsDescription: 'Du har ikke søkt på publikasjoner'
  },
  emptyList: { text: 'Fant ingen treff. Prøv et annet søkeord.' },
  results: [
    {
      htmlId: 'year-2018',
      title: '2018',
      results: utils.ContentArea(
        utils.SearchResult({
          title: 'Sluttrapport for Program for samisk forskning II 2007-2017',
          url: '/',
          metadata: {
            items: [
              { label: 'Type', text: 'Ikke kategorisert' },
              { label: 'Dokument', text: 'pdf (2.3 Mb)' },
              { label: 'Antall sider', text: '69' },
              { label: 'Forfatter', text: '-' },
              { label: 'Gjelder fra', text: '-' }
            ]
          }
        }),
        utils.SearchResult({
          title: 'Evalueringsrapport',
          url: '/?9',
          tags: { tags: [{ text: 'Fil' }] },
          text:
            'Delicatissimi omnium <mark>Evalueringsrapport</mark> mediocritatem duo eu. Ei simul perfecto utlysning tempor tamquam scaevola.',
          metadata: {
            items: [
              {
                icon: { iconTheme: 'pdf' },
                label: 'Dokument',
                text: 'PDF (539.048 kB)'
              },
              {
                label: 'Publikasjonsstype',
                text: 'Evalueringer'
              },
              {
                label: 'Utgivelsesdato',
                text: '18.12.2020'
              }
            ]
          }
        })
      )
    },
    {
      htmlId: 'year-2017',
      title: '2017',
      results: utils.ContentArea(
        utils.SearchResult({
          title:
            'Evaluation of the Social Sciences in Norway Report from Panel 6 – Economic Administrative Research Areas',
          url: '/',
          metadata: {
            items: [
              { label: 'Type', text: 'Rapport' },
              { label: 'Dokument', text: 'pdf (3.4 Mb)' },
              { label: 'Antall sider', text: '260' },
              { label: 'Forfatter', text: '-' },
              { label: 'Gjelder fra', text: '2018' }
            ]
          }
        }),
        utils.SearchResult({
          title: 'MARINFORSK – Marine resources and the Environment',
          url: '/',
          metadata: {
            items: [
              { label: 'Type', text: 'Work programme' },
              { label: 'Dokument', text: 'pdf (342 Kb)' },
              { label: 'Antall sider', text: '25' },
              { label: 'Forfatter', text: '-' },
              { label: 'Gjelder fra', text: '2018' }
            ]
          }
        })
      )
    }
  ],
  navigation: [
    {
      isCurrent: true,
      link: {
        text: '2018',
        url: '/2018'
      }
    },
    {
      isCurrent: false,
      link: {
        text: '2017',
        url: '/2017'
      }
    },
    {
      isCurrent: false,
      link: {
        text: '2016',
        url: '/2016'
      }
    },
    {
      isCurrent: false,
      link: {
        text: '2015',
        url: '/2015'
      }
    },
    {
      isCurrent: false,
      link: {
        text: '2014',
        url: '/2014'
      }
    },
    {
      isCurrent: false,
      link: {
        text: '2013',
        url: '/2013'
      }
    },
    {
      isCurrent: false,
      link: {
        text: '2012',
        url: '/2012'
      }
    },
    {
      isCurrent: false,
      link: {
        text: '2011',
        url: '/2011'
      }
    },
    {
      isCurrent: false,
      link: {
        text: '2010',
        url: '/2010'
      }
    },
    {
      isCurrent: false,
      link: {
        text: '2009 og eldre',
        url: '/2009'
      }
    }
  ],
  filterLayout: {
    labels: {
      toggleFilters: 'Filtrér'
    },
    filters: {
      items: [
        {
          title: 'Alle publikasjonstyper',
          selectedAll: true,
          accordion: {
            guid: '98vynvq09384tynmaspoifmvoaiugn',
            expandLabel: 'Vis',
            collapseLabel: 'Skjul'
          },
          options: [
            {
              name: 'type',
              label: 'Programplan (3)',
              value: '1',
              checked: true
            },
            {
              name: 'type',
              label: 'Work plan 2018-2021 (2)',
              value: '2',
              checked: true
            },
            {
              name: 'type',
              label: 'Forskningskartlegging (2)',
              value: '3',
              checked: true
            },
            {
              name: 'type',
              label: 'Rapport (2)',
              value: '4',
              checked: true
            }
          ]
        }
      ]
    },
    contentArea: utils.ContentArea(
      utils.InfoBlock({
        title: 'Mulighet for blokk her',
        text: utils.RichText(
          '<p>Lorem ipsum dolor sit amet, choro civibus eu sed, graeco evertitur sed at, invidunt voluptaria mea an. Set commodo vocibus at mea, augue inpercipit ne vis at erat soluta audiam sit:</p>'
        ),
        editorTheme: '-theme-blue'
      })
    )
  },
  pagination: {
    title: 'Navigasjon for søkeresultater',
    pages: [
      {
        isCurrent: false,
        label: 'Forrige side',
        link: {
          url: '/?0',
          text: 'Forrige'
        }
      },
      {
        isCurrent: false,
        label: 'Side 1',
        link: {
          url: '/?1',
          text: '1'
        }
      },
      {
        isCurrent: false,
        label: 'Side 2',
        link: {
          url: '/?2',
          text: '2'
        }
      },
      {
        isCurrent: true,
        label: 'Side 3',
        link: {
          url: '/?3',
          text: '3'
        }
      },
      {
        isCurrent: false,
        label: 'Side 4',
        link: {
          url: '/?4',
          text: '4'
        }
      },
      {
        isCurrent: false,
        label: 'Side 5',
        link: {
          url: '/?5',
          text: '5'
        }
      },
      {
        isCurrent: false,
        label: 'Neste side',
        link: {
          url: '/?6',
          text: 'Neste'
        }
      }
    ]
  }
};
