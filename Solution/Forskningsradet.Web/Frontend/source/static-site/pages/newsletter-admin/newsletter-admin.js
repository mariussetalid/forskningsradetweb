export default {
  layout: {
    header: {
      text: 'Forskningsrådet',
      url: '/'
    }
  },
  form: { endpoint: '/newsletter-admin' },
  title: 'Mitt nyhetsbrev',
  inputFieldsTitle: 'E-post varslinger sendes til',
  inputFieldsItems: [
    {
      label: 'E-postadresse*',
      name: 'e-post'
    }
  ],
  checkboxGroups: [
    {
      title: 'Velg oppdateringer på fag/tema',
      options: [
        {
          label: 'Arbeidsliv, velferd og integrering',
          name: 'arbeidsliv'
        },
        {
          label: 'Bioteknologi',
          name: 'bioteknologi'
        },
        {
          label: 'Globalisering og utvikling ',
          name: 'globalisering'
        },
        {
          label: 'Humaniora',
          name: 'humaniora'
        },
        {
          label: 'IKT',
          name: 'ikt'
        },
        {
          label: 'Klima og miljø',
          name: 'klima'
        },
        {
          label: 'Landbruk, fiskeri og havbruk',
          name: 'landbruk'
        },
        {
          label: 'Matematikk og naturvitenskap ',
          name: 'matematikk'
        },
        {
          label: 'Medisin og helse',
          name: 'medisin'
        },
        {
          label: 'Miljøteknologi',
          name: 'miljo'
        },
        {
          label: 'Nano- og materialteknologi',
          name: 'nano'
        },
        {
          label: 'Petroleum og energi',
          name: 'petroleum'
        },
        {
          label: 'Polar- og nordområdeforskning',
          name: 'polar'
        },
        {
          label: 'Samferdsel',
          name: 'samferdsel'
        },
        {
          label: 'Samfunnvitenskap',
          name: 'samfunnvitenskap'
        },
        {
          label: 'Tjenesteytende næringer',
          name: 'tjenesteytende'
        },
        {
          label: 'Utdanning',
          name: 'utdanning'
        },
        {
          label: 'Øvrige teknologier',
          name: 'ovrige'
        }
      ]
    },
    {
      title: 'Velg oppdateringer for',
      options: [
        {
          label: 'Søknadresultater',
          name: 'soknadsresultater'
        },
        {
          label: 'Ledige stillinger i Forskningsrådet',
          name: 'ledige-stillinger'
        }
      ]
    }
  ],
  submitButton: {
    text: 'Lagre endringer'
  },
  unsubscribeLink: {
    text: 'Meld av nyhetsbrev',
    url: '/'
  }
};
