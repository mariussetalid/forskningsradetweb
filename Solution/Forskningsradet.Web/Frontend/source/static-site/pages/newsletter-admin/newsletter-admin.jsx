/*
name: Administrere innstillinger
group: Nyhetsbrev
*/
import React from 'react';

import NewsletterAdminPage from 'components/newsletter-admin-page';

import content from './newsletter-admin.js';

const NewsletterAdmin = () => <NewsletterAdminPage {...content} />;

export default NewsletterAdmin;
