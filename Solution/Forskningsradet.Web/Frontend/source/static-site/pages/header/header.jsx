/*
name: Header
group: Globale komponenter
*/
import React from 'react';

import content from './header.json';

import Header from 'components/header';

const HeaderPage = () => (
  <React.Fragment>
    <Header {...content} />
    {/* To make wcag tests happy, #main needs to exist on the page */}
    <main id="main" />
  </React.Fragment>
);

export default HeaderPage;
