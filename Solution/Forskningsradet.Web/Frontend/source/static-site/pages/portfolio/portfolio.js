import utils from '../../content-area-utils';
export default {
  pageHeader: {
    title: 'Naturvitenskap og Teknologi',
    ingress:
      'Porteføljen for energi, transport og lavutslipp arbeider for at energi- og transportsektorene skal levere bærekraftige, smarte og sikre løsninger, teknologier og tjenester, at byer og byregioner er inkluderende, tilpasningsdyktige og attraktive å leve i og at vi som innbyggere og samfunnsaktører tar klima- og miljøvennlige valg som reduserer press på naturmangfoldet.',
    portfolioLinks: [
      {
        url: '/',
        text: 'Aktuelle utlysninger'
      },
      {
        url: '/',
        text: 'Aktuelle arrangementer'
      }
    ]
  },
  content: utils.ContentArea(
    utils.PortfolioContent({
      title: 'Hvem kan søke om støtte',
      content: utils.ContentArea(
        utils.LinkWithText({
          link: { text: 'Offsentlig///sektor', url: '#' },
          text:
            'Fornyelse og innovasjon i offentlig sektor gkær Norge mer effektivt og bedre rustet til å takle store samfunnsutfordringer.'
        }),
        utils.LinkWithText({
          link: { text: 'Næringsliv', url: '#' },
          text:
            'Innovasjon og kunnskapsbyggning i nøringslivet gjør Norges verdiskaping mer bærekraftig i framtiden.'
        }),
        utils.LinkWithText({
          link: { text: 'Forskningsinstituskjoner', url: '#' },
          text:
            'Økonomisk støtte til forskningsprosjekter hever nivået på rapportene og bidrar til verdifull kunnskap som kan deles med flere.'
        })
      )
    }),
    utils.PortfolioContent({
      title: 'Vi støtter forskning innen',
      content: utils.ContentArea(
        utils.LinkLists({
          nestedLinks: [
            {
              items: [
                {
                  text: 'Link somewhere really special',
                  url: '/en/'
                },
                {
                  text: 'Link somewhere Link somewhere Link somewhere',
                  url: '/en/'
                },
                {
                  text: 'Link somewhere',
                  url: '/en/'
                },
                {
                  text: 'Link somewhere',
                  url: '/en/'
                },
                {
                  text: 'Link somewhere',
                  url: '/en/'
                },
                {
                  text: 'Link/// somewhere',
                  url: '/en/'
                },
                {
                  text: 'Link somewhere',
                  url: '/en/'
                },
                {
                  text: 'Link somewhere',
                  url: '/en/'
                },
                {
                  text: 'Link somewhere',
                  url: '/en/'
                },
                {
                  text: 'Link somewhere',
                  url: '/en/'
                }
              ]
            } /*,
            {
              title: 'Global Link List Block',
              items: [
                {
                  text: 'Somewhere decisive',
                  url: '/en/'
                },
                {
                  text: 'Oddly placed',
                  url: '/en/neweventlistpage/'
                },
                {
                  text: 'Continuous feriocity',
                  url: '/en/framtidigevent/'
                },
                {
                  text: 'Tender/// niblings/// placed',
                  url: '/en/newspage/'
                },
                {
                  text: 'Somewhere',
                  url: 'mailto:Some@where.com'
                }
              ]
            },
            {
              title: 'Global Link List Block',
              items: [
                {
                  text: 'Somewhere decisive',
                  url: '/en/'
                },
                {
                  text: 'Oddly placed',
                  url: '/en/neweventlistpage/'
                },
                {
                  text: 'Continuous feriocity',
                  url: '/en/framtidigevent/'
                },
                {
                  text: 'Tender niblings',
                  url: '/en/newspage/'
                },
                {
                  text: 'Somewhere',
                  url: 'mailto:Some@where.com'
                }
              ]
            }*/
          ]
        })
      )
    }),
    utils.PortfolioContent({
      title: 'Vi støtter forskning innen',
      content: utils.ContentArea(
        utils.LinkLists({
          nestedLinks: [
            {
              items: [
                {
                  text: 'Link somewhere really special',
                  url: '/en/'
                },
                {
                  text: 'Link somewhere Link somewhere Link somewhere',
                  url: '/en/'
                },
                {
                  text: 'Link somewhere',
                  url: '/en/'
                },
                {
                  text: 'Link somewhere',
                  url: '/en/'
                },
                {
                  text: 'Link somewhere',
                  url: '/en/'
                },
                {
                  text: 'Link/// somewhere',
                  url: '/en/'
                },
                {
                  text: 'Link somewhere',
                  url: '/en/'
                },
                {
                  text: 'Link somewhere',
                  url: '/en/'
                },
                {
                  text: 'Link somewhere',
                  url: '/en/'
                },
                {
                  text: 'Link somewhere',
                  url: '/en/'
                }
              ]
            },
            {
              title: 'Global Link List Block',
              items: [
                {
                  text: 'Somewhere decisive',
                  url: '/en/'
                },
                {
                  text: 'Oddly placed',
                  url: '/en/neweventlistpage/'
                },
                {
                  text: 'Continuous feriocity',
                  url: '/en/framtidigevent/'
                },
                {
                  text: 'Tender/// niblings/// placed',
                  url: '/en/newspage/'
                },
                {
                  text: 'Somewhere',
                  url: 'mailto:Some@where.com'
                }
              ]
            },
            {
              title: 'Global Link List Block',
              items: [
                {
                  text: 'Somewhere decisive',
                  url: '/en/'
                },
                {
                  text: 'Oddly placed',
                  url: '/en/neweventlistpage/'
                },
                {
                  text: 'Continuous feriocity',
                  url: '/en/framtidigevent/'
                },
                {
                  text: 'Tender niblings',
                  url: '/en/newspage/'
                },
                {
                  text: 'Somewhere',
                  url: 'mailto:Some@where.com'
                }
              ]
            }
          ]
        })
      )
    }),
    utils.PortfolioContent({
      title: 'Aktuelle arrangementer og webinarer',
      content: utils.ContentArea(
        utils.RelatedDates({
          relatedContent: {
            link: { text: 'Alle arrangementer', url: '#' },
            border: ''
          },
          isGrid: true,
          dates: [
            {
              id: 'asdasda1',
              title:
                'Hvordan søke ERC Advanced Grants eller andre søkbare ting som man kan søke på.',
              url: '/event',
              dateContainer: {
                eventImage: {
                  background: 'background1'
                },
                dates: [{ day: '24.', month: 'apr' }],
                isPastDate: false
              },
              metadata: {
                items: [
                  { label: 'Arrangementstype', text: 'Digitalt arrengement' },
                  { label: 'Sted', text: 'Trondheim' },
                  { label: 'Gøy', text: 'Passe' },
                  { label: 'Dyrt', text: 'Jepp' }
                ]
              },
              media: {
                items: [{ icon: 'camera' }]
              }
            },
            {
              id: '2123',
              title: 'Drift og økonomi i et EU-prosjekt',
              dateContainer: {
                eventImage: {
                  image: {
                    src: '/static-site/assets/students.png',
                    alt: 'hei'
                  }
                },
                dates: [
                  { day: '04.', month: 'apr', year: '2020' },
                  { day: '09.', month: 'apr', year: '2020' }
                ],
                labels: { pastDate: 'Gjennomført' },
                isPastDate: false
              },
              url: '/event',
              metadata: {
                items: [
                  { label: 'Varighet', text: '2 dager' },
                  { label: 'Sted', text: 'Trondheim' }
                ]
              },
              media: {
                items: [{ icon: 'camera' }]
              }
            },
            {
              id: '3asd',
              title: 'Hvordan søke BLT Advanced Grants?',
              url: '/event',
              dateContainer: {
                dates: [
                  { day: '31.', month: 'apr', year: '2020' },
                  { day: '2.', month: 'mai', year: '2021' }
                ],
                eventImage: {
                  image: {
                    src: '/static-site/assets/students.png',
                    alt: 'hei'
                  }
                }
              },
              metadata: {
                items: [
                  { label: 'Varighet', text: '2 dager' },
                  { label: 'Sted', text: 'Trondheim' }
                ]
              },
              media: {
                items: [{ icon: 'camera' }]
              }
            },
            {
              id: 'asda2f3sda1',
              title: 'Hvordan søke KRS Grants?',
              url: '/event',
              dateContainer: {
                eventImage: {
                  background: 'background3'
                },
                dates: [
                  { day: '31.', month: 'apr', year: '2020' },
                  { day: '2.', month: 'mai', year: '2020' }
                ],
                isPastDate: false
              },
              metadata: {
                items: [
                  { label: 'Varighet', text: '2 dager' },
                  { label: 'Sted', text: 'Trondheim' }
                ]
              }
            },
            {
              id: '2121323',
              title: 'Drift og økonomi i et prosjekt',
              url: '/event',

              dateContainer: {
                dates: [
                  { day: '2.', month: 'apr', year: '2020' },
                  { day: '22.', month: 'apr', year: '2021' }
                ],
                eventImage: {
                  background: 'background2'
                },
                labels: { pastDate: 'Gjennomført' }
              },
              metadata: {
                items: [
                  { label: 'Varighet', text: '2 dager' },
                  { label: 'Sted', text: 'Trondheim' }
                ]
              },
              media: {
                items: [{ icon: 'camera' }]
              }
            },
            {
              id: '3aadsf3sd',
              title: 'Hvordan søke ABC Advanced Grants?',
              url: '/event',
              dateContainer: {
                dates: [
                  { day: '31', month: 'apr' },
                  { day: '2', month: 'mai' }
                ],
                eventImage: {
                  background: 'background2'
                }
              },
              metadata: {
                items: [
                  { label: 'Varighet', text: '2 dager' },
                  { label: 'Sted', text: 'Trondheim' }
                ]
              },
              media: {
                items: [{ icon: 'camera' }]
              }
            }
          ]
        })
      )
    }),
    utils.PortfolioContent({
      title: 'Aktuelle arrangementer og webinarer',
      content: utils.ContentArea(
        utils.RelatedArticles({
          articles: [
            {
              title: {
                text:
                  'Idékraft verden trenger: Strategi for Norges forskningsråds 2020-2024',
                url: '/'
              },
              metadata: {
                items: [
                  { label: 'Type', text: 'Andre kategorieer' },
                  { label: 'Dokument', text: 'pdf (3.4 Mb)' }
                ]
              },
              eventImage: {
                background: 'background1'
              },
              icon: { iconTheme: 'pdf' }
            },
            {
              title: {
                text: 'Evaluation of the Norwegian Centres fo Excellence (SFF)',
                url: '/'
              },
              image: { src: '/static-site/assets/students.png', alt: 'hei' },
              metadata: {
                items: [{ label: 'Type', text: 'Evaluering' }]
              }
            },
            {
              title: {
                text: 'Forskningsrådets policy for åpen forskning',
                url: '/'
              },
              metadata: {
                items: [
                  { label: 'Type', text: 'Rapport' },
                  { label: 'Dokument', text: 'pdf (3.4 Mb)' }
                ]
              }
            }
          ],
          relatedContent: {
            link: { text: 'Alle aktuelle saker', url: '#' },
            border: ''
          },
          isGrid: true,
          isPublication: true
        })
      )
    }),
    utils.PortfolioContent({
      title: 'Aktuelt for klima og polar',
      content: utils.ContentArea(
        utils.RelatedArticles({
          articles: [
            {
              title: { text: 'Nyhetssak med', url: '#' },
              image: { src: '/static-site/assets/smartphone.png', alt: 'hei' },
              published: { type: 'Publisert', date: '21. september 2019' }
            },
            {
              title: {
                text:
                  'Nyhetssak med lengre tittel – kun én dato synes i byline, enten publisert eller sist oppdatert',
                url: '#'
              },
              image: { src: '/static-site/assets/space.png', alt: 'hei' },
              published: { type: 'Publisert', date: '21. september 2019' }
            },
            {
              title: { text: 'Nyhetssak', url: '#' },
              image: { src: '/static-site/assets/students.png', alt: 'hei' },
              published: { type: 'Publisert', date: '21. september 2019' }
            },
            {
              title: {
                text:
                  'Nyhetssak med lengre tittel kun én dato synes i byline, enten publisert eller sist oppdatert',
                url: '#'
              },
              image: { src: '/static-site/assets/space.png', alt: 'hei' },
              published: { type: 'Publisert', date: '21. september 2019' }
            },
            {
              title: { text: 'Nyhetssak - enda en', url: '#' },
              image: { src: '/static-site/assets/students.png', alt: 'hei' },
              published: { type: 'Publisert', date: '21. september 2019' }
            },
            {
              title: {
                text:
                  'Nyhetssak med en tittel – kun én dato synes i byline, enten publisert eller sist oppdatert',
                url: '#'
              },
              image: { src: '/static-site/assets/space.png', alt: 'hei' },
              published: { type: 'Publisert', date: '21. september 2019' }
            },
            {
              title: { text: 'Nyhetssaken', url: '#' },
              image: { src: '/static-site/assets/students.png', alt: 'hei' },
              published: { type: 'Publisert', date: '21. september 2019' }
            },
            {
              title: {
                text:
                  'Nyhetssaket med lengre tittel – kun én dato synes i byline, enten publisert eller sist oppdatert',
                url: '#'
              },
              image: { src: '/static-site/assets/space.png', alt: 'hei' },
              published: { type: 'Publisert', date: '21. september 2019' }
            },
            {
              title: { text: 'Nyhetssakkan', url: '#' },
              image: { src: '/static-site/assets/students.png', alt: 'hei' },
              published: { type: 'Publisert', date: '21. september 2019' }
            }
          ],
          relatedContent: {
            link: { text: 'Alle aktuelle saker', url: '#' },
            border: ''
          },
          isGrid: true
        })
      )
    })
  )
};
