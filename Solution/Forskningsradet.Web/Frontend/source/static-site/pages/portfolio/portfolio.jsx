/*
group: CMS-maler
name: portefølje
path: portfolio
*/

import React from 'react';
import Layout from '../../layout';
import content from './portfolio.js';

import ContentAreaPage from 'components/content-area-page';

const Portfolio = () => (
  <Layout>
    <ContentAreaPage {...content} />
  </Layout>
);

export default Portfolio;
