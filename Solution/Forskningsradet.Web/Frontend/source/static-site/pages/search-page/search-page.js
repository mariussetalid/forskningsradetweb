export default {
  title: 'Søkeresultater',
  resultDescription: 'Viser 1–7 av 3991 treff',
  form: { endpoint: '/search-page' },
  search: {
    input: {
      label: 'Søk',
      name: 'search',
      placeholder: 'Søk',
      value: 'Utlysninger'
    },
    submit: {
      text: 'Søk'
    },
    externalResultsEndpoint: '/static-site/api/external-search-results.json',
    externalResultsLabel: 'treff i Prosjektbanken',
    resultsDescription:
      'Ditt søk på “Utlysninger” ga 3991 treff i forskningradet.no og '
  },
  emptyList: {
    text:
      'Det finnes ingen treff som oppfyller søkekriteriene dine. Prøv å endre søket eller kombinasjonen av filter.'
  },
  results: [
    {
      descriptiveTitle: 'Porteføljeplan for Demokrati, styring og fonyelse:',
      title: 'Forprosjekt – <mark>Utlysning</mark>',
      image: {
        alt: 'Glade gutter',
        src: '/static-site/assets/tto-lederne.png'
      },
      url: '/?1',
      text:
        'Discere iracundia contentiones cu mei. Ut tacimates democritum usu, ea vim spesifisert i <mark>utlysning</mark>. Omnium utamur mediocritatem duo eu, ei simul perfecto has. Tempor tamquam scaevola sit et, pri veri eruditi in.',
      metadata: {
        items: [
          {
            label: 'Søknadsfrist',
            text: '19. august 2018',
            url: '#test-metadata-url'
          },
          { label: 'Søknadsfrist', text: '20. august 2018' },
          { label: 'Innholdstype', text: 'Utlysning', isDisabled: true }
        ]
      },
      statusList: [{ theme: '-is-active', text: 'Søk nå' }]
    },
    {
      title: 'Forprosjekt – <mark>Utlysning</mark>',
      image: {
        alt: 'Glade gutter',
        src: '/static-site/assets/tto-lederne.png'
      },
      url: '/?1',
      text:
        'Discere iracundia contentiones cu mei. Ut tacimates democritum usu, ea vim spesifisert i <mark>utlysning</mark>. Omnium utamur mediocritatem duo eu, ei simul perfecto has. Tempor tamquam scaevola sit et, pri veri eruditi in.',
      metadata: {
        items: [
          { label: 'Innholdstype', text: 'Utlysning', isDisabled: true },
          { label: 'Søknadsfrist', text: '20. august 2018' }
        ]
      },
      statusList: [{ theme: '-is-active', text: 'Søk nå' }]
    },
    {
      title: 'Webinar om Horisont 2020 sine nye utlysninger om CO2 håndtering',
      url: '/?2',
      text:
        'Delicatissimi omnium <mark>utlysning</mark> mediocritatem duo eu. Ei simul perfecto utlysning tempor tamquam scaevola.',
      metadata: {
        items: [
          {
            label: 'Arrangementdato',
            text: '01.–02. september 2018',
            isDisabled: true
          }
        ]
      }
    },

    {
      title: 'Evalueringsrapport',
      url: '/?9',
      icon: { iconTheme: 'pdf' },
      text:
        'Delicatissimi omnium <mark>Evalueringsrapport</mark> mediocritatem duo eu. Ei simul perfecto utlysning tempor tamquam scaevola.',
      metadata: {
        items: [
          {
            label: 'Publikasjonsstype',
            text: 'Evalueringer'
          }
        ]
      }
    },
    {
      title: 'Lorem ipsum utlysning',
      url: '/?3',
      text:
        'Delicatissimi omnium <mark>utlysning</mark> mediocritatem duo eu. Ei simul perfecto utlysning tempor tamquam scaevola.',
      metadata: {
        items: [
          {
            label: 'Søknadsfrist',
            text: '01.–02. september 2018'
          }
        ]
      },
      statusList: [
        {
          theme: '-result-is-published',
          text: 'Se resultater'
        },
        {
          theme: '-is-active',
          text: 'Søk nå'
        }
      ],
      themeTags: {
        accordion: {
          guid: 'axldkvhmfvqoewy8tmvlskjdhfm',
          collapseLabel: 'Skjul',
          expandLabel: '+ 3 temaer'
        },
        items: [
          { text: 'Energi', url: '/?1' },
          { text: 'Klima/Miljøet', url: '/?2' },
          { text: 'Helse', url: '/?3' }
        ],
        numberOfVisibleItems: 4
      }
    },
    {
      title: 'Lorem ipsum',
      url: '/?3',
      text:
        'Delicatissimi omnium <mark>utlysning</mark> mediocritatem duo eu. Ei simul perfecto utlysning tempor tamquam scaevola.',
      metadata: {
        items: [
          {
            label: 'Søknadsfrist',
            text: '01.–03. september 2018'
          }
        ]
      },
      statusList: [
        {
          theme: '-is-completed',
          text: 'Gjennomført'
        }
      ],
      themeTags: {
        items: [
          { text: 'Energa' },
          { text: 'Klimaet/Miljø' },
          { text: 'Helse' },
          { text: 'Energi ' },
          { text: 'Klim/Miljø' },
          { text: 'Livet' },
          { text: 'Energiet' },
          { text: 'eha/Miljø' },
          { text: 'Helsa' },
          { text: 'Energø' },
          { text: 'luft/Miljø' },
          { text: 'Helset' }
        ]
      }
    },
    {
      title: 'Overskrift',
      url: '/?4',
      text:
        'Delicatissimi omnium <mark>utlysning</mark> mediocritatem duo eu. Ei simul perfecto utlysning tempor tamquam scaevola.',
      metadata: {
        items: [
          {
            label: 'Søknadsfrist',
            text: '01.–04. september 2018'
          }
        ]
      },

      statusList: [
        {
          theme: '-result-is-published',
          text: 'Se resultater'
        }
      ]
    },
    {
      title: 'Enda en utlysning',
      url: '/?5',
      text:
        'Delicatissimi omnium <mark>utlysning</mark> mediocritatem duo eu. Ei simul perfecto utlysning tempor tamquam scaevola.',
      metadata: {
        items: [
          {
            label: 'Søknadsfrist',
            text: '01.–05. september 2018'
          }
        ]
      },
      statusList: [
        {
          theme: '-is-canceled',
          text: 'Kansellert'
        }
      ]
    }
  ],
  filterLayout: {
    isLeft: false,
    filters: {
      mobileTitle: 'Filtre',
      title: 'Filtre',
      labels: {
        showResults: undefined,
        reset: 'Fjern alle filtre'
      },
      items: [
        {
          title: 'Fag/tema',
          accordion: {
            guid: 'p0213urc9a8sdfunpv9qwuner',
            expandLabel: 'Vis',
            collapseLabel: 'Skjul'
          },
          options: [
            {
              name: 'subjects',
              label: 'Arbeidsliv, velferd og integrering (3)',
              value: '1'
            },
            { name: 'subjects', label: 'Bioteknologi (2)', value: '2' },
            {
              name: 'subjects',
              label: 'Globalisering og utvikling (2)',
              value: '3'
            },
            { name: 'subjects', label: 'Humaniora (2)', value: '4' },
            { name: 'subjects', label: 'IKT (2)', value: '5' },
            { name: 'subjects', label: 'Klima og miljø (2)', value: '6' },
            {
              name: 'subjects',
              label: 'Landbruk, fiskeri og havbruk (2)',
              value: '7'
            },
            {
              name: 'subjects',
              label: 'Matematikk og naturvitenskap (2)',
              value: '8'
            },
            {
              name: 'subjects',
              label: 'Medisin og helse (2)',
              value: '9'
            },
            { name: 'subjects', label: 'Miljøteknologi (2)', value: '10' },
            {
              name: 'subjects',
              label: 'Nano- og materialteknologi (2)',
              value: '11'
            },
            {
              name: 'subjects',
              label: 'Petroleum og energi (3)',
              value: '12'
            },
            {
              name: 'subjects',
              label: 'Polar- og nordområdeforskning (3)',
              value: '13'
            },
            { name: 'subjects', label: 'Samferdsel (2)', value: '14' },
            {
              name: 'subjects',
              label: 'Samfunnvitenskap (3)',
              value: '15'
            },
            {
              name: 'subjects',
              label: 'Tjenesteytende næringer (2)',
              value: '16'
            },
            { name: 'subjects', label: 'Utdanning (2)', value: '17' },
            {
              name: 'subjects',
              label: 'Øvrige teknologier (32)',
              value: '18'
            }
          ]
        },
        {
          title: 'Type innhold',
          accordion: {
            guid: '98vynvq09384tynmaspoifmvoaiugn',
            expandLabel: 'Vis',
            collapseLabel: 'Skjul'
          },
          options: [
            {
              name: 'targetGroups',
              label: 'Arbeidsliv, velferd og integrering (3)',
              value: '1',
              checked: true
            },
            {
              name: 'targetGroups',
              label: 'Bioteknologi (2)',
              value: '2',
              checked: true
            },
            {
              name: 'targetGroups',
              label: 'Globalisering og utvikling (2)',
              value: '3',
              checked: true
            },
            {
              name: 'targetGroups',
              label: 'Humaniora (2)',
              value: '4',
              checked: true
            },
            {
              name: 'targetGroups',
              label: 'IKT (2)',
              value: '5',
              checked: true
            },
            {
              name: 'targetGroups',
              label: 'Klima og miljø (2)',
              value: '6',
              checked: true
            }
          ]
        }
      ]
    }
  },
  pagination: {
    title: 'Navigasjon for søkeresultater',
    pages: [
      {
        isCurrent: false,
        label: 'Første side',
        link: {
          url: '/?first',
          text: 'Første'
        }
      },
      {
        isCurrent: false,
        label: 'Forrige side',
        link: {
          url: '/?previous',
          text: 'Forrige'
        }
      },
      {
        isCurrent: false,
        label: 'Side 11',
        link: {
          url: '/?11',
          text: '11'
        }
      },
      {
        isCurrent: true,
        label: 'Side 12',
        link: {
          url: '/?12',
          text: '12'
        }
      },
      {
        isCurrent: false,
        label: 'Side 13',
        link: {
          url: '/?13',
          text: '13'
        }
      },
      {
        isCurrent: false,
        label: 'Side 14',
        link: {
          url: '/?14',
          text: '14'
        }
      },
      {
        isCurrent: false,
        label: 'Side 15',
        link: {
          url: '/?15',
          text: '15'
        }
      },
      {
        isCurrent: false,
        label: 'Side 16',
        link: {
          url: '/?16',
          text: '16'
        }
      },
      {
        isCurrent: false,
        label: 'Side 17',
        link: {
          url: '/?17',
          text: '17'
        }
      },
      {
        isCurrent: false,
        label: 'Side 18',
        link: {
          url: '/?18',
          text: '18'
        }
      },
      {
        isCurrent: false,
        label: 'Side 19',
        link: {
          url: '/?19',
          text: '19'
        }
      },
      {
        isCurrent: false,
        label: 'Side 20',
        link: {
          url: '/?20',
          text: '20'
        }
      },
      {
        isCurrent: false,
        label: 'Neste side',
        link: {
          url: '/?next',
          text: 'Neste'
        }
      },
      {
        isCurrent: false,
        label: 'Siste side',
        link: {
          url: '/?last',
          text: 'Siste'
        }
      }
    ]
  },
  fetchFilteredResultsEndpoint: '/static-site/api/filtered-search-result.json'
};
