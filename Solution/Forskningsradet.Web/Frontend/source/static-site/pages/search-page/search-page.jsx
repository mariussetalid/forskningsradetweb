/*
name: Med resultater
group: Søk
*/
import React from 'react';
import Layout from '../../layout';

import SearchPage from 'components/search-page';

import content from './search-page.js';

const Search = () => (
  <Layout>
    <SearchPage {...content} />
  </Layout>
);

export default Search;
