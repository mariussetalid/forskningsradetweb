import content from '../glossary/glossary.js';

export default Object.assign({}, content, {
  navigation: content.navigation.map(letter => ({
    isCurrent: false,
    link: { text: letter.link.text }
  })),
  resultsDescription: 'Viser 0 av 0 treff',
  results: [],
  pagination: {}
});
