/*
name: Ordliste uten resultater
group: Informasjon
*/
import React from 'react';
import Layout from '../../layout';

import content from './glossary-no-results';

import GroupedSearchPage from 'components/grouped-search-page';

const GlossaryNoResults = () => (
  <Layout>
    <GroupedSearchPage {...content} />
  </Layout>
);

export default GlossaryNoResults;
