/*
name: Artikkelside
group: CMS-maler
*/
import React from 'react';
import Layout from '../../layout';

import content from './article.js';

import ArticlePage from 'components/article-page';

const Article = () => (
  <Layout>
    <ArticlePage {...content} />
  </Layout>
);

export default Article;
