import utils from '../../content-area-utils';
import eventList from '../event-list/event-list.json';
import proposalList from '../proposal-list/proposal-list.js';

export default {
  title: 'Tenk kommersialisering før du publiserer',
  ingress:
    'Forskningsrådet ønsker mer samfunnsnytte fra forskerprosjekter. Vi går sammen med universitetenes TTO-er for å finne flere gode kandidater for kommersialisering fra prosjekter Forskningsrådet har finansiert.',
  content: utils.RichText(
    utils.ImageWithCaption({
      image: {
        alt: 'Glade gutter',
        src: '/static-site/assets/tto-lederne.png'
      },
      caption:
        'Når du har resultater, ta kontakt med oss uansett, oppfordrer TTO-lederne Stein Eggan (f.v.), Anders Aune og Eirik Wasmuth Lundblad. Forskere på høyt nivå innenfor sitt fagområde kan sitte med resultater de ikke selv aner anvende mulighetene av.  (Foto: Thomas Keilmann)'
    }),
    '<p>I første omgang henvender Forskningsrådet seg direkte til forskerprosjekter i sju programmer (mer om det i rammen lenger nede). Grunnforskning fører sjelden til mer eller annet enn publisering, siden det er det som blir belønnet og gir poeng i akademia. Men resultater fra grunnforskning kan også innebære funn som kan få direkte samfunnsnytte, og få en markedsverdi, dersom man ser på mulighetene.</p><p>Du må ha mer enn en idé – noen lovende resultater, noe håndfast og nytt. Men det viktigste er at du er interessert i å gjøre noe mer. Ta kontakt med TTO-en ved institusjonen din så tidlig du kan for å få vurdert mulighetene for kommersialisering av resultater, oppfordrer Eirik Wasmuth Lundblad i NORINNOVA, leder for teknologioverføring (TTO) i Tromsø.</p><p>Hvis det er mulig å lage en prototyp, er det en fordel – men ingen forutsetning, sier Stein Eggan, daglig leder i NTNU Technology Transfer.</p><p>En annen ting: Husk alltid patentering før publisering! Om arbeidet ditt er publisert, vil det være umulig å patentere. Presentasjon på konferanser er et eksempel på publisering. Etter patentering er det derimot mulig å publisere. Derfor bør du ta kontakt med TTO-en din tidlig for å få en vurdering, om du er interessert i kommersialisering, anbefaler Eggan.</p><p>Så snart andre har fått vite om funnene dine, blir det vanskeligere å beskytte funnene og finne investorer som vil satse penger på å bringe resultatene fram til anvendelse i form av et produkt eller en tjeneste, opplyser Lundblad.</p><h2>Lag en plan før publisering</h2><p>Det behøver ikke ta lang tid. I møtet med TTO-representantene vil dere sammen vurdere kommersialiseringsmulighetene, og samtidig legge opp en plan for dette og publisering.</p><p>Det kan hende møtet konkluderer med at publisering er det riktige å gjøre.</p><p>Det kan også hende at resultatene kan publiseres delvis, mens andre deler kan vente, dersom noen resultater kan være interessante for kommersialisering.</p><p>Så snart resultater er publisert, får du ikke patentbeskyttelse. Personer eller industri som er villig til å investere penger i forskningsresultater for å utvikle et produkt eller en tjeneste, trenger ofte å få patentbeskyttelse for dette, så det kan gi avkastning på sikt.</p><p>Et typisk eksempel er nye medisiner, som det tar lang tid å utvikle. Det krever ofte store investeringer i form av penger og ressurser. Det koster veldig mye før medisinen kommer på markedet. Hvis det ikke er mulig å få patentbeskyttelse og dermed eksklusivt tjene penger på produktet en gang i fremtiden, blir det ikke investert i utviklingen av produktet.</p><p>– Mye grunnforskning har ingen kommersialiseringsverdi i seg selv, men når du har resultater, ta kontakt med oss uansett. Det hender vi ser ting på andre måter enn forskerne selv gjør. Forskere på høyt nivå innenfor sitt fagområde kan ofte sitte med resultater de ikke selv aner anvendelsesmulighetene av. Hvis forskeren holder igjen publiseringen bare litt og lar oss få se på hva de har, øker det sjansen for at resultatene kan komme til anvendelse. La oss se på det i fellesskap! oppfordrer Stein Eggan.</p><h2>Fra ideer til konsept</h2><p>– Men forskeren må også være klar over at et kommersialiseringsprosjekt krever tid, når resultater tas videre. Det er ikke bare å lene seg tilbake og vente på hva som kommer ut av det. Forskeren må involvere og engasjere seg. Da er det mange spennende muligheter. Det kan gi muligheter for mer forskning, mer finansiering, mer oppmerksomhet, og institusjonen kan bli mer attraktiv både som studiested og samarbeidspartner. Når du starter et selskap og tar ut et patent – det er da jobben begynner. Mange synes det er morsomt og blir veldig motivert av det, forteller Eggan.</p><p>– De fleste forskere vil ikke bli gründere og bør trolig heller ikke bli gründere. Forsker og TTO samarbeider om å utvikle ideene frem til et konsept som kan tas over av investorer eller bedrifter. Dette er den vanligste formen for kommersialisering av forskningsresultater. Da kan forskeren fortsette i sin jobb og samtidig bidra til verdiskaping i samfunnet i form av at forskningsfunn danner grunnlag for nyetablering av bedrifter eller styrking av etablerte bedrifter, forklarer Eirik Lindblad.</p><p>Forskeren skal ikke bruke egne prosjektmidler eller søke midler til kommersialisering på egen hånd. TTO-en søker sammen med forskeren eller gjør mye av jobben med å skaffe til veie midlene som trengs for testing, verifisering og kommersialisering.</p><p>Et første steg etter møtet i din TTO, kan være å søke verfiseringsmidler fra Forskningsrådet. Det får du hjelp til fra dine støttespillere i TTO-en. <b>Søknaden må være hos Forskningsrådet innen 10. oktober.</b> </p><p>I 2017 delte Forskningsrådet ut over 50 millioner kroner i kommersialiseringsstøtte til 15 prosjekter med forskningsrsultater som kan gi verdiskaping.</p>',
    utils.ImageWithCaption({
      caption:
        'Når du har resultater, ta kontakt med oss uansett, oppfordrer TTO-lederne Stein Eggan (f.v.), Anders Aune og Eirik Wasmuth Lundblad. Forskere på høyt nivå innenfor sitt fagområde kan sitte med resultater de ikke selv aner anvende mulighetene av.  (Foto: Thomas Keilmann)',
      image: {
        alt: 'Glade gutter',
        src: '/static-site/assets/tto-lederne.png'
      },
      title: 'Tittel for bilde med bildetekst'
    }),
    '<p>Forskeren skal ikke bruke egne prosjektmidler eller søke midler til kommersialisering på egen hånd. TTO-en søker sammen med forskeren eller gjør mye av jobben med å skaffe til veie midlene som trengs for testing, verifisering og kommersialisering.</p>',
    '<p>Et første steg etter møtet i din TTO, kan være å søke verfiseringsmidler fra Forskningsrådet. Det får du hjelp til fra dine støttespillere i TTO-en. Søknaden må være hos Forskningsrådet innen 10. oktober.</p>',
    utils.MediaWithCaption({
      caption:
        'Når du har resultater, ta kontakt med oss uansett, oppfordrer TTO-lederne Stein Eggan (f.v.), Anders Aune og Eirik Wasmuth Lundblad. Forskere på høyt nivå innenfor sitt fagområde kan sitte med resultater de ikke selv aner anvende mulighetene av.  (Foto: Thomas Keilmann)',
      title: 'Tittel for Youtube-video i artikkel',
      youTubeVideo: {
        urlOrEmbed: 'https://youtu.be/y8Ezdbz93GM'
      }
    }),
    '<p>Forskeren skal ikke bruke egne prosjektmidler eller søke midler til kommersialisering på egen hånd. TTO-en søker sammen med forskeren eller gjør mye av jobben med å skaffe til veie midlene som trengs for testing, verifisering og kommersialisering.</p>',
    utils.MediaWithCaption({
      caption:
        'Når du har resultater, ta kontakt med oss uansett, oppfordrer TTO-lederne Stein Eggan (f.v.), Anders Aune og Eirik Wasmuth Lundblad. Forskere på høyt nivå innenfor sitt fagområde kan sitte med resultater de ikke selv aner anvende mulighetene av. (Foto: Thomas Keilmann)',
      title: 'Tittel',
      url: '#',
      image: {
        src: '/static-site/assets/tto-lederne.png',
        alt: 'Glade gutter'
      }
    }),
    utils.Process({
      title: 'Søknadsprosess',
      introText: 'Slik fungerer søknadsprosessen.',
      isCarousel: true,
      items: [
        {
          icon: {
            alt: 'globe',
            src: '/static-site/assets/icon-money.png'
          },
          url: '/',
          text:
            'Dere har en idé eller et behov som en forsker kan hjelpe til med'
        },
        {
          icon: {
            alt: 'house',
            src: '/static-site/assets/icon-money.png'
          },
          text: 'Sjekk forskerne i forskerpoolen'
        },
        {
          icon: {
            alt: 'info',
            src: '/static-site/assets/icon-money.png'
          },
          text: 'Søk om å få dekket forskere fra poolen'
        },
        {
          text: 'Forskningsrådet vurderer søknaden det går raskt'
        },
        {
          icon: {
            alt: 'mail',
            src: '/static-site/assets/icon-money.png'
          },
          url: '/',
          text: `Hvis ok, akseptér kontrakt på "Mitt nettsted" og ta i bruke forsker(e)`
        },
        {
          text:
            'Når dere har brukt timene får dere faktura med moms fra forskningsinstitusjonen'
        },
        {
          text:
            'Dere sender e-faktura til Forskningsrådet på nettobeløpet Det vil si uten MVA*'
        },
        {
          text: 'Forskningsrådet foretar en utbetaling til dere'
        }
      ]
    }),
    utils.Timeline({
      startIndex: 0,
      title: 'Søknadsprosessen',
      labels: {
        next: 'Neste',
        previous: 'Forrige'
      },
      items: [
        {
          title: '02 aug 2018',
          subTitle: 'Åpent for søknad',
          isPastDate: true,
          text: utils.RichText(
            'Søknadstypen unge forskertalenter er beregnet på forskere som er på et tidlig stadium i karrieren og som har vist evne til å utføre forskning av høy vitenskapelig kvalitet.'
          )
        },
        {
          title: '24 nov 2018',
          subTitle: 'Søknadsfrist',
          isPastDate: true,
          text: utils.RichText(
            'Ved vurderingen av søknadene vil det bli lagt vekt på prosjektleders evne til å arbeide selvstendig, faglige modenhet og forskerpotensial, dokumentert gjennom for eksempel publikasjoner og mobilitet.'
          ),
          progress: 35
        },
        {
          title: '05 mar 2019',
          subTitle: 'Utsendelse av svar'
        },
        {
          title: '05 mar 2019',
          subTitle: 'Utsendelse av svar'
        },
        {
          title: '05 mar 2020',
          subTitle: 'Utsendelse av svar'
        }
      ]
    }),
    utils.Quote({
      text:
        "Vi ønsker å bli sett på som en 'kompetansehub', et knutepunkt for akademia med forbindelser ut i industrien og investeringsmiljøene.",
      quoteBy: 'Anders Aune',
      onPageEditing: {
        text: 'QuoteText',
        quoteBy: 'QuoteByText'
      }
    }),
    utils.PortfolioContent({
      title: 'Hvem kan søke om støtte',
      content: utils.ContentArea(
        utils.LinkWithText({
          link: { text: 'Offsentlig sektor', url: '#' },
          text:
            'Fornyelse og innovasjon i offentlig sektor gkær Norge mer effektivt og bedre rustet til å takle store samfunnsutfordringer.'
        }),
        utils.LinkWithText({
          link: { text: 'Næringsliv', url: '#' },
          text:
            'Innovasjon og kunnskapsbyggning i nøringslivet gjør Norges verdiskaping mer bærekraftig i framtiden.'
        }),
        utils.LinkWithText({
          link: { text: 'Forskningsinstituskjoner', url: '#' },
          text:
            'Økonomisk støtte til forskningsprosjekter hever nivået på rapportene og bidrar til verdifull kunnskap som kan deles med flere.'
        })
      )
    }),
    utils.AccordionBlock({
      accordion: {
        guid: '012unvoqd9fyqv098wetunv',
        expandLabel: 'Vis mer',
        collapseLabel: 'Vis mindre',
        initiallyOpen: true
      },
      title: 'Fakta om Lorem ipusm',
      text: utils.RichText(
        '<p>Lorem ipsum dolor sit amet, choro civibus eu sed, graeco evertitur sed at, invidunt voluptaria mea an. Set commodo vocibus at mea, augue inpercipit ne vis at erat soluta audiam sit.</p><p>Lorem ipsum dolor sit amet, choro civibus eu sed, graeco evertitur sed at, invidunt voluptaria mea an. Set commodo vocibus at mea, augue inpercipit ne vis at erat soluta audiam sit.</p>'
      )
    }),
    utils.AccordionBlock({
      accordion: {
        guid: '98u1h34cv09q8weyfcq09',
        expandLabel: 'Vis mer',
        collapseLabel: 'Vis mindre'
      },
      title: 'NTNU Technology Transfer',
      text: utils.RichText(
        '<p>NTNU Technology Transferer kommersialiseringsaktør for NTNU og Helse Midt-Norge, med tilhold i Trondheim, Ålesund og på Gjøvik. NTNU TTO har bidratt til rundt 150 nye bedrifter, 135 lisensavtaler med eksisterende industri og flere hundre arbeidsplasser som har tatt en rekke nye produkter og tjenester ut i markedet. Her er noen:</p><ul><li><a href="www.kahoot.com">Kahoot</a> – læringsspill med 70 millioner måndelige brukere globalt</li><li><a href="www.kahoot.com">Kahoot</a> – læringsspill med 70 millioner måndelige brukere globalt</li><li><a href="www.kahoot.com">Kahoot</a> – læringsspill med 70 millioner måndelige brukere globalt</li><li><a href="www.kahoot.com">Kahoot</a> – læringsspill med 70 millioner måndelige brukere globalt</li><li><a href="www.kahoot.com">Kahoot</a> – læringsspill med 70 millioner måndelige brukere globalt</li></ul>'
      )
    }),
    utils.TagLinkList({
      leftAligned: true,
      tags: [
        { link: { text: 'Hovedtema', url: '?1' } },
        { link: { text: 'Undertema' }, inactive: true },
        { link: { text: 'Tilknyttet tema', url: '?3' } },
        { link: { text: 'Tema under', url: '?4' } },
        { link: { text: 'Undertema', url: '?5' } },
        { link: { text: 'Tilknyttet tema', url: '?6' } },
        { link: { text: 'Tema under', url: '?7' } },
        { link: { text: 'Undertema', url: '?8' } },
        { link: { text: 'Tilknyttet tema', url: '?9' } }
      ]
    })
  ),
  header: {
    share: {
      openButtonText: 'Del',
      shareContent: {
        items: [
          { url: '#', text: 'Mail', icon: 'mail' },
          { url: '#', text: 'Twitter', icon: 'twitter' },
          { url: '#', text: 'Facebook', icon: 'facebook' },
          { url: '#', text: 'LinkedIn', icon: 'linkedin' }
        ]
      }
    },
    accordion: {
      guid: '92837n4tv09w8erugnvp20nu',
      collapseLabel: 'Vis mindre',
      expandLabel: 'Vis mer'
    },

    byline: {
      items: [
        { text: 'Av Brita Skuland' },
        { text: 'Oversatt av Karin Johanne Normann' },
        { text: 'Publisert 13. april 2018' }
      ]
    },
    download: {
      url: '/',
      text: 'Last ned'
    }
  },
  sidebar: utils.ContentArea(
    utils.PortfolioContent({
      isInsidePriorityBlock: true,
      title: 'Har du spørsmål',
      content: utils.ContentArea(
        utils.ContactList({
          contactGroups: [
            {
              title: 'Nanoteknologi',
              contacts: [
                [
                  { text: 'Aase Marie Hundere' },
                  {
                    text: 'amh@forskningsradet.no',
                    url: '#'
                  },
                  { url: '#', text: '+4712345678' }
                ]
              ]
            },
            {
              title: 'Nanoteknologi',

              contacts: [
                [
                  { text: 'Aase Marie Hundere' },
                  {
                    text: 'amh@forskningsradet.no',
                    url: '#'
                  },
                  { url: '#', text: '+4712345678' }
                ],
                [
                  { text: 'Aase Marie Hundere' },
                  {
                    text: 'amh@forskningsradet.no',
                    url: '#'
                  },
                  { url: '#', text: '+4712345678' }
                ]
              ]
            },
            {
              title: 'Nanoteknologi',

              contacts: [
                [
                  { text: 'Aase Marie Hundere' },
                  {
                    text: 'amh@forskningsradet.no',
                    url: '#'
                  },
                  { url: '#', text: '+4712345678' }
                ]
              ]
            }
          ]
        })
      )
    }),
    utils.ContactInfo({
      title: 'Kontaktperson',
      details: [
        { text: 'Spesialrådgiver' },
        { text: 'Næringslig og teknologi' },
        {
          text: 'omr@forskningsradet.no',
          url: 'mailto:omr@forskningsradet.no'
        },
        { text: '+47 91 65 86 03', url: 'tel:+4712345678' }
      ]
    }),
    utils.InfoBlock({
      title: 'Teknologioverførings-kontorer (TTO)',
      text: utils.RichText(
        '<p>FORNY2020 samarbeider med 10 teknologioverføringskontorer. Her finner du <a href="/test">en ovesikt over TTOene</a>.</p>'
      ),
      editorTheme: '-theme-blue'
    }),
    utils.InfoBlock({
      title: 'Infoboks til bruk i høyrespalten',
      text: utils.RichText(
        '<p>Lorem ipsum dolor sit amet, choro civibus eu sed, graeco evertitur sed at, invidunt voluptaria mea an. Set commodo vocibus at mea, augue inpercipit ne vis at erat soluta audiam sit:</p><ul><li>Vim ex consul principes</li><li>Et vivendo delicatissimi vim, vikno saperet <a href="www.appetere.no">appetere.no</a></li><li>Ad per doctus accusam </li><li>Pri esse salutandi interessetin</li><li>Melius tamquam consetetur euda</li></ul>'
      ),
      editorTheme: '-theme-orange'
    }),
    utils.InfoBlock({
      title: 'Tittel til liten lenkeboks i høyrespalten',
      text: utils.RichText(
        '<p>Lorem ipsum dolor sit amet, choro civibus eu sed, graeco evertitur sed at, invidunt voluptaria mea an. Stet commodo vocibus. </p>'
      ),
      editorTheme: '-theme-blue',
      cta: {
        url: '/test',
        text: 'Call to action'
      }
    }),
    utils.RelatedArticles({
      usedInSidebar: true,
      articles: [
        {
          title: { text: 'Nyhetssak med lenke på tittel', url: '#' },
          published: { type: 'Publisert', date: '21. september 2019' },
          image: {
            alt: 'Space',
            src: '/static-site/assets/students.png'
          }
        },
        {
          title: {
            text:
              'Nyhetssak med lengre tittel – kun én dato synes i byline, enten publisert eller sist oppdatert',
            url: '#'
          },
          published: { type: 'Publisert', date: '21. september 2019' }
        },
        {
          title: { text: 'Nyhetssak', url: '#' },
          published: { type: 'Publisert', date: '21. september 2019' }
        }
      ],
      relatedContent: {
        link: { text: 'Se alle', url: '#' },
        title: 'Aktuelt'
      }
    }),
    utils.RelatedPublications({
      usedInSidebar: true,
      publications: [
        {
          title: 'Sluttrapport for Program for samisk forskning II 2007-2017',
          url: '/',
          metadata: {
            items: [
              { label: 'Arrangementstype', text: 'kurs' },
              { label: 'Varighet', text: '2 dager' },
              {
                icon: { iconTheme: 'pdf' },
                label: 'Sted',
                text: 'Trondheim'
              }
            ]
          }
        },
        {
          title:
            'Evaluation of the Social Sciences in Norway Report from Panel 6 – Economic Administrative Research Areas',
          url: '/',
          metadata: {
            items: [{ label: 'Type', text: 'Rapport' }]
          }
        }
      ],
      relatedContent: {
        link: { text: 'Se alle', url: '#' },
        title: 'Aktuelle publikasjoner'
      }
    }),

    utils.RelatedDates({
      usedInSidebar: true,
      dates: eventList.events,
      relatedContent: {
        link: { text: 'Se alle', url: '#' },
        title: 'Kommende arrangementer'
      }
    }),
    utils.PortfolioContent({
      title: 'Vi støtter forsking innen',
      content: utils.ContentArea(
        utils.LinkList({
          items: [
            {
              text: 'Somewhere decisive',
              url: '/en/'
            },
            {
              text: 'Oddly placed',
              url: '/en/neweventlistpage/'
            },
            {
              text: 'Continuous feriocity',
              url: '/en/framtidigevent/'
            },
            {
              text: 'Tender niblings',
              url: '/en/newspage/'
            },
            {
              text: 'Somewhere',
              url: 'mailto:Some@where.com'
            }
          ]
        })
      )
    }),
    utils.RelatedDates({
      usedInSidebar: true,
      dates: proposalList.groups[0].proposals,
      relatedContent: {
        link: { text: 'Se alle', url: '#' },
        title: 'Aktuelle utlysninger'
      }
    })
  ),
  footer: {
    byline: { items: [{ text: 'Publisert 13.05.2018' }] },
    share: {
      openButtonText: 'Del',
      shareContent: {
        items: [
          { url: '#', text: 'Mail', icon: 'mail' },
          { url: '#', text: 'Twitter', icon: 'twitter' },
          { url: '#', text: 'Facebook', icon: 'facebook' },
          { url: '#', text: 'LinkedIn', icon: 'linkedin' }
        ]
      }
    },
    download: {
      url: '/',
      text: 'Last ned'
    }
  }
};
