/*
name: Regionale Forskningsfond Vestlandet
group: Satelitt
*/
import React from 'react';

import merge from 'lodash/merge';

import Header from 'components/header';

import content from './rfv.json';
import headerContent from '../header/header.json';

const RFV = () => (
  <React.Fragment>
    <Header {...merge({}, headerContent, content)} />
    {/* To make wcag tests happy, #main needs to exist on the page */}
    <main id="main" />
  </React.Fragment>
);

export default RFV;
