import utils from '../../content-area-utils';

export default {
  satelliteHeader: {
    image: {
      alt: 'Header',
      src: '/static-site/assets/students.png'
    },
    links: [
      { text: 'Finn utlysninger', url: '?1' },
      { text: 'Søknadsresultater', url: '?2' },
      { text: 'En knapp til', url: '?3' }
    ],
    ingress:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus scelerisque sem vitae faucibus viverra. Phasellus non augue lacus. Lorem ipsum dolor sit amet. Sem vitae faucibus viverra et non augue lacus. Lorem ipsum dolor sit amet.',
    onPageEditing: { title: 'Title', ingress: 'Ingress' },
    title: 'Forside for satelitter',
    text:
      'Lorem ipsum vi øker kvaliteten på norsk forskning. Hvert år bidrar vi med opp mot 9 milliarder kroner for å fremme innovasjon og bærekraft.'
  },
  content: utils.ContentArea(
    utils.InfoBlock(
      {
        icon: { alt: 'money icon', src: '/static-site/assets/icon-money.png' },
        title: '2 i bredden blokk med ikon og lenke i tittel',
        text: utils.RichText(
          'Ut ius ignota scripta disputando, deleniti perfecto dignissim mea an. Phasellus scelerisque sem vitae faucibus viverra. Mucius epicurei definitionem cum in, in his veritus referrentur. '
        ),
        url: '/article'
      },
      '-size-half'
    ),
    utils.InfoBlock(
      {
        cta: {
          onPageEditing: 'CallToAction',
          text: 'Call to action',
          url: '/'
        },
        title: '2 i bredden blokk med knapp',
        text: utils.RichText(
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus scelerisque sem vitae faucibus viverra. Phasellus non augue lacus. Lorem ipsum dolor sit amet.'
        )
      },
      '-size-half'
    ),
    utils.InfoBlock(
      {
        icon: { alt: 'money icon', src: '/static-site/assets/icon-money.png' },
        title: '3 i bredden blokk med ikon og lenke på tekst',
        text: utils.RichText(
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus scelerisque sem vitae faucibus viverra. Phasellus non augue lacus. '
        ),
        url: '/article'
      },
      '-size-third'
    ),
    utils.InfoBlock(
      {
        icon: { alt: 'students', src: '/static-site/assets/students.png' },
        title: 'Med foto i stedet for ikon',
        text: utils.RichText(
          'Phasellus scelerisque sem vitae faucibus viverra. Phasellus non augue lacus. Lorem ipsum dolor sit amet.'
        )
      },
      '-size-third'
    ),
    utils.InfoBlock(
      {
        title: '3 i bredden blokk med lenkeliste',
        linkList: {
          items: [
            {
              url: '/',
              text: 'Forskningrådets prisar for unge framifrå forskarar'
            },
            {
              url: '/',
              text: 'Forskningrådets formidlingspris'
            },
            {
              url: '/',
              text: 'Forskningsrådets innovasjonspris'
            }
          ],
          onPageEditing: { items: 'Items' }
        }
      },
      '-size-third'
    ),
    utils.RichTextBlock({
      title: 'Riktekstblokk',
      text: utils.RichText(
        '<p>NTNU Technology Transferer kommersialiseringsaktør for NTNU og Helse Midt-Norge, med tilhold i Trondheim, Ålesund og på Gjøvik. NTNU TTO har bidratt til rundt 150 nye bedrifter, 135 lisensavtaler med eksisterende industri og flere hundre arbeidsplasser som har tatt en rekke nye produkter og tjenester ut i markedet. Her er noen:</p><ul><li><a href="www.kahoot.com">Kahoot</a> – læringsspill med 70 millioner måndelige brukere globalt</li><li><a href="www.kahoot.com">Kahoot</a> – læringsspill med 70 millioner måndelige brukere globalt</li><li><a href="www.kahoot.com">Kahoot</a> – læringsspill med 70 millioner måndelige brukere globalt</li><li><a href="www.kahoot.com">Kahoot</a> – læringsspill med 70 millioner måndelige brukere globalt</li><li><a href="www.kahoot.com">Kahoot</a> – læringsspill med 70 millioner måndelige brukere globalt</li></ul>'
      )
    }),

    utils.NumberBlock(
      {
        number: '23,7',
        description: 'prosent',
        text: 'Tekst på en linje'
      },
      '-size-half'
    ),
    utils.ContactBlock(
      {
        title: 'Kontaktinformasjon',
        items: utils.ContentArea(
          utils.ContactInfo({
            title: 'Odd M. Reitevold',
            details: [
              { text: 'Spesialrådgiver' },
              { text: 'Næringsliv og teknologi' },
              {
                url: 'mailto:omr@forskningsradet.no',
                text: 'omr@forskningsradet.no'
              }
            ]
          }),
          utils.ContactInfo({
            title: 'Per Ivar Høvring',
            details: [
              { text: 'Norges Forskningsråd' },
              { text: '+47 123 45 678', url: '#' }
            ]
          })
        )
      },
      '-size-half'
    ),
    utils.CampaignBlock(
      {
        cta: {
          text: 'Call to action',
          url: '/article'
        },
        image: {
          alt: 'Space',
          src: '/static-site/assets/students.png'
        },
        title:
          'Kampanjefelt med kort, fengende budskap. Denne har litt lengre tekst så den blir litt høyere'
      },
      '-size-full-screen'
    ),
    utils.InfoBlock(
      {
        title: 'Lenke men ingen ikon',
        text: utils.RichText(
          'Phasellus scelerisque sem vitae faucibus viverra. Phasellus non augue lacus. Lorem ipsum dolor sit amet.'
        ),
        url: '#'
      },
      '-size-third'
    ),
    utils.NumberBlock(
      {
        number: '23,7',
        description: 'prosent',
        text: 'Tekst på en linje'
      },
      '-size-third'
    ),
    utils.NumberBlock(
      {
        number: '23753',
        description: 'gule bakterieprøver',
        text: 'Tekst på flere linjer og kanskje så mange som tre hele linjer'
      },
      '-size-third'
    ),
    utils.ImageWithLink(
      {
        image: {
          alt: 'Call to action',
          src: '/static-site/assets/students.png',
          onPageEditing: { image: 'Image' }
        },
        onPageEditing: { text: 'Text', url: 'URL' },
        text: 'Med on page editing',
        url: 'Url'
      },
      '-size-half'
    ),
    utils.ImageWithLink(
      {
        image: {
          alt: 'Call to action',
          src: '/static-site/assets/students.png'
        },
        text:
          'Tittel tittel tittel her kan man ha en lang lang lang lang tittel tittel',
        url: 'Url'
      },
      '-size-half'
    ),
    utils.CampaignBlock({
      cta: {
        text: 'Call to action',
        url: '/article'
      },
      image: {
        alt: 'Space',
        src: '/static-site/assets/students.png'
      },
      title: 'Kampanjefelt med kort, fengende budskap'
    }),
    utils.CampaignBlock({
      editorTheme: '-theme-dark-blue',
      cta: {
        text: 'Call to action',
        url: '/article'
      },
      image: {
        alt: 'Space',
        src: '/static-site/assets/students.png'
      },
      title: 'Kampanjefelt med kort, fengende budskap'
    }),
    utils.ImageWithLink(
      {
        image: {
          alt: 'Call to action',
          src: '/static-site/assets/students.png'
        },

        text: 'Tittel',
        url: 'Url'
      },
      '-size-third'
    ),
    utils.ImageWithLink(
      {
        image: {
          alt: 'Call to action',
          src: '/static-site/assets/students.png'
        },
        text:
          'Tittel tittel tittel her kan man ha en lang lang lang lang tittel tittel',
        url: 'Url'
      },
      '-size-third'
    ),
    utils.InfoBlock(
      {
        editorTheme: '-theme-dark-blue',
        title: 'Teknologioverførings-kontorer (TTO)',
        text: utils.RichText(
          'FORNY2020 <a href="/">samarbeider</a> med 10 teknologioverføringskontorer. Her finner du en ovesikt over TTOene.'
        ),
        url: '/article'
      },
      '-size-third'
    )
  )
};
