/*
name: Satelittforside
group: Satelitt
*/

import React from 'react';
import SatelliteFrontpage from 'components/satellite-frontpage';

import content from './satellite-frontpage.js';

const SatelliteFrontpagePage = () => <SatelliteFrontpage {...content} />;

export default SatelliteFrontpagePage;
