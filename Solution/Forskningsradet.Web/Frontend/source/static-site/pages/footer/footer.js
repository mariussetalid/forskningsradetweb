import utils from '../../content-area-utils';

export default {
  contactInfo: utils.RichText(
    '<p><b>Norges forskningsråd</b><br />Postboks 564<br />1327 Lysaker</p> <p><b>Besøksadresse</b><br />Drammensveien 288<br />0283 Oslo</p><p><b>E-post</b><br /><a href="mailto:post@forskningsradet.no">post@forskningsradet.no</a></p>'
  ),
  linkLists: [
    {
      id: 1,
      title: 'Våre satelitter',
      links: [
        {
          text: 'Nysgjerrigper',
          url: '/'
        },
        {
          text: 'Regionale forskningsfond',
          url: '/'
        },
        {
          text: 'Skattefunn',
          url: '/'
        },
        {
          text: 'Unge forskere',
          url: '/'
        },
        {
          text: 'Forskningsdagene',
          url: '/'
        }
      ]
    },
    {
      id: 2,
      title: 'Andre snarveier',
      links: [
        {
          text: 'Prosjektbanken',
          url: '/'
        },
        {
          text: 'Publikasjoner',
          url: '/'
        },
        {
          text: 'Ofte stilte spørsmål',
          url: '/'
        }
      ]
    }
  ],
  infoLinks: [
    {
      text: 'Bruk av informasjonskapsler',
      url: '/'
    },
    {
      text: 'Miljøpolicy',
      url: '/'
    },
    {
      text: 'Personvernerklæring',
      url: '/'
    },
    {
      text: 'Tildelingsbrev',
      url: '/'
    },
    {
      text: 'Serviceerklæring',
      url: '/'
    },
    {
      text: 'Offentlig postjournal',
      url: '/'
    }
  ],
  socialMedia: {
    items: [
      {
        url: '/?1',
        provider: 'facebook',
        text: 'Facebook'
      },
      {
        url: '/?2',
        provider: 'linkedin',
        text: 'LinkedIn'
      },
      {
        url: '/?3',
        provider: 'twitter',
        text: 'Twitter'
      },
      {
        url: '/?4',
        provider: 'rss',
        text: 'Rss'
      }
    ]
  },
  newsLetter: {
    url: '/',
    text: 'Meld deg på nyhetsbrev'
  }
};
