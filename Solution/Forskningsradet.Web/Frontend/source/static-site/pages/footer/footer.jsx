/*
name: Footer
group: Globale komponenter
*/
import React from 'react';

import content from './footer.js';

import Footer from 'components/footer';

const FooterPage = () => <Footer {...content} />;

export default FooterPage;
