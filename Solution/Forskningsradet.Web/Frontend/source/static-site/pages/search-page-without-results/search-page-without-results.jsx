/*
name: Uten resultater
group: Søk
*/
import React from 'react';
import Layout from '../../layout';
import SearchPage from 'components/search-page';
import content from './search-page-without-results.json';

const SearchWithoutResults = () => (
  <Layout>
    <SearchPage {...content} />
  </Layout>
);

export default SearchWithoutResults;
