import utils from '../../content-area-utils';

export default {
  title: 'Ordliste',
  form: { endpoint: '/glossary' },
  resultsDescription: 'Viser 1-10 av 200 treff',
  search: {
    input: {
      label: 'Søk',
      name: 'search',
      placeholder: 'Søk'
    },
    submit: {
      text: 'Søk'
    },
    resultsDescription:
      'Ditt søk på “bevilgninger” ga 1 treff i forskningradet.no'
  },
  emptyList: { text: 'Fant ingen treff. Prøv et annet søkeord.' },
  results: [
    {
      htmlId: 'letter-b',
      title: 'b',
      results: utils.ContentArea(
        utils.SearchResult({
          title: 'Bevilgning',
          text: 'Bindende løfte om finansiering fra Forskningsrådet.'
        })
      )
    },
    {
      htmlId: 'letter-f',
      title: 'f',
      results: utils.ContentArea(
        utils.SearchResult({
          title: 'Faktiske inntekter',
          text:
            'Utgifter som utbetales av prosjektet og utgifter som henføres til prosjektet og som ikke utløser en utbetaling fra prosjektet, herunder utgifter til lokaler, forskningsinfrastruktur og personellressurser.'
        }),
        utils.SearchResult({
          title: 'Faktiske inntekter',
          text:
            'Utgifter som utbetales av prosjektet og utgifter som henføres til prosjektet og som ikke utløser en utbetaling fra prosjektet, herunder utgifter til lokaler, forskningsinfrastruktur og personellressurser.'
        })
      )
    }
  ],
  navigation: [
    {
      isCurrent: false,
      link: {
        text: 'a',
        url: '/a'
      }
    },
    {
      isCurrent: true,
      link: {
        text: 'b',
        url: '#letter-b'
      }
    },
    {
      isCurrent: false,
      link: {
        text: 'c',
        url: ''
      }
    },
    {
      isCurrent: false,
      link: {
        text: 'd',
        url: ''
      }
    },
    {
      isCurrent: false,
      link: {
        text: 'e'
      }
    },
    {
      isCurrent: false,
      link: {
        text: 'f',
        url: '#letter-f'
      }
    },
    {
      isCurrent: false,
      link: {
        text: 'g',
        url: '/g'
      }
    },
    {
      isCurrent: false,
      link: {
        text: 'h',
        url: '/h'
      }
    },
    {
      isCurrent: false,
      link: {
        text: 'i',
        url: '/i'
      }
    },
    {
      isCurrent: false,
      link: {
        text: 'j',
        url: '/j'
      }
    },
    {
      isCurrent: false,
      link: {
        text: 'k',
        url: '/k'
      }
    },
    {
      isCurrent: false,
      link: {
        text: 'l',
        url: '/l'
      }
    },
    {
      isCurrent: false,
      link: {
        text: 'm',
        url: '/m'
      }
    },
    {
      isCurrent: false,
      link: {
        text: 'n',
        url: '/n'
      }
    },
    {
      isCurrent: false,
      link: {
        text: 'o',
        url: '/o'
      }
    },
    {
      isCurrent: false,
      link: {
        text: 'p',
        url: '/p'
      }
    },
    {
      isCurrent: false,
      link: {
        text: 'q',
        url: '/q'
      }
    },
    {
      isCurrent: false,
      link: {
        text: 'r',
        url: '/r'
      }
    },
    {
      isCurrent: false,
      link: {
        text: 's',
        url: '/s'
      }
    },
    {
      isCurrent: false,
      link: {
        text: 't',
        url: '/t'
      }
    },
    {
      isCurrent: false,
      link: {
        text: 'u',
        url: '/u'
      }
    },
    {
      isCurrent: false,
      link: {
        text: 'v',
        url: '/v'
      }
    },
    {
      isCurrent: false,
      link: {
        text: 'w',
        url: '/w'
      }
    },
    {
      isCurrent: false,
      link: {
        text: 'x',
        url: '/x'
      }
    },
    {
      isCurrent: false,
      link: {
        text: 'y',
        url: '/y'
      }
    },
    {
      isCurrent: false,
      link: {
        text: 'z'
      }
    },
    {
      isCurrent: false,
      link: {
        text: 'æ'
      }
    },
    {
      isCurrent: false,
      link: {
        text: 'ø'
      }
    },
    {
      isCurrent: false,
      link: {
        text: 'å',
        url: '/å'
      }
    }
  ],
  filterLayout: {
    contentArea: utils.ContentArea(
      utils.InfoBlock({
        title: 'Mulighet for blokk her',
        text: utils.RichText(
          '<p>Lorem ipsum dolor sit amet, choro civibus eu sed, graeco evertitur sed at, invidunt voluptaria mea an. Set commodo vocibus at mea, augue inpercipit ne vis at erat soluta audiam sit:</p>'
        ),
        editorTheme: '-theme-blue'
      })
    )
  },
  pagination: {
    title: 'Navigasjon for søkeresultater',
    pages: [
      {
        isCurrent: false,
        label: 'Forrige side',
        link: {
          url: '/?0',
          text: 'Forrige'
        }
      },
      {
        isCurrent: false,
        label: 'Side 1',
        link: {
          url: '/?1',
          text: '1'
        }
      },
      {
        isCurrent: false,
        label: 'Side 2',
        link: {
          url: '/?2',
          text: '2'
        }
      },
      {
        isCurrent: true,
        label: 'Side 3',
        link: {
          url: '/?3',
          text: '3'
        }
      },
      {
        isCurrent: false,
        label: 'Side 4',
        link: {
          url: '/?4',
          text: '4'
        }
      },
      {
        isCurrent: false,
        label: 'Side 5',
        link: {
          url: '/?5',
          text: '5'
        }
      },
      {
        isCurrent: false,
        label: 'Side 6',
        link: {
          url: '/?6',
          text: '6'
        }
      },
      {
        isCurrent: false,
        label: 'Side 7',
        link: {
          url: '/?7',
          text: '7'
        }
      },
      {
        isCurrent: false,
        label: 'Side 8',
        link: {
          url: '/?8',
          text: '8'
        }
      },
      {
        isCurrent: false,
        label: 'Neste side',
        link: {
          url: '/?9',
          text: 'Neste'
        }
      }
    ]
  }
};
