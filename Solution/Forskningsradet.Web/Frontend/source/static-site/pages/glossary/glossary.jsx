/*
name: Ordliste
group: Informasjon
*/
import React from 'react';
import Layout from '../../layout';

import GroupedSearchPage from 'components/grouped-search-page';

import content from './glossary.js';

const Glossary = () => (
  <Layout>
    <GroupedSearchPage {...content} />
  </Layout>
);

export default Glossary;
