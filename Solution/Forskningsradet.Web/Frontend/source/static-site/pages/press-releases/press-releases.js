import utils from '../../content-area-utils';

export default {
  title: 'Pressemeldinger',
  content: utils.RichText(
    utils.RelatedArticles({
      articles: [
        {
          image: {
            alt: 'Space',
            src: '/static-site/assets/students.png'
          },
          title: { text: 'Nyhetssak med lenke på tittel', url: '#' },
          published: { type: 'Publisert', date: '21. september 2019' },
          text:
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus scelerisque sem vitae faucibus viverra. Phasellus non augue lacus. Lorem ipsum dolor sit amet. Sem vitae faucibus viverra et non augue lacus. Lorem ipsum dolor sit amet.'
        },
        {
          title: {
            text:
              'Nyhetssak med lengre tittel – kun én dato synes i byline, enten publisert eller sist oppdatert',
            url: '#'
          },
          published: { type: 'Publisert', date: '3. oktober 2019' },
          text:
            'Scelerisque sem vitae faucibus viverra, phasellus non augue lacus. Lorem ipsum dolor sit amet. Consectetur adipiscing elit sem vitae faucibus viverra et non augue lacus. '
        }
      ],
      relatedContent: {
        border: 'top'
      }
    })
  ),
  sidebar: utils.ContentArea(
    utils.InfoBlock({
      title: 'Pressevakt',
      text: utils.RichText(
        '<div><span>Telefon:</span><a href="#">+47 409 22 288</a></div><div><span>E-post:</span><a href="mailto:media@forskningsradet.no">media@forskningsradet.no</a></div>'
      ),
      editorTheme: '-theme-orange'
    }),
    utils.ContactBlock(
      {
        title: 'Pressekontakter',
        items: utils.ContentArea(
          utils.ContactInfo({
            title: 'Christian Haug-Moberg',
            details: [
              { text: '+47 123 45 678', url: '#' },
              {
                url: 'mailto:chh@forskningsradet.no',
                text: 'chh@forskningsradet.no'
              }
            ]
          }),
          utils.ContactInfo({
            title: 'Per Ivar Høvring',
            details: [
              { text: 'Norges Forskningsråd' },
              { text: '+47 123 45 678', url: '#' }
            ]
          }),
          utils.ContactInfo({
            title: 'Cathrine Torp',
            details: [
              { text: 'Kommunikasjonsdirektør' },
              { text: '+47 22 03 72 46', url: '#' },
              {
                url: 'mailto:cto@forskningsradet.no',
                text: 'cto@forskningsradet.no'
              }
            ]
          })
        )
      },
      '-size-half'
    ),
    utils.InfoBlock({
      title: 'Forskningsrådets logoer til nedlasting',
      linkList: {
        items: [
          {
            url: '/',
            text: 'Logoer til bruk i prosjekter som har fått støtte (.png)'
          },
          {
            url: '/',
            text: 'Logoer for digitale flater (.png)'
          },
          {
            url: '/',
            text: 'Logoer for trykk (.pdf)'
          }
        ]
      },
      editorTheme: '-theme-blue'
    }),
    utils.InfoBlock({
      title: 'Bilder til nedlasting',
      url: '/',
      text: utils.RichText(
        '<p>Her finner du våre bilder som er tilrettelagt for ruk i ulike medier.</p>'
      ),
      editorTheme: '-theme-blue'
    })
  ),
  footer: {
    share: {
      openButtonText: 'Del',
      shareContent: {
        items: [
          { url: '#', text: 'Mail', icon: 'mail' },
          { url: '#', text: 'Twitter', icon: 'twitter' },
          { url: '#', text: 'Facebook', icon: 'facebook' },
          { url: '#', text: 'LinkedIn', icon: 'linkedin' }
        ]
      }
    },
    download: {
      text: 'Last ned',
      url: '#'
    }
  }
};
