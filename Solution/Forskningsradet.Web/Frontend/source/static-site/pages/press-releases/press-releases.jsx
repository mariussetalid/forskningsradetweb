/*
group: CMS-maler
name: Pressemeldinger
path: press-releases
*/

import React from 'react';
import Layout from '../../layout';

import ArticlePage from 'components/article-page';

import content from './press-releases.js';

const PressReleases = () => (
  <Layout>
    <ArticlePage {...content} />
  </Layout>
);

export default PressReleases;
