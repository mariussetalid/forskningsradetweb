/*
name: Listeside
group: Arrangementer
*/
import React from 'react';
import Layout from '../../layout';

import content from './event-list.json';
import EventListPage from 'components/event-list-page';

const EventList = () => (
  <Layout>
    <EventListPage {...content} />
  </Layout>
);

export default EventList;
