import utils from '../../content-area-utils';

import eventList from '../event-list/event-list.json';
import proposalList from '../proposal-list/proposal-list.js';

export default {
  pageHeader: {
    image: {
      src: '/static-site/assets/students.png',
      alt: 'Header'
    },
    ingress:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus scelerisque sem vitae faucibus viverra. Phasellus non augue lacus. Lorem ipsum dolor sit amet. Sem vitae faucibus viverra et non augue lacus. Lorem ipsum dolor sit amet.',
    onPageEditing: { title: 'Title', ingress: 'Ingress' },
    title: 'Læreveiledning i Ny_sgjerrigpermetoden',
    links: [
      { text: 'Finn utlysninger', url: '?1' },
      { text: 'Søknadsresultater', url: '?2' }
    ],
    tags: {
      tags: [
        { link: { text: 'Hovedtema', url: '?1', isExternal: true } },
        { link: { text: 'Undertema' }, inactive: true },
        { link: { text: 'Tilknyttet tema', url: '?3' } },
        { link: { text: 'Tema under', url: '?4' } },
        { link: { text: 'Undertema', url: '?5' } },
        { link: { text: 'Tilknyttet tema', url: '?6' } },
        { link: { text: 'Tema under', url: '?7' } },
        { link: { text: 'Undertema', url: '?8' } },
        { link: { text: 'Tilknyttet tema', url: '?9' } }
      ]
    }
  },
  content: utils.ContentArea(
    utils.RelatedDates({
      relatedContent: {
        emptyList: { text: 'Det er ingen arrangementer akkurat nå.' },
        title: 'Relaterte arrangementer (tom)'
      }
    }),
    utils.RelatedArticles({
      articles: [
        {
          title: { text: 'Nyhetssak med lenke på tittel', url: '#' },
          published: { type: 'Publisert', date: '21. september 2019' }
        },
        {
          title: {
            text:
              'Nyhetssak med lengre tittel – kun én dato synes i byline, enten publisert eller sist oppdatert',
            url: '#'
          },
          published: { type: 'Publisert', date: '21. september 2019' }
        },
        {
          title: { text: 'Nyhetssak', url: '#' },
          published: { type: 'Publisert', date: '21. september 2019' }
        }
      ],
      relatedContent: {
        link: { text: 'Se alle', url: '#' },
        title: 'Aktuelt'
      }
    }),
    utils.RelatedPublications({
      publications: [
        {
          title: 'Sluttrapport for Program for samisk forskning II 2007-2017',
          url: '/',
          metadata: {
            items: [
              { label: 'Type', text: 'Ikke kategorisert' },
              { label: 'Dokument', text: 'pdf (2.3 Mb)' },
              { label: 'Antall sider', text: '69' },
              { label: 'Forfatter', text: '-' },
              { label: 'Gjelder fra', text: '-' }
            ]
          }
        },
        {
          title:
            'Evaluation of the Social Sciences in Norway Report from Panel 6 – Economic Administrative Research Areas',
          url: '/',
          icon: { iconTheme: 'pdf' },
          metadata: {
            items: [
              { label: 'Type', text: 'Rapport' },
              { label: 'Dokument', text: 'pdf (3.4 Mb)' },
              { label: 'Antall sider', text: '260' },
              { label: 'Forfatter', text: '-' },
              { label: 'Gjelder fra', text: '2018' }
            ]
          }
        }
      ],
      relatedContent: {
        link: { text: 'Se alle', url: '#' },
        title: 'Aktuelle publikasjoner'
      }
    }),
    utils.RelatedDates({
      dates: eventList.events,
      relatedContent: {
        link: { text: 'Se alle', url: '#' },
        title: 'Kommende arrangementer'
      }
    }),
    utils.RelatedDates({
      dates: proposalList.groups[0].proposals,
      relatedContent: {
        link: { text: 'Se alle', url: '#' },
        title: 'Aktuelle utlysninger'
      }
    }),
    {
      componentName: 'RichText',
      componentData: utils.RichText(
        '<h2>Fritekst</h2><p>NTNU Technology Transferer kommersialiseringsaktør for NTNU og Helse Midt-Norge, med tilhold i Trondheim, Ålesund og på Gjøvik. NTNU TTO har bidratt til rundt 150 nye bedrifter, 135 lisensavtaler med eksisterende industri og flere hundre arbeidsplasser som har tatt en rekke nye produkter og tjenester ut i markedet. Her er noen:</p><ul><li><a href="www.kahoot.com">Kahoot</a> – læringsspill med 70 millioner måndelige brukere globalt</li><li><a href="www.kahoot.com">Kahoot</a> – læringsspill med 70 millioner måndelige brukere globalt</li><li><a href="www.kahoot.com">Kahoot</a> – læringsspill med 70 millioner måndelige brukere globalt</li><li><a href="www.kahoot.com">Kahoot</a> – læringsspill med 70 millioner måndelige brukere globalt</li><li><a href="www.kahoot.com">Kahoot</a> – læringsspill med 70 millioner måndelige brukere globalt</li></ul>'
      )
    },
    utils.RichTextBlock({
      title: 'Riktekstblokk',
      text: utils.RichText(
        '<p>NTNU Technology Transferer kommersialiseringsaktør for NTNU og Helse Midt-Norge, med tilhold i Trondheim, Ålesund og på Gjøvik. NTNU TTO har bidratt til rundt 150 nye bedrifter, 135 lisensavtaler med eksisterende industri og flere hundre arbeidsplasser som har tatt en rekke nye produkter og tjenester ut i markedet. Her er noen:</p><ul><li><a href="www.kahoot.com">Kahoot</a> – læringsspill med 70 millioner måndelige brukere globalt</li><li><a href="www.kahoot.com">Kahoot</a> – læringsspill med 70 millioner måndelige brukere globalt</li><li><a href="www.kahoot.com">Kahoot</a> – læringsspill med 70 millioner måndelige brukere globalt</li><li><a href="www.kahoot.com">Kahoot</a> – læringsspill med 70 millioner måndelige brukere globalt</li><li><a href="www.kahoot.com">Kahoot</a> – læringsspill med 70 millioner måndelige brukere globalt</li></ul>'
      )
    }),
    utils.InfoBlock(
      {
        icon: { alt: 'money icon', src: '/static-site/assets/icon-money.png' },
        title: '2 i bredden blokk med ikon og lenke i tittel',
        text: utils.RichText(
          'Ut ius ignota scripta disputando, deleniti perfecto dignissim mea an. Phasellus scelerisque sem vitae faucibus viverra. Mucius epicurei definitionem cum in, in his veritus referrentur. '
        ),
        url: '/article'
      },
      '-size-half'
    ),
    utils.InfoBlock(
      {
        cta: {
          onPageEditing: 'CallToAction',
          text: 'Call to action',
          url: '/'
        },
        title: '2 i bredden blokk med knapp',
        text: utils.RichText(
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus scelerisque sem vitae faucibus viverra. Phasellus non augue lacus. Lorem ipsum dolor sit amet.'
        )
      },
      '-size-half'
    ),
    utils.InfoBlock(
      {
        icon: { alt: 'money icon', src: '/static-site/assets/icon-money.png' },
        title: '3 i bredden blokk med ikon og lenke på tekst',
        text: utils.RichText(
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus scelerisque sem vitae faucibus viverra. Phasellus non augue lacus. '
        ),
        url: '/article'
      },
      '-size-third'
    ),
    utils.InfoBlock(
      {
        icon: { alt: 'students', src: '/static-site/assets/students.png' },
        title: 'Med foto i stedet for ikon',
        text: utils.RichText(
          'Phasellus scelerisque sem vitae faucibus viverra. Phasellus non augue lacus. Lorem ipsum dolor sit amet.'
        )
      },
      '-size-third'
    ),
    utils.InfoBlock(
      {
        title: '3 i bredden blokk med lenkeliste',
        linkList: {
          items: [
            {
              text: 'Forskningrådets prisar for unge framifrå forskarar',
              isExternal: true
            },
            {
              url: '/',
              text: 'Forskningrådets formidlingspris',
              isExternal: true
            },
            {
              url: '/',
              text: 'Forskningsrådets innovasjonspris'
            }
          ],
          onPageEditing: { items: 'Items' }
        }
      },
      '-size-third'
    ),
    utils.NumberBlock(
      {
        number: '23,7',
        description: 'prosent',
        text: 'Tekst på en linje'
      },
      '-size-half'
    ),
    utils.ContactBlock(
      {
        title: 'Kontaktinformasjon',
        items: utils.ContentArea(
          utils.ContactInfo({
            title: 'Odd M. Reitevold',
            details: [
              { text: 'Spesialrådgiver' },
              { text: 'Næringsliv og teknologi' },
              {
                url: 'mailto:omr@forskningsradet.no',
                text: 'omr@forskningsradet.no'
              }
            ]
          }),
          utils.ContactInfo({
            title: 'Per Ivar Høvring',
            details: [
              { text: 'Norges Forskningsråd' },
              { text: '+47 123 45 678', url: '#' }
            ]
          })
        )
      },
      '-size-half'
    ),
    utils.CampaignBlock(
      {
        cta: {
          text: 'Se forslagene',
          url: '/article'
        },
        image: {
          alt: 'Space',
          src: '/static-site/assets/students.png'
        },
        title: 'Norges forslag til satsinger for FNs havforskningstiår'
      },
      '-size-full-screen'
    ),
    utils.CampaignBlock({
      cta: {
        text: 'Se forslagene',
        url: '/article'
      },
      image: {
        alt: 'Space',
        src: '/static-site/assets/students.png'
      },
      title: 'Norges forslag til satsinger for FNs havforskningstiår'
    }),
    utils.InfoBlock(
      {
        title: 'Lenke men ingen ikon',
        text: utils.RichText(
          'Phasellus scelerisque sem vitae faucibus viverra. Phasellus non augue lacus. Lorem ipsum dolor sit amet.'
        ),
        url: '#'
      },
      '-size-third'
    ),
    utils.NumberBlock(
      {
        number: '23,7',
        description: 'prosent',
        text: 'Tekst på en linje'
      },
      '-size-third'
    ),
    utils.NumberBlock(
      {
        number: '23753',
        description: 'gule bakterieprøver',
        text: 'Tekst på flere linjer og kanskje så mange som tre hele linjer'
      },
      '-size-third'
    ),
    utils.ImageWithLink(
      {
        image: {
          alt: 'Call to action',
          src: '/static-site/assets/students.png',
          onPageEditing: { image: 'Image' }
        },
        onPageEditing: { text: 'Text', url: 'URL' },
        text: 'Med on page editing',
        url: 'Url'
      },
      '-size-half'
    ),
    utils.ImageWithLink(
      {
        image: {
          alt: 'Call to action',
          src: '/static-site/assets/students.png'
        },
        text:
          'Tittel tittel tittel her kan man ha en lang lang lang lang tittel tittel',
        url: 'Url'
      },
      '-size-half'
    ),
    utils.CampaignBlock({
      cta: {
        text: 'Call to action',
        url: '/article'
      },
      image: {
        alt: 'Space',
        src: '/static-site/assets/students.png'
      },
      title: 'Kampanjefelt med kort, fengende budskap'
    }),
    utils.CampaignBlock({
      editorTheme: '-theme-dark-blue',
      cta: {
        text: 'Call to action',
        url: '/article'
      },
      image: {
        alt: 'Space',
        src: '/static-site/assets/students.png'
      },
      title: 'Kampanjefelt med kort, fengende budskap'
    }),
    utils.ImageWithLink(
      {
        image: {
          alt: 'Call to action',
          src: '/static-site/assets/students.png'
        },

        text: 'Tittel',
        url: 'Url'
      },
      '-size-third'
    ),
    utils.ImageWithLink(
      {
        image: {
          alt: 'Call to action',
          src: '/static-site/assets/students.png'
        },
        text:
          'Tittel tittel tittel her kan man ha en lang lang lang lang tittel tittel',
        url: 'Url'
      },
      '-size-third'
    ),
    utils.InfoBlock(
      {
        editorTheme: '-theme-dark-blue',
        title: 'Teknologioverførings-kontorer (TTO)',
        text: utils.RichText(
          'FORNY2020 <a href="/">samarbeider</a> med 10 teknologioverføringskontorer. Her finner du en ovesikt over TTOene.'
        ),
        url: '/article'
      },
      '-size-third'
    ),
    utils.MediaBlock(
      {
        title: 'Tittel tittel kjempelang tittel kjempelang kjempelang tittel',
        youTubeVideo: {
          urlOrEmbed:
            '<iframe width="560" height="315" src="https://www.youtube.com/embed/GN-peWeNBkU?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>'
        }
      },

      '-size-half'
    ),

    utils.MediaBlock(
      {
        title: 'Tittel',
        url: '#',
        embed: {
          src:
            'https://public.tableau.com/views/Kjnnsbalansebarometerapril2018/Utviklingperinstitusjon?:embed=y&:display_count=yes&:showVizHome=no',
          title: 'Tableau-graf'
        }
      },
      '-size-half'
    ),
    utils.MediaBlock(
      {
        title: 'Tittel',
        url: '#',
        image: {
          src: '/static-site/assets/students.png',
          alt: 'Bilde'
        }
      },
      '-size-half'
    ),
    utils.MediaBlock(
      {
        title: 'Tittel tittel kjempelang tittel kjempelang kjempelang tittel',
        youTubeVideo: {
          urlOrEmbed:
            '<iframe width="560" height="315" src="https://www.youtube.com/embed/GN-peWeNBkU?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>'
        }
      },
      '-size-half'
    ),
    utils.MediaBlock(
      {
        title: 'Tittel',
        youTubeVideo: {
          urlOrEmbed:
            '<iframe width="560" height="315" src="https://www.youtube.com/embed/GN-peWeNBkU?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>'
        }
      },
      '-size-two-thirds'
    ),
    utils.InfoBlock(
      {
        title: '3 i bredden blokk med lenkeliste',
        linkList: {
          items: [
            {
              url: '/',
              text: 'Forskningrådets prisar for unge framifrå forskarar'
            },
            {
              url: '/',
              text: 'Forskningrådets formidlingspris'
            },
            {
              url: '/',
              text: 'Forskningsrådets innovasjonspris'
            }
          ],
          onPageEditing: { items: 'Items' }
        }
      },
      '-size-third'
    ),
    utils.MediaBlock(
      {
        image: {
          src: '/static-site/assets/students.png',
          alt: 'Bilde'
        }
      },
      '-size-half'
    ),
    utils.MediaBlock(
      {
        title:
          'Tittel tittel kjemplelang tittel som er kjempe kjempelang tittel',
        youTubeVideo: {
          urlOrEmbed:
            '<iframe width="560" height="315" src="https://www.youtube.com/embed/GN-peWeNBkU?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>'
        }
      },
      '-size-half'
    ),
    utils.MediaBlock(
      {
        title: 'Tittel for video embed',
        embed: {
          src:
            'https://videoportal.rcn.no/embed?id=e1e3c05b-ccf8-44d0-8ce3-0268b3c200b5',
          title: 'Video'
        }
      },
      '-size-half'
    )
  )
};
