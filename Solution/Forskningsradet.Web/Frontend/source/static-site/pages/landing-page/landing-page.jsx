/*
name: Landingsside
group: CMS-maler
*/
import React from 'react';
import Layout from '../../layout';

import ContentAreaPage from 'components/content-area-page';

import content from './landing-page.js';

const LandingPageMock = () => (
  <Layout>
    <ContentAreaPage {...content} />
  </Layout>
);

export default LandingPageMock;
