/*
name: Fritekst
*/
import React from 'react';
import Layout from '../../layout';

import content from './rich-text.js';

import ContentContainer from 'components/content-container';
import RichText from 'components/rich-text';

const RichTextPage = () => (
  <Layout>
    <ContentContainer>
      <RichText {...content} />
    </ContentContainer>
  </Layout>
);

export default RichTextPage;
