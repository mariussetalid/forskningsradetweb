/*
name: Kontakt oss
group: Informasjon
*/
import React from 'react';
import Layout from '../../layout';

import content from './contact.js';

import ContentAreaPage from 'components/content-area-page';

const Contact = () => (
  <Layout>
    <ContentAreaPage {...content} />
  </Layout>
);

export default Contact;
