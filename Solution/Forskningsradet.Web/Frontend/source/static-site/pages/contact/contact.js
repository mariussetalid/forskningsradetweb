import utils from '../../content-area-utils';

export default {
  pageHeader: {
    title: 'Kontakt oss',
    map: {
      googleMapsAPIKey: 'AIzaSyBbbBTY4NSmbRtGUyitGmUj2KEiW142Mqc',
      labels: {
        zoomIn: 'Zoom inn',
        zoomOut: 'Zoom out'
      },
      markers: [
        {
          latitude: 59.9134964,
          longitude: 10.6450097,
          name: 'Norges forskningsråd',
          popup: {
            title: 'Norges forskningsråd',
            address: utils.RichText(
              '<p>Hovedkontor</p><p>Drammensveien 288, 0283 Oslo</p>'
            ),
            link: {
              text: 'Veibeskrivelse',
              url: '#'
            }
          }
        }
      ]
    }
  },
  content: utils.ContentArea(
    utils.OfficeListBlock({
      links: [
        {
          id: '1',
          isActive: true,
          link: { text: 'Hovedkontor', url: '/contact' }
        },
        {
          id: '2',
          isActive: false,
          link: { text: 'Brusselkontor', url: '/contact?1' }
        },
        {
          id: '3',
          isActive: false,
          link: { text: 'Våre 13 regionalkontorer', url: '/contact?2' }
        }
      ],
      offices: utils.ContentArea(
        utils.OfficeBlock({
          title: 'Norges forskningsråd/Noregs forskingsråd - hovedkontor',
          info: [
            { title: 'Besøksadresse', text: 'Drammensveien 288' },
            {
              title: 'Postadresse',
              text: 'Postboks 564, 1327 Lysaker',
              link: { text: 'Vis veibeskrivelse', url: '#' }
            },
            {
              title: 'Telefon',
              link: { text: '22 03 70 00', url: '#' }
            },
            {
              title: 'E-post',
              link: { text: 'post@forskningsradet.no', url: '#' }
            },
            {
              title: 'Åpningstider',
              text: 'Mandag til fredag',
              moreText: '08:00 – 15:45'
            },
            {
              title: 'Mellom 15. mai - 14. september',
              text: 'Mandag til fredag',
              moreText: '08:00 – 15:00'
            },
            {
              title: 'Vakttelefon',
              text: 'Settes opp før hver søknads- og rapporteringsfrist på',
              link: { text: 'forskningsradet.no', url: '#' }
            },
            {
              title: 'Administrasjon nettsider',
              link: { text: 'webmaster@forskningsradet.no', url: '#' }
            },
            {
              title: 'Organisasjonsnummer',
              text: '970141669'
            }
          ]
        })
      )
    }),
    utils.InfoBlock(
      {
        title: 'Henvendelser om søknad og søknadsbehandling',
        text: utils.RichText(
          '<p>E-post:<a href="#">post@forskningsradet.no</a></p><p>Telefon: <a href="#">+47 22 03 70 00</a></p>'
        )
      },
      '-size-third'
    ),
    utils.InfoBlock(
      {
        title: 'Henvendelser om næringsliv og offentlig sektor',
        text: utils.RichText(
          '<p>E-post:<a href="#">post@forskningsradet.no</a></p><p>Telefon: <a href="#">+47 22 03 70 00</a></p>'
        )
      },
      '-size-third'
    ),
    utils.InfoBlock(
      {
        title: 'Andre henvendelser',
        text: utils.RichText(
          '<p>E-post:<a href="#">post@forskningsradet.no</a></p><p>Telefon: <a href="#">+47 22 03 70 00</a></p>'
        )
      },
      '-size-third'
    )
  )
};
