/*
name: Forside
group: CMS-maler
*/
import React from 'react';
import Layout from '../../layout';

import content from './frontpage.js';

import Frontpage from 'components/frontpage';

const FrontpagePage = () => (
  <Layout showBreadcrumbs={false}>
    <Frontpage {...content} />
  </Layout>
);

export default FrontpagePage;
