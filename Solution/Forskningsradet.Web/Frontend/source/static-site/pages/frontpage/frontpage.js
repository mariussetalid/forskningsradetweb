import utils from '../../content-area-utils';

export default {
  header: {
    theme: '-theme-dark-blue',
    title:
      'Senterordningen (SFF) gir forskningsmiljøene en unik  mulighet for samarbeid',
    text:
      'Vi investerer i forskning og innovasjon som bygger kunnskap for en bærekraftig framtid.',
    image: { src: '/static-site/assets/students.png', alt: 'students' },
    links: [
      { text: 'Les mer', url: '?1', isPrimary: true },
      { text: 'Se utlysningen', url: '?2' }
    ]
  },
  linkList: {
    items: [
      { text: 'Se alle utlysninger', url: '#' },
      { text: 'Hjelp og kontakt', url: '#' },
      { text: 'HVem kan søke', url: '#' },
      { text: 'Hvem kan søke', url: '#' },
      { text: 'Alle arrangementer', url: '#' }
    ]
  },
  content: utils.ContentArea(
    utils.CampaignBlockList({
      list: [
        {
          cta: { url: '#', text: 'Se forslagene' },
          image: { src: '/static-site/assets/students.png', alt: 'students' },
          title: 'Norges forslag til satsinger for FNs havforskningstiår'
        },
        {
          cta: { url: '#', text: 'Se forslagene' },
          image: {
            src: '/static-site/assets/smartphone.png',
            alt: 'students'
          },
          title:
            'Forskning på avanserte materialer er en prioritert satsing i regjeringens langtidsplan'
        }
      ]
    }),
    utils.Message(
      {
        text: { text: '<p>Et lorem <a href="#">ipsum</a> varsel globalt.' }
      },
      '-size-full-screen'
    ),
    utils.ArticleBlock(
      {
        title: { text: 'Nyhetssak med lenke på tittel', url: '#' },
        byline: { items: [{ text: 'Publisert 14.10.2018' }] },
        text:
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus scelerisque sem vitae faucibus viverra. Phasellus non augue lacus. Lorem ipsum dolor sit amet. Sem vitae faucibus viverra et non augue lacus. Lorem ipsum dolor sit amet.'
      },
      '-size-half'
    ),
    utils.ArticleBlock(
      {
        title: {
          text:
            'Nyhetssak med lengre tittel – kun én dato synes i byline, enten publisert eller sist oppdatert',
          url: '#'
        },
        byline: { items: [{ text: 'Sist oppdatert 14.05.2018' }] },
        text:
          'Scelerisque sem vitae faucibus viverra, phasellus non augue lacus. Lorem ipsum dolor sit amet. Consectetur adipiscing elit sem vitae faucibus viverra et non augue lacus. '
      },
      '-size-half'
    ),
    utils.StatsLinkList({
      heading: 'Hvordan blir investeringene fordelt',
      aboutImage: {
        src: '/static-site/assets/document-image.png',
        alt: 'et bilde'
      },
      seeMoreLink: {
        text: 'Se flere tall og fakta i Prosjektbanken',
        url: '#'
      },
      statsLinks: [
        {
          image: { src: '/static-site/assets/lightbulb.svg', alt: 'icon' },
          title: '3 006 mill',
          link: {
            url: '#',
            text: 'Til styrket konkurransekraft og innovasjonsevne'
          }
        },
        {
          image: { src: '/static-site/assets/iceblock.svg', alt: 'icon' },
          title: '4 720 mill',
          link: {
            url: '#',
            text: 'Til å møte de store samfunnsutfordringene'
          }
        },
        {
          image: { src: '/static-site/assets/people.svg', alt: 'icon' },
          title: '2 600 mill',
          link: {
            url: '#',
            text: 'Utvikle fagmiljøer av fremragende kvalitet'
          }
        }
      ]
    }),
    utils.PortfolioContent({
      title: 'Hvem kan søke om støtte',
      content: utils.ContentArea(
        utils.LinkWithText({
          link: { text: 'Offsentlig sektor', url: '#' },
          text:
            'Fornyelse og innovasjon i offentlig sektor gkær Norge mer effektivt og bedre rustet til å takle store samfunnsutfordringer.'
        }),
        utils.LinkWithText({
          link: { text: 'Næringsliv', url: '#' },
          text:
            'Innovasjon og kunnskapsbyggning i nøringslivet gjør Norges verdiskaping mer bærekraftig i framtiden.'
        }),
        utils.LinkWithText({
          link: { text: 'Forskningsinstituskjoner', url: '#' },
          text:
            'Økonomisk støtte til forskningsprosjekter hever nivået på rapportene og bidrar til verdifull kunnskap som kan deles med flere.'
        })
      )
    }),
    utils.PortfolioContent({
      title: 'Vi støtter forsking innen',
      content: utils.ContentArea(
        utils.LinkLists({
          nestedLinks: [
            {
              items: [
                {
                  text: 'Link somewhere',
                  url: '/en/'
                },
                {
                  text: 'Link somewhere',
                  url: '/en/'
                },
                {
                  text: 'Link somewhere',
                  url: '/en/'
                },
                {
                  text: 'Link somewhere',
                  url: '/en/'
                },
                {
                  text: 'Link somewhere',
                  url: '/en/'
                },
                {
                  text: 'Link somewhere',
                  url: '/en/'
                },
                {
                  text: 'Link somewhere',
                  url: '/en/'
                },
                {
                  text: 'Link somewhere',
                  url: '/en/'
                },
                {
                  text: 'Link somewhere',
                  url: '/en/'
                },
                {
                  text: 'Link somewhere',
                  url: '/en/'
                }
              ]
            },
            {
              title: 'Global Link List Block',
              items: [
                {
                  text: 'Somewhere decisive',
                  url: '/en/'
                },
                {
                  text: 'Oddly placed',
                  url: '/en/neweventlistpage/'
                },
                {
                  text: 'Continuous feriocity',
                  url: '/en/framtidigevent/'
                },
                {
                  text: 'Tender niblings',
                  url: '/en/newspage/'
                },
                {
                  text: 'Somewhere',
                  url: 'mailto:Some@where.com'
                }
              ]
            },
            {
              title: 'Global Link List Block',
              items: [
                {
                  text: 'Somewhere decisive',
                  url: '/en/'
                },
                {
                  text: 'Oddly placed',
                  url: '/en/neweventlistpage/'
                },
                {
                  text: 'Continuous feriocity',
                  url: '/en/framtidigevent/'
                },
                {
                  text: 'Tender niblings',
                  url: '/en/newspage/'
                },
                {
                  text: 'Somewhere',
                  url: 'mailto:Some@where.com'
                }
              ]
            }
          ]
        })
      )
    }),
    utils.PageLinksBlock({
      title: 'Målgrupper og tema',
      text:
        'Lorem ipsum dolor sit sortering. Phasellus non agueue lacus quis nostrud magna.',
      highlightedLinks: utils.ContentArea(
        utils.LinkWithText({
          link: { text: 'Forskningsinstitusjon', url: '#' },
          text: 'Sem vitae faucibus viverra. Phasellus non augue lacus. '
        }),
        utils.LinkWithText({
          link: { text: 'Næringsliv', url: '#' },
          text: 'Sem vitae faucibus viverra. Phasellus non augue lacus. '
        }),
        utils.LinkWithText({
          link: { text: 'Offentlig sektor', url: '#' },
          text: 'Sem vitae faucibus viverra. Phasellus non augue lacus. '
        })
      ),
      linkLists: [
        {
          id: '1',
          links: utils.ContentArea(
            utils.LinkWithText({
              link: { text: 'Muliggjørende teknologi', url: '#' },
              text: 'Sem vitae faucibus viverra. Phasellus non augue lacus. '
            }),
            utils.LinkWithText({
              link: { text: 'Verdiskapning i næringslivet', url: '#' },
              text: 'Sem vitae faucibus viverra. Phasellus non augue lacus. '
            }),
            utils.LinkWithText({
              link: { text: 'Nyskapning og teknologioverføring', url: '#' },
              text: 'Sem vitae faucibus viverra. Phasellus non augue lacus. '
            }),
            utils.LinkWithText({
              link: { text: 'Hav', url: '#' },
              text: 'Sem vitae faucibus viverra. Phasellus non augue lacus. '
            }),
            utils.LinkWithText({
              link: { text: 'Energi og lavutslipp', url: '#' },
              text: 'Sem vitae faucibus viverra. Phasellus non augue lacus. '
            }),
            utils.LinkWithText({
              link: { text: 'Petroleum', url: '#' },
              text: 'Sem vitae faucibus viverra. Phasellus non augue lacus. '
            }),
            utils.LinkWithText({
              link: { text: 'Klima og polar', url: '#' },
              text: 'Sem vitae faucibus viverra. Phasellus non augue lacus. '
            }),
            utils.LinkWithText({
              link: { text: 'Landbaserte bioressurser og mat', url: '#' },
              text: 'Sem vitae faucibus viverra. Phasellus non augue lacus. '
            })
          )
        },
        {
          id: '2',
          links: utils.ContentArea(
            utils.LinkWithText({
              link: { text: 'Muliggjørende teknologi', url: '#' },
              text: 'Sem vitae faucibus viverra. Phasellus non augue lacus. '
            }),
            utils.LinkWithText({
              link: { text: 'Verdiskapning i næringslivet', url: '#' },
              text: 'Sem vitae faucibus viverra. Phasellus non augue lacus. '
            }),
            utils.LinkWithText({
              link: { text: 'Nyskapning og teknologioverføring', url: '#' },
              text: 'Sem vitae faucibus viverra. Phasellus non augue lacus. '
            }),
            utils.LinkWithText({
              link: { text: 'Hav', url: '#' },
              text: 'Sem vitae faucibus viverra. Phasellus non augue lacus. '
            }),
            utils.LinkWithText({
              link: { text: 'Energi og lavutslipp', url: '#' },
              text: 'Sem vitae faucibus viverra. Phasellus non augue lacus. '
            }),
            utils.LinkWithText({
              link: { text: 'Petroleum', url: '#' },
              text: 'Sem vitae faucibus viverra. Phasellus non augue lacus. '
            }),
            utils.LinkWithText({
              link: { text: 'Klima og polar', url: '#' },
              text: 'Sem vitae faucibus viverra. Phasellus non augue lacus. '
            }),
            utils.LinkWithText({
              link: { text: 'Landbaserte bioressurser og mat', url: '#' },
              text: 'Sem vitae faucibus viverra. Phasellus non augue lacus. '
            })
          )
        }
      ]
    }),
    utils.FutureEvents({
      title: 'Kommende arrangementer',
      text:
        'Vi arrangerer mange konferanser, informasjonsmøter og seminarer. På våre arrangementssider kan du melde deg på, ta del i livesendinger og finne presentasjonsmateriale.',
      link: {
        url: '/',
        text: 'Se alle arrangementer'
      },
      items: [
        {
          id: '1',
          isPastDate: false,
          title: 'Hvordan søke ERC Advanced Grants',
          url: '/test',
          dates: [{ day: '26', month: 'jun' }],
          metadata: {
            items: [
              { label: 'Type', text: 'Kurs' },
              { label: 'Varighet', text: '6 timer' },
              { label: 'Sted', text: 'Trondheim' }
            ]
          },
          tags: {
            items: [
              { text: 'Humanoria', url: '/?1' },
              { text: 'Matematikk/Naturvitenskap', url: '/?2' }
            ]
          },
          text:
            'Kurset er avlyst. Har du planer om å skrive en prosjektsøknad til Horisont 2020? Eller er du en EU-rådgiver som trenger å kjenne til hvordan man skriver gode søknader til Horisont 2020?'
        },
        {
          id: '2',
          isPastDate: false,
          title: 'Drift og økonomi i et EU-prosjekt',
          url: '/test',
          dates: [{ day: '26-27', month: 'jun' }],
          metadata: {
            items: [
              { label: 'Type', text: 'Kurs' },
              { label: 'Varighet', text: '1,5 dager' },
              { label: 'Sted', text: 'Bergen' }
            ]
          },
          text:
            'På grunn av korona-situasjonen erstattes vårens planlagte SkatteFUNN informasjonsmøter og prosjektverksted med digitale kurs. Kursene vil bli avholdt i alle fylker i hele landet. Bedrifter som deltar på kurs kan dessuten booke individuelle, digitale møter med våre rådgivere og få råd om hvordan du setter opp et godt prosjekt. Arrangementene er gratis, men du må melde deg på for å delta.',
          tags: {
            items: [
              { text: 'Tjenesteytende næringer', url: '/?1' },
              { text: 'IKT', url: '/?2' },
              { text: 'Samfunnsvitenskap', url: '/?3' },
              { text: 'Tjenesteytende næringer', url: '/?4' },
              { text: 'IKT', url: '/?5' },
              { text: 'Samfunnsvitenskap', url: '/?6' },
              { text: 'Tjenesteytende næringer', url: '/?7' },
              { text: 'IKT', url: '/?8' },
              { text: 'Samfunnsvitenskap', url: '/?9' }
            ]
          }
        },
        {
          id: '3',
          isPastDate: false,
          title:
            'Forskning for verdiskaping - Kunnskapsgrunnlagskonferansen 2019',
          subtitle: 'Med en undertittel som krever mer plass til tittelområdet',
          url: '/test',
          dates: [{ day: '31', month: 'jul' }, { day: '01', month: 'aug' }],
          metadata: {
            items: [
              { label: 'Type', text: 'Seminar/Webinar' },
              { label: 'Varighet', text: '2 dager' },
              { label: 'Sted', text: 'Oslo' }
            ]
          },
          tags: {
            items: [
              { text: 'Klima/Miljø' },
              { text: 'Petroleum/Energi' },
              { text: 'Globalisering/Utvikling' }
            ]
          }
        }
      ]
    })
  )
};
