import utils from '../../content-area-utils';

export default {
  title: 'JPI FACCE - ERA-NET CoFound FOSC',
  isInternational: true,
  header: {
    accordion: {
      guid: '92837n4tv09w8erugnvp20nu',
      collapseLabel: 'Vis mindre',
      expandLabel: 'Vis mer'
    },
    byline: {
      items: [
        { text: 'Sist oppdatert 20.mai 2918' },
        { text: 'Publisert 13. april 2018' }
      ]
    },
    download: {
      url: '/',
      text: 'Last ned'
    }
  },
  message: {
    text: {
      text:
        '<b>Viktig/kritisk overordnet varsel.</b> Denne utlysningen vil oppdateres fortløpende fram til seks uker før søknadsfrist. Når utlysningen er aktivert vil du ha mulighet til å opprette og sende inn søknad.'
    }
  },
  downloadLinks: [{ text: 'Last ned utlysning', url: '/?pdf' }],
  share: {
    openButtonText: 'Del utlysningen',
    shareContent: {
      items: [
        { url: '#', text: 'Mail', icon: 'mail' },
        { url: '#', text: 'Twitter', icon: 'twitter' },
        { url: '#', text: 'Facebook', icon: 'facebook' },
        { url: '#', text: 'LinkedIn', icon: 'linkedin' }
      ]
    }
  },
  contact: {
    title: 'Kontaktperson',
    details: [
      { text: 'Thorbjørn Gilberg' },
      { text: 'Seniorrådgiver - Bioøkonomi' },
      {
        url: 'mailto:thgi@forskningsradet.no',
        text: 'thgi@forskningsradet.no'
      }
    ]
  },
  contactLabel: 'Kontakt',
  metadataLeft: {
    items: [
      {
        label: 'Utlysningstype',
        links: [
          {
            url: '/',
            text: 'Internasjonal utlysning'
          }
        ]
      },
      {
        label: 'Søknadsfrist trinn 1',
        text: '24. mars 2020, klokken 13:00 CEST'
      },
      {
        label: 'Søknadsfrist trinn 2',
        text: '20. juni 2020, 13:00 CEST'
      }
    ]
  },
  metadataRight: {
    items: [
      {
        label: 'Aktuelt tema',
        links: [
          {
            url: '/',
            text: 'Bioøkonomi'
          }
        ]
      }
    ]
  },
  descriptionTitle: 'Om utlysingen',
  descriptionIngress:
    'Som prosjektleder for et unge forskertalenter-prosjekt skal du få erfaring med å lede et forskningsprosjekt og å veilede doktorgrads- og/eller postdoktorstipendiater.',
  descriptionText: utils.RichText(
    '<p>Søknadstypen unge forskertalenter er beregnet på forskere som er på et tidlig stadium i karrieren og som har vist evne til å utføre forskning av høy vitenskapelig kvalitet. Ved vurderingen av søknadene vil det bli lagt vekt på prosjektleders evne til å arbeide selvstendig, faglige modenhet og forskerpotensial, dokumentert gjennom for eksempel publikasjoner og mobilitet. Som prosjektleder for et unge forskertalenter-prosjekt skal du få erfaring med å lede et forskningsprosjekt og å veilede doktorgrads- og/eller postdoktorstipendiater.</p><p>Mer informasjon og opprettelse av søknad på sidene for JPI FACCE programmet:</p><p><a href="#">JPI FACCE</a></p><p><a href="#">ERA-NET CoFound FOSC</a></p>'
  )
};
