/*
name: Internasjonal utlysning
group: Internasjonale utlysninger
 */

import React from 'react';
import Layout from '../../layout';

import content from './international-proposal.js';

import ProposalPage from 'components/proposal-page';

const InternationalProposal = () => (
  <Layout>
    <ProposalPage {...content} />
  </Layout>
);

export default InternationalProposal;
