import utils from '../../content-area-utils';

export default {
  title: 'Nyhetsbrev',
  richText: utils.RichText(
    '<p>Discere iracundia contentiones cu mei. Ut tacimates democritum usu, ea vim spesifisert i utlysning. Omnium utamur mediocritatem duo eu, ei simul perfecto has. Tempor tamquam scaevola sit et, pri veri eruditi in.</p><p>Husk at * betyr at utfylling er påkrevd.</p>'
  ),
  iframe: {
    src: '/static-site/assets/skjema.html',
    title: 'Nyhetsbrev påmelding'
  },
  sidebar: utils.ContentArea(
    utils.InfoBlock({
      title: 'Newsletter in english',
      text: utils.RichText(
        '<p>Lorem ipsum dolor sit amet, choro civibus eu sed, graeco evertitur sed at, invidunt voluptaria mea an. Set commodo vocibus at mea, augue inpercipit ne vis at erat soluta audiam sit:</p>'
      ),
      url: '/',
      editorTheme: '-theme-blue'
    })
  ),
  footer: {}
};
