/*
group: Nyhetsbrev
name: Apsis nyhetsbrev påmelding
*/

import React from 'react';
import Layout from '../../layout';

import ResponsiveIframePage from 'components/responsive-iframe-page';

import content from './apsis-newsletter.js';

const ApsisNewsletter = () => (
  <Layout>
    <ResponsiveIframePage {...content} />
  </Layout>
);

export default ApsisNewsletter;
