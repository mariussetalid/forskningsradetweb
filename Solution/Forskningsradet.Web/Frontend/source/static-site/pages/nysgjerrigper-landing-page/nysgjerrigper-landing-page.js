export default {
  title: 'Nysgjerrigper_metoden',
  ingress: 'Nysgjerrigpermetoden er en oppskrift på forskning, laget for barn.',
  link: { text: 'Lærerveiledning', url: '#' },
  menu: [
    { text: 'Dette lurer vi på', isActive: false },
    { text: 'Hvorfor er det slik?' },
    { text: 'Legg en plan' },
    { text: 'Hent opplysninger' },
    { text: 'Dette har vi funnet ut' },
    { text: 'Fortell til andre' }
  ],
  stepsContent: {
    close: {
      textDesktop: 'Lukk',
      textMobile: 'Tilbake til nysgjerrigpermetoden'
    },
    steps: [
      {
        videoBlock: {
          video: {
            urlOrEmbed:
              'https://www.youtube.com/watch?v=cwAdKxJmoN0?rel=0&amp;showinfo=0'
          },
          step: 'Steg 1',
          title: 'Dette lurer vi på',
          text:
            'Først handler det om å finne noe dere kan undersøke. Det er lov å lure på alt mulig! Hør hvordan Anna og hennes klasse fant sin problemtilling?',
          playText: 'Spill film!'
        }
      },
      {
        videoBlock: {
          video: {
            urlOrEmbed:
              'https://www.youtube.com/watch?v=cwAdKxJmoN0?rel=0&amp;showinfo=0'
          },
          step: 'Steg 2',
          title: 'Dette lurer vi på',
          text:
            'Først handler det om å finne noe dere kan undersøke. Det er lov å lure på alt mulig! Hør hvordan Anna og hennes klasse fant sin problemtilling?',
          playText: 'Spill film!'
        },
        stepsBlock: {
          pages: [
            {
              title: 'Kom i gang da',
              text:
                'Dette er Jonny. Han har mange ideer og kan hjelpe deg igang med å finne spørsmål.',
              buttonText: 'Få hjelp av Jonny',
              image: {
                alt: 'Space',
                src: '/static-site/assets/example-gif.gif'
              }
            },
            {
              title: '– Hvorfor liker edderkopper  så godt i kjellere?',
              buttonText: 'Få et tip til',
              image: {
                alt: 'Space',
                src: '/static-site/assets/jonny-2.png'
              }
            },
            {
              title: '–Hvorfor liker edderkopper seg så godt i kjellere?',
              buttonText: 'Få et tip til',
              image: {
                alt: 'Space',
                src: '/static-site/assets/vr-glasses.png'
              }
            },
            {
              title: 'Hvorfor liker edderkopper seg så godt i kjellere?',
              buttonText: 'Få et tip til',
              image: {
                alt: 'Space',
                src: '/static-site/assets/tto-lederne.png'
              }
            },
            {
              title: 'Hvorfor liker edderkopper seg godt i kjellere?',
              buttonText: 'Få et tip til',
              image: {
                alt: 'Space',
                src: '/static-site/assets/students.png'
              }
            },
            {
              title: '–Hvorfor liker edderkopper seg i kjellere?',
              buttonText: 'Få et tip til',
              image: {
                alt: 'Space',
                src: '/static-site/assets/document-image.png'
              }
            },
            {
              title: '– Hvorfor liker edderkopper seg  i kjellere?',
              buttonText: 'Tilbake til steg 1',
              image: {
                alt: 'Space',
                src: '/static-site/assets/smartphone.png'
              }
            }
          ]
        },
        quotesBlock: {
          title: 'Bli inspirert av andres problemstillinger!',
          quotes: []
        }
      },
      {
        videoBlock: {
          video: {
            urlOrEmbed:
              'https://www.youtube.com/watch?v=cwAdKxJmoN0?rel=0&amp;showinfo=0'
          },
          step: 'Steg 2',
          title: 'Dette lurer vi på',
          text:
            'Først handler det om å finne noe dere kan undersøke. Det er lov å lure på alt mulig! Hør hvordan Anna og hennes klasse fant sin problemtilling?',
          playText: 'Spill film!'
        },
        stepsBlock: {
          pages: [
            {
              title: 'Kom i gang da',
              text:
                'Dette er Jonny. Han har mange ideer og kan hjelpe deg igang med å finne spørsmål.',
              buttonText: 'Få hjelp av Jonny',
              image: {
                alt: 'Space',
                src: '/static-site/assets/example-gif.gif'
              }
            },
            {
              title: '– Hvorfor liker edderkopper  så godt i kjellere?',
              buttonText: 'Få et tip til',
              image: {
                alt: 'Space',
                src: '/static-site/assets/jonny-2.png'
              }
            }
          ]
        },
        quotesBlock: {
          title: 'Bli inspirert av andres problemstillinger!',
          quotes: [
            {
              text:
                'Vi var en gjeng som dro til universitet som ligger i nærheten av vår skole. Der hadde møtte vi en språkforsker som ga oss tips om hva de gjør og hvordan de tenker når de skal finne hypoteser.',
              quotee: 'Klasse 1b, Bekekstua skole',
              addQuoteDash: true,
              image: {
                alt: 'jonny-boy',
                src: '/static-site/assets/vr-glasses.png'
              }
            },
            {
              text:
                'Vi intervjuet en annen klasse på skolen og stilte de spørsmålet: Hvorfor tror dere at bananer blir brune? Da fikk vi mange bra hypoteser.',
              quotee: 'Klasse 2A, xx skole',
              addQuoteDash: true,
              image: {
                alt: 'jonny-boy',
                src: '/static-site/assets/tto-lederne.png'
              }
            },
            {
              text:
                'Vi var en gjeng som dro til universitet som ligger i nærheten av vår skole. Der hadde møtte vi en språkforsker som ga oss tips om hva de gjør og hvordan de tenker når de skal finne hypoteser.',
              quotee: 'Klasse 3b, Bekekstua skole',
              image: {
                alt: 'jonny-boy',
                src: '/static-site/assets/smartphone.png'
              }
            },
            {
              text:
                'Vi intervjuet en annen klasse på skolen og stilte de spørsmålet: Hvorfor tror dere at bananer blir brune? Da fikk vi mange bra hypoteser.',
              quotee: 'Klasse 6A, xx skole',
              addQuoteDash: true,
              image: {
                alt: 'jonny-boy',
                src: '/static-site/assets/document-image.png'
              }
            }
          ]
        }
      },
      {
        videoBlock: {
          video: {
            urlOrEmbed:
              'https://www.youtube.com/watch?v=cwAdKxJmoN0?rel=0&amp;showinfo=0'
          },
          step: 'Steg 2',
          title: 'Dette lurer vi på',
          text:
            'Først handler det om å finne noe dere kan undersøke. Det er lov å lure på alt mulig! Hør hvordan Anna og hennes klasse fant sin problemtilling?',
          playText: 'Spill film!'
        },
        quotesBlock: {
          title: 'Bli inspirert av andres problemstillinger!',
          quotes: [
            {
              text:
                'Vi var en gjeng som dro til universitet som ligger i nærheten av vår skole. Der hadde møtte vi en språkforsker som ga oss tips om hva de gjør og hvordan de tenker når de skal finne hypoteser.',
              quotee: 'Klasse 1b, Bekekstua skole',
              addQuoteDash: true,
              image: {
                alt: 'jonny-boy',
                src: '/static-site/assets/vr-glasses.png'
              }
            },
            {
              text:
                'Vi intervjuet en annen klasse på skolen og stilte de spørsmålet: Hvorfor tror dere at bananer blir brune? Da fikk vi mange bra hypoteser.',
              quotee: 'Klasse 2A, xx skole',
              image: {
                alt: 'jonny-boy',
                src: '/static-site/assets/tto-lederne.png'
              }
            },
            {
              text:
                'Vi var en gjeng som dro til universitet som ligger i nærheten av vår skole. Der hadde møtte vi en språkforsker som ga oss tips om hva de gjør og hvordan de tenker når de skal finne hypoteser.',
              quotee: 'Klasse 3b, Bekekstua skole',
              addQuoteDash: true,
              image: {
                alt: 'jonny-boy',
                src: '/static-site/assets/smartphone.png'
              }
            },
            {
              text:
                'Vi intervjuet en annen klasse på skolen og stilte de spørsmålet: Hvorfor tror dere at bananer blir brune? Da fikk vi mange bra hypoteser.',
              quotee: 'Klasse 4A, xx skole',
              image: {
                alt: 'jonny-boy',
                src: '/static-site/assets/document-image.png'
              }
            }
          ]
        },
        stepsBlock: {
          pages: [
            {
              title: 'Kom i gang',
              text:
                'Dette er Jonny. Han har mange ideer og kan hjelpe deg igang med å finne spørsmål.',
              buttonText: 'Få hjelp av Jonny',
              image: {
                alt: 'Space',
                src: '/static-site/assets/example-gif.gif'
              }
            },
            {
              title: '– Hvorfor liker edderkopper seg så godt i kjellere?',
              buttonText: 'Få et tips til',
              image: {
                alt: 'Space',
                src: '/static-site/assets/jonny-2.png'
              }
            }
          ]
        }
      },
      {
        videoBlock: {
          video: {
            urlOrEmbed:
              'https://www.youtube.com/watch?v=cwAdKxJmoN0?rel=0&amp;showinfo=0'
          },
          step: 'Steg 2',
          title: 'Dette lurer vi på',
          text:
            'Først handler det om å finne noe dere kan undersøke. Det er lov å lure på alt mulig! Hør hvordan Anna og hennes klasse fant sin problemtilling?',
          playText: 'Spill film!'
        },
        quotesBlock: {
          title: 'Bli inspirert av andres problemstillinger!',
          quotes: [
            {
              text:
                'Vi var en gjeng som dro til universitet som ligger i nærheten av vår skole. Der hadde møtte vi en språkforsker som ga oss tips om hva de gjør og hvordan de tenker når de skal finne hypoteser.',
              quotee: 'Klasse 1b, Bekekstua skole',
              addQuoteDash: true,
              image: {
                alt: 'jonny-boy',
                src: '/static-site/assets/vr-glasses.png'
              }
            },
            {
              text:
                'Vi intervjuet en annen klasse på skolen og stilte de spørsmålet: Hvorfor tror dere at bananer blir brune? Da fikk vi mange bra hypoteser.',
              quotee: 'Klasse 2A, xx skole',
              addQuoteDash: true,
              image: {
                alt: 'jonny-boy',
                src: '/static-site/assets/tto-lederne.png'
              }
            },
            {
              text:
                'Vi var en gjeng som dro til universitet som ligger i nærheten av vår skole. Der hadde møtte vi en språkforsker som ga oss tips om hva de gjør og hvordan de tenker når de skal finne hypoteser.',
              quotee: 'Klasse 3b, Bekekstua skole',
              addQuoteDash: true,
              image: {
                alt: 'jonny-boy',
                src: '/static-site/assets/smartphone.png'
              }
            },
            {
              text:
                'Vi intervjuet en annen klasse på skolen og stilte de spørsmålet: Hvorfor tror dere at bananer blir brune? Da fikk vi mange bra hypoteser.',
              quotee: 'Klasse 4A, xx skole',
              image: {
                alt: 'jonny-boy',
                src: '/static-site/assets/document-image.png'
              }
            }
          ]
        },
        stepsBlock: {
          pages: [
            {
              title: 'Kom i gang',
              text:
                'Dette er Jonny. Han har mange ideer og kan hjelpe deg igang med å finne spørsmål.',
              buttonText: 'Få hjelp av Jonny',
              image: {
                alt: 'Space',
                src: '/static-site/assets/example-gif.gif'
              }
            },
            {
              title: '– Hvorfor liker edderkopper seg så godt i kjellere?',
              buttonText: 'Få et tips til',
              image: {
                alt: 'Space',
                src: '/static-site/assets/jonny-2.png'
              }
            }
          ]
        }
      },
      {
        videoBlock: {
          video: {
            urlOrEmbed:
              'https://www.youtube.com/watch?v=cwAdKxJmoN0?rel=0&amp;showinfo=0'
          },
          step: 'Steg 2',
          title: 'Dette lurer vi på',
          text:
            'Først handler det om å finne noe dere kan undersøke. Det er lov å lure på alt mulig! Hør hvordan Anna og hennes klasse fant sin problemtilling?',
          playText: 'Spill film!'
        },
        stepsBlock: {
          pages: [
            {
              title: 'Kom i gang',
              text:
                'Dette er Jonny. Han har mange ideer og kan hjelpe deg igang med å finne spørsmål.',
              buttonText: 'Få hjelp av Jonny',
              image: {
                alt: 'Space',
                src: '/static-site/assets/example-gif.gif'
              }
            },
            {
              title: '– Hvorfor liker edderkopper seg så godt i kjellere?',
              buttonText: 'Få et tips til',
              image: {
                alt: 'Space',
                src: '/static-site/assets/jonny-2.png'
              }
            }
          ]
        },
        quotesBlock: {
          title: 'Bli inspirert av andres problemstillinger!',
          quotes: [
            {
              text:
                'Vi var en gjeng som dro til universitet som ligger i nærheten av vår skole. Der hadde møtte vi en språkforsker som ga oss tips om hva de gjør og hvordan de tenker når de skal finne hypoteser.',
              quotee: 'Klasse 1b, Bekekstua skole',
              addQuoteDash: true,
              image: {
                alt: 'jonny-boy',
                src: '/static-site/assets/vr-glasses.png'
              }
            },
            {
              text:
                'Vi intervjuet en annen klasse på skolen og stilte de spørsmålet: Hvorfor tror dere at bananer blir brune? Da fikk vi mange bra hypoteser.',
              quotee: 'Klasse 2A, xx skole',
              addQuoteDash: true,
              image: {
                alt: 'jonny-boy',
                src: '/static-site/assets/tto-lederne.png'
              }
            },
            {
              text:
                'Vi var en gjeng som dro til universitet som ligger i nærheten av vår skole. Der hadde møtte vi en språkforsker som ga oss tips om hva de gjør og hvordan de tenker når de skal finne hypoteser.',
              quotee: 'Klasse 3b, Bekekstua skole',
              image: {
                alt: 'jonny-boy',
                src: '/static-site/assets/smartphone.png'
              }
            },
            {
              text:
                'Vi intervjuet en annen klasse på skolen og stilte de spørsmålet: Hvorfor tror dere at bananer blir brune? Da fikk vi mange bra hypoteser.',
              quotee: 'Klasse 4A, xx skole',
              addQuoteDash: true,
              image: {
                alt: 'jonny-boy',
                src: '/static-site/assets/document-image.png'
              }
            }
          ]
        }
      },
      {
        videoBlock: {
          video: {
            urlOrEmbed:
              'https://www.youtube.com/watch?v=cwAdKxJmoN0?rel=0&amp;showinfo=0'
          },
          step: 'Steg 6',
          title: 'Dette lurer vi på',
          text:
            'Først handler det om å finne noe dere kan undersøke. Det er lov å lure på alt mulig! Hør hvordan Anna og hennes klasse fant sin problemtilling?',
          playText: 'Spill film!'
        },
        stepsBlock: {
          pages: [
            {
              title: 'Kom i gang',
              text:
                'Dette er Jonny. Han har mange ideer og kan hjelpe deg igang med å finne spørsmål.',
              buttonText: 'Få hjelp av Jonny',
              image: {
                alt: 'Space',
                src: '/static-site/assets/example-gif.gif'
              }
            },
            {
              title: '– Hvorfor liker edderkopper seg så godt i kjellere?',
              buttonText: 'Få et tips til',
              image: {
                alt: 'Space',
                src: '/static-site/assets/jonny-2.png'
              }
            }
          ]
        },
        quotesBlock: {
          title: 'Bli inspirert av andres problemstillinger!',
          quotes: [
            {
              text:
                'Vi var en gjeng som dro til universitet som ligger i nærheten av vår skole. Der hadde møtte vi en språkforsker som ga oss tips om hva de gjør og hvordan de tenker når de skal finne hypoteser.',
              quotee: 'Klasse 1b, Bekekstua skole',
              addQuoteDash: true,
              image: {
                alt: 'jonny-boy',
                src: '/static-site/assets/vr-glasses.png'
              }
            },
            {
              text:
                'Vi intervjuet en annen klasse på skolen og stilte de spørsmålet: Hvorfor tror dere at bananer blir brune? Da fikk vi mange bra hypoteser.',
              quotee: 'Klasse 2A, xx skole',
              image: {
                alt: 'jonny-boy',
                src: '/static-site/assets/tto-lederne.png'
              }
            },
            {
              text:
                'Vi var en gjeng som dro til universitet som ligger i nærheten av vår skole. Der hadde møtte vi en språkforsker som ga oss tips om hva de gjør og hvordan de tenker når de skal finne hypoteser.',
              quotee: 'Klasse 3b, Bekekstua skole',
              addQuoteDash: true,
              image: {
                alt: 'jonny-boy',
                src: '/static-site/assets/smartphone.png'
              }
            },
            {
              text:
                'Vi intervjuet en annen klasse på skolen og stilte de spørsmålet: Hvorfor tror dere at bananer blir brune? Da fikk vi mange bra hypoteser.',
              quotee: 'Klasse 4A, xx skole',
              addQuoteDash: true,
              image: {
                alt: 'jonny-boy',
                src: '/static-site/assets/document-image.png'
              }
            }
          ]
        }
      }
    ]
  }
};
