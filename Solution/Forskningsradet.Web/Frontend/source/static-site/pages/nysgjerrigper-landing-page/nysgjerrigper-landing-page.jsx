/*
group: Nysgjerriper
name: Nysgjerrigper Landingsside
*/

import React from 'react';
import Layout from '../../layout';
import content from './nysgjerrigper-landing-page.js';

import NPMethod from 'components/nysgjerrigper/np-method';

const NysgjerrigperLandingPage = () => (
  <Layout showFooter={false}>
    <NPMethod {...content} />
  </Layout>
);

export default NysgjerrigperLandingPage;
