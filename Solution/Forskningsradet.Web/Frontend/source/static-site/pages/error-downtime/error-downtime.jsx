/*
name: Nedetid
*/

import React from 'react';
import Layout from '../../layout';

import content from './error-downtime.js';

import ErrorPage from 'components/error-page';

const ErrorDowntime = () => (
  <Layout>
    <ErrorPage {...content} />
  </Layout>
);

export default ErrorDowntime;
