import React from 'react';
import Button from 'components/button';
import utils from '../../content-area-utils';
import articleBlock from 'mock/components/article-block';
import articleHeader from 'mock/components/article-header';
import button from 'mock/components/button';
import campaignBlock from 'mock/components/campaign-block';
import checkboxGroup from 'mock/components/checkbox-group';
import contactBlock from 'mock/components/contact-block';
import contactList from 'mock/components/contact-list';
import downloadList from 'mock/components/download-list';
import eventImage from 'mock/components/event-image';
import textInput from 'mock/components/text-input';
import frontpageHeader from 'mock/components/frontpage-header';
import imageWithLink from 'mock/components/image-with-link';
import infoBlock from 'mock/components/info-block';
import linkList from 'mock/components/link-list';
import nestedLink from 'mock/components/nested-link';
import linkLists from 'mock/components/link-lists';
import linkWithText from 'mock/components/link-with-text';
import mediaBlock from 'mock/components/media-block';
import message from 'mock/components/message';
import metadata from 'mock/components/metadata';
import pageHeader from 'mock/components/page-header';
import personBlock from 'mock/components/person-block';
import portfolioContent from 'mock/components/portfolio-content';
import process from 'mock/components/process';
import relatedArticles from 'mock/components/related-articles';
import relatedDates from 'mock/components/related-dates';
import relatedPublications from 'mock/components/related-publications';
import searchResult from 'mock/components/search-result';
import accordion from 'mock/components/accordion';
import accordionBlock from 'mock/components/accordion-block';
import accordionWithContentArea from 'mock/components/accordion-with-content-area';
import publication from 'mock/components/publication';
import event from 'mock/components/event';
import dateCard from 'mock/components/date-card';
import optionsModal from 'mock/components/options-modal';

import dataInterface from 'mock/data-interface';

export default [
  {
    name: 'Accordion',
    epiName: undefined,
    description: 'Is used for all animated dropdown effects',
    versions: [
      {
        componentList: [
          utils.Accordion(
            accordion(
              {
                children: ({ Collapse, Button: ExpandButton }) => (
                  <div>
                    <ExpandButton theme={Button.themes.fill} />
                    <Collapse>hei</Collapse>
                  </div>
                )
              },
              1
            ),
            '-size-half'
          )
        ]
      }
    ]
  },
  {
    name: 'AccordionBlock',
    epiName: 'Faktaboksblokk',
    description:
      'A dropdown containing a RichText field. Is used on news- and article pages in main content and sidebar.',
    versions: [
      {
        componentList: [
          utils.AccordionBlock(accordionBlock({}, 1), '-size-half'),
          utils.AccordionBlock(accordionBlock({}, 2), '-size-half'),
          utils.AccordionBlock(accordionBlock({}, 3)),
          utils.AccordionBlock(accordionBlock({}, 4))
        ]
      }
    ]
  },
  {
    name: 'AccordionWithContentArea',
    epiName: undefined,
    description:
      'Is used in AccordionWithContentAreaList which exists on Eventpage/Arrangementsside - schedule/program and Proposalpage/Utlysningsside - content/blokkområde',
    versions: [
      {
        componentList: [
          utils.AccordionWithContentArea(
            accordionWithContentArea({ iconWarning: undefined }, 1)
          ),
          utils.AccordionWithContentArea(
            accordionWithContentArea(
              { content: utils.ContentArea(utils.Message(message({}, 4))) },
              2
            )
          ),
          utils.AccordionWithContentArea(accordionWithContentArea({}, 3))
        ]
      }
    ]
  },
  {
    name: 'ArticleBlock',
    epiName: 'Kategori',
    description: "Used in CategoryListPage (not sure if it's still in use)",
    versions: [
      {
        componentList: [
          utils.ArticleBlock(articleBlock({ byline: undefined }, 1)),
          utils.ArticleBlock(articleBlock({ byline: undefined }, 2)),
          utils.ArticleBlock(articleBlock({ byline: undefined }, 3)),
          utils.ArticleBlock(articleBlock({ byline: undefined }, 4)),
          utils.ArticleBlock(
            articleBlock({ linkTags: undefined, byline: undefined }, 5)
          ),
          utils.ArticleBlock(articleBlock({ byline: undefined }, 6))
        ]
      }
    ]
  },
  {
    name: 'ArticleHeader',
    description:
      'Can be used at the top of ArticlePage ProposalPage and LargeDocumentsPage',
    versions: [
      {
        componentList: [
          utils.ArticleHeader(
            articleHeader(
              { download: { url: '#', text: 'Last ned', isExternal: true } },
              1
            )
          )
        ]
      }
    ]
  },
  {
    name: 'Button',
    epiName: 'Knapp',
    versions: [
      {
        name: 'Fill',
        componentList: [utils.Button(button({}, 1))]
      },
      {
        name: 'Fill Big',
        componentList: [
          utils.Button(button({ theme: ['-theme-fill', '-theme-big'] }, 1))
        ]
      },
      {
        name: 'Fill Small',
        componentList: [
          utils.Button(button({ theme: ['-theme-fill', '-theme-small'] }, 12))
        ]
      },
      {
        name: 'Outline',
        componentList: [utils.Button(button({ theme: '-theme-outline' }, 2))]
      },
      {
        name: 'Outline Big',
        componentList: [
          utils.Button(button({ theme: ['-theme-outline', '-theme-big'] }, 3))
        ]
      },

      {
        name: 'Outline Small',
        componentList: [
          utils.Button(button({ theme: ['-theme-outline', '-theme-small'] }, 3))
        ]
      },
      {
        name: 'Orange-outline',
        componentList: [
          utils.Button(button({ theme: '-theme-orange-outline' }, 4))
        ]
      },
      {
        name: 'Fill with icon',
        componentList: [utils.Button(button({ icon: 'info' }, 1))]
      },
      {
        name: 'Fill with icon left',
        componentList: [
          utils.Button(button({ iconBeforeChildren: 'link-arrow' }, 1))
        ]
      },
      {
        name: 'Link',
        componentList: [utils.Button(button({ theme: ['-theme-link'] }, 5))]
      },
      {
        name: 'Link Big',
        componentList: [
          utils.Button(button({ theme: ['-theme-outline', '-theme-big'] }, 6))
        ]
      },
      {
        name: 'Link-primary',
        componentList: [
          utils.Button(button({ theme: ['-theme-link-primary'] }, 7))
        ]
      },
      {
        name: 'Link-secondary',
        componentList: [
          utils.Button(button({ theme: ['-theme-link-secondary'] }, 8))
        ]
      },
      {
        name: 'white',
        componentList: [utils.Button(button({ theme: ['-theme-white'] }, 9))]
      }
    ]
  },
  {
    name: 'CampaignBlock',
    epiName: 'Kampanjeblokk',
    description:
      'Used to highlight content for a period. Uses full content-width or window-width',
    versions: [
      {
        componentList: [
          utils.CampaignBlock(campaignBlock({}, 1)),
          utils.CampaignBlock(campaignBlock({ editorTheme: undefined }, 2)),
          utils.CampaignBlock(campaignBlock({}, 3)),
          utils.CampaignBlock(campaignBlock({ editorTheme: undefined }, 4))
        ]
      }
    ]
  },
  {
    name: 'CheckboxGroup',
    epiName: undefined,
    description: 'Groups of checkboxes used on newletter registration page',
    versions: [
      {
        name: 'checkboxgroup',
        componentList: [
          utils.CheckboxGroup(checkboxGroup({}, 1)),
          utils.CheckboxGroup(
            checkboxGroup({}, { additionalProps: { checked: true } }, 2)
          )
        ]
      },
      {
        name: 'checkboxgroup theme: grey',
        componentList: [
          utils.CheckboxGroup(
            checkboxGroup(
              {},
              { additionalProps: { checked: true, theme: '-theme-grey' } },
              3
            )
          ),
          utils.CheckboxGroup(
            checkboxGroup({}, { additionalProps: { theme: '-theme-grey' } }, 4)
          )
        ]
      }
    ]
  },
  {
    name: 'ContactBlock',
    epiName: 'Kontaktblokk',
    description: 'Block with contact information.',
    versions: [
      {
        name: 'contact block',
        componentList: [
          utils.ContactBlock(contactBlock({}, 1)),
          utils.ContactBlock(contactBlock({}, 2), '-size-half')
        ]
      }
    ]
  },
  {
    name: 'ContactList',
    epiName: 'Kontaktblokker',
    description: 'List of blocks with contact information',
    versions: [
      {
        name: 'contactList',
        componentList: [
          utils.ContactList(contactList({}, 1)),
          utils.ContactList(contactList({}, 2), '-size-half')
        ]
      }
    ]
  },
  {
    name: 'DownloadList',
    epiName: undefined,
    description: "Used in ProposalPage's TabContentSection and i OptionsModal",
    versions: [
      {
        name: 'DownloadList',
        componentList: [
          utils.DownloadList(downloadList({}, 1)),
          utils.DownloadList(downloadList({}, 2), '-size-half')
        ]
      }
    ]
  },
  {
    name: 'DateCard',
    epiName: 'Arrangement',
    description: 'Used in events-page',
    versions: [
      {
        name: 'DateCard',
        componentList: [
          utils.DateCard(event({}, 2)),
          utils.DateCard(event({}, 3)),
          utils.DateCard(
            event(
              {
                dateContainer: {
                  isPastDate: true,
                  labels: { pastDate: 'Gjennomført' }
                }
              },
              4
            )
          ),
          utils.DateCard(
            event(
              {
                dateContainer: {
                  isPastDate: true,
                  labels: { pastDate: 'Gjennomført' }
                }
              },
              5
            )
          ),
          utils.DateCard(event({}, 6))
        ]
      },
      {
        name: 'DateCard in sidebar',
        componentList: [
          utils.DateCard(event({ theme: '-theme-sidebar' }, 2), '-size-third'),
          utils.DateCard(
            event(
              {
                theme: '-theme-sidebar',
                dateContainer: {
                  isPastDate: true,
                  labels: { pastDate: 'Gjennomført' }
                }
              },
              4
            ),
            '-size-third'
          ),
          utils.DateCard(
            event(
              {
                theme: '-theme-sidebar',
                dateContainer: {
                  isPastDate: true,
                  labels: { pastDate: 'Gjennomført' }
                }
              },
              5
            ),
            '-size-third'
          ),
          utils.DateCard(event({ theme: '-theme-sidebar' }, 6), '-size-third')
        ]
      }
    ]
  },
  {
    name: 'DateCard',
    epiName: 'Utlysning',
    description: 'Used in proposals-page',
    versions: [
      {
        name: 'DateCard',
        componentList: [
          utils.DateCard(
            dateCard(
              {
                dateContainer: {
                  isInternational: false,
                  isPlanned: false
                },
                media: undefined,
                status: { text: 'Søk nå', theme: '-is-active' }
              },
              1
            )
          ),
          utils.DateCard(
            dateCard(
              {
                dateContainer: {
                  isInternational: false,
                  isPlanned: false
                },
                media: undefined,
                status: { text: 'Søk nå', theme: '-is-active' }
              },
              2
            )
          ),
          utils.DateCard(
            dateCard(
              {
                dateContainer: {
                  isInternational: false,
                  isPlanned: false
                },
                media: undefined,
                status: { text: 'Søk nå', theme: '-is-active' }
              },
              3
            )
          ),
          utils.DateCard(
            dateCard(
              {
                dateContainer: {
                  dates: [{}],
                  isInternational: false,
                  isPlanned: false
                },
                media: undefined,
                status: { text: 'Søk nå', theme: '-is-active' }
              },
              4
            )
          ),
          utils.DateCard(
            dateCard(
              {
                dateContainer: {
                  dates: [{}],
                  isInternational: false,
                  isPlanned: false
                },
                metadata: undefined,
                media: undefined,
                status: { text: 'Søk nå', theme: '-is-active' }
              },
              5
            )
          ),
          utils.DateCard(
            dateCard(
              {
                dateContainer: {
                  dates: [{}],
                  isInternational: false,
                  isPlanned: true
                },
                metadata: undefined,
                media: undefined,
                status: { text: 'Søk nå', theme: '-is-active' }
              },
              6
            )
          ),
          utils.DateCard(
            dateCard(
              {
                dateContainer: {
                  isInternational: false,
                  isPlanned: true
                },
                metadata: undefined,
                media: undefined,
                status: undefined
              },
              7
            )
          ),
          utils.DateCard(
            dateCard(
              {
                dateContainer: {
                  isInternational: true,
                  isPlanned: false
                },
                media: undefined,
                status: { text: 'Søk nå', theme: '-is-active' }
              },
              8
            )
          ),
          utils.DateCard(
            dateCard(
              {
                dateContainer: {
                  isInternational: true,
                  isPlanned: false
                },
                media: undefined,
                status: { text: 'Søk nå', theme: '-is-active' }
              },
              9
            )
          )
        ]
      }
    ]
  },
  {
    name: 'EventImage',
    epiName: 'Arrangementsbilde',
    description: 'Used in DateCardDates, ArticleBlock and eventPage',
    versions: [
      {
        name: 'With image',
        componentList: [
          utils.EventImage(eventImage({}, 5), '-size-quarter'),
          utils.EventImage(eventImage({}, 4), '-size-quarter'),
          utils.EventImage(eventImage({}, 3), '-size-quarter'),
          utils.EventImage(eventImage({}, 6), '-size-quarter')
        ]
      },
      {
        name: 'With image + dark filter',
        componentList: [
          utils.EventImage(
            eventImage({ darkFilter: true }, 5),
            '-size-quarter'
          ),
          utils.EventImage(
            eventImage({ darkFilter: true }, 4),
            '-size-quarter'
          ),
          utils.EventImage(
            eventImage({ darkFilter: true }, 3),
            '-size-quarter'
          ),
          utils.EventImage(eventImage({ darkFilter: true }, 6), '-size-quarter')
        ]
      },
      {
        name: 'Without image',
        componentList: [
          utils.EventImage(
            eventImage({ image: undefined }, 5),
            '-size-quarter'
          ),
          utils.EventImage(
            eventImage({ image: undefined }, 4),
            '-size-quarter'
          ),
          utils.EventImage(
            eventImage({ image: undefined }, 3),
            '-size-quarter'
          ),
          utils.EventImage(eventImage({ image: undefined }, 6), '-size-quarter')
        ]
      },
      {
        name: 'Without image + grey filter',
        componentList: [
          utils.EventImage(
            eventImage({ image: undefined, greyFilter: true }, 5),
            '-size-quarter'
          ),
          utils.EventImage(
            eventImage({ image: undefined, greyFilter: true }, 4),
            '-size-quarter'
          ),
          utils.EventImage(
            eventImage({ image: undefined, greyFilter: true }, 3),
            '-size-quarter'
          ),
          utils.EventImage(
            eventImage({ image: undefined, greyFilter: true }, 6),
            '-size-quarter'
          )
        ]
      }
    ]
  },
  {
    name: 'TextInput',
    epiName: undefined,
    versions: [
      {
        name: 'Normal',
        componentList: [
          utils.TextInput(textInput({}, 1), '-size-third'),
          utils.TextInput(textInput({ icon: 'mail-square' }, 2), '-size-third'),
          utils.TextInput(
            textInput({ label: 'Alder', type: 'number', value: 34 }, 2),
            '-size-third'
          )
        ]
      },
      {
        name: 'Inverted',
        componentList: [
          utils.TextInput(
            textInput({ theme: '-theme-inverted' }, 1),
            '-size-third'
          ),
          utils.TextInput(
            textInput({ theme: '-theme-inverted', icon: 'mail-square' }, 2),
            '-size-third'
          ),
          utils.TextInput(
            textInput(
              {
                theme: '-theme-inverted',
                label: 'Alder',
                type: 'number',
                value: 34
              },
              2
            ),
            '-size-third'
          )
        ]
      },
      {
        name: 'Small',
        componentList: [
          utils.TextInput(
            textInput({ theme: '-theme-small' }, 1),
            '-size-third'
          ),
          utils.TextInput(
            textInput({ theme: '-theme-small', icon: 'mail-square' }, 2),
            '-size-third'
          ),
          utils.TextInput(
            textInput(
              {
                theme: '-theme-small',
                label: 'Alder',
                type: 'number',
                value: 34
              },
              2
            ),
            '-size-third'
          )
        ]
      },
      {
        name: 'Rounded',
        componentList: [
          utils.TextInput(
            textInput({ theme: '-theme-rounded' }, 1),
            '-size-third'
          ),
          utils.TextInput(
            textInput({ theme: '-theme-rounded', icon: 'mail-square' }, 2),
            '-size-third'
          ),
          utils.TextInput(
            textInput(
              {
                theme: '-theme-rounded',
                label: 'Alder',
                type: 'number',
                value: 34
              },
              2
            ),
            '-size-third'
          )
        ]
      },
      {
        name: 'Orange-focus',
        componentList: [
          utils.TextInput(
            textInput({ theme: '-theme-orange-focus' }, 1),
            '-size-third'
          ),
          utils.TextInput(
            textInput({ theme: '-theme-orange-focus', icon: 'mail-square' }, 2),
            '-size-third'
          ),
          utils.TextInput(
            textInput(
              {
                theme: '-theme-orange-focus',
                label: 'Alder',
                type: 'number',
                value: 34
              },
              2
            ),
            '-size-third'
          )
        ]
      }
    ]
  },
  {
    name: 'FrontpageHeader',
    epiName: 'Toppfelt på forsiden',
    versions: [
      {
        componentList: [
          utils.FrontpageHeader(frontpageHeader({}, 7)),
          utils.FrontpageHeader(
            frontpageHeader(
              {
                theme: '-theme-grey-blue',
                links: [{ isPrimary: true, text: 'Les mer', url: '#' }]
              },
              6
            )
          ),
          utils.FrontpageHeader(frontpageHeader({ theme: '-theme-purple' }, 24))
        ]
      }
    ]
  },
  {
    name: 'ImageWithLink',
    epiName: 'Lenkeblokk med bilde',
    description: 'Used in ContentArea only',
    versions: [
      {
        componentList: [
          utils.ImageWithLink(imageWithLink({}, 13), '-size-half'),
          utils.ImageWithLink(imageWithLink({}, 4), '-size-half'),
          utils.ImageWithLink(imageWithLink({}, 234))
        ]
      }
    ]
  },
  {
    name: 'InfoBlock',
    epiName: 'Informasjonsblokk',
    description:
      'Designed for short texts. Can be used in sidebar, 1/2,1/3 on landingpages. Never full width',
    versions: [
      {
        componentList: [
          utils.InfoBlock(infoBlock({}, 13), '-size-half'),
          utils.InfoBlock(
            infoBlock({ theme: '-theme-blue', icon: undefined }, 14),
            '-size-half'
          ),
          utils.InfoBlock(
            infoBlock({ theme: '-theme-dark-blue' }, 15),
            '-size-half'
          ),
          utils.InfoBlock(
            infoBlock({ theme: '-theme-orange', icon: undefined }, 16),
            '-size-third'
          )
        ]
      }
    ]
  },
  {
    name: 'LinkList',
    epiName: 'Lenkelisteblokk',
    description:
      'Can have LinkList nested within itself with a heading. Can be used in sidebar, 1/2 or 1/3 on landingpages. Never full width.',
    versions: [
      {
        name: 'LinkList',
        componentList: [
          utils.LinkList(linkList({ withIcon: false }, 13), '-size-half'),
          utils.LinkList(
            linkList(
              { theme: '-theme-portfolio', linkTheme: '-theme-portfolio' },
              15
            ),
            '-size-half'
          ),
          utils.LinkList(linkList({ theme: '-theme-border' }, 14), '-size-half')
        ]
      },
      {
        name: 'LinkList with NestedLinks',
        componentList: [
          utils.LinkList(
            linkList(
              { withIcon: false, nestedLinks: [nestedLink({}, 1).item] },
              13
            ),
            '-size-half'
          ),
          utils.LinkList(
            linkList(
              {
                theme: '-theme-portfolio',
                linkTheme: '-theme-portfolio',
                nestedLinks: [nestedLink({}, 2).item, nestedLink({}, 3).item]
              },
              15
            ),
            '-size-half'
          ),
          utils.LinkList(
            linkList(
              {
                theme: '-theme-border',
                nestedLinks: [nestedLink({}, 5).item, nestedLink({}, 4).item]
              },
              14
            ),
            '-size-half'
          )
        ]
      }
    ]
  },
  {
    name: 'LinkLists',
    epiName: undefined,
    description: 'a list of LinkList components',
    versions: [
      {
        name: 'LinkLists',
        componentList: [utils.LinkLists(linkLists({}, 13))]
      },

      {
        name: 'LinkLists SingleColumn',
        componentList: [utils.LinkLists(linkLists({ singleColumn: true }, 14))]
      }
    ]
  },
  {
    name: 'LinkWithText',
    epiName: 'Lenkelisteblokk',
    description: 'Used in PortfolioContent',
    versions: [
      {
        name: 'LinkWithText',
        componentList: [
          utils.PortfolioContent({
            title: 'Lenker med tekst',
            content: utils.ContentArea(
              utils.LinkWithText(linkWithText({}, 13)),
              utils.LinkWithText(linkWithText({}, 14)),
              utils.LinkWithText(linkWithText({}, 15))
            )
          })
        ]
      },
      {
        name: 'LinkWithText isInsidePriorityBlock style',
        componentList: [
          utils.PortfolioContent({
            title: 'Lenker med tekst',
            isInsidePriorityBlock: true,
            content: utils.ContentArea(
              utils.LinkWithText(linkWithText({}, 16)),
              utils.LinkWithText(linkWithText({}, 17)),
              utils.LinkWithText(linkWithText({}, 18))
            )
          })
        ]
      },
      {
        name: 'LinkWithText SingleColumn',
        componentList: [
          utils.PortfolioContent({
            title: 'Lenker med tekst',
            singleColumn: true,
            content: utils.ContentArea(
              utils.LinkWithText(linkWithText({}, 3)),
              utils.LinkWithText(linkWithText({}, 1)),
              utils.LinkWithText(linkWithText({}, 2))
            )
          })
        ]
      }
    ]
  },
  {
    name: 'MediaBlock',
    epiName: 'MediaBlokk',
    description: 'Join block for images, YouTube-videos, videos and graphs.',
    versions: [
      {
        name: 'MediaBlocks with youtube-video',
        componentList: [
          utils.MediaBlock(
            mediaBlock({ image: undefined, embed: undefined }, 6),
            '-size-half'
          ),
          utils.MediaBlock(mediaBlock({}, 7), '-size-half'),
          utils.MediaBlock(mediaBlock({}, 8), '-size-half')
        ]
      },
      {
        name: 'MediaBlocks with embed content',
        componentList: [
          utils.MediaBlock(
            mediaBlock({ image: undefined, youTubeVideo: undefined }, 9),
            '-size-half'
          ),
          utils.MediaBlock(
            mediaBlock({ image: undefined, youTubeVideo: undefined }, 10),
            '-size-half'
          ),
          utils.MediaBlock(
            mediaBlock({ image: undefined, youTubeVideo: undefined }, 11),
            '-size-half'
          )
        ]
      },
      {
        name: 'MediaBlocks with image',
        componentList: [
          utils.MediaBlock(
            mediaBlock({ youTubeVideo: undefined, embed: undefined }, 1),
            '-size-half'
          ),
          utils.MediaBlock(
            mediaBlock({ youTubeVideo: undefined, embed: undefined }, 2),
            '-size-half'
          ),
          utils.MediaBlock(
            mediaBlock({ youTubeVideo: undefined, embed: undefined }, 3),
            '-size-half'
          )
        ]
      }
    ]
  },
  {
    name: 'Message',
    epiName: 'Driftsmelding',
    description:
      'Local and global messages are the exact same component, global only appears futher out in the HTML - making them the full width of the window.',
    versions: [
      {
        componentList: [
          utils.Message(message({ theme: undefined }, 1)),
          utils.Message(message({ theme: '-theme-red' }, 2)),
          utils.Message(message({ theme: '-theme-blue' }, 3))
        ]
      }
    ]
  },
  {
    name: 'Metadata',
    epiName: undefined,
    description: 'List of types and values, supports DocumentIcon',
    versions: [
      {
        name: 'Metadata',
        componentList: [
          utils.Metadata(metadata({}, 1)),
          utils.Metadata(metadata({}, 2)),
          utils.Metadata(metadata({}, 3))
        ]
      },
      {
        name: 'Metadata vertical',
        componentList: [
          utils.Metadata(metadata({ theme: '-theme-vertical' }, 1)),
          utils.Metadata(metadata({ theme: '-theme-vertical' }, 2)),
          utils.Metadata(metadata({ theme: '-theme-vertical' }, 3))
        ]
      }
    ]
  },
  {
    name: 'OptionsModal',
    epiName: undefined,
    description: '',
    versions: [
      {
        name: 'Download templates in proposal',
        componentList: [
          utils.OptionsModal(
            optionsModal(
              {
                openButtonText: 'Last ned maler',
                theme: '-is-proposal-or-event',
                shareContent: undefined
              },
              1
            )
          )
        ]
      },
      {
        name: 'Share in proposal',
        componentList: [
          utils.OptionsModal(
            optionsModal(
              {
                openButtonText: 'Del utlysning',
                theme: '-is-proposal-or-event',
                downloadList: undefined
              },
              1
            )
          )
        ]
      },
      {
        name: 'Share in TextWithSidebar',
        componentList: [
          utils.OptionsModal(
            optionsModal(
              {
                openButtonText: 'Del utlysning',
                downloadList: undefined
              },
              1
            )
          )
        ]
      },
      {
        name: 'Share in ArticleHeader',
        componentList: [
          utils.OptionsModal(
            optionsModal(
              {
                downloadList: undefined
              },
              1
            )
          )
        ]
      },
      {
        name: 'Share in EventPage',
        componentList: [
          utils.OptionsModal(
            optionsModal(
              {
                openButtonText: 'Del arrangement',
                theme: '-is-proposal-or-event',
                downloadList: undefined
              },
              1
            )
          )
        ]
      }
    ]
  },
  {
    name: 'PageHeader',
    epiName: undefined,
    description: 'Header for ContentAreaPage',
    versions: [
      {
        componentList: [
          utils.PageHeader(pageHeader({ map: undefined }, 1)),
          utils.PageHeader(
            pageHeader(
              { image: undefined, links: undefined, portfolioLinks: undefined },
              2
            )
          ),
          utils.PageHeader(
            pageHeader({ map: undefined, links: undefined, tags: undefined }, 3)
          )
        ]
      }
    ]
  },
  {
    name: 'PersonBlock',
    epiName: 'ProfilBlock',
    description: '(Event/Arrangement) used in ContentArea for speakers.',
    versions: [
      {
        componentList: [
          utils.PersonBlock(personBlock({}, 1)),
          utils.PersonBlock(personBlock({ company: undefined }, 2)),
          utils.PersonBlock(personBlock({ image: undefined }, 3)),
          utils.PersonBlock(personBlock({ text: 'Kort tekst.' }, 4)),
          utils.PersonBlock(personBlock({ jobTitle: undefined }, 5))
        ]
      }
    ]
  },
  {
    name: 'PortfolioContent',
    epiName: undefined,
    description: 'A wrapper with title + underline for certain components',
    versions: [
      {
        name: 'PortfolioContent renders any arbitrary component',
        componentList: [
          utils.PortfolioContent(portfolioContent({}, 1)),
          utils.PortfolioContent(
            portfolioContent(
              {
                title: 'renders articleBlocks',
                content: utils.ContentArea(
                  utils.ArticleBlock(articleBlock({}, 1)),
                  utils.ArticleBlock(articleBlock({}, 2))
                )
              },
              2
            )
          )
        ]
      },
      {
        name: 'PortfolioContent with is-inside-priority-block-style',
        componentList: [
          utils.PortfolioContent(
            portfolioContent(
              {
                isInsidePriorityBlock: true
              },
              1
            )
          ),
          utils.PortfolioContent(
            portfolioContent(
              {
                isInsidePriorityBlock: true,
                title: 'renders LinkWithText',
                content: utils.ContentArea(
                  utils.LinkWithText(linkWithText({}, 1)),
                  utils.LinkWithText(linkWithText({}, 2))
                )
              },
              2
            )
          )
        ]
      },
      {
        name: 'PortfolioContent with is-inside-priority-block-style',
        componentList: [
          utils.PortfolioContent(
            portfolioContent(
              {
                isInsidePriorityBlock: true
              },
              1
            )
          ),
          utils.PortfolioContent(
            portfolioContent(
              {
                isInsidePriorityBlock: true,
                title: 'renders LinkWithText',
                content: utils.ContentArea(
                  utils.LinkWithText(linkWithText({}, 1)),
                  utils.LinkWithText(linkWithText({}, 2))
                )
              },
              2
            )
          )
        ]
      }
    ]
  },
  {
    name: 'Process',
    epiName: 'Prosessblokk',
    description:
      'Block to illustrate processes. Can be grid or a carousel. Used in ContentArea.',
    versions: [
      {
        name: 'Process-grid',
        componentList: [utils.Process(process({}, 1))]
      },
      {
        name: 'Process-carousel',
        componentList: [utils.Process(process({ isCarousel: true }, 2))]
      }
    ]
  },
  {
    name: 'RelatedArticles',
    epiName: 'Relaterte artikkler',
    versions: [
      {
        name: 'RelatedArticle list',
        componentList: [
          utils.RelatedArticles(relatedArticles({ isGrid: undefined }, 1))
        ]
      },
      {
        name: 'RelatedArticle list in sidebar',
        componentList: [
          utils.RelatedArticles(
            relatedArticles(
              { isPublication: false, isGrid: undefined, usedInSidebar: true },
              3
            ),
            '-size-third'
          )
        ]
      }
    ]
  },
  {
    name: 'RelatedArticles',
    epiName: 'Pressemeldinger',
    description: 'RelatedArticles with border top',
    versions: [
      {
        componentList: [
          utils.RelatedArticles(
            relatedArticles(
              {
                isGrid: undefined,
                relatedContent: { border: 'top' },
                articles: relatedArticles({}, 5).articles.map(article => ({
                  ...article,
                  metadata: undefined
                }))
              },
              5
            )
          )
        ]
      }
    ]
  },
  {
    name: 'RelatedArticles',
    epiName: 'Relatert aktuelt innhold',
    description: 'List of RelatedArticles',
    versions: [
      {
        componentList: [
          utils.PortfolioContent({
            title: 'Aktuelt',
            content: utils.ContentArea(
              utils.RelatedArticles(
                relatedArticles(
                  {
                    isGrid: true,
                    relatedContent: {
                      link: { text: 'Alle aktuelle saker', url: '#' },
                      border: ''
                    },
                    articles: [
                      articleBlock(
                        {
                          tags: undefined,
                          byline: undefined,
                          linkTags: undefined,
                          text: undefined,
                          metadata: undefined
                        },
                        1
                      ),
                      articleBlock(
                        {
                          tags: undefined,
                          byline: undefined,
                          linkTags: undefined,
                          text: undefined,
                          metadata: undefined
                        },
                        2
                      ),
                      articleBlock(
                        {
                          tags: undefined,
                          byline: undefined,
                          linkTags: undefined,
                          text: undefined,
                          metadata: undefined
                        },
                        3
                      ),
                      articleBlock(
                        {
                          tags: undefined,
                          byline: undefined,
                          linkTags: undefined,
                          text: undefined,
                          metadata: undefined
                        },
                        4
                      )
                    ]
                  },
                  2
                )
              )
            )
          })
        ]
      }
    ]
  },
  {
    name: 'RelatedArticles',
    epiName: 'Relaterte publikasjoner med portefølje-stil',
    description: 'List of related publications inside portfolioContent',
    versions: [
      {
        name: 'RelatedArticle with grid and publication style',
        componentList: [
          utils.PortfolioContent({
            title: 'Relaterte publikasjoner',
            content: utils.ContentArea(
              utils.RelatedArticles(
                relatedArticles(
                  {
                    isPublication: true,
                    isGrid: true,
                    relatedContent: {
                      link: { text: 'Alle aktuelle saker', url: '#' },
                      border: ''
                    },
                    articles: [
                      articleBlock(
                        {
                          image: undefined,
                          tags: undefined,
                          byline: undefined,
                          linkTags: undefined,
                          text: undefined,
                          published: undefined,
                          icon: { iconTheme: 'pdf' }
                        },
                        1
                      ),
                      articleBlock(
                        {
                          image: undefined,
                          tags: undefined,
                          byline: undefined,
                          linkTags: undefined,
                          text: undefined,
                          published: undefined,
                          icon: { iconTheme: 'word' }
                        },
                        2
                      ),
                      articleBlock(
                        {
                          image: undefined,
                          tags: undefined,
                          byline: undefined,
                          linkTags: undefined,
                          text: undefined,
                          published: undefined,
                          icon: { iconTheme: undefined }
                        },
                        3
                      ),
                      articleBlock(
                        {
                          tags: undefined,
                          byline: undefined,
                          linkTags: undefined,
                          text: undefined,
                          published: undefined
                        },
                        4
                      ),
                      articleBlock(
                        {
                          image: undefined,
                          tags: undefined,
                          byline: undefined,
                          linkTags: undefined,
                          text: undefined,
                          published: undefined
                        },
                        5
                      ),
                      articleBlock(
                        {
                          tags: undefined,
                          byline: undefined,
                          linkTags: undefined,
                          text: undefined,
                          published: undefined
                        },
                        6
                      )
                    ]
                  },
                  4
                )
              )
            )
          })
        ]
      }
    ]
  },
  {
    name: 'RelatedDates',
    epiName: 'Relaterte arrangementer',
    description:
      'Shows a list of related events, can be a list or grid. Only used in ContentArea',
    versions: [
      {
        name: 'RelatedDates list',
        componentList: [utils.RelatedDates(relatedDates({ isGrid: false }, 1))]
      },
      {
        name: 'RelatedDates grid',
        componentList: [
          utils.PortfolioContent({
            title: 'Porteføljestil med relaterte arrengementer',
            content: utils.ContentArea(
              utils.RelatedDates(
                relatedDates(
                  {
                    dates: [
                      dateCard({
                        dateContainer: {
                          isPlanned: undefined,

                          eventImage: {
                            background: 'background1'
                          }
                        },
                        tags: undefined,
                        status: undefined,
                        text: undefined
                      }),
                      dateCard(
                        {
                          dateContainer: {
                            isInternational: undefined,
                            isPlanned: undefined,
                            eventImage: {
                              image: dataInterface(1).image
                            }
                          },
                          tags: undefined,
                          status: undefined,
                          text: undefined
                        },
                        2
                      ),
                      dateCard(
                        {
                          dateContainer: {
                            isPlanned: undefined,
                            eventImage: {
                              background: 'background2'
                            }
                          },
                          tags: undefined,
                          status: undefined,
                          text: undefined
                        },
                        3
                      ),
                      dateCard(
                        {
                          dateContainer: {
                            isPlanned: undefined,
                            isInternational: undefined,
                            eventImage: {
                              image: dataInterface(4).image
                            }
                          },
                          tags: undefined,
                          status: undefined,
                          text: undefined
                        },
                        4
                      )
                    ]
                  },
                  2
                )
              )
            )
          })
        ]
      }
    ]
  },
  {
    name: 'RelatedPublication',
    epiName: 'Relaterte publikasjoner',
    description: 'Shows a list of related proposals. Only used in ContentArea',
    versions: [
      {
        componentList: [utils.RelatedPublications(relatedPublications({}, 1))]
      }
    ]
  },
  {
    name: 'SearchResult',
    epiName: 'søkeresultat',
    description: 'Used in search-page',
    versions: [
      {
        componentList: [
          utils.SearchResult(searchResult({}, 1)),
          utils.SearchResult(
            searchResult(
              {
                image: undefined,
                icon: undefined,
                descriptiveTitle: undefined
              },
              2
            )
          ),
          utils.SearchResult(
            searchResult(
              {
                image: undefined,
                statusList: undefined,
                descriptiveTitle: undefined
              },
              3
            )
          ),
          utils.SearchResult(
            searchResult(
              {
                descriptiveTitle: undefined,
                icon: undefined,
                metaData: undefined
              },
              4
            )
          )
        ]
      },
      {
        name: `SearchResult dark-text (doesn't seem to have any effect)`,
        componentList: [
          utils.SearchResult(searchResult({}, 1)),
          utils.SearchResult(
            searchResult(
              {
                theme: '-theme-dark-text',
                image: undefined,
                icon: undefined,
                descriptiveTitle: undefined
              },
              2
            )
          ),
          utils.SearchResult(
            searchResult(
              {
                theme: '-theme-dark-text',
                image: undefined,
                statusList: undefined,
                descriptiveTitle: undefined
              },
              3
            )
          ),
          utils.SearchResult(
            searchResult(
              {
                theme: '-theme-dark-text',
                descriptiveTitle: undefined,
                icon: undefined,
                metaData: undefined
              },
              4
            )
          )
        ]
      },
      {
        name: 'SearchResult no border',
        componentList: [
          utils.SearchResult(searchResult({ theme: '-theme-no-border' }, 1)),
          utils.SearchResult(
            searchResult(
              {
                theme: '-theme-no-border',
                image: undefined,
                icon: undefined,
                descriptiveTitle: undefined
              },
              2
            )
          ),
          utils.SearchResult(
            searchResult(
              {
                theme: '-theme-no-border',
                image: undefined,
                statusList: undefined,
                descriptiveTitle: undefined
              },
              3
            )
          ),
          utils.SearchResult(
            searchResult(
              {
                theme: '-theme-no-border',
                descriptiveTitle: undefined,
                icon: undefined,
                metaData: undefined
              },
              4
            )
          )
        ]
      }
    ]
  },
  {
    name: 'SearchResult',
    epiName: 'publikasjon',
    description: 'Used in publications-page',
    versions: [
      {
        componentList: [
          utils.SearchResult(publication({}, 2)),
          utils.SearchResult(publication({}, 6)),
          utils.SearchResult(publication({}, 4)),
          utils.SearchResult(publication({}, 7)),
          utils.SearchResult(publication({}, 8))
        ]
      }
    ]
  }
];
