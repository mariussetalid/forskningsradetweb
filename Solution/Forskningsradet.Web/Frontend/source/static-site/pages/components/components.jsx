/*
name: komponenter
*/

import React, { useState } from 'react';
import Layout from '../../layout';
import components from './components.js';

import ContentAreaItem from 'components/content-area-item';
import ContentContainer from 'components/content-container';
import Button from 'components/button/button';

const Components = () => {
  const [component, setComponent] = useState(components[0]);
  const [componentsState, setComponentsState] = useState(components);

  const showComponent = componentIndex => {
    const selectedComponent = components.find(
      (_, index) => index === componentIndex
    );

    setComponent(selectedComponent);
  };

  const sort = nameKey => {
    setComponentsState(state =>
      [...state].sort((a, b) => {
        return ('' + a[nameKey]).localeCompare(b[nameKey]);
      })
    );
  };

  return (
    <Layout>
      <div className="components">
        <ul className="components--list">
          <li>
            <Button
              className="components--list-sort-button"
              onClick={() => sort('name')}
            >
              FE-name
            </Button>
            |
            <Button
              className="components--list-sort-button"
              onClick={() => sort('epiName')}
            >
              EPI-name
            </Button>
          </li>
          {componentsState.map(({ name, epiName }, index) => (
            <li className="components--list-item" key={name + index}>
              <Button
                className="components--list-item-button"
                onClick={() => showComponent(index)}
              >
                <span> {name}</span> {epiName && <span>{epiName}</span>}
              </Button>
            </li>
          ))}
        </ul>
        <ContentContainer>
          <div className="content-area -flex-layout">
            {component && (
              <>
                <h1 className="components--component-title">
                  {component.name}
                  {component.epiName && `/${component.epiName}`}
                </h1>
                {component.description && (
                  <p className="components--component-description">
                    {component.description}
                  </p>
                )}
                {component.versions.map(({ name, componentList }, index) => {
                  return (
                    <React.Fragment key={`${name}${index}`}>
                      <h3 className="components--component-version-title">
                        {name}
                      </h3>
                      {componentList.map((component, i) => (
                        <ContentAreaItem
                          key={`${component.componentName} ${i}`}
                          enableElementSizing
                          {...component}
                          id={`${component.componentName} ${i}`}
                        />
                      ))}
                    </React.Fragment>
                  );
                })}
              </>
            )}
          </div>
        </ContentContainer>
      </div>
    </Layout>
  );
};

export default Components;
