/*
name: Bekreftelse på avmelding
group: Nyhetsbrev
*/

import React from 'react';

import NewsletterUnsubscribeConfirmationPage from 'components/newsletter-unsubscribe-confirmation-page';

import content from './newsletter-unsubscribe-confirmation.js';

const NewsletterUnsubscribeConfirmation = () => (
  <NewsletterUnsubscribeConfirmationPage {...content} />
);

export default NewsletterUnsubscribeConfirmation;
