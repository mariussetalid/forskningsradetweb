export default {
  layout: {
    header: {
      text: 'Forskningsrådet',
      url: '/'
    }
  },
  form: { endpoint: '/newsletter-unsubscribe' },
  title: 'Avbestill e-postvarslinger til',
  submitButton: {
    text: 'Lagre endringer'
  },
  input: {
    label: 'E-post',
    name: 'e-post'
  }
};
