/*
name: Avmelding
group: Nyhetsbrev
*/
import React from 'react';

import NewsletterUnsubscribePage from 'components/newsletter-unsubscribe-page';

import content from './newsletter-unsubscribe.js';

const NewsletterUnsubscribe = () => <NewsletterUnsubscribePage {...content} />;

export default NewsletterUnsubscribe;
