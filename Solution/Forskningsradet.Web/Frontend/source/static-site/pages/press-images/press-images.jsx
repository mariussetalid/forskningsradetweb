/*
group: CMS-maler
name: Bilder til nedlastning
path: press-images
*/

import React from 'react';
import Layout from '../../layout';

import ContentAreaPage from 'components/content-area-page';

import Content from './press-images.js';

const PressImages = () => (
  <Layout>
    <ContentAreaPage {...Content} />
  </Layout>
);

export default PressImages;
