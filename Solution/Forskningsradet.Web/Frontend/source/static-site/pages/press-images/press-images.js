import utils from '../../content-area-utils';

export default {
  pageHeader: { title: 'Bilder til nedlasting' },
  content: utils.ContentArea(
    utils.ImageWithLink(
      {
        image: {
          alt: 'Call to action',
          src: '/static-site/assets/students.png'
        },
        text:
          'Tittel tittel tittel her kan man ha en lang lang lang lang tittel tittel',
        url: 'Url',
        shareImageHeight: true
      },
      '-size-third'
    ),
    utils.ImageWithLink(
      {
        image: {
          alt: 'Call to action',
          src: '/static-site/assets/students.png'
        },
        text:
          'Tittel tittel tittel her kan man ha en lang lang lang lang tittel tittel',
        url: 'Url',
        shareImageHeight: true
      },
      '-size-third'
    ),
    utils.ImageWithLink(
      {
        image: {
          alt: 'Call to action',
          src: '/static-site/assets/students.png'
        },
        text:
          'Tittel tittel tittel her kan man ha en lang lang lang lang tittel tittel',
        url: 'Url',
        shareImageHeight: true
      },
      '-size-third'
    ),
    utils.InfoBlock(
      {
        title: 'Forskningsrådets logoer til nedlasting',
        linkList: {
          items: [
            {
              url: '/',
              text: 'Logoer til bruk i prosjekter som har fått støtte (.png)'
            },
            {
              url: '/',
              text: 'Logoer for digitale flater (.png)'
            },
            {
              url: '/',
              text: 'Logoer for trykk (.pdf)'
            }
          ]
        },
        editorTheme: '-theme-blue',
        shareImageHeight: true
      },
      '-size-third'
    ),
    utils.ImageWithLink(
      {
        image: {
          alt: 'Call to action',
          src: '/static-site/assets/students.png'
        },
        text:
          'Tittel tittel tittel her kan man ha en lang lang lang lang tittel tittel',
        url: 'Url',
        shareImageHeight: true
      },
      '-size-third'
    ),
    utils.ImageWithLink(
      {
        image: {
          alt: 'Call to action',
          src: '/static-site/assets/students.png'
        },
        text:
          'Tittel tittel tittel her kan man ha en lang lang lang lang tittel tittel',
        url: 'Url',
        shareImageHeight: true
      },
      '-size-third'
    ),
    utils.ImageWithLink(
      {
        image: {
          alt: 'Call to action',
          src: '/static-site/assets/students.png'
        },
        text:
          'Tittel tittel tittel her kan man ha en lang lang lang lang tittel tittel',
        url: 'Url',
        shareImageHeight: true
      },
      '-size-half'
    ),
    utils.ImageWithLink(
      {
        image: {
          alt: 'Call to action',
          src: '/static-site/assets/students.png'
        },
        text:
          'Tittel tittel tittel her kan man ha en lang lang lang lang tittel tittel',
        url: 'Url',
        shareImageHeight: true
      },
      '-size-half'
    ),
    utils.ImageWithLink(
      {
        image: {
          alt: 'Call to action',
          src: '/static-site/assets/students.png'
        },
        text:
          'Tittel tittel tittel her kan man ha en lang lang lang lang tittel tittel',
        url: 'Url',
        shareImageHeight: true
      },
      '-size-third'
    ),
    utils.ImageWithLink(
      {
        image: {
          alt: 'Call to action',
          src: '/static-site/assets/students.png'
        },
        text:
          'Tittel tittel tittel her kan man ha en lang lang lang lang tittel tittel',
        url: 'Url',
        shareImageHeight: true
      },
      '-size-third'
    ),
    utils.ImageWithLink(
      {
        image: {
          alt: 'Call to action',
          src: '/static-site/assets/students.png'
        },
        text:
          'Tittel tittel tittel her kan man ha en lang lang lang lang tittel tittel',
        url: 'Url',
        shareImageHeight: true
      },
      '-size-third'
    )
  )
};
