/*
name: Driftsmeldinger
*/

import React from 'react';
import Layout from '../../layout';

import Message from 'components/message';

const Messages = () => (
  <Layout>
    <Message
      text={{
        text: '<p>Et lorem <a href="#">ipsum</a> red alert varsel globalt.'
      }}
      theme={Message.themes.red}
    />
    <Message
      text={{
        text:
          '<p>Et lorem <a href="#">ipsum</a> Fusce dictum risus risus. Phasellus in gravida orci. Nunc vel mollis risus.'
      }}
    />
    <Message
      text={{
        text:
          '<p>Har du spørsmål knyttet til innlevering av søknad kan du kontakte oss på telefon: 41479820 alle dager, hele døgnet.'
      }}
      theme={Message.themes.blue}
    />
  </Layout>
);

export default Messages;
