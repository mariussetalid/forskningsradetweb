/*
name: Brødsmulesti
group: Globale komponenter
*/
// Globale komponenter/Brødsmulesti
import React from 'react';

import content from './breadcrumbs.json';

import Breadcrumbs from 'components/breadcrumbs';

const BreadcrumbsPage = () => <Breadcrumbs {...content} />;

export default BreadcrumbsPage;
