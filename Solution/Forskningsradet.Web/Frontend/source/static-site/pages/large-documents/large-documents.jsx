/*
group: CMS-maler
name: Store dokumenter
*/

import React from 'react';
import Layout from '../../layout';

import content from './large-documents.js';

import LargeDocumentsPage from 'components/large-documents-page';

const LargeDocuments = () => (
  <Layout>
    <LargeDocumentsPage {...content} />
  </Layout>
);

export default LargeDocuments;
