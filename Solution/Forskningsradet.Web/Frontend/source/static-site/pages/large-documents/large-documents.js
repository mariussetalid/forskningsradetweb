import utils from '../../content-area-utils';

export default {
  title: '2.2 Utviklingen i internasjonal FoU',
  ingress:
    'I dette delkapitlet presenterer vi først utviklingen i FoU-satsingen for verdens regioner med data for 2007 til 2015 fra UNESCO. Deretter benytter vi OECDs FoU-data som har oppdatert statistikk til og med 2016 (kapittel 2.2.1). Vi belyser utviklingen i FoU etter utførende sektor og finansieringskilde (kapittel 2.2.2) og ser på de seneste trendene ved hjelp av tall over satsing på FoU i statsbudsjettet (kapittel 2.2.3).',
  header: {
    accordion: {
      guid: '92837n4tv09w8erugnvp20nu',
      collapseLabel: 'Vis mindre',
      expandLabel: 'Vis mer'
    },
    byline: {
      items: [
        { text: 'Publisert 13. april 2018' },
        { text: 'Oppdatert 14. mai 2018' }
      ]
    },
    download: {
      url: '/',
      text: 'Last ned pdf'
    }
  },
  content: utils.RichText(
    '<p>Curabitur sem odio pulvinar eget ex et dictum sagittis ex. Morbi purus velit cursus eu nibh a, viverra porttitor sem. Vivamus sagittis rutrum facilisis. Ut sed dignissim felis. Proin mattis ultrices quam vitae scelerisque. Suspendisse sagittis, lorem et hendrerit iaculis, enim sapien maximus ipsum, nec ultricies ex sapien ut nunc.</p><table><tr><th>Tittel</th><th>Publisering</th></tr><tr><td><a href="/">2.2.1  Utvikling i internasjonal FoU</a></td><td>24. september 2018</td></tr><tr><td><a href="/">2.2.2  FoU-utgifter etter sektor</a></td><td>24. september 2018</td></tr><tr><td><a href="/">2.2.3  FoU-bevilgninger over statlige budsjetter</a></td><td>10. oktober 2018</td></tr></table><p>Aenean nec rhoncus nisi, vel sagittis mauris. Maecenas blandit at arcu at mattis. Aliquam ac erat efficitur, molestie arcu non, placerat mi. Cras ut nulla in magna ornare blandit. In hac habitasse platea dictumst. Aliquam erat volutpat.</p>',
    '<div class="html-string"><p>&nbsp;</p><p>I 2017 utgjorde Norges samlede utgifter til forskning og utviklingsarbeid (FoU) 69,2&nbsp;milliarder kroner. Utgiftene økte nominelt med 5,8 milliarder kroner fra 2016.</p><p><strong>Figur 1.1a Totale FoU-utgifter etter sektor for utførelse. 2007–2017. Faste 2010-priser.</strong></p><p><img src="/contentassets/2a6be06ff7254111bbdcc5490a2e2adc/imageaf3g.png" alt="imageaf3g.png"></p><p>¹Helseforetak inngår i hhv. UoH- og instituttsektoren, se faktaboks om sektorinndeling i FoU-statistikken.</p><p><em>Kilde: SSB og NIFU, FoU-statistikk</em></p><p>Justert for lønns- og prisstigning var det en realvekst i FoU-giftene på 7&nbsp;prosent fra 2016.<a href="#_ftn1" name="_ftnref1">[1]</a></p><p>I løpet av de siste ti årene var det kun i 2015 at veksten lå på et høyere nivå enn dette, da var den på nærmere 9 prosent. I tiårsperioden 2007–2017 var det en gjennomsnittlig årlig realvekst på 3 prosent. Av figur 1.1a fremgår det at i den siste tiårsperioden har FoU-utgiftene i næringslivet og universitets- og høgskolesektoren økt langt sterkere enn instituttsektoren og helseforetakene.</p><p>Som andel av total FoU var næringslivets FoU 46&nbsp;prosent både i 2017 og 2007, med en liten nedgang i midten av perioden. Instituttsektorens andel er redusert fra 23&nbsp;prosent til 20&nbsp;prosent fra 2007 til 2017, mens universitets- og høgskolesektorens andel har økt fra 32&nbsp;prosent til nesten 34&nbsp;prosent i samme periode. Helseforetakenes andel av total FoU har ligget omkring 6&nbsp;prosent i hele perioden.</p></div>'
  ),
  footer: {},
  pageNavigation: {
    previous: {
      url: '/',
      text: 'Forrige side'
    },
    next: {
      url: '/',
      text: 'Neste side'
    }
  },
  tableOfContents: {
    title: 'Innhold',
    accordion: {
      collapseLabel: 'Skjul',
      expandLabel: 'Vis',
      guid: 'as0f9uvmnqw3p095vumqwerum'
    },
    items: [
      {
        linkOrLinkList: null,
        link: {
          link: {
            text: 'Om indikatorrapporten 2018',
            url: '/?3'
          }
        }
      },
      {
        link: null,
        linkOrLinkList: {
          accordion: {
            collapseLabel: 'Skjul lenker',
            expandLabel: 'Vis lenker',
            guid: 'asdfouasnfv2347n5v9',
            initiallyOpen: true
          },
          link: {
            chapterNumber: '1',
            link: {
              text: 'FoU i Norge',
              url: '/?1'
            }
          },
          isCurrent: true,
          linkOrLinkList: [
            {
              linkOrLinkList: null,
              link: {
                chapterNumber: '1.1',
                link: {
                  text: 'Masse linker',
                  url: '/?1'
                }
              }
            },
            {
              linkOrLinkList: {
                accordion: {
                  guid: 'asioun3lmkvanfv2347n5v9',
                  collapseLabel: 'Skjul lenker',
                  expandLabel: 'Vis lenker'
                },
                isCurrent: true,
                link: {
                  hasDash: true,
                  link: {
                    text: 'FoU i Norge',
                    url: '/?1'
                  }
                },
                linkOrLinkList: [
                  {
                    linkOrLinkList: {
                      accordion: {
                        guid: 'asioun3lmkvanfasdgrrwegv2347n5v9',
                        collapseLabel: 'Skjul lenker',
                        expandLabel: 'Vis lenker'
                      },
                      isCurrent: true,
                      link: {
                        hasDash: true,
                        link: {
                          text: 'FoU i Norge',
                          url: '/?1'
                        }
                      },
                      linkOrLinkList: [
                        {
                          linkOrLinkList: null,
                          link: {
                            hasDash: true,
                            link: {
                              text: 'I en lang liste',
                              url: '/?1'
                            }
                          }
                        }
                      ]
                    },
                    link: null
                  }
                ]
              },
              link: null
            },
            {
              linkOrLinkList: null,
              link: {
                chapterNumber: '1.3',
                link: {
                  text: 'FoU-utgifter etter sektor',
                  url: '/?3'
                }
              }
            },
            {
              linkOrLinkList: null,
              link: {
                chapterNumber: '1.4',
                link: {
                  text: 'FoU-bevilgninger over statlige budsjetter',
                  url: '/?4'
                }
              }
            },
            {
              linkOrLinkList: null,
              link: {
                chapterNumber: '1.5',
                link: {
                  text:
                    'Regional fordeling av FoU i europeisk og nordisk perspektiv',
                  url: '/?5'
                }
              }
            },
            {
              linkOrLinkList: null,
              link: {
                chapterNumber: '1.6',
                link: {
                  text:
                    'Europeisk sammenligning av FoU-aktivitet i næringslivet',
                  url: '/?6'
                }
              }
            },
            {
              linkOrLinkList: null,
              link: {
                chapterNumber: '1.7',
                link: {
                  text: 'Forskning på fossil og fornybar energi',
                  url: '/?7'
                }
              }
            },
            {
              linkOrLinkList: null,
              link: {
                chapterNumber: '1.8',
                link: {
                  text: 'Energirelaterte FoU-D-bevilgninger',
                  url: '/?8'
                }
              }
            },
            {
              linkOrLinkList: null,
              link: {
                chapterNumber: '1.9',
                link: {
                  text: 'FoU-D på energi i Norden',
                  url: '/?9'
                }
              }
            }
          ]
        }
      },
      {
        link: null,
        linkOrLinkList: {
          accordion: {
            guid: 'asd47n5vfouasnfv239',
            collapseLabel: 'Skjul lenker',
            expandLabel: 'Vis lenker'
          },
          link: {
            chapterNumber: '2',
            link: {
              text: 'Internasjonal FoU',
              url: '/?1'
            }
          },
          linkOrLinkList: [
            {
              link: {
                chapterNumber: '2.1',
                link: {
                  text: 'Utviklingen i internasjonal økonomi',
                  url: '/?1'
                }
              },
              linkOrLinkList: null
            }
          ]
        }
      },
      {
        link: null,
        linkOrLinkList: {
          accordion: {
            guid: 'uasnfv2347asdfon5v9',
            collapseLabel: 'Skjul lenker',
            expandLabel: 'Vis lenker'
          },
          link: {
            chapterNumber: '3',
            link: {
              text: 'Menneskelige ressurser',
              url: '/?1'
            }
          },
          linkOrLinkList: [
            {
              link: {
                chapterNumber: '3.1',
                link: {
                  text: 'Utviklingen i internasjonal økonomi',
                  url: '/?1'
                }
              },
              linkOrLinkList: null
            }
          ]
        }
      },
      {
        link: null,
        linkOrLinkList: {
          accordion: {
            guid: 'asasnfv23dfou47n5v9',
            collapseLabel: 'Skjul lenker',
            expandLabel: 'Vis lenker'
          },
          link: {
            chapterNumber: '4',
            link: {
              text: 'Bevilgninger og virkemidler',
              url: '/?1'
            }
          },
          linkOrLinkList: [
            {
              link: {
                chapterNumber: '4.1',
                link: {
                  text: 'Utviklingen i internasjonal økonomi',
                  url: '/?1'
                }
              },
              linkOrLinkList: null
            }
          ]
        }
      },
      {
        link: null,
        linkOrLinkList: {
          accordion: {
            guid: 'asdfo347n5vuasnfv29',
            collapseLabel: 'Skjul lenker',
            expandLabel: 'Vis lenker'
          },
          link: {
            chapterNumber: '5',
            link: {
              text: 'Immaterielle rettigheter',
              url: '/?1'
            }
          },
          linkOrLinkList: [
            {
              link: {
                chapterNumber: '5.1',
                link: {
                  text: 'Utviklingen i internasjonal økonomi',
                  url: '/?1'
                }
              },
              linkOrLinkList: null
            }
          ]
        }
      },
      {
        link: null,
        linkOrLinkList: {
          accordion: {
            guid: 'asdasnfvfou2347n5v9',
            collapseLabel: 'Skjul lenker',
            expandLabel: 'Vis lenker'
          },
          link: {
            chapterNumber: '6',
            link: {
              text: 'Vitenskapelige publiseringer',
              url: '/?1'
            }
          },
          linkOrLinkList: [
            {
              link: {
                chapterNumber: '6.1',
                link: {
                  text: 'Utviklingen i internasjonal økonomi',
                  url: '/?1'
                }
              },
              linkOrLinkList: null
            }
          ]
        }
      },
      {
        link: null,
        linkOrLinkList: {
          accordion: {
            guid: 'asdv2347nfouasnf5v9',
            collapseLabel: 'Skjul lenker',
            expandLabel: 'Vis lenker'
          },
          link: {
            chapterNumber: '7',
            link: {
              text: 'Innovasjon i Norge og Europa',
              url: '/?1'
            }
          },
          linkOrLinkList: [
            {
              link: {
                chapterNumber: '7.1',
                link: {
                  text: 'Utviklingen i internasjonal økonomi',
                  url: '/?1'
                }
              },
              linkOrLinkList: null
            }
          ]
        }
      },
      {
        linkOrLinkList: null,
        link: {
          link: {
            text: 'Statistikk og tabeller',
            url: '/?2'
          }
        }
      },
      {
        linkOrLinkList: null,
        link: {
          link: {
            text: 'Nyttige lenker',
            url: '/?3'
          }
        }
      }
    ]
  }
};
