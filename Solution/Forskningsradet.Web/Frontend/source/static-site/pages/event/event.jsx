/*
name: Ett arrangement
group: Arrangementer
*/
import React from 'react';
import Layout from '../../layout';

import content from './event.js';

import EventPage from 'components/event-page';

const Event = () => (
  <Layout>
    <EventPage {...content} />
  </Layout>
);

export default Event;
