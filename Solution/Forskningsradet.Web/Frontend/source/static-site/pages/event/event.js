import utils from '../../content-area-utils';

export default {
  title: 'Hvordan søke ERC Advanced Grants',
  registrationLink: { text: 'Meld deg på', url: '#', isExternal: true },
  dateContainer: {
    datesTitle: 'En tittel',
    datesDescription: 'søknad',
    isPastDate: false,
    dates: [
      { day: '17.', month: 'nov' },
      { day: '23.', month: 'oct' }
    ],
    labels: { pastDate: 'Concluded' }
  },
  eventImage: {
    image: {
      alt: 'Olav Normann Hansen'
      //src: '/static-site/assets/profile-pic-2.jpg'
    },
    background: 'background1'
  },
  menu: {
    title: 'Snarveier',
    navGroups: [
      {
        links: {
          items: [
            {
              link: {
                text: 'Om arrangementet',
                url: '#om-arrangementet'
              }
            },
            {
              link: {
                text: 'Foredragsholdere',
                url: '#foredragsholdere'
              }
            },
            {
              link: {
                text: 'Program',
                url: '#program'
              }
            },
            {
              link: {
                text: 'Kontakt informasjon',
                url: '#kontakt-informasjon'
              }
            }
          ]
        }
      }
    ]
  },
  labels: {
    about: { htmlId: 'om-arrangementet', title: 'Om arrangementet' },
    contact: { htmlId: 'kontakt-informasjon' },
    schedule: { htmlId: 'program', title: 'Program' },
    speakers: { htmlId: 'foredragsholdere', title: 'Foredragsholdere' }
  },
  onPageEditing: {
    title: 'Title'
  },
  contactInfo: utils.ContentArea(
    utils.ContactInfo({
      onPageEditing: { title: 'Title' },
      title: 'Odd M. Reitevold',
      details: [
        { text: 'Spesialrådgiver' },
        { text: 'Næringsliv og teknologi' },
        {
          url: 'mailto:omr@forskningsradet.no',
          text: 'omr@forskningsradet.no'
        }
      ]
    }),
    utils.ContactInfo({
      title: 'Per Ivar Høvring',
      details: [
        { text: 'Norges Forskningsråd' },
        { text: '+47 123 45 678', url: '#' }
      ]
    })
  ),
  links: [
    { icon: 'calendar', text: 'Legg til i min kalender', url: '/?1' },
    { icon: 'video', text: 'Følg sending direkte', url: '/?2' }
  ],
  share: {
    openButtonText: 'Del arrangement',
    shareContent: {
      items: [
        { url: '#', text: 'Mail', icon: 'mail' },
        { url: '#', text: 'Twitter', icon: 'twitter' },
        { url: '#', text: 'Facebook', icon: 'facebook' },
        { url: '#', text: 'LinkedIn', icon: 'linkedin' }
      ]
    }
  },
  metadata: {
    onPageEditing: { items: 'Items' },
    items: [
      {
        label: 'Når',
        text: [
          ['Mandag 24. april 2018', 'kl 08:00 - 16:00'],
          ['Tirsdag 25. april 2018', 'kl 08:00 - 12:00']
        ]
      },
      {
        label: 'Hvor',
        text: [
          ['Trondheim, NTNU Gløshaugen Campus Styrerommet, hovedbygget 3. etg.']
        ]
      },
      {
        label: 'Arrangementstype',
        text: [['Kurs / webinar']]
      },
      {
        label: 'Passer for',
        text: [
          [
            'Antarktisforskere i Norge, samt norske myndigheter og beslutningstakere med interesser og asnvar i Antarktis'
          ]
        ]
      },
      {
        label: 'Pris',
        text: [['Gratis']]
      },
      {
        label: 'Påmeldingsfrist',
        text: [['30. august 2018']]
      },
      {
        label: 'Neste kurs',
        text: [['22. oktober 2018']]
      }
    ]
  },
  media: {
    items: [
      {
        icon: 'camera',
        text: 'Arrangementet strømmes og opptak blir tilgjengelig'
      }
    ]
  },
  richText: {
    onPageEditing: { name: 'MainBody' },
    blocks: utils.RichText(
      '<p>Forskningsrådet vil sammen med NTNU hjelpe deg med å sende en konkurransedyktig søknad til ERC og inviterer til workshop. På workshopen vil du få viktig informasjon om hvordan skrive en best mulig søknad. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p><p><b>Fristen for å søke ERC Advanced Grants (WP2018) er 30. august 2018.</b></p> </br> <h2>Training cour output:</h2> <ul><li>Have the proper H2020 management tools</li><li>Have the proper H2020 management tools</li><li>Have the proper H2020 management tools</li><li>Have the proper H2020 management tools</li></ul>'
    ).blocks
  },
  speakers: {
    onPageEditing: { name: 'Speakers' },
    blocks: utils.ContentArea(
      utils.PersonBlock(
        {
          company: 'Yellow Research',
          onPageEditing: {
            company: 'Company',
            jobTitle: 'JobTitle',
            name: 'Name',
            text: 'Text'
          },
          image: {
            alt: 'Olav Normann Hansen',
            src: '/static-site/assets/logo-nysgjerrigper-white.png'
          },
          jobTitle: 'Konsulent',
          name: 'Mette Skraastad',
          text:
            'Hun har omfattende erfaring med å lede ERC-workshops og med å se gjennom søknader til ERC Starting, Consolidator og Advanced Grant. Hun har trenet søkere til ERC siden den andre utlysningen i EUs forrige rammeprogram, 7RP.'
        },
        {
          contentName: 'SomePersonBlock',
          contentId: '11239847192'
        }
      ),
      utils.PersonBlock({
        company: 'Forskningsrådet',
        image: {
          alt: 'Olav Normann Hansen',
          src: '/static-site/assets/profile-pic-2.jpg'
        },
        jobTitle: 'Divisjonsdirektør',
        name: 'Olav Normann Hansen',
        text:
          'Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.'
      })
    ).blocks
  },
  schedule: utils.ContentArea(
    utils.AccordionWithContentAreaList({
      accordion: {
        collapseLabel: 'Skjul',
        expandLabel: 'Vis',
        guid: 'as+df098nas+df08cunas0+df9n'
      },
      content: [
        {
          htmlId: 'first',
          initiallyOpen: true,
          title: 'Onsdag 17.juni 2020',
          content: utils.ContentArea(
            utils.Message({
              theme: '-theme-red',
              text: {
                text:
                  '<b>Viktig/kritisk overordnet varsel.</b> Denne utlysningen vil oppdateres fortløpende fram til seks uker før søknadsfrist. Når utlysningen er aktivert vil du ha mulighet til å opprette og sende inn søknad.'
              }
            }),
            utils.RichTextBlock({
              text: utils.RichText('<p><b>Møteleder:</b> Ruth Astrid Sæter</p>')
            }),
            utils.Schedule({
              lastTitle: 'Slutt',
              programList: [
                {
                  time: '08:45',
                  title: 'Oppkobling',
                  text: utils.RichText(
                    '<p> Vi anbefaler å koble opp i god tid før konferansen begynner. </p>'
                  ),
                  isBreak: false
                },
                {
                  time: '09:00',
                  title:
                    'Hvordan satse for å løse energi- og klimautfordinger midt i en pandemi?',
                  text: utils.RichText(
                    "<p>En flott forklaring på hva man skal gjøre her. Hvordan ser det ut når jeg skriver en litt lengre tekst i dette feltet?</p> <blockquote id='output'>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</blockquote><ul><li>tets</li></ul></div>"
                  ),
                  isBreak: true
                },
                {
                  time: '09:30',
                  title: 'Foredrag nummer 3',
                  text: utils.RichText(
                    "<ul> <li><h3><b>På hvilke områder kan Norge ta en lederrolle i internasjonalt samarbeid om erergi- og klimatutfordinger?</b></h3><p>John-Arne Røttingen, administrerende direktør i Forskingsrådet | <a href='#'>Opptak</a> | <a href='#'>Presentasjon</a></p><p><b>På hvilke områder kan Norge ta en lederrolle i internasjonalt samarbeid om erergi- og klimatutfordinger?</b></p></li></ul>"
                  ),
                  isBreak: false
                },
                {
                  time: '11:00',
                  text: utils.RichText(
                    "<ul> <li><p><b>På hvilke områder kan Norge ta en lederrolle i internasjonalt samarbeid om erergi- og klimatutfordinger?</b></p><p>John-Arne Røttingen, administrerende direktør i Forskingsrådet | <a href='other-page'>Opptak</a> | <a href='#'>Presentasjon</a></p></li></ul>"
                  )
                }
              ]
            })
          )
        },
        {
          htmlId: 'second',
          title: 'Onsdag 17.juni 2020',
          content: utils.ContentArea(
            utils.RichTextBlock({
              text: utils.RichText('<p><b>Møteleder:</b> Ruth Astrid Sæter</p>')
            }),
            utils.Schedule({
              lastTitle: 'Slutt',
              programList: [
                {
                  time: '08:45',
                  title: 'Oppkobling',
                  text: utils.RichText(
                    '<p> Vi anbefaler å koble opp i god tid før konferansen begynner. </p>'
                  ),
                  isBreak: false
                },
                {
                  time: '09:00',
                  title:
                    'Hvordan satse for å løse energi- og klimautfordinger midt i en pandemi?',
                  text: utils.RichText(
                    "<ul> <li><p><b>På hvilke områder kan Norge ta en lederrolle i internasjonalt samarbeid om erergi- og klimatutfordinger?</b></p><p>John-Arne Røttingen, administrerende direktør i Forskingsrådet | <a href='#'>Opptak</a> | <a href='google.com'>Presentasjon</a></p></li></ul>"
                  ),
                  isBreak: true
                }
              ]
            })
          )
        },
        {
          htmlId: 'third',
          title: 'Brukerstyrt innovasjon (BIA)',
          share: {
            openButtonText: 'Del utlysningen',
            shareContent: {
              items: [
                { url: '#', text: 'Mail', icon: 'mail' },
                { url: '#', text: 'LinkedIn', icon: 'linkedin' }
              ]
            }
          },
          content: utils.ContentArea(
            utils.TextWithSidebar({
              htmlId: 'brukerstyrt-innovasjon',
              text: utils.RichText(
                '<p>BIA skal bidra til størst mulig verdiskaping i norsk næringsliv gjennom forskningsbasert innovasjon i bedrifter og samarbeidende FoU-miljøer innenfor BIA-programmets ansvarsområde.</p>'
              )
            })
          )
        }
      ]
    })
    /*
    utils.Schedule(
      {
        onPageEditing: { title: 'Title' },
        title: 'Mandag 24. april',
        times: utils.RichText(
          '<p>08:30 – 09:00:  Registrering</p><p>09:00 – 12:30:  Hovedsesjon</p><p>12:30 – 13:30:  Lunsj</p><p>14:00 – 16:00:  Individuelle samtaler</p>'
        )
      },
      '-size-half'
    ),
    utils.Schedule(
      {
        title: 'Tirsdag 25. april',
        times: utils.RichText(
          '<p>08:30 – 09:00:  Registrering</p><p>09:00 – 12:30:  Hovedsesjon</p>'
        )
      },
      '-size-half'
    ),
    utils.Schedule(
      {
        title: 'Onsdag 26. april',
        times: utils.RichText(
          '<p>08:30 – 09:00:  Registrering</p><p>09:00 – 12:30:  Hovedsesjon</p>'
        )
      },
      '-size-half'
    )*/
  )
};
