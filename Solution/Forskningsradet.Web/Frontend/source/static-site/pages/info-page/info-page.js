import utils from '../../content-area-utils';

export default {
  title: 'Forskningsrådets priser',
  ingress:
    'Forskningsrådet deler årleg ut fem prisar til forskarar innanfor tre kategoriar. Kjenner du ein forskar som har utmerkt seg med forsking av høg kvalitet, fagleg nyskaping, eller med særs god kommunikasjon om resultat frå eige fagfelt? Eller som har utnytta forskingsresultat og skapt forskingsbasert innovasjon med Forskingsrådets verkemidlar? Send oss forslaga dine!',
  header: {},
  content: utils.RichText(
    utils.ImageWithCaption({
      editorTheme: '-theme-half-width',
      caption:
        'Vim ex consul principes. Et vivendo delicatissimi vim, vix no saperet appetere.',
      image: {
        alt: 'VR-glasses',
        src: '/static-site/assets/vr-glasses.png'
      }
    }),
    '<p>Fastidii delicatissimi nec, no ius alia timeam deleniti. Graeco consectetuer te qui, est ferri suavitate posidonium ex. Cum legendos voluptaria expetendis ex. At delenit contentiones eum. Sanctus salutandi consetetur mea et. At laudem molestiae scribentur nec, quo te tantas prompta oportere. Lorem ipsum dolor sit amet, choro civibus eu sed, graeco evertitur sed at, invidunt voluptaria mea an. Stet commodo vocibus at mea, augue percipit ne vis, at erat soluta audiam sit. </p>',
    utils.YoutubeVideo(
      {
        urlOrEmbed:
          '<iframe width="560" height="315" src="https://www.youtube.com/embed/GN-peWeNBkU?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>'
      },
      '-size-half'
    )
  ),
  sidebar: utils.ContentArea(
    utils.InfoBlock({
      title: 'Relatert innhold',
      text: utils.RichText('Her kan du lese vedtektene for de ulike prisene'),
      linkList: {
        items: [
          {
            url: '/',
            text: 'Forskningrådets prisar for unge framifrå forskarar'
          },
          {
            url: '/',
            text: 'Forskningrådets formidlingspris'
          },
          {
            url: '/',
            text: 'Forskningsrådets innovasjonspris'
          }
        ]
      },
      editorTheme: '-theme-blue'
    }),
    utils.CtaLink({
      text: 'Send oss ditt forslag',
      url: '/',
      image: {
        alt: 'Premie',
        src: '/static-site/assets/trophy.png'
      }
    })
  ),
  footer: {
    byline: { items: [{ text: 'Publisert 13.05.2018' }] },
    share: {
      openButtonText: 'Del',
      shareContent: {
        items: [
          { url: '#', text: 'Mail', icon: 'mail' },
          { url: '#', text: 'Twitter', icon: 'twitter' },
          { url: '#', text: 'Facebook', icon: 'facebook' },
          { url: '#', text: 'LinkedIn', icon: 'linkedin' }
        ]
      }
    },
    download: {
      url: '/',
      text: 'Last ned'
    }
  }
};
