/*
name: Informasjonsartikkel
group: CMS-maler
*/
import React from 'react';
import Layout from '../../layout';

import content from './info-page.js';

import ArticlePage from 'components/article-page';

const InfoPage = () => (
  <Layout>
    <ArticlePage {...content} />
  </Layout>
);

export default InfoPage;
