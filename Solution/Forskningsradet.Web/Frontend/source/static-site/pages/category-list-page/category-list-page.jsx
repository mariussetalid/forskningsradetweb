/*
group: CMS-maler
name: Kategorilisteside
path: category-page
 */

import React from 'react';
import Layout from '../../layout';

import CategoryListPage from 'components/category-list-page';

import content from './category-list-page.js';

const CategoryList = () => (
  <Layout>
    <CategoryListPage {...content} />
  </Layout>
);

export default CategoryList;
