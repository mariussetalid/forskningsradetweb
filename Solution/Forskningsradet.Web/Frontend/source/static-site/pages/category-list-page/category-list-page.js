import utils from '../../content-area-utils';

export default {
  title: 'Kategorilisteside',

  form: {
    endpoint: '/category-page'
  },

  fetchFilteredResultsEndpoint:
    '/static-site/api/filtered-category-list-result.json',
  emptyList: { text: 'Det er dessverre ingen utlysninger akkurat nå.' },
  filterLayout: {
    labels: {
      toggleFilters: 'Filter'
    },
    filters: {
      labels: {
        showResults: undefined,
        reset: 'Fjern alle filtre'
      },
      items: [
        {
          title: 'Temaer',
          accordion: {
            guid: 'asd0vunqwe0ctunqwet',
            expandLabel: 'Vis',
            collapseLabel: 'Skjul'
          },
          options: [
            {
              name: 'subjects',
              label: 'Arbeidsliv, velferd og integrering (3)',
              value: '1'
            },
            { name: 'subjects', label: 'Bioteknologi (2)', value: '2' },
            {
              name: 'subjects',
              label: 'Globalisering og utvikling (2)',
              value: '3'
            },
            { name: 'subjects', label: 'Humaniora (2)', value: '4' },
            { name: 'subjects', label: 'IKT (2)', value: '5' },
            { name: 'subjects', label: 'Klima og miljø (2)', value: '6' },
            {
              name: 'subjects',
              label: 'Landbruk, fiskeri og havbruk (2)',
              value: '7'
            }
          ]
        },
        {
          title: 'Målgruppe',
          accordion: {
            guid: 'ozdsufn0q9w38r7nc1',
            expandLabel: 'Vis',
            collapseLabel: 'Skjul'
          },
          options: [
            {
              name: 'targetGroups',
              label: 'Arbeidsliv, velferd og integrering (3)',
              value: '1',
              checked: true
            },
            {
              name: 'targetGroups',
              label: 'Bioteknologi (2)',
              value: '2',
              checked: true
            },
            {
              name: 'targetGroups',
              label: 'Globalisering og utvikling (2)',
              value: '3',
              checked: true
            },
            {
              name: 'targetGroups',
              label: 'Humaniora (2)',
              value: '4',
              checked: true
            }
          ]
        }
      ]
    },
    contentArea: utils.ContentArea(
      utils.InfoBlock({
        title: 'Lenkeliste',

        linkList: {
          items: [
            {
              url: '/',
              text: 'Forskningrådets prisar for unge framifrå forskarar'
            },
            {
              url: '/',
              text: 'Forskningrådets formidlingspris'
            },
            {
              url: '/',
              text: 'Forskningsrådets innovasjonspris'
            }
          ]
        }
      }),
      utils.PortfolioContent({
        title: 'Relevante porteføljer',
        content: utils.ContentArea(
          utils.LinkList({
            items: [
              {
                url: '/',
                text: 'Hav'
              },
              {
                url: '/',
                text: 'Infrastruktur'
              },
              {
                url: '/',
                text: 'Naturvitenskap og teknologi'
              }
            ]
          })
        )
      })
    )
  },

  groups: [
    {
      image: {
        alt: 'Space',
        src: '/static-site/assets/students.png'
      },
      title: { text: 'Nyhetssak med lenke på tittel', url: '#' },
      published: { type: 'Publisert', date: '21. september 2019' },
      metadata: {
        items: [
          {
            label: 'Søknadsfrist',
            text: '01.–02. september 2018'
          }
        ]
      },
      text:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus scelerisque sem vitae faucibus viverra. Phasellus non augue lacus. Lorem ipsum dolor sit amet. Sem vitae faucibus viverra et non augue lacus. Lorem ipsum dolor sit amet.',
      linkTags: {
        leftAligned: true,
        tags: [
          { link: { text: 'Hovedtema', url: '?1' } },
          { link: { text: 'Undertema' }, inactive: true },
          { link: { text: 'Tilknyttet tema', url: '?3' } },
          { link: { text: 'Tema under', url: '?4' } }
        ]
      },

      tags: {
        tags: [{ text: 'Tilknyttet tema' }, { text: 'Tema under' }]
      }
    },
    {
      title: {
        text:
          'Nyhetssak med lengre tittel – kun én dato synes i byline, enten publisert eller sist oppdatert',
        url: '#'
      },
      published: { type: 'Publisert', date: '3. oktober 2019' },
      text:
        'Scelerisque sem vitae faucibus viverra, phasellus non augue lacus. Lorem ipsum dolor sit amet. Consectetur adipiscing elit sem vitae faucibus viverra et non augue lacus. ',
      linkTags: {
        leftAligned: true,
        tags: [
          { link: { text: 'Hovedtema', url: '?1' } },
          { link: { text: 'Undertema' }, inactive: true },
          { link: { text: 'Tilknyttet tema', url: '?3' } },
          { link: { text: 'Tema under', url: '?4' } }
        ]
      },

      tags: {
        tags: [{ text: 'Tilknyttet tema' }, { text: 'Tema under' }]
      }
    }
  ],
  pagination: {
    title: 'Navigasjon for søkeresultater',
    pages: [
      {
        isCurrent: false,
        label: 'Forrige side',
        link: {
          url: '/?0',
          text: 'Forrige'
        }
      },
      {
        isCurrent: false,
        label: 'Side 1',
        link: {
          url: '/?1',
          text: '1'
        }
      },
      {
        isCurrent: false,
        label: 'Side 2',
        link: {
          url: '/?2',
          text: '2'
        }
      },
      {
        isCurrent: true,
        label: 'Side 3',
        link: {
          url: '/?3',
          text: '3'
        }
      },
      {
        isCurrent: false,
        label: 'Side 4',
        link: {
          url: '/?4',
          text: '4'
        }
      },
      {
        isCurrent: false,
        label: 'Side 5',
        link: {
          url: '/?5',
          text: '5'
        }
      },
      {
        isCurrent: false,
        label: 'Side 6',
        link: {
          url: '/?6',
          text: '6'
        }
      },
      {
        isCurrent: false,
        label: 'Side 7',
        link: {
          url: '/?7',
          text: '7'
        }
      },
      {
        isCurrent: false,
        label: 'Side 8',
        link: {
          url: '/?8',
          text: '8'
        }
      },
      {
        isCurrent: false,
        label: 'Neste side',
        link: {
          url: '/?9',
          text: 'Neste'
        }
      }
    ]
  }
};
