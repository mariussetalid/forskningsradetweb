/*
name: Alternativ/ny utlysning
group: Utlysninger
*/
import React from 'react';
import Layout from '../../layout';

import content from './proposal.js';

import ProposalPage from 'components/proposal-page';

const Proposal = () => (
  <Layout>
    <ProposalPage {...content} />
  </Layout>
);

export default Proposal;
