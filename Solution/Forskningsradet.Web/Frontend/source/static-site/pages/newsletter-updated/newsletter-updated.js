export default {
  layout: {
    header: {
      text: 'Forskningsrådet',
      url: '/'
    }
  },
  title: 'Ditt nyhetsbrev er oppdatert',
  emailTitle: 'E-postvarslinger sendes til',
  emailText: 'n***.******@@m***.com',
  categoriesTitle: 'Du ønsker å abonnere på oppdateringer innen',
  categoriesList: [
    {
      name: 'Nyheter',
      description:
        'Bioteknologi, IKT, Samfunn/Vitenskap, Nano- og materialteknologi'
    },
    {
      name: 'Arrangementer',
      description: 'Bioteknologi'
    },
    {
      name: 'Utlysninger',
      description: 'Bioteknologi, IKT, Samfunn/Vitenskap'
    },
    {
      name: 'Søknadsresultater'
    }
  ]
};
