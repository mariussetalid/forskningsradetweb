/*
name: Bekreftelse på oppdaterte innstillinger
group: Nyhetsbrev
*/

import React from 'react';

import NewsletterUpdatedPage from 'components/newsletter-updated-page';

import content from './newsletter-updated.js';

const NewsletterUpdated = () => <NewsletterUpdatedPage {...content} />;

export default NewsletterUpdated;
