/*
name: Blokker
*/
import React from 'react';
import Layout from '../../layout';
import content from './blocks.js';
import ContentAreaItem from 'components/content-area-item';
import ContentContainer from 'components/content-container';

let previousComponent;

const Blocks = () => (
  <Layout>
    <ContentContainer>
      <div className="content-area -flex-layout">
        {content.blocks.map(block => {
          const sameAsPrevious = previousComponent === block.componentName;
          previousComponent = block.componentName;
          return (
            <React.Fragment key={block.id}>
              {!sameAsPrevious && (
                <h2 style={{ width: '100%', fontWeight: 600 }}>
                  {block.componentName}
                </h2>
              )}
              <ContentAreaItem enableElementSizing {...block} />
            </React.Fragment>
          );
        })}
      </div>
    </ContentContainer>
  </Layout>
);

export default Blocks;
