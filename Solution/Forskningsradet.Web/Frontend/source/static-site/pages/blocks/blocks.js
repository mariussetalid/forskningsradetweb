import utils from '../../content-area-utils';

const loremText =
  'Vestibulum lobortis scelerisque tincidunt. Curabitur sem odio, pulvinar eget ex et, dictum sagittis ex. Morbi purus velit, cursus eu nibh a, viverra porttitor sem. Nulla facilisi. Vivamus sagittis rutrum facilisis. Ut sed dignissim felis. Proin mattis ultrices quam vitae scelerisque.';

export default {
  blocks: [
    utils.Link({
      isExternal: true,
      text: 'Ekstern lenke',
      url: '#'
    }),
    utils.Link({
      isExternal: true,
      text: 'Ekstern lenke',
      url: '#',
      theme: '-theme-small'
    }),
    utils.Link({
      isExternal: true,
      text: 'Ekstern lenke',
      url: '#',
      theme: '-theme-portfolio'
    }),
    utils.Link({
      isExternal: true,
      text: 'Primærknapp',
      theme: '-theme-fill',
      useButtonStyles: true,
      url: '#'
    }),
    utils.Link({
      isExternal: true,
      text: 'Primærknapp',
      theme: ['-theme-fill', '-theme-big'],
      useButtonStyles: true,
      url: '#'
    }),
    utils.Link({
      isExternal: true,
      text: 'sekundærknapp',
      theme: ['-theme-orange-outline', '-theme-big'],
      useButtonStyles: true,
      url: '#'
    }),
    utils.Link({
      isExternal: true,
      text: 'sekundærknapp',
      theme: '-theme-orange-outline',
      useButtonStyles: true,
      url: '#'
    }),
    utils.PortfolioContent({
      title: 'Hvordan blir investeringene fordelt',
      content: utils.ContentArea(
        utils.StatsLinkList({
          statsLinks: [
            {
              image: { src: '/static-site/assets/lightbulb.svg', alt: 'icon' },
              title: '3 006 mill',
              link: {
                url: '#',
                text: 'Til styrket konkurransekraft og innovasjonsevne'
              }
            },
            {
              image: { src: '/static-site/assets/iceblock.svg', alt: 'icon' },
              title: '4 720 mill',
              link: {
                url: '#',
                text: 'Til å møte de store samfunnsutfordringene'
              }
            },
            {
              image: { src: '/static-site/assets/people.svg', alt: 'icon' },
              title: '2 600 mill',
              link: {
                url: '#',
                text: 'Utvikle fagmiljøer av fremragende kvalitet'
              }
            }
          ]
        })
      )
    }),
    utils.PortfolioContent({
      title: 'Hvordan blir investeringene fordelt',
      content: utils.ContentArea(
        utils.StatsLinkList({
          statsLinks: [
            {
              image: { src: '/static-site/assets/lightbulb.svg', alt: 'icon' },
              title: '3 006',
              link: {
                url: '#',
                text: 'Til styrket konkurransekraft og innovasjonsevne'
              }
            },
            {
              image: { src: '/static-site/assets/iceblock.svg', alt: 'icon' },
              title: '4 720',
              link: {
                url: '#',
                text:
                  'Til å møte de store samfunnsutfordringene og mye annen problematikk som du kan lese mer om dersom du klikker teksten her som fører deg til mer innhold om denne saken.'
              }
            }
          ]
        })
      )
    }),
    utils.PortfolioContent({
      title: 'Hvordan blir investeringene fordelt',
      content: utils.ContentArea(
        utils.StatsLinkList({
          statsLinks: [
            {
              image: { src: '/static-site/assets/lightbulb.svg', alt: 'icon' },
              title: '3 006 mill',
              link: {
                url: '#',
                text: 'Til styrket konkurransekraft og innovasjonsevne'
              }
            }
          ]
        })
      )
    }),
    utils.FrontpageHeader({
      theme: '-theme-dark-blue',
      title:
        'Senterordningen (SFF) gir forskningsmiljøene en unik  mulighet for samarbeid',
      text:
        'Vi investerer i forskning og innovasjon som bygger kunnskap for en bærekraftig framtid.',
      image: { src: '/static-site/assets/students.png', alt: 'students' },
      links: [
        { text: 'Les mer', url: '?1', isPrimary: true },
        { text: 'Se utlysningen', url: '?2' }
      ]
    }),
    utils.FrontpageHeader({
      theme: '-theme-grey-blue',
      title:
        'Senterordningen (SFF) gir forskningsmiljøene en unik  mulighet for samarbeid Senterordningen (SFF) gir forskningsmiljøene en unik  mulighet for samarbeid',
      text:
        'Vi investerer i forskning og innovasjon som bygger kunnskap for en bærekraftig framtid.',
      image: { src: '/static-site/assets/smartphone.png', alt: 'students' },
      links: [{ text: 'Les mer', url: '?1', isPrimary: true }]
    }),
    utils.FrontpageHeader({
      theme: '-theme-purple',
      title:
        'Senterordningen (SFF) gir forskningsmiljøene en unik  mulighet for samarbeid Senterordningen (SFF) gir forskningsmiljøene en unik  mulighet for samarbeid',
      text:
        'Vi investerer i forskning og innovasjon som bygger kunnskap for en bærekraftig framtid. Vi investerer i forskning og innovasjon som bygger kunnskap for en bærekraftig framtid.',
      image: { src: '/static-site/assets/tto-lederne.png', alt: 'students' },
      links: [
        { text: 'Les mer om dette her', url: '?1', isPrimary: true },
        { text: 'Se utlysningen eller noe', url: '?2' }
      ]
    }),
    utils.FrontpageHeader({
      theme: '-theme-grey-blue',
      title: 'Senterordningen',
      text:
        'Vi investerer i forskning og innovasjon som bygger kunnskap for en bærekraftig framtid.',
      image: { src: '/static-site/assets/students.png', alt: 'students' },
      links: [
        { text: 'Les mer', url: '?1', isPrimary: true },
        { text: 'Se utlysningen', url: '?2' }
      ]
    }),
    utils.CampaignBlockList({
      list: [
        {
          cta: { url: '#', text: 'Se forslagene' },
          image: { src: '/static-site/assets/students.png', alt: 'students' },
          title:
            'Innovasjon og kunnskapsbygging i næringslivet gjør Norges verdiskaping mer bærekraftig i framtiden. '
        },
        {
          cta: { url: '#', text: 'Se forslagene' },
          image: {
            src: '/static-site/assets/smartphone.png',
            alt: 'students'
          },
          title:
            'Forskning på avanserte materialer er en prioritert satsing i regjeringens langtidsplan',
          editorTheme: '-theme-dark-blue'
        }
      ]
    }),
    utils.CampaignBlockList({
      list: [
        {
          cta: { url: '#', text: 'Se forslagene' },
          image: { src: '/static-site/assets/students.png', alt: 'students' },
          title:
            'Innovasjon og kunnskapsbygging i næringslivet gjør Norges verdiskaping mer bærekraftig i framtiden. '
        }
      ]
    }),
    utils.CampaignBlockList({
      list: [
        {
          cta: { url: '#', text: 'Se' },
          title: 'Norges forslag',
          editorTheme: '-theme-dark-blue'
        }
      ]
    }),
    utils.CampaignBlockList({
      list: [
        {
          cta: { url: '#', text: 'les mer om artikkelen her sånn' },
          title: 'Norges forslag til satsinger for FNs havforskningstiår'
        },
        {
          cta: { url: '#', text: 'les mer' },
          image: {
            src: '/static-site/assets/smartphone.png',
            alt: 'students'
          },
          title:
            'Forskning på avanserte materialer er en prioritert satsing i regjeringens langtidsplan'
        }
      ]
    }),
    utils.CampaignBlock({
      cta: { url: '#', text: 'les mer' },
      editorTheme: '-theme-dark-blue',
      image: {
        src: '/static-site/assets/smartphone.png',
        alt: 'students'
      },
      title:
        'Forskning på avanserte materialer er en prioritert satsing i regjeringens langtidsplan'
    }),
    utils.InfoBlock(
      {
        icon: { alt: 'money icon', src: '/static-site/assets/icon-money.png' },
        title: 'Teknologioverførings-kontorer (TTO)',
        onPageEditing: { title: 'Title' },
        text: utils.RichText(
          'FORNY2020 samarbeider med 10 teknologioverføringskontorer. Her finner du en ovesikt over TTOene.'
        ),
        url: '/article'
      },
      '-size-half'
    ),
    utils.InfoBlock(
      {
        icon: { alt: 'money icon', src: '/static-site/assets/icon-money.png' },
        title: 'Teknologioverførings-kontorer (TTO)',
        text: utils.RichText(
          'FORNY2020 samarbeider med 10 teknologioverføringskontorer. Her finner du en ovesikt over TTOene.'
        ),
        url: '/article'
      },
      '-size-half'
    ),
    utils.InfoBlock(
      {
        title: 'Teknologioverførings-kontorer (TTO)',
        text: utils.RichText(
          'FORNY2020 samarbeider med 10 teknologioverføringskontorer. Her finner du en ovesikt over TTOene.'
        ),
        url: '/article'
      },
      '-size-third'
    ),
    utils.InfoBlock(
      {
        title: 'Teknologioverførings-kontorer (TTO)',
        text: utils.RichText(
          'FORNY2020 samarbeider med 10 teknologioverføringskontorer. Her finner du en ovesikt over TTOene.'
        ),
        url: '/article'
      },
      '-size-third'
    ),
    utils.InfoBlock(
      {
        title: 'Teknologioverførings-kontorer (TTO)',
        text: utils.RichText(
          'FORNY2020 samarbeider med 10 teknologioverføringskontorer. Her finner du en ovesikt over TTOene.'
        ),
        url: '/article'
      },
      '-size-third'
    ),
    utils.InfoBlock(
      {
        editorTheme: '-theme-dark-blue',
        title: 'Teknologioverførings-kontorer (TTO)',
        onPageEditing: { title: 'Title' },
        text: utils.RichText(
          'FORNY2020 samarbeider med 10 teknologioverføringskontorer. Her finner du en ovesikt over TTOene.'
        ),
        url: '/article'
      },
      '-size-half'
    ),
    utils.InfoBlock(
      {
        editorTheme: '-theme-dark-blue',
        title: 'Teknologioverførings-kontorer (TTO)',
        text: utils.RichText(
          'FORNY2020 samarbeider med 10 teknologioverføringskontorer. Her finner du en ovesikt over TTOene.'
        ),
        url: '/article'
      },
      '-size-half'
    ),
    utils.NumberBlock(
      {
        number: '23,7',
        description: 'prosent',
        text: 'Tekst på en linje',
        url: '#',
        onPageEditing: {
          number: 'Number',
          description: 'Description',
          text: 'Text'
        }
      },
      '-size-half'
    ),
    utils.NumberBlock(
      {
        number: '89,5',
        description: 'NOK millioner',
        text: 'Programmets budsjettramme for 2018'
      },
      '-size-half'
    ),
    utils.NumberBlock(
      {
        number: '89,5',
        description: 'NOK millioner',
        text: 'Programmets budsjettramme for 2018'
      },
      '-size-third'
    ),
    utils.NumberBlock(
      {
        number: '23,7',
        description: 'prosent',
        text: 'Tekst på en linje'
      },
      '-size-third'
    ),
    utils.NumberBlock(
      {
        number: '23753',
        description: 'gule bakterieprøver',
        text: 'Tekst på flere linjer og kanskje så mange som tre hele linjer'
      },
      '-size-third'
    ),
    utils.TimelineBlock({
      labels: {
        collapse: 'Vis mindre',
        expand: 'Vis mer',
        next: 'Neste',
        previous: 'Forrige'
      },
      items: [
        {
          title: '02 aug 2018',
          subTitle: 'Åpent for søknad',
          text: utils.RichText(loremText),
          isLast: false,
          progress: 0,
          isPastDate: true
        },
        {
          title: '02 aug 2018',
          subTitle: 'Søknadsfrist',
          text: utils.RichText(loremText),
          isLast: false,
          progress: 0,
          isPastDate: true
        },
        {
          title: '02 aug 2018',
          subTitle: 'Utsendelse av svar',
          text: utils.RichText(loremText),
          isLast: false,
          progress: 0,
          isPastDate: true
        },
        {
          title: '02 aug 2018',
          subTitle: 'Prosjektstart',
          text: utils.RichText(loremText),
          isLast: false,
          progress: 0,
          isPastDate: true
        },
        {
          title: '02 aug 2018',
          subTitle: 'Leveringsfrist',
          text: utils.RichText(loremText),
          isLast: false,
          progress: 50,
          isPastDate: true
        },
        {
          subTitle: 'Lorem ipsum',
          title: 'Midnatt 15. august',
          text: utils.RichText(loremText),
          isLast: true,
          progress: 0,
          isPastDate: false
        }
      ]
    }),
    utils.ImageBlock(
      {
        title: 'Tittel',
        image: {
          alt: 'Space',
          src: '/static-site/assets/students.png'
        }
      },
      '-size-half'
    ),
    utils.ImageBlock(
      {
        image: {
          alt: 'Space',
          src: '/static-site/assets/students.png'
        }
      },
      '-size-half'
    ),
    utils.ImageBlock({
      title: 'Tittel',
      image: {
        alt: 'Space',
        src: '/static-site/assets/students.png'
      }
    }),
    utils.ImageBlock({
      image: {
        alt: 'Space',
        src: '/static-site/assets/students.png'
      }
    }),
    utils.CampaignBlock({
      cta: {
        text: 'Call to action',
        url: '/article'
      },
      image: {
        alt: 'Space',
        src: '/static-site/assets/students.png'
      },
      title: 'Kampanjefelt med kort, fengende budskap'
    }),
    utils.CampaignBlock(
      {
        cta: {
          text: 'Call to action',
          url: '/article'
        },
        image: {
          alt: 'Space',
          src: '/static-site/assets/students.png'
        },
        title:
          'Kampanjefelt med kort, fengende budskap. Denne har litt lengre tekst så den blir litt høyere'
      },
      '-size-full-screen'
    ),
    utils.EmbedBlock({
      src:
        'https://www.forskningsradet.no/prosjektbanken/embed.html?Kilde=FORISS&distribution=Fag&chart=bar&calcType=funding&Sprak=no&sortBy=date&sortOrder=desc&Departement=N%C3%A6rings-%20og%20fiskeridepartementet&Departement=N%C3%A6rings-%20og%20handelsdepartementet&width=800&height=300&showSum=false&customHeading=Kanin',
      height: 300,
      width: 800,
      title: 'En graf'
    }),
    utils.EmbedBlock({
      src:
        'https://public.tableau.com/views/Kjnnsbalansebarometerapril2018/Utviklingperinstitusjon?:embed=y&:display_count=yes&:showVizHome=no',
      title: 'Tableau-graf'
    }),
    utils.HtmlString({
      text:
        '<p>I første omgang henvender Forskningsrådet seg direkte til forskerprosjekter i sju programmer (mer om det i rammen lenger nede). Grunnforskning fører sjelden til mer eller annet enn publisering, siden det er det som blir belønnet og gir poeng i akademia. Men resultater fra grunnforskning kan også innebære funn som kan få direkte samfunnsnytte, og få en markedsverdi, dersom man ser på mulighetene.</p><p>Du må ha mer enn en idé – noen lovende resultater, noe håndfast og nytt. Men det viktigste er at du er interessert i å gjøre noe mer. Ta kontakt med TTO-en ved institusjonen din så tidlig du kan for å få vurdert mulighetene for kommersialisering av resultater, oppfordrer Eirik Wasmuth Lundblad i NORINNOVA, leder for teknologioverføring (TTO) i Tromsø.</p><p>Hvis det er mulig å lage en prototyp, er det en fordel – men ingen forutsetning, sier Stein Eggan, daglig leder i NTNU Technology Transfer.</p><p>En annen ting: Husk alltid patentering før publisering! Om arbeidet ditt er publisert, vil det være umulig å patentere. Presentasjon på konferanser er et eksempel på publisering. Etter patentering er det derimot mulig å publisere. Derfor bør du ta kontakt med TTO-en din tidlig for å få en vurdering, om du er interessert i kommersialisering, anbefaler Eggan.</p><p>Så snart andre har fått vite om funnene dine, blir det vanskeligere å beskytte funnene og finne investorer som vil satse penger på å bringe resultatene fram til anvendelse i form av et produkt eller en tjeneste, opplyser Lundblad.</p><h3>Lag en plan før publisering</h3><p>Det behøver ikke ta lang tid. I møtet med TTO-representantene vil dere sammen vurdere kommersialiseringsmulighetene, og samtidig legge opp en plan for dette og publisering.</p><p>Det kan hende møtet konkluderer med at publisering er det riktige å gjøre.</p><p>Det kan også hende at resultatene kan publiseres delvis, mens andre deler kan vente, dersom noen resultater kan være interessante for kommersialisering.</p><p>Så snart resultater er publisert, får du ikke patentbeskyttelse. Personer eller industri som er villig til å investere penger i forskningsresultater for å utvikle et produkt eller en tjeneste, trenger ofte å få patentbeskyttelse for dette, så det kan gi avkastning på sikt.</p><p>Et typisk eksempel er nye medisiner, som det tar lang tid å utvikle. Det krever ofte store investeringer i form av penger og ressurser. Det koster veldig mye før medisinen kommer på markedet. Hvis det ikke er mulig å få patentbeskyttelse og dermed eksklusivt tjene penger på produktet en gang i fremtiden, blir det ikke investert i utviklingen av produktet.</p><p>– Mye grunnforskning har ingen kommersialiseringsverdi i seg selv, men når du har resultater, ta kontakt med oss uansett. Det hender vi ser ting på andre måter enn forskerne selv gjør. Forskere på høyt nivå innenfor sitt fagområde kan ofte sitte med resultater de ikke selv aner anvendelsesmulighetene av. Hvis forskeren holder igjen publiseringen bare litt og lar oss få se på hva de har, øker det sjansen for at resultatene kan komme til anvendelse. La oss se på det i fellesskap! oppfordrer Stein Eggan.</p><h3>Fra ideer til konsept</h3><p>– Men forskeren må også være klar over at et kommersialiseringsprosjekt krever tid, når resultater tas videre. Det er ikke bare å lene seg tilbake og vente på hva som kommer ut av det. Forskeren må involvere og engasjere seg. Da er det mange spennende muligheter. Det kan gi muligheter for mer forskning, mer finansiering, mer oppmerksomhet, og institusjonen kan bli mer attraktiv både som studiested og samarbeidspartner. Når du starter et selskap og tar ut et patent – det er da jobben begynner. Mange synes det er morsomt og blir veldig motivert av det, forteller Eggan.</p><p>– De fleste forskere vil ikke bli gründere og bør trolig heller ikke bli gründere. Forsker og TTO samarbeider om å utvikle ideene frem til et konsept som kan tas over av investorer eller bedrifter. Dette er den vanligste formen for kommersialisering av forskningsresultater. Da kan forskeren fortsette i sin jobb og samtidig bidra til verdiskaping i samfunnet i form av at forskningsfunn danner grunnlag for nyetablering av bedrifter eller styrking av etablerte bedrifter, forklarer Eirik Lindblad.</p><p>Forskeren skal ikke bruke egne prosjektmidler eller søke midler til kommersialisering på egen hånd. TTO-en søker sammen med forskeren eller gjør mye av jobben med å skaffe til veie midlene som trengs for testing, verifisering og kommersialisering.</p><p>Et første steg etter møtet i din TTO, kan være å søke verfiseringsmidler fra Forskningsrådet. Det får du hjelp til fra dine støttespillere i TTO-en. <b>Søknaden må være hos Forskningsrådet innen 10. oktober.</b> </p><p>I 2017 delte Forskningsrådet ut over 50 millioner kroner i kommersialiseringsstøtte til 15 prosjekter med forskningsrsultater som kan gi verdiskaping.</p>'
    }),
    utils.YoutubeVideo(
      {
        urlOrEmbed:
          '<iframe width="560" height="315" src="https://www.youtube.com/embed/GN-peWeNBkU?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>'
      },
      '-size-half'
    ),
    utils.YoutubeVideo(
      {
        urlOrEmbed: 'https://youtu.be/y8Ezdbz93GM'
      },
      '-size-half'
    )
  ]
};
