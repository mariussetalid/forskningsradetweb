/*
name: Listeside
group: Utlysninger
*/

import React from 'react';
import Layout from '../../layout';

import content from './proposal-list.js';

import ProposalListPage from 'components/proposal-list-page';

const ProposalList = () => (
  <Layout>
    <ProposalListPage {...content} />
  </Layout>
);

export default ProposalList;
