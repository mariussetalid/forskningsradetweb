import utils from '../../content-area-utils';
import dateCard from 'mock/components/date-card';
export default {
  title: 'Utlysninger',
  sectionTitle: 'Utlystninger som er lukket for søking',
  fetchFilteredResultsEndpoint:
    '/static-site/api/filtered-proposal-list-result.json',
  form: {
    endpoint: '/proposal-list'
  },
  emptyList: { text: 'Det er dessverre ingen utlysninger akkurat nå.' },
  topFilter: {
    timeframe: {
      value: 1,
      name: 'timeframe',
      id: 'timeframe'
    },
    options: [
      { link: { text: 'Aktuelle', url: '/proposal-list?timeframe=future' } },
      {
        isCurrent: true,
        link: {
          text: 'Gjennomførte',
          url: '/proposal-list?timeframe=completed'
        }
      },
      {
        link: {
          text: 'Søknadsresultater',
          url: '/proposal-list?timeframe=results'
        }
      },
      {
        link: {
          text: 'Søknader til behandling',
          url: '/proposal-list?timeframe=pending'
        }
      }
    ]
  },
  filterLayout: {
    labels: {
      toggleFilters: 'Filter'
    },
    filters: {
      items: [
        {
          title: 'Fagområde',
          accordion: {
            guid: 'asd0vunqwe0ctunqwet',
            expandLabel: 'Vis',
            collapseLabel: 'Skjul'
          },
          options: [
            {
              name: 'subjects',
              label: 'Arbeidsliv, velferd og integrering (3)',
              value: '1'
            },
            { name: 'subjects', label: 'Bioteknologi (2)', value: '2' },
            {
              name: 'subjects',
              label: 'Globalisering og utvikling (2)',
              value: '3'
            },
            { name: 'subjects', label: 'Humaniora (2)', value: '4' },
            { name: 'subjects', label: 'IKT (2)', value: '5' },
            { name: 'subjects', label: 'Klima og miljø (2)', value: '6' },
            {
              name: 'subjects',
              label: 'Landbruk, fiskeri og havbruk (2)',
              value: '7'
            }
          ]
        },
        {
          title: 'Målgruppe',
          accordion: {
            guid: 'ozdsufn0q9w38r7nc1',
            expandLabel: 'Vis',
            collapseLabel: 'Skjul'
          },
          options: [
            {
              name: 'targetGroups',
              label: 'Arbeidsliv, velferd og integrering (3)',
              value: '1',
              checked: true
            },
            {
              name: 'targetGroups',
              label: 'Bioteknologi (2)',
              value: '2',
              checked: true
            },
            {
              name: 'targetGroups',
              label: 'Globalisering og utvikling (2)',
              value: '3',
              checked: true
            },
            {
              name: 'targetGroups',
              label: 'Humaniora (2)',
              value: '4',
              checked: true
            }
          ]
        },
        {
          title: 'Søknadsfrist',
          accordion: {
            guid: 'ozufnfc0q9385nv1qpw0',
            expandLabel: 'Vis',
            collapseLabel: 'Skjul'
          },
          options: []
        }
      ]
    }
  },
  groups: [
    {
      accordion: {
        guid: 'f98ynq34t098n731409tv87qnopigfun',
        collapseLabel: 'Skjul 2 utlysninger',
        expandLabel: '+ 2 utlysninger'
      },
      headerAccordion: {
        guid: 'f98ynq34t098n731409tv87qndfdopigfun',
        collapseLabel: 'Skjul',
        expandLabel: 'Vis mer'
      },
      numberOfVisibleItems: 6,
      title: 'Forskerprosjekter (8 utlysninger)',
      subtitle: 'Gjelder for forskningsorganisasjoner',
      text: utils.RichText(
        'Forskerprosjekter skal bidra til vetenskaplig fornyelse og utvikling av fagene og/eler ny kunnskap om relevante problemstillinger i samfunnet. Les mer om <a href="#">forskerprosjekter</a>.'
      ),
      proposals: [
        dateCard(
          {
            dateContainer: {
              isInternational: false
            }
          },
          13522
        ),
        dateCard(
          {
            dateContainer: {
              isInternational: false
            }
          },
          234
        ),
        dateCard(
          {
            status: {
              theme: '-is-canceled',
              text: 'Kansellert'
            },
            dateContainer: {
              isInternational: false
            }
          },
          3
        ),
        dateCard(
          {
            dateContainer: {
              isInternational: false
            }
          },
          34568
        ),
        dateCard(
          {
            dateContainer: {
              isInternational: false
            }
          },
          234578
        ),
        dateCard(
          {
            dateContainer: {
              isInternational: false
            }
          },
          254
        ),
        dateCard(
          {
            dateContainer: {
              isInternational: false
            }
          },
          3474568
        ),
        dateCard(
          {
            dateContainer: {
              isInternational: false
            }
          },
          234987578
        )
      ]
    },
    {
      accordion: {
        guid: 'f98ynq34t098n731409fdtv87qnopigfun',
        collapseLabel: 'Skjul 2 utlysninger',
        expandLabel: '+ 2 utlysninger'
      },
      headerAccordion: {
        guid: 'f98ynq34t09s8n731409tv87qndfdopigfun',
        collapseLabel: 'Skjul',
        expandLabel: 'Vis mer'
      },
      numberOfVisibleItems: 2,
      title: 'Internasjonale utlysninger (8 utlysninger)',
      subtitle: 'Gjelder for forskningsorganisasjoner',
      text: utils.RichText(
        'Forskerprosjekter skal bidra til vetenskaplig fornyelse og utvikling av fagene og/eler ny kunnskap om relevante problemstillinger i samfunnet. Les mer om <a href="#">forskerprosjekter</a>.'
      ),
      proposals: [
        dateCard(
          {
            dateContainer: {
              isInternational: true
            }
          },
          233
        ),

        dateCard(
          {
            dateContainer: {
              isInternational: true
            }
          },
          2733
        ),
        dateCard(
          {
            dateContainer: {
              isInternational: true
            }
          },
          23653
        ),
        dateCard(
          {
            dateContainer: {
              isInternational: true
            }
          },
          2254
        ),
        dateCard(
          {
            dateContainer: {
              isInternational: true
            }
          },
          2727
        )
      ]
    }
  ]
};
