import utils from '../../content-area-utils';

export default {
  title: 'Nyhetsbrev',
  richText: utils.RichText(
    '<p>Discere iracundia contentiones cu mei. Ut tacimates democritum usu, ea vim spesifisert i utlysning. Omnium utamur mediocritatem duo eu, ei simul perfecto has. Tempor tamquam scaevola sit et, pri veri eruditi in.</p><p>Husk at * betyr at utfylling er påkrevd.</p>'
  ),
  htmlString:
    '<input name="__RequestVerificationToken" type="hidden" value="K_krbcnN2gj41ZIF21c4Zk1cEUD7vsu8RgzLsfz8GqMwWrCszzk8JgubPwTfHUvsYZeGikKnO_DkewJFgy_xiegbxjyM-GqDcYbYlBbMqyIfHJZ2RHAoameYmA4JEA4QI4XPvlBxSxzDsE_ovc9JiA2" />',
  sidebar: utils.ContentArea(
    utils.InfoBlock({
      title: 'Newsletter in english',
      text: utils.RichText(
        '<p>Lorem ipsum dolor sit amet, choro civibus eu sed, graeco evertitur sed at, invidunt voluptaria mea an. Set commodo vocibus at mea, augue inpercipit ne vis at erat soluta audiam sit:</p>'
      ),
      url: '/',
      editorTheme: '-theme-blue'
    })
  ),
  form: { endpoint: '/newsletter' },
  inputTitle:
    'Oppgi den e-postadressen du ønsker å få oppdateringer tilsendt til',
  inputFields: [
    {
      label: 'E-postadresse*',
      name: 'e-post'
    },
    {
      label: 'Gjenta e-postadresse*',
      name: 'gjenta-e-post'
    }
  ],
  submitButton: {
    text: 'Send påmelding'
  },
  footer: {
    share: {
      openButtonText: 'Del',
      shareContent: {
        items: [
          { url: '#', text: 'Mail', icon: 'mail' },
          { url: '#', text: 'Twitter', icon: 'twitter' },
          { url: '#', text: 'Facebook', icon: 'facebook' },
          { url: '#', text: 'LinkedIn', icon: 'linkedin' }
        ]
      }
    }
  }
};
