/*
name: Påmelding Mailchimp
group: Nyhetsbrev
path: newsletter-mailchimp
*/
import React from 'react';
import Layout from '../../layout';

import NewsletterRegistrationPage from 'components/newsletter-registration-page';

import content from './newsletter-registration-mailchimp.js';

const NewsletterRegistrationMailchimp = () => (
  <Layout>
    <NewsletterRegistrationPage {...content} />
  </Layout>
);

export default NewsletterRegistrationMailchimp;
