/*
name: Utlysning
group: Utlysninger
*/
import React from 'react';
import Layout from '../../layout';

import content from './old-proposal.js';

import ProposalPage from 'components/proposal-page';

const OldProposal = () => (
  <Layout>
    <ProposalPage {...content} />
  </Layout>
);

export default OldProposal;
