import utils from '../../content-area-utils';

const loremText =
  '<p>Vestibulum <a href="#">lobortis</a> scelerisque tincidunt. Curabitur sem odio, pulvinar eget ex et, dictum sagittis ex. Morbi purus velit, cursus eu nibh a, viverra porttitor sem. Nulla facilisi. Vivamus sagittis rutrum facilisis. Ut sed dignissim felis. Proin mattis ultrices quam vitae scelerisque.</p><ul><li>Vivamus sagittis rutrum facilisis</li><li>Curabitur sem odio, pulvinar eget ex et, dictum sagittis ex</li></ul>';

export default {
  title: 'Unge forskertalenter',
  header: {
    accordion: {
      guid: '92837n4tv09w8erugnvp20nu',
      collapseLabel: 'Vis mindre',
      expandLabel: 'Vis mer'
    },
    byline: {
      items: [
        { text: 'Sist oppdatert 20.mai 2918' },
        { text: 'Publisert 13. april 2018' }
      ]
    },
    download: {
      url: '/',
      text: 'Last ned'
    }
  },
  message: {
    text: {
      text:
        '<b>Viktig/kritisk overordnet varsel.</b> Denne utlysningen vil oppdateres fortløpende fram til seks uker før søknadsfrist. Når utlysningen er aktivert vil du ha mulighet til å opprette og sende inn søknad.'
    }
  },
  downloadLinks: [
    { text: 'Last ned utlysningen som PDF', url: '/?pdf' },
    { text: 'Last ned mal for søknadsskjema', url: '#download-templates' }
  ],
  share: {
    openButtonText: 'Del utlysningen',
    shareContent: {
      items: [
        { url: '#', text: 'Mail', icon: 'mail' },
        { url: '#', text: 'Twitter', icon: 'twitter' },
        { url: '#', text: 'Facebook', icon: 'facebook' },
        { url: '#', text: 'LinkedIn', icon: 'linkedin' }
      ]
    }
  },
  download: {
    openButtonText: 'Last ned utlysning og maler',
    downloadContent: {
      groups: [
        {
          heading: 'Last ned utlysning',
          items: [
            {
              iconTheme: 'word',
              url: '#',
              text: 'Midler til forskning på dyrehelse.pdf'
            }
          ]
        },
        {
          heading: 'Last ned maler',
          items: [
            {
              iconTheme: 'word',
              url: '#',
              text:
                'Mal for prosjektbeskrivelse Kompetanse- og samarbeidsprosjekt/Project description template Collaborative and Knowledge-building Project (docx)'
            },
            {
              iconTheme: 'pdf',
              url: '#',
              text: 'CV-mal for forskere/CV template researchers (docx)'
            },
            {
              iconTheme: 'pdf',
              url: '#',
              text: 'CV-mal/CV template.docx'
            }
          ]
        }
      ],
      downloadAllText: 'Last ned alle',
      downloadAllUrl: '#'
    }
  },
  contact: {
    title: 'Ola Nordmann',
    details: [
      { text: 'Spesialrådgiver' },
      { text: 'Næringsliv og teknologi' },
      {
        url: 'mailto:omr@forskningsradet.no',
        text: 'omr@forskningsradet.no'
      },
      { url: 'tel:12345678', text: '+47 12345678' }
    ]
  },
  contactLabel: 'Kontakt',
  statusList: [
    {
      theme: '-is-active',
      text: 'SØK NÅ - trinn 2'
    }
  ],
  metadataLeft: {
    items: [
      {
        label: 'Utlysningstype',
        links: [
          {
            url: '/',
            text: 'Forskningsprosjekt'
          }
        ]
      },
      {
        label: 'Søknadsfrist',
        text: '24. november 2018, klokken 13:00 CEST'
      },
      {
        label: 'Prosjektstørrelse',
        text: 'NOK 1 000 000–3 000 000',
        links: [{ url: '/', text: 'Les mer' }]
      },
      {
        label: 'Varighet',
        text: '12–36 måneder'
      }
    ]
  },
  metadataRight: {
    items: [
      {
        label: 'Fagområder som kan søke',
        links: [
          {
            url: '/',
            text: 'IKT'
          },
          {
            url: '/',
            text: 'Natur og miljø'
          },
          {
            url: '/',
            text: 'Matematikk og naturvitenskap'
          }
        ]
      },
      {
        label: 'Neste utlysning',
        text: '08. oktober 2019'
      }
    ]
  },
  descriptionTitle: 'Formål',
  descriptionText: utils.RichText(
    'Søknadstypen unge forskertalenter er beregnet på forskere som er på et tidlig stadium i karrieren og som har vist evne til å utføre forskning av høy vitenskapelig kvalitet. Ved vurderingen av søknadene vil det bli lagt vekt på prosjektleders evne til å arbeide selvstendig, faglige modenhet og forskerpotensial, dokumentert gjennom for eksempel publikasjoner og mobilitet. Som prosjektleder for et unge forskertalenter-prosjekt skal du få erfaring med å lede et forskningsprosjekt og å veilede doktorgrads- og/eller postdoktorstipendiater.'
  ),
  applyButton: {
    text: 'Opprett søknad',
    url: '/'
  },
  timeline: {
    startIndex: 2,
    title: 'Søknadsprosessen',
    labels: {
      collapse: 'Vis mindre',
      expand: 'Vis mer',
      next: 'Neste',
      previous: 'Forrige'
    },
    items: [
      {
        title: '02 aug 2018',
        subTitle: 'Åpent for søknad',
        text: utils.RichText(loremText),
        isActive: true
      },
      {
        title: '24 nov 2018',
        subTitle: 'Søknadsfrist',
        text: utils.RichText(loremText),
        isActive: true
      },
      {
        title: '05 mar 2019',
        subTitle: 'Utsendelse av svar',
        text: utils.RichText(loremText),
        isActive: true
      },
      {
        title: '20 okt 2019',
        subTitle: 'Prosjektstart',
        text: utils.RichText(loremText)
      },
      {
        title: '10 jun 2020',
        subTitle: 'Leveringsfrist',
        text: utils.RichText(loremText)
      },
      {
        title: '20 jun 2020',
        subTitle: 'Lorem ipsum',
        text: utils.RichText(loremText)
      }
    ]
  },
  tabs: {
    activeTab: '9df8vnasdofinuvamsdficmo',
    items: [
      {
        guid: '91348ntv7o9fnfaofidsunv',
        name: 'Utlysning',
        content: {
          menu: {
            title: 'Snarveier',
            accordion: {
              collapseLabel: 'Skjul',
              expandLabel: 'Vis',
              guid: 'as0f9uvmnqw3p095vumqwerum'
            },
            navGroups: [
              {
                title: 'Om utlysningen',
                links: {
                  items: [
                    {
                      link: {
                        text: 'Dette kan du søke om',
                        url: '#dette-kan-du-soke-om'
                      }
                    },
                    {
                      link: { text: 'Disse kan søke', url: '#disse-kan-soke' },
                      subLinks: {
                        accordion: {
                          collapseLabel: 'Skjul',
                          expandLabel: 'Vis',
                          guid: 'sdoiegwoiurgeiuhergiu'
                        },
                        links: {
                          items: [
                            {
                              text: 'Hvem kan søke?',
                              url: '#hvem-kan-søke'
                            },
                            {
                              text: 'Hvem kan delta i prosjektet?',
                              url: '#hvem-kan-delta'
                            },
                            {
                              text: 'Hva kan du søke om støtte til?',
                              url: '#hva-kan-du-søke-om'
                            },
                            {
                              text: 'Arkivering av forsikringsdata',
                              url: '#arkivering-av-forsikringsdata'
                            }
                          ]
                        }
                      }
                    }
                  ]
                }
              },
              {
                title: 'Aktuelle fagområder',
                links: {
                  items: [
                    {
                      link: {
                        text: 'Muliggjørende teknologi',
                        url: '#muliggjorende-teknologi'
                      },
                      subLinks: {
                        accordion: {
                          collapseLabel: 'Skjul',
                          expandLabel: 'Vis',
                          guid: 'skjabbelelejurbububub'
                        },
                        links: {
                          items: [
                            {
                              text: 'Nanoteteknologi',
                              url: '#nanoteknologi'
                            },
                            {
                              text: 'Brukerstyrt innovasjon',
                              url: '#brukerstyrt-innovasjon'
                            }
                          ]
                        }
                      }
                    },
                    {
                      link: {
                        text: 'Hav, mat og bioøkonomi',
                        url: '#hav-mat-og-biookonomi'
                      }
                    }
                  ]
                }
              },
              {
                title: 'En overskrift til',
                links: {
                  items: [
                    {
                      link: {
                        text: 'Muliggjørende teknologi',
                        url: '#muliggjorende-teknologi'
                      },
                      subLinks: {
                        accordion: {
                          collapseLabel: 'Skjul',
                          expandLabel: 'Vis',
                          guid: 'skjabbeleglejurbububub'
                        },
                        links: {
                          items: [
                            {
                              text: 'Nanoteteknologi',
                              url: '#nanoteknologi'
                            },
                            {
                              text: 'Brukerstyrt innovasjon',
                              url: '#brukerstyrt-innovasjon'
                            }
                          ]
                        }
                      }
                    },
                    {
                      link: {
                        text: 'Hav, mat og bioøkonomi',
                        url: '#hav-mat-og-biookonomi'
                      }
                    }
                  ]
                }
              },
              {
                title: 'En overskrift',
                links: {
                  items: [
                    {
                      link: {
                        text: 'Muliggjørende teknologi',
                        url: '#muliggjorende-teknologi'
                      },
                      subLinks: {
                        accordion: {
                          collapseLabel: 'Skjul',
                          expandLabel: 'Vis',
                          guid: 'skgjabbelelejurbububub'
                        },
                        links: {
                          items: [
                            {
                              text: 'Nanoteteknologi',
                              url: '#nanoteknologi'
                            },
                            {
                              text: 'Brukerstyrt innovasjon',
                              url: '#brukerstyrt-innovasjon'
                            }
                          ]
                        }
                      }
                    },
                    {
                      link: {
                        text: 'Lenke 1',
                        url: '#lenke-1'
                      }
                    },
                    {
                      link: {
                        text: 'Lenke 2',
                        url: '#lenke-2'
                      }
                    },
                    {
                      link: {
                        text: 'Lenke 3',
                        url: '#lenke-3'
                      }
                    },
                    {
                      link: {
                        text: 'Lenke 4',
                        url: '#lenke-4'
                      }
                    }
                  ]
                }
              }
            ]
          },
          contentSections: [
            {
              title: 'Om utlysningen',
              ingress: utils.RichText(
                '<p>Utlysningen er tilgjengelig på norsk og engelsk. Den norske utlysningsteksten er juridisk bindende. Søknader som ikke er mottatt gjennom det elektroniske søknadssystemet innen søknadsfristen vil bli avvist.</p><p>Forskningsrådet kan avvise søknader som ikke oppfyller <a href="#">generelle krav til søknaden</a>, krav som gjelder, og krav/føringer gitt i utlysningen.</p>'
              ),
              process: {
                introText: 'Søknadsprosessen',
                isCarousel: true,
                items: [
                  {
                    icon: {
                      alt: 'globe',
                      src: '/static-site/assets/icon-money.png'
                    },
                    text:
                      'Dere har en idé eller et behov som en forsker kan hjelpe til med'
                  },
                  {
                    icon: {
                      alt: 'house',
                      src: '/static-site/assets/icon-money.png'
                    },
                    text: 'Sjekk forskerne i forskerpoolen'
                  },
                  {
                    icon: {
                      alt: 'info',
                      src: '/static-site/assets/icon-money.png'
                    },
                    text: 'Søk om å få dekket forskere fra poolen'
                  },
                  {
                    text: 'Forskningsrådet vurderer søknaden det går raskt'
                  },
                  {
                    icon: {
                      alt: 'mail',
                      src: '/static-site/assets/icon-money.png'
                    },
                    text: `Hvis ok, akseptér kontrakt på "Mitt nettsted" og ta i bruke forsker(e)`
                  },
                  {
                    text:
                      'Når dere har brukt timene får dere faktura med moms fra forskningsinstitusjonen'
                  },
                  {
                    text:
                      'Dere sender e-faktura til Forskningsrådet på nettobeløpet Det vil si uten MVA*'
                  },
                  {
                    text: 'Forskningsrådet foretar en utbetaling til dere'
                  }
                ]
              },
              texts: [
                {
                  htmlId: 'disse-kan-soke',
                  title: 'Disse kan søke',
                  text: utils.RichText(
                    '<p>Bedrifter som er registrert med organiasjonsnummer i Norge.</p><p>Nasjonale og internasjonale forskningsmiljøer kan delta i prosjektet som samarbeidspartner med en utførende rolle, men de kan ikke bidra med egeninnsats.'
                  )
                },
                {
                  htmlId: 'dette-kan-du-soke-om',
                  title: 'Dette kan du søke om',
                  text: utils.RichText(
                    '<p>Relevante prosjektkostnader slik som personalkostnader, kjøp av eksterne FoU-tjenester, ett eller flere stipend og direkte prosjektutgifter.</p>'
                  )
                },
                {
                  htmlId: 'hvem-kan-søke',
                  title: 'Hvem kan søke?',
                  text: utils.RichText(
                    '<p>Nasjonale og internasjonale forskningsmiljøer kan delta i prosjektet som samarbeidspartner med en utførende rolle, men de kan ikke bidra med egeninnsats.</p>'
                  )
                },
                {
                  htmlId: 'hvem-kan-delta',
                  title: 'Hvem kan delta i prosjektet',
                  text: utils.RichText(
                    '<p>I dette prosjektet kan alle delta, så lenge de er norske, over 40, men under 35.</p>'
                  )
                },
                {
                  htmlId: 'hva-kan-du-søke-om',
                  title: 'Hva kan du søke om støtte til',
                  text: utils.RichText(
                    '<p>Du kan søke om støtte til mennesker med penger.</p>'
                  )
                },
                {
                  htmlId: 'arkivering-av-forsikringsdata',
                  title: 'Arkivering av forsikringsdata',
                  text: utils.RichText(
                    '<p>Arkivering av forsikringsdata skjer.</p>'
                  )
                }
              ],
              link: { text: 'Opprett søknad', url: '/' }
            },
            {
              title: 'Aktuelle fagområder',
              ingress: utils.RichText(
                'Søknadsfrist er 10. oktober 2018. Utlysningen dekker bredden av norsk nærlingsliv, det er øremerkede midler på flere av delområdene.'
              ),
              content: utils.ContentArea(
                utils.AccordionWithContentAreaOld({
                  title: 'Muliggjørende teknologi',
                  description:
                    'Nanoteknologi og avanserte materialer, Bioteknologi, IKT og digitalisering',
                  htmlId: 'muliggjorende-teknologi',
                  accordion: {
                    collapseLabel: 'Skjul',
                    expandLabel: 'Vis',
                    guid: 'as+df098nas+df08cunas0+df9n'
                  },
                  richText: utils.RichText('Beskrivelse av temaer her.'),
                  text:
                    'Nanoteknologi og avanserte materialer, Bioteknologi, IKT og digitalisering',
                  content: utils.ContentArea(
                    utils.TextWithSidebar({
                      htmlId: 'nanoteknologi',
                      title: 'NANO2021 - Nanoteknologi og avanserte materialer',
                      text: utils.RichText(
                        '<p>NANO2021 finansierer strategisk forskning.</p><p>Innenfor dette området kan bedrifter søke støtte til prosjekter basert på:<ul><li>nanoteknologi</li></ul></p>'
                      ),
                      sidebar: utils.ContentArea(
                        utils.InfoBlock({
                          title: 'Dokumenter',
                          text: utils.RichText(
                            '<p>FORNY2020 samarbeider med 10 teknologioverføringskontorer. Her finner du <a href="/test">en ovesikt over TTOene</a>.</p>'
                          ),
                          editorTheme: '-theme-blue'
                        })
                      )
                    }),

                    utils.TextWithSidebar({
                      htmlId: 'brukerstyrt-innovasjon',
                      title: 'Brukerstyrt innovasjon (BIA)',
                      text: utils.RichText(
                        '<p>BIA skal bidra til størst mulig verdiskaping i norsk næringsliv gjennom forskningsbasert innovasjon i bedrifter og samarbeidende FoU-miljøer innenfor BIA-programmets ansvarsområde.</p>'
                      )
                    })
                  )
                }),
                utils.AccordionWithContentAreaOld({
                  title: 'Hav, mat og bioøkonomi',
                  description:
                    'Jordbruk, Matindustri, Skogbruk og andre biobaserte næringer, Havbruk og annen marin virksomhet, Maritim virksomhet, Offshoreoperasjoner, Havteknologi',
                  htmlId: 'hav-mat-og-biookonomi',
                  accordion: {
                    collapseLabel: 'Skjul',
                    expandLabel: 'Vis',
                    guid: '90nfzdnyfsnp9q34nv9q8nfroi'
                  },
                  richText: utils.RichText('Beskrivelse av temaer her.'),
                  text:
                    'Nanoteknologi og avanserte materialer, Bioteknologi, IKT og digitalisering',
                  content: utils.ContentArea(
                    utils.TextWithSidebar({
                      title: 'NANO2021 - Nanoteknologi og avanserte materialer',
                      text: utils.RichText(
                        '<p>NANO2021 finansierer strategisk forskning.</p><p>Innenfor dette området kan bedrifter søke støtte til prosjekter basert på:<ul><li>nanoteknologi</li></ul></p>'
                      ),
                      sidebar: utils.ContentArea(
                        utils.InfoBlock({
                          title: 'Dokumenter',
                          text: utils.RichText(
                            '<p>FORNY2020 samarbeider med 10 teknologioverføringskontorer. Her finner du <a href="/test">en ovesikt over TTOene</a>.</p>'
                          ),
                          editorTheme: '-theme-blue'
                        })
                      )
                    })
                  )
                }),
                utils.AccordionWithContentAreaOld({
                  title: 'Accordion uten innhold',
                  description: 'Innhold kommer',
                  htmlId: 'accordion-uten-innhold',
                  accordion: {
                    collapseLabel: 'Skjul',
                    expandLabel: 'Vis',
                    guid: '90nfzdnyfsnp9q34nv9q8nfroi'
                  }
                })
              )
            },
            {
              title: 'Siste title',
              ingress: utils.RichText('This is last ingress'),
              moreTexts: [
                {
                  htmlId: 'krav-til-utforming-av-soknaden',
                  title: 'Krav til utforming av søknaden',
                  text: utils.RichText(
                    `<ul>
                  <li>Krav til språk søknaden skal skrives på fremgår av utlysningen.</li>
                  <li>Prosjetbeskrivelsen må følge oppgitt mal. Samtlige punkter i malen skal besvares.</li>
                  <li>Partneropplysninger for søkerbedrift og hver av de sentrale bedriftene må gis i henhold til oppgitt mål</li>
                </ul>
                <h3>Vedlegg til søknadsskjemaet</h3>
                <p>Obligatoriske vedlegg</p>
                <ul>
                  <li>Prosjektbeskrivelse (må følge oppgitt mal)</li>
                  <li>Partneropplysninger for søkerbedrift og hver av de sentrale bedriftene (må følge oppgitt mal)</li>
                </ul>`
                  )
                },
                {
                  htmlId: 'vurderingskriterier',
                  title: 'Vurderingskriterier',
                  text: utils.RichText(
                    `<h3>Vurderingskriterier for alle søknader</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                <h4>En overskrift</h4><p>Forskningsrådet forutsetter at prosjektene har en høy forskningsetisk standard, og at hensynet til konsekvenser for naturmiljøet er ivaretatt.</p>
                <h4>En overskrift til</h4><p>Forskningsrådet vil ta hensyn til dette ved prioritering av søknadene.</p>
                <h5>Enda en overskrift</h5><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                <h3>Vurderingskriterier for denne søknaden</h3>
                <ul>
                  <li>Innovasjonsgrad</li>
                  <li>Verdiskapningspotensial for bedriftspartnere</li>
                  <li>Realisering av innovasjonen</li>
                  <li>Forskningsgrad</li>
                  <li>Prosjektkvalitet for FoU-prosjektet</li>
                </ul>`
                  )
                },
                {
                  headingLevelOffset: 1,
                  title: 'Innovasjonsgrad',
                  text: utils.RichText(
                    'Innovasjonsgrad et et utrykk for hvor betydelig innovasjonen er i forhold til "state of the art" på et område. Innovasjonsområder: Nye eller endrede produkter/tjenester, Nye eller endrede metoder for produksjon/leveranse/distribusjon, Nye eller endrede former for ledelse/organisering/arbeidsforhold/kompetanse'
                  )
                }
              ],

              downloads: {
                title: 'Søknadsmaler til nedlastning',
                htmlId: 'download-templates',
                links: {
                  items: [
                    { url: '?1', text: 'Søknad for lorem ipsum 2019.doc' },
                    { url: '?2', text: 'Vedlegg til søknad lorem 2019.xlsx' }
                  ]
                }
              },
              moreContent: utils.ContentArea(
                utils.RichTextBlock({
                  title: 'Innholdsområde i bunn',
                  text: utils.RichText(
                    '<p>NTNU Technology Transferer kommersialiseringsaktør for NTNU og Helse Midt-Norge, med tilhold i Trondheim, Ålesund og på Gjøvik. NTNU TTO har bidratt til rundt 150 nye bedrifter, 135 lisensavtaler med eksisterende industri og flere hundre arbeidsplasser som har tatt en rekke nye produkter og tjenester ut i markedet.'
                  )
                }),
                utils.Message({
                  theme: '-theme-blue',
                  text: {
                    text:
                      '<b>Informativt varsel.</b> Denne utlysningen vil oppdateres fortløpende fram til seks uker før søknadsfrist. Når utlysningen er aktivert vil du ha mulighet til å opprette og sende inn søknad.'
                  }
                }),
                utils.Message({
                  text: {
                    text:
                      '<b>Viktig varsel. </b>Denne utlysningen vil oppdateres fortløpende fram til seks uker før søknadsfrist. Når utlysningen er aktivert vil du ha mulighet til å opprette og sende inn søknad.'
                  }
                }),
                utils.Message({
                  theme: '-theme-red',
                  text: {
                    text:
                      '<b>Kritisk varsel.</b> Denne utlysningen vil oppdateres fortløpende fram til seks uker før søknadsfrist. Når utlysningen er aktivert vil du ha mulighet til å opprette og sende inn søknad.'
                  }
                }),
                utils.RichTextBlock({
                  text: utils.RichText(
                    '<p>NTNU Technology Transferer kommersialiseringsaktør for NTNU og Helse Midt-Norge, med tilhold i Trondheim, Ålesund og på Gjøvik. NTNU TTO har bidratt til rundt 150 nye bedrifter, 135 lisensavtaler med eksisterende industri og flere hundre arbeidsplasser som har tatt en rekke nye produkter og tjenester ut i markedet.'
                  )
                })
              )
            }
          ]
        }
      },
      {
        guid: '9df8vnasdofinuvamsdficmo',
        name: 'Under behandling',
        content: {
          contentSections: [
            {
              title: 'Under behandling',
              ingress: utils.RichText(
                '<p>Dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.</p>'
              )
            }
          ]
        }
      },
      {
        guid: 'a9sdf8vnusdofpvm',
        name: 'Søknadsresultater',
        content: {
          contentSections: [
            {
              title: 'Om resultatet',
              ingress: utils.RichText(
                '<p>Dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.</p>'
              ),
              content: utils.ContentArea(
                utils.DescriptionListAndTables({
                  descriptionList: {
                    title: 'Lorem ipsum',
                    items: [
                      { label: 'Søkt beløp totalt', text: '28 millioner NOK' },
                      { label: 'Tildelte midler', text: '10 millioner NOK' },
                      { label: 'Mottatte søknader totalt', text: '14' },
                      { label: 'Innvilgede søknader', text: '5' }
                    ]
                  },
                  tables: [
                    {
                      title: 'Karakterfordeling av søknadene',
                      table: {
                        header: ['Karakter', '7', '6', '5', '4', '3', '2', '1'],
                        rows: [['Prosjekt', '0', '4', '4', '4', '2', '0', '0']]
                      }
                    },
                    {
                      title: 'Innvilgede søknader',
                      table: {
                        header: [
                          'Nr',
                          'Prosjekttittel',
                          'Institusjon/bedrift',
                          'Sum (NOK)'
                        ],
                        rows: [
                          [
                            '288249',
                            'Green Growth in the Russian Arctic: Examples from the Murmansk region',
                            'Fritjof Nansen stiftelsen på Polhøgda',
                            '10 000 000'
                          ],
                          [
                            '288250',
                            'SMARTNORTH: Sustainable development and MAnagement by paRTicipatory governance practices in the High NORTH',
                            'Nord universitet',
                            '10 000 000'
                          ],
                          [
                            '288500',
                            'Spatial shifts of marine stocks and the resilience of polar resource management - Russian perspectives on stock shifts and zonal attachment',
                            'Fritjof Nansen stiftelsen på Polhøgda',
                            '10 000 000'
                          ]
                        ]
                      }
                    }
                  ]
                })
              )
            }
          ]
        }
      }
    ]
  }
};
