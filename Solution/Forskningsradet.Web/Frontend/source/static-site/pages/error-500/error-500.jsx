/*
name: Feilside - 500
*/
import React from 'react';
import Layout from '../../layout';

import content from './error-500.js';

import ErrorPage from 'components/error-page';

const Error500 = () => (
  <Layout showHeader={false} showBreadcrumbs={false}>
    <ErrorPage {...content} />
  </Layout>
);

export default Error500;
