import utils from '../../content-area-utils';

export default {
  errorCode: '500',
  logo: {
    alt: 'Forskningsrådet logo',
    src: '/static-site/assets/logo-nn-no.png'
  },
  linkUrl: '/frontpage',
  text: utils.RichText(
    "<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut <a href='#'>labore</a> et dolore.</p>"
  )
};
