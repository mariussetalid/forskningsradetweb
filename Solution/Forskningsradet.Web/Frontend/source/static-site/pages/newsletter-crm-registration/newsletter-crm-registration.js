import utils from '../../content-area-utils';

export default {
  registrationPage: {
    title: 'Nyhetsbrev',
    richText: utils.RichText(
      '<p>Discere iracundia contentiones cu mei. Ut tacimates democritum usu, ea vim spesifisert i utlysning. Omnium utamur mediocritatem duo eu, ei simul perfecto has. Tempor tamquam scaevola sit et, pri veri eruditi in.</p><p>Husk at * betyr at utfylling er påkrevd.</p>'
    ),
    message: {
      text: {
        text: 'Du må velge minst en målgruppe'
      }
    },
    form: { endpoint: '/newsletter' },
    checkboxGroups: [
      {
        title: 'Målgruppe',
        options: [
          {
            label: 'Alle nyheter',
            name: 'alle-nyheter'
          },
          {
            label: 'Alle arrangementer',
            name: 'alle-arrangementer'
          },
          {
            label: 'Alle utlysninger',
            name: 'alle-utlysninger'
          },
          {
            label: 'Søknadsresultater',
            name: 'soknadsresultater'
          },
          {
            label: 'Ledige stillinger i Forskningsrådet',
            name: 'ledige-arrangementer'
          }
        ]
      },
      {
        title: 'Tema',
        options: [
          {
            label: 'Arbeidsliv, velferd og integrering',
            name: 'arbeidsliv'
          },
          {
            label: 'Bioteknologi',
            name: 'bioteknologi'
          },
          {
            label: 'Globalisering og utvikling ',
            name: 'globalisering'
          },
          {
            label: 'Humaniora',
            name: 'humaniora'
          },
          {
            label: 'IKT',
            name: 'ikt'
          },
          {
            label: 'Klima og miljø',
            name: 'klima'
          },
          {
            label: 'Landbruk, fiskeri og havbruk',
            name: 'landbruk'
          },
          {
            label: 'Matematikk og naturvitenskap ',
            name: 'matematikk'
          },
          {
            label: 'Medisin og helse',
            name: 'medisin'
          },
          {
            label: 'Miljøteknologi',
            name: 'miljo'
          },
          {
            label: 'Nano- og materialteknologi',
            name: 'nano'
          },
          {
            label: 'Petroleum og energi',
            name: 'petroleum'
          },
          {
            label: 'Polar- og nordområdeforskning',
            name: 'polar'
          },
          {
            label: 'Samferdsel',
            name: 'samferdsel'
          },
          {
            label: 'Samfunnvitenskap',
            name: 'samfunnvitenskap'
          },
          {
            label: 'Tjenesteytende næringer',
            name: 'tjenesteytende'
          },
          {
            label: 'Utdanning',
            name: 'utdanning'
          },
          {
            label: 'Øvrige teknologier',
            name: 'ovrige'
          }
        ]
      }
    ],
    inputTitle: 'Fyll inn E-postadresse og registrer deg',
    inputFields: [
      {
        label: 'E-post:',
        name: 'e-post',
        validationError: 'Du må fylle ut e-post adressen'
      }
    ],
    submitButton: {
      text: 'Registrer deg'
    },
    footer: {
      share: {
        openButtonText: 'Del',
        shareContent: {
          items: [
            { url: '#', text: 'Mail', icon: 'mail' },
            { url: '#', text: 'Twitter', icon: 'twitter' },
            { url: '#', text: 'Facebook', icon: 'facebook' },
            { url: '#', text: 'LinkedIn', icon: 'linkedin' }
          ]
        }
      }
    }
  }
};
