/*
name: påmelding CRM
group: Nyhetsbrev
path: newsletter-crm
*/
import React from 'react';
import Layout from '../../layout';

import NewsletterCrmRegistrationPage from 'components/newsletter-crm-registration-page';

import content from './newsletter-crm-registration.js';

const NewsletterCrmRegistration = () => (
  <Layout>
    <NewsletterCrmRegistrationPage {...content} />
  </Layout>
);

export default NewsletterCrmRegistration;
