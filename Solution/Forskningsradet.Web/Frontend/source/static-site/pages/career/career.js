import utils from '../../content-area-utils';

export default {
  pageHeader: {
    image: {
      src: '/static-site/assets/students.png',
      alt: 'Header'
    },
    title: 'Jobb hos oss'
  },
  content: utils.ContentArea(
    utils.InfoBlock(
      {
        title: 'Å jobbe med oss',
        text: utils.RichText(
          '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus scelerisque sem vitae faucibus viverra. Phasellus non augue lacus.</p>'
        )
      },
      '-size-third'
    ),
    utils.InfoBlock(
      {
        title: 'Hva vi ser etter',
        text: utils.RichText(
          '<p>Vi vil gjerne ha nye kolleger med stort engasjement og solid kompetanse innenfor fagfeltet sitt. Initiativ, selvstendighet og gode samarbeids-evner er viktige egenskaper vi ser etter.</p>'
        )
      },
      '-size-third'
    ),
    utils.InfoBlock(
      {
        title: 'Slik søker du',
        text: utils.RichText(
          '<p>Ut tacimates democritum usu, ea vim adipiscing delicatissimi. Omnium utamur mediocritatem duo eu, ei simul perfecto has. Discere iracundia contentiones cu mei. Veamquam in scaevola sit et, pri veri eruditi est.</p>'
        )
      },
      '-size-third'
    ),
    utils.Vacancies({
      title: 'Ledige stillinger i Forskningsrådet',
      text:
        'Ønsker du å bli informert om ledige stillinger i Forskningsrådet? Abonnér på Jobbagent.',
      link: {
        url: '/',
        text: 'Abonnér på Jobbagent'
      },
      emptyList: {
        text: 'Det er dessverre ingen ledige stillinger akkurat nå.'
      },
      items: [
        {
          id: '1',
          title:
            'Rådgiver/serniorrådgiver for bærekraftig produksjon og verdiskapning',
          url: '/vacancy',
          dateContainer: {
            datesTitle: 'Søknadsfrist',
            isPastDate: false,
            dates: [{ day: '24', month: 'apr' }]
          },
          metadata: {
            items: [
              { label: 'Arbeidssted', text: 'Trondheim' },
              { label: 'Søknadsfrist', text: '20. august 2018' }
            ]
          },
          text:
            'Discere iracundia contentiones cu mei. Ut tacimates democritum usu, ea vim adipiscing delicatissimi. Omnium utamur mediocritatem duo eu, ei simul perfecto has. Tempor tamquam scaevola sit et, pri veri eruditi in.'
        },
        {
          id: '2',
          title: 'Annen type ledig stilling',
          dateContainer: {
            isPastDate: false,
            datesTitle: 'Søknadsfrist',

            dates: [{ day: '03', month: 'sep' }]
          },
          url: '/vacancy',
          metadata: {
            items: [
              { label: 'Arbeidssted', text: 'Oslo' },
              { label: 'Søknadsfrist', text: '1. september 2018' }
            ]
          },
          text:
            'Delicatissimi omnium utamur mediocritatem duo eu. Ei simul perfecto Has tempor tamquam scaevola.'
        }
      ]
    })
  )
};
