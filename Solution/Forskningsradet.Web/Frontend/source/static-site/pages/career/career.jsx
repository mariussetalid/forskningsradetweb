/*
name: Karriereside
group: Karriere
*/
import React from 'react';
import content from './career.js';

import ContentAreaPage from 'components/content-area-page';
import Layout from '../../layout';

const Career = () => (
  <Layout>
    <ContentAreaPage {...content} />
  </Layout>
);

export default Career;
