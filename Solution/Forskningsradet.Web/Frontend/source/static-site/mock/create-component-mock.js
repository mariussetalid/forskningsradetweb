const fs = require('fs');
const path = require('path');
const { KebabToCamel } = require('@creuna/utils');

const componentName = process.argv[2];
const shouldReplaceExisiting = process.argv[3];

const writeFile = () => {
  const componentPath = path.resolve(
    __dirname,
    `./components/${componentName}.js`
  );
  if (fs.existsSync(componentPath) && !shouldReplaceExisiting)
    throw new Error(`${componentName} allready exists`);

  const componentCamelCased = KebabToCamel(componentName);
  const content = `import dataInterface from 'mock/data-interface';

  const ${componentCamelCased} = (definedValues = {}, seed = 1) => {
    const { image } = dataInterface(seed);

    const defaultValues = {
    };

    return {
      ...defaultValues,
      ...definedValues
    };
  };

  export default ${componentCamelCased};
  `;

  fs.writeFileSync(componentPath, content);
};

try {
  writeFile();
} catch (err) {
  console.error(err);
}

//fs.writeFileSync()
