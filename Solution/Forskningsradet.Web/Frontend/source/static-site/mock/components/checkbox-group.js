import dataInterface from 'mock/data-interface';

const checkboxGroup = (definedValues = {}, optionArgs = {}, seed = 1) => {
  const { titleContainer, optionsFunction } = dataInterface(seed);
  const { length = 10, shouldShuffle, additionalProps } = optionArgs;

  const defaultValues = {
    title: titleContainer.theme,
    options: optionsFunction(length, shouldShuffle, additionalProps),
    validationError: undefined
  };

  return {
    ...defaultValues,
    ...definedValues
  };
};

export default checkboxGroup;
