import dataInterface from 'mock/data-interface';

import utils from '../../content-area-utils';

const InfoBlock = (definedValues = {}, seed = 1) => {
  const { titleContainer, image, link, richText } = dataInterface(seed);

  const defaultValues = {
    text: utils.RichText(richText),
    title: titleContainer.category,
    cta: link,
    theme: undefined,
    icon: image,
    url: '#'
  };
  return {
    ...defaultValues,
    ...definedValues
  };
};

export default InfoBlock;
