import articleBlock from 'mock/components/article-block';

const relatedArticles = (definedValues = {}, seed = 1) => {
  const defaultValues = {
    articles: [
      articleBlock(
        {
          tags: undefined,
          byline: undefined,
          linkTags: undefined,
          metadata: undefined
        },
        seed
      ),
      articleBlock(
        {
          image: undefined,
          tags: undefined,
          byline: undefined,
          linkTags: undefined
        },
        seed + 1
      ),
      articleBlock(
        {
          tags: undefined,
          byline: undefined,
          linkTags: undefined,
          metadata: undefined
        },
        seed + 2
      ),
      articleBlock(
        {
          image: undefined,
          tags: undefined,
          byline: undefined,
          linkTags: undefined,
          metadata: undefined
        },
        seed + 3
      ),
      articleBlock(
        {
          tags: undefined,
          byline: undefined,
          linkTags: undefined,
          metadata: undefined
        },
        seed + 4
      ),
      articleBlock(
        {
          tags: undefined,
          byline: undefined,
          linkTags: undefined,
          metadata: undefined
        },
        seed + 5
      )
    ],
    relatedContent: {
      title: 'Aktuelt',
      link: { text: 'Alle aktuelle saker', url: '#' },
      border: ''
    },
    isGrid: true,
    usedInSidebar: undefined,
    isPublication: undefined
  };

  return {
    ...defaultValues,
    ...definedValues
  };
};

export default relatedArticles;
