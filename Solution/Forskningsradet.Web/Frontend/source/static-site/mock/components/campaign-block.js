import dataInterface from 'mock/data-interface';

const campaignBlock = (definedValues = {}, seed = 1) => {
  const { titleContainer, link, image } = dataInterface(seed);

  const defaultValues = {
    title: titleContainer.article,
    cta: link,
    image,
    editorTheme: '-theme-dark-blue'
  };

  return {
    ...defaultValues,
    ...definedValues
  };
};

export default campaignBlock;
