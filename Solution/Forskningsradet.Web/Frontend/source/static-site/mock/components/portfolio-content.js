import utils from '../../content-area-utils';
import linkLists from './link-lists';

const portfolioContent = (definedValues = {}, seed = 1) => {
  const defaultValues = {
    isInsidePriorityBlock: undefined,
    title: 'Renders LinkList',
    content: utils.ContentArea(utils.LinkLists(linkLists({}, seed))),
    singleColumn: undefined
  };

  return {
    ...defaultValues,
    ...definedValues
  };
};

export default portfolioContent;
