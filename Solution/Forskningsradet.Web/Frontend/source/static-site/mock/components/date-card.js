import dataInterface from 'mock/data-interface';

const dateCard = (definedValues = {}, seed = 1) => {
  const {
    bool,
    titleContainer,
    url,
    metadata,
    status,
    textContainer,
    dateCardTagFunction,
    dateContainer,
    dateCardMediaItem
  } = dataInterface(seed);

  const definedDateContainer = definedValues.dateContainer
    ? definedValues.dateContainer
    : {};

  const defaultValues = {
    id: seed,
    title: titleContainer.proposal,
    url,
    metadata,
    status,
    text: textContainer.proposal,
    tags: { items: dateCardTagFunction(6) },
    showMedia: true,
    media: { items: [dateCardMediaItem] },
    dateContainer: {
      ...dateContainer,
      eventImage: undefined,
      isInternational: bool,
      isPlanned: !bool,
      ...definedDateContainer
    }
  };

  return {
    ...defaultValues,
    ...definedValues,
    dateContainer: defaultValues.dateContainer
  };
};

export default dateCard;
