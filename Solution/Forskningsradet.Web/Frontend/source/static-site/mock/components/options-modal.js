import dataInterface from 'mock/data-interface';

const optionsModal = (definedValues = {}, seed = 1) => {
  const { downloadList } = dataInterface(seed);

  const defaultValues = {
    theme: undefined,
    openButtonText: 'Del',
    downloadContent: downloadList,
    shareContent: {
      items: [
        { url: '#', text: 'Mail', icon: 'mail' },
        { url: '#', text: 'Twitter', icon: 'twitter' },
        { url: '#', text: 'Facebook', icon: 'facebook' },
        { url: '#', text: 'LinkedIn', icon: 'linkedin' }
      ]
    }
  };

  return {
    ...defaultValues,
    ...definedValues
  };
};

export default optionsModal;
