import dateCard from 'mock/components/date-card';

const relatedDates = (definedValues = {}, seed = 1) => {
  const defaultValues = {
    relatedContent: {
      link: { text: 'Alle arrangementer', url: '#' },
      border: ''
    },
    isGrid: true,
    dates: [
      dateCard(
        {
          dateContainer: {
            isInternational: false,
            isPlanned: false,
            eventImage: {
              background: 'background1'
            }
          },
          tags: undefined,
          status: undefined
        },
        seed
      ),
      dateCard(
        {
          dateContainer: {
            isInternational: false,
            isPlanned: false,
            eventImage: {
              background: 'background1'
            }
          },
          tags: undefined,
          status: undefined
        },
        seed + 1
      ),
      dateCard(
        {
          dateContainer: {
            isInternational: false,
            isPlanned: false,
            eventImage: {
              background: 'background1'
            }
          },
          tags: undefined,
          status: undefined
        },
        seed + 2
      ),
      dateCard(
        {
          dateContainer: {
            isInternational: false,
            isPlanned: false,
            eventImage: {
              background: 'background1'
            }
          },
          tags: undefined,
          status: undefined
        },

        seed + 3
      ),
      dateCard(
        {
          dateContainer: {
            isInternational: false,
            isPlanned: false,
            eventImage: {
              background: 'background1'
            }
          },
          tags: undefined,
          status: undefined
        },

        seed + 4
      ),
      dateCard(
        {
          dateContainer: {
            isInternational: false,
            isPlanned: false,
            eventImage: {
              background: 'background1'
            }
          },
          tags: undefined,
          status: undefined
        },

        seed + 5
      ),
      dateCard(
        {
          dateContainer: {
            isInternational: false,
            isPlanned: false,
            eventImage: {
              background: 'background1'
            }
          },
          tags: undefined,
          status: undefined
        },

        seed + 6
      ),
      dateCard(
        {
          dateContainer: {
            isInternational: false,
            isPlanned: false,
            eventImage: {
              background: 'background1'
            }
          },
          tags: undefined,
          status: undefined
        },

        seed + 7
      )
    ]
  };

  return {
    ...defaultValues,
    ...definedValues
  };
};

export default relatedDates;
