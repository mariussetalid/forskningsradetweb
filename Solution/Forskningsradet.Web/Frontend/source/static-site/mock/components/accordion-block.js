import dataInterface from 'mock/data-interface';
import utils from '../../content-area-utils';
import accordion from 'mock/components/accordion';

const accordionBlock = (definedValues = {}, seed = 1) => {
  const { titleContainer, richText } = dataInterface(seed);

  const defaultValues = {
    accordion: accordion({}, seed),
    title: titleContainer.article,
    text: utils.RichText(richText)
  };

  return {
    ...defaultValues,
    ...definedValues
  };
};

export default accordionBlock;
