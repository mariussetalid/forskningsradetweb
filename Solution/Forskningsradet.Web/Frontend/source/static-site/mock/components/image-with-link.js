import dataInterface from 'mock/data-interface';

const ImageWithLink = (definedValues = {}, seed = 1) => {
  const { titleContainer, image } = dataInterface(seed);

  const defaultValues = {
    text: titleContainer.article,
    image,
    url: '#'
  };

  return {
    ...defaultValues,
    ...definedValues
  };
};

export default ImageWithLink;
