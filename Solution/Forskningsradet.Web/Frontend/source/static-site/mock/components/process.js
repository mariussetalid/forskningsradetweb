import dataInterface from 'mock/data-interface';

const process = (definedValues = {}, seed = 1) => {
  const { image } = dataInterface(seed);

  const defaultValues = {
    title: 'Søknadsprosess',
    introText: 'Slik fungerer søknadsprosessen.',
    isCarousel: false,
    items: [
      {
        icon: {
          alt: 'globe',
          src: '/static-site/assets/icon-money.png'
        },
        title: 'Lorem ipsum',
        text: 'Kort tekst'
      },
      {
        icon: {
          alt: 'house',
          src: '/static-site/assets/icon-money.png'
        },
        text: 'Sjekk forskerne i forskerpoolen'
      },
      {
        icon: {
          alt: 'info',
          src: '/static-site/assets/icon-money.png'
        },
        title: 'Veldig lang tittel det her alts, må vel nesten breake',
        text: 'Søk om å få dekket forskere fra poolen'
      },
      {
        text: 'Forskningsrådet vurderer søknaden det går raskt'
      },
      {
        icon: {
          alt: 'mail',
          src: '/static-site/assets/icon-money.png'
        },
        url: '/',
        text: `Hvis ok, akseptér kontrakt på "Mitt nettsted" og ta i bruke forsker(e)`
      },
      {
        title: 'Tittel med url',
        url: '/',
        text:
          'Når dere har brukt timene får dere faktura med moms fra forskningsinstitusjonen'
      },
      {
        text:
          'Dere asdasdsadsadas asdadsadasd asdasdasd asdsadsa asdd asdsaa sender e-faktura til Forskningsrådet på nettobeløpet Det vil si uten MVA*'
      },
      {
        title: 'Tittel uten url',
        text: 'Forskningsrådet foretar en utbetaling til dere'
      }
    ]
  };

  return {
    ...defaultValues,
    ...definedValues
  };
};

export default process;
