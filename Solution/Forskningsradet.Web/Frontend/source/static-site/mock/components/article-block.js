import dataInterface from 'mock/data-interface';

const articleBlock = (definedValues = {}, seed = 1) => {
  const {
    byline,
    image,
    titleContainer,
    textContainer,
    published,
    linkTags,
    metadata,
    articleTags
  } = dataInterface(seed);

  const defaultValues = {
    tags: articleTags,
    usedInSidebar: false,
    title: { text: titleContainer.article, url: '#' },
    byline,
    image,
    text: textContainer.article,
    published,
    linkTags,
    metadata,
    icon: undefined,
    documentImage: undefined,
    eventImage: undefined
  };

  return {
    ...defaultValues,
    ...definedValues
  };
};

export default articleBlock;
