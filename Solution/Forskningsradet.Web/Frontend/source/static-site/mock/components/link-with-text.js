import dataInterface from 'mock/data-interface';

const linkWithText = (definedValues = {}, seed = 1) => {
  const { link, textContainer } = dataInterface(seed);

  const defaultValues = { link, text: textContainer.article };

  return {
    ...defaultValues,
    ...definedValues
  };
};

export default linkWithText;
