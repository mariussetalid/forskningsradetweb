import dataInterface from 'mock/data-interface';

const mediaBlock = (definedValues = {}, seed = 1) => {
  const { titleContainer, image, url } = dataInterface(seed);

  const defaultValues = {
    title: titleContainer.proposal,
    image,
    url,
    youTubeVideo: {
      urlOrEmbed:
        '<iframe width="560" height="315" src="https://www.youtube.com/embed/GN-peWeNBkU?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>'
    },
    embed: {
      src:
        'https://public.tableau.com/views/Kjnnsbalansebarometerapril2018/Utviklingperinstitusjon?:embed=y&:display_count=yes&:showVizHome=no',
      title: 'Tableau-graf'
    }
  };

  return {
    ...defaultValues,
    ...definedValues
  };
};

export default mediaBlock;
