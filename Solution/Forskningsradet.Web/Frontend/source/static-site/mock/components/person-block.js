import dataInterface from 'mock/data-interface';

const personBlock = (definedValues = {}, seed = 1) => {
  const { titleContainer, labelContainer, image } = dataInterface(seed);

  const defaultValues = {
    company: titleContainer.category,
    image: image,
    jobTitle: labelContainer.position,
    name: labelContainer.name,
    text:
      'Hun har omfattende erfaring med å lede ERC-workshops og med å se gjennom søknader til ERC Starting, Consolidator og Advanced Grant. Hun har trenet søkere til ERC siden den andre utlysningen i EUs forrige rammeprogram, 7RP.'
  };

  return {
    ...defaultValues,
    ...definedValues
  };
};

export default personBlock;
