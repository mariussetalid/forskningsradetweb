import dataInterface from 'mock/data-interface';

import nestedLink from './nested-link';

const linkLists = (definedValues = {}, seed = 1) => {
  const defaultValues = {
    nestedLinks: [
      nestedLink({}, seed).item,
      nestedLink({}, seed + 1).item,
      nestedLink({}, seed + 1).item
    ],
    singleColumn: undefined
  };

  return {
    ...defaultValues,
    ...definedValues
  };
};

export default linkLists;
