import dataInterface from 'mock/data-interface';

const button = (definedValues = {}, seed = 1) => {
  const { textContainer } = dataInterface(seed);

  const defaultValues = {
    text: textContainer.button,
    theme: '-theme-fill',
    icon: undefined
  };

  return {
    ...defaultValues,
    ...definedValues
  };
};

export default button;
