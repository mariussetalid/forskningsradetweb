import dataInterface from 'mock/data-interface';

const contactList = (definedValues = {}, seed = 1) => {
  const { titleContainer } = dataInterface(seed);

  const contactList = n => {
    const { labelContainer } = dataInterface(seed + n);
    return {
      name: { text: labelContainer.name, position: labelContainer.position },
      email: { text: labelContainer.email, url: '#' },
      phone: { text: labelContainer.phone, url: '#' }
    };
  };

  const defaultValues = {
    contactGroups: [
      {
        title: titleContainer.category,
        contactLists: [contactList(1), contactList(2), contactList(3)]
      }
    ]
  };

  return {
    ...defaultValues,
    ...definedValues
  };
};

export default contactList;
