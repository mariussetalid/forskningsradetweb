import dataInterface from 'mock/data-interface';

const LinkList = (definedValues = {}, seed = 1) => {
  const { linksFunction } = dataInterface(seed);

  const defaultValues = {
    items: linksFunction(5, true),
    nestedLinks: undefined,
    linkTheme: undefined,
    theme: undefined,
    withIcon: true
  };

  return {
    ...defaultValues,
    ...definedValues
  };
};

export default LinkList;
