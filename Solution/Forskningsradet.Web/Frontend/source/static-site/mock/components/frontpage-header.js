import dataInterface from 'mock/data-interface';

import { unique } from 'mock/mock-utils';

const FrontpageHeader = (definedValues = {}, seed = 1) => {
  const { image } = dataInterface(seed);

  const defaultValues = {
    image,
    title: unique(seed).titleContainer.proposal,
    text: unique(seed).titleContainer.proposal,
    links: [{ ...unique(seed).link, isPrimary: true }, unique(seed).link],
    theme: undefined
  };

  return {
    ...defaultValues,
    ...definedValues
  };
};

export default FrontpageHeader;
