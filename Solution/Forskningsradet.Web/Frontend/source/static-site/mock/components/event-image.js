import dataInterface from 'mock/data-interface';

const EventImage = (definedValues = {}, seed = 1) => {
  const { image } = dataInterface(seed);

  const defaultValues = {
    image,
    background: undefined,
    darkFilter: undefined,
    greyFilter: undefined
  };

  return {
    ...defaultValues,
    ...definedValues
  };
};

export default EventImage;
