import publication from 'mock/components/publication';

const relatedPublications = (definedValues = {}, seed = 1) => {
  const defaultValues = {
    publications: [
      publication({}, seed),
      publication({}, seed + 1),
      publication({}, seed + 2)
    ],
    relatedContent: {
      link: { text: 'Se alle', url: '#' },
      title: 'Aktuelle publikasjoner',
      border: null
    }
  };

  return {
    ...defaultValues,
    ...definedValues
  };
};

export default relatedPublications;
