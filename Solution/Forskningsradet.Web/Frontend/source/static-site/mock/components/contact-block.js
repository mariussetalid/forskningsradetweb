import utils from '../../content-area-utils';

import { unique } from 'mock/mock-utils';

const contactBlock = (definedValues = {}, seed = 1) => {
  const defaultValues = {
    title: 'Kontaktinformasjon',
    items: utils.ContentArea(
      utils.ContactInfo({
        title: unique(seed).labelContainer.name,
        details: [
          { text: 'Spesialrådgiver' },
          { text: 'Næringsliv og teknologi' },
          {
            url: 'mailto:omr@forskningsradet.no',
            text: 'omr@forskningsradet.no'
          }
        ]
      }),
      utils.ContactInfo({
        title: unique(seed).labelContainer.name,
        details: [
          { text: 'Norges Forskningsråd' },
          { text: '+47 123 45 678', url: '#' }
        ]
      })
    )
  };

  return {
    ...defaultValues,
    ...definedValues
  };
};

export default contactBlock;
