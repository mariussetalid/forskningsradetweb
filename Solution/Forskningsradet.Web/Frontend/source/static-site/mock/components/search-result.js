import dataInterface from 'mock/data-interface';

import metadata from 'mock/components/metadata';

const searchResult = (definedValues = {}, seed = 1) => {
  const {
    titleContainer,
    image,
    status,
    richText,
    dateCardTagFunction
  } = dataInterface(seed);

  const defaultValues = {
    title: titleContainer.proposal,
    url: '#',
    tags: { tags: [{ text: 'Fil' }] },
    icon: { iconTheme: 'pdf' },
    theme: undefined,
    text: richText,
    metadata: metadata({}, seed),
    image,
    descriptiveTitle: 'Porteføljeplan for Demokrati, styring of fornyelse:',
    statusList: [status, dataInterface(seed + 23).status],
    themeTags: { items: dateCardTagFunction(5) }
  };

  return {
    ...defaultValues,
    ...definedValues
  };
};

export default searchResult;
