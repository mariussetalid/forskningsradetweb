// Not a component, but data for DateCard version of event-list-page
import dateCard from 'mock/components/date-card';
import dataInterface from 'mock/data-interface';

const event = (definedValues = {}, seed = 1) => {
  const { dateContainer } = dataInterface(seed);

  const definedDateContainer = definedValues.dateContainer
    ? definedValues.dateContainer
    : {};

  const defaultValues = {
    ...dateCard(
      {
        status: undefined,
        theme: undefined, // test
        tags: undefined,
        dateContainer: {
          ...dateContainer,
          isInternational: false,
          datesDescription: undefined,
          datesTitle: undefined,
          isPlanned: undefined,
          ...definedDateContainer
        }
      },
      seed
    )
  };

  return {
    ...defaultValues,
    ...definedValues,
    dateContainer: defaultValues.dateContainer
  };
};

export default event;
