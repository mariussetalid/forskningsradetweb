import dataInterface from 'mock/data-interface';
import utils from '../../content-area-utils';
import accordion from 'mock/components/accordion';

const accordionWithContentArea = (definedValues = {}, seed = 1) => {
  const { id, iconWarning, richText } = dataInterface(seed);

  const defaultValues = {
    accordion: accordion({}, seed),
    htmlId: id,
    initiallyOpen: false,
    title: 'Onsdag 17.juni 2020',
    iconWarning,
    content: utils.ContentArea(
      utils.RichTextBlock({
        text: utils.RichText(richText)
      }),
      utils.Schedule({
        lastTitle: 'Slutt',
        programList: [
          {
            time: '08:45',
            title: 'Oppkobling',
            text: utils.RichText(
              '<p> Vi anbefaler å koble opp i god tid før konferansen begynner. </p>'
            ),
            isBreak: false
          },
          {
            time: '09:00',
            title:
              'Hvordan satse for å løse energi- og klimautfordinger midt i en pandemi?',
            text: utils.RichText(
              "<p>En flott forklaring på hva man skal gjøre her. Hvordan ser det ut når jeg skriver en litt lengre tekst i dette feltet?</p> <blockquote id='output'>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</blockquote><ul><li>tets</li></ul></div>"
            ),
            isBreak: true
          },
          {
            time: '09:30',
            title: 'Foredrag nummer 3',
            text: utils.RichText(
              "<ul> <li><h3><b>På hvilke områder kan Norge ta en lederrolle i internasjonalt samarbeid om erergi- og klimatutfordinger?</b></h3><p>John-Arne Røttingen, administrerende direktør i Forskingsrådet | <a href='#'>Opptak</a> | <a href='#'>Presentasjon</a></p><p><b>På hvilke områder kan Norge ta en lederrolle i internasjonalt samarbeid om erergi- og klimatutfordinger?</b></p></li></ul>"
            ),
            isBreak: false
          },
          {
            time: '11:00',
            text: utils.RichText(
              "<ul> <li><p><b>På hvilke områder kan Norge ta en lederrolle i internasjonalt samarbeid om erergi- og klimatutfordinger?</b></p><p>John-Arne Røttingen, administrerende direktør i Forskingsrådet | <a href='other-page'>Opptak</a> | <a href='#'>Presentasjon</a></p></li></ul>"
            )
          }
        ]
      })
    )
  };

  return {
    ...defaultValues,
    ...definedValues
  };
};

export default accordionWithContentArea;
