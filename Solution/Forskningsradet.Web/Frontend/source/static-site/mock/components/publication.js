import searchResult from 'mock/components/search-result';

const publication = (definedValues = {}, seed = 1) => {
  const defaultValues = {
    ...searchResult(
      {
        tags: undefined,
        image: undefined,
        icon: undefined,
        descriptiveTitle: undefined,
        statusList: undefined,
        themeTags: undefined
      },
      seed
    )
  };

  return {
    ...defaultValues,
    ...definedValues
  };
};

export default publication;
