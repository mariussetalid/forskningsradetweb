import dataInterface from 'mock/data-interface';

const message = (definedValues = {}, seed = 1) => {
  const { iconWarning } = dataInterface(seed);
  const defaultValues = {
    text: {
      text:
        '<b>Kritisk varsel.</b> Denne utlysningen vil oppdateres fortløpende fram til seks uker før søknadsfrist. Når utlysningen er aktivert vil du ha mulighet til å opprette og sende inn søknad.'
    },
    ...iconWarning
  };

  return {
    ...defaultValues,
    ...definedValues
  };
};

export default message;
