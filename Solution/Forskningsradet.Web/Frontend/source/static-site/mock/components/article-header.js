import dataInterface from 'mock/data-interface';

const articleHeader = (definedValues = {}, seed = 1) => {
  const { accordion, byline, share } = dataInterface(seed);

  const defaultValues = {
    accordion,
    byline,
    download: { url: '#', text: 'Last ned' },
    share
  };

  return {
    ...defaultValues,
    ...definedValues
  };
};

export default articleHeader;
