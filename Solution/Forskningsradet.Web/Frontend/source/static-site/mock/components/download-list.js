import dataInterface from 'mock/data-interface';

const DownloadList = (definedValues = {}, seed = 1) => {
  const { downloadList } = dataInterface(seed);

  const defaultValues = {
    ...downloadList
  };

  return {
    ...defaultValues,
    ...definedValues
  };
};

export default DownloadList;
