import dataInterface from 'mock/data-interface';

import linkList from './link-list';

const nestedLink = (definedValues = {}, seed = 1) => {
  const { link, linksFunction, titleContainer } = dataInterface(seed);

  const defaultValues = {
    item: {
      url: '#',
      title: titleContainer.category,
      ...linkList({}, seed)
    },
    linkTheme: undefined,
    iconData: undefined
  };

  return {
    ...defaultValues,
    ...definedValues
  };
};

export default nestedLink;
