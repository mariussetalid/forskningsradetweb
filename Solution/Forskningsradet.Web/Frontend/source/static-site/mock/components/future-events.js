import dataInterface from 'mock/data-interface';
import dateCard from './date-card';

const FutureEvents = (definedValues = {}, seed = 1) => {
  const { link } = dataInterface(seed);

  const defaultValues = {
    items: [dateCard({}, seed), dateCard({}, seed + 1), dateCard({}, seed + 2)],
    title: 'Kommende arrangementer',
    text:
      'Vi arrangerer mange konferanser, informasjonsmøter og seminarer. På våre arrangementssider kan du melde deg på, ta del i livesendinger og finne presentasjonsmateriale.',
    link
  };

  return {
    ...defaultValues,
    ...definedValues
  };
};

export default FutureEvents;
