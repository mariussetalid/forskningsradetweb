import dataInterface from 'mock/data-interface';

const accordion = (definedValues = {}, seed = 1) => {
  const { id } = dataInterface(seed);

  const defaultValues = {
    animate: true,
    guid: id,
    collapseLabel: 'Collapse',
    expandLabel: 'Expand',
    showButtonText: true
  };

  return {
    ...defaultValues,
    ...definedValues
  };
};

export default accordion;
