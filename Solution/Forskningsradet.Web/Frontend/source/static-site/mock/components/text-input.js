const TextInput = (definedValues = {}, seed = 1) => {
  const defaultValues = {
    label: 'E-post:',
    type: 'text',
    theme: undefined,
    icon: undefined,
    value: 'Eksempel@gmail.com'
  };

  return {
    ...defaultValues,
    ...definedValues
  };
};

export default TextInput;
