import dataInterface from 'mock/data-interface';

const metadata = (definedValues = {}, seed = 1) => {
  //const {} = dataInterface(seed);

  const defaultValues = {
    items: [
      { label: 'Arrangementstype', text: 'kurs' },
      { label: 'Varighet', text: '2 dager' },
      {
        icon: { iconTheme: 'pdf' },
        label: 'Sted',
        text: 'Trondheim'
      }
    ],
    theme: undefined
  };

  return {
    ...defaultValues,
    ...definedValues
  };
};

export default metadata;
