import dataInterface from 'mock/data-interface';

import utils from '../../content-area-utils';
import { unique } from '../mock-utils';

const pageHeader = (definedValues = {}, seed = 1) => {
  const { image, textContainer } = dataInterface(seed);

  const defaultValues = {
    title: 'Læreveiledning i Ny_sgjerrigpermetoden',
    image,
    ingress: textContainer.article,
    links: [unique(seed).link, unique(seed).link],
    portfolioLinks: [unique(seed).link, unique(seed).link],
    tags: {
      tags: [
        { link: { text: 'Hovedtema', url: '?1' } },
        { link: { text: 'Undertema' }, inactive: true },
        { link: { text: 'Tilknyttet tema', url: '?3' } },
        { link: { text: 'Tema under', url: '?4' } },
        { link: { text: 'Undertema', url: '?5' } },
        { link: { text: 'Tilknyttet tema', url: '?6' } },
        { link: { text: 'Tema under', url: '?7' } },
        { link: { text: 'Undertema', url: '?8' } },
        { link: { text: 'Tilknyttet tema', url: '?9' } }
      ]
    },
    map: {
      googleMapsAPIKey: 'AIzaSyBbbBTY4NSmbRtGUyitGmUj2KEiW142Mqc',
      labels: {
        zoomIn: 'Zoom inn',
        zoomOut: 'Zoom out'
      },
      markers: [
        {
          latitude: 59.9134964,
          longitude: 10.6450097,
          name: 'Norges forskningsråd',
          popup: {
            title: 'Norges forskningsråd',
            address: utils.RichText(
              '<p>Hovedkontor</p><p>Drammensveien 288, 0283 Oslo</p>'
            ),
            link: {
              text: 'Veibeskrivelse',
              url: '#'
            }
          }
        }
      ]
    }
  };

  return {
    ...defaultValues,
    ...definedValues
  };
};

export default pageHeader;
