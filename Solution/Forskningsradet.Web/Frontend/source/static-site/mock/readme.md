## This is a stystem to make it easier to create and maintain mock data.

##### It consists of three layers:

#### 1. data.js

---

- Consists of only raw data expected to be reused.
- The data is always stored in a list of examples of the data.
- The variables are named as if not stored in a list. They are named according to what data will be taken from the variable, to attempt to make them share the name of the prop they represent.

#### 2. data-interface.js

---

**A function taking a seed value as input, which is used to generate random data that doesn't change when rerunning the app.**

It exports the raw data into three easier-to-use data of three categories:

1. **randomItem()** - it returns a random item from the data-list based on the seed value.
2. **randomContainer()** - it returns an object with keys holding a random value.
   Example:

```
titleContainer = {
    proposal:[...],
    article:[...],
    theme:[...]
}
```

becomes

```
titleContainer = {
    proposal: "some title",
    article: "some title",
    theme: "some title"
}
```

3. **listFunction()** - It returns a function with inputs to manipulate the list passed as argument.
   You can then chose the length of the list, randomize the list or add properties (if they're objects).

#### 3. components/[component]

---

**A function that returns default values for a component and allows them to be overriden.
It takes an object and a seed value which is passed into `dataInterface(seed)` to retrieve randomized data.
The object contains the values that will override the default ones.**

This is what should be mainly used to generate data.
Example:

```javascript
import relatedArticles from 'mock/components/related-articles';
...
relatedArticles: relatedArticles({usedInSidebar: true, isGrid: false}, 1)
```

#### Utilities

---

Sometimes you want multiples of the same data, e.g a component with two links,
but `const {link} = dataInterface(seed)` gives you only 1 link object, so you want to make multiple calls to `dataInterface()` with different seeds to get different links.
For this problem there is a `unique(seed)` function that returns `dataInterace(newSeed)` with a unique seed value:

```javascript
links: [unqiue(seed).link, unqiue(seed).link];
```

The function is stored in `mock-utils.js` as I assume more utility functions will come.

#### Create-mock script

---

**There is a script for creating a mock-component: `mock/create-component-mock.js`**
This takes 2 arguments: component name in kebab-case and a shouldReplaceExisitingComponent bool that must be true if you want to replace a component.
Example: `yarn mock media-with-caption` `yarn mock media-with-caption true`
