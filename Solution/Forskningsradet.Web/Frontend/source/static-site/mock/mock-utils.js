import dataInterface from 'mock/data-interface';

let inc = 0;

export const unique = seed => {
  inc++;
  return dataInterface(seed + inc);
};
