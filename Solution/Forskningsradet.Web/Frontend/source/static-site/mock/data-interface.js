import {
  id,
  titleContainer,
  labelContainer,
  metadata,
  textContainer,
  tags,
  dates,
  eventImage,
  status,
  image,
  byline,
  published,
  linkTags,
  icon,
  documentIcon,
  accordion,
  downloadList,
  share,
  link,
  options,
  richText,
  iconWarning,
  dateCardTagItem,
  dateCardMediaItem,
  articleTags
} from 'mock/data';
// Exposes all props
const dataInterface = seed => {
  const randomItem = arr => arr[seed % arr.length];
  const randomContainer = obj =>
    Object.keys(obj).reduce(
      (acc, key) => ({ ...acc, [key]: randomItem(obj[key]) }),
      {}
    );

  const listFunction = arr => (
    length,
    shouldShuffle = true,
    additionalProps
  ) => {
    const shuffle = list => {
      let indeces = list.map((_, index) => index);
      return list.map(() => {
        const randomIndex = indeces[Math.floor(Math.random() * indeces.length)];
        indeces = indeces.filter(item => item !== randomIndex);
        return list[randomIndex];
      });
    };

    var list = arr.slice(0, Math.min(length, arr.length));
    if (shouldShuffle) list = shuffle(list);
    if (additionalProps)
      list = list.map(item => ({ ...item, ...additionalProps }));
    return list;
  };

  return {
    bool: seed % 2 === 0,
    titleContainer: randomContainer(titleContainer),
    labelContainer: randomContainer(labelContainer),
    url: '#',
    metadata: randomItem(metadata),
    status: randomItem(status),
    textContainer: randomContainer(textContainer),
    tags: randomItem(tags),
    dateContainer: {
      datesTitle: 'søknadsfrist',
      datesDescription: 'løpende',
      dates: randomItem(dates),
      eventImage: randomItem(eventImage)
    },
    byline: randomItem(byline),
    image: randomItem(image),
    published: randomItem(published),
    linkTags: randomItem(linkTags),
    documentIcon: randomItem(icon),
    eventImage: randomItem(eventImage),
    accordion,
    downloadList,
    share: randomItem(share),
    icon: randomItem(icon),
    link: randomItem(link),
    optionsFunction: listFunction(options),
    linksFunction: listFunction(link),
    id: randomItem(id),
    richText: randomItem(richText),
    iconWarning: randomItem(iconWarning),
    dateCardTagFunction: listFunction(dateCardTagItem),
    dateCardMediaItem: randomItem(dateCardMediaItem),
    articleTags: randomItem(articleTags)
  };
};

export default dataInterface;
