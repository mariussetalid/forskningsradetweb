export const titleContainer = {
  proposal: [
    'Forskerprosjekt for fornyelse',
    'Forskerprosjekt for unge talenter',
    'Treårig Forskerprosjekt med internasjonal mobilitet',
    'Samarbeidsprosjekt for å møte utfordringer i samfunn og næringsliv',
    'Kompetansebyggende prosjekt for næringslivet',
    'Samarbeidsprosjekt innenfor digital sikkerhet og kunstig intelligens, robotikk og autonomi med tilhørende systemer',
    'Obligatorisk skisse før søknad til forskningssenter for klinisk behandling',
    'Forprosjekt for å mobilisere bedrifter til forskning',
    'Nærings-ph.d. – doktorgradsprosjekt i bedrift',
    'Offentlig sektor-ph.d. – doktorgradsprosjekt i offentlig virksomhet',
    'Innovasjonscamp – Registrering av forprosjekt'
  ],
  article: [
    '193 millioner til innovasjon i offentlig sektor',
    '2,65 milliarder kroner til nyskapende forskning',
    '1,6 milliarder til forskning for å løse store samfunnsutfordringer',
    '120 millioner til nytt forskningssenter for miljøvennlig energi',
    'Mari Sundli Tveit er ny administrerende direktør i Forskningsrådet',
    'Rekordtro på forskning',
    'Forskningsrådets innovasjonspris går til Agnete Fredriksen i Vaccibody',
    'Brønnekspert vinner prestisjefylt teknologipris',
    'Oppstart for Grønn plattform – 1 milliard kroner nå tilgjengelig',
    'Slik kan maskinlæring gjøre norsk sokkel mer effektiv'
  ],
  theme: [
    'Målgruppe',
    'Innholdstype',
    'Tema',
    'Publikasjon',
    'Utlysning',
    'Artikkel',
    'Arrangement'
  ],
  category: [
    'Finans og bank',
    'media og kultur resiseliv',
    'varehandel og annen tjenesteyting',
    'Nanoteknologi',
    'Muligjørende teknologier',
    'Hav',
    'Næringsliv',
    'Forskningsorganisasjon',
    'Landbasert mat, miljø og bioressurser',
    'Offentlig sektor',
    'Internasjonalisering'
  ]
};

export const labelContainer = {
  position: ['Spesialrådgiver', 'Norges Forskningsråd', 'Avdelings direktør'],
  email: [
    'amh@forskingsraadet.no',
    'jonKaare@hotmail.no',
    'MarieH@forskningsraadet.no'
  ],
  phone: [
    '22 03 70 00',
    '+47 54 34 54 23',
    '+47 65 45 34 24',
    '94 23 00 32',
    '67 34 23 67',
    '+47 23 94 05 00'
  ],
  name: [
    'Elisabeth Frydenlund',
    'Arthur Almestad',
    'Bjørn Gjellan Nielsen',
    'Tom Skyrud',
    'Hilde Aarsheim',
    'Matthias Kock',
    'Marianne Haavardsholm Aandahl',
    'Aase Marie Hundere'
  ]
};

export const options = [
  {
    label: 'Arbeidsliv, velferd og integrering',
    name: 'arbeidsliv'
  },
  {
    label: 'Bioteknologi',
    name: 'bioteknologi'
  },
  {
    label: 'Globalisering og utvikling ',
    name: 'globalisering'
  },
  {
    label: 'Humaniora',
    name: 'humaniora'
  },
  {
    label: 'IKT',
    name: 'ikt'
  },
  {
    label: 'Klima og miljø',
    name: 'klima'
  },
  {
    label: 'Landbruk, fiskeri og havbruk',
    name: 'landbruk'
  },
  {
    label: 'Matematikk og naturvitenskap ',
    name: 'matematikk'
  },
  {
    label: 'Medisin og helse',
    name: 'medisin'
  },
  {
    label: 'Miljøteknologi',
    name: 'miljo'
  },
  {
    label: 'Nano- og materialteknologi',
    name: 'nano'
  },
  {
    label: 'Petroleum og energi',
    name: 'petroleum'
  },
  {
    label: 'Polar- og nordområdeforskning',
    name: 'polar'
  },
  {
    label: 'Samferdsel',
    name: 'samferdsel'
  },
  {
    label: 'Samfunnvitenskap',
    name: 'samfunnvitenskap'
  },
  {
    label: 'Tjenesteytende næringer',
    name: 'tjenesteytende'
  },
  {
    label: 'Utdanning',
    name: 'utdanning'
  },
  {
    label: 'Øvrige teknologier',
    name: 'ovrige'
  }
];

export const textContainer = {
  button: [
    'Se all',
    'Aktuelle Utlysninger',
    'Se mer',
    'Alle aktuelle saker',
    'Meld deg på'
  ],
  proposal: [
    'Formålet er fornyelse og utvikling i forskningen som kan bidra til å flytte forskningsfronten. Utlysningen retter seg mot forskere som har vist evne til å utføre forskning av høy vitenskapelig kvalitet og er åpen for alle fag- og forskningsområder.',
    'Støtten skal gi talenter under 40 år mulighet til å forfølge egne ideer og å lede et forskningsprosjekt. Utlysningen er rettet mot forskere tidlig i karrieren, 2–7 år etter disputas, som har vist potensial til å utføre forskning av høy vitenskapelig kvalitet.',
    'Formålet er å fremme internasjonal mobilitet og karriereutvikling blant forskere tidlig i karrieren, og bidra til kunnskapsoverføring til norske forskningsmiljøer. Utlysningen retter seg mot forskere på postdoktornivå, som skal tilbringe to år ved en utenlandsk forskningsorganisasjon og det tredje året ved en norsk. Utlysningen er åpen for alle fag- og forskningsområder.',
    'Formålet er å flytte forskningsfronten gjennom tverrfaglige prosjekter med større bevilgninger. Vi støtter forskere fra forskjellige fag som sammen vil frembringe ny kunnskap som ikke ville vært mulig å oppnå uten tverrfaglig samarbeid.',
    'Formålet med søknadstypen er å utvikle ny kunnskap og bygge forskningskompetanse som samfunnet eller næringslivet trenger for å møte viktige samfunnsutfordringer. Prosjektene skal stimulere og støtte samarbeid mellom forskningsmiljøer og aktører utenfor forskningssektoren som representerer samfunnets og/eller næringslivets behov for kunnskap og forskningskompetanse.',
    'Målet med utlysningen er å utvikle ny kunnskap innenfor temaene digital sikkerhet og kunstig intelligens, robotikk og autonomi med tilhørende systemer. Innsatsen skal resultere i at forskningsfronten flyttes, sørge for viktig kompetansebygging og skape nye muligheter for mennesker og samfunn på kortere og lengre sikt.',
    'Skisseutlysningen kvalifiserer til å sende inn fullstendig søknad om midler til Forskningssentre for klinisk behandling med søknadsfrist 12. mai 2021. Søkere som ikke har sendt inn en skisse til skissefristen 6. januar 2021, kan ikke søke midler gjennom hovedutlysningen.',
    'Denne utlysningen er kun for registrering av forprosjekter som er tildelt etter Landbrukets innovasjonscamp.'
  ],
  article: [
    'Gjennom Forskningsrådet deler regjeringen ut 193 millioner til prosjekter der forskere og offentlige virksomheter samarbeider om å løse offentlig sektors egne utfordringer.',
    '256 forskerprosjekter får i dag til sammen 2,65 milliarder kroner i støtte fra Forskningsrådet. Pengene går til forskere ved universiteter, høyskoler og institutter over hele landet.',
    'Regjeringen investerer gjennom Forskningsrådet nå 1,6 milliarder kroner i 113 forskningsprosjekter som skal gi oss ny kunnskap om blant annet matproduksjon, helse, miljøpåvirkning, klimateknologi, energi og samfunn.',
    'Forskningsrådet har besluttet å investere 120 millioner kroner i et nytt forskningssenter for miljøvennlig energi. Forskningen fra det nye senteret skal skape eksportmuligheter for norsk næringsliv og sikre at miljøpåvirkningene fra fremtidig vindkraftutbygging blir minst mulig.',
    'Mari Sundli Tveit (46) er ansatt som ny administrerende direktør i Norges forskningsråd. Hun kommer fra rollen som direktør for politikk i NHO og har tidligere vært rektor ved NMBU og styreleder i Universitets- og høgskolerådet.',
    'Innovasjonsprisen skal hedre forskningsbasert innovasjon, og i år også koblet til koronapandemien.',
    'Nye tillitstall viser at nordmenn har rekordtro på forskning - men vi tviler på at våre meningsmotstandere har samme tillit til forskning som oss selv hvis forskningen kommer i konflikt med deres holdninger.',
    '"OG21 Technology Champion" for 2020 går til Knut Henriksen fra oljeserviceselskapet Baker Hughes. Prisen gis til ildsjeler som har vært drivende for innføring av ny, verdiskapende teknologi på norsk sokkel.',
    'Regjeringen bevilget tidligere i år en milliard kroner til oppstart av Grønn plattform. Dette er en del av regjeringens tiltakspakke til grønn omstilling. Første utlysning er nå ute.',
    'Maskinlæring er en spennende ny teknologi som kan bidra til å senke kostnader, øke produksjonen og redusere klimagassutslipp, viser en ny rapport fra OG21.'
  ]
};

export const dateCardTagItem = [
  { text: 'Finans og bank', isExternal: false, isPrimary: false },
  { text: 'media og kultur resiseliv', isExternal: false, isPrimary: false },
  {
    text: 'varehandel og annen tjenesteyting',
    isExternal: false,
    isPrimary: false
  },
  { text: 'Nanoteknologi', isExternal: false, isPrimary: false },
  { text: 'Muligjørende teknologier', isExternal: false, isPrimary: false },
  { text: 'Hav', isExternal: false, isPrimary: false },
  { text: 'Næringsliv', isExternal: false, isPrimary: false },
  { text: 'Forskningsorganisasjon', isExternal: false, isPrimary: false },
  { text: 'Internasjonalisering', isExternal: false, isPrimary: false },
  { text: 'Offentlig', isExternal: false, isPrimary: false },
  {
    text: 'Landbasert mat, miljø og bioressurser',
    isExternal: false,
    isPrimary: false
  }
];

export const dateCardMediaItem = [
  { icon: 'slideshow', text: 'Se bilder' },
  { icon: 'video', text: 'Se opptak' },
  { icon: 'camera', text: 'Arrangementet strømmes' }
];

'Næringsliv',
  'Forskningsorganisasjon',
  'Landbasert mat, miljø og bioressurser',
  'Offentlig sektor',
  'Internasjonalisering';

export const articleTags = [
  { tags: [{ text: 'Næringsliv' }, { text: 'Forskningsorganisasjon' }] },
  {
    tags: [{ text: 'Landbasert mat, miljø og bioressurser' }]
  },
  { tags: [{ text: 'Hav' }, { text: 'Internasjonalisering' }] }
];

export const icon = [
  {
    name: 'book',
    fill: true
  },

  {
    name: 'chat',
    fill: true
  },
  {
    name: 'globe',
    fill: true
  },
  {
    name: 'info',
    fill: true
  },
  {
    name: 'link-arrow',
    fill: true
  },
  {
    name: 'link',
    fill: true
  },
  {
    name: 'link-arrow',
    fill: true
  },

  {
    name: 'plus-circled',
    fill: true
  }
];

export const iconWarning = [
  { theme: '-theme-red', isColored: true },
  { theme: '-theme-blue', isColored: false },
  { theme: '-theme-blue', isColored: true },
  { theme: '-theme-red', isColored: false },
  { theme: '-theme-red', isColored: false }
];

export const link = [
  {
    text: 'Call to action',
    url: '#'
  },
  {
    text: 'Les mer',
    url: '#'
  },
  {
    text: 'Gå til artikkel',
    url: '#'
  },
  {
    text: 'Finn utlysninger',
    url: '#'
  }
];
export const accordion = {
  guid: '981298urnpunvp9qpow8eunrvq',
  expandLabel: 'Vis',
  collapseLabel: 'Skjul'
};

export const downloadList = {
  groups: [
    {
      heading: 'Last ned utlysning',
      items: [
        {
          iconTheme: 'word',
          url: '#',
          text: 'Midler til forskning på dyrehelse.pdf'
        }
      ]
    },
    {
      heading: 'Last ned maler',
      items: [
        {
          iconTheme: 'word',
          url: '#',
          text:
            'Mal for prosjektbeskrivelse Kompetanse- og samarbeidsprosjekt/Project description template Collaborative and Knowledge-building Project (docx)'
        },
        {
          iconTheme: 'pdf',
          url: '#',
          text: 'CV-mal for forskere/CV template researchers (docx)'
        },
        {
          iconTheme: 'pdf',
          url: '#',
          text: 'CV-mal/CV template.docx'
        }
      ]
    }
  ],
  downloadAllText: 'Last ned alle',
  downloadAllUrl: '#'
};

export const share = [
  {
    openButtonText: 'Del',
    closeButtonLabel: 'Lukk',
    downloadContent: downloadList,
    shareContent: {
      items: [
        { url: '#', text: 'Mail', icon: 'mail' },
        { url: '#', text: 'Twitter', icon: 'twitter' },
        { url: '#', text: 'Facebook', icon: 'facebook' },
        { url: '#', text: 'LinkedIn', icon: 'linkedin' }
      ]
    }
  }
];

export const byline = [
  {
    items: [
      { text: 'Sist oppdatert 20.mai 2017' },
      { text: 'Publisert 13. april 2018' }
    ]
  },
  { items: [{ text: 'Publisert 14.10.2018' }] }
];

export const published = [{ type: 'Publisert', date: '21. september 2019' }];

export const image = [
  {
    src: '/static-site/assets/profile-pic-2.jpg',
    alt: 'et bilde'
  },
  {
    src: '/static-site/assets/profile-pic-1.jpg',
    alt: 'et bilde'
  },
  {
    src: '/static-site/assets/document-image.png',
    alt: 'et bilde'
  },
  {
    src: '/static-site/assets/smartphone.png',
    alt: 'et bilde'
  },
  {
    src: '/static-site/assets/vr-glasses.png',
    alt: 'et bilde'
  },
  {
    src: '/static-site/assets/tto-lederne.png',
    alt: 'et bilde'
  },
  {
    src: '/static-site/assets/students.png',
    alt: 'et bilde'
  }
];

export const linkTags = [
  {
    leftAligned: true,
    tags: [
      { link: { text: 'Hovedtema', url: '?1' } },
      { link: { text: 'Undertema' }, inactive: true },
      { link: { text: 'Tilknyttet tema', url: '?3' } },
      { link: { text: 'Tema under', url: '?4' } }
    ]
  },
  {
    leftAligned: true,
    tags: [
      { link: { text: 'Hovedtema', url: '?1' } },
      { link: { text: 'Undertema' } },
      { link: { text: 'Tilknyttet tema', url: '?3' } },
      { link: { text: 'Tema under', url: '?4' } },
      { link: { text: 'Banebrytende forskning', url: '?1' } },
      {
        link: { text: 'Global utvikling og internasjonale relasjoner' },
        inactive: true
      },
      { link: { text: 'Naturvitenskap og teknologi', url: '?3' } },
      { link: { text: 'Helse', url: '?4' } }
    ]
  },
  {
    tags: [
      { link: { text: 'Hovedtema', url: '?1' } },
      { link: { text: 'Undertema', url: '#' } },
      { link: { text: 'Tilknyttet tema', url: '?3' } }
    ]
  }
];

export const documentIcon = [
  { iconTheme: 'word' },
  { iconTheme: 'pdf' },
  { iconTheme: 'ppt' },
  { iconTheme: 'csv' },
  { iconTheme: 'excel' },
  { iconTheme: 'image' },
  { iconTheme: undefined }
];

export const metadata = [
  {
    items: [
      { label: 'Prosjektstørrelse', text: 'NOK 1 000 000 – 3 000 000' },
      { label: 'Varighet', text: '12 - 36 måneder' }
    ]
  },
  {
    items: [
      {
        label: 'Antatt tilgjengelige midler:',
        text: 'NOK 1 000 000 – 3 000 000'
      },
      { label: 'Støttegrenser', text: 'Kr 5 000-100 000' },
      { label: 'Varighet', text: '1-24 måneder' }
    ]
  },
  {
    items: [
      { label: 'Antatt tilgjengelige midler', text: 'NOK 123 000 000' },
      { label: 'Støttegrenser', text: 'Kr 10 000-70 000' }
    ]
  },
  {
    items: [{ label: 'Varighet', text: '12 måneder' }]
  }
];

export const id = [...new Array(50)].map(() => Math.random().toString());

export const status = [
  {
    theme: '-is-canceled',
    text: 'Kansellert'
  },
  {
    theme: '-is-active',
    text: 'Søk nå'
  },
  {
    theme: '-is-planned',
    text: 'Planlagt'
  },
  {
    theme: '-is-completed',
    text: 'gjennomført'
  },
  {
    theme: '-result-is-published',
    text: 'publisert'
  }
];
export const richText = [
  '<p>E-post:<a href="#">post@forskningsradet.no</a></p><p>Telefon: <a href="#">+47 22 03 70 00</a></p>',
  '<p>Hovedkontor</p><p>Drammensveien 288, 0283 Oslo</p>',
  '<p>FORNY2020 samarbeider med 10 teknologioverføringskontorer. Her finner du <a href="/test">en ovesikt over TTOene</a>.</p>',
  'Søknadstypen unge forskertalenter er beregnet på forskere som er på et tidlig stadium i karrieren og som har vist evne til å utføre forskning av høy vitenskapelig kvalitet.',
  '<p>Lorem ipsum dolor sit amet, choro civibus eu sed, graeco evertitur sed at, invidunt voluptaria mea an. Set commodo vocibus at mea, augue inpercipit ne vis at erat soluta audiam sit.</p><p>Lorem ipsum dolor sit amet, choro civibus eu sed, graeco evertitur sed at, invidunt voluptaria mea an. Set commodo vocibus at mea, augue inpercipit ne vis at erat soluta audiam sit.</p>',
  '<p>NTNU Technology Transferer kommersialiseringsaktør for NTNU og Helse Midt-Norge, med tilhold i Trondheim, Ålesund og på Gjøvik. NTNU TTO har bidratt til rundt 150 nye bedrifter, 135 lisensavtaler med eksisterende industri og flere hundre arbeidsplasser som har tatt en rekke nye produkter og tjenester ut i markedet. Her er noen:</p><ul><li><a href="www.kahoot.com">Kahoot</a> – læringsspill med 70 millioner måndelige brukere globalt</li><li><a href="www.kahoot.com">Kahoot</a> – læringsspill med 70 millioner måndelige brukere globalt</li><li><a href="www.kahoot.com">Kahoot</a> – læringsspill med 70 millioner måndelige brukere globalt</li><li><a href="www.kahoot.com">Kahoot</a> – læringsspill med 70 millioner måndelige brukere globalt</li><li><a href="www.kahoot.com">Kahoot</a> – læringsspill med 70 millioner måndelige brukere globalt</li></ul>'
];
export const tags = [
  {
    accordion: {
      guid: 'asd0vunqwe0ctunqwet',
      expandLabel: 'Vis',
      collapseLabel: 'Skjul'
    },
    items: [
      { text: 'Energi', url: '?1' },
      { text: 'Klima/Miljø', url: '?2' },
      { text: 'Helse', url: '?3' }
    ],
    numberOfVisibleItems: 2
  },
  {
    accordion: {
      guid: 'asd0vunqwe0ctunqwet',
      expandLabel: 'Vis',
      collapseLabel: 'Skjul'
    },
    items: [
      { text: 'Energi', url: '?1' },
      { text: 'Klima/Miljø', url: '?2' },
      { text: 'Helse', url: '?3' }
    ],
    numberOfVisibleItems: 0
  },
  {
    accordion: {
      guid: 'asd0vunqwe0ctunqwet',
      expandLabel: 'Vis',
      collapseLabel: 'Skjul'
    },
    items: [
      { text: 'Energi', url: '?1' },
      { text: 'Klima/Miljø', url: '?2' },
      { text: 'Helse', url: '?3' },
      { text: 'Utdanning og kompetanse' },
      { text: 'Klima/Miljø' },
      { text: 'Helse' },
      { text: 'Muliggjørende teknologier' },
      { text: 'Demokrati, styring og fornyelse' }
    ],
    numberOfVisibleItems: 2
  },
  {
    items: [
      { text: 'Utdanning og kompetanse' },
      { text: 'Klima/Miljø' },
      { text: 'Helse' },
      { text: 'Muliggjørende teknologier' },
      { text: 'Demokrati, styring og fornyelse' }
    ]
  },

  {
    items: [{ text: 'Utdanning og kompetanse' }]
  },
  {
    accordion: {
      guid: 'asd0vunqwe0ctunqwet',
      expandLabel: 'Vis',
      collapseLabel: 'Skjul'
    },
    items: [
      { text: 'Energi', url: '?1' },
      { text: 'Klima/Miljø', url: '?2' },
      { text: 'Helse', url: '?3' },
      { text: 'Muliggjørende teknologier' },
      { text: 'Demokrati, styring og fornyelse' }
    ],
    numberOfVisibleItems: 4
  }
];

export const dates = [
  [{ day: '24', month: 'apr', year: '2018' }],
  [{ day: '31', month: 'apr', year: '2018' }],
  [{ day: '03', month: 'sep' }],
  [{ day: '13', month: 'okt' }],
  [
    { day: '31.', month: 'apr', year: '2020' },
    { day: '2.', month: 'mai', year: '2021' }
  ],
  [{ day: '31.', month: 'apr' }, { day: '2.', month: 'mai' }],
  [
    { day: '4.', month: 'apr', year: '2020' },
    { day: '21.', month: 'mai', year: '2021' }
  ],
  [
    { day: '03.', month: 'apr', year: '2020' },
    { day: '25.', month: 'des', year: '2021' }
  ]
];

export const eventImage = [
  {
    image: {
      alt: 'Olav Normann Hansen',
      src: '/static-site/assets/profile-pic-2.jpg'
    }
  },
  {
    image: {
      alt: 'Olav Normann Hansen',
      src: '/static-site/assets/students.png'
    }
  },
  {
    image: {
      alt: 'Olav Normann Hansen'
    }
  },
  {
    background: 'background1'
  },
  {
    background: 'background2'
  },
  {
    background: 'background3'
  }
];
