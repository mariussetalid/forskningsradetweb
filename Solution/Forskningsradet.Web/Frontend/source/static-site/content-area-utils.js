/*
This file contains helper functions that build mock data for ContentArea and RichText.
It exists because working with mock data for ContentAreas and RichTexts can get very verbose.
The exported object contains a helper function for each existing React component.
*/

/**
 * @function utils.*
 *
 * @param { Object } componentData data to render the component with.
 * @param { String } size the size of the content area item ('-size-half' etc)
 * @param { Object } onPageEditing for ContentAreaItem instance
 * @returns { Object } ContentAreaItem data object
 * Use these functions when mocking the content inside of a ContentArea or RichText
 */

// Special cases:
/**
 *   @function utils.ContentArea
 *
 *   @param { ...Object } items any number of ContentAreaItem objects
 *   @returns { Object } ContentArea data object
 */

/**
 *   @function utils.RichText
 *
 *   @param { ...(string|Object) } items any number of string or ContentAreaItem objects
 *   @returns { Obejct } RichText data object
 *
 *   Strings are converted to data objects for 'HtmlString'
 */

/* Example (data):

  const contentAreaData = utils.ContentArea(
    utils.RichText('Hello'),
    utils.Image({ src: 'blabla', alt: 'blabla' })
  );
*/

/* Example (in React with data generated above):

  <ContentArea {...contentAreaData} />
*/

import * as Components from '../app.components';

// Builds model for ContentAreaItem
const generic = (componentName, componentData, size, onPageEditing) => ({
  componentName,
  componentData,
  onPageEditing,
  size
});

const componentNames = Object.keys(Components);

const utils = componentNames.reduce((accum, name) => {
  return Object.assign(accum, { [name]: (...args) => generic(name, ...args) });
}, {});

// Builds mock model for ContentArea component
const contentArea = (...items) => {
  // NOTE: This functions needs to support both array and spread arguments because IE11 crashes (because of some polyfill) when doing `contentArea(...blocks)`.
  const blocksData = Array.isArray(items[0]) ? items[0] : items;

  return {
    blocks: blocksData.map(
      ({ componentName, componentData, onPageEditing, size }, index) => ({
        id: String(index),
        componentName,
        componentData,
        onPageEditing,
        size
      })
    )
  };
};

// RichText is rendered using the ContentArea component. This helper function supports supplying one or more plain strings in addition to ContentAreaItem objects. All plain strings are rendered using the 'HtmlString' component.
const richText = (...items) =>
  contentArea(
    items.map(item =>
      typeof item === 'string' ? generic('HtmlString', { text: item }) : item
    )
  );

utils.ContentArea = contentArea;
utils.RichText = richText;

export default utils;
