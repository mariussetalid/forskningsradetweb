import React from 'react';
import PropTypes from 'prop-types';

import Breadcrumbs from 'components/breadcrumbs';
import Footer from 'components/footer';
import Header from 'components/header';

import breadcrumbsData from '../static-site/pages/breadcrumbs/breadcrumbs.json';
import footerData from '../static-site/pages/footer/footer.js';
import headerData from '../static-site/pages/header/header.json';

const Layout = ({ children, showBreadcrumbs, showHeader, showFooter }) => (
  <React.Fragment>
    <div>
      {showHeader && <Header {...headerData} />}
      {showBreadcrumbs && <Breadcrumbs {...breadcrumbsData} />}
      <main style={{ height: '100%' }} id="main">
        {children}
      </main>
      {showFooter && <Footer {...footerData} />}
    </div>
  </React.Fragment>
);

Layout.propTypes = {
  children: PropTypes.node,
  showBreadcrumbs: PropTypes.bool,
  showHeader: PropTypes.bool,
  showFooter: PropTypes.bool
};

Layout.defaultProps = {
  showHeader: true,
  showBreadcrumbs: true,
  showFooter: true
};

export default Layout;
