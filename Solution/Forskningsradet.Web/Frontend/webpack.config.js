/* eslint-env node */
/* eslint-disable no-console */
const path = require('path');
const webpack = require('webpack');
const autoprefixer = require('autoprefixer');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer')
  .BundleAnalyzerPlugin;
const cssnano = require('cssnano');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const CSharpWebpackPlugin = require('@creuna/prop-types-csharp/webpack-plugin');
const DirectoryNamedPlugin = require('directory-named-webpack-plugin');
const LodashModuleReplacementPlugin = require('lodash-webpack-plugin');
const { WebpackManifestPlugin } = require('webpack-manifest-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const StaticSiteGeneratorPlugin = require('static-site-generator-webpack-plugin');
const SuppressChunksPlugin = require('suppress-chunks-webpack-plugin').default;

// Relative to webpack.config
const CSharpModelsPath = path.join(
  '..',
  '..',
  'Forskningsradet.Core',
  'Models',
  'ReactModels'
);

module.exports = (env = {}, options = {}) => {
  const devServer = env.devServer;
  const shouldBuildStaticSite = env.static === true;
  const isProduction = options.mode === 'production';
  const shouldUseAnalyzer = env.analyzer === true;
  const filePathPrefix = env.prefix || '';
  const outputFolder = path.join(__dirname, 'dist');

  if (shouldBuildStaticSite) {
    console.log('🖥  Building static site');
  }

  if (isProduction) {
    console.log('📦  Minifying code');
  }

  if (shouldUseAnalyzer) {
    console.log('🕵🏻  Starting bundle analyzer');
  }

  return {
    devServer: {
      disableHostCheck: true,
      inline: false,
      stats: 'minimal',
      writeToDisk: true
    },
    devtool: isProduction ? 'source-map' : 'cheap-module-source-map',
    entry: (() => {
      const entries = isProduction
        ? {
            colorThemeKilden: './source/scss/color-theme-kilden.scss',
            colorThemeForskningsradet:
              './source/scss/color-theme-forskningsradet.scss',
            colorThemeRff: './source/scss/color-theme-rff.scss',
            colorThemeNp: './source/scss/color-theme-np.scss'
          }
        : {
            style:
              env.style === 'np'
                ? './source/scss/color-theme-np.scss'
                : './source/scss/color-theme-forskningsradet.scss'
          };
      const clientCommons = [
        'whatwg-fetch',
        './source/js/polyfills/scroll-behavior.js',
        './source/js/polyfills/nodelist-foreach.js',
        './source/js/polyfills/includes.js',
        './source/js/input-detection-loader'
        // NOTE: runs accessibility checks (using 'axe-core'). Should not be included in production bundle:
      ].concat(devServer ? './source/js/axe-runner' : []);

      if (shouldBuildStaticSite) {
        entries.client = clientCommons.concat(['./source/static-client.js']);
        entries.server = './source/static-server.js';
      } else {
        entries.client = clientCommons.concat([
          'expose-loader?exposes=React!react',
          'expose-loader?exposes=ReactDOM!react-dom',
          'expose-loader?exposes=Components!./source/app.components.js'
        ]);
        entries.server = [
          './source/js/server-polyfills.js',
          'expose-loader?exposes=React!react',
          'expose-loader?exposes=ReactDOM!react-dom',
          'expose-loader?exposes=ReactDOMServer!react-dom/server',
          'expose-loader?exposes=Components!./source/app.components.js'
        ];
      }

      return entries;
    })(),
    output: (() => {
      const output = {
        path: outputFolder,
        filename: '[name].[chunkhash].js',
        publicPath: ''
      };

      if (shouldBuildStaticSite) {
        output.libraryTarget = 'umd';
        output.globalObject = 'this';
      }

      return output;
    })(),
    module: {
      rules: [
        {
          test: /\.jsx?$/,
          exclude: /node_modules/,
          use: [
            'babel-loader',
            { loader: 'eslint-loader', options: { quiet: true } }
          ]
        },

        {
          enforce: 'pre',
          test: /\.scss$/,
          exclude: /node_modules/,
          use: 'import-glob'
        },
        {
          test: /\.scss$/,
          exclude: /node_modules/,
          use: [
            {
              loader: MiniCssExtractPlugin.loader
            },
            {
              loader: 'css-loader',
              options: {
                importLoaders: 1
              }
            },
            {
              loader: 'postcss-loader',
              options: {
                postcssOptions: {
                  plugins: [autoprefixer({ grid: true })].concat(
                    isProduction ? cssnano : []
                  )
                }
              }
            },
            // NOTE: the 'engine' config shouldn't be necessary, but because of a bug in 'resolve-url-loader@3.0.0' it is needed for now. See https://github.com/bholloway/resolve-url-loader/issues/107. When upgrading, the build should be checked on both MacOS and Windows.
            { loader: 'resolve-url-loader', options: { engine: 'rework' } },
            { loader: 'sass-loader' }
          ]
        },
        {
          test: /\.(svg|png|jpg|woff2?|ttf|eot)$/,
          exclude: [
            path.resolve(__dirname, './source/assets/inline-svg-icons')
          ],
          use: {
            loader: 'file-loader',
            options: {
              name: '[name].[hash].[ext]',
              publicPath: filePathPrefix
            }
          }
        },
        {
          test: /\.svg$/,
          include: [
            path.resolve(__dirname, './source/assets/inline-svg-icons')
          ],
          use: [
            { loader: 'svg-react-loader' },
            {
              loader: 'svgo-loader',
              options: {
                plugins: [
                  { removeViewBox: false },
                  { removeAttrs: { attrs: '(class|fill|stroke|data-name|id)' } }
                ]
              }
            }
          ]
        }
      ].concat(
        !shouldBuildStaticSite
          ? [
              {
                test: require.resolve('./source/app.components.js'),
                loader: 'expose-loader',
                options: {
                  exposes: { globalName: 'Components', override: true }
                }
              }
            ]
          : []
      )
    },
    resolve: {
      extensions: ['.js', '.jsx'],
      alias: {
        components: path.resolve(__dirname, 'source/components'),
        js: path.resolve(__dirname, 'source/js'),
        utils: path.resolve(__dirname, 'source/js/utils'),
        images: path.resolve(__dirname, 'source/assets/images'),
        mock: path.resolve(__dirname, 'source/static-site/mock')
      },
      plugins: [
        new DirectoryNamedPlugin({
          honorIndex: true,
          include: [
            path.resolve('./source/components'),
            path.resolve('./source/static-site/pages')
          ]
        })
      ]
    },
    plugins: [
      new MiniCssExtractPlugin({ filename: '[name].[contenthash].css' }),
      new LodashModuleReplacementPlugin({
        paths: true
      }),
      new WebpackManifestPlugin({
        filter: ({ name }) => {
          // Don't include map files and 'style.js' in manifest
          return (
            name.match(/\.(js|css)$/) &&
            ![
              'colorThemeForskningsradet.js',
              'colorThemeRff.js',
              'colorThemeNp.js',
              'colorThemeKilden.js'
            ].some(styleJS => styleJS === name)
          );
        }
      }),
      new SuppressChunksPlugin(
        [
          {
            name: 'style',
            match: /\.js(.map)?$/
          }
        ].concat(shouldBuildStaticSite ? ['server'] : [])
      )
    ]
      .concat(
        // NOTE: This plugin currently makes the codebase crash when recompiling using webpack-dev-server
        isProduction ? [new webpack.optimize.ModuleConcatenationPlugin()] : []
      )
      .concat(
        shouldBuildStaticSite
          ? [
              new StaticSiteGeneratorPlugin({
                entry: 'server',
                locals: {
                  isProduction
                },
                paths: require('./source/static-site/pages/paths')
              }),
              new CopyWebpackPlugin({
                patterns: [
                  {
                    from: 'source/static-site/assets',
                    to: 'static-site/assets'
                  },
                  {
                    from: 'source/static-site/api',
                    to: 'static-site/api'
                  }
                ]
              })
            ]
          : []
      )
      .concat([
        new CleanWebpackPlugin({
          cleanOnceBeforeBuildPatterns: [
            outputFolder,
            path.join(__dirname, CSharpModelsPath, '*.cs')
          ],
          dangerouslyAllowCleanPatternsOutsideProject: true,
          verbose: false,
          dry: false
        }),
        new CSharpWebpackPlugin({
          exclude: ['node_modules', 'source/static-site'],
          log: true,
          compilerOptions: {
            baseClass: 'ReactComponent',
            indent: 4,
            namespace: 'Forskningsradet.Core.Models.ReactModels'
          },
          path: path.join('..', CSharpModelsPath) // One extra '..' is added because this path is relative to the 'dist' folder
        })
      ])
      .concat(shouldUseAnalyzer ? [new BundleAnalyzerPlugin()] : []),
    optimization: {
      splitChunks: {
        cacheGroups: {
          commons: {
            test: module => {
              if (module.resource && /^.*\.(css|scss)$/.test(module.resource)) {
                return false;
              }

              return module.context && module.context.includes('node_modules');
            },
            chunks: chunk => chunk.name === 'client',
            name: 'vendor'
          }
        }
      }
    }
  };
};
