# Header

### Description

Top header bar (without mega menu)

### Tasks

1.  Content container
    - Some reusable component for setting max width on the content and horizontal centering
1.  Logo
    - Link - home
    - Image - logo
    - Image - logo inverted colors
1.  Links (language, log in)
    - Array of Link
    - Mobile?
1.  GlobalSearch
    - hidden on mobile
    - basic form that POSTs to `endpoint`
    - endpoint: `String`
    - placeholder: `String`
    - toggleButtonText: `String`
    - submitButtonText: `String`
    - Icon - magnifier
    - Icon - X
    - Animated width when it receives and loses focus
    - header changes color when search is expanded
1.  Toggle menu button
    - text: `String`
    - Icon
1.  [Menu](./menu.md)
