# Event list page

### Tasks

1.  Content container
1.  title: `String`
1.  Event element
    - title: `String`
    - url: `String`
    - description: `Array`
      - label: `String`
      - value: `String`
      - render as `<dl />`
    - text: `String`
    - tags: `Array`
      - Link: `Object`
    - dates: `Array` of `Object`
      - day: `String`
        - Can be a single number or two numbers (`'12'` or `'12-14'`)
      - month: `String`
    - isPastEvent: `Boolean`
      - special styling if true
    - pastEventLabel: `String`
    - Link to video: `Object`
    - Link to slides: `Object`
1.  Form for handling filters (sidebar)
    - Should send filter data asynchronously to an API endpoint, which should return a new list of event elements
1.  Calendar filter
    - Check if there's something we can use
