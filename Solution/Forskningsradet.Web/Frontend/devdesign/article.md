# Article

### Description

Article page. "Nyhetssak" and "Eksisterende nyhet".

### Tasks

1.  Add title, ingress and byline, and style article header.
    - title: `String`
    - ingress: `String`
    - byline: `Array` of `String`
    - bylineAtTop: `bool`
    - downloadLink: Link
      - Add icon in css
1.  PageLayout
    - Content area and sidebar
1.  Add and style article content
    - Image: `Object`
      - Link to image: Image object with link and alt
      - Image caption: `String`
      - Make image-with-caption component that use `image`-component
    - Rich text: Assuming that text is written in a rich text field, we can use `dangerouslysetinnerhtml` and style with css
1.  Accordion blocks in article
    - title: `String`
    - text: Assuming that text is written in a rich text field, we can use `dangerouslysetinnerhtml` and style with css
    - Can use `react-tiny-collapse` for accordions
1.  Sharing links
    - Link: `Array` of objects:
      - Provider: ``
      - text: ``
      - url: ``
1.  Contact info
    - title: `String`
    - items: `Array` of `Object`
      - title: `String`
      - type: `String` (to select icon)
      - text: `String` (rich text field)
1.  Info blocks:
    - Info blocks list: `Array` with [info blocks](info-block.md) in the correct order
1.  CTALink, `Object`
    - Custom a-tag
    - Image: `Object`
    - Text
    - Url
1.  Related content
    - Title
    - Text
    - Links: `Array`
      - Add icon for each link. Add iconClassName prop to Link-component.
1.  ImageAndText
    - Title
    - Text
    - Image
    - Use css grid
