# Info block

- Title: `String`
- Text: `String`, use `dangerouslysetinnerhtml`
- Pictogram: `String`, name of pictogram to use.
  - Pictograms should be separate from icons, because pictograms are editorial
  - Provide a list of supported pictograms
- Theme: Blue and yellow
- Url: Optional link URL for title or entire block. Needs clarification
- Optional call to action
  - Button: Link
