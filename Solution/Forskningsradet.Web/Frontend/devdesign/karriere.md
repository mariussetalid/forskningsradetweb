# Karriere/Ledige stillinger

### Tasks

- PageHeader:
  - Image: `FluidImage`
  - Title: `String`
- `ContentArea`:
  - Add support for icon to `InfoBlock`
- Add Vacancies component:
  - title: `String`
  - text: `String`
  - link: `Link`
  - items: `Array` of DateCard (Reuse the event item block)
- Refactor EventItem to DateCard
  - Add an option to have text ("Søknadsfrist") above the date and optional labels.
