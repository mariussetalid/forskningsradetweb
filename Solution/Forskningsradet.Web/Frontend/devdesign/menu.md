# Menu

### Description

Main navigation. Shown by clicking the "MENU" button in the [Header](./header.md). Slides in from the right

### Tasks

1.  Navigation groups: `Array`
    - title: `String`
    - list of navigation items: `Array`
      - items can be either a Link or a LinkList
      - Link: `Object`
      - LinkList:
        - title: `String`
        - links: `Array` of Link `Object`
        - Expand / Collapse (use something like `react-tiny-collapse`)
1.  Fixed positioning, fill screen vertically, internal scroll
1.  Global search
    - Only visible on mobile
1.  Close menu button
    - text: `String`
    - icon: X
1.  Toggling the menu
    - Do accessibility things
    - Animate sliding in from the right using `react-tiny-transition`
