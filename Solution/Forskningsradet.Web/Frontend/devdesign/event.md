# Event page

### Tasks

1.  title: `String`
1.  registrationLink: `Link` object
1.  registrationLabel: `String`
1.  links: `Array`
    - text: `String`
    - type: `String`
      - use to set icon and color
    - url: `String`
1.  metadata
    - items
      - label: `String`
      - text: `Array` of `Array` of `String`
1.  richText: HTML `String`
1.  speakers
    - title: `String`
    - items:
      - image: Image `Oject`
      - jobTitle: `String`
      - name: `String`
      - text: `String`
1.  schedule
    - title: `String`
    - dates: `Array`
      - title: `String`
      - times: `Array`
        - time: `String`
        - text: `String`
1.  contactInfo (see [Article](./article.md))
