using System;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Creuna.AzureAD.Contracts;
using Creuna.AzureAD.Utils.FeatureToggles;
using EPiServer.Security;
using EPiServer.ServiceLocation;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Owin.Extensions;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.Notifications;
using Microsoft.Owin.Security.OpenIdConnect;
using Owin;

namespace Forskningsradet.Web
{
    public partial class Startup
    {
        // This class comes as a part of the Creuna.AzureAD nuget package.
        //
        // The Client ID is used by the application to uniquely identify itself to Azure AD.
        // The Metadata Address is used by the application to retrieve the signing keys used by Azure AD.
        // The AAD Instance is the instance of Azure, for example public Azure or Azure China.
        // The Authority is the sign-in URL of the tenant.
        // The Post Logout Redirect Uri is the URL where the user will be redirected after they sign out.
        //
        private static readonly string clientId = ConfigurationManager.AppSettings["Creuna.AzureAD.ClientId"];
        private static readonly string aadInstance = ConfigurationManager.AppSettings["Creuna.AzureAD.AADInstance"];
        private static readonly string tenant = ConfigurationManager.AppSettings["Creuna.AzureAD.Tenant"];
        private static readonly string LoginRedirectUri = ConfigurationManager.AppSettings["Creuna.AzureAD.LoginRedirectUri"];
        private static readonly string postLogoutRedirectUri = ConfigurationManager.AppSettings["Creuna.AzureAD.PostLogoutRedirectUri"];
        private static readonly string authority = string.Format(CultureInfo.InvariantCulture, aadInstance, tenant);
        private const string LogoutUrl = "/util/logout.aspx";

        public void ConfigureAuth(IAppBuilder app)
        {
            if (new FeatureToggleDisableAzureAD().FeatureEnabled)
            {
                // feel free to call your own method here
                return;
            }
            else
            {
                app.SetDefaultSignInAsAuthenticationType(CookieAuthenticationDefaults.AuthenticationType);
                app.UseCookieAuthentication(new CookieAuthenticationOptions());
                app.UseOpenIdConnectAuthentication(new OpenIdConnectAuthenticationOptions
                {
                    ClientId = clientId,
                    Authority = authority,
                    PostLogoutRedirectUri = postLogoutRedirectUri,
                    UseTokenLifetime = false,
                    TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = false,
                        RoleClaimType = ClaimTypes.Role
                    },
                    Notifications = new OpenIdConnectAuthenticationNotifications
                    {
                        AuthenticationFailed = context =>
                        {
                            context.HandleResponse();
                            context.Response.Write(context.Exception.Message);
                            return Task.FromResult(0);
                        },
                        RedirectToIdentityProvider = context =>
                        {
                            // Here you can change the return uri based on multisite
                            context.ProtocolMessage.RedirectUri = GetRedirectUri();

                            // To avoid a redirect loop to the federation server send 403 
                            // when user is authenticated but does not have access
                            if (context.OwinContext.Response.StatusCode == 401 &&
                                context.OwinContext.Authentication.User.Identity.IsAuthenticated)
                            {
                                context.OwinContext.Response.StatusCode = 403;
                                context.HandleResponse();
                            }
                            return Task.FromResult(0);
                        },
                        SecurityTokenValidated = (ctx) =>
                        {
                            //Logger.Log(Level.Error, "SecurityTokenValidated: ctx.AuthenticationTicket.Identity.Claims, {Type, Value}: " + JsonConvert.SerializeObject(ctx.AuthenticationTicket.Identity.Claims.Select(x => new { x.Type, x.Value })));
                            ctx.AuthenticationTicket.Properties.ExpiresUtc = DateTime.UtcNow.AddHours(2);
                            var redirectUri = new Uri(ctx.AuthenticationTicket.Properties.RedirectUri,
                                UriKind.RelativeOrAbsolute);
                            if (redirectUri.IsAbsoluteUri)
                            {
                                ctx.AuthenticationTicket.Properties.RedirectUri = redirectUri.PathAndQuery;
                            }
                            ServiceLocator.Current.GetInstance<IIdentityUpdater>()
                                .UpdateIdentity(ctx.AuthenticationTicket.Identity);
                            //Sync user and the roles to EPiServer in the background
                            ServiceLocator.Current.GetInstance<ISynchronizingUserService>()
                                .SynchronizeAsync(ctx.AuthenticationTicket.Identity);
                            return Task.FromResult(0);
                        }
                    }
                });
                app.UseStageMarker(PipelineStage.Authenticate);
                app.Map(LogoutUrl, map =>
                {
                    map.Run(ctx =>
                    {
                        ctx.Authentication.SignOut();
                        return Task.FromResult(0);
                    });
                });
            }
        }

        private string GetRedirectUri()
        {
            var environment = ConfigurationManager.AppSettings["episerver:EnvironmentName"];

            if (ProbablyRunningLocalDev())
            {
                return LoginRedirectUri;
            }

            // When DB is copied from prod to QA or Dev login will fail as this method uses EPiServer.Web.SiteDefinition.Current.SiteUrl.Host which
            // will return the value configured in the database. The following will override the domain in the database to allow login and change
            // the values in admin for the different environments. 

            if (environment == "Production")
            {
                if (EPiServer.Web.SiteDefinition.Current.SiteUrl.Host.Contains("qa.nfr") || EPiServer.Web.SiteDefinition.Current.SiteUrl.Host.Contains("dev.nfr"))
                {
                    return $"https://beta.forskningsradet.no/episerver/";
                }
            }

            if (environment == "Preproduction")
            {
                if (EPiServer.Web.SiteDefinition.Current.SiteUrl.Host.Contains("forskningsradet.no") || EPiServer.Web.SiteDefinition.Current.SiteUrl.Host.Contains("dev.nfr"))
                {
                    return $"https://qa.nfr.no/episerver/";
                }
            }

            if (environment == "Integration")
            {
                if (EPiServer.Web.SiteDefinition.Current.SiteUrl.Host.Contains("qa.nfr") || EPiServer.Web.SiteDefinition.Current.SiteUrl.Host.Contains("forskningsradet.no"))
                {
                    return $"https://dev.nfr.no/episerver/";
                }
            }

            return $"https://{EPiServer.Web.SiteDefinition.Current.SiteUrl.Host}/episerver/";

            bool ProbablyRunningLocalDev() => !(new[] { "Integration", "Preproduction", "Production" }.Contains(environment));
        }
    }
}
