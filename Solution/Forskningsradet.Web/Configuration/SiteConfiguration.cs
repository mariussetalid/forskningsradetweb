﻿using System.Configuration;
using Forskningsradet.Common.Logging;
using Forskningsradet.Core.Configuration;
using Forskningsradet.ServiceAgents.Contracts;

namespace Forskningsradet.Web.Configuration
{
    public class SiteConfiguration 
        : IHrManagerServiceAgentConfiguration
        , IArchivingServiceAgentConfiguration
        , IArchiveConfiguration
        , IApsisConfiguration
        , ICrmConfiguration
        , IGoogleMapsConfiguration
        , IGoogleTagManagerConfiguration
        , IPaginationConfiguration
        , IProposalPageConfiguration
        , ICacheConfiguration
        , IPdfConfiguration
        , IProjectDatabankConfiguration
        , ILoggingConfiguration
        , INtbServiceAgentConfiguration
    {
        public string HrManagerBaseUri => GetAppSetting("HrManagerBaseUri");
        public string HrManagerVacanciesUri => GetAppSetting("HrManagerVacanciesUri");
        public string HrManagerVacancyUri => GetAppSetting("HrManagerVacancyUri");
        public string NtbUri => GetAppSetting("NtbUri");
        public string ArchivingServiceBaseUri => GetAppSetting("Archiving.ServiceBaseUri");
        public string ArchivingServiceSaveWebContentUri => GetAppSetting("Archiving.ServiceSaveWebContentUri");
        public string GoogleMapsApiKey => GetAppSetting("GoogleMapsApiKey");
        public string GoogleTagManagerId => GetAppSetting("GoogleTagManagerId");
        public int? EmployeeListPaginationSize => GetAppSettingAsInt("EmployeeListPaginationSize");
        public int? CacheEmployeeLettersInDays => GetAppSettingAsInt("CacheEmployeeLettersInDays");
        public string ServerIp => GetAppSetting("PdfConfiguration.ServerIp");
        public int Port => GetAppSettingAsInt("PdfConfiguration.Port") ?? 0;
        public int HtmlViewerWidth => GetAppSettingAsInt("PdfConfiguration.HtmlViewerWidth") ?? 1200;
        public int ConversionDelay => GetAppSettingAsInt("PdfConfiguration.ConversionDelay") ?? 0;
        public int ArchiveConversionDelay => GetAppSettingAsInt("PdfConfiguration.ArchiveConversionDelay") ?? ConversionDelay;
        public string LicenseKey => GetAppSetting("PdfConfiguration.LicenseKey");
        public string MyPageUrl => GetAppSetting("ProposalPageMyPageUrl");
        public string ProjectDatabankSearchUrl => GetAppSetting("ProjectDataBankSearchUrl");
        public string QueueEndpoint => GetAppSetting("QueueEndpoint");
        public string ArchivingQueueName => GetAppSetting("Archiving.QueueName");
        public string ApsisFormId => GetAppSetting("ApsisFormId");
        public string ApsisDivId => GetAppSetting("ApsisDivId");
        public string DynamicsAppId => GetAppSetting("DynamicsAppId");
        public string DynamicsSecret => GetAppSetting("DynamicsSecret");
        public string DynamicsUrl => GetAppSetting("DynamicsUrl");
        public bool UseSerilog => GetAppSettingAsBool("UseSerilog");
        
        private static string GetAppSetting(string key) => ConfigurationManager.AppSettings[key];

        private static int? GetAppSettingAsInt(string key)
        {
            var value = GetAppSetting(key);
            
            if (string.IsNullOrEmpty(value))
                return null;
            
            return int.TryParse(value, out var returnValue)
                ? returnValue
                : (int?) null;
        }

        private static bool GetAppSettingAsBool(string key) =>
            GetAppSetting(key) == true.ToString().ToLower();
    }
}
