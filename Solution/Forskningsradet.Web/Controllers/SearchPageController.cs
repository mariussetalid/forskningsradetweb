﻿using System.Web.Mvc;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Factories;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.QueryParameters;

namespace Forskningsradet.Web.Controllers
{
    public class SearchPageController : BasePageController<SearchPage>
    {
        private readonly IViewModelFactory _viewModelFactory;

        public SearchPageController(IViewModelFactory viewModelFactory)
        {
            _viewModelFactory = viewModelFactory;
        }

        public ViewResult Index(SearchPage currentPage, string q, string[] subjects, string[] categories, string[] targetgroups, string[] type, int? page, int? from, int? to)
        {
            var query = new SearchQueryParameter(subjects, targetgroups, type, categories, q, from, to, page);

            var model = _viewModelFactory.GetPageViewModel(currentPage, query);
            return View(RazorViews.DefaultReactViewPath, model);
        }
    }
}