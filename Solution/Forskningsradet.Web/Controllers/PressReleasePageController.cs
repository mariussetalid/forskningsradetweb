using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Extensions;
using Forskningsradet.Core.Factories;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.QueryParameters;
using Forskningsradet.Core.Models.ViewModels.Contracts;
using Forskningsradet.Core.Models.ViewModels.Pages;
using Forskningsradet.Core.Processes.Providers;
using Forskningsradet.Core.Repositories.Contracts;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Web.Controllers
{
    public class PressReleasePageController : BasePageController<PressReleasePage>
    {
        private readonly IViewModelFactory _viewModelFactory;
        private readonly IHtmlToPdfProvider _htmlToPdfProvider;

        public PressReleasePageController(
            IViewModelFactory viewModelFactory,
            IHtmlToPdfProvider htmlToPdfProvider)
        {
            _viewModelFactory = viewModelFactory;
            _htmlToPdfProvider = htmlToPdfProvider;
        }

        public ActionResult Index(PressReleasePage currentPage)
        {
            var model = _viewModelFactory.GetPageViewModel(currentPage);
            return View(RazorViews.DefaultReactViewPath, model);
        }

        public ActionResult Download(PressReleasePage currentPage)
        {
            var file = _htmlToPdfProvider.GetPageAsDownloadablePdf(currentPage);
            return File(file, "application/pdf", GetFileName(currentPage));
        }

        private string GetFileName(PressReleasePage currentPage) =>
            $"{currentPage.PageName.EnsureValidEnglishWindowsFileName()}.pdf";
    }
}