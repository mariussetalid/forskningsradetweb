﻿using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Factories;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.QueryParameters;
using System.Web.Mvc;

namespace Forskningsradet.Web.Controllers
{
    public class CategoryListPageController : BasePageController<CategoryListPage>
    {
        private readonly IViewModelFactory _viewModelFactory;

        public CategoryListPageController(IViewModelFactory viewModelFactory) => _viewModelFactory = viewModelFactory;

        public ViewResult Index(CategoryListPage currentPage, int[] categories, int? page)
        {
            var query = (QueryParameterBase)new CategoryListQueryParameter
            {
                Categories = categories,
                Page = page ?? 1
            };
            var model = _viewModelFactory.GetPageViewModel(currentPage, query);
            return View(RazorViews.DefaultReactViewPath, model);
        }
    }
}