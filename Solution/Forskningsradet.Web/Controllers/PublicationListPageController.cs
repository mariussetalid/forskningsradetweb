﻿using System.Web.Mvc;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Factories;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.QueryParameters;

namespace Forskningsradet.Web.Controllers
{
    public class PublicationListPageController : BasePageController<PublicationListPage>
    {
        private readonly IViewModelFactory _viewModelFactory;

        public PublicationListPageController(IViewModelFactory viewModelFactory) 
            => _viewModelFactory = viewModelFactory;

        public ViewResult Index(PublicationListPage currentPage, string q, int[] type, int? year, int? page)
        {
            var queryParameter = (QueryParameterBase) new PublicationListQueryParameter
            {
                Query = q,
                Year = year ?? 0,
                Types = type,
                Page = page ?? 0
            };
            var model = _viewModelFactory.GetPageViewModel(currentPage, queryParameter);
            return View(RazorViews.DefaultReactViewPath, model);
        }
    }
}