﻿using EPiServer.Framework.DataAnnotations;
using Forskningsradet.Core.Factories;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Web.RenderTemplates;

namespace Forskningsradet.Web.Controllers
{
    [TemplateDescriptor(Default = true, Inherited = true)]
    public class BaseBlockDataController : ViewRenderTemplate<BaseBlockData>
    {
        private readonly IViewModelFactory _viewModelFactory;

        public BaseBlockDataController(IViewModelFactory viewModelFactory) 
            => _viewModelFactory = viewModelFactory;

        protected override void Render(BaseBlockData currentContent)
        {
            var viewModel = _viewModelFactory.GetPartialViewModel(currentContent);
            RenderPartialView(viewModel);
        }
    }
}