﻿using System.Web.Mvc;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Factories;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.QueryParameters;

namespace Forskningsradet.Web.Controllers
{
    public class GroupedSearchPageController : BasePageController<GroupedSearchPage>
    {
        private readonly IViewModelFactory _viewModelFactory;

        public GroupedSearchPageController(IViewModelFactory viewModelFactory)
        {
            _viewModelFactory = viewModelFactory;
        }

        public ViewResult Index(GroupedSearchPage currentPage, string q = null, int? page = null, char? letter = null)
        {
            var query = currentPage is GlossaryPage
                ? (QueryParameterBase) new GlossaryQueryParameter {Query = q}
                : (QueryParameterBase) new EmployeeListQueryParameter {Query = q, Page = page, Letter = letter};
            
            var model = _viewModelFactory.GetPageViewModel(currentPage, query);
            return View(RazorViews.DefaultReactViewPath, model);
        }
    }
}