﻿using System;
using System.IO;
using System.Threading.Tasks;
using System.Web.Mvc;
using EPiServer;
using EPiServer.Core;
using EPiServer.Framework.Blobs;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Extensions;
using Forskningsradet.Core.Factories;
using Forskningsradet.Core.Models.ContentModels.Media;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.QueryParameters;
using Forskningsradet.Core.Processes.Providers;

namespace Forskningsradet.Web.Controllers
{
    public class LargeDocumentPageController : BasePageController<LargeDocumentPageBase>
    {
        private readonly IViewModelFactory _viewModelFactory;
        private readonly IHtmlToPdfProvider _htmlToPdfProvider;
        private readonly IContentLoader _contentLoader;

        public LargeDocumentPageController(IViewModelFactory viewModelFactory, IHtmlToPdfProvider htmlToPdfProvider, IContentLoader contentLoader)
        {
            _viewModelFactory = viewModelFactory;
            _htmlToPdfProvider = htmlToPdfProvider;
            _contentLoader = contentLoader;
        }

        public ViewResult Index(LargeDocumentPageBase currentPage, bool download = false)
        {
            var query = (QueryParameterBase)new LargeDocumentQueryParameter
            {
                Download = download
            };

            var model = _viewModelFactory.GetPageViewModel(currentPage, query);
            return View(RazorViews.DefaultReactViewPath, model);
        }

        public ActionResult Download(LargeDocumentPageBase currentPage)
        {
            var file = GetOverridePdf(currentPage) ?? GetDownloadFile(currentPage);
            return File(file, "application/pdf", $"{currentPage.PageName.EnsureValidEnglishWindowsFileName()}.pdf");
        }

        public byte[] GetDownloadFile(LargeDocumentPageBase currentPage)
        {
            var queryString = $"?{QueryParameters.Download}=true";
            return _htmlToPdfProvider.GetPageAsDownloadablePdf(currentPage, queryString);
        }

        public byte[] GetOverridePdf(LargeDocumentPageBase currentPage)
        {
            var reference = (currentPage as LargeDocumentPage)?.OverrideDownloadPdf;
            if (ContentReference.IsNullOrEmpty(reference))
                return null;

            try
            {
                var media = _contentLoader.Get<GenericMedia>(reference);
                return media.BinaryData.ReadAllBytes();
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}