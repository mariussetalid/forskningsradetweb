﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using EPiServer;
using EPiServer.Core;
using EPiServer.Framework.DataAnnotations;
using EPiServer.Framework.Web;
using EPiServer.Web;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.ContentModels.Support;
using Forskningsradet.Core.Models.ViewModels.Contracts;
using Forskningsradet.Core.Models.ViewModels.Pages;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Web.Controllers
{
    [TemplateDescriptor(
        Inherited = true,
        ModelType = typeof(BaseBlockData),
        Tags = new[] { RenderingTags.Preview },
        TemplateTypeCategory = TemplateTypeCategories.MvcController)]
    public class BlockPreviewController : BasePageController<BlockPreviewPage>, IRenderTemplate<BaseBlockData>
    {
        private readonly IContentAreaReactModelBuilder _contentAreaReactModelBuilder;
        private readonly IContentLoader _contentLoader;

        public BlockPreviewController(IContentAreaReactModelBuilder contentAreaReactModelBuilder, IContentLoader contentLoader)
        {
            _contentAreaReactModelBuilder = contentAreaReactModelBuilder;
            _contentLoader = contentLoader;
        }

        public ActionResult Index(IContent currentContent)
        {
            var previewItem = BuildPreviewItem(currentContent.ContentLink);
            var includeFullwidthPreview = CheckForFullWidthAttribute(currentContent.ContentLink);
            var contentAreaPage = new Core.Models.ReactModels.ContentAreaPage
            {
                Content = BuildPreviewContentArea(previewItem, includeFullwidthPreview)
            };
            var secondContentAreaPage = new Core.Models.ReactModels.ContentAreaPage
            {
                Content = BuildSecondPreviewContentArea(previewItem)
            };
            var model = new BlockPreviewPageViewModel(new BlockPreviewPage(), contentAreaPage, secondContentAreaPage, previewItem.FullRefreshProperties);

            return View("~/Views/Pages/BlockPreviewPage.cshtml", model);
        }

        private IReactViewModel BuildPreviewItem(ContentReference contentReference)
        {
            return _contentAreaReactModelBuilder.BuildContentAreaPreviewItem(contentReference);
        }
        
        private bool CheckForFullWidthAttribute(ContentReference contentReference)
            => _contentLoader.Get<IContentData>(contentReference)
                .GetOriginalType()
                .GetCustomAttributes(typeof(IncludeFullWidthPreview), true)
                .FirstOrDefault() is IncludeFullWidthPreview;

        private ReactModels.ContentArea BuildPreviewContentArea(IReactViewModel previewItem, bool includeFullWidthPreview)
        {
            var previewContentArea = new ReactModels.ContentArea
            {
                Blocks = new List<ReactModels.ContentAreaItem>()
            };

            if (includeFullWidthPreview)
            {
                previewContentArea.Blocks.Add(BuildPreviewItem(previewItem, "0", ReactModels.ContentAreaItem_Size.FullScreen));
            }
            previewContentArea.Blocks.Add(BuildPreviewItem(previewItem, "1", ReactModels.ContentAreaItem_Size.None));
            previewContentArea.Blocks.Add(BuildPreviewItem(previewItem, "2", ReactModels.ContentAreaItem_Size.Half));

            return previewContentArea;
        }

        private ReactModels.ContentArea BuildSecondPreviewContentArea(IReactViewModel previewItem)
        {
            var previewContentArea = new ReactModels.ContentArea
            {
                Blocks = new List<ReactModels.ContentAreaItem>()
            };

            previewContentArea.Blocks.Add(BuildPreviewItem(previewItem, "4", ReactModels.ContentAreaItem_Size.Third));

            return previewContentArea;
        }

        private ReactModels.ContentAreaItem BuildPreviewItem(IReactViewModel item, string id, ReactModels.ContentAreaItem_Size size)
            => new ReactModels.ContentAreaItem
            {
                ComponentData = item.Model,
                ComponentName = item.ReactComponentName,
                Id = id,
                Size = size
            };
    }
}