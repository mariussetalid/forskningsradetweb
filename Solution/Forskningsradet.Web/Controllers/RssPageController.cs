﻿using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.Globalization;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Extensions;
using Forskningsradet.Core.Facades.Contracts;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.RequestModels;
using Forskningsradet.Core.Models.SearchModels;
using Forskningsradet.Core.Models.ViewModels.Pages;
using Forskningsradet.Core.Services;
using Forskningsradet.Web.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Syndication;
using System.Web.Mvc;
using ReactModels = Forskningsradet.Core.Models.ReactModels;
using SyndicationCategory = System.ServiceModel.Syndication.SyndicationCategory;

namespace Forskningsradet.Web.Controllers
{
    public class RssPageController : BasePageController<RssPage>
    {
        private readonly CategoryRepository _categoryRepository;
        private readonly IContentLoader _contentLoader;
        private readonly IContentListService _contentListService;
        private readonly IDateCardDatesReactModelBuilder _dateCardDatesReactModelBuilder;
        private readonly IPageEditingAdapter _pageEditingAdapter;

        public RssPageController(
            CategoryRepository categoryRepository,
            IContentLoader contentLoader,
            IPageEditingAdapter pageEditingAdapter,
            IDateCardDatesReactModelBuilder dateCardDatesReactModelBuilder,
            IContentListService contentListService)
        {
            _categoryRepository = categoryRepository;
            _contentLoader = contentLoader;
            _contentListService = contentListService;
            _dateCardDatesReactModelBuilder = dateCardDatesReactModelBuilder;
            _pageEditingAdapter = pageEditingAdapter;
        }

        public ActionResult Index(RssPage currentPage)
        {
            if (_pageEditingAdapter.PageIsInEditMode())
                return Preview(currentPage);
            return RssResult(currentPage);
        }

        private ViewResult Preview(RssPage currentPage)
        {
            var pages = GetPages(currentPage);
            var items = BuildPreviewItems(pages);

            var model = new ReactPageViewModel(
                new EventListPage(),
                new ReactModels.EventListPage
                {
                    Title = currentPage.PageName,
                    TopFilter = new ReactModels.TopFilter
                    {
                        Options = new List<ReactModels.TopFilter_Options>(),
                        Timeframe = new ReactModels.TopFilter_Timeframe {Id = "1"},
                        Title = currentPage.Description
                    },
                    Events = items
                });
            return View(RazorViews.DefaultReactViewPath, model);
        }

        private ActionResult RssResult(RssPage currentPage)
        {
            var pages = GetPages(currentPage);
            var items = BuildSyndicationItems(pages);

            var feed = new SyndicationFeed(
                currentPage.PageName,
                currentPage.Description,
                CreateExternalUrlAsUri(currentPage.PageLink));
            feed.Items = items;
            return new RssActionResult { Feed = feed };
        }

        private IEnumerable<EditorialPage> GetPages(RssPage currentPage)
        {
            if (currentPage.ContentArea?.Count > 0)
            {
                return GetPagesFromContentArea(currentPage);
            }

            if (Search(currentPage) is ContentListSearchResults result && result.Results.Any())
            {
                return result.Results;
            }

            return new List<EditorialPage>();
        }

        private IEnumerable<EditorialPage> GetPagesFromContentArea(RssPage page) =>
            page.ContentArea.FilteredItems
                .Select(x => x.ContentLink)
                .Select(x => _contentLoader.Get<PageData>(x))
                .OfType<EditorialPage>();

        private List<SyndicationItem> BuildSyndicationItems(IEnumerable<EditorialPage> pages)
        {
            var items = new List<SyndicationItem>();
            foreach (var page in pages)
            {
                var item = new SyndicationItem(
                    page.GetListTitle(),
                    page.GetListIntroOrMainIntro(),
                    CreateExternalUrlAsUri(page.PageLink));

                item.PublishDate = new DateTimeOffset(page.StartPublish ?? DateTime.Now);

                var categories = page.Category.Select(x => new SyndicationCategory(_categoryRepository.Get(x).LocalizedDescription));
                foreach (var category in categories)
                {
                    item.Categories.Add(category);
                }

                items.Add(item);
            }

            return items;
        }

        private Uri CreateExternalUrlAsUri(ContentReference pageLink) =>
            new Uri(
                pageLink.ContentExternalUrl(ContentLanguage.PreferredCulture, true),
                UriKind.Absolute);

        private ContentListSearchResults Search(RssPage currentPage)
        {
            var query = new ContentListRequestModel
            {
                PageRoot = currentPage.PageRoot?.ID ?? ContentReference.StartPage.ID,
                Take = currentPage.NumberOfVisibleItems > 0 ? currentPage.NumberOfVisibleItems : 100,
                Categories = currentPage.Category.ToArray(),
                ContentTypes = currentPage.PageTypes?.Split(',').Select(int.Parse).ToArray()
            };
            return _contentListService.Search<EditorialPage>(query);
        }

        private List<ReactModels.DateCard> BuildPreviewItems(IEnumerable<EditorialPage> pages) =>
            pages.Select(page =>
                    new ReactModels.DateCard
                    {
                        Title = page.GetListTitle(),
                        Text = page.GetListIntroOrMainIntro(),
                        DateContainer = _dateCardDatesReactModelBuilder.BuildReactModel(BuildDates(page.Changed), null),
                        Tags = new ReactModels.DateCardTags
                        {
                            Items = page.Category
                                .Select(x => _categoryRepository.Get(x))
                                .Select(x => new ReactModels.Link { Text = x.LocalizedDescription })
                                .ToList()
                        },
                        Metadata = new ReactModels.Metadata
                        {
                            Items = new List<ReactModels.Metadata_Items>
                            {
                                new ReactModels.Metadata_Items {Label = "Type", Text = page.PageTypeName}
                            }
                        }
                    })
                .ToList();

        private List<ReactModels.DateCardDates_Dates> BuildDates(DateTime date) =>
            new List<ReactModels.DateCardDates_Dates>
            {
                new ReactModels.DateCardDates_Dates
                {
                    Day = date.CultureSpecificDay(),
                    Month = date.ToString(DateTimeFormats.ShortMonth),
                    Year = date.Year.ToString()
                }
            };
    }
}