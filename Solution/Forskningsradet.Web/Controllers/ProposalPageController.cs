﻿using EPiServer;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Extensions;
using Forskningsradet.Core.Factories;
using Forskningsradet.Core.Models.ContentModels.Media;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Processes.Providers;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Forskningsradet.Web.Controllers
{
    public class ProposalPageController : BasePageController<ProposalPage>
    {
        private readonly IViewModelFactory _viewModelFactory;
        private readonly IContentLoader _contentLoader;
        private readonly IHtmlToPdfProvider _htmlToPdfProvider;

        public ProposalPageController(IViewModelFactory viewModelFactory, IContentLoader contentLoader, IHtmlToPdfProvider htmlToPdfProvider)
        {
            _viewModelFactory = viewModelFactory;
            _contentLoader = contentLoader;
            _htmlToPdfProvider = htmlToPdfProvider;
        }

        public ActionResult Index(ProposalPage currentPage)
        {
            var model = _viewModelFactory.GetPageViewModel(currentPage);
            return View(RazorViews.DefaultReactViewPath, model);
        }

        public ActionResult Download(BasePageData currentPage)
        {
            var file = _htmlToPdfProvider.GetPageAsDownloadablePdf(currentPage);
            return File(file, "application/pdf", $"{currentPage.PageName.EnsureValidEnglishWindowsFileName()}.pdf");
        }

        public async Task<ActionResult> DownloadTemplates(ProposalPage currentPage)
        {
            using (var ms = new MemoryStream())
            {
                using (var archive = new ZipArchive(ms, ZipArchiveMode.Create, true))
                {
                    foreach (var genericMedia in currentPage.ApplicationTemplates.Select(x => _contentLoader.Get<GenericMedia>(x)))
                    {
                        var buffer = await GetBinaryData(genericMedia);

                        if (buffer is null) 
                            continue;

                        var zipEntry = archive.CreateEntry(genericMedia.Name, CompressionLevel.Fastest);
                        using (var zipStream = zipEntry.Open())
                        {
                            zipStream.Write(buffer, 0, buffer.Length);
                        }
                    }
                }
                return File(ms.ToArray(), "application/zip", $"{currentPage.Name.EnsureValidEnglishWindowsFileName()}.zip");
            }
        }

        private async Task<byte[]> GetBinaryData(GenericMedia media)
        {
            try
            {
                using (var mediaStream = media.BinaryData.OpenRead())
                {
                    var buffer = new byte[mediaStream.Length];
                    _ = await mediaStream.ReadAsync(buffer, 0, (int)mediaStream.Length);
                    return buffer;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}