﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using EPiServer;
using EPiServer.Core;
using EPiServer.Framework.Localization;
using EPiServer.Globalization;
using EPiServer.Web;
using EPiServer.Web.Routing;
using Forskningsradet.Core.Models.ContentModels.Media;
using Forskningsradet.Core.Models.ContentModels.Pages;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Web.Controllers
{
    public class ErrorPageController : Controller
    {
        readonly LocalizationService _localizationService;
        readonly IContentLoader _contentLoader;
        readonly IUrlResolver _urlResolver;
        readonly IUpdateCurrentLanguage _updateCurrentLanguage;

        public ErrorPageController(LocalizationService localizationService, IUpdateCurrentLanguage updateCurrentLanguage, IContentLoader contentLoader, IUrlResolver urlResolver)
        {
            _localizationService = localizationService;
            _updateCurrentLanguage = updateCurrentLanguage;
            _contentLoader = contentLoader;
            _urlResolver = urlResolver;
        }

        public ActionResult Gone()
        {
            HttpContext.Response.StatusCode = 410;

            SetLanguageFromUrl(Request.RawUrl);
            var message = _localizationService.GetString($"/{nameof(ReactModels.ErrorPage)}/NotFoundMessage");
            var model = BuildErrorPage("410", message);

            return View("NotFound", model);
        }

        public ActionResult NotFound()
        {
            HttpContext.Response.StatusCode = 404;

            SetLanguageFromUrl(Request.RawUrl);
            var message = _localizationService.GetString($"/{nameof(ReactModels.ErrorPage)}/NotFoundMessage");
            var model = BuildErrorPage("404", message, GetLogoUrl());

            return View(model);
        }

        private ReactModels.Image GetLogoUrl()
        {
            if (ContentReference.IsNullOrEmpty(ContentReference.StartPage))
                return null;

            if(_contentLoader.TryGet(ContentReference.StartPage, out FrontPageBase frontPage))
            {
                return GetImage(frontPage?.MainLogo);
            }

            return null;
        }

        private ReactModels.Image GetImage(ContentReference contentReference)
        {
            return GetImageFile() is ImageFile imageFile
                ? new ReactModels.Image
                {
                    Alt = imageFile?.AltText ?? string.Empty,
                    Src = imageFile != null ? _urlResolver.GetUrl(imageFile.ContentLink) : string.Empty,
                }
                : null;

            ImageFile GetImageFile() =>
                contentReference is null ? null : _contentLoader.Get<ImageFile>(contentReference);
        }

        public ActionResult Error()
        {
            HttpContext.Response.StatusCode = 500;
            try
            {
                SetLanguageFromUrl(Request.RawUrl);
                var message = _localizationService.GetString($"/{nameof(ReactModels.ErrorPage)}/Error500Message");
                var model = BuildErrorPage("500", message);
                return View("NotFound", model);
            }
            catch (Exception)
            {
                return File("~/500.html", "text/html");
            }
        }

        private ReactModels.ErrorPage BuildErrorPage(string errorCode, string message, ReactModels.Image logoImage = null)
            => new ReactModels.ErrorPage
            {
                ErrorCode = errorCode,
                LinkUrl = ContentLanguage.PreferredCulture.TwoLetterISOLanguageName == "en"
                    ? "/en"
                    : "/",
                Text = BuildRichText(message),
                Logo = logoImage
            };

        private ReactModels.RichText BuildRichText(string message)
            => new ReactModels.RichText
            {
                Blocks = new List<ReactModels.ContentAreaItem>
                {
                    new ReactModels.ContentAreaItem
                    {
                        Id = "1",
                        ComponentName = nameof(ReactModels.HtmlString),
                        ComponentData = new ReactModels.HtmlString
                        {
                            Text = message,
                            OnPageEditing = null,
                        }
                    }
                }
            };

        private void SetLanguageFromUrl(string url)
        {
            var segments = url.Trim('/').Split('/');
            if (segments.Any())
            {
                var languageSegment = segments.First();
                if (languageSegment == "en")
                {
                    _updateCurrentLanguage.UpdateLanguage("en");
                }
            }

            var resolvedLanguage = ContentLanguage.PreferredCulture.Name;
            Request.RequestContext.SetLanguage(resolvedLanguage);
        }
    }
}