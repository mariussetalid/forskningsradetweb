using System.Threading.Tasks;
using System.Web.Http;
using Forskningsradet.Common.Logging;
using Forskningsradet.Core.Behavior;
using Forskningsradet.Core.Extensions;
using Forskningsradet.Core.Models.ApiModels.DWH;
using Forskningsradet.Core.Models.ApiModels.Evurdering;
using Forskningsradet.Web.Infrastructure;
using MediatR;
using Microsoft.Web.Http;
using Swashbuckle.Swagger.Annotations;

namespace Forskningsradet.Web.Controllers.Api
{
    /// <summary>
    /// API for mottak av meldinger fra eVurdering.
    /// </summary>
    [RequireHttps]
    [BasicAuthenticationFilter]
    [ApiExceptionFilter]
    [ApiVersion("1.0")]
    [RoutePrefix("api/v{version:apiVersion}")]
    public class ProposalController : ApiController
    {
        private readonly IMediator _mediator;
        private readonly IIntegrationLogger _integrationLogger;

        public ProposalController(IMediator mediator, IIntegrationLogger integrationLogger)
        {
            _mediator = mediator;
            _integrationLogger = integrationLogger;
        }

        /// <summary>
        /// Tar i mot oppdatering fra eVurdering. Oppretter eller oppdaterer utlysning med detaljene fra forespørselen.
        /// </summary>
        /// <param name="utlysning">Utlysningen</param>
        /// <param name="brukernavn">Navn på brukeren som utførte handlingen</param>
        /// <returns>Tomt resultat med HTTP-kode.</returns>
        /// <response code="200">Utlysning oppdatert/opprettet.</response>
        /// <response code="400">Ugyldig format på forespørselen.</response>
        /// <response code="401">Autentisering feilet.</response>
        /// <response code="403">Ugyldig URL scheme.</response>
        [HttpPost]
        [Route("utlysninger/{brukernavn?}")]
        [SwaggerOperation(Tags = new[] { "Utlysninger" })]
        public async Task<IHttpActionResult> HandleUpdate(
            [FromBody]Proposal utlysning, 
            string brukernavn, 
            long? oppdatertTidspunkt = null)
        {
            _integrationLogger.LogInfo("Received request to update proposal with object {@Proposal}.", utlysning);
            
            if (!ModelState.IsValid)
            {
                LogInvalidModelState();
                return BadRequest(ModelState);
            }
            
            var command = new CreateOrUpdateProposal.Command
            {
                Proposal = utlysning, 
                OriginalMessageTimestamp = oppdatertTidspunkt?.FromEpochDateTime()
            };
            await _mediator.Send(command);
            
            _integrationLogger.LogDebug("Proposal with id {Id} updated.", utlysning.Id);
            
            return Ok();
        }

        /// <summary>
        /// Tar i mot oppdatering fra av søknadsresultat.
        /// </summary>
        /// <param name="resultat">Det oppdaterte søknadsresultatet.</param>
        /// <param name="id">ID på utlysningen</param>
        /// <returns>Tomt resultat med HTTP-kode.</returns>
        /// <response code="200">Søknadsresultat oppdatert/opprettet.</response>
        /// <response code="400">Ugyldig format på forespørselen.</response>
        /// <response code="401">Autentisering feilet.</response>
        /// <response code="403">Ugyldig URL scheme.</response>
        [HttpPost]
        [Route("utlysninger/{id}/resultat")]
        [SwaggerOperation(Tags = new[] { "Utlysninger" })]
        public async Task<IHttpActionResult> UpdateApplicationResult(long id, [FromBody]ApplicationResult resultat)
        {
            _integrationLogger.LogInfo("Received request to update application result with object {@Result}.", resultat);
            
            if (!ModelState.IsValid)
            {
                LogInvalidModelState();
                return BadRequest(ModelState);
            }
            
            var command = new UpdateApplicationResult.Command {ApplicationResult = resultat};
            await _mediator.Send(command);
            
            _integrationLogger.LogDebug("Application result for proposal with id {Id} updated.", resultat.ProposalId);
            
            return Ok();
        }
        
        private void LogInvalidModelState() =>
            _integrationLogger.LogError("Bad request, invalid model {@Model}.", null, ModelState.Values);
    }
}