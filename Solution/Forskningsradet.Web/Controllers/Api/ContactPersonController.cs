using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Forskningsradet.Common.Logging;
using Forskningsradet.Core.Behavior;
using Forskningsradet.Core.Models.ApiModels.Idm;
using Forskningsradet.Web.Infrastructure;
using MediatR;
using Microsoft.Web.Http;

namespace Forskningsradet.Web.Controllers.Api
{
    /// <summary>
    /// API for receiving contact person updates.
    /// </summary>
    [RequireHttps]
    [BasicAuthenticationFilter]
    [ApiExceptionFilter]
    [ApiVersion("1.0")]
    [RoutePrefix("api/v{version:apiVersion}/contact-persons")]
    public class ContactPersonController : ApiController
    {
        private readonly IMediator _mediator;
        private readonly IIntegrationLogger _logger;
        private const string GetContactPersonUri = "get-contact-person";

        public ContactPersonController(IMediator mediator, IIntegrationLogger logger)
        {
            _mediator = mediator;
            _logger = logger;
        }

        /// <summary>
        /// Gets all contact persons.
        /// </summary>
        /// <returns>An list of IDM specific contact persons</returns>
        /// <response code="200">Contact person will be returned.</response>
        /// <response code="401">Authentication failed.</response>
        /// <response code="403">Invalid URL scheme.</response>
        [HttpGet]
        [Route("")]
        [ResponseType(typeof(IEnumerable<IdmContactPerson>))]
        public async Task<IHttpActionResult> Get([FromUri] GetContactPersons.Query query)
        {
            _logger.LogDebug("Received request to get all contact persons with query: {@Query}.", query);

            var contactPersons = await _mediator.Send(query);
            return Ok(contactPersons);
        }

        /// <summary>
        /// Gets a contact person by id.
        /// </summary>
        /// <param name="id">A resourceId or GUID/UUID identifying a contact person.</param>
        /// <returns>An IDM specific contact person</returns>
        /// <response code="200">Contact person will be returned.</response>
        /// <response code="400">Invalid request format.</response>
        /// <response code="401">Authentication failed.</response>
        /// <response code="403">Invalid URL scheme.</response>
        /// <response code="404">Contact person with specified id is not found.</response>
        [HttpGet]
        [Route("{id}", Name = GetContactPersonUri)]
        [ResponseType(typeof(IdmContactPerson))]
        public async Task<IHttpActionResult> Get(string id)
        {
            var contactId = new IdmContactPersonId(id);
            if (!contactId.IsValid)
            {
                _logger.LogError("Bad request {Id}", null, id);
                return BadRequest("Request model is not valid.");
            }
            _logger.LogDebug("Received request to get contact person with id: {Id}", id);

            var contactPersonQuery = new GetContactPerson.Query { Id = contactId };
            var contactPerson = await _mediator.Send(contactPersonQuery);

            if (contactPerson is null)
            {
                _logger.LogInfo("Contact person not found when getting with id: {Id}", id);
                return NotFound();
            }

            return Ok(contactPerson);
        }

        /// <summary>
        /// Creates or updates an existing contact person.
        /// </summary>
        /// <param name="contactPerson">An IDM specific contact person to update/create.</param>
        /// <returns>Created result on created and No content result when updated.</returns>
        /// <response code="201">Contact person created.</response>
        /// <response code="204">Contact person updated.</response>
        /// <response code="400">Invalid request format. Or id in url does not match contact person from body. Or one or more contact person with the same ResourceId already exists.</response>
        /// <response code="401">Authentication failed.</response>
        /// <response code="403">Invalid URL scheme.</response>
        [HttpPut]
        [Route("{id}")]
        public async Task<IHttpActionResult> Put(string id, [FromBody] IdmContactPerson contactPerson)
        {
            _logger.LogInfo("Received change request for contact person with id {Id} and object: {@Object}", id, contactPerson);
            var contactId = new IdmContactPersonId(id);

            if (!ModelState.IsValid || !contactId.IsValid)
            {
                _logger.LogError("Bad request {@ModelState}", null, ModelState.Values);
                return BadRequest("Request model is not valid.");
            }

            if (contactId.HasResourceId && string.IsNullOrEmpty(contactPerson.ResourceId))
                contactPerson.ResourceId = contactId.ResourceId.ToString();

            if (!contactId.HasResourceId && contactPerson.Id is null)
                contactPerson.Id = contactId.Guid;

            var idFromRouteDoesNotMatchContactPersonId = contactId.HasResourceId
                ? contactPerson.ResourceId != contactId.ResourceId.ToString()
                : contactPerson.Id != contactId.Guid;

            if (idFromRouteDoesNotMatchContactPersonId)
            {
                _logger.LogError("Bad request id {Id} does not match contact person from body", null, id);
                return BadRequest("Id does not match contact person from body.");
            }

            var resourceIdInUse = false;
            if (!contactId.HasResourceId && !string.IsNullOrEmpty(contactPerson.ResourceId))
            {
                var contactPersonsResourceIdQuery = new GetContactPersons.Query();
                contactPersonsResourceIdQuery.SearchTerms.Add(new IdmContactPersonId(contactPerson.ResourceId).ResourceId.Value.ToString());
                var result = await _mediator.Send(contactPersonsResourceIdQuery);
                var existingPersonsWithSameResourceId = result.Result?.ToList() ?? new List<IdmContactPerson>();
                resourceIdInUse = existingPersonsWithSameResourceId.Any(x => x.Id != contactId.Guid)
                    || existingPersonsWithSameResourceId.Count > 1;
            }

            if (resourceIdInUse)
            {
                _logger.LogError("ResourceId already in use: {Id}", null, contactPerson.ResourceId);
                return BadRequest($"Failed to update contact person with id {contactId.Guid}. One or more contact person with the same ResourceId {contactPerson.ResourceId}, already exists.");
            }

            var contactPersonQuery = new GetContactPerson.Query { Id = contactId, FilterPublished = false };
            var existing = await _mediator.Send(contactPersonQuery);

            if (existing is null)
            {
                _logger.LogInfo("Contact person does not exist, creating new page with id: {Id}", id);

                var command = new CreateContactPerson.Command { ContactPerson = contactPerson };
                await _mediator.Send(command);

                if (!contactId.HasResourceId)
                {
                    var routeUrlGuid = Url.Route(GetContactPersonUri, new { id = command.ContactPerson.Id.ToString() });
                    return Created(routeUrlGuid, command.ContactPerson.Id);
                }
                var routeUrl = Url.Route(GetContactPersonUri, new { id = command.ContactPerson.ResourceId });
                return Created(routeUrl, command.ContactPerson.ResourceId);
            }
            else
            {
                _logger.LogInfo("Updating existing contact person with id: {Id}", id);

                var command = new UpdateContactPerson.Command { ContactPerson = contactPerson };
                await _mediator.Send(command);

                return StatusCode(HttpStatusCode.NoContent);
            }
        }

        /// <summary>
        /// Deletes a contact person.
        /// </summary>
        /// <param name="id">A resourceId or GUID/UUID identifying the contact person to delete.</param>
        /// <returns>No content result when deleted. If the contact person does not exist, returns Not found.</returns>
        /// <response code="204">Contact person deleted.</response>
        /// <response code="400">Invalid request format.</response>
        /// <response code="401">Authentication failed.</response>
        /// <response code="403">Invalid URL scheme.</response>
        /// <response code="404">Contact person with specified id is not found.</response>
        [HttpDelete]
        [Route("{id}")]
        public async Task<IHttpActionResult> Delete(string id)
        {
            _logger.LogInfo("Received delete request for contact person with id: {Id}", id);
            var contactId = new IdmContactPersonId(id);
            if (!contactId.IsValid)
            {
                _logger.LogError("Bad request {Id}", null, id);
                return BadRequest("Request model is not valid.");
            }

            var contactPersonQuery = new GetContactPerson.Query { Id = contactId };
            var contactPerson = await _mediator.Send(contactPersonQuery);

            if (contactPerson is null)
            {
                _logger.LogWarning("Contact person not found when deleting with id: {Id}", id);
                return NotFound();
            }

            await _mediator.Send(new DeleteContactPerson.Command { Id = contactId });

            return StatusCode(HttpStatusCode.NoContent);
        }
    }
}