﻿using System;
using System.Globalization;
using System.Threading;
using System.Web.Routing;
using EPiServer.Core;
using EPiServer.Globalization;
using EPiServer.Web;
using EPiServer.Web.Routing;

namespace Forskningsradet.Web.Controllers.Api.Helpers
{
    public class ApiSiteContextHelper
    {
        private readonly ISiteDefinitionResolver _siteDefinitionResolver;
        private readonly RequestContext _requestContext;

        public ApiSiteContextHelper(ISiteDefinitionResolver siteDefinitionResolver, RequestContext requestContext)
        {
            _siteDefinitionResolver = siteDefinitionResolver;
            _requestContext = requestContext;
        }

        public ApiSiteContextHelper SetCurrentPageAndSite(ContentReference currentPageLink)
        {
            return SetCurrentSite(currentPageLink)
                .SetCurrentPage(currentPageLink);
        }

        public ApiSiteContextHelper SetCurrentPage(ContentReference currentPageLink)
        {
           _requestContext.SetContentLink(currentPageLink);
            return this;
        }

        public ApiSiteContextHelper SetCurrentSite(ContentReference currentPageLink)
        {
            SiteDefinition.Current = _siteDefinitionResolver.GetByContent(currentPageLink, true);
            return this;
        }

        public ApiSiteContextHelper SetCurrentLanguage(string language)
        {
            if (language is null) throw new ArgumentNullException(nameof(language));

            CultureInfo culture = new CultureInfo(language);
            Thread.CurrentThread.CurrentCulture = culture;
            Thread.CurrentThread.CurrentUICulture = culture;
            ContentLanguage.PreferredCulture = culture;

            return this;
        }
    }
}