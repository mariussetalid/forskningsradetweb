﻿using System;
using System.Web.Http;
using System.Web.Routing;
using EPiServer;
using EPiServer.Core;
using EPiServer.Globalization;
using EPiServer.Web.Routing;
using Forskningsradet.Core.Factories;
using Forskningsradet.Core.Models.ApiModels;
using Forskningsradet.Core.Models.ApiModels.Search;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.QueryParameters;
using Forskningsradet.Core.Models.SearchModels;
using Forskningsradet.Core.Services.Contracts;
using Forskningsradet.Web.Controllers.Api.Helpers;
using Forskningsradet.Web.Infrastructure;
using Microsoft.Web.Http;
using UrlHelper = System.Web.Mvc.UrlHelper;

namespace Forskningsradet.Web.Controllers.Api
{
    public class SearchApiUrlResolver : ISearchApiUrlResolver
    {
        private readonly UrlHelper _urlHelper;
        private readonly IPageRouteHelper _pageRouteHelper;

        public SearchApiUrlResolver(UrlHelper urlHelper, IPageRouteHelper pageRouteHelper)
        {
            _urlHelper = urlHelper;
            _pageRouteHelper = pageRouteHelper;
        }

        public string GetSearchUrl() =>
            _urlHelper.HttpRouteUrl(SearchApiController.RouteNames.Search, GetRouteValues());

        public string GetSearchProposalUrl() =>
            _urlHelper.HttpRouteUrl(SearchApiController.RouteNames.Proposal, GetRouteValues());

        public string GetSearchEventUrl() =>
            _urlHelper.HttpRouteUrl(SearchApiController.RouteNames.Event, GetRouteValues());

        public string GetSearchCategoryUrl() =>
            _urlHelper.HttpRouteUrl(SearchApiController.RouteNames.Category, GetRouteValues());

        public string GetSearchPublicationUrl() =>
            _urlHelper.HttpRouteUrl(SearchApiController.RouteNames.Publication, GetRouteValues());

        public string GetSearchHitTrackUrl(SearchHitTrackRequest trackRequest) =>
            _urlHelper.HttpRouteUrl(SearchApiController.RouteNames.Track, GetRouteValues(trackRequest));

        private RouteValueDictionary GetRouteValues(object customValues = null) =>
            new RouteValueDictionary(customValues)
            {
                {"lang", ContentLanguage.PreferredCulture.Name},
                {"pageId", _pageRouteHelper.PageLink?.ID},
                {"version", "1"}
            };
    }

    [RequireHttps]
    [ApiExceptionFilter]
    [ReactJsonFormatter]
    [ApiVersion("1.0")]
    [RoutePrefix("api/v{version:apiVersion}/search")]
    public class SearchApiController : ApiController
    {
        public static class RouteNames
        {
            private const string BaseName = "Forskningsradet_SearchApi.";

            public const string Search = BaseName + "Search";
            public const string Proposal = BaseName + "Proposal";
            public const string Event = BaseName + "Event";
            public const string Category = BaseName + "Category";
            public const string Track = BaseName + "Track";
            public const string Publication = BaseName + "Publication";
        }

        private readonly IContentLoader _contentLoader;
        private readonly IViewModelFactory _viewModelFactory;
        private readonly ApiSiteContextHelper _apiSiteContextHelper;
        private readonly ISearchTrackingService _searchTrackingService;

        public SearchApiController(IContentLoader contentLoader, IViewModelFactory viewModelFactory, ApiSiteContextHelper apiSiteContextHelper, ISearchTrackingService searchTrackingService)
        {
            _contentLoader = contentLoader;
            _viewModelFactory = viewModelFactory;
            _apiSiteContextHelper = apiSiteContextHelper;
            _searchTrackingService = searchTrackingService;
        }

        [HttpPost]
        [Route("{lang}/{pageId}", Name = RouteNames.Search)]
        public IHttpActionResult Search(string lang, int pageId, ApiSearchQueryParameter request) =>
            DoSearch<SearchPage>(lang, pageId, request.ToSearchQueryParameter());

        [HttpPost]
        [Route("proposal/{lang}/{pageId}", Name = RouteNames.Proposal)]
        public IHttpActionResult ProposalSearch(string lang, int pageId, ProposalListQueryParameter request) =>
            DoSearch<ProposalListPage>(lang, pageId, request);

        [HttpPost]
        [Route("event/{lang}/{pageId}", Name = RouteNames.Event)]
        public IHttpActionResult EventSearch(string lang, int pageId, EventListQueryParameter request) =>
            DoSearch<EventListPage>(lang, pageId, request);

        [HttpPost]
        [Route("category/{lang}/{pageId}", Name = RouteNames.Category)]
        public IHttpActionResult CategorySearch(string lang, int pageId, CategoryListQueryParameter request) =>
            DoSearch<CategoryListPage>(lang, pageId, request);

        [HttpPost]
        [Route("publication/{lang}/{pageId}", Name = RouteNames.Publication)]
        public IHttpActionResult PublicationSearch(string lang, int pageId, ApiPublicationListQueryParameter request) =>
            DoSearch<PublicationListPage>(lang, pageId, request.ToPublicationListQueryParameter());

        [HttpPost]
        [Route("track", Name = RouteNames.Track)]
        public IHttpActionResult TrackSearchHit([FromUri] SearchHitTrackRequest trackRequest)
        {
            _searchTrackingService.TrackHit(trackRequest);
            return Ok(new ApiOkResult());
        }

        private IHttpActionResult DoSearch<TSearchPage>(string lang, int pageId, QueryParameterBase searchParameter) where TSearchPage : PageData
        {
            if (!_contentLoader.TryGet(new ContentReference(pageId), out TSearchPage page))
                return BadRequest();

            _apiSiteContextHelper
                .SetCurrentPageAndSite(page.PageLink)
                .SetCurrentLanguage(lang);

            var model = _viewModelFactory.GetPageViewModel(page, searchParameter);

            return Ok(model.Model);
        }
    }
}