﻿using System.Web.Mvc;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Factories;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.QueryParameters;
using Forskningsradet.Core.Models.SearchModels;

namespace Forskningsradet.Web.Controllers
{
    public class HistoricProposalListPageController : BasePageController<HistoricProposalListPage>
    {
        private readonly IViewModelFactory _viewModelFactory;

        public HistoricProposalListPageController(IViewModelFactory viewModelFactory)
            => _viewModelFactory = viewModelFactory;

        public ViewResult Index(HistoricProposalListPage currentPage, string q, int? year)
        {
            var queryParameter = (QueryParameterBase) new PublicationListQueryParameter
            {
                Query = q,
                Year = year ?? 0,
                Types = new []{(int)PublicationType.Proposal}
            };
            var model = _viewModelFactory.GetPageViewModel(currentPage, queryParameter);
            return View(RazorViews.DefaultReactViewPath, model);
        }
    }
}