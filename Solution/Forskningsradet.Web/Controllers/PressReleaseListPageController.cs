using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using EPiServer.Web.Routing;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Extensions;
using Forskningsradet.Core.Factories;
using Forskningsradet.Core.Models.ApiModels.Ntb;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.QueryParameters;
using Forskningsradet.Core.Models.ViewModels.Contracts;
using Forskningsradet.Core.Models.ViewModels.Pages;
using Forskningsradet.Core.Processes.Providers;
using Forskningsradet.Core.Repositories.Contracts;
using Forskningsradet.Core.Services.Contracts;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.Web.Controllers
{
    public class PressReleaseListPageController : BasePageController<PressReleaseListPage>
    {
        private readonly IViewModelFactory _viewModelFactory;
        private readonly IPressReleasePageProvider _pressReleasePageProvider;
        private readonly IUrlResolver _urlResolver;

        public PressReleaseListPageController(
            IViewModelFactory viewModelFactory,
            IPressReleasePageProvider pressReleasePageProvider,
            IUrlResolver urlResolver)
        {
            _viewModelFactory = viewModelFactory;
            _pressReleasePageProvider = pressReleasePageProvider;
            _urlResolver = urlResolver;
        }

        public ActionResult Index(PressReleaseListPage currentPage, int? page, long? id)
        {
            var parameters = new PressReleaseQueryParameter
            {
                Page = page ?? 1
            };

            if (id.HasValue && id.Value > 0)
            {
                var pressReleaseUrl = GetPressReleasePageUrl(id.Value);
                if (string.IsNullOrEmpty(pressReleaseUrl))
                    return new HttpNotFoundResult();

                return Redirect(pressReleaseUrl);
            }

            var model = _viewModelFactory.GetPageViewModel(currentPage, parameters);

            return View(RazorViews.DefaultReactViewPath, model);
        }

        private string GetPressReleasePageUrl(long id) =>
            _pressReleasePageProvider.GetPageByExternalId(id) is PressReleasePage page
            ? _urlResolver.GetUrl(page)
            : null;
    }
}