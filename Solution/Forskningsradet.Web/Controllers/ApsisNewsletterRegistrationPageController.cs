﻿using System.Web.Mvc;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Configuration;
using Forskningsradet.Core.Factories;
using Forskningsradet.Core.Models.ContentModels.Pages;

namespace Forskningsradet.Web.Controllers
{
    public class ApsisNewsletterRegistrationPageController : BasePageController<ApsisNewsletterRegistrationPage>
    {
        private readonly IApsisConfiguration _configuration;
        private readonly IViewModelFactory _viewModelFactory;

        public ApsisNewsletterRegistrationPageController(IApsisConfiguration configuration, IViewModelFactory viewModelFactory)
        {
            _configuration = configuration;
            _viewModelFactory = viewModelFactory;
        }

        public ActionResult Index(ApsisNewsletterRegistrationPage currentPage)
        {
            var model = _viewModelFactory.GetPageViewModel(currentPage);
            return View(RazorViews.DefaultReactViewPath, model);
        }

        public ActionResult Form(ApsisNewsletterRegistrationPage currentPage)
        {
            var formId = string.IsNullOrEmpty(currentPage.FormId)
                ? _configuration.ApsisFormId
                : currentPage.FormId;
            var divId = string.IsNullOrEmpty(currentPage.DivId)
                ? _configuration.ApsisDivId
                : currentPage.DivId;
            return Content($"<script src=\"https://forms.apsisforms.com/formbuilder.js\"></script><script>(function() {{var form = new ApsisForms.FormbuilderInstance();form.init({{ formId: \"{ formId }\"}});}})();</script><div class={divId} ></div> ", "text/html");
        }
    }
}
