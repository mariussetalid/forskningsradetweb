﻿using System.Web.Mvc;
using EPiServer.Web.Mvc;
using EPiServer.Web.Routing;
using Forskningsradet.Core.Models.ContentModels.Media;
using Forskningsradet.Core.Models.ViewModels.Media;

namespace Forskningsradet.Web.Controllers
{
    /// <summary>
    /// Controller for an image file. Returns a view with an img tag.
    /// </summary>
    public class ImageFileController : PartialContentController<ImageFile>
    {
        private readonly UrlResolver _urlResolver;

        public ImageFileController(UrlResolver urlResolver)
        {
            _urlResolver = urlResolver;
        }

        public override ActionResult Index(ImageFile currentContent)
        {
            var model = new ImageFileViewModel
            {
                Url = _urlResolver.GetUrl(currentContent.ContentLink),
                AltText = currentContent.AltText
            };

            return PartialView("/Views/Media/ImageFile/Index.cshtml", model);
        }
    }
}