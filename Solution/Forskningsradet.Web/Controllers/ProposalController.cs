using System.Web.Mvc;
using EPiServer.Web.Routing;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Repositories.Contracts;

namespace Forskningsradet.Web.Controllers
{
    public class ProposalController : Controller
    {
        private readonly IPageRepository _pageRepository;
        private readonly IUrlResolver _urlResolver;

        public ProposalController(IPageRepository pageRepository, IUrlResolver urlResolver)
        {
            _pageRepository = pageRepository;
            _urlResolver = urlResolver;
        }
        
        public ActionResult Index(long id, string language)
        {
            if(!((_pageRepository.GetCurrentStartPage() ?? _pageRepository.GetStartPageFromWildcardHost()) is FrontPage frontPage))
                return new HttpNotFoundResult();

            var languageBranch = !string.IsNullOrEmpty(language)
                ? language
                : LanguageConstants.MasterLanguageBranch;
            
            var page = _pageRepository.GetPageByExternalId<ProposalPage>(id.ToString(), frontPage.PageLink, languageBranch);
            
            if(page is null)
                return new HttpNotFoundResult();
            
            return Redirect(_urlResolver.GetUrl(page));
        }
    }
}