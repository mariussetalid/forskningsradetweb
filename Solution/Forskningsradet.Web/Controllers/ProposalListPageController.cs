using System.Linq;
using System.Web.Mvc;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Factories;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.QueryParameters;
using Forskningsradet.Core.Models.SearchModels;

namespace Forskningsradet.Web.Controllers
{
    public class ProposalListPageController : BasePageController<ProposalListPage>
    {
        private readonly IViewModelFactory _viewModelFactory;

        public ProposalListPageController(IViewModelFactory viewModelFactory)
            => _viewModelFactory = viewModelFactory;

        public ActionResult Index(ProposalListPage currentPage, TimeFrameFilter? timeframe,
            string[] subjects,
            string[] targetGroups,
            string[] deadlines,
            int[] applicationTypes,
            string[] deadlineTypes)
        {
            var model = _viewModelFactory.GetPageViewModel(currentPage,
                new ProposalListQueryParameter
                {
                    TimeFrame = timeframe ?? TimeFrameFilter.Future,
                    Subjects = subjects,
                    TargetGroups = targetGroups,
                    ApplicationTypes = applicationTypes,
                    DeadlineTypes = deadlineTypes?.Union(deadlines ?? new string[0]).ToArray() ?? deadlines
                });
            return View(RazorViews.DefaultReactViewPath, model);
        }
    }
}