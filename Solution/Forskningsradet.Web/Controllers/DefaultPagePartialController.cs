﻿using System.Web.Mvc;
using EPiServer.Framework.DataAnnotations;
using EPiServer.Framework.Web;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Factories;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.ViewModels.Pages;

namespace Forskningsradet.Web.Controllers
{
    /// <summary>
    /// This controller is only used for Episerver's partial rendering in OnPageEdit mode
    /// </summary>
    [TemplateDescriptor(Inherited = true, TemplateTypeCategory = TemplateTypeCategories.MvcPartialController)]
    public class DefaultPagePartialController : BasePageController<BasePageData>
    {
        private readonly IViewModelFactory _viewModelFactory;

        public DefaultPagePartialController(IViewModelFactory viewModelFactory) 
            => _viewModelFactory = viewModelFactory;

        public PartialViewResult Index(BasePageData currentPage)
        {
            var model = _viewModelFactory.GetPartialViewModel(currentPage);

            if (model != null)
                return PartialView(RazorViews.DefaultReactViewPath, model);
            
            var viewName = GetViewNameForPageData(currentPage);
            if (!ViewExists(ControllerContext, viewName))
                viewName = RazorViews.DefaultViewPath;

            return PartialView(viewName, new BasePageViewModel(currentPage));
        }
    }
}