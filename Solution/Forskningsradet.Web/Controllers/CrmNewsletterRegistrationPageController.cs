﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.Web.Routing;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core;
using Forskningsradet.Core.Configuration;
using Forskningsradet.Core.Factories;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.QueryParameters;
using Forskningsradet.Core.Models.Validation;
using Forskningsradet.Core.Services;
using Forskningsradet.ServiceAgents.Contracts;

namespace Forskningsradet.Web.Controllers
{
    public class CrmNewsletterRegistrationPageController : BasePageController<CrmNewsletterRegistrationPage>
    {
        private readonly ICrmConfiguration _configuration;
        private readonly IViewModelFactory _viewModelFactory;
        private readonly IDynamicsCrmServiceAgent _serviceAgent;
        private readonly IUrlResolver _urlResolver;

        public CrmNewsletterRegistrationPageController(
            ICrmConfiguration configuration,
            IViewModelFactory viewModelFactory,
            IDynamicsCrmServiceAgent dynamicsCrmServiceAgent,
            IUrlResolver urlResolver)
        {
            _configuration = configuration;
            _viewModelFactory = viewModelFactory;
            _serviceAgent = dynamicsCrmServiceAgent;
            _urlResolver = urlResolver;
        }

        [HttpGet]
        public ActionResult Index(CrmNewsletterRegistrationPage currentPage)
        {
            var model = _viewModelFactory.GetPageViewModel(currentPage, GetParameter());
            return View(RazorViews.DefaultReactViewPath, model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(CrmNewsletterRegistrationPage currentPage, string email, int[] subjects, int[] targetGroups)
        {
            if (ModelState.IsValid)
            {
                var subscriptionSuccess = _serviceAgent.Subscribe(email, subjects, targetGroups, _configuration.DynamicsAppId, _configuration.DynamicsSecret, _configuration.DynamicsUrl);
                if (subscriptionSuccess && !ContentReference.IsNullOrEmpty(currentPage.ConfirmationPage))
                {
                    var redirectUrl = _urlResolver.GetUrl(currentPage.ConfirmationPage);
                    return Redirect(redirectUrl);
                }
            }
            var model = _viewModelFactory.GetPageViewModel(currentPage, GetParameter());
            return View(RazorViews.DefaultReactViewPath, model);
        }

        private CrmNewsletterQueryParameter GetParameter() =>
            new CrmNewsletterQueryParameter
            {
                ValidSubjects = _serviceAgent.GetTopics(),
                ValidTargetGroups = _serviceAgent.GetGroups()
            };
    }
}
