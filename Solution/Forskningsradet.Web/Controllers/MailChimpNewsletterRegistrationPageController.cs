﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using EPiServer.Core;
using EPiServer.Web.Routing;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core;
using Forskningsradet.Core.Factories;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.QueryParameters;
using Forskningsradet.Core.Models.Validation;
using Forskningsradet.Core.Models.ViewModels.Forms;
using Forskningsradet.ServiceAgents.Contracts;

namespace Forskningsradet.Web.Controllers
{
    public class MailChimpNewsletterRegistrationPageController : BasePageController<MailChimpNewsletterRegistrationPage>
    {
        private readonly IViewModelFactory _viewModelFactory;
        private readonly IMailChimpServiceAgent _serviceAgent;
        private readonly IUrlResolver _urlResolver;

        public MailChimpNewsletterRegistrationPageController(
            IViewModelFactory viewModelFactory,
            IMailChimpServiceAgent serviceAgent,
            IUrlResolver urlResolver)
        {
            _viewModelFactory = viewModelFactory;
            _serviceAgent = serviceAgent;
            _urlResolver = urlResolver;
        }

        [HttpGet]
        public ActionResult Index(MailChimpNewsletterRegistrationPage currentPage)
        {
            var model = _viewModelFactory.GetPageViewModel(currentPage, null);
            return View(RazorViews.DefaultReactViewPath, model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(MailChimpNewsletterRegistrationPage currentPage, NewsletterRegistrationFormModel form)
        {
            if (ModelState.IsValid)
            {
                var subscriptions = form.Subscriptions?.ToArray() ?? new string[] { };
                var subscriptionSuccess = AsyncHelpers.RunSync(
                    () => _serviceAgent.Subscribe(form.Email, subscriptions, currentPage.MailChimpApiKey, currentPage.MailChimpListId)
                    );
                if (subscriptionSuccess && !ContentReference.IsNullOrEmpty(currentPage.ConfirmationPage))
                {
                    var redirectUrl = _urlResolver.GetUrl(currentPage.ConfirmationPage);
                    return Redirect(redirectUrl);
                }
            }
            var model = _viewModelFactory.GetPageViewModel(currentPage, BuildParameters(ModelState));
            return View(RazorViews.DefaultReactViewPath, model);
        }

        private static NewsletterRegistrationQueryParameter BuildParameters(ModelStateDictionary modelState) =>
            new NewsletterRegistrationQueryParameter
            {
                ModelValidation = GetModelValidation(modelState),
            };

        private static List<NewsletterModelValidation> GetModelValidation(ModelStateDictionary modelState) =>
            modelState.Select(BuildFormFromModelState).ToList();

        private static NewsletterModelValidation BuildFormFromModelState(KeyValuePair<string, ModelState> keyValuePair) =>
            keyValuePair.Value is null
                ? null
                : new NewsletterModelValidation
                {
                    FieldName = keyValuePair.Key,
                    AttemptedValue = keyValuePair.Value?.Value?.AttemptedValue,
                    Errors = BuildModelStateErrors(keyValuePair.Value.Errors)
                };

        private static IEnumerable<string> BuildModelStateErrors(ModelErrorCollection valueErrors) =>
            valueErrors.Select(x => x.ErrorMessage);
    }
}
