﻿using System.Web.Mvc;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Factories;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.QueryParameters;
using Forskningsradet.Core.Models.SearchModels;

namespace Forskningsradet.Web.Controllers
{
    public class EventListPageController : BasePageController<EventListPage>
    {
        private readonly IViewModelFactory _viewModelFactory;

        public EventListPageController(IViewModelFactory viewModelFactory) 
            => _viewModelFactory = viewModelFactory;

        public ViewResult Index(EventListPage currentPage, TimeFrameFilter? timeframe, int[] subjects, int[] targetgroups, int[] categories, string[] location, string[] type, bool video = false)
        {
            var query = (QueryParameterBase) new EventListQueryParameter
            {
                TimeFrame = timeframe ?? TimeFrameFilter.Future,
                Location = location,
                Subjects = subjects,
                TargetGroups = targetgroups,
                Categories = categories,
                Type = type,
                VideoOnly = video
            };
            var model = _viewModelFactory.GetPageViewModel(currentPage, query);
            return View(RazorViews.DefaultReactViewPath, model);
        }
    }
}