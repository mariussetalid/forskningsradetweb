﻿using System.Web.Mvc;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Extensions;
using Forskningsradet.Core.Factories;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.ViewModels.Pages;
using Forskningsradet.Core.Processes.Providers;

namespace Forskningsradet.Web.Controllers
{
    public class DefaultPageController : BasePageController<BasePageData>
    {
        private readonly IViewModelFactory _viewModelFactory;
        private readonly IHtmlToPdfProvider _htmlToPdfProvider;

        public DefaultPageController(IViewModelFactory viewModelFactory, IHtmlToPdfProvider htmlToPdfProvider)
        {
            _viewModelFactory = viewModelFactory;
            _htmlToPdfProvider = htmlToPdfProvider;
        }

        public ViewResult Index(BasePageData currentPage)
        {
            var model = _viewModelFactory.GetPageViewModel(currentPage);

            if (model != null)
                return View(RazorViews.DefaultReactViewPath, model);
            
            var viewName = GetViewNameForPageData(currentPage);
            if (!ViewExists(ControllerContext, viewName))
                viewName = RazorViews.DefaultViewPath;

            return View(viewName, new BasePageViewModel(currentPage));
        }

        public ActionResult Download(BasePageData currentPage)
        {
            var file = _htmlToPdfProvider.GetPageAsDownloadablePdf(currentPage);
            return File(file, "application/pdf", $"{currentPage.PageName.EnsureValidEnglishWindowsFileName()}.pdf");
        }
    }
}