﻿using System.Web.Mvc;
using EPiServer;
using EPiServer.Core;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Facades.Contracts;
using Forskningsradet.Core.Factories;
using Forskningsradet.Core.Models.ContentModels.Contracts;
using Forskningsradet.Core.Models.ContentModels.Pages;

namespace Forskningsradet.Web.Controllers
{
    public class PublicationPageController : BasePageController<PublicationBasePage>
    {
        private readonly IContentLoader _contentLoader;
        private readonly IViewModelFactory _viewModelFactory;
        private readonly IPageEditingAdapter _pageEditingAdapter;

        public PublicationPageController(
            IContentLoader contentLoader,
            IViewModelFactory viewModelFactory,
            IPageEditingAdapter pageEditingAdapter)
        {
            _contentLoader = contentLoader;
            _viewModelFactory = viewModelFactory;
            _pageEditingAdapter = pageEditingAdapter;
        }

        public ActionResult Index(PublicationBasePage currentPage)
        {
            if (_pageEditingAdapter.PageIsInEditMode() || 
                currentPage is HistoricProposalPage historicProposalPage && !ContentReference.IsNullOrEmpty(historicProposalPage.ApplicationResultPage))
                return PageView(currentPage);
            return DocumentResult(currentPage);
        }

        public ActionResult Download(PublicationBasePage currentPage) =>
            DocumentResult(currentPage);

        private ViewResult PageView(PublicationBasePage page)
        {
            var model = _viewModelFactory.GetPageViewModel(page);

            return View(RazorViews.DefaultReactViewPath, model);
        }

        private ActionResult DocumentResult(PublicationBasePage currentPage)
        {
            var attachment = GetAttachment(currentPage);
            if (attachment is null)
                return new EmptyResult();

            if (attachment is IContentMediaMetaData mediaData)
                return File(attachment.BinaryData.OpenRead(), attachment.MimeType, $"{currentPage.PageName}{mediaData.FileExtension}");

            return File(attachment.BinaryData.OpenRead(), attachment.MimeType, currentPage.PageName);
        }

        private MediaData GetAttachment(PublicationBasePage currentPage) =>
            currentPage.Attachment != null &&
            _contentLoader.TryGet<MediaData>(currentPage.Attachment, out var content)
                ? content
                : null;
    }
}