﻿using System.Web.Mvc;
using EPiServer;
using EPiServer.Web.Mvc;
using Forskningsradet.Core.Models.ContentModels.Pages;

namespace Forskningsradet.Web.Controllers
{
    public class BasePageController<T> : PageController<T> where T : BasePageData
    {
        public static string GetViewNameForPageData(BasePageData pageData)
        {
            return $"~/Views/Pages/{pageData.GetOriginalType().Name}.cshtml";
        }

        public static bool ViewExists(ControllerContext controllerContext, string viewName)
        {
            return ViewEngines.Engines.FindView(controllerContext, viewName, null).View != null;
        }
    }
}