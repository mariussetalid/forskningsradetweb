﻿using System.Web.Mvc;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Factories;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Services.Contracts;

namespace Forskningsradet.Web.Controllers
{
    public class EventPageController : BasePageController<EventPage>
    {
        private readonly IViewModelFactory _viewModelFactory;
        private readonly ICalendarService _calendarService;

        public EventPageController(IViewModelFactory viewModelFactory, ICalendarService calendarService)
        {
            _viewModelFactory = viewModelFactory;
            _calendarService = calendarService;
        }

        public ActionResult Index(EventPage currentPage, bool downloadCalendar = false)
        {
            if (downloadCalendar && !currentPage.HideCalendarLink)
                return DownloadCalendar(currentPage);

            var model = _viewModelFactory.GetPageViewModel(currentPage);
            return View(RazorViews.DefaultReactViewPath, model);
        }

        private ActionResult DownloadCalendar(EventPage currentPage)
        {
            if (_calendarService.GetCalendarStringForEvent(currentPage) is string calendarString)
            {
                var calendarBytes = System.Text.Encoding.UTF8.GetBytes(calendarString);
                return File(calendarBytes, "text/calendar", "event.ics");
            }
            return new HttpNotFoundResult();
        }
    }
}