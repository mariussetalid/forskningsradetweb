﻿using EPiServer.Framework;
using EPiServer.Framework.Initialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Forskningsradet.Web.AdminTools.FileReport
{
    [InitializableModule]
    public class FileReportRouteInitialization : IInitializableModule
    {
        public void Initialize(InitializationEngine context)
        {
            RouteTable.Routes.MapRoute(
                "FileReport",
                "AdminTools/FileReport/{action}",
                new { controller = "FileReport", action = "Index" });
        }

        public void Uninitialize(InitializationEngine context)
        {
        }
    }
}