﻿using EPiServer.Find;
using EPiServer.PlugIn;
using Forskningsradet.Core.Models.ContentModels.Media;
using Forskningsradet.Core.Models.RequestModels;
using Forskningsradet.Core.Models.ViewModels.Forms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Forskningsradet.Core.Extensions;
using EPiServer.Core;
using Forskningsradet.Common.Constants;
using EPiServer;
using Forskningsradet.Core.Models.ContentModels.Pages;
using EPiServer.Find.Cms;
using EPiServer.Web.Routing;
using System.Diagnostics;

namespace Forskningsradet.Web.AdminTools.FileReport
{
    [GuiPlugIn(
        Area = PlugInArea.ReportMenu,
        Url = "/AdminTools/FileReport",
        Category = "Custom Reports",
        DisplayName = "Filrapport")]
    [Authorize(Roles = "CmsAdmins")]
    public class FileReportController : Controller
    {
        private const string ViewPath = "~/AdminTools/FileReport/Index.cshtml";
        private const int Interval = 500;
        private readonly IClient _searchClient;
        private readonly IContentRepository _contentRepository;
        private readonly IUrlResolver _urlResolver;

        public FileReportController(IClient searchClient, IContentRepository contentRepository, IUrlResolver urlResolver)
        {
            _searchClient = searchClient;
            _contentRepository = contentRepository;
            _urlResolver = urlResolver;
        }

        public ActionResult Index()
        {
            var model = new FileReportViewModel { Index = 0, Interval = Interval };
            (model.Files, model.TotalMatching, model.Time) = GetFiles(0);

            return View(ViewPath, model);
        }

        public ActionResult Move(int index)
        {
            var model = new FileReportViewModel { Index = index, Interval = Interval };
            (model.Files, model.TotalMatching, model.Time) = GetFiles(index);

            return View(ViewPath, model);
        }

        private (IList<FileReportFileModel>, int, long) GetFiles(int index)
        {
            var sw = new Stopwatch();
            sw.Start();
            var files = GetContent(index, Interval);
            var items = files.Items.Select(MapMedia).ToList();
            sw.Stop();

            return (items, files.TotalMatching, sw.ElapsedMilliseconds);
        }

        private IContentResult<GenericMedia> GetContent(int skip, int take) =>
            _searchClient.Search<GenericMedia>()
            .Skip(skip)
            .Take(take)
            .GetContentResultSafe();

        private FileReportFileModel MapMedia(GenericMedia x) =>
            new FileReportFileModel
            {
                Id = x.ContentLink.ID.ToString(),
                FileName = x.Name,
                Link = _urlResolver.GetUrl(x.ContentLink),
                Info = x.Created.ToString(DateTimeFormats.NorwegianDate),
                ReferenceCount = _contentRepository.GetReferencesToContent(x.ContentLink, true).Count(),
                Folders = GetFolders(x).Reverse(),
                ParentId = x.ParentLink.ID.ToString()
            };

        private IEnumerable<string> GetFolders(GenericMedia media)
        {
            var ancestors = media.Ancestors().Select(x => new ContentReference(int.Parse(x)));
            foreach (var ancestor in ancestors)
            {
                if (_contentRepository.TryGet<IContent>(ancestor, out var content))
                    yield return content.Name;
            }
        }
    }
}