﻿using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.RequestModels;
using Forskningsradet.Core.Models.ViewModels.Forms;
using System.Collections.Generic;
using System.Linq;

namespace Forskningsradet.Web.AdminTools.RevisionReport
{
    public class RevisionReportViewModelBuilder
    {
        public RevisionReportRequestModel Build(IEnumerable<EditorialPage> pages) =>
            new RevisionReportRequestModel
            {
                Pages = pages.Select(p => GetViewModel(p))
            };

        private RevisionReportViewModel GetViewModel(EditorialPage page) =>
            new RevisionReportViewModel
            {
                PageGuid = page.ContentGuid,
                PageName = page.PageName,
                PublishedDate = page.StartPublish,
                LastEdit = page.Saved,
                RevisionDate = page.RevisionDate,
                ChangedBy = page.ChangedBy,
                Language = page.Language,
                PageType = page.PageTypeName,
                EditUrl = $"/EPiServer/Cms/?language={page.Language}#context=epi.cms.contentdata:///{page.ContentLink.ID}&viewsetting=viewlanguage:///{page.Language}"
            };
    }
}