﻿using EPiServer.Framework;
using EPiServer.Framework.Initialization;
using System.Web.Mvc;
using System.Web.Routing;

namespace Forskningsradet.Web.AdminTools.RevisionReport
{
    [InitializableModule]
    public class RevisionReportRouteInitialization : IInitializableModule
    {
        public void Initialize(InitializationEngine context)
        {
            RouteTable.Routes.MapRoute(
                "RevisionReport",
                "AdminTools/RevisionReport/{action}",
                new { controller = "RevisionReport", action = "Index" });
        }

        public void Uninitialize(InitializationEngine context)
        {
        }
    }
}