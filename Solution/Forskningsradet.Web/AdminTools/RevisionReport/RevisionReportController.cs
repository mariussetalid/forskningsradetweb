﻿using EPiServer.PlugIn;
using Forskningsradet.Core.Models.QueryParameters;
using Forskningsradet.Core.Models.RequestModels;
using Forskningsradet.Core.Models.ViewModels.Forms;
using Forskningsradet.Core.Services;
using Forskningsradet.Core.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Forskningsradet.Web.AdminTools.RevisionReport
{
    [GuiPlugIn(
        Area = PlugInArea.ReportMenu,
        Url = "/AdminTools/RevisionReport",
        Category = "Custom Reports",
        DisplayName = "Revisjonsrapport")]
    [Authorize(Roles = "CmsAdmins")]
    public class RevisionReportController : Controller
    {
        private const string ViewPath = "~/AdminTools/RevisionReport/Index.cshtml";
        private readonly IRevisionService _revisionReportService;
        private readonly RevisionReportViewModelBuilder _revisionReportViewModelBuilder;

        public RevisionReportController(RevisionReportViewModelBuilder revisionReportViewModelBuilder, IRevisionService revisionReportService)
        {
            _revisionReportViewModelBuilder = revisionReportViewModelBuilder;
            _revisionReportService = revisionReportService;
        }

        public ActionResult Index()
        {
            var model = new RevisionReportRequestModel();
            model.DateStart = null;
            model.DateEnd = DateTime.UtcNow;
            model.Pages = GetPages(model);
            return View(ViewPath, model);
        }

        public ActionResult SubmitForm(RevisionReportRequestModel formModel)
        {
            formModel.Pages = GetPages(formModel);
            return View(ViewPath, formModel);
        }

        private IEnumerable<RevisionReportViewModel> GetPages(RevisionReportRequestModel formModel)
        {
            var pages = _revisionReportService.GetEditorialPages(formModel);
            return _revisionReportViewModelBuilder.Build(pages).Pages;
        }
    }
}