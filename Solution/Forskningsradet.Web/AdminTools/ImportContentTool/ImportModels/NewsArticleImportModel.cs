﻿using System;
using System.Collections.Generic;

namespace Forskningsradet.Web.AdminTools.ImportContentTool.ImportModels
{
    public class NewsArticleImportModel
    {
        public string _id { get; set; }
        public string _index { get; set; }
        public double _score { get; set; }
        public Content _source { get; set; }
        public string _type { get; set; }
        public List<string> satellitt { get; set; }
    }

    public class Content
    {
        public string Ingress { get; set; }
        public string Innhold { get; set; }
        public List<string> OvrigeKategorierArray { get; set; }
        public string SkrevetAvIntern { get; set; }
        public string SkrevetAv { get; set; }
        public List<string> TemaordArray { get; set; }
        public List<string> AnnetArray { get; set; }
        public List<string> InterneLinkerWCSArray { get; set; }
        public string Tittel { get; set; }
        public string Stikktittel { get; set; }
        public long assetid { get; set; }
        public string assetname { get; set; }
        public DateTimeOffset startdate { get; set; }
        public DateTimeOffset updateddate { get; set; }
    }
}
