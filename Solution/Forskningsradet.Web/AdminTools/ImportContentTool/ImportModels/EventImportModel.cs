﻿using System;
using System.Collections.Generic;

namespace Forskningsradet.Web.AdminTools.ImportContentTool.ImportModels
{
    public class EventImportModel
    {
        public string _id { get; set; }
        public string _index { get; set; }
        public double _score { get; set; }
        public EventImportContent _source { get; set; }
        public string _type { get; set; }
    }

    public class EventImportContent
    {
        public string Ingress { get; set; }
        public string Innhold { get; set; }
        public List<string> OvrigeKategorierArray { get; set; }
        public string SkrevetAvIntern { get; set; }
        public string SkrevetAv { get; set; }
        public List<string> TemaordArray { get; set; }
        public List<string> PresentasjonerArray { get; set; }
        public string Tittel { get; set; }
        public long assetid { get; set; }
        public string assetname { get; set; }
        public DateTime StartDato { get; set; }
        public DateTime SluttDato { get; set; }
        public DateTime updateddate { get; set; }
        public string Sted { get; set; }
    }
}