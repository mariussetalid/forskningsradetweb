﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Collections.Generic;
using System.Diagnostics;

namespace Forskningsradet.Web.AdminTools.ImportContentTool.ImportModels
{
    [DebuggerDisplay("Id = {Id}, Title = {Title}, Forfattere = {Forfattere?.Count ?? 0}")]
    public class SatelliteArticleImportModel
    {
        [JsonProperty("tm_serial")]
        public int Id { get; set; }
        public string Title { get; set; }
        public string Url { get; set; }
        public string Hovedtekst { get; set; }
        public List<ImportPerson> Forfattere { get; set; } = new List<ImportPerson>();
        [JsonProperty("id")]
        public string UrlId { get; set; }
        public string Ingress { get; set; }
        public ImportForsideBilde Ingressbilde { get; set; }
        public string Date { get; set; }
        [JsonProperty("readable_url")]
        public string ReadableUrl { get; set; }
        public string Faktaboks { get; set; }
        public string CreationDate { get; set; }
        public string Forsideingress { get; set; }
        public string EffectiveDate { get; set; }
        [JsonProperty("show_publish_date")]
        public string ShowPublishDate { get; set; }
        [JsonProperty("handler_om")]
        public List<string> HandlerOm { get; set; }
        public ImportForsideBilde Forsidebilde { get; set; }
        public ImportForsideBilde Campaignimage { get; set; }
        [JsonProperty("also_published_in")]
        public List<string> AlsoPublishedIn { get; set; }
        public string Syndikerbar { get; set; }
        public string Modified { get; set; }
        [JsonProperty("review_state")]
        [JsonConverter(typeof(StringEnumConverter))]
        public ReviewState ReviewState { get; set; }
    }

    public class ImportPerson
    {
        public string Tittel { get; set; }
        public string Navn { get; set; }
    }

    public class ImportForsideBilde
    {
        public string Url { get; set; }
        public string Fotograf { get; set; }
        [JsonProperty("readable_url")]
        public string ReadableUrl { get; set; }
        public string Original { get; set; }
        public string Alttext { get; set; }
    }

    public enum ReviewState
    {
        None = 0,
        Published = 1,
        Private = 2,
        Rejected = 3,
    }
}
