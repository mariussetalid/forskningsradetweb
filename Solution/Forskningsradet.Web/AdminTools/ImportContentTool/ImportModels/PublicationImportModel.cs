﻿using Newtonsoft.Json;

namespace Forskningsradet.Web.AdminTools.ImportContentTool.ImportModels
{
    public class PublicationImportModel
    {
        [JsonProperty("_id")]
        public string Id { get; set; }

        [JsonProperty("_index")]
        public string Index { get; set; }

        [JsonProperty("_score")]
        public long Score { get; set; }

        [JsonProperty("_source")]
        public PublicationSource Source { get; set; }
    }

    public class PublicationSource
    {
        [JsonProperty("Aar", NullValueHandling = NullValueHandling.Ignore)]
        public long? Aar { get; set; }

        [JsonProperty("Divisjon", NullValueHandling = NullValueHandling.Ignore)]
        public string Divisjon { get; set; }

        [JsonProperty("ISBN", NullValueHandling = NullValueHandling.Ignore)]
        public string Isbn { get; set; }

        [JsonProperty("Kontaktperson", NullValueHandling = NullValueHandling.Ignore)]
        public string Kontaktperson { get; set; }

        [JsonProperty("Program", NullValueHandling = NullValueHandling.Ignore)]
        public string Program { get; set; }

        [JsonProperty("Sidetall", NullValueHandling = NullValueHandling.Ignore)]
        public long? Sidetall { get; set; }

        [JsonProperty("Sted", NullValueHandling = NullValueHandling.Ignore)]
        public string Sted { get; set; }

        [JsonProperty("Tittel")]
        public string Tittel { get; set; }

        [JsonProperty("Type", NullValueHandling = NullValueHandling.Ignore)]
        public string Type { get; set; }

        [JsonProperty("Undertittel", NullValueHandling = NullValueHandling.Ignore)]
        public string Undertittel { get; set; }

        [JsonProperty("VedleggArray", NullValueHandling = NullValueHandling.Ignore)]
        public string[] VedleggArray { get; set; }

        [JsonProperty("Forfatter", NullValueHandling = NullValueHandling.Ignore)]
        public string Forfatter { get; set; }

        [JsonProperty("AnsvarligUtgiver", NullValueHandling = NullValueHandling.Ignore)]
        public string AnsvarligUtgiver { get; set; }

        [JsonProperty("NETTISBN", NullValueHandling = NullValueHandling.Ignore)]
        public string Nettisbn { get; set; }

        [JsonProperty("ISSN", NullValueHandling = NullValueHandling.Ignore)]
        public string Issn { get; set; }

        [JsonProperty("Spraak", NullValueHandling = NullValueHandling.Ignore)]
        public string Spraak { get; set; }

        [JsonProperty("StikkTittel", NullValueHandling = NullValueHandling.Ignore)]
        public string StikkTittel { get; set; }
    }
}
