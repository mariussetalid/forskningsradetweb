﻿using EPiServer.Core;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Web.AdminTools.ImportContentTool.ImportModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Forskningsradet.Web.AdminTools.ImportContentTool
{
    public interface IImportContentHelper
    {
        Dictionary<string, T> DeserializeContent<T>(string json);
        string DecodeString(string text);
        void PopulateArticlePage(ArticlePage page, SatelliteArticleImportModel importModel);
    }

    public class ImportContentHelper : IImportContentHelper
    {
        public Dictionary<string, T> DeserializeContent<T>(string json)
        {
            return JsonConvert.DeserializeObject<Dictionary<string, T>>(json);
        }

        public string DecodeString(string text) =>
            HttpUtility.UrlDecode(HttpUtility.HtmlDecode(text));

        public string DecodeStringTwice(string text) =>
            DecodeString(DecodeString(text));

        public void PopulateArticlePage(ArticlePage page, SatelliteArticleImportModel importModel)
        {
            if (importModel.Id <= 0)
                return;

            var title = DecodeString(importModel.Title);
            page.Name = string.IsNullOrWhiteSpace(title)
                ? importModel.Id.ToString()
                : title;
            var importAuthor = importModel.Forfattere.FirstOrDefault();
            page.Author = importAuthor?.Navn is string authorName
                ? DecodeString(authorName)
                : null;

            page.MigratedSatelliteData.Authors = importModel.Forfattere.Select(f => DecodeString(f.Navn) + " - " + DecodeString(f.Tittel)).ToList();
            page.MigratedSatelliteData.About = importModel.HandlerOm.Select(DecodeString).ToList();
            page.MigratedSatelliteData.AlsoPublishedIn = importModel.AlsoPublishedIn.Select(DecodeString).ToList();
            page.MigratedSatelliteData.Id = importModel.Id;
            page.MigratedSatelliteData.Title = DecodeString(importModel.Title);
            page.MigratedSatelliteData.Url = DecodeString(importModel.Url);
            page.MigratedSatelliteData.MainText = DecodeStringTwice(importModel.Hovedtekst);
            page.MainBody = new XhtmlString(page.MigratedSatelliteData.MainText);
            page.MigratedSatelliteData.UrlId = DecodeString(importModel.UrlId);
            page.MainIntro = DecodeString(importModel.Ingress);
            page.MigratedSatelliteData.Date = DecodeString(importModel.Date);
            page.Created = DateTime.TryParse(page.MigratedSatelliteData.Date, out var createdDate) ? createdDate : page.Created;
            page.SetChangedOnPublish = true;
            page.MigratedSatelliteData.ReadableUrl = DecodeString(importModel.ReadableUrl);
            page.MigratedSatelliteData.Factbox = DecodeStringTwice(importModel.Faktaboks);
            page.MigratedSatelliteData.CreationDate = DecodeString(importModel.CreationDate);
            page.ListIntro = DecodeString(importModel.Forsideingress);
            page.MigratedSatelliteData.EffectiveDate = DecodeString(importModel.EffectiveDate);
            page.MigratedSatelliteData.ShowPublishedDate = DecodeString(importModel.ShowPublishDate);
            page.MigratedSatelliteData.ImageUrl = DecodeString(importModel.Forsidebilde?.Url);
            page.MigratedSatelliteData.ImagePhotographer = DecodeString(importModel.Forsidebilde?.Fotograf);
            page.MigratedSatelliteData.ImageReadableUrl = DecodeString(importModel.Forsidebilde?.ReadableUrl);
            page.MigratedSatelliteData.ImageOriginal = DecodeString(importModel.Forsidebilde?.Original);
            page.MigratedSatelliteData.ImageAltText = DecodeString(importModel.Forsidebilde?.Alttext);
            page.MigratedSatelliteData.IngressImageUrl = DecodeString(importModel.Ingressbilde?.Url);
            page.MigratedSatelliteData.IngressImagePhotographer = DecodeString(importModel.Ingressbilde?.Fotograf);
            page.MigratedSatelliteData.IngressImageReadableUrl = DecodeString(importModel.Ingressbilde?.ReadableUrl);
            page.MigratedSatelliteData.IngressImageOriginal = DecodeString(importModel.Ingressbilde?.Original);
            page.MigratedSatelliteData.IngressImageAltText = DecodeString(importModel.Ingressbilde?.Alttext);
            page.MigratedSatelliteData.CampaignImagePhotographer = DecodeString(importModel.Campaignimage?.Fotograf);
            page.MigratedSatelliteData.CampaignImageOriginal = DecodeString(importModel.Campaignimage?.Original);
            page.MigratedSatelliteData.CampaignImageAltText = DecodeString(importModel.Campaignimage?.Alttext);
            page.MigratedSatelliteData.CanBeSyndicated = DecodeString(importModel.Syndikerbar);
            page.MigratedSatelliteData.Modified = DecodeString(importModel.Modified);
            page.MigratedSatelliteData.ReviewState = importModel.ReviewState.ToString();
        }
    }
}