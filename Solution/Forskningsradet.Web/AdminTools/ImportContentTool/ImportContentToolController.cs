﻿using EPiServer;
using EPiServer.Core;
using EPiServer.PlugIn;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Repositories.Contracts;
using Forskningsradet.Web.AdminTools.ImportContent.ViewModels;
using Forskningsradet.Web.AdminTools.ImportContentTool.ImportModels;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Forskningsradet.Web.AdminTools.ImportContentTool
{
    [GuiPlugIn(
        Area = PlugInArea.AdminMenu,
        Url = "/AdminTools/import-content",
        DisplayName = "Import Content [NP]")]
    [Authorize(Roles = "CmsAdmins")]
    public class ImportContentToolController : Controller
    {
        private readonly IPageRepository _pageRepository;
        private readonly IContentLoader _contentLoader;
        private readonly IImportContentHelper _importContentHelper;
        private PageReference _importRootPageReference;
        private PageReference _importRootFolderReferenceForContentNotMarkedAsPublished;
        private const string _language = "no";

        public ImportContentToolController(IPageRepository pageRepository, IContentLoader contentLoader)
        {
            _pageRepository = pageRepository;
            _contentLoader = contentLoader;
            _importContentHelper = new ImportContentHelper();
        }

        public ActionResult Index()
        {
            var viewModel = new ImportContentViewModel();
            return View("~/AdminTools/ImportContentTool/Views/Index.cshtml", viewModel);
        }

        [HttpPost]
        public ActionResult Index(HttpPostedFileBase file, int rootPageId, int? rootPrivateId, int? take, bool? testRun)
        {
            if (file is null || !file.ContentType.Equals("application/json") || rootPageId == 0)
                return RedirectToAction("Index");

            _importRootPageReference = new PageReference(rootPageId);
            _importRootFolderReferenceForContentNotMarkedAsPublished = rootPrivateId is null ? null : new PageReference(rootPrivateId.Value);
            var counter = ImportArticles<NewsArticlePage>(file, take ?? 0, testRun ?? false);

            var viewModel = new ImportContentViewModel
            {
                Message = "Success!",
                Count = counter
            };
            return View("~/AdminTools/ImportContentTool/Views/Index.cshtml", viewModel);
        }

        private int ImportArticles<T>(HttpPostedFileBase file, int take, bool testRun) where T : ArticlePage
        {
            var articles = GetImportModels<List<SatelliteArticleImportModel>>(file);
            return BuildArticlePages<T>(articles, _language, take, testRun);
        }

        private int BuildArticlePages<T>(Dictionary<string, List<SatelliteArticleImportModel>> articles, string language, int take, bool testRun) where T : ArticlePage
        {
            var lists = articles.Where(x => x.Value?.Count > 0);
            if (take > 0)
                lists = lists.Take(take);

            return _importRootFolderReferenceForContentNotMarkedAsPublished is null
                ? BuildContentArticles<T>(lists, _importRootPageReference, language, take, testRun)
                : BuildContentArticlesSeparateFolders<T>(lists, _importRootPageReference, _importRootFolderReferenceForContentNotMarkedAsPublished, language, take, testRun);
        }

        private int BuildContentArticles<T>(IEnumerable<KeyValuePair<string, List<SatelliteArticleImportModel>>> lists, PageReference importRootReference, string language, int take, bool testRun) where T : ArticlePage
        {
            var counter = 0;

            foreach (var yearLists in lists)
            {
                var yearFolder = testRun
                    ? new FolderPage()
                    : _pageRepository.GetOrCreateFolder(importRootReference, yearLists.Key, language);

                CreateArticlesInYearList(yearLists.Value, yearFolder);
            }

            return counter;

            void CreateArticlesInYearList(IEnumerable<SatelliteArticleImportModel> list, FolderPage folder)
            {
                if (take > 0)
                    list = list.Take(take).ToList();

                foreach (var importModel in list)
                {
                    if (!testRun)
                        CreateArticlePage<T>(folder.PageLink, importModel, language);
                    counter++;
                }
            }
        }

        private int BuildContentArticlesSeparateFolders<T>(IEnumerable<KeyValuePair<string, List<SatelliteArticleImportModel>>> lists, PageReference importRootReference, PageReference importRootPrivateReference, string language, int take, bool testRun) where T : ArticlePage
        {
            var counter = 0;

            foreach (var yearLists in lists)
            {
                var yearFolder = testRun
                    ? new FolderPage()
                    : _pageRepository.GetOrCreateFolder(importRootReference, yearLists.Key, language);

                var articleList = yearLists.Value.Where(x => x.ReviewState == ReviewState.Published);
                CreateArticlesInYearList(articleList, yearFolder);

                var privateYearFolder = testRun || ListContainsOnlyContentMarkedAsPublished(yearLists.Value)
                    ? new FolderPage()
                    : _pageRepository.GetOrCreateFolder(importRootPrivateReference, yearLists.Key, language);

                var notPublishedArticleList = yearLists.Value.Where(x => x.ReviewState != ReviewState.Published);
                CreateArticlesInYearList(notPublishedArticleList, privateYearFolder);
            }

            return counter;

            bool ListContainsOnlyContentMarkedAsPublished(List<SatelliteArticleImportModel> list) =>
                list.All(x => x.ReviewState == ReviewState.Published);

            void CreateArticlesInYearList(IEnumerable<SatelliteArticleImportModel> list, FolderPage folder)
            {
                if (take > 0)
                    list = list.Take(take).ToList();

                foreach (var importModel in list)
                {
                    if (!testRun)
                        CreateArticlePage<T>(folder.PageLink, importModel, language);
                    counter++;
                }
            }
        }

        private void CreateArticlePage<T>(PageReference parentReference, SatelliteArticleImportModel importModel, string language) where T : ArticlePage
        {
            if (parentReference is null)
                return;

            if (PageWithSameIdAlreadyExists<T>(parentReference, importModel.Id))
                return;

            var page = _pageRepository.Create<T>(parentReference, new CultureInfo(language));
            _importContentHelper.PopulateArticlePage(page, importModel);
            _pageRepository.SaveAsDraft(page);
        }

        private bool PageWithSameIdAlreadyExists<T>(ContentReference folderReference, int importModelId) where T : ArticlePage
        {
            var childrenOfFolder = _contentLoader.GetChildren<T>(folderReference, LanguageSelector.AutoDetect(true));

            if (childrenOfFolder.Any(x => x.MigratedSatelliteData.Id == importModelId))
                return true;

            var folders = _contentLoader.GetChildren<FolderPage>(folderReference, LanguageSelector.AutoDetect(true));
            return folders.Any(x => PageWithSameIdAlreadyExists<T>(x.PageLink, importModelId));
        }

        private Dictionary<string, T> GetImportModels<T>(HttpPostedFileBase file)
        {
            using (var sr = new StreamReader(file.InputStream))
            {
                var json = sr.ReadToEnd();
                return _importContentHelper.DeserializeContent<T>(json);
            }
        }
    }
}