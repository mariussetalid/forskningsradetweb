using System;
using System.Globalization;
using System.Linq;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAccess;
using EPiServer.PlugIn;
using EPiServer.Scheduler;
using EPiServer.Web;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ContentModels.Contracts;
using log4net;

namespace Forskningsradet.Web.ScheduledJobs
{
    [ScheduledPlugIn(
        DisplayName = "Kalkuler metadata for filer",
        Description = "Kan ta lang tid. Oppdaterer filstørrelse og filtype for mediafiler som allerede er lastet opp.",
        SortIndex = 4)]
    public class UpdateContentMediaMetaDataJob : ScheduledJobBase
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(UpdateContentMediaMetaDataJob));
        private readonly IContentRepository _contentRepository;

        public UpdateContentMediaMetaDataJob(IContentRepository contentRepository) => _contentRepository = contentRepository;

        public override string Execute()
        {
            try
            {
                var existingMedia = _contentRepository
                    .GetItems(
                        _contentRepository.GetDescendents(SiteDefinition.Current.GlobalAssetsRoot),
                        new CultureInfo(LanguageConstants.NorwegianLanguageBranch))
                    .OfType<MediaData>()
                    .Where(x => x is IContentMediaMetaData);

                int i = 0;
                foreach (var mediaData in existingMedia)
                {
                    var writableMedia = mediaData.CreateWritableClone() as MediaData;
                    _contentRepository.Save(writableMedia, SaveAction.Publish);
                    i++;
                }

                return $"Lagrer {i} filer";
            }
            catch (Exception e)
            {
                Logger.Error($"Exception while running {nameof(UpdateContentMediaMetaDataJob)}", e);
                throw;
            }
        }
    }
}