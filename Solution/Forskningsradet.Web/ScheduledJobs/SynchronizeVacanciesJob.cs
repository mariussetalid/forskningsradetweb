﻿using EPiServer.DataAbstraction;
using EPiServer.Logging.Compatibility;
using EPiServer.PlugIn;
using EPiServer.Scheduler;
using Forskningsradet.Core;
using Forskningsradet.Core.Services.Contracts;

namespace Forskningsradet.Web.ScheduledJobs
{
    [ScheduledPlugIn(
        DisplayName = "Synkroniser \"Ledig stilling\"-sider",
        Description = "Synkroniserer ledige stillinger fra HR Manager",
        DefaultEnabled = true,
        InitialTime = "1.1:0:0",
        IntervalLength = 1,
        IntervalType = ScheduledIntervalType.Days,
        SortIndex = 3)]
    public class SynchronizeVacanciesJob : ScheduledJobBase
    {
        private readonly IHrManagerSynchronizationService _synchronizationService;
        private static readonly ILog Log = LogManager.GetLogger(typeof(SynchronizeVacanciesJob));

        public SynchronizeVacanciesJob(IHrManagerSynchronizationService synchronizationService)
            => _synchronizationService = synchronizationService;

        public override string Execute() 
            => AsyncHelpers.RunSync(() => _synchronizationService.Synchronize());
    }
}