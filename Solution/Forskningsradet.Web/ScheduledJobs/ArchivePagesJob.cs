using EPiServer.PlugIn;
using EPiServer.Scheduler;
using Forskningsradet.Common.Logging;
using Forskningsradet.Core.Logging;
using Forskningsradet.Core.Processes.Contexts;
using Forskningsradet.Core.Services.Contracts;

namespace Forskningsradet.Web.ScheduledJobs
{
    [ScheduledPlugIn(
        DisplayName = "Arkiver sider i WebSak",
        Description = "Sender arkivert PDF av nylig publiserte sider til WebSak.",
        SortIndex = 1)]
    public class ArchivePagesJob : ScheduledJobBase
    {
        private readonly IProcessFactory _processFactory;
        private readonly IIntegrationLogger _logger;

        public ArchivePagesJob(IProcessFactory processFactory, IIntegrationLogger integrationLogger)
        {
            _processFactory = processFactory;
            _logger = integrationLogger;
        }

        public override string Execute()
        {
            var context = new ArchivingContext(_logger);
            var process = _processFactory.GetProcessForContext(context);

            process.Run();

            return context.GetProgressReport();
        }
    }
}