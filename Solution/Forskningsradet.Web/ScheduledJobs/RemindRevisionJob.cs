﻿using EPiServer.DataAbstraction;
using EPiServer.Editor;
using EPiServer.Logging.Compatibility;
using EPiServer.Notification;
using EPiServer.PlugIn;
using EPiServer.Scheduler;
using EPiServer.ServiceLocation;
using EPiServer.Web.Routing;
using Forskningsradet.Core;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.QueryParameters;
using Forskningsradet.Core.Models.RequestModels;
using Forskningsradet.Core.Models.ViewModels.Forms;
using Forskningsradet.Core.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Forskningsradet.Web.ScheduledJobs
{
    [ScheduledPlugIn(
        DisplayName = "Revisjonspåminnelse",
        Description = "Send ut påminnelse om revisjon av sider til redaktører",
        DefaultEnabled = true,
        IntervalLength = 1,
        IntervalType = ScheduledIntervalType.Days,
        SortIndex = 2)]
    public class RemindRevisionJob : ScheduledJobBase
    {
        private IRevisionService _revisionReportService;

        public RemindRevisionJob(IRevisionService revisionReportService) => 
            _revisionReportService = revisionReportService;

        public override string Execute() =>
             AsyncHelpers.RunSync(() => _revisionReportService.NotifyEditorsWithUnrevisedContent());
    }
}