using EPiServer.PlugIn;
using EPiServer.Scheduler;
using Forskningsradet.Common.Logging;
using Forskningsradet.Core.Processes.Contexts;
using Forskningsradet.Core.Services.Contracts;

namespace Forskningsradet.Web.ScheduledJobs
{
    [ScheduledPlugIn(
        DisplayName = "Oppdater pressemeldinger fra NTB",
        Description = "Henter alle pressemeldinger fra NTB og oppdaterer nettsiden med disse.",
        SortIndex = 5)]
    public class UpdatePressReleasesJob : ScheduledJobBase
    {
        private readonly IProcessFactory _processFactory;
        private readonly IIntegrationLogger _logger;

        public UpdatePressReleasesJob(IProcessFactory processFactory, IIntegrationLogger logger)
        {
            _processFactory = processFactory;
            _logger = logger;
        }
        
        public override string Execute()
        {
            var context = new UpdatePressReleasesContext(_logger);
            var process = _processFactory.GetProcessForContext(context);

            process.Run();

            return context.GetProgressReport();
        }
    }
}