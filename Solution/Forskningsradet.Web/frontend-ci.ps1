#
# frontend-ci.ps1
#
$frontendDirectory = $PSScriptRoot + "\Frontend"
Write-Host "Trying to find Frontend directory at: "$frontendDirectory
yarn --cwd $frontendDirectory install --production
yarn --cwd $frontendDirectory audit
yarn --cwd $frontendDirectory test:unit
yarn --cwd $frontendDirectory build
yarn --cwd $frontendDirectory build:mailchimp