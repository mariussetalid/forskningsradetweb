﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace Forskningsradet.Web.RenderTemplates
{
    public class ImageSize
    {
        private static readonly Regex ImageIdRemoveRegex = new Regex(@",,\d+", RegexOptions.Compiled);

        private const string QueryStartMark = "?";
        private const string QueryParamsSeparator = "&";
        private const string WidthParamTemplate = "width={0}";
        private const string HeightParamTemplate = "height={0}";
        private const string TransformParamTemplate = "transform={0}";


        private int? _mobileWidth;
        private int? _mobileHeight;
        private ImageTransformType? _mobileTransformType;

        public int Width { get; set; }

        public int Height { get; set; }

        public int MobileWidth
        {
            get { return _mobileWidth ?? Width; }
            set { _mobileWidth = value; }
        }

        public int MobileHeight
        {
            get { return _mobileHeight ?? Height; }
            set { _mobileHeight = value; }
        }

        public ImageTransformType? TransformType { get; set; }

        public ImageTransformType? MobileTransformType
        {
            get { return _mobileTransformType ?? TransformType; }
            set { _mobileTransformType = value; }
        }

        public string AppendNormalSizeParamsToUrl(string url)
        {
            return AppendQueryParamsToUrl(url, GetNormalSizeQueryParams());
        }

        public string AppendMobileSizeParamsToUrl(string url)
        {
            return AppendQueryParamsToUrl(url, GetMobileSizeQueryParams());
        }

        private string GetNormalSizeQueryParams()
        {
            return GetSizeQueryParams(Width, Height, TransformType);
        }

        private string GetMobileSizeQueryParams()
        {
            return GetSizeQueryParams(MobileWidth, MobileHeight, MobileTransformType);
        }

        private string GetSizeQueryParams(int width, int height, ImageTransformType? transformType)
        {
            StringBuilder queryBuilder = new StringBuilder();
            string separator = string.Empty;

            if (width > 0)
            {
                queryBuilder.Append(string.Format(WidthParamTemplate, width));
                separator = QueryParamsSeparator;
            }

            if (height > 0)
            {
                queryBuilder.Append(separator);
                queryBuilder.Append(string.Format(HeightParamTemplate, height));
                separator = QueryParamsSeparator;
            }

            if (queryBuilder.Length > 0 && transformType != null)
            {
                queryBuilder.Append(separator);
                queryBuilder.Append(string.Format(TransformParamTemplate, transformType));
            }

            return queryBuilder.ToString();
        }

        private string AppendQueryParamsToUrl(string url, string queryParams)
        {
            if (string.IsNullOrEmpty(queryParams))
            {
                return url;
            }

            url = ImageIdRemoveRegex.Replace(url, string.Empty); // episerver bug workaround: episerver generates idit url with id for images opened in edit mode firstly.

            string separator = url.Contains(QueryStartMark) ? QueryParamsSeparator : QueryStartMark;
            return url + separator + queryParams;
        }
    }
}