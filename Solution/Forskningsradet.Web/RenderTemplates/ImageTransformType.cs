﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Forskningsradet.Web.RenderTemplates
{
    public enum ImageTransformType
    {
        Fit,
        Fill,
        DownFit,
        DownFill,
        Crop,
        Stretch
    }
}