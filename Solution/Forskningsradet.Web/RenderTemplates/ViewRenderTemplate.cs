﻿using System;
using System.IO;
using System.Web.Mvc;
using EPiServer;
using EPiServer.Web;
using EPiServer.Web.Routing;
using Forskningsradet.Core.Models.ContentModels.Blocks;

namespace Forskningsradet.Web.RenderTemplates
{
    public abstract class ViewRenderTemplate<T> : IView, IRenderTemplate<T> where T : BaseBlockData
    {
        protected virtual string DefaultViewName => "ReactBlockView";
        protected ViewContext ViewContext { get; private set; }
        protected TextWriter Writer { get; private set; }

        public virtual void Render(ViewContext viewContext, TextWriter writer)
        {
            ViewContext = viewContext;
            Writer = writer;
            var currentContent = ViewContext.RequestContext.GetRoutedData<T>() 
                                    ?? viewContext.RouteData.Values["currentContent"] as T;
            ViewContext.RequestContext.SetController(currentContent.GetOriginalType().Name);
            Render(currentContent);
        }

        protected abstract void Render(T currentContent);

        protected virtual void RenderView(object model) 
            => RenderView(DefaultViewName, model);

        protected virtual void RenderView(string viewName, object model) 
            => RenderView(viewName, null, model);

        protected virtual void RenderView(string viewName, string masterName, object model)
        {
            var result = GetViewEngineResult(viewName, masterName);
            RenderView(model, result, viewName);
        }

        protected virtual void RenderPartialView(object model) 
            => RenderPartialView(DefaultViewName, model);

        protected virtual void RenderPartialView(string viewName, object model)
        {
            var result = GetPartialViewEngineResult(viewName);
            RenderView(model, result, viewName);
        }

        private void RenderView(object model, ViewEngineResult result, string viewName)
        {
            if (result.View is null)
                throw new InvalidOperationException($"The view '{viewName}' or its master was not found or no view engine supports the searched locations.");
            ViewContext.ViewData.Model = model;
            result.View.Render(ViewContext, Writer);
        }

        private ViewEngineResult GetPartialViewEngineResult(string viewName) 
            => ViewEngines.Engines.FindPartialView(ViewContext, viewName);

        private ViewEngineResult GetViewEngineResult(string viewName, string masterName) 
            => ViewEngines.Engines.FindView(ViewContext, viewName, masterName);
    }
}