﻿using Destructurama;
using Forskningsradet.Core.Logging;
using Forskningsradet.Web;
using Microsoft.Owin;
using Owin;
using Serilog;

[assembly: OwinStartup(typeof(Startup))]

namespace Forskningsradet.Web
{
    /// <summary>
    /// Owin startup class.
    /// </summary>
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            ConfigureLogging(app);
        }

        private void ConfigureLogging(IAppBuilder app)
        {
            Log.Logger = new LoggerConfiguration()
                .ReadFrom.AppSettings()
                .Enrich.FromLogContext()
                .Enrich.WithCorrelationId()
                .Enrich.With<IdentityEnricher>()
                .Destructure.UsingAttributes()
                .CreateLogger();
        }
    }
}