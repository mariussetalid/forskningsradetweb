﻿using System;
using System.Globalization;
using System.Threading;
using EPiServer;
using EPiServer.ServiceLocation;
using FakeItEasy;
using Forskningsradet.Core.Services;
using Forskningsradet.Core.Services.Contracts;

namespace Forskningsradet.UnitTests.TestUtils
{
    public class FakeServiceLocator : IDisposable
    {
        private static readonly AsyncLocal<FakeServiceLocator> CurrentContext = new AsyncLocal<FakeServiceLocator>();
        private static readonly AsyncLocal<FakeServiceLocator> PrevContext = new AsyncLocal<FakeServiceLocator>();

        private ILanguageContext _languageContext;
        private IContentLoader _contentLoader;
        private IApiKeyStore _apiKeyStore;

        static FakeServiceLocator()
        {
            var fakeLocator = A.Fake<IServiceLocator>();
            ServiceLocator.SetLocator(fakeLocator);

            A.CallTo(fakeLocator).WithReturnType<ILanguageContext>().ReturnsLazily(() => CurrentContext.Value._languageContext);
            A.CallTo(fakeLocator).WithReturnType<IContentLoader>().ReturnsLazily(() => CurrentContext.Value._contentLoader);
            A.CallTo(fakeLocator).WithReturnType<IApiKeyStore>().ReturnsLazily(() => CurrentContext.Value._apiKeyStore);
        }

        public FakeServiceLocator()
        {
            PrevContext.Value = CurrentContext.Value;
            CurrentContext.Value = this;
        }

        public FakeServiceLocator WithLanguageContext(CultureInfo culture)
        {
            var languageContext = A.Fake<ILanguageContext>();
            A.CallTo(() => languageContext.CurrentCulture).Returns(culture);
            _languageContext = languageContext;
            return this;
        }

        public FakeServiceLocator WithContentLoader(IContentLoader contentLoader)
        {
            _contentLoader = contentLoader;
            return this;
        }

        public FakeServiceLocator WithApiKeyStore(IApiKeyStore apiKeyStore)
        {
            _apiKeyStore = apiKeyStore;
            return this;
        }

        public void Dispose()
        {
            CurrentContext.Value = PrevContext.Value;
            _languageContext = null;
        }
    }
}
