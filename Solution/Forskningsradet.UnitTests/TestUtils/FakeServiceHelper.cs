﻿using EPiServer;
using EPiServer.Core;
using FakeItEasy;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.UnitTests.TestUtils
{
    public static class FakeServiceHelper
    {
        public static IImageReactModelBuilder CreateImageRefToSrcFakeImageBuilder()
        {
            var builder = A.Fake<IImageReactModelBuilder>();

            A.CallTo(() => builder.BuildImage(A<ContentReference>._))
                .ReturnsLazily(x =>
                {
                    var imageLink = x.GetArgument<ContentReference>(0);
                    return imageLink is null ? null : new ReactModels.Image { Src = imageLink.ID.ToString() };
                });

            return builder;
        }

        public static IFluidImageReactModelBuilder CreateImageRefToSrcFakeFluidImageBuilder()
        {
            var builder = A.Fake<IFluidImageReactModelBuilder>();

            A.CallTo(() => builder.BuildReactModel(A<ContentReference>._, A<string>._))
                .ReturnsLazily(x =>
                {
                    var imageLink = x.GetArgument<ContentReference>(0);
                    return new ReactModels.FluidImage { Src = imageLink?.ID.ToString() };
                });

            return builder;
        }

        public static ILinkReactModelBuilder CreateFakeLinkBuilder()
        {
            var builder = A.Fake<ILinkReactModelBuilder>();
            A.CallTo(() => builder.BuildLink(A<Url>._, A<string>._))
                .ReturnsLazily(x =>
                {
                    var url = x.GetArgument<Url>(0)?.ToString();
                    var text = x.GetArgument<string>(1);
                    return string.IsNullOrEmpty(url) || string.IsNullOrEmpty(text) ? null : new ReactModels.Link { Url = url, Text = text };
                });

            A.CallTo(() => builder.BuildLink(A<EditLinkBlock>._, A<string>._))
                .ReturnsLazily(x =>
            {
                var linkBlock = x.GetArgument<EditLinkBlock>(0);
                return linkBlock is null || string.IsNullOrEmpty(linkBlock.Link?.ToString()) || string.IsNullOrEmpty(linkBlock.Text) ? null :
                    new ReactModels.Link
                    {
                        Url = linkBlock.Link?.ToString(),
                        Text = linkBlock.Text?.ToString(),
                        Id = linkBlock.AnchorId,
                    };
            });

            return builder;
        }
    }
}
