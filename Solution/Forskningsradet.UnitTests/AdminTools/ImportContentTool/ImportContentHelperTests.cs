﻿using FakeItEasy;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Web.AdminTools.ImportContentTool;
using Forskningsradet.Web.AdminTools.ImportContentTool.ImportModels;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Forskningsradet.UnitTests.AdminTools.ImportContentTool
{
    public class ImportContentHelperTests
    {
        private readonly IImportContentHelper _sut;

        public ImportContentHelperTests()
        {
            _sut = new ImportContentHelper();
        }

        [Theory]
        [InlineData("http%3A//test.no/things/title-thing", "http://test.no/things/title-thing")]
        [InlineData("2003-12-22%2000%3A00%3A00", "2003-12-22 00:00:00")]
        [InlineData("Forskerne%20har%20samlet%20ledetr%C3%A5dene.%20%20Kan%20du%20l%C3%B8se%20mysteriet%3F", "Forskerne har samlet ledetrådene.  Kan du løse mysteriet?")]
        public void DecodeString_ShouldReturnUrlDecodedAndHtmlDecodedString_GivenString(string input, string expected)
        {
            var result = _sut.DecodeString(input);

            Assert.Equal(expected, result);
        }

        [Theory]
        [InlineData("{\"2003\": []}", "2003")]
        public void DeserializeContent_ShouldReturnDictionary_WhenInputIsValid(string json, string expected)
        {
            var result = _sut.DeserializeContent<List<SatelliteArticleImportModel>>(json);

            Assert.Equal(expected, result.Keys.FirstOrDefault());
        }

        [Theory]
        [InlineData("{\"2003\": [    {\n      \"forfattere\": [],\n      \"url\": \"\",\n      \"hovedtekst\": \"\",\n      \"also_published_in\": [],\n      \"show_publish_date\": \"Ja\",\n      \"ingress\": \"\",\n      \"Date\": \"2003-12-22%2000%3A00%3A00\",\n      \"ingressbilde\": {},\n      \"tm_serial\": 71859,\n      \"readable_url\": \"\",\n      \"handler_om\": [],\n      \"faktaboks\": \"\",\n      \"CreationDate\": \"\",\n      \"forsidebilde\": {},\n      \"title\": \"Zzzzzov%20godt%21\",\n      \"syndikerbar\": \"Nei\",\n      \"forsideingress\": \"\",\n      \"id\": \"zzzzzov-godt\",\n      \"EffectiveDate\": \"\",\n      \"modified\": \"\"\n    }]}", 1)]
        public void DeserializeContent_ShouldReturnArticles_WhenInputIsValid(string json, int expected)
        {
            var result = _sut.DeserializeContent<List<SatelliteArticleImportModel>>(json);

            Assert.Equal(expected, result.Values.Count);
        }

        [Theory]
        [InlineData("{\"2003\": [    {\n      \"forfattere\": [\n        {\n          \"tittel\": \"skribent\",\n          \"navn\": \"Terje%20Stenstad\"\n        }\n      ],\n      \"url\": \"http%3A//www.nysgjerrigper.no/Artikler/zzzzzov-godt\",\n      \"hovedtekst\": \"%3Cp%3E%0D%0AVed%20enkelte%20sykehus%20finnes%20det%20laboratorier%20der%20man%20studerer%20folk%20mens%20de%20sover.%20Her%20arbeider%20forskere%20med%20%26aring%3B%20finne%20ut%20om%20s%26oslash%3Bvnens%20hemmeligheter.%20%0D%0A%3C/p%3E%0D%0A%3Cp%3E%0D%0AVed%20Ullev%26aring%3Bl%20sykehus%20i%20Oslo%20finnes%20ett%20av%20s%26oslash%3Bvnlaboratoriene.%20Hver%20uke%20kommer%20det%20mennesker%20for%20%26aring%3B%20overnatte%20her.%20Dette%20er%20mennesker%20som%20har%20s%26oslash%3Bvnproblemer.%20Det%20kan%20for%20eksempel%20v%26aelig%3Bre%20at%20de%20sovner%20p%26aring%3B%20tidspunkter%20da%20det%20ikke%20er%20naturlig%20%26aring%3B%20sove%2C%20eller%20at%20de%20faktisk%20%26rdquo%3Bglemmer%26rdquo%3B%20%26aring%3B%20puste%20n%26aring%3Br%20de%20sover.%20%0D%0A%3C/p%3E%0D%0A%3Cdiv%20class%3D%22factbox%20small%20right%20clearfix%22%3E%0D%0A%3Ch2%3EElektroder%3C/h2%3E%0D%0A%3Cp%3E%0D%0AEn%20str%26oslash%3Bmledende%20plate%20eller%20stav%20som%20vanligvis%20er%20plassert%20i%20en%20v%26aelig%3Bske%20som%20leder%20elektrisitet.%20%0D%0A%3C/p%3E%0D%0A%3C/div%3E%0D%0A%3Cp%3E%0D%0AN%26aring%3B%20er%20det%20ikke%20sikkert%20det%20er%20like%20lett%20%26aring%3B%20sove%20p%26aring%3B%20kommando%20p%26aring%3B%20et%20sykehus%2C%20men%20hvis%20de%20dupper%20litt%20av%2C%20kan%20det%20gi%20forskerne%20enkelte%20svar%20p%26aring%3B%20hvorfor%20de%20har%20problemer%20med%20%26aring%3B%20sove.%20For%20%26aring%3B%20finne%20ut%20av%20dette%2C%20unders%26oslash%3Bker%20de%20hva%20som%20skjer%20inne%20i%20hjernen%20mens%20pasienten%20sover.%20Dette%20gj%26oslash%3Br%20de%20ved%20%26aring%3B%20koble%20elektroder%20til%20hodet%20for%20%26aring%3B%20m%26aring%3Ble%20aktiviteten%20i%20hjernen%20til%20den%20som%20sover.%20Forskerne%20vet%20nemlig%20at%20det%20er%20hjernen%20som%20bestemmer%20at%20vi%20trenger%20s%26oslash%3Bvn%3A%20I%20en%20del%20av%20hjernen%20som%20kalles%20s%26oslash%3Bvnsenteret%2C%20lages%20det%20stoffer%20som%20gj%26oslash%3Br%20at%20vi%20blir%20tr%26oslash%3Btte.%20%5Bfaktaboks%20liten%20hoyre%5D%20%0D%0A%3C/p%3E%0D%0A%3Ch2%3EKroppen%20repareres%20%3C/h2%3E%0D%0A%3Cp%3E%0D%0ASelv%20om%20forskerne%20ikke%20er%20helt%20sikre%20p%26aring%3B%20hvorfor%20vi%20m%26aring%3B%20sove%2C%20er%20de%20enige%20om%20at%20s%26oslash%3Bvn%20er%20n%26oslash%3Bdvendig%20for%20at%20et%20menneske%20skal%20kunne%20leve.%20Poenget%20med%20%26aring%3B%20sove%20er%20%26aring%3B%20hvile%20seg%20og%20f%26aring%3B%20tilbake%20de%20kreftene%20man%20har%20brukt%20opp%20i%20l%26oslash%3Bpet%20av%20dagen.%20%0D%0A%3C/p%3E%0D%0A%3Cp%3E%0D%0AFor%20at%20en%20sliten%20kropp%20skal%20kunne%20hvile%20ut%20og%20v%26aelig%3Bre%20frisk%20til%20en%20ny%20dag%2C%20m%26aring%3B%20den%20%26rdquo%3Brepareres%26rdquo%3B%3A%20D%26oslash%3Bde%20celler%20m%26aring%3B%20erstattes%20med%20nye.%20Dette%20kalles%20cellefornyelse.%20Cellefornyelsen%20er%20mest%20aktiv%20mens%20vi%20sover%20dypt%20og%20tungt.%20Stoffer%20som%20f%26aring%3Br%20kroppen%20til%20%26aring%3B%20vokse%2C%20skilles%20ut%20n%26aring%3Br%20vi%20sover%20dypt.%20Derfor%20er%20det%20sv%26aelig%3Brt%20viktig%20at%20alle%20barn%20f%26aring%3Br%20nok%20s%26oslash%3Bvn.%20%0D%0A%3C/p%3E%0D%0A%3Cp%3E%0D%0ADet%20er%20ogs%26aring%3B%20viktig%20%26aring%3B%20sove%20mye%20n%26aring%3Br%20man%20er%20syk.%20N%26aring%3Br%20man%20er%20syk%20har%20kroppen%20ekstra%20mye%20arbeid%20%26aring%3B%20gj%26oslash%3Bre%3A%20I%20tillegg%20til%20cellefornyelse%20og%20reparasjon%20skal%20den%20kjempe%20mot%20bakterier%20og%20virus%20slik%20at%20man%20blir%20frisk%20igjen.%20%0D%0A%3C/p%3E%0D%0A%3Ch2%3EBarn%20trenger%20mye%20s%26oslash%3Bvn%20%3C/h2%3E%0D%0A%3Cp%3E%0D%0ADet%20finnes%20ingen%20fasit%20til%20hva%20som%20er%20nok%20s%26oslash%3Bvn%20for%20hver%20enkelt%20av%20oss.%20De%20fleste%20voksne%20trenger%207-8%20timer%20s%26oslash%3Bvn%2C%20mens%20barn%20ikke%20b%26oslash%3Br%20sove%20mindre%20enn%208-10%20timer.%20Det%20er%20grunnen%20til%20at%20barn%20m%26aring%3B%20legge%20seg%20litt%20tidligere%20enn%20voksne.%20Geniet%20og%20vitenskapsmannen%20Albert%20Einstein%20sov%20visstnok%20i%2010%20timer%20hver%20natt.%20Mange%20forskere%20mener%20at%20flere%20mennesker%20b%26oslash%3Br%20gj%26oslash%3Bre%20som%20Einstein%2C%20for%20de%20fleste%20av%20oss%20f%26aring%3Br%20for%20lite%20s%26oslash%3Bvn.%20Men%20kroppene%20v%26aring%3Bre%20er%20ulike%2C%20og%20det%20er%20ingen%20regel%20for%20hva%20som%20er%20nok%20s%26oslash%3Bvn%20for%20voksne.%20En%20natt%20uten%20s%26oslash%3Bvn%20er%20heller%20ikke%20farlig.%20Men%20hvis%20det%20g%26aring%3Br%20flere%20dager%20i%20strekk%20uten%20at%20vi%20sover%2C%20kan%20vi%20begynne%20%26aring%3B%20forestille%20oss%20ting%20og%20fantasere%20om%20ting%20vi%20tror%20vi%20har%20sett.%20%0D%0A%3C/p%3E%0D%0A%3Cp%3E%0D%0AS%26aring%3B%20f%26oslash%3Blg%20forskernes%20r%26aring%3Bd%20om%20%26aring%3B%20sove%20lenge%20%26ndash%3B%20og%20sov%20godt%21%20%0D%0A%3C/p%3E%0D%0A%3Ch2%3ETILLEGGSINFORMASJON%20%3C/h2%3E%0D%0A%3Ch2%3EVisste%20du%20at%3F%20%3C/h2%3E%0D%0A%3Cp%3E%0D%0AKroppen%20er%20lagd%20slik%20at%20vi%20blir%20tr%26oslash%3Btte%20n%26aring%3Br%20det%20blir%20m%26oslash%3Brkt%21%20Da%20blir%20nemlig%20et%20stoff%20som%20heter%20melatonin%20skilt%20ut%20i%20kroppen.%20Melatonin%20er%20et%20hormon%20som%20styrer%20d%26oslash%3Bgnrytmen%20v%26aring%3Br.%20Om%20dagen%2C%20n%26aring%3Br%20det%20er%20lyst%2C%20lager%20ikke%20kroppen%20melatonin.%20%0D%0A%3C/p%3E%0D%0A%3Cp%3E%0D%0ADe%20fleste%20husdyr%20og%20andre%20pattedyr%20trenger%20daglig%20s%26oslash%3Bvn.%20Noen%20fugler%20og%20fisker%20sover%20ogs%26aring%3B.%20Dyr%20sover%20p%26aring%3B%20sv%26aelig%3Brt%20ulikt%20vis.%20Noen%20fisker%20tar%20seg%20en%20liten%20lur%20mens%20de%20lener%20seg%20til%20en%20stein%20eller%20st%26aring%3Br%20loddrett%20i%20vannet.%20Hunder%2C%20for%20eksempel%2C%20liker%20%26aring%3B%20ta%20flere%20korte%20blunder%20i%20l%26oslash%3Bpet%20av%20dagen.%20Flaggermusen%20sover%20hele%20dagen%2C%20mens%20den%20er%20v%26aring%3Bken%20om%20natten.%20Noen%20dyr%2C%20som%20for%20eksempel%20bj%26oslash%3Brnen%2C%20kan%20sove%20hele%20vinteren%20igjennom%21%20%0D%0A%3C/p%3E%0D%0A%3Cp%3E%0D%0A%3Cem%3EPublisert%20i%20Nysgjerrigper%202%2C%202001%3C/em%3E%20%0D%0A%3C/p%3E%0D%0A\",\n      \"also_published_in\": [\n        \"Nysgjerrigper-bladet\"\n      ],\n      \"show_publish_date\": \"Ja\",\n      \"ingress\": \"Det%20er%20rart%20%C3%A5%20tenke%20p%C3%A5%20at%20hvis%20du%20lever%20til%20du%20er%2080%20%C3%A5r%2C%20har%20du%20brukt%20omtrent%2025%20av%20dem%20til%20%C3%A5%20sove%21%20Men%20til%20tross%20for%20at%20vi%20bruker%20s%C3%A5%20mye%20tid%20til%20denne%20aktiviteten%2C%20vet%20forskerne%20fortsatt%20lite%20om%20hvorfor%20vi%20sover.\",\n      \"Date\": \"2003-12-22%2000%3A00%3A00\",\n      \"ingressbilde\": {\n        \"url\": \"http%3A//www.nysgjerrigper.no/imagearchive/caseimage_sovn-sover-sove\",\n        \"fotograf\": \"\",\n        \"readable_url\": \"http://www.nysgjerrigper.no/imagearchive/caseimage_sovn-sover-sove\",\n        \"original\": \"http%3A//www.nysgjerrigper.no/imagearchive/sovn-sover-sove\",\n        \"alttext\": \"s%C3%B8vn%20sover%20sove\"\n      },\n      \"tm_serial\": 71859,\n      \"readable_url\": \"http://www.nysgjerrigper.no/Artikler/zzzzzov-godt\",\n      \"handler_om\": [\n        \"KROPPEN%20V%C3%85R\",\n        \"Celler\",\n        \"Helse\",\n        \"Hjernen\"\n      ],\n      \"faktaboks\": \"%3Ch2%3EHormon%3C/h2%3E%0D%0A%3Cp%3E%0D%0AHormoner%20er%20en%20type%20proteiner%2C%20og%20proteiner%20er%20stoffer%20som%20finnes%20i%20cellene%20til%20alt%20som%20lever%2C%20b%26aring%3Bde%20planter%20og%20dyr.%20Proteinene%20har%20forskjellige%20oppgaver%2C%20de%20virker%20blant%20annet%20som%20verkt%26oslash%3By%2C%20maskiner%20og%20byggematerialer%20i%20kroppen.%20Hormonene%20er%20kroppens%20budbringere%2C%20og%20seiler%20med%20blodet%20med%20beskjeder%20til%20cellene%20om%20%26aring%3B%20lage%20mer%20eller%20mindre%20av%20bestemte%20stoffer.%20%0D%0A%3C/p%3E\",\n      \"CreationDate\": \"2008-04-04%2012%3A19%3A29\",\n      \"forsidebilde\": {\n        \"url\": \"http%3A//www.nysgjerrigper.no/imagearchive/frontpageimage_sovn-sover-sove\",\n        \"fotograf\": \"\",\n        \"readable_url\": \"http://www.nysgjerrigper.no/imagearchive/frontpageimage_sovn-sover-sove\",\n        \"original\": \"http%3A//www.nysgjerrigper.no/imagearchive/sovn-sover-sove\",\n        \"alttext\": \"s%C3%B8vn%20sover%20sove\"\n      },\n      \"title\": \"Zzzzzov%20godt%21\",\n      \"syndikerbar\": \"Nei\",\n      \"forsideingress\": \"Det%20er%20rart%20%C3%A5%20tenke%20p%C3%A5%20at%20hvis%20du%20lever%20til%20du%20er%2080%20%C3%A5r%2C%20har%20du%20brukt%20omtrent%2025%20av%20dem%20til%20%C3%A5%20sove%21%20Men%20til%20tross%20for%20at%20vi%20bruker%20s%C3%A5%20mye%20tid%20til%20denne%20aktiviteten%2C%20vet%20forskerne%20fortsatt%20lite%20om%20hvorfor%20vi%20sover.\",\n      \"id\": \"zzzzzov-godt\",\n      \"EffectiveDate\": \"2003-12-22%2000%3A00%3A00\",\n      \"modified\": \"\"\n    }]}")]
        public void DeserializeContent_ShouldReturnArticleWithSimpleFields_WhenInputIsValid(string json)
        {
            var expectedUrl = "http%3A//www.nysgjerrigper.no/Artikler/zzzzzov-godt";
            var expectedHovedTekst = "%3Cp%3E%0D%0AVed%20enkelte%20sykehus%20finnes%20det%20laboratorier%20der%20man%20studerer%20folk%20mens%20de%20sover.%20Her%20arbeider%20forskere%20med%20%26aring%3B%20finne%20ut%20om%20s%26oslash%3Bvnens%20hemmeligheter.%20%0D%0A%3C/p%3E%0D%0A%3Cp%3E%0D%0AVed%20Ullev%26aring%3Bl%20sykehus%20i%20Oslo%20finnes%20ett%20av%20s%26oslash%3Bvnlaboratoriene.%20Hver%20uke%20kommer%20det%20mennesker%20for%20%26aring%3B%20overnatte%20her.%20Dette%20er%20mennesker%20som%20har%20s%26oslash%3Bvnproblemer.%20Det%20kan%20for%20eksempel%20v%26aelig%3Bre%20at%20de%20sovner%20p%26aring%3B%20tidspunkter%20da%20det%20ikke%20er%20naturlig%20%26aring%3B%20sove%2C%20eller%20at%20de%20faktisk%20%26rdquo%3Bglemmer%26rdquo%3B%20%26aring%3B%20puste%20n%26aring%3Br%20de%20sover.%20%0D%0A%3C/p%3E%0D%0A%3Cdiv%20class%3D%22factbox%20small%20right%20clearfix%22%3E%0D%0A%3Ch2%3EElektroder%3C/h2%3E%0D%0A%3Cp%3E%0D%0AEn%20str%26oslash%3Bmledende%20plate%20eller%20stav%20som%20vanligvis%20er%20plassert%20i%20en%20v%26aelig%3Bske%20som%20leder%20elektrisitet.%20%0D%0A%3C/p%3E%0D%0A%3C/div%3E%0D%0A%3Cp%3E%0D%0AN%26aring%3B%20er%20det%20ikke%20sikkert%20det%20er%20like%20lett%20%26aring%3B%20sove%20p%26aring%3B%20kommando%20p%26aring%3B%20et%20sykehus%2C%20men%20hvis%20de%20dupper%20litt%20av%2C%20kan%20det%20gi%20forskerne%20enkelte%20svar%20p%26aring%3B%20hvorfor%20de%20har%20problemer%20med%20%26aring%3B%20sove.%20For%20%26aring%3B%20finne%20ut%20av%20dette%2C%20unders%26oslash%3Bker%20de%20hva%20som%20skjer%20inne%20i%20hjernen%20mens%20pasienten%20sover.%20Dette%20gj%26oslash%3Br%20de%20ved%20%26aring%3B%20koble%20elektroder%20til%20hodet%20for%20%26aring%3B%20m%26aring%3Ble%20aktiviteten%20i%20hjernen%20til%20den%20som%20sover.%20Forskerne%20vet%20nemlig%20at%20det%20er%20hjernen%20som%20bestemmer%20at%20vi%20trenger%20s%26oslash%3Bvn%3A%20I%20en%20del%20av%20hjernen%20som%20kalles%20s%26oslash%3Bvnsenteret%2C%20lages%20det%20stoffer%20som%20gj%26oslash%3Br%20at%20vi%20blir%20tr%26oslash%3Btte.%20%5Bfaktaboks%20liten%20hoyre%5D%20%0D%0A%3C/p%3E%0D%0A%3Ch2%3EKroppen%20repareres%20%3C/h2%3E%0D%0A%3Cp%3E%0D%0ASelv%20om%20forskerne%20ikke%20er%20helt%20sikre%20p%26aring%3B%20hvorfor%20vi%20m%26aring%3B%20sove%2C%20er%20de%20enige%20om%20at%20s%26oslash%3Bvn%20er%20n%26oslash%3Bdvendig%20for%20at%20et%20menneske%20skal%20kunne%20leve.%20Poenget%20med%20%26aring%3B%20sove%20er%20%26aring%3B%20hvile%20seg%20og%20f%26aring%3B%20tilbake%20de%20kreftene%20man%20har%20brukt%20opp%20i%20l%26oslash%3Bpet%20av%20dagen.%20%0D%0A%3C/p%3E%0D%0A%3Cp%3E%0D%0AFor%20at%20en%20sliten%20kropp%20skal%20kunne%20hvile%20ut%20og%20v%26aelig%3Bre%20frisk%20til%20en%20ny%20dag%2C%20m%26aring%3B%20den%20%26rdquo%3Brepareres%26rdquo%3B%3A%20D%26oslash%3Bde%20celler%20m%26aring%3B%20erstattes%20med%20nye.%20Dette%20kalles%20cellefornyelse.%20Cellefornyelsen%20er%20mest%20aktiv%20mens%20vi%20sover%20dypt%20og%20tungt.%20Stoffer%20som%20f%26aring%3Br%20kroppen%20til%20%26aring%3B%20vokse%2C%20skilles%20ut%20n%26aring%3Br%20vi%20sover%20dypt.%20Derfor%20er%20det%20sv%26aelig%3Brt%20viktig%20at%20alle%20barn%20f%26aring%3Br%20nok%20s%26oslash%3Bvn.%20%0D%0A%3C/p%3E%0D%0A%3Cp%3E%0D%0ADet%20er%20ogs%26aring%3B%20viktig%20%26aring%3B%20sove%20mye%20n%26aring%3Br%20man%20er%20syk.%20N%26aring%3Br%20man%20er%20syk%20har%20kroppen%20ekstra%20mye%20arbeid%20%26aring%3B%20gj%26oslash%3Bre%3A%20I%20tillegg%20til%20cellefornyelse%20og%20reparasjon%20skal%20den%20kjempe%20mot%20bakterier%20og%20virus%20slik%20at%20man%20blir%20frisk%20igjen.%20%0D%0A%3C/p%3E%0D%0A%3Ch2%3EBarn%20trenger%20mye%20s%26oslash%3Bvn%20%3C/h2%3E%0D%0A%3Cp%3E%0D%0ADet%20finnes%20ingen%20fasit%20til%20hva%20som%20er%20nok%20s%26oslash%3Bvn%20for%20hver%20enkelt%20av%20oss.%20De%20fleste%20voksne%20trenger%207-8%20timer%20s%26oslash%3Bvn%2C%20mens%20barn%20ikke%20b%26oslash%3Br%20sove%20mindre%20enn%208-10%20timer.%20Det%20er%20grunnen%20til%20at%20barn%20m%26aring%3B%20legge%20seg%20litt%20tidligere%20enn%20voksne.%20Geniet%20og%20vitenskapsmannen%20Albert%20Einstein%20sov%20visstnok%20i%2010%20timer%20hver%20natt.%20Mange%20forskere%20mener%20at%20flere%20mennesker%20b%26oslash%3Br%20gj%26oslash%3Bre%20som%20Einstein%2C%20for%20de%20fleste%20av%20oss%20f%26aring%3Br%20for%20lite%20s%26oslash%3Bvn.%20Men%20kroppene%20v%26aring%3Bre%20er%20ulike%2C%20og%20det%20er%20ingen%20regel%20for%20hva%20som%20er%20nok%20s%26oslash%3Bvn%20for%20voksne.%20En%20natt%20uten%20s%26oslash%3Bvn%20er%20heller%20ikke%20farlig.%20Men%20hvis%20det%20g%26aring%3Br%20flere%20dager%20i%20strekk%20uten%20at%20vi%20sover%2C%20kan%20vi%20begynne%20%26aring%3B%20forestille%20oss%20ting%20og%20fantasere%20om%20ting%20vi%20tror%20vi%20har%20sett.%20%0D%0A%3C/p%3E%0D%0A%3Cp%3E%0D%0AS%26aring%3B%20f%26oslash%3Blg%20forskernes%20r%26aring%3Bd%20om%20%26aring%3B%20sove%20lenge%20%26ndash%3B%20og%20sov%20godt%21%20%0D%0A%3C/p%3E%0D%0A%3Ch2%3ETILLEGGSINFORMASJON%20%3C/h2%3E%0D%0A%3Ch2%3EVisste%20du%20at%3F%20%3C/h2%3E%0D%0A%3Cp%3E%0D%0AKroppen%20er%20lagd%20slik%20at%20vi%20blir%20tr%26oslash%3Btte%20n%26aring%3Br%20det%20blir%20m%26oslash%3Brkt%21%20Da%20blir%20nemlig%20et%20stoff%20som%20heter%20melatonin%20skilt%20ut%20i%20kroppen.%20Melatonin%20er%20et%20hormon%20som%20styrer%20d%26oslash%3Bgnrytmen%20v%26aring%3Br.%20Om%20dagen%2C%20n%26aring%3Br%20det%20er%20lyst%2C%20lager%20ikke%20kroppen%20melatonin.%20%0D%0A%3C/p%3E%0D%0A%3Cp%3E%0D%0ADe%20fleste%20husdyr%20og%20andre%20pattedyr%20trenger%20daglig%20s%26oslash%3Bvn.%20Noen%20fugler%20og%20fisker%20sover%20ogs%26aring%3B.%20Dyr%20sover%20p%26aring%3B%20sv%26aelig%3Brt%20ulikt%20vis.%20Noen%20fisker%20tar%20seg%20en%20liten%20lur%20mens%20de%20lener%20seg%20til%20en%20stein%20eller%20st%26aring%3Br%20loddrett%20i%20vannet.%20Hunder%2C%20for%20eksempel%2C%20liker%20%26aring%3B%20ta%20flere%20korte%20blunder%20i%20l%26oslash%3Bpet%20av%20dagen.%20Flaggermusen%20sover%20hele%20dagen%2C%20mens%20den%20er%20v%26aring%3Bken%20om%20natten.%20Noen%20dyr%2C%20som%20for%20eksempel%20bj%26oslash%3Brnen%2C%20kan%20sove%20hele%20vinteren%20igjennom%21%20%0D%0A%3C/p%3E%0D%0A%3Cp%3E%0D%0A%3Cem%3EPublisert%20i%20Nysgjerrigper%202%2C%202001%3C/em%3E%20%0D%0A%3C/p%3E%0D%0A";
            var expectedIngress = "Det%20er%20rart%20%C3%A5%20tenke%20p%C3%A5%20at%20hvis%20du%20lever%20til%20du%20er%2080%20%C3%A5r%2C%20har%20du%20brukt%20omtrent%2025%20av%20dem%20til%20%C3%A5%20sove%21%20Men%20til%20tross%20for%20at%20vi%20bruker%20s%C3%A5%20mye%20tid%20til%20denne%20aktiviteten%2C%20vet%20forskerne%20fortsatt%20lite%20om%20hvorfor%20vi%20sover.";
            var expectedId = 71859;
            var expectedReadableUrl = "http://www.nysgjerrigper.no/Artikler/zzzzzov-godt";
            var expectedFaktaboks = "%3Ch2%3EHormon%3C/h2%3E%0D%0A%3Cp%3E%0D%0AHormoner%20er%20en%20type%20proteiner%2C%20og%20proteiner%20er%20stoffer%20som%20finnes%20i%20cellene%20til%20alt%20som%20lever%2C%20b%26aring%3Bde%20planter%20og%20dyr.%20Proteinene%20har%20forskjellige%20oppgaver%2C%20de%20virker%20blant%20annet%20som%20verkt%26oslash%3By%2C%20maskiner%20og%20byggematerialer%20i%20kroppen.%20Hormonene%20er%20kroppens%20budbringere%2C%20og%20seiler%20med%20blodet%20med%20beskjeder%20til%20cellene%20om%20%26aring%3B%20lage%20mer%20eller%20mindre%20av%20bestemte%20stoffer.%20%0D%0A%3C/p%3E";
            var expectedCreationDate = "2008-04-04%2012%3A19%3A29";
            var expectedTitle = "Zzzzzov%20godt%21";
            var expectedForsideingress = "Det%20er%20rart%20%C3%A5%20tenke%20p%C3%A5%20at%20hvis%20du%20lever%20til%20du%20er%2080%20%C3%A5r%2C%20har%20du%20brukt%20omtrent%2025%20av%20dem%20til%20%C3%A5%20sove%21%20Men%20til%20tross%20for%20at%20vi%20bruker%20s%C3%A5%20mye%20tid%20til%20denne%20aktiviteten%2C%20vet%20forskerne%20fortsatt%20lite%20om%20hvorfor%20vi%20sover.";
            var expectedUrlId = "zzzzzov-godt";
            var expectedEffectiveDate = "2003-12-22%2000%3A00%3A00";

            var result = _sut.DeserializeContent<List<SatelliteArticleImportModel>>(json)
                .Values
                .First()
                .FirstOrDefault();

            Assert.Equal(expectedUrl, result.Url);
            Assert.Equal(expectedHovedTekst, result.Hovedtekst);
            Assert.Equal(expectedIngress, result.Ingress);
            Assert.Equal(expectedId, result.Id);
            Assert.Equal(expectedReadableUrl, result.ReadableUrl);
            Assert.Equal(expectedFaktaboks, result.Faktaboks);
            Assert.Equal(expectedCreationDate, result.CreationDate);
            Assert.Equal(expectedTitle, result.Title);
            Assert.Equal(expectedForsideingress, result.Forsideingress);
            Assert.Equal(expectedUrlId, result.UrlId);
            Assert.Equal(expectedEffectiveDate, result.EffectiveDate);
        }

        [Theory]
        [InlineData("{\"2003\": [    {\"forfattere\": [{\"tittel\": \"skribent\",\"navn\": \"Terje%20Stenstad\"}]}    ]}")]
        public void DeserializeContent_ShouldReturnArticleWithForfatter_WhenGivenForfatter(string json)
        {
            var expectedTittel = "skribent";
            var expectedNavn = "Terje%20Stenstad";

            var result = _sut.DeserializeContent<List<SatelliteArticleImportModel>>(json)
                .Values
                .First()
                .FirstOrDefault()
                .Forfattere
                .First();

            Assert.Equal(expectedTittel, result.Tittel);
            Assert.Equal(expectedNavn, result.Navn);
        }

        [Fact]
        public void PopulateArticlePage_ShouldReturnArticleWithSimpleFieldsDecoded_WhenGivenArticle()
        {
            const string ReadableText = "Zzzzzov godt!";
            const string EncodedText = "Zzzzzov%20godt%21";
            const string ReadableUrl = "http://www.nysgjerrigper.no/Artikler/zzzzzov-godt";
            const string EncodedUrl = "http%3A//www.nysgjerrigper.no/Artikler/zzzzzov-godt";
            const int expectedId = 71859;
            const string expectedTitle = ReadableText;
            const string expectedUrl = ReadableUrl;
            const string expectedMainBody = ReadableText;
            const string expectedUrlId = ReadableUrl;
            const string expectedMainIntro = ReadableText;
            const string expectedDate = ReadableText;
            const string expectedReadableUrl = ReadableUrl;
            const string expectedFactbox = ReadableText;
            const string expectedCreationDate = ReadableText;
            const string expectedListIntro = ReadableText;
            const string expectedEffectiveDate = ReadableText;
            const string expectedShowPublishDate = ReadableText;
            const string expectedImageUrl = ReadableUrl;
            const string expectedImagePhotographer = ReadableText;
            const string expectedImageReadableUrl = ReadableUrl;
            const string expectedImageOriginal = ReadableText;
            const string expectedImageAlttext = ReadableText;
            const string expectedIngressImageUrl = ReadableUrl;
            const string expectedIngressImagePhotographer = ReadableText;
            const string expectedIngressImageReadableUrl = ReadableUrl;
            const string expectedIngressImageOriginal = ReadableText;
            const string expectedIngressImageAlttext = ReadableText;
            const string expectedCampaignImagePhotographer = ReadableText;
            const string expectedCampaignImageOriginal = ReadableText;
            const string expectedCampaignImageAlttext = ReadableText;
            const string expectedBar = ReadableText;
            const string expectedModified = ReadableText;
            const string expectedReviewState = "Private";
            var expectedAuthors = new List<string> { "Someone Famous - Famous Person", "Some Other Guy - Test" };
            var expectedAbout = new List<string> { ReadableText, ReadableText };
            var expectedAlsoPublishedIn = new List<string> { ReadableText, ReadableText };

            var importModel = new SatelliteArticleImportModel
            {
                Id = expectedId,
                Title = EncodedText,
                Url = EncodedUrl,
                Hovedtekst = EncodedText,
                Forfattere = new List<ImportPerson>
                {
                    new ImportPerson { Navn = "Someone%20Famous", Tittel = "Famous%20Person"},
                    new ImportPerson { Navn = "Some%20Other%20Guy", Tittel = "Test"}
                },
                UrlId = EncodedUrl,
                Ingress = EncodedText,
                Date = EncodedText,
                ReadableUrl = EncodedUrl,
                Faktaboks = EncodedText,
                CreationDate = EncodedText,
                Forsideingress = EncodedText,
                EffectiveDate = EncodedText,
                ShowPublishDate = EncodedText,
                HandlerOm = new List<string>
                {
                    EncodedText,
                    EncodedText
                },
                Forsidebilde = new ImportForsideBilde
                {
                    Url = EncodedUrl,
                    Fotograf = EncodedText,
                    ReadableUrl = EncodedUrl,
                    Original = EncodedText,
                    Alttext = EncodedText
                },
                Ingressbilde = new ImportForsideBilde
                {
                    Url = EncodedUrl,
                    Fotograf = EncodedText,
                    ReadableUrl = EncodedUrl,
                    Original = EncodedText,
                    Alttext = EncodedText
                },
                Campaignimage = new ImportForsideBilde
                {
                    Url = EncodedUrl,
                    Fotograf = EncodedText,
                    ReadableUrl = EncodedUrl,
                    Original = EncodedText,
                    Alttext = EncodedText
                },
                AlsoPublishedIn = new List<string>
                {
                    EncodedText,
                    EncodedText
                },
                Syndikerbar = EncodedText,
                Modified = EncodedText,
                ReviewState = ReviewState.Private,
            };

            var article = A.Fake<InformationArticlePage>();

            _sut.PopulateArticlePage(article, importModel);

            Assert.Equal(expectedId, article.MigratedSatelliteData.Id);
            Assert.Equal(expectedTitle, article.MigratedSatelliteData.Title);
            Assert.Equal(expectedUrl, article.MigratedSatelliteData.Url);
            Assert.Equal(expectedMainBody, article.MigratedSatelliteData.MainText);
            Assert.Equal(expectedAuthors, article.MigratedSatelliteData.Authors);
            Assert.Equal(expectedUrlId, article.MigratedSatelliteData.UrlId);
            Assert.Equal(expectedMainIntro, article.MainIntro);
            Assert.Equal(expectedDate, article.MigratedSatelliteData.Date);
            Assert.Equal(expectedReadableUrl, article.MigratedSatelliteData.ReadableUrl);
            Assert.Equal(expectedFactbox, article.MigratedSatelliteData.Factbox);
            Assert.Equal(expectedCreationDate, article.MigratedSatelliteData.CreationDate);
            Assert.Equal(expectedListIntro, article.ListIntro);
            Assert.Equal(expectedEffectiveDate, article.MigratedSatelliteData.EffectiveDate);
            Assert.Equal(expectedShowPublishDate, article.MigratedSatelliteData.ShowPublishedDate);
            Assert.Equal(expectedAbout, article.MigratedSatelliteData.About);
            Assert.Equal(expectedImageUrl, article.MigratedSatelliteData.ImageUrl);
            Assert.Equal(expectedImagePhotographer, article.MigratedSatelliteData.ImagePhotographer);
            Assert.Equal(expectedImageReadableUrl, article.MigratedSatelliteData.ImageReadableUrl);
            Assert.Equal(expectedImageOriginal, article.MigratedSatelliteData.ImageOriginal);
            Assert.Equal(expectedImageAlttext, article.MigratedSatelliteData.ImageAltText);
            Assert.Equal(expectedIngressImageUrl, article.MigratedSatelliteData.IngressImageUrl);
            Assert.Equal(expectedIngressImagePhotographer, article.MigratedSatelliteData.IngressImagePhotographer);
            Assert.Equal(expectedIngressImageReadableUrl, article.MigratedSatelliteData.IngressImageReadableUrl);
            Assert.Equal(expectedIngressImageOriginal, article.MigratedSatelliteData.IngressImageOriginal);
            Assert.Equal(expectedIngressImageAlttext, article.MigratedSatelliteData.IngressImageAltText);
            Assert.Equal(expectedCampaignImagePhotographer, article.MigratedSatelliteData.CampaignImagePhotographer);
            Assert.Equal(expectedCampaignImageOriginal, article.MigratedSatelliteData.CampaignImageOriginal);
            Assert.Equal(expectedCampaignImageAlttext, article.MigratedSatelliteData.CampaignImageAltText);
            Assert.Equal(expectedAlsoPublishedIn, article.MigratedSatelliteData.AlsoPublishedIn);
            Assert.Equal(expectedBar, article.MigratedSatelliteData.CanBeSyndicated);
            Assert.Equal(expectedModified, article.MigratedSatelliteData.Modified);
            Assert.Equal(expectedReviewState, article.MigratedSatelliteData.ReviewState);
            Assert.True(article.SetChangedOnPublish);
        }
    }
}
