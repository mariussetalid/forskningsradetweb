﻿using System;
using System.Globalization;
using EPiServer.Core;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ContentModels.Pages;

namespace Forskningsradet.UnitTests.Builders
{
    public class PressReleasePageBuilder
    {
        private readonly PressReleasePage _page = new TestablePressReleasePage();

        public static PressReleasePageBuilder Create => new PressReleasePageBuilder();

        public PressReleasePageBuilder Default()
        {
            _page.Language = new CultureInfo(LanguageConstants.MasterLanguageBranch);
            _page.Category = new CategoryList();
            _page.Id = "1";
            return this;
        }

        public PressReleasePageBuilder WithContentLink(ContentReference value) =>
            UpdatePage(x => x.ContentLink = value);

        public PressReleasePageBuilder WithStartPublish(DateTime? value) =>
            UpdatePage(x => x.StartPublish = value);

        public PressReleasePageBuilder WithChanged(DateTime value) =>
            UpdatePage(x => x.Changed = value);

        public PressReleasePageBuilder WithName(string value) =>
            UpdatePage(x => x.Name = value);

        public PressReleasePageBuilder WithListTitle(string value) =>
            UpdatePage(x => x.ListTitle = value);

        public PressReleasePageBuilder WithListIntro(string value) =>
            UpdatePage(x => x.ListIntro = value);

        public PressReleasePageBuilder WithId(string value) =>
            UpdatePage(x => x.Id = value);

        public PressReleasePageBuilder WithTitle(string value) =>
            UpdatePage(x => x.Title = value);

        public PressReleasePageBuilder WithLead(string value) =>
            UpdatePage(x => x.Lead = value);

        public PressReleasePageBuilder WithImageUrl(string value) =>
            UpdatePage(x => x.ImageUrl = value);

        public PressReleasePageBuilder WithImageThumbnailUrl(string value) =>
            UpdatePage(x => x.ImageThumbnailUrl = value);

        public PressReleasePageBuilder WithImageCaption(string value) =>
            UpdatePage(x => x.ImageCaption = value);

        public PressReleasePageBuilder WithExternalPublishedAt(DateTime? value) =>
            UpdatePage(x => x.ExternalPublishedAt = value);

        private PressReleasePageBuilder UpdatePage(Action<PressReleasePage> updateAction)
        {
            updateAction(_page);
            return this;
        }

        public static implicit operator PressReleasePage(PressReleasePageBuilder builder) =>
            builder._page;
    }
}
