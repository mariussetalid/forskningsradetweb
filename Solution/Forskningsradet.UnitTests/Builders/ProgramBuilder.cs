﻿using Forskningsradet.Core.Models.ApiModels.Evurdering;

namespace Forskningsradet.UnitTests.Builders
{
    public class ProgramBuilder
    {
        private Program _program;

        public static ProgramBuilder Create =>
            new ProgramBuilder();

        public ProgramBuilder Default()
        {
            _program = new Program
            {
                Id = 939,
                ShortName = "FORSKERMOBILITET",
                Title = "Forskermobilitet"
            };
            return this;
        }

        public static implicit operator Program(ProgramBuilder builder) =>
            builder._program;
    }
}
