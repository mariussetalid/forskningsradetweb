﻿using System;
using System.Globalization;
using EPiServer.Core;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ApiModels.Evurdering;

namespace Forskningsradet.UnitTests.Builders
{
    public class InternationalProposalPageBuilder
    {
        private readonly InternationalProposalPage _proposalPage = new TestableInternationalProposalPage();

        public static InternationalProposalPageBuilder Create => new InternationalProposalPageBuilder();

        public InternationalProposalPageBuilder Default()
        {
            _proposalPage.Language = new CultureInfo(LanguageConstants.MasterLanguageBranch);
            _proposalPage.Category = new CategoryList();
            _proposalPage.TextBlock = new InternationalProposalPageTextBlock();
            return this;
        }

        public InternationalProposalPageBuilder WithContentLink(ContentReference value) =>
            UpdatePage(x => x.ContentLink = value);

        public InternationalProposalPageBuilder WithDeadline(DateTime? value) =>
            UpdatePage(x => x.Deadline = value);

        public InternationalProposalPageBuilder WithFirstDeadline(DateTime? value) =>
            UpdatePage(x => x.FirstDeadline = value);

        public InternationalProposalPageBuilder WithActivationDate(DateTime? value) =>
            UpdatePage(x => x.ActivationDate = value);

        public InternationalProposalPageBuilder WithDeadlineDisplayName(string value) =>
            UpdatePage(x => x.DeadlineDisplayName = value);

        public InternationalProposalPageBuilder WithFirstDeadlineDisplayName(string value) =>
            UpdatePage(x => x.FirstDeadlineDisplayName = value);

        public InternationalProposalPageBuilder WithActivationDateDisplayName(string value) =>
            UpdatePage(x => x.ActivationDateDisplayName = value);

        public InternationalProposalPageBuilder WithListTitle(string value) =>
            UpdatePage(x => x.ListTitle = value);

        public InternationalProposalPageBuilder WithListIntro(string value) =>
            UpdatePage(x => x.ListIntro = value);

        public InternationalProposalPageBuilder WithCategory(CategoryList value) =>
            UpdatePage(x => x.Category = value);

        public InternationalProposalPageBuilder WithId(string value) =>
            UpdatePage(x => x.Id = value);

        public InternationalProposalPageBuilder WithProgram(string value) =>
            UpdatePage(x => x.Program = value);

        public InternationalProposalPageBuilder WithActivationTimeUnit(TimeUnit value) =>
            UpdatePage(x => x.ActivationTimeUnit = value);

        public InternationalProposalPageBuilder WithProposalStateCompleted() =>
            UpdatePage(x => x.Deadline = new DateTime(2010, 1, 2));

        public InternationalProposalPageBuilder WithProposalStateActive()
        {
            _proposalPage.Deadline = null;
            _proposalPage.ActivationDate = new DateTime(2010, 1, 2);
            return this;
        }

        public InternationalProposalPageBuilder WithProposalStatePlanned()
        {
            _proposalPage.Deadline = null;
            _proposalPage.ActivationDate = null;
            return this;
        }

        private InternationalProposalPageBuilder UpdatePage(Action<InternationalProposalPage> updateAction)
        {
            updateAction(_proposalPage);
            return this;
        }

        public static implicit operator InternationalProposalPage(InternationalProposalPageBuilder builder) =>
            builder._proposalPage;
    }
}
