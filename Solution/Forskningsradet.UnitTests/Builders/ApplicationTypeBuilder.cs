﻿using System;
using System.Collections.Generic;
using Forskningsradet.Core.Models.ApiModels.Evurdering;

namespace Forskningsradet.UnitTests.Builders
{
    public class ApplicationTypeBuilder
    {
        private ApplicationType _applicationType;

        public static ApplicationTypeBuilder Create => new ApplicationTypeBuilder();

        public ApplicationTypeBuilder Default()
        {
            _applicationType = new ApplicationType
            {
                MinApplicationAmount = 1,
                MaxApplicationAmount = 2,
                MinProjectLength = 3,
                MaxProjectLength = 4,
                EarliestStartDate = new DateTime(2018, 12, 1),
                LatestProjectPeriodStart = new DateTime(2018, 12, 2),
                LatestProjectPeriodEnd = new DateTime(2018, 12, 3)
            };
            return this;
        }

        public ApplicationTypeBuilder WithSubjects(List<Subject> subjects)
        {
            _applicationType.Subjects = subjects;
            return this;
        }

        public ApplicationTypeBuilder WithMainCriteria(MainCriterion[] mainCriteria)
        {
            _applicationType.MainCriteria = mainCriteria;
            return this;
        }

        public ApplicationTypeBuilder WithEarliestStartDate(DateTime? earliestStartDate)
        {
            _applicationType.EarliestStartDate = earliestStartDate;
            return this;
        }

        public ApplicationTypeBuilder WithLatestProjectPeriodStart(DateTime? latestProjectPeriodStart)
        {
            _applicationType.LatestProjectPeriodStart = latestProjectPeriodStart;
            return this;
        }

        public ApplicationTypeBuilder WithLatestProjectPeriodEnd(DateTime? latestProjectPeriodEnd)
        {
            _applicationType.LatestProjectPeriodEnd = latestProjectPeriodEnd;
            return this;
        }

        public static implicit operator ApplicationType(ApplicationTypeBuilder builder) =>
            builder._applicationType;

        public static explicit operator ApplicationType[](ApplicationTypeBuilder builder) =>
            new List<ApplicationType> { builder._applicationType }.ToArray();
    }
}
