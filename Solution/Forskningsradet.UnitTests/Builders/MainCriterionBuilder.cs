﻿using System.Collections.Generic;
using Forskningsradet.Core.Models.ApiModels.Evurdering;

namespace Forskningsradet.UnitTests.Builders
{
    public class MainCriterionBuilder
    {
        private MainCriterion _mainCriterion;

        public static MainCriterionBuilder Create => new MainCriterionBuilder();

        public MainCriterionBuilder Default()
        {
            _mainCriterion = new MainCriterion
            {
                Id = 2942,
                Title = "Forskning og innovasjon | IP-Næringsliv",
                Description = "I hvilken grad støtter tester at man har \n linjeskift"
            };
            return this;
        }

        public MainCriterionBuilder WithId(long id)
        {
            _mainCriterion.Id = id;
            return this;
        }

        public static implicit operator MainCriterion(MainCriterionBuilder builder) =>
            builder._mainCriterion;

        public static explicit operator MainCriterion[](MainCriterionBuilder builder) =>
            new List<MainCriterion> { builder._mainCriterion }.ToArray();
    }
}
