﻿using System;
using Forskningsradet.Core.Models.ApiModels.Evurdering;

namespace Forskningsradet.UnitTests.Builders
{
    public class ProposalBuilder
    {
        private Proposal _proposal;

        public static ProposalBuilder Create => new ProposalBuilder();

        public ProposalBuilder Default(ProgramBuilder programBuilder, ApplicationTypeBuilder applicationTypeBuilder)
        {
            _proposal = new Proposal
            {
                Title = "NO title",
                EnglishTitle = "EN title",
                Status = ProposalState.Planned,
                DeadlineType = DeadlineType.Date,
                Program = programBuilder,
                ApplicationDeadline = new DateTime(2018, 12, 11),
                Amount = 10,
                StartDate = new DateTime(2018, 12, 4),
                CaseResponsible = "erx",
                ApplicationTypes = (ApplicationType[])applicationTypeBuilder
            };
            return this;
        }

        public ProposalBuilder WithEnglishTitle(string englishTitle)
        {
            _proposal.EnglishTitle = englishTitle;
            return this;
        }

        public ProposalBuilder WithStatus(ProposalState status)
        {
            _proposal.Status = status;
            return this;
        }

        public ProposalBuilder WithStartDate(DateTime? startDate)
        {
            _proposal.StartDate = startDate;
            return this;
        }

        public ProposalBuilder WithApplicationDeadline(DateTime? applicationDeadline)
        {
            _proposal.ApplicationDeadline = applicationDeadline;
            return this;
        }

        public static implicit operator Proposal(ProposalBuilder builder) =>
            builder._proposal;
    }
}
