﻿using System;
using EPiServer.Core;
using System.Globalization;
using System.Linq;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ApiModels.Evurdering;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.ContentModels.PropertyList;

namespace Forskningsradet.UnitTests.Builders
{
    public class ProposalPageBuilder
    {
        private readonly ProposalPage _proposalPage = new TestableProposalPage();

        public static ProposalPageBuilder Create => new ProposalPageBuilder();

        public ProposalPageBuilder Default()
        {
            _proposalPage.Language = new CultureInfo(LanguageConstants.MasterLanguageBranch);
            _proposalPage.Category = new CategoryList();
            _proposalPage.Contact = new ContactBlock();
            _proposalPage.Timeline = new TimeLineBlock();
            return this;
        }

        public ProposalPageBuilder WithContentLink(ContentReference value) =>
            UpdatePage(x => x.ContentLink = value);

        public ProposalPageBuilder WithDeadline(DateTime? value) =>
            UpdatePage(x => x.Deadline = value);

        public ProposalPageBuilder WithSortingNumberInList(int? value) =>
            UpdatePage(x => x.SortingNumberInList = value);

        public ProposalPageBuilder WithProposalState(ProposalState value) =>
            UpdatePage(x => x.ProposalState = value);

        public ProposalPageBuilder WithNonEmptyApplicationResult() =>
            UpdatePage(x => x.ApplicationResult = new PageReference(1));

        public ProposalPageBuilder WithCancellationShouldHideFromActiveTab(bool value) =>
            UpdatePage(x => x.CancellationShouldHideFromActiveTab = value);

        public ProposalPageBuilder WithDeadlineType(DeadlineType value) =>
            UpdatePage(x => x.DeadlineType = value);

        public ProposalPageBuilder WithName(string value) =>
            UpdatePage(x => x.Name = value);

        public ProposalPageBuilder WithActivity(string value) =>
            UpdatePage(x => x.Activity = value);

        public ProposalPageBuilder WithCaseResponsible(string value) =>
            UpdatePage(x => x.CaseResponsible = value);

        public ProposalPageBuilder WithStartDate(DateTime? value) =>
            UpdatePage(x => x.StartDate = value);

        public ProposalPageBuilder WithTimeline(params TimeLineItem[] timeLineItems) =>
            UpdatePage(x => x.Timeline = new TimeLineBlock
            {
                TimeLineItems = timeLineItems?.ToList()
            });

        public ProposalPageBuilder WithProposalAmount(string value) =>
            UpdatePage(x => x.ProposalAmount = value);

        public ProposalPageBuilder WithListTitle(string value) =>
            UpdatePage(x => x.ListTitle = value);

        public ProposalPageBuilder WithListIntro(string value) =>
            UpdatePage(x => x.ListIntro = value);

        public ProposalPageBuilder WithCategory(CategoryList value) =>
            UpdatePage(x => x.Category = value);

        public ProposalPageBuilder WithId(string value) =>
            UpdatePage(x => x.Id = value);

        public ProposalPageBuilder WithMinApplicationAmount(int value) =>
            UpdatePage(x => x.MinApplicationAmount = value);

        public ProposalPageBuilder WithMaxApplicationAmount(int value) =>
            UpdatePage(x => x.MaxApplicationAmount = value);

        public ProposalPageBuilder WithMinProjectLength(int value) =>
            UpdatePage(x => x.MinProjectLength = value);

        public ProposalPageBuilder WithMaxProjectLength(int value) =>
            UpdatePage(x => x.MaxProjectLength = value);

        public ProposalPageBuilder WithPurpose(XhtmlString value) =>
            UpdatePage(x => x.Purpose = value);

        public ProposalPageBuilder WithDisableLink(bool value) =>
            UpdatePage(x => x.DisableLink = value);
        public ProposalPageBuilder WithSpecificApplicationTypeId(string value) =>
            UpdatePage(x => x.SpecificApplicationTypeId = value);

        public ProposalPageBuilder WithApplicationText(string value) =>
            UpdatePage(x => x.ApplicationText = value);

        private ProposalPageBuilder UpdatePage(Action<ProposalPage> updateAction)
        {
            updateAction(_proposalPage);
            return this;
        }

        public static implicit operator ProposalPage(ProposalPageBuilder builder) =>
            builder._proposalPage;
    }
}
