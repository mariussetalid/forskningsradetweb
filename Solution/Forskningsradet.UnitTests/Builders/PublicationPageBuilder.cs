﻿using System;
using EPiServer.Core;
using Forskningsradet.Core.Models.ContentModels.Pages;

namespace Forskningsradet.UnitTests.Builders
{
    public class PublicationPageBuilder
    {
        private readonly PublicationPage _publicationPage = new TestablePublicationPage();

        public static PublicationPageBuilder Create => new PublicationPageBuilder();

        public PublicationPageBuilder Default() => this;

        public PublicationPageBuilder WithPageName(string pageName)
        {
            _publicationPage.PageName = pageName;
            return this;
        }

        public PublicationPageBuilder WithAttachment(ContentReference attachment)
        {
            _publicationPage.Attachment = attachment;
            return this;
        }

        public PublicationPageBuilder WithSubtitle(string subtitle)
        {
            _publicationPage.Subtitle = subtitle;
            return this;
        }

        public PublicationPageBuilder WithAuthor(string author)
        {
            _publicationPage.Author = author;
            return this;
        }

        public PublicationPageBuilder WithPublisher(string publisher)
        {
            _publicationPage.Publisher = publisher;
            return this;
        }

        public PublicationPageBuilder WithStartPublish(DateTime? startPublish)
        {
            _publicationPage.StartPublish = startPublish;
            return this;
        }

        public PublicationPageBuilder WithListImage(ContentReference listImage)
        {
            _publicationPage.ListImage = listImage;
            return this;
        }

        public PublicationPageBuilder WithContentLink(ContentReference contentLink)
        {
            _publicationPage.ContentLink = contentLink;
            return this;
        }

        public static implicit operator PublicationPage(PublicationPageBuilder builder) =>
            builder._publicationPage;
    }
}