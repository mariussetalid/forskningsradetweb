﻿using System;
using Forskningsradet.Core.Models.ContentModels.Blocks;

namespace Forskningsradet.UnitTests.Builders
{
    public class NpQuotesBlockBuilder
    {
        private NpQuotesBlock _npQuotesBlock;

        public static NpQuotesBlockBuilder Create => new NpQuotesBlockBuilder();

        public NpQuotesBlockBuilder Default()
        {
            _npQuotesBlock = new NpQuotesBlock();
            return this;
        }

        public NpQuotesBlockBuilder WithTitle(string value) =>
            UpdateContent(x => x.Title = value);

        public NpQuotesBlockBuilder WithQuotes(ContentAreaBuilder value) =>
            UpdateContent(x => x.Quotes = value);

        private NpQuotesBlockBuilder UpdateContent(Action<NpQuotesBlock> updateAction)
        {
            updateAction(_npQuotesBlock);
            return this;
        }

        public static implicit operator NpQuotesBlock(NpQuotesBlockBuilder builder) =>
            builder._npQuotesBlock;
    }
}
