﻿using System;
using System.Collections.Generic;
using EPiServer.Core;
using Forskningsradet.Core.Models.ContentModels.Blocks;

namespace Forskningsradet.UnitTests.Builders
{
    public class NpImageWithQuoteBlockBuilder
    {
        private NpImageWithQuoteBlock _npImageWithQuoteBlock;

        public static NpImageWithQuoteBlockBuilder Create => new NpImageWithQuoteBlockBuilder();

        public NpImageWithQuoteBlockBuilder Default()
        {
            _npImageWithQuoteBlock = new NpImageWithQuoteBlock
            {
                Image = new ContentReference(1),
                Text = "Man only suffers because he takes seriously what the gods make for fun.",
                QuoteBy = "Alan Watts",
                AddQuoteDash = true
            };
            return this;
        }

        public NpImageWithQuoteBlockBuilder WithText(string value) =>
            UpdateContent(x => x.Text = value);

        public NpImageWithQuoteBlockBuilder WithQuoteBy(string value) =>
            UpdateContent(x => x.QuoteBy = value);

        public NpImageWithQuoteBlockBuilder WithAddQuoteDash(bool value) =>
            UpdateContent(x => x.AddQuoteDash = value);

        private NpImageWithQuoteBlockBuilder UpdateContent(Action<NpImageWithQuoteBlock> updateAction)
        {
            updateAction(_npImageWithQuoteBlock);
            return this;
        }

        public List<NpImageWithQuoteBlock> AsList() =>
            new List<NpImageWithQuoteBlock> { _npImageWithQuoteBlock };

        public static implicit operator NpImageWithQuoteBlock(NpImageWithQuoteBlockBuilder builder) =>
            builder._npImageWithQuoteBlock;
    }
}
