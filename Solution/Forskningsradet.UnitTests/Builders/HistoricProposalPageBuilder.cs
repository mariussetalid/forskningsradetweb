﻿using System;
using EPiServer.Core;
using Forskningsradet.Core.Models.ContentModels.Pages;

namespace Forskningsradet.UnitTests.Builders
{
    public class HistoricProposalPageBuilder
    {
        private readonly HistoricProposalPage _historicProposalPage = new TestableHistoricProposalPage();

        public static HistoricProposalPageBuilder Create => new HistoricProposalPageBuilder();

        public HistoricProposalPageBuilder Default() => this;

        public HistoricProposalPageBuilder WithPageName(string pageName)
        {
            _historicProposalPage.PageName = pageName;
            return this;
        }

        public HistoricProposalPageBuilder WithAttachment(ContentReference attachment)
        {
            _historicProposalPage.Attachment = attachment;
            return this;
        }

        public HistoricProposalPageBuilder WithSubtitle(string subtitle)
        {
            _historicProposalPage.Subtitle = subtitle;
            return this;
        }

        public static implicit operator HistoricProposalPage(HistoricProposalPageBuilder builder) =>
            builder._historicProposalPage;
    }
}