﻿using System.Collections.Generic;
using Forskningsradet.Core.Models.ApiModels.Evurdering;

namespace Forskningsradet.UnitTests.Builders
{
    public class SubjectBuilder
    {
        private Subject _subject;

        public static SubjectBuilder Create => new SubjectBuilder();

        public SubjectBuilder Default()
        {
            _subject = new Subject
            {
                Code = "UTLPSHELSE",
                NameNorwegian = "Helse",
                NameEnglish = "Health"
            };
            return this;
        }

        public SubjectBuilder WithCode(string code)
        {
            _subject.Code = code;
            return this;
        }

        public SubjectBuilder WithName(string nameNorwegian, string nameEnglish)
        {
            _subject.NameNorwegian = nameNorwegian;
            _subject.NameEnglish = nameEnglish;
            return this;
        }

        public SubjectBuilder WithTitle(string title)
        {
            _subject.Title = title;
            return this;
        }

        public SubjectBuilder WithSubSubjects()
        {
            _subject.SubSubjects = new List<Subject>
            {
                Create.Default()
                    .WithCode("HELSEVEL")
                    .WithTitle("Helse-, omsorgs- og velferdstjenester")
            };
            return this;
        }

        public SubjectBuilder WithSubSubjects(List<Subject> subSubjects)
        {
            _subject.SubSubjects = subSubjects;
            return this;
        }

        public static implicit operator Subject(SubjectBuilder builder) =>
            builder._subject;

        public static explicit operator List<Subject>(SubjectBuilder builder) =>
            new List<Subject> { builder._subject };
    }
}
