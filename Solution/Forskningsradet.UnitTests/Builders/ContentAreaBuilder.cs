﻿using System.Collections.Generic;
using EPiServer.Core;

namespace Forskningsradet.UnitTests.Builders
{
    public class ContentAreaBuilder
    {
        private readonly ContentArea _contentArea = new TestableContentArea();

        public static ContentAreaBuilder Create => new ContentAreaBuilder();

        public ContentAreaBuilder Default() => this;

        public ContentAreaBuilder WithDefaultFilteredItems()
        {
            ((TestableContentArea)_contentArea).SettableFilteredItemsForTest = new[]
            {
                new ContentAreaItem()
            };
            return this;
        }

        public ContentAreaBuilder WithDefaultFilteredItems(int count)
        {
            var items = new List<ContentAreaItem>();
            for (int i = 0; i < count; i++)
                items.Add(new ContentAreaItem());

            ((TestableContentArea)_contentArea).SettableFilteredItemsForTest = items;
            return this;
        }

        public ContentAreaBuilder WithFilteredItems(params ContentAreaItem[] filteredItems)
        {
            ((TestableContentArea)_contentArea).SettableFilteredItemsForTest = filteredItems;
            return this;
        }

        public static implicit operator ContentArea(ContentAreaBuilder builder) =>
            builder._contentArea;
    }
}
