﻿using System.Globalization;
using EPiServer.Core;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ContentModels.Pages;

namespace Forskningsradet.UnitTests.Builders
{
    public class ApplicationTypePageBuilder
    {
        private readonly ApplicationTypePage _applicationTypePage = new TestableApplicationTypePage();

        public static ApplicationTypePageBuilder Create => new ApplicationTypePageBuilder();

        public ApplicationTypePageBuilder Default()
        {
            _applicationTypePage.Language = new CultureInfo(LanguageConstants.MasterLanguageBranch);
            return this;
        }

        public ApplicationTypePageBuilder WithName(string name)
        {
            _applicationTypePage.Name = name;
            return this;
        }

        public ApplicationTypePageBuilder WithPageLink(PageReference pageLink)
        {
            _applicationTypePage.PageLink = pageLink;
            return this;
        }

        public static implicit operator ApplicationTypePage(ApplicationTypePageBuilder builder) =>
            builder._applicationTypePage;
    }
}
