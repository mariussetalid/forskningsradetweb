﻿using System.Collections.Generic;
using EPiServer.Core;
using Forskningsradet.Core.Models.RequestModels;

namespace Forskningsradet.UnitTests.Builders
{
    public class NodeBuilder
    {
        private ContentReference _contentReference;
        private int _depth;
        private List<Node> _children;
        private Node _parentNode;

        public static NodeBuilder Create => new NodeBuilder();

        public NodeBuilder Default()
        {
            _contentReference = new ContentReference(1);
            _depth = 0;
            _children = new List<Node>();
            _parentNode = null;

            return this;
        }

        public NodeBuilder WithDepth(int depth)
        {
            _depth = depth;

            return this;
        }

        public NodeBuilder WithId(int id)
        {
            _contentReference = new ContentReference(id);

            return this;
        }

        public NodeBuilder WithParentNode(Node node)
        {
            _parentNode = node;

            return this;
        }

        public NodeBuilder WithChildren(List<Node> children)
        {
            _children = children;

            return this;
        }

        public static implicit operator Node(NodeBuilder builder)
        {
            var node = new Node(builder._parentNode, builder._contentReference, builder._depth);
            foreach (var child in builder._children)
            {
                node.AddChild(child);
            }

            return node;
        }
    }
}
