﻿using System;
using System.Globalization;
using EPiServer;
using EPiServer.Core;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ContentModels.Pages;

namespace Forskningsradet.UnitTests.Builders
{
    public class SatelliteFrontPageBuilder
    {
        private readonly SatelliteFrontPage _page = new TestableSatelliteFrontPage();

        public static SatelliteFrontPageBuilder Create => new SatelliteFrontPageBuilder();

        public SatelliteFrontPageBuilder Default()
        {
            _page.Language = new CultureInfo(LanguageConstants.MasterLanguageBranch);
            _page.Category = new CategoryList();
            return this;
        }

        public SatelliteFrontPageBuilder WithLoginUrl(Url value) =>
            UpdatePage(x => x.LoginUrl = value);

        public SatelliteFrontPageBuilder WithSearchPage(PageReference value) =>
            UpdatePage(x => x.SearchPage = value);

        public SatelliteFrontPageBuilder WithMainMenuRoot(PageReference value) =>
            UpdatePage(x => x.MainMenuRoot = value);

        public SatelliteFrontPageBuilder WithShowTopBanner(bool value) =>
            UpdatePage(x => x.ShowTopBanner = value);

        private SatelliteFrontPageBuilder UpdatePage(Action<SatelliteFrontPage> updateAction)
        {
            updateAction(_page);
            return this;
        }

        public static implicit operator SatelliteFrontPage(SatelliteFrontPageBuilder builder) =>
            builder._page;
    }
}
