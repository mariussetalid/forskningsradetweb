﻿using System;
using System.Globalization;
using EPiServer;
using EPiServer.Core;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ContentModels.Pages;

namespace Forskningsradet.UnitTests.Builders
{
    public class EventPageBuilder
    {
        private readonly EventPage _eventPage = new TestableEventPage();

        public static EventPageBuilder Create => new EventPageBuilder();

        public EventPageBuilder Default()
        {
            _eventPage.Language = new CultureInfo(LanguageConstants.MasterLanguageBranch);
            return this;
        }

        public EventPageBuilder WithPageLink(PageReference pageLink)
        {
            _eventPage.ContentLink = pageLink;
            _eventPage.PageLink = pageLink;
            return this;
        }

        public EventPageBuilder WithListTitle(string listTitle)
        {
            _eventPage.ListTitle = listTitle;
            return this;
        }

        public EventPageBuilder WithListIntro(string listIntro)
        {
            _eventPage.ListIntro = listIntro;
            return this;
        }

        public EventPageBuilder WithLocation(string location)
        {
            _eventPage.Location = location;
            return this;
        }

        public EventPageBuilder WithStartDate(DateTime? startdate)
        {
            _eventPage.StartDate = startdate;
            return this;
        }

        public EventPageBuilder WithEndDate(DateTime? endDate)
        {
            _eventPage.EndDate = endDate;
            return this;
        }

        public EventPageBuilder WithEventData(EventDataBlock eventData)
        {
            _eventPage.EventData = eventData;
            return this;
        }

        public EventPageBuilder WithCanceled(bool canceled)
        {
            _eventPage.Canceled = canceled;
            return this;
        }

        public EventPageBuilder WithRegistrationLink(Url url)
        {
            _eventPage.RegistrationLink = url;
            return this;
        }

        public EventPageBuilder WithName(string name)
        {
            _eventPage.Name = name;
            return this;
        }

        public static implicit operator EventPage(EventPageBuilder builder) =>
            builder._eventPage;
    }
}
