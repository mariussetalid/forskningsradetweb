﻿using System;
using EPiServer.Core;
using Forskningsradet.Core.Models.ContentModels.Blocks;

namespace Forskningsradet.UnitTests.Builders
{
    public class CampaignBlockBuilder
    {
        private CampaignBlock _campaignBlock;

        public static CampaignBlockBuilder Create => new CampaignBlockBuilder();

        public CampaignBlockBuilder Default()
        {
            _campaignBlock = new CampaignBlock();
            return this;
        }

        public CampaignBlockBuilder WithImage(ContentReference value) =>
            UpdateContent(x => x.Image = value);

        public CampaignBlockBuilder WithTitle(string value) =>
            UpdateContent(x => x.Title = value);

        public CampaignBlockBuilder WithButton(EditLinkBlock value) =>
            UpdateContent(x => x.Button = value);

        public CampaignBlockBuilder WithStyle(CampaignBlockStyle value) =>
            UpdateContent(x => x.Style = value);

        private CampaignBlockBuilder UpdateContent(Action<CampaignBlock> updateAction)
        {
            updateAction(_campaignBlock);
            return this;
        }

        public static implicit operator CampaignBlock(CampaignBlockBuilder builder) =>
            builder._campaignBlock;
    }
}

