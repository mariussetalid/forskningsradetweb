﻿using EPiServer.Core;
using Forskningsradet.Core.Models.ContentModels.Blocks;

namespace Forskningsradet.UnitTests.Builders
{
    public class SubjectPriorityBlockBuilder
    {
        private TestableSubjectPriorityBlock _subjectBlock;

        public static SubjectPriorityBlockBuilder Create => new SubjectPriorityBlockBuilder();

        public SubjectPriorityBlockBuilder Default()
        {
            _subjectBlock = new TestableSubjectPriorityBlock
            {
                ContentLink = new ContentReference(1)
            };

            return this;
        }

        public SubjectPriorityBlockBuilder WithId(int id)
        {
            _subjectBlock.ContentLink = new ContentReference(id);
            return this;
        }

        public SubjectPriorityBlockBuilder WithTitle(string title)
        {
            _subjectBlock.Title = title;
            return this;
        }

        public SubjectPriorityBlockBuilder WithRightBlockArea(ContentAreaBuilder contentAreaBuilder)
        {
            _subjectBlock.RightBlockArea = contentAreaBuilder;
            return this;
        }

        public SubjectPriorityBlockBuilder WithPortfolioContent(ContentAreaBuilder contentAreaBuilder)
        {
            _subjectBlock.PortfolioContent = contentAreaBuilder;
            return this;
        }

        public SubjectPriorityBlockBuilder WithText(XhtmlString text)
        {
            _subjectBlock.Text = text;
            return this;
        }

        public static implicit operator SubjectPriorityBlock(SubjectPriorityBlockBuilder builder) =>
            builder._subjectBlock;
    }
}
