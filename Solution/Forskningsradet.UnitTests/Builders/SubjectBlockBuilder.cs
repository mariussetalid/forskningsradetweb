﻿using EPiServer.Core;
using Forskningsradet.Core.Models.ContentModels.Blocks;

namespace Forskningsradet.UnitTests.Builders
{
    public class SubjectBlockBuilder
    {
        private SubjectBlock _subjectBlock;

        public static SubjectBlockBuilder Create => new SubjectBlockBuilder();

        public SubjectBlockBuilder Default()
        {
            _subjectBlock = new SubjectBlock();
            return this;
        }

        public SubjectBlockBuilder WithPriorities(ContentAreaBuilder contentAreaBuilder)
        {
            _subjectBlock.Priorities = contentAreaBuilder;
            return this;
        }

        public SubjectBlockBuilder WithSubjectName(string subjectName)
        {
            _subjectBlock.SubjectName = subjectName;
            return this;
        }

        public SubjectBlockBuilder WithDescription(string description)
        {
            _subjectBlock.Description = description;
            return this;
        }

        public SubjectBlockBuilder WithSubjectCode(string subjectCode)
        {
            _subjectBlock.SubjectCode = subjectCode;
            return this;
        }

        public SubjectBlockBuilder WithSubjectChildren(string subjectChildren)
        {
            _subjectBlock.SubjectChildren = subjectChildren;
            return this;
        }

        public SubjectBlockBuilder WithSubjectChildrenDescription(XhtmlString subjectChildrenDescription)
        {
            _subjectBlock.SubjectChildrenDescription = subjectChildrenDescription;
            return this;
        }

        public static implicit operator SubjectBlock(SubjectBlockBuilder builder) =>
            builder._subjectBlock;
    }
}
