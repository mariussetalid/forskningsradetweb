﻿using System;
using System.Globalization;
using EPiServer.Core;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ContentModels.Pages;

namespace Forskningsradet.UnitTests.Builders
{
    public class NewsArticlePageBuilder
    {
        private readonly NewsArticlePage _newsArticlePage = new TestableNewsArticlePage();

        public static NewsArticlePageBuilder Create => new NewsArticlePageBuilder();

        public NewsArticlePageBuilder Default()
        {
            _newsArticlePage.Language = new CultureInfo(LanguageConstants.MasterLanguageBranch);
            _newsArticlePage.Category = new CategoryList();
            return this;
        }

        public NewsArticlePageBuilder WithContentLink(ContentReference value) =>
            UpdateValue(x => x.ContentLink = value);

        public NewsArticlePageBuilder WithStartPublish(DateTime? value) =>
            UpdateValue(x => x.StartPublish = value);

        public NewsArticlePageBuilder WithChanged(DateTime value) =>
            UpdateValue(x => x.Changed = value);

        public NewsArticlePageBuilder WithListImage(ContentReference value) =>
            UpdateValue(x => x.ListImage = value);

        public NewsArticlePageBuilder WithListIntro(string value) =>
            UpdateValue(x => x.ListIntro = value);

        public NewsArticlePageBuilder WithListTitle(string value) =>
            UpdateValue(x => x.ListTitle = value);

        private NewsArticlePageBuilder UpdateValue(Action<NewsArticlePage> updateAction)
        {
            updateAction(_newsArticlePage);
            return this;
        }

        public static implicit operator NewsArticlePage(NewsArticlePageBuilder builder) =>
            builder._newsArticlePage;
    }
}
