﻿using Forskningsradet.Core.Models.ContentModels.Blocks;

namespace Forskningsradet.UnitTests.Builders
{
    public class LinkListContainerBlockBuilder
    {
        private LinkListContainerBlock linkListContainerBlock;

        public static LinkListContainerBlockBuilder Create => new LinkListContainerBlockBuilder();

        public LinkListContainerBlockBuilder Default()
        {
            linkListContainerBlock = new LinkListContainerBlock();
            return this;
        }

        public LinkListContainerBlockBuilder WithTitle(string title)
        {
            linkListContainerBlock.Title = title;
            return this;
        }

        public LinkListContainerBlockBuilder WithSingleColumn(bool singleColumn)
        {
            linkListContainerBlock.SingleColumn = singleColumn;
            return this;
        }

        public LinkListContainerBlockBuilder WithLinkList(ContentAreaBuilder linkList)
        {
            linkListContainerBlock.LinkList = linkList;
            return this;
        }

        public LinkListContainerBlockBuilder WithContactList(ContentAreaBuilder contactList)
        {
            linkListContainerBlock.ContactList = contactList;
            return this;
        }

        public LinkListContainerBlockBuilder WithLinkWithTextList(ContentAreaBuilder linkWithTextList)
        {
            linkListContainerBlock.LinkWithTextList = linkWithTextList;
            return this;
        }

        public static implicit operator LinkListContainerBlock(LinkListContainerBlockBuilder builder) =>
            builder.linkListContainerBlock;
    }
}
