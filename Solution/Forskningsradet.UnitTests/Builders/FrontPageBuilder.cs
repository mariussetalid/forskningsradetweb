﻿using System;
using System.Globalization;
using EPiServer;
using EPiServer.Core;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ContentModels.Pages;

namespace Forskningsradet.UnitTests.Builders
{
    public class FrontPageBuilder
    {
        private readonly FrontPage _page = new TestableFrontPage();

        public static FrontPageBuilder Create => new FrontPageBuilder();

        public FrontPageBuilder Default()
        {
            _page.Language = new CultureInfo(LanguageConstants.MasterLanguageBranch);
            _page.Category = new CategoryList();
            return this;
        }

        public FrontPageBuilder WithLoginUrl(Url value) =>
            UpdatePage(x => x.LoginUrl = value);

        public FrontPageBuilder WithSearchPage(PageReference value) =>
            UpdatePage(x => x.SearchPage = value);

        public FrontPageBuilder WithMainMenuRoot(PageReference value) =>
            UpdatePage(x => x.MainMenuRoot = value);

        private FrontPageBuilder UpdatePage(Action<FrontPage> updateAction)
        {
            updateAction(_page);
            return this;
        }

        public static implicit operator FrontPage(FrontPageBuilder builder) =>
            builder._page;
    }
}
