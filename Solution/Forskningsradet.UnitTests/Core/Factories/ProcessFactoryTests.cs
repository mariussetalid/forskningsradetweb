using EPiServer.Core;
using FakeItEasy;
using Forskningsradet.Common.Logging;
using Forskningsradet.Core.Factories;
using Forskningsradet.Core.MessageBrokers;
using Forskningsradet.Core.Models.ApiModels.Evurdering;
using Forskningsradet.Core.Processes;
using Forskningsradet.Core.Processes.Contexts;
using Forskningsradet.Core.Processes.Providers;
using Forskningsradet.Core.Processes.Steps.ApplicationResults;
using Forskningsradet.Core.Processes.Steps.Archiving;
using Forskningsradet.Core.Processes.Steps.PrepareForArchiving;
using Forskningsradet.Core.Processes.Steps.Proposals;
using Forskningsradet.Core.Processes.Steps.UpdatePressReleases;
using Forskningsradet.Core.Repositories.Contracts;
using Forskningsradet.Core.Services;
using Forskningsradet.Core.Services.Contracts;
using Forskningsradet.ServiceAgents.Archiving;
using Forskningsradet.ServiceAgents.Contracts;
using Forskningsradet.UnitTests.Builders;
using Xunit;

namespace Forskningsradet.UnitTests.Core.Factories
{
    public class ProcessFactoryTests
    {
        private readonly IPageRepository _pageRepository;
        private readonly ProcessFactory _factory;

        public ProcessFactoryTests()
        {
            _pageRepository = A.Fake<IPageRepository>();
            _factory = new ProcessFactory(
                _pageRepository,
                A.Fake<IBlockRepository>(),
                A.Fake<IProposalValidationService>(),
                A.Fake<IProposalPageProvider>(),
                A.Fake<IPressReleasePageProvider>(),
                A.Fake<ILocalizationProvider>(),
                A.Fake<IEpiPageVersionProvider>(),
                A.Fake<IArchiveMessageBroker>(),
                A.Fake<IHtmlToPdfProvider>(),
                A.Fake<IArchivingServiceAgent>(),
                A.Fake<INtbServiceAgent>());
        }

        [Fact]
        public void ProposalContext_WithPlannedState_ShouldReturnProcessForState()
        {
            var result = _factory.GetProcessForContext(GetProposalContextForState(ProposalState.Planned)) as ProcessRunner<ProposalContext>;

            Assert.NotNull(result);
            Assert.True(result.HasStep(typeof(UpdateProposalValues)));
            Assert.True(result.HasStep(typeof(UpdateSubjects)));
            Assert.True(result.HasStep(typeof(UpdateAssessmentCriteria)));
            Assert.True(result.HasStep(typeof(UpdateTimeline)));
            Assert.True(result.HasStep(typeof(HandleInactiveState)));
            Assert.True(result.HasStep(typeof(PublishProposal)));
            Assert.Equal(6, result.StepCount);
        }

        [Fact]
        public void ProposalContext_WithActiveState_ShouldReturnProcessForState()
        {
            var result = _factory.GetProcessForContext(GetProposalContextForState(ProposalState.Active)) as ProcessRunner<ProposalContext>;

            Assert.NotNull(result);
            Assert.True(result.HasStep(typeof(UpdateProposalValues)));
            Assert.True(result.HasStep(typeof(UpdateSubjects)));
            Assert.True(result.HasStep(typeof(UpdateAssessmentCriteria)));
            Assert.True(result.HasStep(typeof(UpdateTimeline)));
            Assert.True(result.HasStep(typeof(HandleActiveState)));
            Assert.True(result.HasStep(typeof(PublishProposal)));
            Assert.Equal(6, result.StepCount);
        }

        [Fact]
        public void ProposalContext_WithCancelledState_ShouldReturnProcessForState()
        {
            var result = _factory.GetProcessForContext(GetProposalContextForState(ProposalState.Cancelled)) as ProcessRunner<ProposalContext>;

            Assert.NotNull(result);
            Assert.True(result.HasStep(typeof(UpdateProposalValues)));
            Assert.True(result.HasStep(typeof(UpdateSubjects)));
            Assert.True(result.HasStep(typeof(UpdateAssessmentCriteria)));
            Assert.True(result.HasStep(typeof(UpdateTimeline)));
            Assert.True(result.HasStep(typeof(HandleActiveState)));
            Assert.True(result.HasStep(typeof(CancelProposal)));
            Assert.True(result.HasStep(typeof(PublishProposal)));
            Assert.Equal(7, result.StepCount);
        }

        [Fact]
        public void ProposalContext_WithCompletedState_ShouldReturnProcessForState()
        {
            var context = ArrangeCompletedState(true);

            var result = _factory.GetProcessForContext(context) as ProcessRunner<ProposalContext>;

            Assert.NotNull(result);
            Assert.True(result.HasStep(typeof(UpdateProposalValues)));
            Assert.True(result.HasStep(typeof(UpdateSubjects)));
            Assert.True(result.HasStep(typeof(UpdateAssessmentCriteria)));
            Assert.True(result.HasStep(typeof(UpdateTimeline)));
            Assert.True(result.HasStep(typeof(HandleInactiveState)));
            Assert.True(result.HasStep(typeof(PublishProposal)));
            Assert.Equal(6, result.StepCount);
        }

        [Fact]
        public void ProposalContext_WithCompletedStateAndOriginalPageIsDraftAndActive_ShouldReturnProcessWithHandleActiveDraftStep()
        {
            var context = ArrangeCompletedState(false);

            var result = _factory.GetProcessForContext(context) as ProcessRunner<ProposalContext>;

            Assert.NotNull(result);
            Assert.True(result.HasStep(typeof(HandleActiveDraft)));
            Assert.True(result.HasStep(typeof(UpdateProposalValues)));
            Assert.True(result.HasStep(typeof(UpdateSubjects)));
            Assert.True(result.HasStep(typeof(UpdateAssessmentCriteria)));
            Assert.True(result.HasStep(typeof(UpdateTimeline)));
            Assert.True(result.HasStep(typeof(HandleInactiveState)));
            Assert.True(result.HasStep(typeof(PublishProposal)));
            Assert.Equal(7, result.StepCount);
        }

        [Fact]
        public void ProposalContext_WithCompletedStateAndOriginalPageIsDraftAndActive_ShouldReturnProcessWithHandleActiveDraftAsFirstStep()
        {
            var context = ArrangeCompletedState(false);
            var result = _factory.GetProcessForContext(context) as ProcessRunner<ProposalContext>;

            Assert.NotNull(result);
            Assert.Equal(typeof(HandleActiveDraft), result.GetStep(0).GetType());
        }

        [Fact]
        public void ApplicationResultContext_ShouldReturnProcess()
        {
            var result = _factory.GetProcessForContext(new ApplicationResultContext(A.Fake<IIntegrationLogger>())) as ProcessRunner<ApplicationResultContext>;

            Assert.NotNull(result);
            Assert.True(result.HasStep(typeof(PrepareApplicationResultUpdate)));
            Assert.True(result.HasStep(typeof(UpdateApplicationResultPage)));
            Assert.True(result.HasStep(typeof(SaveChanges)));
            Assert.True(result.HasStep(typeof(ConnectResultToProposal)));
        }

        [Fact]
        public void PrepareForAchivingContext_ShouldReturnProcess()
        {
            var result = _factory.GetProcessForContext(new PrepareForArchivingContext(A.Fake<IIntegrationLogger>())) as ProcessRunner<PrepareForArchivingContext>;

            Assert.NotNull(result);
            Assert.True(result.HasStep(typeof(ValidatePageForArchiving)));
            Assert.True(result.HasStep(typeof(PopulateMetaData)));
            Assert.True(result.HasStep(typeof(GenerateArchivedPagePdf)));
            Assert.True(result.HasStep(typeof(SavePageForArchiving)));
        }

        [Fact]
        public void ArchivingContext_ShouldReturnProcess()
        {
            var result = _factory.GetProcessForContext(new ArchivingContext(A.Fake<IIntegrationLogger>())) as ProcessRunner<ArchivingContext>;

            Assert.NotNull(result);
            Assert.True(result.HasStep(typeof(GetPagesPreparedForArchiving)));
            Assert.True(result.HasStep(typeof(DispatchPagesToArchive)));
            Assert.True(result.HasStep(typeof(ResendFailedMessages)));
        }

        [Fact]
        public void UpdatePressReleasesContext_ShouldReturnProcess()
        {
            var result = _factory.GetProcessForContext(new UpdatePressReleasesContext(A.Fake<IIntegrationLogger>())) as ProcessRunner<UpdatePressReleasesContext>;

            Assert.NotNull(result);
            Assert.True(result.HasStep(typeof(FetchPressReleases)));
            Assert.True(result.HasStep(typeof(CreateOrUpdatePressReleases)));
        }

        private static ProposalContext GetProposalContextForState(ProposalState state) =>
            new ProposalContext(A.Fake<IIntegrationLogger>())
            {
                Proposal = ProposalBuilder.Create.Default(ProgramBuilder.Create.Default(), ApplicationTypeBuilder.Create.Default())
                    .WithStatus(state)
            };

        private ProposalContext ArrangeCompletedState(bool isOriginalPagePublished)
        {
            var context = GetProposalContextForState(ProposalState.Completed);
            context.OriginalPage = ProposalPageBuilder.Create.Default()
                    .WithProposalState(ProposalState.Active);
            A.CallTo(() => _pageRepository.IsPublished(A<PageData>._))
                .Returns(isOriginalPagePublished);
            return context;
        }
    }
}