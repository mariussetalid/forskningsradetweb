using EPiServer;
using EPiServer.Core;
using EPiServer.Web.Routing;
using FakeItEasy;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.ReactModels;
using Forskningsradet.Core.Repositories.Contracts;
using Forskningsradet.Core.Services;
using Forskningsradet.Core.Services.Contracts;
using Forskningsradet.Core.ViewModelMappers.Blocks;
using Xunit;

namespace Forskningsradet.UnitTests.Core.ViewModelMappers.Blocks
{
    public class FrontPageHeaderBlockViewModelMapperTests
    {
        private readonly IPageRepository _pageRepository;
        private readonly IContentLoader _contentLoader;
        private readonly IUrlResolver _urlResolver;
        private readonly IUrlCheckingService _urlCheckingService;
        private readonly IFluidImageReactModelBuilder _fluidImageReactModelBuilder;
        private readonly ILocalizationProvider _localizationProvider;
        private readonly FrontPageHeaderBlockViewModelMapper _mapper;

        public FrontPageHeaderBlockViewModelMapperTests()
        {
            _pageRepository = A.Fake<IPageRepository>();
            _contentLoader = A.Fake<IContentLoader>();
            _urlResolver = A.Fake<IUrlResolver>();
            _urlCheckingService = A.Fake<IUrlCheckingService>();
            _fluidImageReactModelBuilder = A.Fake<IFluidImageReactModelBuilder>();
            _localizationProvider = A.Fake<ILocalizationProvider>();
            _mapper = new FrontPageHeaderBlockViewModelMapper(
                _pageRepository,
                _contentLoader,
                _urlResolver,
                _urlCheckingService,
                _fluidImageReactModelBuilder,
                _localizationProvider);
        }

        [Fact]
        public void WithDefaultValues_ShouldMapFields()
        {
            var expectedImage = new FluidImage
            {
                Alt = "image alt",
                Src = "image url"
            };

            A.CallTo(() => _fluidImageReactModelBuilder.BuildReactModel(A<ContentReference>._, A<string>._))
                .Returns(expectedImage);
            A.CallTo(() => _pageRepository.GetCurrentStartPage())
                .Returns(A.Fake<FrontPage>());

            var block = A.Fake<FrontPageHeaderBlock>();
            block.Text = "text";

            var result = (FrontpageHeader)_mapper.GetPartialViewModel(block).Model;

            Assert.Equal(block.Text, result.Text);
            Assert.Equal(expectedImage.Alt, result.Image.Alt);
            Assert.Equal(expectedImage.Src, result.Image.Src);
            Assert.Equal(FrontpageHeader_Theme.GreyBlue, result.Theme);
        }

        [Theory]
        [InlineData(FrontPageHeaderBlockTheme.DarkBlue, FrontpageHeader_Theme.DarkBlue)]
        [InlineData(FrontPageHeaderBlockTheme.GreyBlue, FrontpageHeader_Theme.GreyBlue)]
        [InlineData(FrontPageHeaderBlockTheme.Purple, FrontpageHeader_Theme.Purple)]
        public void WithTextStyle_ShouldMapCorrectStyle(FrontPageHeaderBlockTheme theme, FrontpageHeader_Theme expectedTheme)
        {
            var block = new FrontPageHeaderBlock
            {
                Theme = theme
            };

            var result = (FrontpageHeader)_mapper.GetPartialViewModel(block).Model;

            Assert.Equal(expectedTheme, result.Theme);
        }

        private void ArrangeTranslation(string expected, string label)
        {
            A.CallTo(() => _localizationProvider.GetLabel(nameof(FrontPage), label))
                .Returns(expected);
        }
    }
}