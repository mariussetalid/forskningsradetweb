using FakeItEasy;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Facades.Contracts;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ReactModels;
using Forskningsradet.Core.ViewModelMappers.Blocks;
using Xunit;

namespace Forskningsradet.UnitTests.Core.ViewModelMappers.Blocks
{
    public class IframeBlockViewModelMapperTests
    {
        const string ValidSrcStart = EmbedSourceConstants.Iframe.ValidSrcStart;

        private readonly IframeBlockViewModelMapper _mapper;
        private readonly IPageEditingAdapter _pageEditingAdapter;

        public IframeBlockViewModelMapperTests()
        {
            _pageEditingAdapter = A.Fake<IPageEditingAdapter>();
            _mapper = new IframeBlockViewModelMapper(_pageEditingAdapter);
        }

        [Fact]
        public void GetViewModel_WhenAllFieldsProvided_ShouldMapFields()
        {
            var expectedSrc = $"{ValidSrcStart}";
            var block = new IframeBlock
            {
                AlternativeText = "Alt",
                Src = ValidSrcStart,
                Width = 20,
                Height = 30
            };

            var result = (EmbedBlock)_mapper.GetPartialViewModel(block).Model;

            Assert.Equal(block.AlternativeText, result.Title);
            Assert.StartsWith(block.Src, result.Src);
            Assert.Equal(expectedSrc, result.Src);
            Assert.Equal(block.Width, result.Width);
            Assert.Equal(block.Height, result.Height);
        }

        [Theory]
        [InlineData(null)]
        [InlineData(" ")]
        [InlineData("Something")]
        [InlineData("http://")]
        public void GetViewModel_WhenSrcIsInvalid_ShouldNotMapSrc(string invalidSrc)
        {
            var block = new IframeBlock
            {
                Src = invalidSrc
            };

            var result = (EmbedBlock)_mapper.GetPartialViewModel(block).Model;

            Assert.Null(result.Src);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public void GetViewModel_WhenDimensionsAreInvalid_ShouldNotMapDimensions(int invalidNumber)
        {
            var block = new IframeBlock
            {
                Width = invalidNumber,
                Height = invalidNumber
            };

            var result = (EmbedBlock)_mapper.GetPartialViewModel(block).Model;

            Assert.Null(result.Width);
            Assert.Null(result.Height);
        }

        [Fact]
        public void GetViewModel_WhenPageIsInEditMode_ShouldMapOnPageEditing()
        {
            A.CallTo(() => _pageEditingAdapter.PageIsInEditMode()).Returns(true);

            var result = (EmbedBlock)_mapper.GetPartialViewModel(new IframeBlock()).Model;

            Assert.NotNull(result.OnPageEditing);
        }
    }
}