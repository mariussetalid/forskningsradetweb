using System;
using System.Collections.Generic;
using System.Linq;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.Web.Routing;
using FakeItEasy;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Extensions;
using Forskningsradet.Core.Facades.Contracts;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ContentModels.Media;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.Enums;
using Forskningsradet.Core.Models.QueryParameters;
using Forskningsradet.Core.Models.RequestModels;
using Forskningsradet.Core.Models.SearchModels;
using Forskningsradet.Core.Services;
using Forskningsradet.Core.ViewModelMappers.Blocks;
using Forskningsradet.UnitTests.Builders;
using Xunit;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.UnitTests.Core.ViewModelMappers.Blocks
{
    public class RelatedContentListBlockViewModelMapperTests
    {
        private readonly IUrlResolver _urlResolver;
        private readonly IContentListService _contentListService;
        private readonly IContentLoader _contentLoader;
        private readonly IFluidImageReactModelBuilder _fluidImageReactModelBuilder;
        private readonly IPublicationReactModelBuilder _publicationReactModelBuilder;
        private readonly IContentTypeRepository<PageType> _pageTypeRepository;
        private readonly CategoryRepository _categoryRepository;
        private readonly ILocalizationProvider _localizationProvider;
        private readonly IPageEditingAdapter _pageEditingAdapter;
        private readonly IPageRouteHelper _pageRouteHelper;
        private readonly RelatedContentListBlockViewModelMapper _mapper;

        public RelatedContentListBlockViewModelMapperTests()
        {
            _urlResolver = A.Fake<IUrlResolver>();
            _contentListService = A.Fake<IContentListService>();
            _contentLoader = A.Fake<IContentLoader>();
            _fluidImageReactModelBuilder = A.Fake<IFluidImageReactModelBuilder>();
            _publicationReactModelBuilder = A.Fake<IPublicationReactModelBuilder>();
            _pageTypeRepository = A.Fake<IContentTypeRepository<PageType>>();
            _categoryRepository = A.Fake<CategoryRepository>();
            _localizationProvider = A.Fake<ILocalizationProvider>();
            _pageEditingAdapter = A.Fake<IPageEditingAdapter>();
            _pageRouteHelper = A.Fake<IPageRouteHelper>();
            _mapper = new RelatedContentListBlockViewModelMapper(_urlResolver, _contentListService, _contentLoader, _fluidImageReactModelBuilder, _publicationReactModelBuilder, _pageTypeRepository, _categoryRepository, _localizationProvider, _pageEditingAdapter, _pageRouteHelper);
        }

        [Fact]
        public void GetViewModel_WhenAllFieldsProvided_ShouldMapFields()
        {
            const string expectedUrl = "url";
            const string expectedEmptyListText = "empty";
            ArrangeGetUrl(expectedUrl);
            ArrangeTranslation(expectedEmptyListText, "EmptyListLabel");

            var block = new RelatedContentListBlock
            {
                Title = "T",
                Button = new EditLinkBlock
                {
                    Text = "L",
                    Link = new Url("u")
                }
            };

            var result = (ReactModels.RelatedArticles)_mapper.GetPartialViewModel(block, new RenderOptionQueryParameter { RenderWidthOption = RenderWidthOption.None }).Model;

            Assert.Equal(block.Title, result.RelatedContent.Title);
            Assert.Equal(block.Button.Text, result.RelatedContent.Link.Text);
            Assert.Equal(expectedUrl, result.RelatedContent.Link.Url);
            Assert.Equal(expectedEmptyListText, result.RelatedContent.EmptyList.Text);
        }

        [Fact]
        public void GetViewModel_WhenPagesAreReturned_ShouldMapAllPages()
        {
            ArrangeSearch(new List<EditorialPage>
                {
                    NewsArticlePageBuilder.Create.Default(),
                    NewsArticlePageBuilder.Create.Default(),
                    NewsArticlePageBuilder.Create.Default()
                });

            var result = (ReactModels.RelatedArticles)_mapper.GetPartialViewModel(new RelatedContentListBlock(), new RenderOptionQueryParameter { RenderWidthOption = RenderWidthOption.None }).Model;

            Assert.Equal(3, result.Articles.Count);
        }

        [Fact]
        public void GetViewModel_WhenPageIsReturned_ShouldMapFields()
        {
            var page = A.Fake<EditorialPage>();
            page.ListTitle = "L";
            page.ListIntro = "Li";
            ArrangeSearch(new List<EditorialPage>
            {
                page
            });

            var result = ((ReactModels.RelatedArticles)_mapper.GetPartialViewModel(new RelatedContentListBlock(), new RenderOptionQueryParameter { RenderWidthOption = RenderWidthOption.None }).Model)
                .Articles[0];

            Assert.Equal(page.ListTitle, result.Title.Text);
        }

        [Fact]
        public void GetViewModel_WhenPageIsReturned_ShouldMapPublishedDate()
        {
            const string expectedLabel = "p";
            var date = new DateTime(2017, 5, 13);
            var expectedDateString = $"{date.ToDisplayDate().ToString(DateTimeFormats.NorwegianDate)}";
            var page = A.Fake<EditorialPage>();
            page.StartPublish = date;
            ArrangeSearch(new List<EditorialPage>
            {
                page
            });
            ArrangeTranslation(expectedLabel, "Published");

            var result = ((ReactModels.RelatedArticles)_mapper.GetPartialViewModel(new RelatedContentListBlock(), new RenderOptionQueryParameter { RenderWidthOption = RenderWidthOption.None }).Model)
                .Articles[0];

            Assert.Equal(expectedLabel, result.Published.Type);
            Assert.Equal(expectedDateString, result.Published.Date);
        }

        [Fact]
        public void GetViewModel_WithoutPortfolioModeButWithPublications_ShouldMapPortfolioMode()
        {
            var block = new RelatedContentListBlock
            {
                Publications = ContentAreaBuilder.Create.Default().WithDefaultFilteredItems(),
                PortfolioMode = false
            };

            var result = ((ReactModels.PortfolioContent)_mapper.GetPartialViewModel(block, new RenderOptionQueryParameter { RenderWidthOption = RenderWidthOption.None }).Model)
                .Content.Blocks.FirstOrDefault();

            Assert.NotNull(result);
        }

        [Fact]
        public void GetViewModel_WithPublications_ShouldMapIsPublication()
        {
            var block = new RelatedContentListBlock
            {
                Publications = ContentAreaBuilder.Create.Default().WithDefaultFilteredItems()
            };

            var result = ((ReactModels.PortfolioContent)_mapper.GetPartialViewModel(block, new RenderOptionQueryParameter { RenderWidthOption = RenderWidthOption.None }).Model)
                .Content
                .Blocks
                .FirstOrDefault()?.ComponentData as ReactModels.RelatedArticles;

            Assert.True(result.IsPublication);
        }

        [Fact]
        public void GetViewModel_WithPublicationsAndPages_ShouldMapPublicationEvenIfSomePagesAreNewer()
        {
            var expectedTitle = "Publication";
            var newerDate = new DateTime(2017, 5, 13);
            var olderDate = new DateTime(2001, 5, 13);
            ArrangeSearch(new List<EditorialPage>
            {
                NewsArticlePageBuilder.Create.Default()
                    .WithStartPublish(newerDate),
                NewsArticlePageBuilder.Create.Default()
                    .WithStartPublish(newerDate)
            });
            A.CallTo(() => _contentLoader.Get<BasePageData>(A<ContentReference>._))
                .Returns(PublicationPageBuilder.Create.Default()
                    .WithStartPublish(olderDate));
            A.CallTo(() => _publicationReactModelBuilder.BuildReactModel(A<PublicationBasePage>._, A<PublicationMetadata>._, A<bool>._))
                .Returns(new ReactModels.SearchResult { Title = expectedTitle });
            var block = new RelatedContentListBlock
            {
                Publications = ContentAreaBuilder.Create.Default().WithDefaultFilteredItems(),
                NumberOfVisibleItems = 1
            };

            var result = ((ReactModels.PortfolioContent)_mapper.GetPartialViewModel(block, new RenderOptionQueryParameter { RenderWidthOption = RenderWidthOption.None }).Model)
                .Content
                .Blocks
                .FirstOrDefault()?.ComponentData as ReactModels.RelatedArticles;

            Assert.Equal(expectedTitle, result.Articles.Single().Title.Text);
        }

        [Fact]
        public void GetViewModel_WithManyPublications_ShouldMapAmountSpecifiedByNumberOfVisibleItems()
        {
            var expectedCount = 5;
            var inputCount = 10;
            A.CallTo(() => _contentLoader.Get<BasePageData>(A<ContentReference>._))
                .Returns(PublicationPageBuilder.Create.Default());
            var block = new RelatedContentListBlock
            {
                Publications = ContentAreaBuilder.Create.Default().WithDefaultFilteredItems(inputCount),
                NumberOfVisibleItems = expectedCount
            };

            var result = ((ReactModels.PortfolioContent)_mapper.GetPartialViewModel(block, new RenderOptionQueryParameter { RenderWidthOption = RenderWidthOption.None }).Model)
                .Content
                .Blocks
                .FirstOrDefault()?.ComponentData as ReactModels.RelatedArticles;

            Assert.Equal(expectedCount, result.Articles.Count);
        }

        [Fact]
        public void GetViewModel_WithManyPublicationsAndSomeArticles_ShouldMapNewestPublicationsOrderedByStartPublishDescending()
        {
            PublicationPage publicationA = PublicationPageBuilder.Create.Default()
                .WithContentLink(new ContentReference(1))
                .WithStartPublish(new DateTime(2010, 6, 5));
            PublicationPage publicationB = PublicationPageBuilder.Create.Default()
                .WithContentLink(new ContentReference(2))
                .WithStartPublish(new DateTime(2009, 6, 5));
            PublicationPage publicationC = PublicationPageBuilder.Create.Default()
                .WithContentLink(new ContentReference(3))
                .WithStartPublish(new DateTime(2011, 6, 5));
            PublicationPage publicationD = PublicationPageBuilder.Create.Default()
                .WithContentLink(new ContentReference(4))
                .WithStartPublish(new DateTime(2012, 6, 5));

            ArrangeSearch(new List<EditorialPage>
            {
                NewsArticlePageBuilder.Create.Default()
                    .WithStartPublish(new DateTime(2018, 6, 5)),
                NewsArticlePageBuilder.Create.Default()
                    .WithStartPublish(new DateTime(2007, 6, 5))
            });
            A.CallTo(() => _contentLoader.Get<BasePageData>(A<ContentReference>.That.Matches(x => x.ID == publicationA.ContentLink.ID)))
                .Returns(publicationA);
            A.CallTo(() => _contentLoader.Get<BasePageData>(A<ContentReference>.That.Matches(x => x.ID == publicationB.ContentLink.ID)))
                .Returns(publicationB);
            A.CallTo(() => _contentLoader.Get<BasePageData>(A<ContentReference>.That.Matches(x => x.ID == publicationC.ContentLink.ID)))
                .Returns(publicationC);
            A.CallTo(() => _contentLoader.Get<BasePageData>(A<ContentReference>.That.Matches(x => x.ID == publicationD.ContentLink.ID)))
                .Returns(publicationD);
            A.CallTo(() => _publicationReactModelBuilder.BuildReactModel(A<PublicationBasePage>.That.Matches(x => x.ContentLink.ID == publicationA.ContentLink.ID), A<PublicationMetadata>._, A<bool>._))
                .Returns(new ReactModels.SearchResult { Title = "A" });
            A.CallTo(() => _publicationReactModelBuilder.BuildReactModel(A<PublicationBasePage>.That.Matches(x => x.ContentLink.ID == publicationB.ContentLink.ID), A<PublicationMetadata>._, A<bool>._))
                .Returns(new ReactModels.SearchResult { Title = "B" });
            A.CallTo(() => _publicationReactModelBuilder.BuildReactModel(A<PublicationBasePage>.That.Matches(x => x.ContentLink.ID == publicationC.ContentLink.ID), A<PublicationMetadata>._, A<bool>._))
                .Returns(new ReactModels.SearchResult { Title = "C" });
            A.CallTo(() => _publicationReactModelBuilder.BuildReactModel(A<PublicationBasePage>.That.Matches(x => x.ContentLink.ID == publicationD.ContentLink.ID), A<PublicationMetadata>._, A<bool>._))
                .Returns(new ReactModels.SearchResult { Title = "D" });
            var block = new RelatedContentListBlock
            {
                Publications = ContentAreaBuilder.Create.Default()
                    .WithFilteredItems(
                        new ContentAreaItem { ContentLink = new ContentReference(1) },
                        new ContentAreaItem { ContentLink = new ContentReference(2) },
                        new ContentAreaItem { ContentLink = new ContentReference(3) },
                        new ContentAreaItem { ContentLink = new ContentReference(4) }
                    ),
                NumberOfVisibleItems = 2
            };

            var result = ((ReactModels.PortfolioContent)_mapper.GetPartialViewModel(block, new RenderOptionQueryParameter { RenderWidthOption = RenderWidthOption.None }).Model)
                .Content
                .Blocks
                .FirstOrDefault()?.ComponentData as ReactModels.RelatedArticles;

            Assert.Equal("D", result.Articles[0].Title.Text);
            Assert.Equal("C", result.Articles[1].Title.Text);
        }

        [Fact]
        public void GetViewModel_WithPublication_PublicationShouldUseIconAndEventImage()
        {
            var expectedDocumentIcon = new ReactModels.DocumentIcon
            {
                IconTheme = ReactModels.DocumentIcon_IconTheme.Pdf
            };

            A.CallTo(() => _contentLoader.Get<BasePageData>(A<ContentReference>._))
                .Returns(PublicationPageBuilder.Create.Default());
            A.CallTo(() => _publicationReactModelBuilder.BuildReactModel(A<PublicationBasePage>._, A<PublicationMetadata>._, A<bool>._))
                .Returns(new ReactModels.SearchResult { Icon = expectedDocumentIcon });
            var block = new RelatedContentListBlock
            {
                Publications = ContentAreaBuilder.Create.Default().WithDefaultFilteredItems()
            };

            var result = (((ReactModels.PortfolioContent)_mapper.GetPartialViewModel(block, new RenderOptionQueryParameter { RenderWidthOption = RenderWidthOption.None }).Model)
                .Content
                .Blocks
                .FirstOrDefault()?.ComponentData as ReactModels.RelatedArticles)
                .Articles
                .FirstOrDefault();

            Assert.Equal(expectedDocumentIcon, result.Icon);
            Assert.NotNull(result.EventImage);
        }

        [Fact]
        public void GetViewModel_WithPublication_PublicationWithImageShouldUseDocumentImageAndEventImage()
        {
            var expectedDocumentImage = new ReactModels.FluidImage();

            A.CallTo(() => _contentLoader.Get<BasePageData>(A<ContentReference>._))
                .Returns(PublicationPageBuilder.Create.Default()
                    .WithListImage(new ContentReference(2)));
            A.CallTo(() => _publicationReactModelBuilder.BuildReactModel(A<PublicationBasePage>._, A<PublicationMetadata>._, A<bool>._))
                .Returns(new ReactModels.SearchResult { Icon = new ReactModels.DocumentIcon() });
            A.CallTo(() => _contentLoader.Get<ImageFile>(A<ContentReference>._))
                .Returns(new ImageFile());
            A.CallTo(() => _fluidImageReactModelBuilder.BuildReactModel(A<ImageFile>._, A<string>._))
                .Returns(expectedDocumentImage);
            var block = new RelatedContentListBlock
            {
                Publications = ContentAreaBuilder.Create.Default().WithDefaultFilteredItems()
            };

            var result = (((ReactModels.PortfolioContent)_mapper.GetPartialViewModel(block, new RenderOptionQueryParameter { RenderWidthOption = RenderWidthOption.None }).Model)
                .Content
                .Blocks
                .FirstOrDefault()?.ComponentData as ReactModels.RelatedArticles)
                .Articles
                .FirstOrDefault();

            Assert.Null(result.Icon);
            Assert.Null(result.Image);
            Assert.Equal(expectedDocumentImage, result.DocumentImage);
            Assert.NotNull(result.EventImage);
        }

        [Fact]
        public void GetViewModel_WithPublicationAndArticle_ArticleWithImageShouldUseImage()
        {
            var expectedImage = new ReactModels.FluidImage();

            var articleTitle = "Article";
            ArrangeSearch(new List<EditorialPage>
            {
                NewsArticlePageBuilder.Create.Default()
                    .WithContentLink(new ContentReference(3))
                    .WithListImage(new ContentReference(4))
                    .WithListTitle(articleTitle)
            });
            A.CallTo(() => _contentLoader.Get<BasePageData>(A<ContentReference>._))
                .Returns(PublicationPageBuilder.Create.Default()
                    .WithListImage(new ContentReference(2)));
            A.CallTo(() => _publicationReactModelBuilder.BuildReactModel(A<PublicationBasePage>._, A<PublicationMetadata>._, A<bool>._))
                .Returns(new ReactModels.SearchResult { Icon = new ReactModels.DocumentIcon() });
            A.CallTo(() => _contentLoader.Get<ImageFile>(A<ContentReference>._))
                .Returns(new ImageFile());
            A.CallTo(() => _fluidImageReactModelBuilder.BuildReactModel(A<ImageFile>._, A<string>._))
                .Returns(expectedImage);
            var block = new RelatedContentListBlock
            {
                Publications = ContentAreaBuilder.Create.Default().WithDefaultFilteredItems()
            };

            var result = (((ReactModels.PortfolioContent)_mapper.GetPartialViewModel(block, new RenderOptionQueryParameter { RenderWidthOption = RenderWidthOption.None }).Model)
                .Content
                .Blocks
                .FirstOrDefault()?.ComponentData as ReactModels.RelatedArticles)
                .Articles
                .FirstOrDefault(x => x.Title.Text == articleTitle);

            Assert.Null(result.Icon);
            Assert.Equal(expectedImage, result.Image);
            Assert.Null(result.DocumentImage);
            Assert.Null(result.EventImage);
        }

        [Fact]
        public void GetViewModel_WithPublication_PublicationWithImageShouldUsePdfIconInMetadata()
        {
            var expectedIconTheme = ReactModels.DocumentIcon_IconTheme.Pdf;
            A.CallTo(() => _contentLoader.Get<BasePageData>(A<ContentReference>._))
                .Returns(PublicationPageBuilder.Create.Default()
                    .WithListImage(new ContentReference(2)));
            A.CallTo(() => _publicationReactModelBuilder.BuildReactModel(A<PublicationBasePage>._, A<PublicationMetadata>._, A<bool>.That.Matches(x => x == true)))
                .Returns(new ReactModels.SearchResult
                {
                    Metadata = new ReactModels.Metadata
                    {
                        Items = new List<ReactModels.Metadata_Items>
                        {
                            new ReactModels.Metadata_Items
                            {
                                Icon = new ReactModels.DocumentIcon { IconTheme = expectedIconTheme }
                            }
                        }
                    }
                });
            var block = new RelatedContentListBlock
            {
                Publications = ContentAreaBuilder.Create.Default().WithDefaultFilteredItems()
            };

            var result = (((ReactModels.PortfolioContent)_mapper.GetPartialViewModel(block, new RenderOptionQueryParameter { RenderWidthOption = RenderWidthOption.None }).Model)
                .Content
                .Blocks
                .FirstOrDefault()?.ComponentData as ReactModels.RelatedArticles)
                .Articles
                .FirstOrDefault();

            Assert.Equal(expectedIconTheme, result.Metadata.Items[0].Icon.IconTheme);
        }

        [Fact]
        public void GetViewModel_WhenButtonTextIsNull_ShouldFallbackToLabel()
        {
            const string expectedLabel = "label";
            ArrangeTranslation(expectedLabel, "GoToListButton");

            var block = new RelatedContentListBlock
            {
                Button = new EditLinkBlock
                {
                    Link = new Url("u")
                }
            };

            var result = (ReactModels.RelatedArticles)_mapper.GetPartialViewModel(block, new RenderOptionQueryParameter { RenderWidthOption = RenderWidthOption.None }).Model;

            Assert.Equal(expectedLabel, result.RelatedContent.Link.Text);
        }

        [Fact]
        public void GetViewModel_WhenPageIsInEditMode_ShouldMapOnPageEditing()
        {
            A.CallTo(() => _pageEditingAdapter.PageIsInEditMode()).Returns(true);

            var result = (ReactModels.RelatedArticles)_mapper.GetPartialViewModel(new RelatedContentListBlock(), new RenderOptionQueryParameter { RenderWidthOption = RenderWidthOption.None }).Model;

            Assert.NotNull(result.RelatedContent.OnPageEditing);
        }

        [Fact]
        public void GetViewModel_WhenUsedInSidebar_TextShouldBeNull()
        {
            var page = A.Fake<ArticlePage>();
            page.ListIntro = "listIntro";
            page.ListTitle = "listTitle";
            page.StartPublish = new DateTime(2020, 4, 1);
            ArrangeSearch(new List<EditorialPage>
            {
                page
            });

            var result = ((ReactModels.RelatedArticles)_mapper.GetPartialViewModel(new RelatedContentListBlock(), new RenderOptionQueryParameter { RenderWidthOption = RenderWidthOption.IsInSidebar }).Model)
                .Articles[0];

            Assert.Equal(result.Title.Text, page.ListTitle);
            Assert.Null(result.Text);
        }

        private void ArrangeSearch(List<EditorialPage> contentList)
        {
            var result = new ContentListSearchResults(
                contentList,
                new List<CategoryGroup>(),
                contentList.Count);

            A.CallTo(() => _contentListService.Search<EditorialPage>(A<ContentListRequestModel>._))
                .Returns(result);
        }

        private void ArrangeGetUrl(string expectedUrl) =>
            A.CallTo(() => _urlResolver.GetUrl(A<UrlBuilder>._, A<UrlResolverArguments>._))
                .Returns(expectedUrl);

        private void ArrangeTranslation(string expected, string label) =>
            A.CallTo(() => _localizationProvider.GetLabel(nameof(RelatedContentListBlock), label))
                .Returns(expected);
    }
}