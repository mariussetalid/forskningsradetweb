using FakeItEasy;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Facades.Contracts;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ReactModels;
using Forskningsradet.Core.ViewModelMappers.Blocks;
using Xunit;

namespace Forskningsradet.UnitTests.Core.ViewModelMappers.Blocks
{
    public class ProjectDatabankBlockViewModelMapperTests
    {
        const string ValidSrcStart = EmbedSourceConstants.ProjectDatabank.ValidSrcStart;

        private readonly ProjectDatabankBlockViewModelMapper _mapper;
        private readonly IPageEditingAdapter _pageEditingAdapter;

        public ProjectDatabankBlockViewModelMapperTests()
        {
            _pageEditingAdapter = A.Fake<IPageEditingAdapter>();
            _mapper = new ProjectDatabankBlockViewModelMapper(_pageEditingAdapter);
        }

        [Fact]
        public void GetViewModel_WhenAllFieldsProvided_ShouldMapFields()
        {
            var block = new ProjectDatabankBlock
            {
                AlternativeText = "Alt",
                Src = ValidSrcStart,
                Width = 20,
                Height = 30
            };

            var result = (EmbedBlock)_mapper.GetPartialViewModel(block).Model;

            Assert.Equal(block.AlternativeText, result.Title);
            Assert.Equal(block.Src, result.Src);
            Assert.Equal(block.Width, result.Width);
            Assert.Equal(block.Height, result.Height);
        }
        
        [Theory]
        [InlineData(null)]
        [InlineData(" ")]
        [InlineData("Something")]
        [InlineData("http://www.forskningsradet.no/prosjektbanken")]
        [InlineData("https://www.forskningsradet.no/prosjektbanken")]
        public void GetViewModel_WhenSrcIsInvalid_ShouldNotMapSrc(string invalidSrc)
        {
            var block = new ProjectDatabankBlock
            {
                Src = invalidSrc
            };

            var result = (EmbedBlock)_mapper.GetPartialViewModel(block).Model;

            Assert.Null(result.Src);
        }
                
        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public void GetViewModel_WhenDimensionsAreInvalid_ShouldNotMapDimensions(int invalidNumber)
        {
            var block = new ProjectDatabankBlock
            {
                Width = invalidNumber,
                Height = invalidNumber
            };

            var result = (EmbedBlock)_mapper.GetPartialViewModel(block).Model;

            Assert.Null(result.Width);
            Assert.Null(result.Height);
        }

        [Fact]
        public void GetViewModel_WhenPageIsInEditMode_ShouldMapOnPageEditing()
        {
            A.CallTo(() => _pageEditingAdapter.PageIsInEditMode()).Returns(true);

            var result = (EmbedBlock)_mapper.GetPartialViewModel(new ProjectDatabankBlock()).Model;

            Assert.NotNull(result.OnPageEditing);
        }
    }
}