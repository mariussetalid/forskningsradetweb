﻿using EPiServer;
using EPiServer.Core;
using EPiServer.Web.Routing;
using FakeItEasy;
using Forskningsradet.Core.Facades.Contracts;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ContentModels.Media;
using Forskningsradet.Core.ViewModelMappers.Blocks;
using Xunit;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.UnitTests.Core.ViewModelMappers.Blocks
{
    public class ProfileBlockViewModelMapperTests
    {
        private readonly IUrlResolver _urlResolver;
        private readonly IContentLoader _contentLoader;
        private readonly IPageEditingAdapter _pageEditingAdapter;
        private readonly ProfileBlockViewModelMapper _mapper;

        public ProfileBlockViewModelMapperTests()
        {
            _urlResolver = A.Fake<IUrlResolver>();
            _contentLoader = A.Fake<IContentLoader>();
            _pageEditingAdapter = A.Fake<IPageEditingAdapter>();
            _mapper = new ProfileBlockViewModelMapper(_urlResolver, _contentLoader, _pageEditingAdapter);
        }

        [Fact]
        public void GetViewModel_WhenAllFieldsProvided_ShouldMapFields()
        {
            var expectedName = "name";
            var expectedImageUrl = "imageUrl";
            var expectedImageAltText = "alt";

            A.CallTo(() => _contentLoader.Get<ImageFile>(A<ContentReference>._))
                .Returns(new ImageFile { AltText = expectedImageAltText, ContentLink = ContentReference.EmptyReference });
            A.CallTo(() => _urlResolver.GetUrl(A<ContentReference>._, A<string>._, A<UrlResolverArguments>._))
                .Returns(expectedImageUrl);

            var block = new ProfileBlock
            {
                ProfileName = expectedName,
                JobTitle = "J",
                Company = "C",
                MainIntro = "Text",
                Image = A.Fake<ContentReference>()
            };

            var result = (ReactModels.PersonBlock)_mapper.GetPartialViewModel(block).Model;

            Assert.Equal(block.ProfileName, result.Name);
            Assert.Equal(block.JobTitle, result.JobTitle);
            Assert.Equal(block.Company, result.Company);
            Assert.Equal(block.MainIntro, result.Text);
            Assert.Equal(expectedImageUrl, result.Image.Src);
            Assert.Equal(expectedImageAltText, result.Image.Alt);
        }

        [Fact]
        public void GetViewModel_WhenCultureSpecificFieldsProvided_ShouldMapCultureSpecificFields()
        {
            var block = new ProfileBlock
            {
                ProfileName = "N",
                CultureSpecificProfileName = "CName",
                Company = "C",
                CultureSpecificCompany = "CComp",
            };

            var result = (ReactModels.PersonBlock) _mapper.GetPartialViewModel(block).Model;

            Assert.Equal(block.CultureSpecificProfileName, result.Name);
            Assert.Equal(block.CultureSpecificCompany, result.Company);
        }

        [Fact]
        public void GetViewModel_WhenPageIsInEditMode_ShouldMapOnPageEditing()
        {
            A.CallTo(() => _pageEditingAdapter.PageIsInEditMode()).Returns(true);

            var result = (ReactModels.PersonBlock)_mapper.GetPartialViewModel(new ProfileBlock()).Model;

            Assert.NotNull(result.OnPageEditing);
            Assert.NotNull(result.Image.OnPageEditing);
        }
    }
}
