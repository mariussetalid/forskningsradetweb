﻿using System.Collections.Generic;
using System.Linq;
using EPiServer;
using EPiServer.Web.Routing;
using FakeItEasy;
using Forskningsradet.Core.Facades.Contracts;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ContentModels.PropertyList;
using Forskningsradet.Core.ViewModelMappers.Blocks;
using Xunit;

namespace Forskningsradet.UnitTests.Core.ViewModelMappers.Blocks
{
    public class OfficeBlockViewModelMapperTests
    {
        private readonly OfficeBlockViewModelMapper _mapper;
        private readonly IPageEditingAdapter _pageEditingAdapter;
        private readonly IUrlResolver _urlResolver;

        public OfficeBlockViewModelMapperTests()
        {
            _urlResolver = A.Fake<IUrlResolver>();
            _pageEditingAdapter = A.Fake<IPageEditingAdapter>();
            _mapper = new OfficeBlockViewModelMapper(_urlResolver, _pageEditingAdapter);
        }

        [Fact]
        public void GetViewModel_ShouldMapAll_WhenAllFieldsAreSet()
        {
            var expectedTitle = "OfficeBlock";
            var informationTitle = "InformationTitle";
            var informationText = "InformationText";
            var informationMoreText = "InformationMoreText";
            var informationLinkUrl = new Url("http://www.InformationLinkUrl.no");
            var informationLinkText = "InformationLinkText";
            A.CallTo(() => _urlResolver.GetUrl(A<UrlBuilder>.That.Matches(x => x.Path == informationLinkUrl.Path), A<UrlResolverArguments>._))
                .Returns(informationLinkUrl.ToString());

            var information = new List<OfficeInformation>
            {
                new OfficeInformation
                {
                    Title = informationTitle,
                    Text = informationText,
                    MoreText = informationMoreText,
                    Link = informationLinkUrl,
                    LinkText = informationLinkText
                }
            };
            var officeBlock = new OfficeBlock
            {
                Title = expectedTitle,
                Information = information
            };

            var result = (Forskningsradet.Core.Models.ReactModels.OfficeBlock) _mapper.GetPartialViewModel(officeBlock).Model;

            Assert.Equal(expectedTitle, result.Title);
            Assert.Equal(informationTitle, result.Info.FirstOrDefault()?.Title);
            Assert.Equal(informationText, result.Info.FirstOrDefault()?.Text);
            Assert.Equal(informationMoreText, result.Info.FirstOrDefault()?.MoreText);
            Assert.Equal(informationLinkUrl, result.Info.FirstOrDefault()?.Link?.Url);
            Assert.Equal(informationLinkText, result.Info.FirstOrDefault()?.Link?.Text);
        }

        [Fact]
        public void GetViewModel_ShouldMapInfoBlock_WhenInfoIsSet()
        {
            var officeBlock = new OfficeBlock
            {
                Information = new List<OfficeInformation>
                {
                    new OfficeInformation(),
                }
            };

            var result = (Forskningsradet.Core.Models.ReactModels.OfficeBlock) _mapper.GetPartialViewModel(officeBlock).Model;

            Assert.Equal(1, result.Info.Count);
        }
    }
}