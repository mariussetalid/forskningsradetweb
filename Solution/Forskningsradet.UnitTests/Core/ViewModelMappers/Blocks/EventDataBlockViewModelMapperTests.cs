using System;
using System.Collections.Generic;
using System.Linq;
using FakeItEasy;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Services;
using Forskningsradet.Core.ViewModelMappers.Blocks;
using Xunit;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.UnitTests.Core.ViewModelMappers.Blocks
{
    public class EventDataBlockViewModelMapperTests
    {
        private readonly ILocalizationProvider _localizationProvider;
        private readonly EventDataBlockViewModelMapper _mapper;

        public EventDataBlockViewModelMapperTests()
        {
            _localizationProvider = A.Fake<ILocalizationProvider>();
            _mapper = new EventDataBlockViewModelMapper(_localizationProvider);
        }

        [Fact]
        public void GetViewModel_WhenAllFieldsProvided_ShouldMapFields()
        {
            const string expectedTypeText = "C";
            var eventDataBlock = new EventDataBlock
            {
                WhenList = new List<string> { "A" },
                Where = "B",
                Type = EventType.Conference,
                TargetGroup = "D",
                Price = "E",
                Deadline = "F",
                Next = "G"
            };
            ArrangeEnumTranslation(expectedTypeText, EventType.Conference);

            var result = (ReactModels.EventData)_mapper.GetPartialViewModel(eventDataBlock).Model;

            Assert.Equal(eventDataBlock.WhenList[0], result.Items[0].Text[0][0]);
            Assert.Equal(eventDataBlock.Where, result.Items[1].Text[0][0]);
            Assert.Equal(expectedTypeText, result.Items[2].Text[0][0]);
            Assert.Equal(eventDataBlock.TargetGroup, result.Items[3].Text[0][0]);
            Assert.Equal(eventDataBlock.Price, result.Items[4].Text[0][0]);
            Assert.Equal(eventDataBlock.Deadline, result.Items[5].Text[0][0]);
            Assert.Equal(eventDataBlock.Next, result.Items[6].Text[0][0]);
        }

        [Fact]
        public void GetViewModel_WhenFieldsAreEmpty_ShouldNotMapLabels()
        {
            A.CallTo(() => _localizationProvider.GetLabel(nameof(EventPage), A<string>._))
                .Returns("Something");

            var result = (ReactModels.EventData)_mapper.GetPartialViewModel(new EventDataBlock()).Model;

            Assert.True(result.Items.All(x => string.IsNullOrEmpty(x.Label)));
        }

        [Theory]
        [InlineData(1, "ABC")]
        [InlineData(1, "\nABC")]
        [InlineData(1, "ABC\n")]
        [InlineData(1, "\nABC\n")]
        [InlineData(1, "\n\nABC\n\n")]
        [InlineData(1, "\n\n\n\nABC\n\n\n\n")]
        [InlineData(2, "A\nBC")]
        [InlineData(2, "A\n\nBC")]
        [InlineData(2, "A\n\n\n\nBC")]
        [InlineData(3, "A\nB\nC")]
        [InlineData(3, "A\n\nB\n\nC")]
        [InlineData(3, "A\n\n\n\nB\n\n\n\nC")]
        public void GetViewModel_WhenFieldsContainNewLines_ShouldSplitTextSecondDimensionAndRemoveEmptyParts(int expectedCount, string inputString)
        {
            var eventDataBlock = new EventDataBlock
            {
                WhenList = new List<string> { inputString },
                Where = inputString,
                TargetGroup = inputString,
                Price = inputString,
                Deadline = inputString,
                Next = inputString
            };

            var result = (ReactModels.EventData)_mapper.GetPartialViewModel(eventDataBlock).Model;

            Assert.Equal(6, result.Items.Count);
            Assert.True(result.Items.All(x => expectedCount == x.Text[0].Count));
        }

        [Fact]
        public void GetViewModel_WhenFirstFieldContainListAndNewLines_ShouldSplitTextBothDimensions()
        {
            var expectedItemText = new List<IList<string>>
            {
                new List<string> {"A"},
                new List<string> {"B", "b", "b"},
                new List<string> {"C"}
            };

            var eventDataBlock = new EventDataBlock
            {
                WhenList = new List<string> { "A", "B\nb\nb", "C" }
            };

            var result = (ReactModels.EventData)_mapper.GetPartialViewModel(eventDataBlock).Model;

            Assert.Equal(expectedItemText, result.Items[0].Text);
        }

        private void ArrangeEnumTranslation<TEnum>(string expected, TEnum value) where TEnum : Enum
        {
            A.CallTo(() => _localizationProvider.GetEnumName<TEnum>(value))
                .Returns(expected);
        }
    }
}