using System.Collections.Generic;
using System.Linq;
using EPiServer;
using EPiServer.Core;
using FakeItEasy;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Services.Contracts;
using Forskningsradet.Core.ViewModelMappers.Blocks;
using Forskningsradet.UnitTests.Builders;
using Xunit;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.UnitTests.Core.ViewModelMappers.Blocks
{
    public class LinkListContainerBlockViewModelMapperTests
    {
        private readonly IContentLoader _contentLoader;
        private readonly ILinkListReactModelBuilder _linkListReactModelBuilder;
        private readonly IContentAreaReactModelBuilder _contentAreaReactModelBuilder;
        private readonly ILinkWithTextReactModelBuilder _linkWithTextReactModelBuilder;
        private readonly IPersonPageStringBuilder _personPageStringBuilder;

        private readonly LinkListContainerBlockViewModelMapper _mapper;

        public LinkListContainerBlockViewModelMapperTests()
        {
            _contentLoader = A.Fake<IContentLoader>();
            _linkListReactModelBuilder = A.Fake<ILinkListReactModelBuilder>();
            _contentAreaReactModelBuilder = A.Fake<IContentAreaReactModelBuilder>();
            _linkWithTextReactModelBuilder = A.Fake<ILinkWithTextReactModelBuilder>();
            _personPageStringBuilder = A.Fake<IPersonPageStringBuilder>();
            _mapper = new LinkListContainerBlockViewModelMapper(
                _contentLoader,
                    _linkListReactModelBuilder,
                    _contentAreaReactModelBuilder,
                    _linkWithTextReactModelBuilder,
                    _personPageStringBuilder);
        }

        [Fact]
        public void GetPartialViewModel_WithDefaultValues_ShouldMapTitle()
        {
            const string expectedTitle = "Title";
            var block = (LinkListContainerBlock)LinkListContainerBlockBuilder.Create.Default()
                .WithTitle(expectedTitle);

            var result = (ReactModels.PortfolioContent)_mapper.GetPartialViewModel(block).Model;

            Assert.Equal(expectedTitle, result.Title);
        }

        [Fact]
        public void GetPartialViewModel_WithDefaultValues_ShouldMapLinkLists()
        {
            const string expectedLinkListsComponentName = nameof(ReactModels.LinkLists);
            ArrangeContentAreaReactModelBuilderForComponentName(expectedLinkListsComponentName);

            var block = (LinkListContainerBlock)LinkListContainerBlockBuilder.Create.Default();

            var result = ((ReactModels.PortfolioContent)_mapper.GetPartialViewModel(block).Model)
                .Content.Blocks.FirstOrDefault();

            Assert.Equal(expectedLinkListsComponentName, result.ComponentName);
        }

        [Theory]
        [InlineData(true)]
        [InlineData(false)]
        public void GetPartialViewModel_WithSingleColumn_ShouldMapSingleColumn(bool trueOrFalse)
        {
            var block = (LinkListContainerBlock)LinkListContainerBlockBuilder.Create.Default()
                .WithSingleColumn(trueOrFalse);

            var result = (ReactModels.PortfolioContent)_mapper.GetPartialViewModel(block).Model;

            Assert.Equal(trueOrFalse, result.SingleColumn);
        }

        [Fact]
        public void GetPartialViewModel_WithLinkList_ShouldMapLinkLists()
        {
            const string expectedLinkListsComponentName = nameof(ReactModels.LinkLists);
            ArrangeContentAreaReactModelBuilderForComponentName(expectedLinkListsComponentName);

            var block = (LinkListContainerBlock)LinkListContainerBlockBuilder.Create.Default()
                .WithLinkList(ContentAreaBuilder.Create.Default().WithDefaultFilteredItems());

            var result = ((ReactModels.PortfolioContent)_mapper.GetPartialViewModel(block).Model)
                .Content.Blocks.FirstOrDefault();

            Assert.Equal(expectedLinkListsComponentName, result.ComponentName);
        }

        [Fact]
        public void GetPartialViewModel_WithContactList_ShouldMapContactList()
        {
            const string expectedComponentName = nameof(ReactModels.ContactList);
            ArrangeContentAreaReactModelBuilderForComponentName(expectedComponentName);

            var block = (LinkListContainerBlock)LinkListContainerBlockBuilder.Create.Default()
                .WithContactList(ContentAreaBuilder.Create.Default().WithDefaultFilteredItems());

            var result = ((ReactModels.PortfolioContent)_mapper.GetPartialViewModel(block).Model)
                .Content.Blocks.FirstOrDefault();

            Assert.Equal(expectedComponentName, result.ComponentName);
        }

        [Fact]
        public void GetPartialViewModel_WithLinkWithTextList_ShouldMapLinkWithText()
        {
            const string expectedComponentName = nameof(ReactModels.LinkWithText);
            ArrangeContentAreaReactModelBuilderForComponentName(expectedComponentName);
            A.CallTo(() => _contentLoader.Get<LinkWithTextBlock>(A<ContentReference>._))
                .Returns(new LinkWithTextBlock());

            var block = (LinkListContainerBlock)LinkListContainerBlockBuilder.Create.Default()
                .WithLinkWithTextList(ContentAreaBuilder.Create.Default().WithDefaultFilteredItems());

            var result = ((ReactModels.PortfolioContent)_mapper.GetPartialViewModel(block).Model)
                .Content.Blocks.FirstOrDefault();

            Assert.Equal(expectedComponentName, result.ComponentName);
        }

        [Fact]
        public void GetPartialViewModel_WithAllContentAreas_ShouldMapLinkLists()
        {
            const string expectedComponentName = nameof(ReactModels.LinkLists);
            ArrangeContentAreaReactModelBuilderForComponentName(expectedComponentName);
            A.CallTo(() => _contentLoader.Get<LinkWithTextBlock>(A<ContentReference>._))
                .Returns(new LinkWithTextBlock());

            var block = (LinkListContainerBlock)LinkListContainerBlockBuilder.Create.Default()
                .WithLinkList(ContentAreaBuilder.Create.Default().WithDefaultFilteredItems())
                .WithContactList(ContentAreaBuilder.Create.Default().WithDefaultFilteredItems())
                .WithLinkWithTextList(ContentAreaBuilder.Create.Default().WithDefaultFilteredItems());

            var result = ((ReactModels.PortfolioContent)_mapper.GetPartialViewModel(block).Model)
                .Content.Blocks.FirstOrDefault();

            Assert.Equal(expectedComponentName, result.ComponentName);
        }

        private void ArrangeContentAreaReactModelBuilderForComponentName(string componentName)
        {
            A.CallTo(() => _contentAreaReactModelBuilder.BuildReactModel(
                    A<IList<ReactModels.ContentAreaItem>>.That.Matches(x => x.FirstOrDefault().ComponentName == componentName),
                    A<string>._))
                .Returns(new ReactModels.ContentArea
                {
                    Blocks = new List<ReactModels.ContentAreaItem>
                    {
                        new ReactModels.ContentAreaItem
                        {
                            ComponentName = componentName
                        }
                    }
                });
        }
    }
}