using System;
using System.Collections.Generic;
using System.Linq;
using EPiServer;
using EPiServer.Core;
using FakeItEasy;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Services.Contracts;
using Forskningsradet.Core.ViewModelMappers.Blocks;
using Forskningsradet.UnitTests.TestUtils;
using Newtonsoft.Json;
using Xunit;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.UnitTests.Core.ViewModelMappers.Blocks
{
    public class StatisticLinksListBlockViewModelMapperTests
    {
        private readonly IContentLoadHelper _contentLoadHelper;
        private readonly IImageReactModelBuilder _imageReactModelBuilder;
        private readonly ILinkReactModelBuilder _linkReactModelBuilder;
        private readonly StatisticLinksListBlockViewModelMapper _mapper;

        public StatisticLinksListBlockViewModelMapperTests()
        {
            _contentLoadHelper = A.Fake<IContentLoadHelper>();
            _imageReactModelBuilder = FakeServiceHelper.CreateImageRefToSrcFakeImageBuilder();
            _linkReactModelBuilder = FakeServiceHelper.CreateFakeLinkBuilder();

            _mapper = new StatisticLinksListBlockViewModelMapper(_contentLoadHelper, _imageReactModelBuilder, _linkReactModelBuilder);
        }

        [Fact]
        public void GetViewModel_ShouldMapFields()
        {
            var block = new StatisticLinksListBlock
            {
                Heading = "Heading",
                AboutImage = new ContentReference(1001),
                Links = new ContentArea(),
                SeeMoreLink = new EditLinkBlock { Link = "http:see-more.com", Text = "See more" }
            };
            var linkBlocks = Enumerable.Range(0, 3)
                .Select(index =>
                    new StatisticLinkBlock
                    {
                        Url = new Url($"http://link{index}_url.com"),
                        Image = new ContentReference(index),
                        Text = $"Link{index} text",
                        Title = $"Link{index} title"
                    }
                ).ToList();

            var expectedResult = new ReactModels.StatsLinkList
            {
                Heading = block.Heading,
                AboutImage = new ReactModels.Image { Src = block.AboutImage.ID.ToString() },
                StatsLinks = linkBlocks.Select((x, index) => new ReactModels.StatsLink
                {
                    Link = new ReactModels.Link { Url = x.Url.ToString(), Text = x.Text },
                    Image = new ReactModels.Image { Src = index.ToString() },
                    Title = x.Title
                }).ToList(),
                SeeMoreLink = new ReactModels.Link
                {
                    Url = block.SeeMoreLink.Link.ToString(),
                    Text = block.SeeMoreLink.Text
                }
            };

            A.CallTo(() => _contentLoadHelper.GetFilteredItems<StatisticLinkBlock>(block.Links)).Returns(linkBlocks);

            var result = (ReactModels.StatsLinkList)_mapper.GetPartialViewModel(block).Model;

            Assert.Equal(JsonConvert.SerializeObject(expectedResult), JsonConvert.SerializeObject(result));
        }

        [Fact]
        public void GetViewModel_ShouldMapFilteredLinks()
        {
            var block = new StatisticLinksListBlock { Links = new ContentArea() };

            _mapper.GetPartialViewModel(block);

            A.CallTo(() => _contentLoadHelper.GetFilteredItems<StatisticLinkBlock>(block.Links)).MustHaveHappenedOnceExactly();
        }

        [Fact]
        public void GetViewModel_ShouldReturnNull_WhenBlockIsEmpty()
        {
            var block = new StatisticLinksListBlock();

            var result = (ReactModels.StatsLinkList)_mapper.GetPartialViewModel(block).Model;

            Assert.Null(result);
        }

        [Fact]
        public void GetViewModel_ShouldReturnNull_When_HeadingAndLinksAndAboutImage_AreEmpty()
        {
            var block = new StatisticLinksListBlock
            {
                Heading = String.Empty,
                Links = new ContentArea(),
                AboutImage = null,
                SeeMoreLink = new EditLinkBlock
                {
                    Link = new Url("http://see-more.com"),
                    Text = "See more"
                }
            };

            A.CallTo(() => _contentLoadHelper.GetFilteredItems<StatisticLinkBlock>(block.Links)).Returns(new List<StatisticLinkBlock>());

            var result = (ReactModels.StatsLinkList)_mapper.GetPartialViewModel(block).Model;

            Assert.Null(result);
        }

        [Fact]
        public void GetViewModel_ShouldNotMapNonEmptyLinksOnly()
        {
            var block = new StatisticLinksListBlock { Links = new ContentArea() };

            var nonEmptyLinkBlocks = new List<StatisticLinkBlock> {
                new StatisticLinkBlock
                {
                    Url = new Url($"http://linkurl.com"),
                    Text = $"Has link",
                },
                new StatisticLinkBlock
                {
                    Image = new ContentReference(111)
                },
                new StatisticLinkBlock
                {
                    Title = "Has title"
                }
            };

            var emptyLinkBlocks = new List<StatisticLinkBlock> {
                new StatisticLinkBlock
                {
                    Url = new Url("http://not_enough_to_build_link_at_least.com")
                },
                new StatisticLinkBlock
                {
                    Text = "Not enough to build link at least"
                },
                new StatisticLinkBlock()
            };

            A.CallTo(() => _contentLoadHelper.GetFilteredItems<StatisticLinkBlock>(block.Links)).Returns(nonEmptyLinkBlocks.Concat(emptyLinkBlocks));

            var result = (ReactModels.StatsLinkList)_mapper.GetPartialViewModel(block).Model;

            Assert.Equal(nonEmptyLinkBlocks.Count, result.StatsLinks.Count);
        }
    }
}