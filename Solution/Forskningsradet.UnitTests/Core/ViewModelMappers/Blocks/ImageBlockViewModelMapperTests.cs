﻿using EPiServer;
using EPiServer.Core;
using EPiServer.Web.Routing;
using FakeItEasy;
using Forskningsradet.Core.Facades.Contracts;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ContentModels.Media;
using Forskningsradet.Core.ViewModelMappers.Blocks;
using Xunit;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.UnitTests.Core.ViewModelMappers.Blocks
{
    public class ImageBlockViewModelMapperTests
    {
        private readonly IUrlResolver _urlResolver;
        private readonly IContentLoader _contentLoader;
        private readonly IPageEditingAdapter _pageEditingAdapter;
        private readonly ImageBlockViewModelMapper _mapper;

        public ImageBlockViewModelMapperTests()
        {
            _urlResolver = A.Fake<IUrlResolver>();
            _contentLoader = A.Fake<IContentLoader>();
            _pageEditingAdapter = A.Fake<IPageEditingAdapter>();
            _mapper = new ImageBlockViewModelMapper(_urlResolver, _contentLoader, _pageEditingAdapter);
        }

        [Fact]
        public void GetViewModel_WhenAllFieldsProvided_ShouldMapFields()
        {
            var expectedTitle = "title";
            var expectedImageUrl = "imageUrl";
            var expectedImageAltText = "alt";

            A.CallTo(() => _contentLoader.Get<ImageFile>(A<ContentReference>._))
                .Returns(new ImageFile { AltText = expectedImageAltText, ContentLink = ContentReference.EmptyReference });
            A.CallTo(() => _urlResolver.GetUrl(A<ContentReference>._, A<string>._, A<UrlResolverArguments>._))
                .Returns(expectedImageUrl);

            var imageBlock = new ImageBlock
            {
                Title = expectedTitle,
                Image = A.Fake<ContentReference>()
            };

            var result = (ReactModels.ImageBlock) _mapper.GetPartialViewModel(imageBlock).Model;

            Assert.Equal(expectedTitle, result.Title);
            Assert.Equal(expectedImageUrl, result.Image.Src);
            Assert.Equal(expectedImageAltText, result.Image.Alt);
        }

        [Fact]
        public void GetViewModel_WhenPageIsInEditMode_ShouldMapOnPageEditing()
        {
            A.CallTo(() => _pageEditingAdapter.PageIsInEditMode()).Returns(true);

            var result = (ReactModels.ImageBlock)_mapper.GetPartialViewModel(new ImageBlock()).Model;

            Assert.NotNull(result.OnPageEditing);
            Assert.NotNull(result.Image.OnPageEditing);
        }
    }
}
