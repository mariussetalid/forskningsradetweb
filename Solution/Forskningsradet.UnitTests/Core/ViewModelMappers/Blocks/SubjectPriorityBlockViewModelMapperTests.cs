using System.Collections.Generic;
using System.Linq;
using EPiServer.Core;
using FakeItEasy;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.QueryParameters;
using Forskningsradet.Core.ViewModelMappers.Blocks;
using Forskningsradet.UnitTests.Builders;
using Xunit;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.UnitTests.Core.ViewModelMappers.Blocks
{
    public class SubjectPriorityBlockViewModelMapperTests
    {
        private readonly IRichTextReactModelBuilder _richTextReactModelBuilder;
        private readonly IContentAreaReactModelBuilder _contentAreaReactModelBuilder;
        private readonly IOptionsModalReactModelBuilder _optionsModalReactModelBuilder;

        private readonly SubjectPriorityBlockViewModelMapper _mapper;

        public SubjectPriorityBlockViewModelMapperTests()
        {
            _richTextReactModelBuilder = A.Fake<IRichTextReactModelBuilder>();
            _contentAreaReactModelBuilder = A.Fake<IContentAreaReactModelBuilder>();
            _optionsModalReactModelBuilder = A.Fake<IOptionsModalReactModelBuilder>();
            _mapper = new SubjectPriorityBlockViewModelMapper(
                    _richTextReactModelBuilder,
                    _contentAreaReactModelBuilder,
                    _optionsModalReactModelBuilder);
        }

        [Fact]
        public void GetPartialViewModel_WithDefaultValues_ShouldMapDefaultValues()
        {
            const string expectedTitle = "Title";
            const string expectedHtmlId = "sub7";
            var expectedContentAreaComponent = new ReactModels.ContentArea();
            var expectedOptionsModalComponent = new ReactModels.OptionsModal();

            var subjectBlock = (SubjectPriorityBlock)SubjectPriorityBlockBuilder.Create.Default()
                .WithTitle(expectedTitle)
                .WithId(7)
                .WithRightBlockArea(ContentAreaBuilder.Create.Default().WithDefaultFilteredItems());

            A.CallTo(() => _contentAreaReactModelBuilder.BuildReactModel(A<List<ReactModels.ContentAreaItem>>._, A<string>._))
                .Returns(expectedContentAreaComponent);
            A.CallTo(() => _optionsModalReactModelBuilder.BuildShareContent(A<PageReference>._, A<string>._, A<string>._, A<string>._, A<ReactModels.ShareOptions_Items_Icon[]>._))
                .Returns(expectedOptionsModalComponent);

            var result = (ReactModels.AccordionWithContentArea)_mapper.GetPartialViewModel(subjectBlock, new SubjectPriorityBlockQueryParameter()).Model;

            Assert.Equal(expectedTitle, result.Title);
            Assert.Equal(expectedOptionsModalComponent, result.Share);
            Assert.Equal(expectedHtmlId, result.HtmlId);
            Assert.Equal(expectedContentAreaComponent, result.Content);
            Assert.Null(result.IconWarning);
        }

        [Fact]
        public void GetPartialViewModel_WithPortfolioContent_ShouldMapPortfolioContent()
        {
            var contentAreaComponentWithPortfolioContent = new ReactModels.ContentArea
            {
                Blocks = new List<ReactModels.ContentAreaItem>
                {
                    new ReactModels.ContentAreaItem
                    {
                        ComponentName = nameof(ReactModels.PortfolioContent),
                        ComponentData = new ReactModels.PortfolioContent()
                    }
                }
            };

            var subjectBlock = (SubjectPriorityBlock)SubjectPriorityBlockBuilder.Create.Default()
                .WithPortfolioContent(ContentAreaBuilder.Create.Default().WithDefaultFilteredItems());

            A.CallTo(() => _contentAreaReactModelBuilder.BuildReactModel(A<ContentArea>._, A<string>._, A<RenderOptionQueryParameter>._))
                .Returns(contentAreaComponentWithPortfolioContent);

            var result = (ReactModels.AccordionWithContentArea)_mapper.GetPartialViewModel(subjectBlock, new SubjectPriorityBlockQueryParameter()).Model;

            Assert.Equal(nameof(ReactModels.TextWithSidebar), result.Content.Blocks.First().ComponentName);
            Assert.Equal(nameof(ReactModels.PortfolioContent), result.Content.Blocks.Last().ComponentName);
        }

        [Fact]
        public void GetPartialViewModel_WithMessageBlockInText_ShouldMapIconWarning()
        {
            var expectedIconWarningTheme = ReactModels.IconWarning_Theme.Red;
            var richTextComponentWithMessage = new ReactModels.RichText
            {
                Blocks = new List<ReactModels.ContentAreaItem>
                {
                    new ReactModels.ContentAreaItem
                    {
                        ComponentName = nameof(ReactModels.Message),
                        ComponentData = new ReactModels.Message
                        {
                            Theme = expectedIconWarningTheme
                        }
                    }
                }
            };

            A.CallTo(() => _richTextReactModelBuilder.BuildReactModel(A<XhtmlString>._, A<string>._))
                .Returns(richTextComponentWithMessage);

            var subjectBlock = (SubjectPriorityBlock)SubjectPriorityBlockBuilder.Create.Default()
                .WithText(new XhtmlString());

            var result = (ReactModels.AccordionWithContentArea)_mapper.GetPartialViewModel(subjectBlock, new SubjectPriorityBlockQueryParameter()).Model;

            Assert.Equal(expectedIconWarningTheme, result.IconWarning.Theme);
        }
    }
}