﻿using EPiServer;
using EPiServer.Core;
using EPiServer.Web.Routing;
using FakeItEasy;
using Forskningsradet.Core.Facades.Contracts;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ContentModels.Media;
using Forskningsradet.Core.Models.ReactModels;
using Forskningsradet.Core.ViewModelMappers.Blocks;
using Xunit;

namespace Forskningsradet.UnitTests.Core.ViewModelMappers.Blocks
{
    public class ImageWithCaptionBlockViewModelMapperTests
    {
        private readonly IUrlResolver _urlResolver;
        private readonly IPageEditingAdapter _pageEditingAdapter;
        private readonly IContentLoader _contentLoader;
        private readonly ImageWithCaptionBlockViewModelMapper _mapper;

        public ImageWithCaptionBlockViewModelMapperTests()
        {
            _urlResolver = A.Fake<IUrlResolver>();
            _pageEditingAdapter = A.Fake<IPageEditingAdapter>();
            _contentLoader = A.Fake<IContentLoader>();
            _mapper = new ImageWithCaptionBlockViewModelMapper(_urlResolver, _pageEditingAdapter, _contentLoader);
        }

        [Fact]
        public void GetViewModel_WhenAllFieldsProvided_ShouldMapFields()
        {
            var expectedCaption = "caption";
            var expectedStyle = ImageWithCaption_EditorTheme.HalfWidth;
            var expectedIconUrl = "imageUrl";
            var expectedIconAltText = "alt";

            A.CallTo(() => _contentLoader.Get<ImageFile>(A<ContentReference>._))
                .Returns(new ImageFile { AltText = expectedIconAltText, ContentLink = ContentReference.EmptyReference });
            A.CallTo(() => _urlResolver.GetUrl(A<ContentReference>._, A<string>._, A<UrlResolverArguments>._))
                .Returns(expectedIconUrl);

            var imageWithCaptionBlock = new ImageWithCaptionBlock
            {
                Image = A.Fake<ContentReference>(),
                Caption = expectedCaption,
                Style = ImageBlockStyle.ThemeHalfWidth,
            };

            var result = (ImageWithCaption) _mapper.GetPartialViewModel(imageWithCaptionBlock).Model;

            Assert.Equal(expectedCaption, result.Caption);
            Assert.Equal(expectedStyle, result.EditorTheme);
            Assert.Equal(expectedIconUrl, result.Image.Src);
            Assert.Equal(expectedIconAltText, result.Image.Alt);
        }

        [Fact]
        public void GetViewModel_WhenPageIsInEditMode_ShouldMapFullRefreshProperties()
        {
            var expectedFullRefreshProperties = new[]
            {
                nameof(ImageWithCaptionBlock.Image),
                nameof(ImageWithCaptionBlock.Caption)
            };

            A.CallTo(() => _pageEditingAdapter.PageIsInEditMode())
                .Returns(true);

            var result = _mapper.GetPartialViewModel(new ImageWithCaptionBlock());

            Assert.Equal(expectedFullRefreshProperties, result.FullRefreshProperties);
        }

        [Fact]
        public void GetViewModel_WhenPageIsInEditMode_ShouldMapOnPageEditing()
        {
            A.CallTo(() => _pageEditingAdapter.PageIsInEditMode()).Returns(true);

            var result = (ImageWithCaption)_mapper.GetPartialViewModel(new ImageWithCaptionBlock()).Model;

            Assert.NotNull(result.OnPageEditing);
            Assert.NotNull(result.Image.OnPageEditing);
        }
    }
}
