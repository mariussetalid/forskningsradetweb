using System.Linq;
using EPiServer.Core;
using FakeItEasy;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Facades.Contracts;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.ViewModelMappers.Blocks;
using Xunit;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.UnitTests.Core.ViewModelMappers.Blocks
{
    public class ScheduleBlockViewModelMapperTests
    {
        private readonly IRichTextReactModelBuilder _richTextReactModelBuilder;
        private readonly IAccordionReactModelBuilder _accordionReactModelBuilder;
        private readonly IPageEditingAdapter _pageEditingAdapter;
        private readonly ScheduleBlockViewModelMapper _mapper;

        public ScheduleBlockViewModelMapperTests()
        {
            _richTextReactModelBuilder = A.Fake<IRichTextReactModelBuilder>();
            _accordionReactModelBuilder = A.Fake<IAccordionReactModelBuilder>();
            _pageEditingAdapter = A.Fake<IPageEditingAdapter>();
            _mapper = new ScheduleBlockViewModelMapper(
                    _richTextReactModelBuilder,
                    _accordionReactModelBuilder,
                    _pageEditingAdapter);
        }

        [Fact]
        public void GetViewModel_WhenUnstructuredScheduleIsUsed_ShouldMapUnstructuredScheduleInFirstEntry()
        {
            var expectedRichText = new ReactModels.RichText();

            var block = new ScheduleBlock
            {
                Title = "T",
                Times = new XhtmlString("Unstructured")
            };

            A.CallTo(() => _richTextReactModelBuilder.BuildReactModel(A<XhtmlString>.That.Matches(x => x == block.Times), A<string>._))
                .Returns(expectedRichText);

            var result = (ReactModels.AccordionWithContentAreaList)_mapper.GetPartialViewModel(block).Model;

            var firstSchedule = result.Content.FirstOrDefault();
            var firstScheduleEntry = (firstSchedule.Content.Blocks.FirstOrDefault().ComponentData as ReactModels.Schedule)
                .ProgramList
                .FirstOrDefault();
            Assert.Equal(block.Title, firstSchedule.Title);
            Assert.Same(expectedRichText, firstScheduleEntry.Text);
            Assert.Null(result.Title);
            Assert.Null(result.OnPageEditing);
        }

        [Fact]
        public void GetViewModel_WithSchedule_FirstAccordionShouldBeInitiallyOpen()
        {
            var expectedAccordion = new ReactModels.Accordion
            {
                InitiallyOpen = true
            };
            A.CallTo(() => _accordionReactModelBuilder.BuildReactModel(true)).Returns(expectedAccordion);

            var result = (ReactModels.AccordionWithContentAreaList)_mapper.GetPartialViewModel(new ScheduleBlock()).Model;

            Assert.True(result.Accordion.InitiallyOpen);
        }
    }
}