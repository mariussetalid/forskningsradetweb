using System.Collections.Generic;
using EPiServer;
using EPiServer.DataAbstraction;
using EPiServer.Web.Routing;
using FakeItEasy;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Facades.Contracts;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.QueryParameters;
using Forskningsradet.Core.Models.RequestModels;
using Forskningsradet.Core.Models.SearchModels;
using Forskningsradet.Core.Services;
using Forskningsradet.Core.ViewModelMappers.Blocks;
using Xunit;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.UnitTests.Core.ViewModelMappers.Blocks
{
    public class RelatedEventListBlockViewModelMapperTests
    {
        private readonly IUrlResolver _urlResolver;
        private readonly IEventListService _eventListService;
        private readonly CategoryRepository _categoryRepository;
        private readonly IDateCardReactModelBuilder _dateCardReactModelBuilder;
        private readonly ILocalizationProvider _localizationProvider;
        private readonly IPageEditingAdapter _pageEditingAdapter;
        private readonly RelatedEventListBlockViewModelMapper _mapper;

        public RelatedEventListBlockViewModelMapperTests()
        {
            _urlResolver = A.Fake<IUrlResolver>();
            _eventListService = A.Fake<IEventListService>();
            _categoryRepository = A.Fake<CategoryRepository>();
            _dateCardReactModelBuilder = A.Fake<IDateCardReactModelBuilder>();
            _localizationProvider = A.Fake<ILocalizationProvider>();
            _pageEditingAdapter = A.Fake<IPageEditingAdapter>();
            _mapper = new RelatedEventListBlockViewModelMapper(_urlResolver, _eventListService, _categoryRepository, _dateCardReactModelBuilder, _localizationProvider, _pageEditingAdapter);
        }

        [Fact]
        public void GetViewModel_WhenAllFieldsProvided_ShouldMapFields()
        {
            const string expectedUrl = "url";
            const string expectedEmptyListText = "empty";
            ArrangeGetUrl(expectedUrl);
            ArrangeTranslation(expectedEmptyListText, "EmptyEventList");

            var block = new RelatedEventListBlock
            {
                Title = "T",
                Button = new EditLinkBlock
                {
                    Text = "L",
                    Link = new Url("u")
                }
            };

            var result = (ReactModels.RelatedDates)_mapper.GetPartialViewModel(block).Model;

            Assert.Equal(block.Title, result.RelatedContent.Title);
            Assert.Equal(block.Button.Text, result.RelatedContent.Link.Text);
            Assert.Equal(expectedUrl, result.RelatedContent.Link.Url);
            Assert.Equal(expectedEmptyListText, result.RelatedContent.EmptyList.Text);
        }

        [Fact]
        public void GetViewModel_WhenEventsAreReturned_ShouldMapAllEvents()
        {
            var dateCard = new ReactModels.DateCard();
            ArrangeEventSearch(new List<EventPage>
                {
                    new EventPage(),
                    new EventPage(),
                    new EventPage()
                });
            ArrangeDateCardMapping(dateCard);

            var result = (ReactModels.RelatedDates)_mapper.GetPartialViewModel(new RelatedEventListBlock()).Model;

            Assert.Equal(3, result.Dates.Count);
        }

        [Fact]
        public void GetViewModel_WhenSearchingForEvents_ShouldOnlySearchForFutureEvents()
        {
            var futureEvent = new EventPage();
            var futureDateCard = new ReactModels.DateCard();
            ArrangeFutureEventSearch(new List<EventPage>
            {
                futureEvent
            });
            A.CallTo(() => _dateCardReactModelBuilder.BuildReactModel(futureEvent, A<RenderOptionQueryParameter>._, false, false, false))
                .Returns(futureDateCard);

            var result = (ReactModels.RelatedDates) _mapper.GetPartialViewModel(new RelatedEventListBlock()).Model;

            Assert.Same(futureDateCard, result.Dates[0]);
        }

        [Fact]
        public void GetViewModel_WhenButtonTextIsNull_ShouldFallbackToLabel()
        {
            const string expectedLabel = "label";
            ArrangeTranslation(expectedLabel, "GoToEventsButton");

            var block = new RelatedEventListBlock
            {
                Button = new EditLinkBlock
                {
                    Link = new Url("u")
                }
            };

            var result = (ReactModels.RelatedDates)_mapper.GetPartialViewModel(block).Model;

            Assert.Equal(expectedLabel, result.RelatedContent.Link.Text);
        }

        [Fact]
        public void GetViewModel_WhenPageIsInEditMode_ShouldMapOnPageEditing()
        {
            A.CallTo(() => _pageEditingAdapter.PageIsInEditMode()).Returns(true);

            var result = (ReactModels.RelatedDates)_mapper.GetPartialViewModel(new RelatedEventListBlock()).Model;

            Assert.NotNull(result.RelatedContent.OnPageEditing);
        }

        private void ArrangeEventSearch(List<EventPage> eventList)
        {
            var result = new EventFilterSearchResults(
                eventList,
                new List<CategoryGroup>(),
                eventList.Count);
            
            A.CallTo(() => _eventListService.Search(A<EventListRequestModel>._))
                .Returns(result);
        }

        private void ArrangeFutureEventSearch(List<EventPage> eventList)
        {
            var result = new EventFilterSearchResults(
                eventList,
                new List<CategoryGroup>(),
                eventList.Count);
            
            A.CallTo(() => _eventListService.Search(A<EventListRequestModel>.That.Matches(x => x.TimeFrame == TimeFrameFilter.Future)))
                .Returns(result);
        }

        private void ArrangeDateCardMapping(ReactModels.DateCard dateCard) =>
            A.CallTo(() =>
                    _dateCardReactModelBuilder.BuildReactModel(A<EventPage>._, null, true, false, false))
                .Returns(dateCard);

        private void ArrangeGetUrl(string expectedUrl) =>
            A.CallTo(() => _urlResolver.GetUrl(A<UrlBuilder>._, A<UrlResolverArguments>._))
                .Returns(expectedUrl);

        private void ArrangeTranslation(string expected, string label) =>
            A.CallTo(() => _localizationProvider.GetLabel(nameof(RelatedContentListBlock), label))
                .Returns(expected);
    }
}