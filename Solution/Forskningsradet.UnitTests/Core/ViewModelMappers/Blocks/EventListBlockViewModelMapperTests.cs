using System.Collections.Generic;
using EPiServer;
using EPiServer.Web.Routing;
using FakeItEasy;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Facades.Contracts;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.RequestModels;
using Forskningsradet.Core.Models.SearchModels;
using Forskningsradet.Core.Services;
using Forskningsradet.Core.ViewModelMappers.Blocks;
using Xunit;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.UnitTests.Core.ViewModelMappers.Blocks
{
    public class EventListBlockViewModelMapperTests
    {
        private readonly IUrlResolver _urlResolver;
        private readonly IEventListService _eventListService;
        private readonly IDateCardReactModelBuilder _dateCardReactModelBuilder;
        private readonly ILocalizationProvider _localizationProvider;
        private readonly IPageEditingAdapter _pageEditingAdapter;
        private readonly EventListBlockViewModelMapper _mapper;

        public EventListBlockViewModelMapperTests()
        {
            _urlResolver = A.Fake<IUrlResolver>();
            _eventListService = A.Fake<IEventListService>();
            _dateCardReactModelBuilder = A.Fake<IDateCardReactModelBuilder>();
            _localizationProvider = A.Fake<ILocalizationProvider>();
            _pageEditingAdapter = A.Fake<IPageEditingAdapter>();
            _mapper = new EventListBlockViewModelMapper(_urlResolver, _eventListService, _dateCardReactModelBuilder, _localizationProvider, _pageEditingAdapter);
        }

        [Fact]
        public void GetViewModel_WhenAllFieldsProvided_ShouldMapFields()
        {
            const string expectedUrl = "url";
            A.CallTo(() => _urlResolver.GetUrl(A<UrlBuilder>._, A<UrlResolverArguments>._))
                .Returns(expectedUrl);
            ArrangeEventSearch(new List<EventPage> { new EventPage() });

            var block = new EventListBlock
            {
                Title = "T",
                Description = "D",
                Button = new EditLinkBlock
                {
                    Text = "BT",
                    Link = new Url(expectedUrl)
                }
            };

            var result = (ReactModels.FutureEvents)_mapper.GetPartialViewModel(block).Model;

            Assert.Equal(block.Title, result.Title);
            Assert.Equal(block.Description, result.Text);
            Assert.Equal(block.Button.Text, result.Link.Text);
            Assert.Equal(block.Button.Link.ToString(), result.Link.Url);
            Assert.Equal(expectedUrl, result.Link.Url);
        }

        [Fact]
        public void GetViewModel_WhenEventsAreReturned_ShouldMapEvents()
        {
            var dateCard = new ReactModels.DateCard();
            ArrangeEventSearch(new List<EventPage> {new EventPage()});
            ArrangeDateCardMapping(dateCard);

            var result = (ReactModels.FutureEvents) _mapper.GetPartialViewModel(new EventListBlock()).Model;

            Assert.Same(dateCard, result.Items[0]);
        }

        [Fact]
        public void GetViewModel_WhenSearchingForEvents_ShouldSpecifyUpToThreeEventsButTakeAllEventsThatAreReturned()
        {
            ArrangeEventSearch(new List<EventPage>
            {
                new EventPage(),
                new EventPage(),
                new EventPage(),
                new EventPage(),
            });
            ArrangeDateCardMapping(new ReactModels.DateCard());

            var result = (ReactModels.FutureEvents) _mapper.GetPartialViewModel(new EventListBlock()).Model;

            A.CallTo(() => _eventListService.Search(A<EventListRequestModel>.That.Matches(x => x.Take == 3)))
                .MustHaveHappened();

            Assert.Equal(4, result.Items.Count);
        }

        [Fact]
        public void GetViewModel_WhenSearchingForEvents_ShouldOnlySearchForFutureEvents()
        {
            var futureEvent = new EventPage();
            var futureDateCard = new ReactModels.DateCard();

            ArrangeFutureEventSearch(new List<EventPage>
            {
                futureEvent
            });

            A.CallTo(() => _dateCardReactModelBuilder.BuildReactModel(futureEvent, null, true, false, false))
                .Returns(futureDateCard);

            var result = (ReactModels.FutureEvents) _mapper.GetPartialViewModel(new EventListBlock()).Model;

            Assert.Same(futureDateCard, result.Items[0]);
        }

        [Fact]
        public void GetViewModel_WhenButtonTextIsNull_ShouldFallbackToLabel()
        {
            const string expectedLabel = "label";
            ArrangeTranslation(expectedLabel, "GoToEventsButton");

            var block = new EventListBlock
            {
                Button = new EditLinkBlock
                {
                    Link = new Url("url")
                }
            };

            var result = (ReactModels.FutureEvents)_mapper.GetPartialViewModel(block).Model;

            Assert.Equal(expectedLabel, result.Link.Text);
        }

        [Fact]
        public void GetViewModel_WhenPageIsInEditMode_ShouldMapOnPageEditing()
        {
            A.CallTo(() => _pageEditingAdapter.PageIsInEditMode()).Returns(true);
            var result = (ReactModels.FutureEvents)_mapper.GetPartialViewModel(new EventListBlock()).Model;

            Assert.NotNull(result.Link.OnPageEditing);
        }
        
        private void ArrangeEventSearch(List<EventPage> eventList)
        {
            var result = new EventFilterSearchResults(
                eventList,
                new List<CategoryGroup>(),
                eventList.Count);
            
            A.CallTo(() => _eventListService.Search(A<EventListRequestModel>._))
                .Returns(result);
        }

        private void ArrangeFutureEventSearch(List<EventPage> eventList)
        {
            var result = new EventFilterSearchResults(
                eventList,
                new List<CategoryGroup>(),
                eventList.Count);
            
            A.CallTo(() => _eventListService.Search(A<EventListRequestModel>.That.Matches(x => x.TimeFrame == TimeFrameFilter.Future)))
                .Returns(result);
        }

        private void ArrangeDateCardMapping(ReactModels.DateCard dateCard) =>
            A.CallTo(() => _dateCardReactModelBuilder.BuildReactModel(A<EventPage>._, null, true, false, false))
                .Returns(dateCard);

        private void ArrangeTranslation(string expected, string label) =>
            A.CallTo(() => _localizationProvider.GetLabel(nameof(EventListPage), label))
                .Returns(expected);
    }
}