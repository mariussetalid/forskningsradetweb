using System;
using EPiServer.Core;
using FakeItEasy;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Facades.Contracts;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Services;
using Forskningsradet.Core.ViewModelMappers.Blocks;
using Xunit;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.UnitTests.Core.ViewModelMappers.Blocks
{
    public class AccordionBlockViewModelMapperTests
    {
        private readonly IRichTextReactModelBuilder _richTextReactModelBuilder;
        private readonly ILocalizationProvider _localizationProvider;
        private readonly IPageEditingAdapter _pageEditingAdapter;
        private readonly AccordionBlockViewModelMapper _mapper;

        public AccordionBlockViewModelMapperTests()
        {
            _richTextReactModelBuilder = A.Fake<IRichTextReactModelBuilder>();
            _localizationProvider = A.Fake<ILocalizationProvider>();
            _pageEditingAdapter = A.Fake<IPageEditingAdapter>();
            _mapper = new AccordionBlockViewModelMapper(_richTextReactModelBuilder, _localizationProvider, _pageEditingAdapter);
        }

        [Fact]
        public void GetViewModel_WhenAllFieldsProvided_ShouldMapFields()
        {
            var expectedLabel = "label";
            var expectedRichText = new ReactModels.RichText();

            A.CallTo(() => _richTextReactModelBuilder.BuildReactModel(A<XhtmlString>._, A<string>._))
                .Returns(expectedRichText);
            A.CallTo(() => _localizationProvider.GetLabel(nameof(AccordionBlock), A<string>._))
                .Returns(expectedLabel);

            var accordionBlock = new AccordionBlock
            {
                Title = "T",
                InitiallyOpen = true,
            };

            var result = (ReactModels.AccordionBlock)_mapper.GetPartialViewModel(accordionBlock).Model;

            Assert.Equal(accordionBlock.Title, result.Title);
            Assert.Equal(expectedRichText, result.Text);
            Assert.Equal(expectedLabel, result.Accordion.CollapseLabel);
            Assert.Equal(expectedLabel, result.Accordion.ExpandLabel);
            Assert.Equal(accordionBlock.InitiallyOpen, result.Accordion.InitiallyOpen);
            Assert.Null(result.OnPageEditing);
            Assert.True(Guid.TryParse(result.Accordion.Guid, out Guid guid));
        }
        
        [Fact]
        public void GetViewModel_WhenPageIsInEditMode_ShouldBeInitiallyOpen()
        {
            A.CallTo(() => _pageEditingAdapter.PageIsInEditMode()).Returns(true);
            var result = (ReactModels.AccordionBlock)_mapper.GetPartialViewModel(new AccordionBlock()).Model;

            Assert.True(result.Accordion.InitiallyOpen);
        }

        [Fact]
        public void GetViewModel_WhenPageIsInEditMode_ShouldMapOnPageEditing()
        {
            A.CallTo(() => _pageEditingAdapter.PageIsInEditMode()).Returns(true);
            var result = (ReactModels.AccordionBlock)_mapper.GetPartialViewModel(new AccordionBlock()).Model;

            Assert.NotNull(result.OnPageEditing);
        }
    }
}