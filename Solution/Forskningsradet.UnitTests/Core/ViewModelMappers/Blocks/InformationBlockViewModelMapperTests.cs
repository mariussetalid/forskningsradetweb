using EPiServer;
using EPiServer.Core;
using EPiServer.Web.Routing;
using FakeItEasy;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Facades.Contracts;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ContentModels.Media;
using Forskningsradet.Core.Models.ReactModels;
using Forskningsradet.Core.ViewModelMappers.Blocks;
using Forskningsradet.UnitTests.TestUtils;
using Xunit;

namespace Forskningsradet.UnitTests.Core.ViewModelMappers.Blocks
{
    public class InformationBlockViewModelMapperTests
    {
        private readonly IRichTextReactModelBuilder _richTextReactModelBuilder;
        private readonly IUrlResolver _urlResolver;
        private readonly IPageEditingAdapter _pageEditingAdapter;
        private readonly InformationBlockViewModelMapper _mapper;

        public InformationBlockViewModelMapperTests()
        {
            _richTextReactModelBuilder = A.Fake<IRichTextReactModelBuilder>();
            _urlResolver = A.Fake<IUrlResolver>();
            _pageEditingAdapter = A.Fake<IPageEditingAdapter>();
            _mapper = new InformationBlockViewModelMapper(_richTextReactModelBuilder, _urlResolver, _pageEditingAdapter);
        }

        [Fact]
        public void GetViewModel_ShouldMapFields_WhenAllFieldsProvided()
        {
            const string expectedTitle = "title";
            const string expectedUrl = "url";
            const string expectedButtonText = "link text";
            const string expectedEditLinkUrl = "edit link url";
            const string expectedIconAltText = "alt text";
            const string expectedIconUrl = "icon url";
            const InformationBlockStyle expectedStyle = InformationBlockStyle.ThemeBlue;
            var expectedRichText = new RichText();
            
            var contentLoader = A.Fake<IContentLoader>();

            A.CallTo(() => contentLoader.Get<ImageFile>(A<ContentReference>._))
                .Returns(new ImageFile {AltText = expectedIconAltText, ContentLink = ContentReference.EmptyReference});
            A.CallTo(() => _urlResolver.GetUrl(A<UrlBuilder>.That.Matches(x => x.Path == expectedUrl), A<UrlResolverArguments>._ ))
                .Returns(expectedUrl);
            A.CallTo(() => _urlResolver.GetUrl(A<UrlBuilder>.That.Matches(x => x.Path == expectedEditLinkUrl), A<UrlResolverArguments>._ ))
                .Returns(expectedEditLinkUrl);
            A.CallTo(() => _urlResolver.GetUrl(A<ContentReference>._, A<string>._, A<UrlResolverArguments>._ ))
                .Returns(expectedIconUrl);
            A.CallTo(() => _richTextReactModelBuilder.BuildReactModel(A<XhtmlString>._, A<string>._))
                .Returns(expectedRichText);

            using (new FakeServiceLocator().WithContentLoader(contentLoader))
            {
                var informationBlock = new InformationBlock
                {
                    Title = expectedTitle,
                    Url = new Url(expectedUrl),
                    Button = new EditLinkBlock {Text = expectedButtonText, Link = new Url(expectedEditLinkUrl)},
                    Icon = A.Fake<ContentReference>(),
                    Style = expectedStyle
                };

                var result = (InfoBlock)_mapper.GetPartialViewModel(informationBlock).Model;

                Assert.Equal(expectedTitle, result.Title);
                Assert.Equal(expectedUrl, result.Url);
                Assert.Equal(expectedRichText, result.Text);
                Assert.Equal(expectedButtonText, result.Cta.Text);
                Assert.Equal(expectedEditLinkUrl, result.Cta.Url);
                Assert.Equal(expectedIconAltText, result.Icon.Alt);
                Assert.Equal(expectedIconUrl, result.Icon.Src);
                Assert.Equal((int)expectedStyle, (int)result.EditorTheme);
            }
        }

        [Theory]
        [InlineData(InformationBlockStyle.ThemeBlue, InfoBlock_EditorTheme.Blue)]
        [InlineData(InformationBlockStyle.ThemeDarkBlue, InfoBlock_EditorTheme.DarkBlue)]
        [InlineData(InformationBlockStyle.ThemeOrange, InfoBlock_EditorTheme.Orange)]
        public void GetViewModel_GivenStyle_ShouldMapCorrectStyle(InformationBlockStyle style, InfoBlock_EditorTheme expectedTheme)
        {
            var informationBlock = new InformationBlock
            {
                Style = style
            };

            var result = (InfoBlock)_mapper.GetPartialViewModel(informationBlock).Model;

            Assert.Equal(expectedTheme, result.EditorTheme);
        }

        [Fact]
        public void GetViewModel_WhenIconIsNotSet_ShouldNotMapIcon()
        {
            var informationBlock = new InformationBlock
            {
                Icon = null
            };

            var result = (InfoBlock)_mapper.GetPartialViewModel(informationBlock).Model;

            Assert.Null(result.Icon);
        }
        
        [Fact]
        public void GetViewModel_WhenButtonIsNotSet_ShouldMapNotEmptyLink()
        {
            var informationBlock = new InformationBlock
            {
                Button = null
            };

            var result = (InfoBlock)_mapper.GetPartialViewModel(informationBlock).Model;

            Assert.NotNull(result.Cta);
        }

        [Fact]
        public void GetViewModel_WhenPageIsInEditMode_ShouldMapOnPageEditing()
        {
            A.CallTo(() => _pageEditingAdapter.PageIsInEditMode()).Returns(true);

            var result = (InfoBlock)_mapper.GetPartialViewModel(new InformationBlock()).Model;

            Assert.NotNull(result.OnPageEditing);
            Assert.NotNull(result.Cta.OnPageEditing);
            Assert.NotNull(result.Icon.OnPageEditing);
        }
    }
}