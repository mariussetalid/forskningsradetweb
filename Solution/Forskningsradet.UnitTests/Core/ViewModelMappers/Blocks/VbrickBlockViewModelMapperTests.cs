using FakeItEasy;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Facades.Contracts;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ReactModels;
using Forskningsradet.Core.ViewModelMappers.Blocks;
using Xunit;

namespace Forskningsradet.UnitTests.Core.ViewModelMappers.Blocks
{
    public class VbrickBlockViewModelMapperTests
    {
        const string ValidSrcStart = EmbedSourceConstants.Vbrick.ValidSrcStart;

        private readonly VbrickBlockViewModelMapper _mapper;
        private readonly IPageEditingAdapter _pageEditingAdapter;

        public VbrickBlockViewModelMapperTests()
        {
            _pageEditingAdapter = A.Fake<IPageEditingAdapter>();
            _mapper = new VbrickBlockViewModelMapper(_pageEditingAdapter);
        }

        [Fact]
        public void GetViewModel_WhenAllFieldsProvided_ShouldMapFields()
        {
            var block = new VbrickBlock
            {
                AlternativeText = "Alt",
                Src = ValidSrcStart,
                Width = 20,
                Height = 30
            };

            var result = (EmbedBlock)_mapper.GetPartialViewModel(block).Model;

            Assert.Equal(block.AlternativeText, result.Title);
            Assert.Equal(block.Src, result.Src);
            Assert.Equal(block.Width, result.Width);
            Assert.Equal(block.Height, result.Height);
        }
        
        [Theory]
        [InlineData(null)]
        [InlineData(" ")]
        [InlineData("Something")]
        [InlineData("http://videoportal.rcn.no/")]
        public void GetViewModel_WhenSrcIsInvalid_ShouldNotMapSrc(string invalidSrc)
        {
            var block = new VbrickBlock
            {
                Src = invalidSrc
            };

            var result = (EmbedBlock)_mapper.GetPartialViewModel(block).Model;

            Assert.Null(result.Src);
        }
                        
        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public void GetViewModel_WhenDimensionsAreInvalid_ShouldNotMapDimensions(int invalidNumber)
        {
            var block = new VbrickBlock
            {
                Width = invalidNumber,
                Height = invalidNumber
            };

            var result = (EmbedBlock)_mapper.GetPartialViewModel(block).Model;

            Assert.Null(result.Width);
            Assert.Null(result.Height);
        }

        [Fact]
        public void GetViewModel_WhenPageIsInEditMode_ShouldMapOnPageEditing()
        {
            A.CallTo(() => _pageEditingAdapter.PageIsInEditMode()).Returns(true);

            var result = (EmbedBlock)_mapper.GetPartialViewModel(new VbrickBlock()).Model;

            Assert.NotNull(result.OnPageEditing);
        }
    }
}