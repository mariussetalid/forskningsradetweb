using System.Collections.Generic;
using EPiServer;
using EPiServer.Core;
using EPiServer.Web.Routing;
using FakeItEasy;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Facades.Contracts;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.ViewModelMappers.Blocks;
using Xunit;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.UnitTests.Core.ViewModelMappers.Blocks
{
    public class PageLinksBlockViewModelMapperTests
    {
        private readonly IContentLoader _contentLoader;
        private readonly IUrlResolver _urlResolver;
        private readonly IContentAreaReactModelBuilder _contentAreaReactModelBuilder;
        private readonly IPageEditingAdapter _pageEditingAdapter;
        private readonly PageLinksBlockViewModelMapper _mapper;

        public PageLinksBlockViewModelMapperTests()
        {
            _contentLoader = A.Fake<IContentLoader>();
            _urlResolver = A.Fake<IUrlResolver>();
            _contentAreaReactModelBuilder = A.Fake<IContentAreaReactModelBuilder>();
            _pageEditingAdapter = A.Fake<IPageEditingAdapter>();
            _mapper = new PageLinksBlockViewModelMapper(_contentLoader, _urlResolver, _contentAreaReactModelBuilder, _pageEditingAdapter);
        }

        [Fact]
        public void GetViewModel_WhenAllFieldsProvided_ShouldMapFields()
        {
            var fakeContentArea = A.Fake<ContentArea>();
            A.CallTo(() => fakeContentArea.FilteredItems)
                .Returns(new List<ContentAreaItem> {
                    new ContentAreaItem { ContentLink = new ContentReference(1) },
                });

            var block = new PageLinksBlock
            {
                Title = "The title",
                Text = "The text",
                SecondPageGroup = fakeContentArea
            };

            var result = (ReactModels.PageLinksBlock)_mapper.GetPartialViewModel(block).Model;

            Assert.Equal(block.Title, result.Title);
            Assert.Equal(block.Text, result.Text);
        }

        [Fact]
        public void GetViewModel_WhenPagesArePresent_ShouldMapLinks()
        {
            var expectedHighlightedLinkWithText = new ReactModels.LinkWithText
            {
                Link = new ReactModels.Link(),
                Text = ""
            };
            var expectedLinkWithText = new ReactModels.LinkWithText
            {
                Link = new ReactModels.Link(),
                Text = ""
            };
            var fakeContentArea = A.Fake<ContentArea>();
            var linkedPage = A.Fake<EditorialPage>();
            linkedPage.ContentLink = new ContentReference(1);
            linkedPage.PageLink = new PageReference(1);
            A.CallTo(() => _contentAreaReactModelBuilder.BuildReactModel(A<IList<ReactModels.ContentAreaItem>>._, A<string>.That.Matches(x => x.Equals(nameof(PageLinksBlock.FirstPageGroup)))))
                .Returns(new ReactModels.ContentArea { Blocks = new List<ReactModels.ContentAreaItem> { new ReactModels.ContentAreaItem { ComponentData = expectedHighlightedLinkWithText } } });
            A.CallTo(() => _contentAreaReactModelBuilder.BuildReactModel(A<IList<ReactModels.ContentAreaItem>>._, A<string>.That.Matches(x => x.Equals(nameof(PageLinksBlock.SecondPageGroup)))))
                .Returns(new ReactModels.ContentArea { Blocks = new List<ReactModels.ContentAreaItem> { new ReactModels.ContentAreaItem { ComponentData = expectedLinkWithText } } });
            A.CallTo(() => fakeContentArea.FilteredItems)
                .Returns(new List<ContentAreaItem> {
                    new ContentAreaItem { ContentLink = new ContentReference(1) },
                });
            A.CallTo(() => _contentLoader.Get<EditorialPage>(A<ContentReference>._))
                .Returns(linkedPage);

            var block = new PageLinksBlock
            {
                FirstPageGroup = fakeContentArea,
                SecondPageGroup = fakeContentArea
            };

            var result = (ReactModels.PageLinksBlock)_mapper.GetPartialViewModel(block).Model;

            var resultHighlightedLink = (ReactModels.LinkWithText) result.HighlightedLinks.Blocks[0].ComponentData;
            Assert.Same(expectedHighlightedLinkWithText, resultHighlightedLink);

            var resultLink = (ReactModels.LinkWithText) result.LinkLists[0].Links.Blocks[0].ComponentData;
            Assert.NotNull(result.LinkLists[0].Id);
            Assert.Same(expectedLinkWithText, resultLink);
        }

        [Fact]
        public void GetViewModel_WhenPagesArePresent_ShouldNotMapSizes()
        {
            var fakeContentArea = A.Fake<ContentArea>();
            A.CallTo(() => _contentAreaReactModelBuilder.BuildReactModel(A<IList<ReactModels.ContentAreaItem>>._, A<string>._))
                .Returns(new ReactModels.ContentArea { Blocks = new List<ReactModels.ContentAreaItem> { new ReactModels.ContentAreaItem { Size = ReactModels.ContentAreaItem_Size.None } } });
            A.CallTo(() => fakeContentArea.FilteredItems)
                .Returns(new List<ContentAreaItem> {
                    new ContentAreaItem { ContentLink = new ContentReference(1) },
                });
            A.CallTo(() => _contentLoader.Get<EditorialPage>(A<ContentReference>._))
                .Returns(A.Fake<EditorialPage>());

            var block = new PageLinksBlock
            {
                FirstPageGroup = fakeContentArea,
                SecondPageGroup = fakeContentArea
            };

            var result = (ReactModels.PageLinksBlock)_mapper.GetPartialViewModel(block).Model;

            Assert.Equal(ReactModels.ContentAreaItem_Size.None, result.HighlightedLinks.Blocks[0].Size);
            Assert.Equal(ReactModels.ContentAreaItem_Size.None, result.LinkLists[0].Links.Blocks[0].Size);
        }

        [Fact]
        public void GetViewModel_WhenPageIsInEditMode_ShouldMapOnPageEditing()
        {
            var fakeContentArea = A.Fake<ContentArea>();
            A.CallTo(() => fakeContentArea.FilteredItems)
                .Returns(new List<ContentAreaItem> { new ContentAreaItem { ContentLink = new ContentReference(1) } });
            A.CallTo(() => _pageEditingAdapter.PageIsInEditMode()).Returns(true);
            A.CallTo(() => _contentAreaReactModelBuilder.BuildReactModel(A<IList<ReactModels.ContentAreaItem>>._, A<string>._))
                .Returns(new ReactModels.ContentArea{OnPageEditing = new ReactModels.ContentArea_OnPageEditing()});

            var block = new PageLinksBlock
            {
                SecondPageGroup = fakeContentArea
            };

            var result = (ReactModels.PageLinksBlock)_mapper.GetPartialViewModel(block).Model;

            Assert.NotNull(result.OnPageEditing);
            Assert.NotNull(result.HighlightedLinks.OnPageEditing);
            Assert.NotNull(result.LinkLists[0].Links.OnPageEditing);
        }
    }
}