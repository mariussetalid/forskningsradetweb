using FakeItEasy;
using Forskningsradet.Core.Facades.Contracts;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ReactModels;
using Forskningsradet.Core.ViewModelMappers.Blocks;
using Xunit;

namespace Forskningsradet.UnitTests.Core.ViewModelMappers.Blocks
{
    public class QuoteBlockViewModelMapperTests
    {
        private readonly QuoteBlockViewModelMapper _mapper;
        private readonly IPageEditingAdapter _pageEditingAdapter;

        public QuoteBlockViewModelMapperTests()
        {
            _pageEditingAdapter = A.Fake<IPageEditingAdapter>();
            _mapper = new QuoteBlockViewModelMapper(_pageEditingAdapter);
        }

        [Fact]
        public void GetViewModel_ShouldMapFields_WhenAllFieldsProvided()
        {
            const string expectedQuote = "Q";
            const string expectedQuoteBy = "R";

            var quoteBlock = new QuoteBlock
            {
                Quote = expectedQuote,
                QuoteBy = expectedQuoteBy
            };

            var result = (Quote)_mapper.GetPartialViewModel(quoteBlock).Model;

            Assert.Equal(expectedQuote, result.Text);
            Assert.Equal(expectedQuoteBy, result.QuoteBy);
        }
    }
}