using System.Collections.Generic;
using System.Linq;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using FakeItEasy;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Facades.Contracts;
using Forskningsradet.Core.Factories;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.QueryParameters;
using Forskningsradet.Core.Models.ViewModels.Blocks;
using Forskningsradet.Core.Services;
using Forskningsradet.Core.ViewModelMappers.Blocks;
using Forskningsradet.UnitTests.Builders;
using Xunit;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.UnitTests.Core.ViewModelMappers.Blocks
{
    public class SubjectBlockViewModelMapperTests
    {
        private readonly IRichTextReactModelBuilder _richTextReactModelBuilder;
        private readonly IContentAreaReactModelBuilder _contentAreaReactModelBuilder;
        private readonly ILocalizationProvider _localizationProvider;
        private readonly ICategoryLocalizationProvider _categoryLocalizationProvider;
        private readonly CategoryRepository _categoryRepository;
        private readonly IPageEditingAdapter _pageEditingAdapter;
        private readonly SubjectBlockViewModelMapper _mapper;
        private readonly IViewModelFactory _viewModelFactory;
        private readonly IContentLoader _contentLoader;

        public SubjectBlockViewModelMapperTests()
        {
            _richTextReactModelBuilder = A.Fake<IRichTextReactModelBuilder>();
            _contentAreaReactModelBuilder = A.Fake<IContentAreaReactModelBuilder>();
            _localizationProvider = A.Fake<ILocalizationProvider>();
            _categoryLocalizationProvider = A.Fake<ICategoryLocalizationProvider>();
            _categoryRepository = A.Fake<CategoryRepository>();
            _pageEditingAdapter = A.Fake<IPageEditingAdapter>();
            _viewModelFactory = A.Fake<IViewModelFactory>();
            _contentLoader = A.Fake<IContentLoader>();
            _mapper = new SubjectBlockViewModelMapper(
                    _richTextReactModelBuilder,
                    _contentAreaReactModelBuilder,
                    _localizationProvider,
                    _categoryLocalizationProvider,
                    _categoryRepository,
                    _pageEditingAdapter,
                    _viewModelFactory,
                    _contentLoader);
        }

        [Fact]
        public void WithDefaultValuesOldDesign_ShouldMapDefaultValues()
        {
            const string expectedTitle = "Title";
            const string expectedDescription = "Description";
            const string expectedHtmlId = "Html id";
            const string expectedText = "Text aka subjectchildren";

            var expectedRichTextComponent = new ReactModels.RichText();
            var expectedContentAreaComponent = new ReactModels.ContentArea();

            var subjectBlock = (SubjectBlock)SubjectBlockBuilder.Create.Default()
                .WithSubjectName(expectedTitle)
                .WithDescription(expectedDescription)
                .WithSubjectCode(expectedHtmlId)
                .WithSubjectChildren(expectedText);

            A.CallTo(() => _richTextReactModelBuilder.BuildReactModel(A<XhtmlString>._, A<string>._))
                .Returns(expectedRichTextComponent);
            A.CallTo(() => _contentAreaReactModelBuilder.BuildReactModel(A<List<ReactModels.ContentAreaItem>>._, A<string>._))
                .Returns(expectedContentAreaComponent);

            var result = (ReactModels.AccordionWithContentAreaOld)_mapper.GetPartialViewModel(subjectBlock, new SubjectBlockQueryParameter()).Model;

            Assert.Equal(expectedTitle, result.Title);
            Assert.Equal(expectedDescription, result.Description);
            Assert.Equal(expectedHtmlId, result.HtmlId);
            Assert.Equal(expectedText, result.Text);
            Assert.Same(expectedRichTextComponent, result.RichText);
            Assert.Same(expectedContentAreaComponent, result.Content);
            Assert.NotNull(result.Accordion);
        }

        [Fact]
        public void WithDefaultValues_ShouldMapDefaultValues()
        {
            const string expectedTitle = "Title";
            const string expectedDescription = "Description";
            const string expectedHtmlId = "Html id";
            const string expectedText = "Text aka subjectchildren";

            var expectedRichTextComponent = new ReactModels.RichText();

            var subjectBlock = (SubjectBlock)SubjectBlockBuilder.Create.Default()
                .WithSubjectName(expectedTitle)
                .WithDescription(expectedDescription)
                .WithSubjectCode(expectedHtmlId)
                .WithSubjectChildren(expectedText);

            A.CallTo(() => _richTextReactModelBuilder.BuildReactModel(A<XhtmlString>._, A<string>._))
                .Returns(expectedRichTextComponent);

            var result = (ReactModels.AccordionWithContentAreaList)_mapper.GetPartialViewModel(subjectBlock, new SubjectBlockQueryParameter { UseAlternativeDesign = true }).Model;

            Assert.Equal(expectedTitle, result.Title);
            Assert.Equal(expectedDescription, result.Description);
            Assert.Equal(expectedHtmlId, result.HtmlId);
            Assert.Equal(expectedText, result.Text);
            Assert.Same(expectedRichTextComponent, result.RichText);
            Assert.NotNull(result.Accordion);
        }

        [Fact]
        public void GetPartialViewModel_WithSubjectPriorityBlock_ShouldMapAccordionWithContentArea()
        {
            var expectedAccordionWithContentArea = A.Fake<ReactModels.AccordionWithContentArea>();

            var subjectBlock = (SubjectBlock)SubjectBlockBuilder.Create.Default()
                .WithPriorities(ContentAreaBuilder.Create.Default().WithDefaultFilteredItems());

            A.CallTo(() => _contentLoader.Get<IContent>(A<ContentReference>._)).Returns(A.Fake<IContent>());
            A.CallTo(() => _viewModelFactory.GetPartialViewModel(A<IContent>._, A<SubjectPriorityBlockQueryParameter>._))
                .Returns(
                    new ReactPartialViewModel(
                        new SubjectPriorityBlock().ReactComponentName(),
                        expectedAccordionWithContentArea
                    )
                );

            var result = (ReactModels.AccordionWithContentAreaList)_mapper.GetPartialViewModel(subjectBlock, new SubjectBlockQueryParameter { UseAlternativeDesign = true }).Model;

            Assert.Same(expectedAccordionWithContentArea, result.Content.FirstOrDefault());
        }

        [Fact]
        public void GetPartialViewModel_WithSubjectChildrenDescription_ShouldShowContent()
        {
            var subjectBlock = (SubjectBlock)SubjectBlockBuilder.Create.Default()
                .WithSubjectChildrenDescription(new XhtmlString());

            var result = (ReactModels.AccordionWithContentAreaList)_mapper.GetPartialViewModel(subjectBlock, new SubjectBlockQueryParameter { UseAlternativeDesign = true }).Model;

            Assert.NotNull(result.Content);
        }

        [Fact]
        public void GetPartialViewModel_WithSubjectChildren_ShouldShowContent()
        {
            var subjectBlock = (SubjectBlock)SubjectBlockBuilder.Create.Default()
                .WithSubjectChildren("Subject children");

            var result = (ReactModels.AccordionWithContentAreaList)_mapper.GetPartialViewModel(subjectBlock, new SubjectBlockQueryParameter { UseAlternativeDesign = true }).Model;

            Assert.NotNull(result.Content);
        }

        [Fact]
        public void GetPartialViewModel_WithPrioritiesFilteredItems_ShouldShowContent()
        {
            var subjectBlock = (SubjectBlock)SubjectBlockBuilder.Create.Default()
                .WithPriorities(ContentAreaBuilder.Create.Default().WithDefaultFilteredItems());

            var result = (ReactModels.AccordionWithContentAreaList)_mapper.GetPartialViewModel(subjectBlock, new SubjectBlockQueryParameter { UseAlternativeDesign = true }).Model;

            Assert.NotNull(result.Content);
        }
    }
}