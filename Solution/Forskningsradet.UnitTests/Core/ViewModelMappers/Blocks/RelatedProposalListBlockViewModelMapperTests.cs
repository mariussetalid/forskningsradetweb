using System.Collections.Generic;
using EPiServer;
using EPiServer.DataAbstraction;
using EPiServer.Web.Routing;
using FakeItEasy;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Facades.Contracts;
using Forskningsradet.Core.Models.ApiModels.Evurdering;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.QueryParameters;
using Forskningsradet.Core.Models.RequestModels;
using Forskningsradet.Core.Models.SearchModels;
using Forskningsradet.Core.Repositories.Contracts;
using Forskningsradet.Core.Services;
using Forskningsradet.Core.Services.Contracts;
using Forskningsradet.Core.ViewModelMappers.Blocks;
using Forskningsradet.UnitTests.Builders;
using Xunit;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.UnitTests.Core.ViewModelMappers.Blocks
{
    public class RelatedProposalListBlockViewModelMapperTests
    {
        private readonly IUrlResolver _urlResolver;
        private readonly IContentListService _contentListService;
        private readonly IContentTypeRepository<PageType> _pageTypeRepository;
        private readonly CategoryRepository _categoryRepository;
        private readonly IPageRepository _pageRepository;
        private readonly ILocalizationProvider _localizationProvider;
        private readonly ICategoryLocalizationProvider _categoryLocalizationProvider;
        private readonly IPageEditingAdapter _pageEditingAdapter;
        private readonly IRichTextReactModelBuilder _richTextReactModelBuilder;
        private readonly IMessageReactModelBuilder _messageReactModelBuilder;
        private readonly IDateCardDatesReactModelBuilder _dateCardDatesReactModelBuilder;
        private readonly IDateCardStatusHandler _dateCardStatusHandler;
        private readonly RelatedProposalListBlockViewModelMapper _mapper;

        public RelatedProposalListBlockViewModelMapperTests()
        {
            _urlResolver = A.Fake<IUrlResolver>();
            _contentListService = A.Fake<IContentListService>();
            _pageTypeRepository = A.Fake<IContentTypeRepository<PageType>>();
            _categoryRepository = A.Fake<CategoryRepository>();
            _pageRepository = A.Fake<IPageRepository>();
            _localizationProvider = A.Fake<ILocalizationProvider>();
            _categoryLocalizationProvider = A.Fake<ICategoryLocalizationProvider>();
            _pageEditingAdapter = A.Fake<IPageEditingAdapter>();
            _richTextReactModelBuilder = A.Fake<IRichTextReactModelBuilder>();
            _messageReactModelBuilder = A.Fake<IMessageReactModelBuilder>();
            _dateCardDatesReactModelBuilder = A.Fake<IDateCardDatesReactModelBuilder>();
            _dateCardStatusHandler = A.Fake<IDateCardStatusHandler>();
            _mapper = new RelatedProposalListBlockViewModelMapper(_urlResolver, _contentListService, _pageTypeRepository, _categoryRepository, _pageRepository, _localizationProvider, _categoryLocalizationProvider, _pageEditingAdapter, _richTextReactModelBuilder, _messageReactModelBuilder, _dateCardDatesReactModelBuilder, _dateCardStatusHandler);
        }

        [Fact]
        public void GetViewModel_WhenAllFieldsProvided_ShouldMapFields()
        {
            const string expectedUrl = "url";
            const string expectedEmptyListText = "empty";
            ArrangeGetUrl(expectedUrl);
            ArrangeTranslation(expectedEmptyListText, "EmptyProposalList");

            var block = new RelatedProposalListBlock
            {
                Title = "T",
                Button = new EditLinkBlock
                {
                    Text = "L",
                    Link = new Url("u")
                }
            };

            var result = (ReactModels.RelatedDates)_mapper.GetPartialViewModel(block).Model;

            Assert.Equal(block.Title, result.RelatedContent.Title);
            Assert.Equal(block.Button.Text, result.RelatedContent.Link.Text);
            Assert.Equal(expectedUrl, result.RelatedContent.Link.Url);
            Assert.Equal(expectedEmptyListText, result.RelatedContent.EmptyList.Text);
        }

        [Fact]
        public void GetViewModel_WhenPagesAreReturned_ShouldMapAllPages()
        {
            ArrangeSearch(new List<ProposalPage>
                {
                    A.Fake<ProposalPage>(),
                    A.Fake<ProposalPage>(),
                    A.Fake<ProposalPage>()
                });

            var result = (ReactModels.RelatedDates)_mapper.GetPartialViewModel(new RelatedProposalListBlock()).Model;

            Assert.Equal(3, result.Dates.Count);
        }

        [Fact]
        public void GetViewModel_WhenPageIsReturned_ShouldMapFields()
        {
            var expectedTitle = "L";
            var expectedDateCardStatusComponent = new ReactModels.DateCardStatus();

            ArrangeSearch(new List<ProposalPage>
            {
                ProposalPageBuilder.Create.Default()
                    .WithListTitle(expectedTitle)
            });

            A.CallTo(() => _dateCardStatusHandler.GetCurrentStatus(A<ProposalPage>._))
                .Returns(expectedDateCardStatusComponent);

            var result = ((ReactModels.RelatedDates)_mapper.GetPartialViewModel(new RelatedProposalListBlock()).Model)
                .Dates[0];

            Assert.Equal(expectedTitle, result.Title);
            Assert.Equal(expectedDateCardStatusComponent, result.Status);
        }

        [Fact]
        public void GetViewModel_WhenPageIsReturned_ShouldNotMapMessageList()
        {
            var page = A.Fake<ProposalPage>();
            ArrangeSearch(new List<ProposalPage> { page });

            var result = ((ReactModels.RelatedDates)_mapper.GetPartialViewModel(new RelatedProposalListBlock()).Model);

            Assert.Null(result.Dates[0].MessageList);
        }

        [Fact]
        public void GetViewModel_WhenPageIsReturned_ShouldFallbackToPageName()
        {
            var page = A.Fake<ProposalPage>();
            page.ListTitle = null;
            page.PageName = "N";
            ArrangeSearch(new List<ProposalPage>
            {
                page
            });

            var result = ((ReactModels.RelatedDates)_mapper.GetPartialViewModel(new RelatedProposalListBlock()).Model)
                .Dates[0];

            Assert.Equal(page.PageName, result.Title);
        }

        [Fact]
        public void GetViewModel_WhenButtonTextIsNull_ShouldFallbackToLabel()
        {
            const string expectedLabel = "label";
            ArrangeTranslation(expectedLabel, "GoToListButton");

            var block = new RelatedProposalListBlock
            {
                Button = new EditLinkBlock
                {
                    Link = new Url("u")
                }
            };

            var result = (ReactModels.RelatedDates)_mapper.GetPartialViewModel(block).Model;

            Assert.Equal(expectedLabel, result.RelatedContent.Link.Text);
        }

        [Fact]
        public void GetViewModel_WhenPageIsInEditMode_ShouldMapOnPageEditing()
        {
            A.CallTo(() => _pageEditingAdapter.PageIsInEditMode()).Returns(true);

            var result = (ReactModels.RelatedDates)_mapper.GetPartialViewModel(new RelatedProposalListBlock()).Model;

            Assert.NotNull(result.RelatedContent.OnPageEditing);
        }

        [Fact]
        public void GetViewModel_WhenUsedInSidebar_TextShouldBeNull()
        {
            var page = A.Fake<ProposalPage>();
            page.ListIntro = "listIntro";
            ArrangeSearch(new List<ProposalPage>
            {
                page
            });

            var result = (ReactModels.RelatedDates)_mapper.GetPartialViewModel(new RelatedProposalListBlock(),
                                                                                new RenderOptionQueryParameter { RenderWidthOption = RenderWidthOption.IsInSidebar }).Model;

            Assert.Null(result.Dates[0].Text);
        }

        private void ArrangeSearch(List<ProposalPage> proposalList)
        {
            var result = new ContentListSearchResults(
                proposalList,
                new List<CategoryGroup>(),
                proposalList.Count);

            A.CallTo(() => _contentListService.Search<ProposalPage>(A<ContentListRequestModel>._))
                .Returns(result);
        }

        private void ArrangeGetUrl(string expectedUrl) =>
            A.CallTo(() => _urlResolver.GetUrl(A<UrlBuilder>._, A<UrlResolverArguments>._))
                .Returns(expectedUrl);

        private void ArrangeTranslation(string expected, string label) =>
            A.CallTo(() => _localizationProvider.GetLabel(nameof(RelatedContentListBlock), label))
                .Returns(expected);
    }
}