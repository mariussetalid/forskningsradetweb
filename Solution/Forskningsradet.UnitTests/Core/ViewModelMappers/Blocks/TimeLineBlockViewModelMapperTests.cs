using System;
using System.Collections.Generic;
using System.Linq;
using EPiServer.Core;
using FakeItEasy;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ContentModels.PropertyList;
using Forskningsradet.Core.Services;
using Forskningsradet.Core.ViewModelMappers.Blocks;
using Xunit;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.UnitTests.Core.ViewModelMappers.Blocks
{
    public class TimeLineBlockViewModelMapperTests
    {
        private readonly TimeLineBlockViewModelMapper _mapper;
        private readonly ILocalizationProvider _localizationProvider;
        private readonly IRichTextReactModelBuilder _richTextReactModelBuilder;

        public TimeLineBlockViewModelMapperTests()
        {
            _localizationProvider = A.Fake<ILocalizationProvider>();
            _richTextReactModelBuilder = A.Fake<IRichTextReactModelBuilder>();
            _mapper = new TimeLineBlockViewModelMapper(_localizationProvider, _richTextReactModelBuilder);
        }

        [Fact]
        [UseCulture("nb-NO")]
        public void GetViewModel_WhenAllFieldsProvided_ShouldMapFields()
        {
            const ReactModels.Timeline_EditorTheme expectedStyle = ReactModels.Timeline_EditorTheme.None;
            const string expectedTimeLineBlockTitle = "Title";
            const string expectedTimeLineItemDateTitle = "Date";
            const string expectedTimeLineItemSubtitleTitle = "ItemTitle";
            var expectedRichText = new ReactModels.RichText();

            A.CallTo(() => _richTextReactModelBuilder.BuildReactModel(A<XhtmlString>._, nameof(TimeLineItem.MainBody)))
                .Returns(expectedRichText);

            var timeLineBlock = new TimeLineBlock
            {
                Title = expectedTimeLineBlockTitle,
                TimeLineItems = new List<TimeLineItem>
                {
                    new TimeLineItem
                    {
                        DateTitle = expectedTimeLineItemDateTitle,
                        Title = expectedTimeLineItemSubtitleTitle,
                        DateTimes = new List<DateTime> {new DateTime(2018, 1, 1)},
                        MainBody = new XhtmlString("t")
                    }
                }
            };

            var result = (ReactModels.TimelineBlock)_mapper.GetPartialViewModel(timeLineBlock).Model;

            Assert.Equal(expectedTimeLineBlockTitle, result.Title);
            Assert.Equal(expectedStyle, result.EditorTheme);
            Assert.Equal(expectedTimeLineItemSubtitleTitle, result.Items[0].SubTitle);
            Assert.Equal(expectedRichText, result.Items[0].Text);
            Assert.Equal(expectedTimeLineItemDateTitle, result.Items[0].Title);
            Assert.True(result.Items[0].IsPastDate);
        }

        [Fact]
        [UseCulture("nb-NO")]
        public void GetViewModel_WhenDateTitleNotSet_ShouldMapDateAsTitle()
        {
            const string expectedTimeLineItemDateTimeString = "01 jan 2018";
            var timeLineItemDateTime = new DateTime(2018, 1, 1);

            var timeLineBlock = new TimeLineBlock
            {
                TimeLineItems = new List<TimeLineItem>
                {
                    new TimeLineItem
                    {
                        DateTitle = null,
                        DateTimes = new List<DateTime> {timeLineItemDateTime},
                    }
                }
            };

            var result = (ReactModels.TimelineBlock)_mapper.GetPartialViewModel(timeLineBlock).Model;

            Assert.Equal(expectedTimeLineItemDateTimeString, result.Items[0].Title);
        }

        [Fact]
        public void GetViewModel_WhenAllItemsHaveDate_ShouldOrderByDate()
        {
            var timeLineItemDateTime = new DateTime(2018, 1, 1);

            var timeLineBlock = new TimeLineBlock
            {
                TimeLineItems = new List<TimeLineItem>
                {
                    new TimeLineItem
                    {
                        Title = "1",
                        DateTimes = new List<DateTime> {timeLineItemDateTime},
                    },
                    new TimeLineItem
                    {
                        Title = "2",
                        DateTimes = new List<DateTime> {timeLineItemDateTime.AddMonths(-1)},
                    },
                    new TimeLineItem
                    {
                        Title = "3",
                        DateTimes = new List<DateTime> {timeLineItemDateTime.AddYears(-1)},
                    },
                }
            };

            var result = (ReactModels.TimelineBlock)_mapper.GetPartialViewModel(timeLineBlock).Model;

            Assert.Equal("3", result.Items[0].SubTitle);
            Assert.Equal("2", result.Items[1].SubTitle);
            Assert.Equal("1", result.Items[2].SubTitle);
        }

        [Fact]
        public void GetViewModel_WhenSomeItemsHaveDate_ShouldOrderByDatesFirst()
        {
            var timeLineItemDateTime = new DateTime(2018, 1, 1);

            var timeLineBlock = new TimeLineBlock
            {
                TimeLineItems = new List<TimeLineItem>
                {
                    new TimeLineItem
                    {
                        Title = "1",
                        DateTimes = new List<DateTime> {timeLineItemDateTime},
                    },
                    new TimeLineItem
                    {
                        Title = "2"
                    },
                    new TimeLineItem
                    {
                        Title = "3",
                        DateTimes = new List<DateTime> {timeLineItemDateTime.AddYears(-1)},
                    },
                }
            };

            var result = (ReactModels.TimelineBlock)_mapper.GetPartialViewModel(timeLineBlock).Model;

            Assert.Equal("3", result.Items[0].SubTitle);
            Assert.Equal("1", result.Items[1].SubTitle);
            Assert.Equal("2", result.Items[2].SubTitle);
        }

        [Fact]
        public void GetViewModel_PastAndFutureEventsExceedsGivenSpace_ShouldSetStartIndexCorrectly()
        {
            var timeLinePastItemDateTime = new DateTime(2018, 1, 1);
            var timeLineFutureItemDateTime = new DateTime(2030, 1, 1);

            var timeLineBlock = new TimeLineBlock
            {
                TimeLineItems = new List<TimeLineItem>
                {
                    new TimeLineItem
                    {
                        DateTimes = new List<DateTime> { timeLinePastItemDateTime }
                    },
                    new TimeLineItem
                    {
                        DateTimes = new List<DateTime> { timeLinePastItemDateTime }
                    },
                    new TimeLineItem
                    {
                        DateTimes = new List<DateTime> { timeLineFutureItemDateTime }
                    },
                    new TimeLineItem
                    {
                        DateTimes = new List<DateTime> { timeLineFutureItemDateTime }
                    },
                    new TimeLineItem
                    {
                        DateTimes = new List<DateTime> { timeLineFutureItemDateTime }
                    }
                }
            };

            var result = (ReactModels.TimelineBlock)_mapper.GetPartialViewModel(timeLineBlock).Model;

            Assert.True(result.StartIndex == 1);
        }

        [Fact]
        public void GetViewModel_WithToday_ShouldNotContainProgress()
        {
            var timeLineItemDateTime = new DateTime(2018, 1, 1);

            var timeLineBlock = new TimeLineBlock
            {
                TimeLineItems = new List<TimeLineItem>
                {
                    new TimeLineItem
                    {
                        DateTimes = new List<DateTime> {timeLineItemDateTime}
                    },
                    new TimeLineItem
                    {
                        DateTimes = new List<DateTime> {DateTime.UtcNow}
                    },
                    new TimeLineItem
                    {
                        DateTimes = new List<DateTime> {DateTime.UtcNow.AddDays(1)}
                    }
                }
            };

            var result = (ReactModels.TimelineBlock)_mapper.GetPartialViewModel(timeLineBlock).Model;

            Assert.True(result.Items.All(x => x.Progress == 0));
        }

        [Theory]
        [InlineData(10, 2)]
        [InlineData(10, 1)]
        [InlineData(2, 1)]
        public void GetViewModel_WithPositiveDaysBetweenAndTimelineDone_ShouldGiveZeroProgress(int daysBefore, int daysDistance)
        {
            const int expectedProgress = 0;

            var pastItem = DateTime.UtcNow.AddDays(-daysBefore).Date;
            var pastItemWithDistance = pastItem.AddDays(daysDistance);
            var pastItemWithDistance2 = pastItemWithDistance.AddDays(daysDistance);
            var pastItemWithDistance3 = pastItemWithDistance2.AddDays(daysDistance);

            var timeLineBlock = new TimeLineBlock
            {
                TimeLineItems = new List<TimeLineItem>
                {
                    new TimeLineItem
                    {
                        DateTimes = new List<DateTime> {pastItem}
                    },
                    new TimeLineItem
                    {
                        DateTimes = new List<DateTime> {pastItemWithDistance}
                    },
                     new TimeLineItem
                    {
                        DateTimes = new List<DateTime> {pastItemWithDistance2}
                    },
                      new TimeLineItem
                    {
                        DateTimes = new List<DateTime> {pastItemWithDistance3}
                    }
                }
            };

            var result = (ReactModels.TimelineBlock)_mapper.GetPartialViewModel(timeLineBlock).Model;

            Assert.Equal(expectedProgress, result.Items[0].Progress);
            Assert.Equal(expectedProgress, result.Items[1].Progress);
            Assert.Equal(expectedProgress, result.Items[2].Progress);
            Assert.Equal(expectedProgress, result.Items[3].Progress);

        }

        [Theory]
        [InlineData(1, 2, 50)]
        [InlineData(2, 4, 50)]
        [InlineData(5, 10, 50)]
        [InlineData(8, 10, 80)]
        public void GetViewModel_WithoutTodayWithPastAndFuture_ShouldMapProgress(int daysProgress, int daysDistance, int expectedProgress)
        {
            var timeLineItemDateTime = new DateTime(2018, 1, 1);
            var pastItem = DateTime.UtcNow.AddDays(-daysProgress).Date;
            var futureItem = pastItem.AddDays(daysDistance);

            var timeLineBlock = new TimeLineBlock
            {
                TimeLineItems = new List<TimeLineItem>
                {
                    new TimeLineItem
                    {
                        DateTimes = new List<DateTime> {timeLineItemDateTime}
                    },
                    new TimeLineItem
                    {
                        DateTimes = new List<DateTime> {pastItem}
                    },
                    new TimeLineItem
                    {
                        DateTimes = new List<DateTime> {futureItem}
                    }
                }
            };

            var result = (ReactModels.TimelineBlock)_mapper.GetPartialViewModel(timeLineBlock).Model;

            Assert.Equal(expectedProgress, result.Items[1].Progress);
        }

        [Fact]
        public void GetViewModel_WhenDateIsTodayOrEarlier_ShouldBePastDate()
        {
            var timeLineItemDateTime = new DateTime(2018, 1, 1);

            var timeLineBlock = new TimeLineBlock
            {
                TimeLineItems = new List<TimeLineItem>
                {
                    new TimeLineItem
                    {
                        DateTimes = new List<DateTime> {timeLineItemDateTime}
                    },
                    new TimeLineItem
                    {
                        DateTimes = new List<DateTime> {DateTime.UtcNow}
                    },
                    new TimeLineItem
                    {
                        DateTimes = new List<DateTime> {DateTime.UtcNow.AddDays(1)}
                    }
                }
            };

            var result = (ReactModels.TimelineBlock)_mapper.GetPartialViewModel(timeLineBlock).Model;

            Assert.True(result.Items[0].IsPastDate);
            Assert.True(result.Items[1].IsPastDate);
            Assert.False(result.Items[2].IsPastDate);
        }

        [Fact]
        public void GetViewModel_WhenDateIsNotSet_ShouldBePastDate()
        {
            var timeLineBlock = new TimeLineBlock
            {
                TimeLineItems = new List<TimeLineItem>
                {
                    new TimeLineItem()
                }
            };

            var result = (ReactModels.TimelineBlock)_mapper.GetPartialViewModel(timeLineBlock).Model;

            Assert.True(result.Items[0].IsPastDate);
        }

        [Fact]
        public void GetViewModel_WhenMainBodyIsNotSet_ShouldNotHaveText()
        {
            var timeLineBlock = new TimeLineBlock
            {
                TimeLineItems = new List<TimeLineItem>
                {
                    new TimeLineItem()
                }
            };

            var result = (ReactModels.TimelineBlock)_mapper.GetPartialViewModel(timeLineBlock).Model;

            Assert.Null(result.Items[0].Text);
        }

        [Theory]
        [InlineData(TimelineBlockStyle.None)]
        [InlineData(TimelineBlockStyle.Gray)]
        public void GetViewModel_GivenStyle_ShouldMapDefaultStyle(TimelineBlockStyle style)
        {
            var block = new TimeLineBlock
            {
                Style = style
            };

            var result = (ReactModels.TimelineBlock)_mapper.GetPartialViewModel(block).Model;

            Assert.Equal(ReactModels.Timeline_EditorTheme.None, result.EditorTheme);
        }
    }
}