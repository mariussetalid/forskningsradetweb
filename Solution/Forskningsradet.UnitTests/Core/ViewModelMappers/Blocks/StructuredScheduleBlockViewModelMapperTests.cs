using FakeItEasy;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.ViewModelMappers.Blocks;
using Xunit;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.UnitTests.Core.ViewModelMappers.Blocks
{
    public class StructuredScheduleBlockViewModelMapperTests
    {
        private readonly IAccordionWithContentAreaListReactModelBuilder _accordionWithContentAreaListReactModelBuilder;
        private readonly StructuredScheduleBlockViewModelMapper _mapper;

        public StructuredScheduleBlockViewModelMapperTests()
        {
            _accordionWithContentAreaListReactModelBuilder = A.Fake<IAccordionWithContentAreaListReactModelBuilder>();
            _mapper = new StructuredScheduleBlockViewModelMapper(
                _accordionWithContentAreaListReactModelBuilder
                );
        }

        [Fact]
        public void GetPartialViewModel_ShouldReturnModelFromBuilder()
        {
            var expectedModel = new ReactModels.AccordionWithContentAreaList();
            A.CallTo(() => _accordionWithContentAreaListReactModelBuilder.BuildReactModel(A<StructuredScheduleBlock[]>._))
                .Returns(expectedModel);

            var result = _mapper.GetPartialViewModel(new StructuredScheduleBlock()).Model;

            Assert.Same(expectedModel, result);
        }
    }
}