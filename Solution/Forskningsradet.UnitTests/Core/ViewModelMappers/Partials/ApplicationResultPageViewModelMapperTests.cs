using System.Collections.Generic;
using FakeItEasy;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.ContentModels.PropertyList;
using Forskningsradet.Core.Models.ReactModels;
using Forskningsradet.Core.Services;
using Forskningsradet.Core.ViewModelMappers.Partials;
using Xunit;

namespace Forskningsradet.UnitTests.Core.ViewModelMappers.Partials
{
    public class ApplicationResultPageViewModelMapperTests
    {
        private readonly ILocalizationProvider _localizationProvider;
        private readonly ApplicationResultPageViewModelMapper _mapper;

        public ApplicationResultPageViewModelMapperTests()
        {
            _localizationProvider = A.Fake<ILocalizationProvider>();
            _mapper = new ApplicationResultPageViewModelMapper(_localizationProvider);
        }
        
        [Fact]
        public void WithDefaultValues_ShouldMapDescriptionList()
        {
            const string expectedAppliedAmountLabel = "applied amount";
            const string expectedAwardedAmountLabel = "awarded amount";
            const string expectedNumberOfApplications = "number of applications";
            const string expectedNumberOfApprovedApplications = "number of approved applications";
            
            ArrangeTranslation(expectedAppliedAmountLabel, "AppliedAmount");
            ArrangeTranslation(expectedAwardedAmountLabel, "AwardedAmount");
            ArrangeTranslation(expectedNumberOfApplications, "NumberOfApplications");
            ArrangeTranslation(expectedNumberOfApprovedApplications, "NumberOfApprovedApplications");
            
            var page = new ApplicationResultPage
            {
                AppliedAmount = "1",
                AwardedAmount = "2",
                NumberOfApplications = "3",
                NumberOfApprovedApplications = "4"
            };

            var result = ((DescriptionListAndTables)_mapper.GetPartialViewModel(page).Model).DescriptionList;

            Assert.Equal(expectedAppliedAmountLabel, result.Items[0].Label);
            Assert.Equal(expectedAwardedAmountLabel, result.Items[1].Label);
            Assert.Equal(expectedNumberOfApplications, result.Items[2].Label);
            Assert.Equal(expectedNumberOfApprovedApplications, result.Items[3].Label);
            
            Assert.Equal(page.AppliedAmount, result.Items[0].Text);
            Assert.Equal(page.AwardedAmount, result.Items[1].Text);
            Assert.Equal(page.NumberOfApplications, result.Items[2].Text);
            Assert.Equal(page.NumberOfApprovedApplications, result.Items[3].Text);
        }
        
        [Fact]
        public void WithDefaultValues_ShouldMapGradeDistributionTable()
        {
            const string expectedTitle = "title";
            const string expectedGradeHeader = "grade";
            const string expectedCountHeader = "count";
            
            ArrangeTranslation(expectedTitle, "GradeDistribution");
            ArrangeTranslation(expectedGradeHeader, "Grade");
            ArrangeTranslation(expectedCountHeader, "Project");
            
            var page = new ApplicationResultPage
            {
                GradeDistribution = new List<GradeDistribution>
                {
                    new GradeDistribution
                    {
                        Grade = "1",
                        Count = "2"
                    }
                }
            };

            var result = ((DescriptionListAndTables)_mapper.GetPartialViewModel(page).Model).Tables[0];
            
            Assert.Equal(expectedTitle, result.Title);
            Assert.Equal(expectedGradeHeader, result.Table.Header[0]);
            Assert.Equal(expectedCountHeader, result.Table.Rows[0][0]);
            Assert.Equal(page.GradeDistribution[0].Grade, result.Table.Header[1]);
            Assert.Equal(page.GradeDistribution[0].Count, result.Table.Rows[0][1]);
        }
        
        [Fact]
        public void WithDefaultValues_ShouldMapGrantedProjectsTable()
        {
            const string expectedTitle = "title";
            const string expectedNumberHeader = "number header";
            const string expectedTitleHeader = "title header";
            const string expectedOrganizationHeader = "org header";
            
            ArrangeTranslation(expectedTitle, "GrantedProjects");
            ArrangeTranslation(expectedNumberHeader, "ProjectNumber");
            ArrangeTranslation(expectedTitleHeader, "ProjectTitle");
            ArrangeTranslation(expectedOrganizationHeader, "ProjectOrganization");
            
            var page = new ApplicationResultPage
            {
                GrantedProjects = new List<GrantedProject>
                {
                    new GrantedProject
                    {
                        Number = "1",
                        Title = "project title",
                        Organization = "org"
                    }
                }
            };

            var result = ((DescriptionListAndTables)_mapper.GetPartialViewModel(page).Model).Tables[0];
            
            Assert.Equal(expectedTitle, result.Title);
            Assert.Equal(expectedNumberHeader, result.Table.Header[0]);
            Assert.Equal(expectedTitleHeader, result.Table.Header[1]);
            Assert.Equal(expectedOrganizationHeader, result.Table.Header[2]);
            Assert.Equal(page.GrantedProjects[0].Number, result.Table.Rows[0][0]);
            Assert.Equal(page.GrantedProjects[0].Title, result.Table.Rows[0][1]);
            Assert.Equal(page.GrantedProjects[0].Organization, result.Table.Rows[0][2]);
        }
        
        [Fact]
        public void WithDefaultValues_ShouldMapSubjectSpecialistsTable()
        {
            const string expectedTitle = "title";
            const string expectedNameHeader = "name header";
            const string expectedOrganizationHeader = "org header";
            
            ArrangeTranslation(expectedTitle, "SubjectSpecialists");
            ArrangeTranslation(expectedNameHeader, "Name");
            ArrangeTranslation(expectedOrganizationHeader, "Organization");
            
            var page = new ApplicationResultPage
            {
                SubjectSpecialists = new List<SubjectSpecialist>
                {
                    new SubjectSpecialist
                    {
                        Name = "name",
                        Organization = "org"
                    }
                }
            };

            var result = ((DescriptionListAndTables)_mapper.GetPartialViewModel(page).Model).Tables[0];
            
            Assert.Equal(expectedTitle, result.Title);
            Assert.Equal(expectedNameHeader, result.Table.Header[0]);
            Assert.Equal(expectedOrganizationHeader, result.Table.Header[1]);
            Assert.Equal(page.SubjectSpecialists[0].Name, result.Table.Rows[0][0]);
            Assert.Equal(page.SubjectSpecialists[0].Organization, result.Table.Rows[0][1]);
        }
        
        private void ArrangeTranslation(string expected, string label)
            => A.CallTo(() => _localizationProvider.GetLabel(nameof(ApplicationResultPage), label))
                .Returns(expected);
    }
}