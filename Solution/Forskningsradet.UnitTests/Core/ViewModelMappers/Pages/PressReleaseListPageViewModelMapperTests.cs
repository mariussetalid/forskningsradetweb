using System;
using System.Collections.Generic;
using EPiServer.Core;
using EPiServer.Web.Routing;
using FakeItEasy;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.QueryParameters;
using Forskningsradet.Core.Models.RequestModels;
using Forskningsradet.Core.Models.SearchModels;
using Forskningsradet.Core.Services;
using Forskningsradet.Core.ViewModelMappers.Pages;
using Forskningsradet.UnitTests.Builders;
using Xunit;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.UnitTests.Core.ViewModelMappers.Pages
{
    public class PressReleaseListPageViewModelMapperTests
    {
        private readonly IContentListService _contentListService;
        private readonly IPaginationReactModelBuilder _paginationReactModelBuilder;
        private readonly IContentAreaReactModelBuilder _contentAreaReactModelBuilder;
        private readonly IOptionsModalReactModelBuilder _optionsModalReactModelBuilder;
        private readonly IFluidImageReactModelBuilder _fluidImageReactModelBuilder;
        private readonly IUrlResolver _urlResolver;
        private readonly ILocalizationProvider _localizationProvider;
        private readonly PressReleaseListPageViewModelMapper _mapper;

        public PressReleaseListPageViewModelMapperTests()
        {
            _contentListService = A.Fake<IContentListService>();
            _paginationReactModelBuilder = A.Fake<IPaginationReactModelBuilder>();
            _contentAreaReactModelBuilder = A.Fake<IContentAreaReactModelBuilder>();
            _optionsModalReactModelBuilder = A.Fake<IOptionsModalReactModelBuilder>();
            _fluidImageReactModelBuilder = A.Fake<IFluidImageReactModelBuilder>();
            _urlResolver = A.Fake<IUrlResolver>();
            _localizationProvider = A.Fake<ILocalizationProvider>();

            _mapper = new PressReleaseListPageViewModelMapper(
                _contentListService,
                _paginationReactModelBuilder,
                _contentAreaReactModelBuilder,
                _optionsModalReactModelBuilder,
                _fluidImageReactModelBuilder,
                _urlResolver,
                _localizationProvider);

            EPiServer.Globalization.ContentLanguage.PreferredCulture = new System.Globalization.CultureInfo("nb-NO");
        }

        [Fact]
        public void WhenPressReleaseIsNotSelected_ShouldMapListPage()
        {
            var page = A.Fake<PressReleaseListPage>();
            page.PageName = "page";
            var query = GetDefaultQueryParameters();

            var expectedSidebar = A.Fake<ReactModels.ContentArea>();
            A.CallTo(() => _contentAreaReactModelBuilder.BuildReactModel(A<ContentArea>._, A<string>._, A<RenderOptionQueryParameter>._))
                .Returns(expectedSidebar);

            var result = (ReactModels.ArticlePage)_mapper.GetPageViewModel(page, query).Model;

            Assert.Equal(page.PageName, result.Title);
            Assert.Same(expectedSidebar, result.Sidebar);
        }

        [Fact]
        [UseCulture("nb-NO")]
        public void GetPageViewModel_WhenListingOutPages_ShouldMapListItemValues()
        {
            const string expectedUrl = "?id=1";
            const string expectedLabel = "published";
            const string expectedPublishedDate = "21. juli 2008";

            var pressReleasePage = (PressReleasePage)PressReleasePageBuilder.Create.Default()
                .WithId("1")
                .WithListTitle("title")
                .WithListIntro("ingress")
                .WithImageThumbnailUrl("thumbnail")
                .WithImageCaption("caption")
                .WithExternalPublishedAt(new DateTime(2008, 7, 21));

            ArrangeSearch(new List<PressReleasePage> { pressReleasePage });
            ArrangeTranslation(expectedLabel, "Published");
            A.CallTo(() => _urlResolver.GetUrl(A<ContentReference>._, A<string>._, A<UrlResolverArguments>._))
                .Returns(expectedUrl);

            var result = (((ReactModels.ArticlePage)_mapper.GetPageViewModel(A.Fake<PressReleaseListPage>(), GetDefaultQueryParameters()).Model)
                    .Content
                    .Blocks[0]
                    .ComponentData as ReactModels.RelatedArticles)
                ?.Articles[0];

            Assert.NotNull(result);
            Assert.Equal(expectedUrl, result.Title.Url);
            Assert.Equal(expectedPublishedDate, result.Published.Date);
            Assert.Equal(expectedLabel, result.Published.Type);
            Assert.Equal(pressReleasePage.ListTitle, result.Title.Text);
            Assert.Equal(pressReleasePage.ListIntro, result.Text);
            Assert.Equal(pressReleasePage.ImageThumbnailUrl, result.Image.Src);
            Assert.Equal(pressReleasePage.ImageCaption, result.Image.Alt);
        }

        [Fact]
        public void WhenListingOutPages_ShouldMapBorderTop()
        {
            ArrangeSearch(new List<PressReleasePage> { PressReleasePageBuilder.Create.Default() });
            var page = A.Fake<PressReleaseListPage>();
            page.PageName = "T";
            var query = GetDefaultQueryParameters();

            var result = (((ReactModels.ArticlePage)_mapper.GetPageViewModel(page, query).Model)
                    .Content
                    .Blocks[0]
                    .ComponentData as ReactModels.RelatedArticles)
                ?.RelatedContent;

            Assert.NotNull(result);
            Assert.Equal(ReactModels.RelatedContent_Border.Top, result.Border);
        }

        [Fact]
        public void WhenListingOutPages_ShouldMapPagination()
        {
            var page = A.Fake<PressReleaseListPage>();
            var query = GetDefaultQueryParameters();

            var expectedPagination = A.Fake<ReactModels.Pagination>();
            A.CallTo(() => _paginationReactModelBuilder.BuildReactModel(A<string>._, A<int>._, A<int>._, A<int>._, A<string>._))
                .Returns(expectedPagination);

            var result = ((ReactModels.ArticlePage)_mapper.GetPageViewModel(page, query).Model)
                .Content
                .Blocks[0]
                .ComponentData;

            Assert.NotNull(result);
            Assert.Same(expectedPagination, result);
        }

        [Fact]
        public void GetViewModel_WhenNoPressReleaseReturned_ShouldMapNoItemsMessage()
        {
            const string expectedLabel = "NA";
            ArrangeTranslation(expectedLabel, "NotAvailable");
            ArrangeSearch(new List<PressReleasePage>());

            var page = A.Fake<PressReleaseListPage>();
            var query = GetDefaultQueryParameters();

            var result = (ReactModels.ArticlePage)_mapper.GetPageViewModel(page, query).Model;

            Assert.Equal(expectedLabel, result.Ingress);
        }

        private void ArrangeSearch(List<PressReleasePage> pages) =>
            A.CallTo(() => _contentListService.Search<PressReleasePage>(A<ContentListRequestModel>._))
                .Returns(new ContentListSearchResults(pages, null, pages.Count));

        private static PressReleaseQueryParameter GetDefaultQueryParameters() =>
            new PressReleaseQueryParameter { Page = 1 };

        private void ArrangeTranslation(string label, string key) =>
            A.CallTo(() => _localizationProvider.GetLabel(nameof(PressReleaseListPage), A<string>.That.Matches(x => x == key)))
                .Returns(label);
    }
}