using System;
using System.Collections.Generic;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.Find.UnifiedSearch;
using EPiServer.Web.Routing;
using FakeItEasy;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Builders;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Configuration;
using Forskningsradet.Core.Models.ApiModels.Evurdering;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.QueryParameters;
using Forskningsradet.Core.Models.RequestModels;
using Forskningsradet.Core.Models.SearchModels;
using Forskningsradet.Core.Repositories.Contracts;
using Forskningsradet.Core.Services;
using Forskningsradet.Core.Services.Contracts;
using Forskningsradet.Core.ViewModelMappers.Pages;
using Xunit;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.UnitTests.Core.ViewModelMappers.Pages
{
    public class SearchPageViewModelMapperTests
    {
        private readonly ISearchService _searchService;
        private readonly IUrlResolver _urlResolver;
        private readonly IContentLoader _contentLoader;
        private readonly IPageRepository _pageRepository;
        private readonly IProjectDatabankConfiguration _projectDatabankConfiguration;
        private readonly FilterLayoutReactModelBuilder _filterLayoutReactModelBuilder;
        private readonly IFluidImageReactModelBuilder _fluidImageReactModelBuilder;
        private readonly IDateCardStatusHandler _dateCardStatusHandler;
        private readonly ICategoryFilterService _categoryFilterService;
        private readonly CategoryRepository _categoryRepository;
        private readonly ICategoryLocalizationProvider _categoryLocalizationProvider;
        private readonly IDateCardTagsReactModelBuilder _dateCardTagsReactModelBuilder;
        private readonly ILocalizationProvider _localizationProvider;
        private readonly IPaginationReactModelBuilder _paginationReactModelBuilder;
        private readonly ISearchApiUrlResolver _searchApiUrlResolver;
        private readonly ISearchTrackingService _searchTrackingService;
        private readonly SearchPageViewModelMapper _mapper;

        public SearchPageViewModelMapperTests()
        {
            _searchService = A.Fake<ISearchService>();
            _urlResolver = A.Fake<IUrlResolver>();
            _contentLoader = A.Fake<IContentLoader>();
            _pageRepository = A.Fake<IPageRepository>();
            _projectDatabankConfiguration = A.Fake<IProjectDatabankConfiguration>();
            _fluidImageReactModelBuilder = A.Fake<IFluidImageReactModelBuilder>();
            _dateCardStatusHandler = A.Fake<IDateCardStatusHandler>();
            _categoryFilterService = A.Fake<ICategoryFilterService>();
            _categoryRepository = A.Fake<CategoryRepository>();
            _categoryLocalizationProvider = A.Fake<ICategoryLocalizationProvider>();
            _localizationProvider = A.Fake<ILocalizationProvider>();
            _filterLayoutReactModelBuilder = new FilterLayoutReactModelBuilder(new FilterGroupReactModelBuilder(), _categoryLocalizationProvider, _localizationProvider);
            _dateCardTagsReactModelBuilder = A.Fake<IDateCardTagsReactModelBuilder>();
            _paginationReactModelBuilder = A.Fake<IPaginationReactModelBuilder>();
            _searchApiUrlResolver = A.Fake<ISearchApiUrlResolver>();
            _searchTrackingService = A.Fake<ISearchTrackingService>();
            _mapper = new SearchPageViewModelMapper(
                _searchService,
                _urlResolver,
                _contentLoader,
                _pageRepository,
                _projectDatabankConfiguration,
                _filterLayoutReactModelBuilder,
                _fluidImageReactModelBuilder,
                _dateCardStatusHandler,
                _categoryFilterService,
                _categoryRepository,
                _categoryLocalizationProvider,
                _dateCardTagsReactModelBuilder,
                _localizationProvider,
                _paginationReactModelBuilder,
                _searchApiUrlResolver,
                _searchTrackingService);
        }

        [Fact]
        public void GetViewModel_WithDefaultValues_ShouldMapDefaultFields()
        {
            const string expectedUrl = "url-to-self";
            const string expectedExternalSearchUrl = "url-to-external";
            const string expectedSearchLabel = "s";
            const string expectedSearchPlaceholder = "ph";
            const string expectedSubmitButtonLabel = "sub";

            var page = A.Fake<SearchPage>();
            page.PageName = "page name";
            page.ContentLink = new PageReference(1);

            ArrangeSearch(new List<UnifiedSearchHit>());
            ArrangeTranslation(expectedSearchLabel, "SearchButtonText");
            ArrangeTranslation(expectedSearchPlaceholder, "SearchPlaceholder");
            ArrangeTranslation(expectedSubmitButtonLabel, "SubmitButtonText");

            A.CallTo(() => _projectDatabankConfiguration.ProjectDatabankSearchUrl)
                .Returns(expectedExternalSearchUrl + "?q={0}");
            A.CallTo(() => _urlResolver.GetUrl(A<ContentReference>.That.Matches(x => x.ID == page.ContentLink.ID), A<string>._, A<UrlResolverArguments>._))
                .Returns(expectedUrl);

            var result = (ReactModels.SearchPage)_mapper.GetPageViewModel(
                    page,
                    A.Fake<SearchQueryParameter>())
                .Model;

            Assert.NotNull(result.EmptyList);
            Assert.Null(result.ResultDescription);
            Assert.Equal(page.PageName, result.Title);
            Assert.Equal(expectedUrl, result.Form.Endpoint);
            Assert.Equal(expectedSubmitButtonLabel, result.Form.SubmitButtonText);
            Assert.Equal(expectedSearchLabel, result.Search.Submit.Text);
            Assert.Null(result.Search.ExternalResultsEndpoint);
            Assert.Equal("q", result.Search.Input.Name);
            Assert.Equal("q", result.Search.Input.Label);
            Assert.Equal(expectedSearchPlaceholder, result.Search.Input.Placeholder);
            Assert.Equal("", result.Search.Input.Value);
        }

        [Fact]
        public void GetViewModel_WithEmptyQuery_ShouldNotMapResults()
        {
            var query = BuildQuery(query: null, subjects: null, targetGroups: null, types: null);
            ArrangeSearch(new List<UnifiedSearchHit> { A.Fake<UnifiedSearchHit>() }, totalMatching: 0);
            ArrangeTranslation("Type something to search", "EmptySearchDescription");

            A.CallTo(() => _paginationReactModelBuilder.BuildReactModel(A<string>._, A<int>._, A<int>._, A<int>._, A<string>._))
                .Returns(null);

            var result = (ReactModels.SearchPage)_mapper.GetPageViewModel(
                    A.Fake<SearchPage>(),
                    query)
                .Model;

            Assert.Equal("Type something to search", result.Search.ResultsDescription);
            Assert.Null(result.ResultDescription);
            Assert.Null(result.Pagination);
        }

        [Fact]
        public void GetViewModel_WithQueryAndEnableExternalSearch_ShouldMapExternalSearch()
        {
            const string expectedExternalSearchUrl = "url-to-external";
            var searchPage = new SearchPage
            {
                EnableExternalSearch = true
            };
            var query = BuildQuery(query: "Q");
            ArrangeTranslation("search gave {0} internal", "InternalSearchDescriptionFormat");
            ArrangeTranslation("and external", "ExternalSearchDescription");

            A.CallTo(() => _projectDatabankConfiguration.ProjectDatabankSearchUrl)
                .Returns(expectedExternalSearchUrl + "?q={0}");

            var result = (ReactModels.SearchPage)_mapper.GetPageViewModel(
                    searchPage,
                    query)
                .Model;

            Assert.Equal("search gave 0 internal", result.Search.ResultsDescription);
            Assert.Equal("and external", result.Search.ExternalResultsLabel);
            Assert.Equal(expectedExternalSearchUrl + "?q=Q", result.Search.ExternalResultsEndpoint);
        }

        [Fact]
        public void GetViewModel_WithoutEnableExternalSearch_ShouldGiveNullExternalResultsEndpointAndEmptyExternalSearchLabel()
        {
            var query = BuildQuery(query: "Q");

            var searchPage = new SearchPage
            {
                EnableExternalSearch = false
            };

            ArrangeTranslation("search gave {0} internal", "InternalSearchDescriptionFormat");

            A.CallTo(() => _projectDatabankConfiguration.ProjectDatabankSearchUrl)
                .Returns("url-to-external?q={0}");

            var result = (ReactModels.SearchPage)_mapper.GetPageViewModel(
                    searchPage,
                    query)
                .Model;

            Assert.Equal("search gave 0 internal", result.Search.ResultsDescription);
            Assert.Equal(string.Empty, result.Search.ExternalResultsLabel);
            Assert.Null(result.Search.ExternalResultsEndpoint);
        }

        [Fact]
        public void GetViewModel_WhenThereAreNoSearchHits_ShouldMapEmptyList()
        {
            const string expectedEmptyLabel = "empty";
            var searchPage = new SearchPage
            {
                EnableExternalSearch = true
            };
            var query = BuildQuery(query: "Q");
            ArrangeTranslation(expectedEmptyLabel, "EmptySearch");
            ArrangeTranslation("search gave {0} internal", "InternalSearchDescriptionFormat");
            ArrangeTranslation("and external", "ExternalSearchDescription");

            var result = (ReactModels.SearchPage)_mapper.GetPageViewModel(
                    searchPage,
                    query)
                .Model;

            Assert.NotNull(result.EmptyList);
            Assert.Equal(expectedEmptyLabel, result.EmptyList.Text);
            Assert.Equal("search gave 0 internal", result.Search.ResultsDescription);
            Assert.Equal("and external", result.Search.ExternalResultsLabel);
        }

        [Fact]
        public void GetViewModel_WhenSearchHitsAreLessThanPageSize_ShouldNotMapPagination()
        {
            var expectedPagination = new ReactModels.Pagination();
            var query = BuildQuery(query: "Something");

            ArrangeSearch(new List<UnifiedSearchHit> { A.Fake<UnifiedSearchHit>() }, totalMatching: 1);
            ArrangeTranslation("search gave {0} internal", "InternalSearchDescriptionFormat");
            ArrangeTranslation("showing {0}-{1} of {2} internal", "ResultDescriptionFormat");
            ArrangeTranslation("page {0}", "PaginationPage");

            A.CallTo(() => _paginationReactModelBuilder.BuildReactModel(A<string>._, A<int>._, A<int>._, A<int>._, A<string>._))
                .Returns(expectedPagination);

            var result = (ReactModels.SearchPage)_mapper.GetPageViewModel(
                    A.Fake<SearchPage>(),
                    query)
                .Model;

            Assert.Equal("search gave 1 internal", result.Search.ResultsDescription);
            Assert.Equal("showing 1-1 of 1 internal", result.ResultDescription);
            Assert.Equal(expectedPagination, result.Pagination);
        }

        [Fact]
        public void GetViewModel_WhenSearchHitsExceedPageSize_ShouldMapPagination()
        {
            var expectedPagination = new ReactModels.Pagination();
            const string expectedPaginationTitle = "p";
            var query = BuildQuery(query: "Q");

            var page = A.Fake<SearchPage>();
            var searchHit = GetSearchHitForTest();

            const int pageSize = SearchConstants.SearchPageSize;
            ArrangeSearch(new List<UnifiedSearchHit> { searchHit }, totalMatching: pageSize + 1);
            ArrangeTranslation("search gave {0} internal", "InternalSearchDescriptionFormat");
            ArrangeTranslation("showing {0}-{1} of {2} internal", "ResultDescriptionFormat");
            ArrangeTranslation(expectedPaginationTitle, "PaginationTitle");
            ArrangeTranslation("page {0}", "PaginationPage");

            A.CallTo(() => _paginationReactModelBuilder.BuildReactModel(A<string>._, A<int>._, A<int>._, A<int>._, A<string>._))
                .Returns(expectedPagination);

            var result = (ReactModels.SearchPage)_mapper.GetPageViewModel(
                    page,
                    query)
                .Model;

            Assert.Equal("showing 1-10 of 11 internal", result.ResultDescription);
            Assert.Equal("search gave 11 internal", result.Search.ResultsDescription);
            Assert.Equal(expectedPagination, result.Pagination);
        }

        [Fact]
        public void GetViewModel_WithSearchHit_ShouldMapSearchHit()
        {
            const string expectedMetadataLabel = "ml";
            const string expectedTagText = "tt";
            var expectedDateCardTagsComponentModel = new ReactModels.DateCardTags();
            var query = BuildQuery(query: "Q");

            var editorialPage = A.Fake<EditorialPage>();
            editorialPage.Category = new CategoryList(new List<int> { 1 });

            var searchHit = GetSearchHitForTest();
            searchHit.PublishDate = new DateTime(2011, 3, 14);
            searchHit.OriginalObjectGetter = new Func<object>(() => editorialPage);

            ArrangeSearch(new List<UnifiedSearchHit> { searchHit });
            ArrangeTranslation(expectedMetadataLabel, "Published");

            A.CallTo(() => _localizationProvider.GetEnumName<SearchResultType>(A<SearchResultType>._))
                .Returns(expectedTagText);
            A.CallTo(() => _dateCardTagsReactModelBuilder.BuildReactModel(A<CategoryList>._, A<int>._))
                .Returns(expectedDateCardTagsComponentModel);

            var result = ((ReactModels.SearchPage)_mapper.GetPageViewModel(
                    A.Fake<SearchPage>(),
                    query)
                .Model).Results[0];

            Assert.Equal(searchHit.Title, result.Title);
            Assert.Equal(searchHit.Url, result.Url);
            Assert.Equal(searchHit.Excerpt, result.Text);
            Assert.Equal(expectedTagText, result.Metadata.Items[0].Text);
            Assert.Equal(expectedMetadataLabel, result.Metadata.Items[1].Label);
            Assert.NotNull(result.Metadata.Items[1].Text);
            Assert.Same(expectedDateCardTagsComponentModel, result.ThemeTags);
        }

        [Fact]
        public void GetViewModel_ProposalStateIsCompletedAndHasApplicationResult_ShouldMapStatusList()
        {
            var expectedStatusList = new List<ReactModels.DateCardStatus>
            {
                new ReactModels.DateCardStatus { Theme = ReactModels.DateCardStatus_Theme.ResultIsPublished }
            };
            var searchQueryParameter = BuildQuery(categories: new string[] { "1" }, query: "");

            var hit = GetSearchHitForTest();
            hit.OriginalObjectGetter = new Func<object>(() => new ProposalPage { ProposalState = ProposalState.Completed, ApplicationResult = new PageReference(1) });
            ArrangeSearch(new List<UnifiedSearchHit> { hit });
            A.CallTo(() => _dateCardStatusHandler.BuildDateCardStatusList(A<ProposalPage>.That.Matches(x => x.ProposalState == ProposalState.Completed)))
                .Returns(expectedStatusList);

            var result = ((ReactModels.SearchPage)_mapper.GetPageViewModel(A.Fake<SearchPage>(), searchQueryParameter).Model)?.Results[0];

            Assert.Equal(expectedStatusList, result.StatusList);
        }

        [Fact]
        public void GetViewModel_ShouldMapSearchSuggestions_FromDidYouMean()
        {
            var suggestions = new List<string> { "suggestion 1", "suggestion 2" };
            A.CallTo(() => _searchService.GetDidYouMeanSuggestions(A<string>._)).Returns(suggestions);

            var result = (_mapper.GetPageViewModel(A.Fake<SearchPage>(), BuildQuery(query: "Q")).Model as ReactModels.SearchPage)?.Search
                .SearchSuggestions.Suggestions;

            Assert.Equal(suggestions, result);
        }

        [Fact]
        public void GetViewModel_ShouldMapSearchSuggestions_AsNull_WhenSuggestionsAreEmpty()
        {
            var suggestions = new List<string>();
            A.CallTo(() => _searchService.GetDidYouMeanSuggestions(A<string>._)).Returns(suggestions);

            var result = (_mapper.GetPageViewModel(A.Fake<SearchPage>(), BuildQuery(query: "Q")).Model as ReactModels.SearchPage)?.Search
                .SearchSuggestions;

            Assert.Null(result);
        }

        [Fact]
        public void GetViewModel_ShouldMapSearchSuggestions_AsNull_WhenSpellCheckIsDisabled()
        {
            var searchPage = A.Fake<SearchPage>();
            A.CallTo(() => searchPage.SpellCheck.IsDisabled(A<int>._)).Returns(true);

            var suggestions = new List<string> { "suggestion 1", "suggestion 2" };
            A.CallTo(() => _searchService.GetDidYouMeanSuggestions(A<string>._)).Returns(suggestions);

            var result = (_mapper.GetPageViewModel(searchPage, BuildQuery(query: "Q")).Model as ReactModels.SearchPage)?.Search
                .SearchSuggestions;

            Assert.Null(result);
        }

        [Fact]
        public void GetViewModel_WithEmptyQueryButNotEmptyRequest_ShouldNotMapTrackUrl()
        {
            var query = BuildQuery(categories: new string[] { "1" }, query: string.Empty);

            var searchHit = GetSearchHitForTest();
            searchHit.OriginalObjectGetter = new Func<object>(() => A.Fake<EditorialPage>());
            ArrangeSearch(new List<UnifiedSearchHit> { searchHit });
            A.CallTo(() => _searchTrackingService.TrackQuery(A<string>.That.IsNullOrEmpty(), A<int>._))
                .Returns(null);

            var result = ((ReactModels.SearchPage)_mapper.GetPageViewModel(
                    A.Fake<SearchPage>(),
                    query)
                .Model).Results[0];

            Assert.Null(result.TrackUrl);
        }

        private static UnifiedSearchHit GetSearchHitForTest()
        {
            var searchHit = A.Fake<UnifiedSearchHit>();
            searchHit.Title = "hit";
            searchHit.Excerpt = "intro";
            searchHit.Url = "url";
            return searchHit;
        }

        private void ArrangeTranslation(string expected, string label) =>
            A.CallTo(() => _localizationProvider.GetLabel(nameof(SearchPage), label))
                .Returns(expected);

        private static SearchQueryParameter BuildQuery(string[] subjects = null, string[] targetGroups = null, string[] types = null, string[] categories = null, string query = null, int? from = null, int? to = null, int? page = null) =>
            new SearchQueryParameter(subjects, targetGroups, types, categories, query, from, to, page);

        private void ArrangeSearch(
            List<UnifiedSearchHit> searchHits,
            List<CategoryGroup> categories = null,
            int? totalMatching = null
            )
        {
            var result = new SearchPageResults(
                searchHits,
                categories ?? new List<CategoryGroup>(),
                totalMatching ?? searchHits.Count);

            A.CallTo(() => _searchService.Search(A<SearchRequestBase>._))
                .Returns(result);
        }
    }
}