using System.Collections.Generic;
using System.Linq;
using EPiServer.Core;
using EPiServer.Web.Routing;
using FakeItEasy;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Configuration;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.QueryParameters;
using Forskningsradet.Core.Models.SearchModels;
using Forskningsradet.Core.Services;
using Forskningsradet.Core.Services.Contracts;
using Forskningsradet.Core.ViewModelMappers.Pages;
using Xunit;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.UnitTests.Core.ViewModelMappers.Pages
{
    public class EmployeeListPageViewModelMapperTests
    {
        private readonly IContentAreaReactModelBuilder _contentAreaReactModelBuilder;
        private readonly IEmployeeService _employeeService;
        private readonly IUrlResolver _urlResolver;
        private readonly ILocalizationProvider _localizationProvider;
        private readonly EmployeeListPageViewModelMapper _mapper;
        private readonly IPaginationConfiguration _paginationConfiguration;
        
        public EmployeeListPageViewModelMapperTests()
        {
            _contentAreaReactModelBuilder = A.Fake<IContentAreaReactModelBuilder>();
            _employeeService = A.Fake<IEmployeeService>();
            _urlResolver = A.Fake<IUrlResolver>();
            _localizationProvider = A.Fake<ILocalizationProvider>();
            _paginationConfiguration = A.Fake<IPaginationConfiguration>();
            
            _mapper = new EmployeeListPageViewModelMapper(
                _contentAreaReactModelBuilder, _employeeService,
                _urlResolver, _localizationProvider, _paginationConfiguration);
        }
        
        [Fact]
        public void GetViewModel_WithDefaultValues_ShouldMapDefaultFields()
        {
            const string expectedLetter = "P";
            const string expectedEmailLink = "mailto:a@b.cd";
            const string expectedUrl = "url-to-self";
            const string expectedLetterUrl = "url-to-self?q=&letter=P";
            const string expectedResultDescription = "search description";
            const string expectedSearchPlaceholder = "search placeholder";
            const string expectedSearchButtonText = "search";
            const string expectedEmailLabel = "email";
            var expectedSidebar = A.Fake<Forskningsradet.Core.Models.ReactModels.ContentArea>();
            
            var page = A.Fake<EmployeeListPage>();
            page.PageName = "Employee page";

            var personPage = GetPersonPageForTest("person page", "P");

            A.CallTo(() => _contentAreaReactModelBuilder.BuildReactModel(A<ContentArea>._, A<string>._, A<RenderOptionQueryParameter>._))
                .Returns(expectedSidebar);

            ArrangePageService(new List<EmployeeSearchHit> {personPage});
            ArrangeGetUrl(expectedUrl);
            ArrangeTranslation(expectedResultDescription, "ResultDescription");
            ArrangeTranslation(expectedSearchPlaceholder, "SearchPlaceholder");
            ArrangeTranslation(expectedSearchButtonText, "SearchButton");
            ArrangeTranslation(expectedEmailLabel, "EmailLabel");
            
            var result = (ReactModels.GroupedSearchPage) _mapper.GetPageViewModel(page, new EmployeeListQueryParameter()).Model;

            var employeeBlock = result.Results[0].Results.Blocks[0].ComponentData as ReactModels.EmployeeSearchResult;
            
            Assert.Equal(page.PageName, result.Title);
            Assert.Same(expectedSidebar, result.FilterLayout.ContentArea);
            Assert.Equal(1, result.Pagination.Pages.Count);
            Assert.Null(result.EmptyList);
            Assert.Equal(expectedLetter, result.Navigation[0].Link.Text);
            Assert.Equal(expectedLetterUrl, result.Navigation[0].Link.Url);
            Assert.Equal(expectedResultDescription, result.ResultsDescription);
            Assert.Equal(expectedUrl, result.Form.Endpoint);
            Assert.Equal(Common.Constants.QueryParameters.SearchQuery, result.Search.Input.Name);
            Assert.Equal(expectedSearchPlaceholder, result.Search.Input.Placeholder);
            Assert.Null(result.Search.Input.Value);
            Assert.Equal(expectedSearchButtonText, result.Search.Submit.Text);
            Assert.Equal(expectedLetter, result.Results[0].Title);
            Assert.NotEmpty(result.Results[0].Results.Blocks[0].Id);
            Assert.Equal(nameof(ReactModels.EmployeeSearchResult), result.Results[0].Results.Blocks[0].ComponentName);
            Assert.NotNull(employeeBlock);
            Assert.Equal(personPage.Name, employeeBlock.Title);
            Assert.Equal(personPage.JobTitle, employeeBlock.SubTitle);
            Assert.Equal(personPage.Department, employeeBlock.Texts[0]);
            Assert.Equal(expectedEmailLabel, employeeBlock.LabeledLinks[0].Label);
            Assert.Equal(expectedEmailLink, employeeBlock.LabeledLinks[0].Link.Url);
            Assert.Equal(personPage.Email, employeeBlock.LabeledLinks[0].Link.Text);
        }
        
        [Fact]
        public void GetViewModel_WhenEmployeeListIsEmpty_ShouldMapEmptyField()
        {
            const string expectedEmptyText = "no results";
            
            ArrangeTranslation(expectedEmptyText, "EmptyListLabel");
            
            var result = _mapper.GetPageViewModel(A.Fake<EmployeeListPage>(), A.Fake<EmployeeListQueryParameter>())
                .Model as ReactModels.GroupedSearchPage;
            
            Assert.Equal(expectedEmptyText, result?.EmptyList.Text);
        }
        
        [Fact]
        public void GetViewModel_WhenThereIsMoreThanOnePageOfData_ShouldShowPaginationWithNextLink()
        {
            const string expectedPage1Url = "?page=1";
            const string expectedPage2Url = "?page=2";
            const string expectedNextText = "next";
            var personA = GetPersonPageForTest("Aaa", "A");
            var personB = GetPersonPageForTest("Bbb", "B");

            ArrangePageService(new List<EmployeeSearchHit> {personA, personB});
            ArrangeGetUrl(string.Empty);
            ArrangeTranslation(expectedNextText, "PaginationNext");
            
            A.CallTo(() => _paginationConfiguration.EmployeeListPaginationSize).Returns(1);

            var result = (ReactModels.GroupedSearchPage) _mapper.GetPageViewModel(
                A.Fake<EmployeeListPage>(), A.Fake<EmployeeListQueryParameter>()).Model;
            
            Assert.True(result.Pagination.Pages[0].IsCurrent);
            Assert.Equal(expectedPage1Url, result.Pagination.Pages[0].Link.Url);
            Assert.Equal(1.ToString(), result.Pagination.Pages[0].Link.Text);
            
            Assert.False(result.Pagination.Pages[1].IsCurrent);
            Assert.Equal(expectedPage2Url, result.Pagination.Pages[1].Link.Url);
            Assert.Equal(2.ToString(), result.Pagination.Pages[1].Link.Text);
            
            Assert.False(result.Pagination.Pages[2].IsCurrent);
            Assert.Equal(expectedPage2Url, result.Pagination.Pages[2].Link.Url);
            Assert.Equal(expectedNextText, result.Pagination.Pages[2].Link.Text);
        }
        
        [Fact]
        public void GetViewModel_WhenResultIsPagedAndCurrentPageIsOtherThan1_ShouldMapPaginationWithPreviousLink()
        {
            const string expectedPage1Url = "?page=1";
            const string expectedPage2Url = "?page=2";
            const string expectedPreviousText = "prev";
            var query = new EmployeeListQueryParameter {Page = 2};
            var personA = GetPersonPageForTest("Aaa", "A");
            var personB = GetPersonPageForTest("Bbb", "B");
            
            ArrangePageService(new List<EmployeeSearchHit> {personA, personB});
            ArrangeGetUrl(string.Empty);
            ArrangeTranslation(expectedPreviousText, "PaginationPrevious");
            
            A.CallTo(() => _paginationConfiguration.EmployeeListPaginationSize).Returns(1);

            var result = (ReactModels.GroupedSearchPage) _mapper.GetPageViewModel(
                A.Fake<EmployeeListPage>(), query).Model;

            Assert.False(result.Pagination.Pages[0].IsCurrent);
            Assert.Equal(expectedPage1Url, result.Pagination.Pages[0].Link.Url);
            Assert.Equal(expectedPreviousText, result.Pagination.Pages[0].Link.Text);
            
            Assert.False(result.Pagination.Pages[1].IsCurrent);
            Assert.Equal(expectedPage1Url, result.Pagination.Pages[1].Link.Url);
            Assert.Equal(1.ToString(), result.Pagination.Pages[1].Link.Text);
            
            Assert.True(result.Pagination.Pages[2].IsCurrent);
            Assert.Equal(expectedPage2Url, result.Pagination.Pages[2].Link.Url);
            Assert.Equal(2.ToString(), result.Pagination.Pages[2].Link.Text);
        }
        
        [Fact]
        public void GetViewModel_WhenLetterIsPressed_ShouldNotMapPagination()
        {
            var query = new EmployeeListQueryParameter {Letter = 'a'};
            
            var result = _mapper.GetPageViewModel(A.Fake<EmployeeListPage>(), query).Model as ReactModels.GroupedSearchPage;
            
            Assert.NotNull(result);
            Assert.Null(result.Pagination);
        }
        
        [Fact]
        public void GetViewModel_WhenLetterIsPressed_ShouldNotLimitAvailableLetters()
        {
            var query = new EmployeeListQueryParameter {Letter = 'b'};
            var personA = GetPersonPageForTest("Aaa", "A");
            var personB = GetPersonPageForTest("Bbb", "B");
            
            ArrangePageService(new List<EmployeeSearchHit> {personA, personB});
            
            var result = _mapper.GetPageViewModel(A.Fake<EmployeeListPage>(), query).Model as ReactModels.GroupedSearchPage;
            
            Assert.NotNull(result?.Navigation[0].Link.Url);
        }
        
        [Fact]
        public void GetViewModel_WhenSearchTermIsSet_ShouldSetSearchTermInForm()
        {
            const string expectedQueryParamValue = "test";
            
            var query = new EmployeeListQueryParameter
            {
                Query = expectedQueryParamValue
            };

            var result = _mapper.GetPageViewModel(A.Fake<EmployeeListPage>(), query).Model as ReactModels.GroupedSearchPage;
            
            Assert.Equal(expectedQueryParamValue, result?.Search.Input.Value);
        }

        [Fact]
        public void GetViewModel_WhenResultsContainMultipleEmployeesWithSameFirstLetterInLastName_ShouldGroupResultsAccordingly()
        {
            const string expectedNameA1 = "a1";
            const string expectedNameA2 = "a2";
            const string expectedNameB = "b";
            
            var personA1 = GetPersonPageForTest(expectedNameA1, "A");
            var personA2 = GetPersonPageForTest(expectedNameA2, "A");
            var personB = GetPersonPageForTest(expectedNameB, "B");
            
            ArrangePageService(new List<EmployeeSearchHit> {personA1, personA2, personB});
            
            var result = (ReactModels.GroupedSearchPage) _mapper.GetPageViewModel(
                A.Fake<EmployeeListPage>(), A.Fake<EmployeeListQueryParameter>()).Model;

            var employeeA1 = result.Results[0].Results.Blocks[0].ComponentData as ReactModels.EmployeeSearchResult;
            var employeeA2 = result.Results[0].Results.Blocks[1].ComponentData as ReactModels.EmployeeSearchResult;
            var employeeB = result.Results[1].Results.Blocks[0].ComponentData as ReactModels.EmployeeSearchResult;
            
            Assert.Equal(expectedNameA1, employeeA1?.Title);
            Assert.Equal(expectedNameA2, employeeA2?.Title);
            Assert.Equal(expectedNameB, employeeB?.Title);
        }

        [Fact]
        public void GetViewModel_WhenThereAreNoSearchResultsForALetter_ShouldMapLetterAsGreyedOut()
        {
            var personA = GetPersonPageForTest("Aaa", "A");
            var personB = GetPersonPageForTest("Bbb", "B");
            var query = new EmployeeListQueryParameter {Query = "abc"};

            ArrangePageService(new List<EmployeeSearchHit> {personA, personB});

            A.CallTo(() => _employeeService.Search(A<string>._, A<char?>._, A<int>._, A<int>._))
                .Returns(GetSearchResultsForTest(new List<EmployeeSearchHit> {personA}));
            
            var result = (ReactModels.GroupedSearchPage)_mapper.GetPageViewModel(A.Fake<EmployeeListPage>(), query).Model;
            
            Assert.Equal(2, result.Navigation.Count);
            Assert.Null(result.Navigation[1].Link.Url);
        }
        
        [Fact]
        public void GetViewModel_WhenCreatingPaginationUrl_ShouldPreserveSearchQuery()
        {
            const string expectedUrl = "?q=abc&page=1";
            var query = new EmployeeListQueryParameter {Query = "abc", Page = 2};
            var personA = GetPersonPageForTest("Aaa", "A");
            var personB = GetPersonPageForTest("Bbb", "B");
            
            A.CallTo(() => _employeeService.Search(A<string>._, A<char?>._, A<int>._, A<int>._))
                .Returns(GetSearchResultsForTest(new List<EmployeeSearchHit> {personA, personB}));
            A.CallTo(() => _paginationConfiguration.EmployeeListPaginationSize).Returns(1);
            
            var result = (ReactModels.GroupedSearchPage)_mapper.GetPageViewModel(A.Fake<EmployeeListPage>(), query).Model;
            
            Assert.Equal(expectedUrl, result.Pagination.Pages[0].Link.Url);
        }
        
        [Fact]
        public void GetViewModel_WhenCreatingLetterUrl_ShouldPreserveSearchQuery()
        {
            const string expectedUrl = "?q=abc&letter=A";
            var query = new EmployeeListQueryParameter {Query = "abc"};
            var personA = GetPersonPageForTest("Aaa", "A");
            
            ArrangePageService(new List<EmployeeSearchHit> {personA});
            
            var result = (ReactModels.GroupedSearchPage)_mapper.GetPageViewModel(A.Fake<EmployeeListPage>(), query).Model;
            
            Assert.Equal(expectedUrl, result.Navigation[0].Link.Url);
        }
        
        private void ArrangePageService(IReadOnlyCollection<EmployeeSearchHit> personPages, IEnumerable<string> letters = null)
        {
            A.CallTo(() => _employeeService.GetAllLetters())
                .Returns(letters ?? personPages.Select(p => p.Letter));

            A.CallTo(() => _employeeService.Search(A<string>._, A<char?>._, A<int>._, A<int>._))
                .Returns(GetSearchResultsForTest(personPages));
        }

        private void ArrangeGetUrl(string expectedUrl)
        {
            A.CallTo(() => _urlResolver.GetUrl(A<ContentReference>._, A<string>._, A<UrlResolverArguments>._))
                .Returns(expectedUrl);
        }
        
        private void ArrangeTranslation(string expected, string label)
        {
            A.CallTo(() => _localizationProvider.GetLabel(nameof(EmployeeListPage), label))
                .Returns(expected);
        }

        private static EmployeeSearchHit GetPersonPageForTest(string name, string letter)
            => new EmployeeSearchHit
            {
                Name = name, JobTitle = "title", Department = "dept", Email = "a@b.cd", Letter = letter
            };

        private static EmployeeSearchResults GetSearchResultsForTest(IReadOnlyCollection<EmployeeSearchHit> personPages)
            => new EmployeeSearchResults(
                personPages,
                personPages.Select(p => p.Letter),
                personPages.Count);
    }
}