using System.Collections.Generic;
using EPiServer;
using EPiServer.Core;
using EPiServer.Web.Routing;
using FakeItEasy;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.QueryParameters;
using Forskningsradet.Core.Models.RequestModels;
using Forskningsradet.Core.Repositories.Contracts;
using Forskningsradet.Core.Services;
using Forskningsradet.Core.ViewModelMappers.Pages;
using Xunit;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.UnitTests.Core.ViewModelMappers.Pages
{
    public class LargeDocumentPageBaseViewModelMapperTests
    {
        private readonly IUrlResolver _urlResolver;
        private readonly IContentLoader _contentLoader;
        private readonly ILargeDocumentService _largeDocumentService;
        private readonly IPageRepository _pageRepository;
        private readonly IArticleHeaderReactModelBuilder _articleHeaderReactModelBuilder;
        private readonly ITableOfContentsReactModelBuilder _tableOfContentsReactModelBuilder;
        private readonly IRichTextReactModelBuilder _richTextReactModelBuilder;
        private readonly ILocalizationProvider _localizationProvider;
        private readonly IOptionsModalReactModelBuilder _optionsModalReactModelBuilder;
        private readonly LargeDocumentPageBaseViewModelMapper _mapper;

        public LargeDocumentPageBaseViewModelMapperTests()
        {
            _urlResolver = A.Fake<IUrlResolver>();
            _contentLoader = A.Fake<IContentLoader>();
            _largeDocumentService = A.Fake<ILargeDocumentService>();
            _pageRepository = A.Fake<IPageRepository>();
            _articleHeaderReactModelBuilder = A.Fake<IArticleHeaderReactModelBuilder>();
            _tableOfContentsReactModelBuilder = A.Fake<ITableOfContentsReactModelBuilder>();
            _richTextReactModelBuilder = A.Fake<IRichTextReactModelBuilder>();
            _localizationProvider = A.Fake<ILocalizationProvider>();
            _optionsModalReactModelBuilder = A.Fake<IOptionsModalReactModelBuilder>();

            _mapper = new LargeDocumentPageBaseViewModelMapper(
                _urlResolver,
                _contentLoader,
                _largeDocumentService,
                _pageRepository,
                _articleHeaderReactModelBuilder,
                _tableOfContentsReactModelBuilder,
                _richTextReactModelBuilder,
                _localizationProvider,
                _optionsModalReactModelBuilder);
        }

        [Fact]
        public void GetViewModel_WhenAllFieldsProvided_ShouldMapFields()
        {
            const string expectedLabel = "Label";
            const string expectedUrl = "url";
            var expectedHeader = new ReactModels.ArticleHeader();
            var expectedRichText = new ReactModels.RichText();
            var expectedTableOfContents = new ReactModels.TableOfContents();

            var page = A.Fake<LargeDocumentPage>();
            page.Title = "T";
            page.IntroTitle = "IntroTitle";
            page.PageName = "Name";
            page.ContentLink = new ContentReference(12);
            var tree = new Node(null, page.ContentLink.ToReferenceWithoutVersion(), 0);

            A.CallTo(() => _largeDocumentService.GetTree(A<ContentReference>._))
                .Returns(tree);
            A.CallTo(() => _localizationProvider.GetLabel(A<string>._, A<string>._))
                .Returns(expectedLabel);
            A.CallTo(() => _articleHeaderReactModelBuilder.BuildReactModel(page))
                .Returns(expectedHeader);
            A.CallTo(() => _richTextReactModelBuilder.BuildReactModel(A<XhtmlString>._, A<string>._))
                .Returns(expectedRichText);
            A.CallTo(() => _tableOfContentsReactModelBuilder.BuildReactModel(A<LargeDocumentModel>._))
                .Returns(expectedTableOfContents);
            A.CallTo(() => _urlResolver.GetUrl(A<ContentReference>._, A<string>._, A<UrlResolverArguments>._))
                .Returns(expectedUrl);

            var result = (ReactModels.LargeDocumentsPage)_mapper.GetPageViewModel(page, new LargeDocumentQueryParameter()).Model;

            Assert.Equal(page.Title, result.Title);
            Assert.Equal(expectedHeader, result.Header);
            Assert.Equal(expectedTableOfContents, result.TableOfContents);
            Assert.Equal(expectedLabel, result.PageNavigation.Previous.Text);
            Assert.Equal(expectedUrl, result.PageNavigation.Previous.Url);
            Assert.Equal(expectedLabel, result.PageNavigation.Next.Text);
            Assert.Equal(expectedUrl, result.PageNavigation.Next.Url);

            var mainContent = (ReactModels.RichTextBlock)result.Content.Blocks[0].ComponentData;
            Assert.Equal(page.IntroTitle, mainContent.Title);
            Assert.Same(expectedRichText, mainContent.Text);
            Assert.Equal(0, mainContent.HeadingLevelOffset);
        }

        [Fact]
        public void GetViewModel_WhenNotDownload_ShouldIncludeTableau()
        {
            var richTextWithTableau = GetRichTextWithTableau();
            var page = new LargeDocumentPage();
            var tree = new Node(null, page.ContentLink.ToReferenceWithoutVersion(), 0);

            A.CallTo(() => _largeDocumentService.GetTree(A<ContentReference>._))
                .Returns(tree);
            A.CallTo(() => _richTextReactModelBuilder.BuildReactModel(A<XhtmlString>._, A<string>._))
                .Returns(richTextWithTableau);

            var result = (ReactModels.LargeDocumentsPage)_mapper.GetPageViewModel(page, new LargeDocumentQueryParameter { Download = false }).Model;
            var mainContent = (ReactModels.RichTextBlock)result.Content.Blocks[0].ComponentData;

            Assert.Same(richTextWithTableau, mainContent.Text);
            Assert.Equal(1, mainContent.Text.Blocks.Count);
        }

        [Fact]
        public void GetViewModel_WhenDownload_ShouldNotIncludeTableau()
        {
            var richTextWithTableau = GetRichTextWithTableau();
            var page = new LargeDocumentPage();
            var tree = new Node(null, page.ContentLink.ToReferenceWithoutVersion(), 0);

            A.CallTo(() => _largeDocumentService.GetTree(A<ContentReference>._))
                .Returns(tree);
            A.CallTo(() => _richTextReactModelBuilder.BuildReactModel(A<XhtmlString>._, A<string>._))
                .Returns(richTextWithTableau);

            var result = (ReactModels.LargeDocumentsPage)_mapper.GetPageViewModel(page, new LargeDocumentQueryParameter { Download = true }).Model;
            var mainContent = (ReactModels.RichTextBlock)result.Content.Blocks[0].ComponentData;

            Assert.Same(richTextWithTableau, mainContent.Text);
            Assert.Equal(0, mainContent.Text.Blocks.Count);
        }

        [Fact]
        public void GetViewModel_WhenDownload_ShouldNotIncludeEmbedBlockWithoutSrc()
        {
            var richTextWithTableauWithoutSrc = GetRichTextWithTableauWithoutSrc();
            var page = new LargeDocumentPage();
            var tree = new Node(null, page.ContentLink.ToReferenceWithoutVersion(), 0);

            A.CallTo(() => _largeDocumentService.GetTree(A<ContentReference>._))
                .Returns(tree);
            A.CallTo(() => _richTextReactModelBuilder.BuildReactModel(A<XhtmlString>._, A<string>._))
                .Returns(richTextWithTableauWithoutSrc);

            var result = (ReactModels.LargeDocumentsPage)_mapper.GetPageViewModel(page, new LargeDocumentQueryParameter { Download = true }).Model;
            var mainContent = (ReactModels.RichTextBlock)result.Content.Blocks[0].ComponentData;

            Assert.Same(richTextWithTableauWithoutSrc, mainContent.Text);
            Assert.Equal(0, mainContent.Text.Blocks.Count);
        }

        private ReactModels.RichText GetRichTextWithTableau() =>
            new ReactModels.RichText
            {
                Blocks = new List<ReactModels.ContentAreaItem>
                {
                    new ReactModels.ContentAreaItem
                    {
                        Id = "1",
                        ComponentData = new ReactModels.EmbedBlock { Src = EmbedSourceConstants.Tableau.ValidSrcStart },
                        ComponentName = new TableauBlock().ReactComponentName()
                    }
                }
            };

        private ReactModels.RichText GetRichTextWithTableauWithoutSrc() =>
            new ReactModels.RichText
            {
                Blocks = new List<ReactModels.ContentAreaItem>
                {
                    new ReactModels.ContentAreaItem
                    {
                        Id = "1",
                        ComponentData = new ReactModels.EmbedBlock { Src = null },
                        ComponentName = new TableauBlock().ReactComponentName()
                    }
                }
            };

        private void ArrangeTranslation(string expected, string label)
        {
            A.CallTo(() => _localizationProvider.GetLabel(nameof(LargeDocumentPage), label))
                .Returns(expected);
        }
    }
}