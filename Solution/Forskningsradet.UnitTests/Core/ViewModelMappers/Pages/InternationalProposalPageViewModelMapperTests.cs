﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.Globalization;
using FakeItEasy;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Extensions;
using Forskningsradet.Core.Models.ApiModels.Evurdering;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Services;
using Forskningsradet.Core.Services.Contracts;
using Forskningsradet.Core.ViewModelMappers.Pages;
using Forskningsradet.UnitTests.Builders;
using Xunit;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.UnitTests.Core.ViewModelMappers.Pages
{
    public class InternationalProposalPageViewModelMapperTests
    {
        private readonly IRichTextReactModelBuilder _richTextReactModelBuilder;
        private readonly IOptionsModalReactModelBuilder _optionsModalReactModelBuilder;
        private readonly ILocalizationProvider _localizationProvider;
        private readonly IContentRepository _contentRepository;
        private readonly CategoryRepository _categoryRepository;
        private readonly ICategoryLocalizationProvider _categoryLocalizationProvider;
        private readonly IContentLoader _contentLoader;
        private readonly IArticleHeaderReactModelBuilder _articleHeaderReactModelBuilder;
        private readonly IProposalContactReactModelBuilder _proposalContactReactModelBuilder;
        private readonly IDateCardStatusHandler _dateCardStatusHandler;

        private readonly InternationalProposalPageViewModelMapper _mapper;

        public InternationalProposalPageViewModelMapperTests()
        {
            _richTextReactModelBuilder = A.Fake<IRichTextReactModelBuilder>();
            _optionsModalReactModelBuilder = A.Fake<IOptionsModalReactModelBuilder>();
            _categoryRepository = A.Fake<CategoryRepository>();
            _localizationProvider = A.Fake<ILocalizationProvider>();
            _contentRepository = A.Fake<IContentRepository>();
            _categoryRepository = A.Fake<CategoryRepository>();
            _categoryLocalizationProvider = A.Fake<ICategoryLocalizationProvider>();
            _contentLoader = A.Fake<IContentLoader>();
            _articleHeaderReactModelBuilder = A.Fake<IArticleHeaderReactModelBuilder>();
            _proposalContactReactModelBuilder = A.Fake<IProposalContactReactModelBuilder>();
            _dateCardStatusHandler = A.Fake<IDateCardStatusHandler>();
            ContentLanguage.Instance = A.Fake<ContentLanguage>();
            _mapper = new InternationalProposalPageViewModelMapper(
                _richTextReactModelBuilder,
                _optionsModalReactModelBuilder,
                _localizationProvider,
                _contentRepository,
                _categoryRepository,
                _categoryLocalizationProvider,
                _contentLoader,
                _articleHeaderReactModelBuilder,
                _proposalContactReactModelBuilder,
                _dateCardStatusHandler
            );
        }

        [Fact]
        public void WithDefaultValues_ShouldMapDefaultFields()
        {
            const string expectedCategoryName = "Bioøkonomi";
            var expectedArticleHeader = new ReactModels.ArticleHeader();
            var expectedRichText = new ReactModels.RichText();
            var expectedContactPerson = new ReactModels.ProposalContact();
            var expectedStatus = new ReactModels.DateCardStatus();

            var applicationTypePage = A.Fake<ApplicationTypePage>();
            applicationTypePage.Name = "Internasjonale fellesutlysninger";
            A.CallTo(() => _contentRepository.Get<ApplicationTypePage>(A<ContentReference>._, A<CultureInfo>._))
                .Returns(applicationTypePage);

            var category = new Category(expectedCategoryName, expectedCategoryName);
            A.CallTo(() => _categoryRepository.Get(1)).Returns(category);
            A.CallTo(() => _categoryLocalizationProvider.GetCategoryName(A<int>._)).Returns(expectedCategoryName);
            A.CallTo(() => _proposalContactReactModelBuilder.BuildReactModel(A<PersonPage>._, A<bool>._, A<bool>._))
                .Returns(expectedContactPerson);

            var page = A.Fake<InternationalProposalPage>();
            page.ProposalState = ProposalState.Planned;
            page.ApplicationTypeReference = applicationTypePage.PageLink;
            page.Program = "Europeisk fellesprogram";
            page.Category = new CategoryList(new List<int> { 1 });
            page.TextBlock.Title = "Om utlysningen";
            page.TextBlock.Intro = "intro";
            page.TextBlock.RichText = new XhtmlString("richtext");

            var expectedOptionsModal = new ReactModels.OptionsModal();
            A.CallTo(() => _articleHeaderReactModelBuilder.BuildReactModelWithoutShare(page))
                .Returns(expectedArticleHeader);
            A.CallTo(() => _richTextReactModelBuilder.BuildReactModel(page.TextBlock.RichText, A<string>._))
                .Returns(expectedRichText);
            A.CallTo(() => _optionsModalReactModelBuilder.BuildShareContent(page))
                .Returns(expectedOptionsModal);
            A.CallTo(() => _dateCardStatusHandler.GetCurrentStatus(A<InternationalProposalPage>._))
                .Returns(expectedStatus);

            var result = (ReactModels.ProposalPage)_mapper.GetPageViewModel(page).Model;

            Assert.Equal(expectedArticleHeader, result.Header);
            Assert.Equal(page.Name, result.Title);
            Assert.Equal(expectedStatus, result.StatusList.Single());
            Assert.Equal(applicationTypePage.Name, result.MetadataLeft.Items[0].Text);
            Assert.Equal(page.Program, result.MetadataLeft.Items[1].Text);

            Assert.Equal(expectedCategoryName, result.MetadataRight.Items[0].Text);
            Assert.Same(expectedContactPerson, result.Contact);
            Assert.Same(expectedOptionsModal, result.Share);

            Assert.Equal(page.TextBlock.Title, result.DescriptionTitle);
            Assert.Equal(page.TextBlock.Intro, result.DescriptionIngress);
            Assert.Equal(expectedRichText, result.DescriptionText);
        }

        [Fact]
        [UseCulture("nb-NO")]
        public void WithDefaultValues_ShouldMapDeadline()
        {
            var page = A.Fake<InternationalProposalPage>();
            page.ActivationDate = new DateTime(2050, 1, 1);
            var expectedActivationDate = page.ActivationDate.Value.ToDisplayDate().ToString("dd. MMMM yyyy");
            page.FirstDeadline = new DateTime(2060, 3, 24, 13, 0, 0);
            var expectedFirstDeadline = page.FirstDeadline.Value.DeadlineFormattedWithDaylightSavingNameOrStandardName();
            page.Deadline = new DateTime(2070, 6, 20, 13, 0, 0);
            var expectedDeadline = page.Deadline.Value.DeadlineFormattedWithDaylightSavingNameOrStandardName();

            var result = (ReactModels.ProposalPage)_mapper.GetPageViewModel(page).Model;

            Assert.Equal(expectedActivationDate, result.MetadataLeft.Items[1].Text);
            Assert.Equal(expectedFirstDeadline, result.MetadataLeft.Items[2].Text);
            Assert.Equal(expectedDeadline, result.MetadataLeft.Items[3].Text);
        }

        [Fact]
        [UseCulture("en-US")]
        public void WithDefaultValues_ShouldMapDeadlineEnglish()
        {
            var page = A.Fake<InternationalProposalPage>();
            page.ActivationDate = new DateTime(2050, 1, 1);
            var expectedActivationDate = page.ActivationDate.Value.ToDisplayDate().ToString("dd. MMMM yyyy");
            page.FirstDeadline = new DateTime(2060, 3, 24, 13, 0, 0);
            var expectedFirstDeadline = page.FirstDeadline.Value.DeadlineFormattedWithDaylightSavingNameOrStandardName();
            page.Deadline = new DateTime(2070, 6, 20, 13, 0, 0);
            var expectedDeadline = page.Deadline.Value.DeadlineFormattedWithDaylightSavingNameOrStandardName();

            var result = (ReactModels.ProposalPage)_mapper.GetPageViewModel(page).Model;

            Assert.Equal(expectedActivationDate, result.MetadataLeft.Items[1].Text);
            Assert.Equal(expectedFirstDeadline, result.MetadataLeft.Items[2].Text);
            Assert.Equal(expectedDeadline, result.MetadataLeft.Items[3].Text);
        }

        [Fact]
        [UseCulture("nb-NO")]
        public void WhenActivationDatePassed_ShouldNotMapActivationDate()
        {
            const string expectedActivationDate = "01. januar 2000";

            var page = (InternationalProposalPage)InternationalProposalPageBuilder.Create.Default()
                .WithActivationDate(new DateTime(2000, 1, 1))
                .WithDeadline(new DateTime(2050, 1, 1));

            var result = (ReactModels.ProposalPage)_mapper.GetPageViewModel(page).Model;

            Assert.DoesNotContain(result.MetadataLeft.Items, x => x.Text == expectedActivationDate);
        }

        [Fact]
        [UseCulture("nb-NO")]
        public void WhenDeadlinesHaveCustomNames_ShouldUseCustomNames()
        {
            var page = (InternationalProposalPage)InternationalProposalPageBuilder.Create.Default()
                .WithActivationDateDisplayName("Åpningsdato")
                .WithActivationDate(new DateTime(2048, 1, 1))
                .WithFirstDeadlineDisplayName("Skissefrist")
                .WithFirstDeadline(new DateTime(2049, 1, 1))
                .WithDeadlineDisplayName("Endelig søknadsfrist")
                .WithDeadline(new DateTime(2050, 1, 1));

            var result = (ReactModels.ProposalPage)_mapper.GetPageViewModel(page).Model;

            Assert.Equal(page.ActivationDateDisplayName, result.MetadataLeft.Items[0].Label);
            Assert.Equal(page.FirstDeadlineDisplayName, result.MetadataLeft.Items[1].Label);
            Assert.Equal(page.DeadlineDisplayName, result.MetadataLeft.Items[2].Label);
        }

        [Theory]
        [InlineData(TimeUnit.Date, "01. februar 2050")]
        [InlineData(TimeUnit.Week, "Uke 5, 2050")]
        [InlineData(TimeUnit.Month, "februar 2050")]
        [UseCulture("nb-NO")]
        public void WhenActivationDateSetForAPeriod_ShouldMapPeriod(TimeUnit timeUnit, string expectedActivationDate)
        {
            var page = (InternationalProposalPage)InternationalProposalPageBuilder.Create.Default()
                .WithActivationTimeUnit(timeUnit)
                .WithActivationDate(new DateTime(2050, 2, 1, 0, 0, 0, DateTimeKind.Utc));
            ArrangeTranslation("Uke {0}, {1}", "WeekFormat");

            var result = (ReactModels.ProposalPage)_mapper.GetPageViewModel(page).Model;

            Assert.Equal(expectedActivationDate, result.MetadataLeft.Items[0].Text);
        }

        [Fact]
        [UseCulture("nb-NO")]
        public void BuildDateCardStatusList_GivenInternationalProposalWithPastDeadline_ShouldReturnCompletedStatusTheme()
        {
            var expectedTheme = ReactModels.DateCardStatus_Theme.IsCompleted;
            var page = (InternationalProposalPage)InternationalProposalPageBuilder.Create.Default()
                .WithDeadline(new DateTime(2010, 4, 2));

            A.CallTo(() => _dateCardStatusHandler.GetCurrentStatus(A<InternationalProposalPage>.That.Matches(x => x.ProposalState == ProposalState.Completed)))
                .Returns(new ReactModels.DateCardStatus { Theme = expectedTheme });

            var result = (ReactModels.ProposalPage)_mapper.GetPageViewModel(page).Model;

            Assert.Equal(expectedTheme, result.StatusList.Single().Theme);
        }

        [Fact]
        public void BuildDateCardStatusList_GivenInternationalProposalWithoutActivationDate_ShouldReturnPlannedStatusTheme()
        {
            var expectedTheme = ReactModels.DateCardStatus_Theme.IsPlanned;
            var page = (InternationalProposalPage)InternationalProposalPageBuilder.Create.Default()
                .WithActivationDate(null);

            A.CallTo(() => _dateCardStatusHandler.GetCurrentStatus(A<InternationalProposalPage>.That.Matches(x => x.ProposalState == ProposalState.Planned)))
                .Returns(new ReactModels.DateCardStatus { Theme = expectedTheme });

            var result = (ReactModels.ProposalPage)_mapper.GetPageViewModel(page).Model;

            Assert.Equal(expectedTheme, result.StatusList.Single().Theme);
        }

        [Fact]
        public void BuildDateCardStatusList_GivenInternationalProposalWithPastActivationDate_ShouldReturnActiveStatusTheme()
        {
            var expectedTheme = ReactModels.DateCardStatus_Theme.IsActive;
            var page = (InternationalProposalPage)InternationalProposalPageBuilder.Create.Default()
                .WithActivationDate(new DateTime(2010, 4, 2));

            A.CallTo(() => _dateCardStatusHandler.GetCurrentStatus(A<InternationalProposalPage>.That.Matches(x => x.ProposalState == ProposalState.Active)))
                .Returns(new ReactModels.DateCardStatus { Theme = expectedTheme });

            var result = (ReactModels.ProposalPage)_mapper.GetPageViewModel(page).Model;

            Assert.Equal(expectedTheme, result.StatusList.Single().Theme);
        }

        private void ArrangeTranslation(string expected, string label) =>
            A.CallTo(() => _localizationProvider.GetLabel(nameof(InternationalProposalPage), label))
                .Returns(expected);
    }
}
