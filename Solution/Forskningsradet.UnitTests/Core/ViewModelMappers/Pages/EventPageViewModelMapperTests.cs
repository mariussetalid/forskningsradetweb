using System;
using System.Collections.Generic;
using System.Linq;
using EPiServer;
using EPiServer.Core;
using EPiServer.Web.Routing;
using FakeItEasy;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Facades.Contracts;
using Forskningsradet.Core.Factories;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.QueryParameters;
using Forskningsradet.Core.Models.ViewModels.Contracts;
using Forskningsradet.Core.Repositories.Contracts;
using Forskningsradet.Core.Services;
using Forskningsradet.Core.Services.Contracts;
using Forskningsradet.Core.ViewModelMappers.Pages;
using Forskningsradet.UnitTests.Builders;
using Xunit;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.UnitTests.Core.ViewModelMappers.Pages
{
    public class EventPageViewModelMapperTests
    {
        private readonly IContentAreaReactModelBuilder _contentAreaReactModelBuilder;
        private readonly IRichTextReactModelBuilder _richTextReactModelBuilder;
        private readonly IDateCardDatesReactModelBuilder _dateCardDatesReactModelBuilder;
        private readonly IFluidImageReactModelBuilder _fluidImageReactModelBuilder;
        private readonly IAccordionWithContentAreaListReactModelBuilder _accordionWithContentAreaListReactModelBuilder;
        private readonly IViewModelFactory _viewModelFactory;
        private readonly IUrlResolver _urlResolver;
        private readonly IUrlCheckingService _urlCheckingService;
        private readonly IContentLoader _contentLoader;
        private readonly ILocalizationProvider _localizationProvider;
        private readonly IPageEditingAdapter _pageEditingAdapter;
        private readonly IOptionsModalReactModelBuilder _optionsModalReactModelBuilder;
        private readonly IPageRepository _pageRepository;
        private readonly EventPageViewModelMapper _mapper;

        public EventPageViewModelMapperTests()
        {
            _contentAreaReactModelBuilder = A.Fake<IContentAreaReactModelBuilder>();
            _richTextReactModelBuilder = A.Fake<IRichTextReactModelBuilder>();
            _dateCardDatesReactModelBuilder = A.Fake<IDateCardDatesReactModelBuilder>();
            _fluidImageReactModelBuilder = A.Fake<IFluidImageReactModelBuilder>();
            _accordionWithContentAreaListReactModelBuilder = A.Fake<IAccordionWithContentAreaListReactModelBuilder>();
            _viewModelFactory = A.Fake<IViewModelFactory>();
            _urlResolver = A.Fake<IUrlResolver>();
            _urlCheckingService = A.Fake<IUrlCheckingService>();
            _contentLoader = A.Fake<IContentLoader>();
            _localizationProvider = A.Fake<ILocalizationProvider>();
            _pageEditingAdapter = A.Fake<IPageEditingAdapter>();
            _optionsModalReactModelBuilder = A.Fake<IOptionsModalReactModelBuilder>();
            _pageRepository = A.Fake<IPageRepository>();

            _mapper = new EventPageViewModelMapper(
                _contentAreaReactModelBuilder,
                _richTextReactModelBuilder,
                _dateCardDatesReactModelBuilder,
                _fluidImageReactModelBuilder,
                _accordionWithContentAreaListReactModelBuilder,
                _viewModelFactory,
                _urlResolver,
                _urlCheckingService,
                _contentLoader,
                _localizationProvider,
                _pageEditingAdapter,
                _optionsModalReactModelBuilder,
                _pageRepository);
        }

        [Fact]
        public void GetViewModel_WhenAllFieldsProvided_ShouldMapFields()
        {
            var expectedRichText = new ReactModels.RichText();
            var expectedContentArea = new ReactModels.ContentArea();
            var fakeContentArea = A.Fake<ContentArea>();
            var expectedEventData = new ReactModels.EventData { Items = new List<ReactModels.EventData_Items>() };
            var fakeEventDataReactViewModel = A.Fake<IReactViewModel>();
            var expectedOptionsModal = new ReactModels.OptionsModal();

            var eventPage = A.Fake<EventPage>();
            eventPage.PageName = "T";
            eventPage.StartDate = new DateTime();
            eventPage.EndDate = new DateTime();
            eventPage.HideCalendarLink = false;
            eventPage.VideoLink = new Url("v");
            eventPage.MainBodyTitle = "AL";
            eventPage.SpeakersTitle = "SpL";
            eventPage.ScheduleTitle = "SL";
            eventPage.MainBody = new XhtmlString("string");
            eventPage.ContactContentArea = fakeContentArea;
            eventPage.SpeakersContentArea = fakeContentArea;
            eventPage.ScheduleContentArea = fakeContentArea;

            A.CallTo(() => _richTextReactModelBuilder.BuildReactModel(A<XhtmlString>._, nameof(EventPage.MainBody)))
                .Returns(expectedRichText);
            A.CallTo(() => _contentAreaReactModelBuilder.BuildReactModel(A<ContentArea>._, A<string>._, A<RenderOptionQueryParameter>._))
                .Returns(expectedContentArea);
            A.CallTo(() => fakeContentArea.FilteredItems)
                .Returns(new List<ContentAreaItem> { new ContentAreaItem() });
            A.CallTo(() => fakeEventDataReactViewModel.Model)
                .Returns(expectedEventData);
            A.CallTo(() => _viewModelFactory.GetPartialViewModel(A<EventDataBlock>._))
                .Returns(fakeEventDataReactViewModel);
            A.CallTo(() => _optionsModalReactModelBuilder.BuildShareContent(eventPage))
                .Returns(expectedOptionsModal);

            var result = (ReactModels.EventPage)_mapper.GetPageViewModel(eventPage).Model;

            Assert.Equal(eventPage.PageName, result.Title);
            Assert.Contains(result.Links, x => x.Icon == ReactModels.EventPageLink_Icon.Video);
            Assert.Equal(eventPage.MainBodyTitle, result.Labels.About.Title);
            Assert.Equal(eventPage.SpeakersTitle, result.Labels.Speakers.Title);
            Assert.Equal(eventPage.ScheduleTitle, result.Labels.Schedule.Title);
            Assert.Same(expectedRichText, result.RichText);
            Assert.Same(expectedContentArea, result.ContactInfo);
            Assert.Same(expectedContentArea, result.Speakers);
            Assert.Same(expectedContentArea, result.Schedule);
            Assert.Same(expectedEventData, result.Metadata);
            Assert.Equal(expectedEventData.Items, result.Metadata.Items);
            Assert.Same(expectedOptionsModal, result.Share);
        }

        [Fact]
        public void GetViewModel_WhenContentAreasAreEmpty_ShouldSetNull()
        {
            var eventPage = new EventPage
            {
                ContactContentArea = new ContentArea(),
                SpeakersContentArea = new ContentArea(),
                ScheduleContentArea = new ContentArea()
            };

            var result = (ReactModels.EventPage)_mapper.GetPageViewModel(eventPage).Model;

            Assert.Null(result.ContactInfo);
            Assert.Null(result.Speakers);
            Assert.Null(result.Schedule);
        }

        [Fact]
        public void GetViewModel_WhenRegistrationLinkIsEmpty_ShouldHideButton()
        {
            var result = (ReactModels.EventPage)_mapper.GetPageViewModel(new EventPage()).Model;

            Assert.Null(result.RegistrationLink.Url);
            Assert.Null(result.RegistrationLink.Text);
        }

        [Fact]
        public void GetViewModel_WhenDatesIsFuture_ShouldMapRegistrationLinkAndCalendarLink()
        {
            const string expectedRegistrationLinkUrl = "reg";
            const string expectedLinkText = "Link";
            A.CallTo(() => _urlResolver.GetUrl(A<UrlBuilder>.That.Matches(x => x.Path == expectedRegistrationLinkUrl), A<UrlResolverArguments>._))
                .Returns(expectedRegistrationLinkUrl);
            A.CallTo(() => _localizationProvider.GetLabel(A<string>._, A<string>._))
                .Returns(expectedLinkText);
            ArrangeEventDataBlock();

            var eventPage = A.Fake<EventPage>();
            eventPage.StartDate = new DateTime(2045, 12, 15);
            eventPage.EndDate = new DateTime(2045, 12, 24);
            eventPage.HideCalendarLink = false;
            eventPage.RegistrationLink = new Url(expectedRegistrationLinkUrl);
            A.CallTo(() => eventPage.ComputedEventEndDate)
                .Returns(eventPage.EndDate);

            var result = (ReactModels.EventPage)_mapper.GetPageViewModel(eventPage).Model;

            Assert.Contains(result.Links, x => x.Icon == ReactModels.EventPageLink_Icon.Calendar);
            Assert.Equal(expectedRegistrationLinkUrl, result.RegistrationLink.Url);
            Assert.Equal(expectedLinkText, result.RegistrationLink.Text);
        }

        [Fact]
        public void GetViewModel_WhenRegistrationLinkIsSetButEventHasPassed_ShouldDisableButtonAndHideCalendarLink()
        {
            const string expectedLinkText = "Link";
            const string disabledUrl = "disabledurl";
            A.CallTo(() => _urlResolver.GetUrl(A<UrlBuilder>.That.Matches(x => x.Path == disabledUrl), A<UrlResolverArguments>._))
                .Returns(disabledUrl);
            A.CallTo(() => _localizationProvider.GetLabel(A<string>._, A<string>._))
                .Returns(expectedLinkText);
            ArrangeEventDataBlock();

            var eventPage = A.Fake<EventPage>();
            eventPage.StartDate = new DateTime(2000, 12, 15);
            eventPage.EndDate = new DateTime(2000, 12, 24);
            eventPage.HideCalendarLink = false;
            eventPage.RegistrationLink = new Url(disabledUrl);
            A.CallTo(() => eventPage.ComputedEventEndDate)
                .Returns(eventPage.EndDate);

            var result = (ReactModels.EventPage)_mapper.GetPageViewModel(eventPage).Model;

            Assert.DoesNotContain(result.Links, x => x.Icon == ReactModels.EventPageLink_Icon.Calendar);
            Assert.Null(result.RegistrationLink.Url);
            Assert.Equal(expectedLinkText, result.RegistrationLink.Text);
        }

        [Fact]
        public void GetViewModel_WhenDigitalEvent_ShouldDisplayStreamingText()
        {
            const string expectedStreamText = "Stream";
            var eventPage = A.Fake<EventPage>();
            eventPage.EventData.Type = EventType.Digital;
            A.CallTo(() => _localizationProvider.GetLabel(A<string>._, A<string>._))
                .Returns(expectedStreamText);

            var result = (ReactModels.EventPage)_mapper.GetPageViewModel(eventPage).Model;

            Assert.Contains(result.Media.Items, x => x.Icon == ReactModels.DateCardMedia_Items_Icon.Camera);
            Assert.Contains(result.Media.Items, x => x.Text == expectedStreamText);
        }

        [Fact]
        public void GetViewModel_WhenDigitalEventAndRecordingWillBeAvailable_ShouldDisplayStreamingAndRecordingText()
        {
            const string expectedStreamAndRecordingText = "StreamAndRecording";
            var eventPage = A.Fake<EventPage>();
            eventPage.EventData.Type = EventType.Digital;
            eventPage.EventData.RecordingWillBeAvailable = true;
            A.CallTo(() => _localizationProvider.GetLabel(A<string>._, A<string>.That.IsSameAs("StreamedAndRecordedEvent")))
                .Returns(expectedStreamAndRecordingText);

            var result = (ReactModels.EventPage)_mapper.GetPageViewModel(eventPage).Model;

            Assert.Contains(result.Media.Items, x => x.Text == expectedStreamAndRecordingText);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        public void GetViewModel_WhenTitlesAreNullOrEmpty_ShouldFallbackToLanguageProvider(string nullOrEmptyTitle)
        {
            const string expectedLabel = "label";

            var eventPage = new EventPage
            {
                MainBodyTitle = nullOrEmptyTitle,
                SpeakersTitle = nullOrEmptyTitle,
                ScheduleTitle = nullOrEmptyTitle,
            };

            A.CallTo(() => _localizationProvider.GetLabel(A<string>._, A<string>._))
                .Returns(expectedLabel);

            var result = (ReactModels.EventPage)_mapper.GetPageViewModel(eventPage).Model;

            Assert.Equal(expectedLabel, result.Labels.About.Title);
            Assert.Equal(expectedLabel, result.Labels.Speakers.Title);
            Assert.Equal(expectedLabel, result.Labels.Schedule.Title);
        }

        [Fact]
        public void GetViewModel_WhenCanceled_ShouldAddCanceledPrefixToTitle()
        {
            var expectedTitlePrefix = "C_A_N_C_E_L_E_D !!! ";

            EventPage eventPage = EventPageBuilder.Create.Default()
                .WithCanceled(true)
                .WithName("Page name");

            A.CallTo(() => _localizationProvider.GetLabel(nameof(EventPage), "CanceledEventTitlePrefix"))
                .Returns(expectedTitlePrefix);

            var result = (ReactModels.EventPage)_mapper.GetPageViewModel(eventPage).Model;

            Assert.StartsWith(expectedTitlePrefix, result.Title);
        }

        [Fact]
        public void GetViewModel_WhenCanceled_ShouldProvideCancelledEventImageFromFrontPage()
        {
            EventPage eventPage = EventPageBuilder.Create.Default()
                .WithCanceled(true);

            var frontPage = A.Fake<FrontPageBase>();
            A.CallTo(() => _pageRepository.GetCurrentStartPageAsFrontPageBase()).Returns(frontPage);

            _mapper.GetPageViewModel(eventPage);

            A.CallTo(() => frontPage.CanceledEventImage).MustHaveHappened();
        }

        [Fact]
        public void GetViewModel_WhenCanceled_ShouldNotProvideRegistrationLink()
        {
            EventPage eventPage = EventPageBuilder.Create.Default()
                .WithCanceled(true)
                .WithRegistrationLink(new Url("http://register-here.com"));

            var result = (ReactModels.EventPage)_mapper.GetPageViewModel(eventPage).Model;

            Assert.Null(result.RegistrationLink);
        }

        [Fact]
        public void GetViewMode_WhenCanceled_ShouldNotProvideCalendarLink()
        {
            var addToCalendarLinkText = "Add to calendar";

            EventPage eventPage = EventPageBuilder.Create.Default()
                .WithCanceled(true)
                .WithEventData(new EventDataBlock { WhenList = new List<string>() { "tomorrow" } })
                .WithStartDate(DateTime.Today)
                .WithEndDate(DateTime.Today.AddDays(1));

            A.CallTo(() => _localizationProvider.GetLabel(nameof(EventPage), "AddToCalendar"))
                .Returns(addToCalendarLinkText);

            var result = (ReactModels.EventPage)_mapper.GetPageViewModel(eventPage).Model;

            var addToCalendarLink = result.Links.FirstOrDefault(x => x.Text == addToCalendarLinkText);

            Assert.Null(addToCalendarLink);
        }

        [Fact]
        public void GetViewModel_WhenPageIsInEditMode_ShouldMapFullRefreshProperties()
        {
            var expectedFullRefreshProperties = new[]
            {
                nameof(EventPage.EventData),
                nameof(EventPage.RegistrationLink)
            };

            A.CallTo(() => _pageEditingAdapter.PageIsInEditMode())
                .Returns(true);

            var result = _mapper.GetPageViewModel(new EventPage());

            Assert.Equal(expectedFullRefreshProperties, result.FullRefreshProperties);
        }

        [Fact]
        public void GetViewModel_WhenPageIsInEditMode_ShouldMapOnPageEditing()
        {
            A.CallTo(() => _richTextReactModelBuilder.BuildReactModel(A<XhtmlString>._, A<string>._))
                .Returns(new ReactModels.RichText { OnPageEditing = new ReactModels.ContentArea_OnPageEditing() });
            A.CallTo(() => _contentAreaReactModelBuilder.BuildReactModel(A<ContentArea>._, A<string>._, A<RenderOptionQueryParameter>._))
                .Returns(new ReactModels.ContentArea { OnPageEditing = new ReactModels.ContentArea_OnPageEditing() });
            A.CallTo(() => _pageEditingAdapter.PageIsInEditMode())
                .Returns(true);

            var result = (ReactModels.EventPage)_mapper.GetPageViewModel(new EventPage()).Model;

            Assert.NotNull(result.OnPageEditing);
            Assert.NotNull(result.Metadata.OnPageEditing);
            Assert.NotNull(result.RegistrationLink.OnPageEditing);
            Assert.NotNull(result.RichText.OnPageEditing);
            Assert.NotNull(result.ContactInfo.OnPageEditing);
            Assert.NotNull(result.Speakers.OnPageEditing);
            Assert.NotNull(result.Schedule.OnPageEditing);
        }

        private void ArrangeEventDataBlock()
        {
            var expectedEventData = new ReactModels.EventData { Items = new List<ReactModels.EventData_Items>() };
            var fakeEventDataReactViewModel = A.Fake<IReactViewModel>();
            A.CallTo(() => fakeEventDataReactViewModel.Model)
                .Returns(expectedEventData);
            A.CallTo(() => _viewModelFactory.GetPartialViewModel(A<EventDataBlock>._))
                .Returns(fakeEventDataReactViewModel);
        }
    }
}