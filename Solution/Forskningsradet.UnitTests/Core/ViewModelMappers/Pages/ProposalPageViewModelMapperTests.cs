using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.Globalization;
using EPiServer.Web.Routing;
using FakeItEasy;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Configuration;
using Forskningsradet.Core.Extensions;
using Forskningsradet.Core.Facades.Contracts;
using Forskningsradet.Core.Factories;
using Forskningsradet.Core.Models.ApiModels.Evurdering;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.ContentModels.PropertyList;
using Forskningsradet.Core.Models.QueryParameters;
using Forskningsradet.Core.Models.ViewModels.Blocks;
using Forskningsradet.Core.Repositories.Contracts;
using Forskningsradet.Core.Services;
using Forskningsradet.Core.Services.Contracts;
using Forskningsradet.Core.ViewModelMappers.Pages;
using Forskningsradet.UnitTests.Builders;
using Forskningsradet.UnitTests.TestUtils;
using Xunit;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.UnitTests.Core.ViewModelMappers.Pages
{
    public class ProposalPageViewModelMapperTests
    {
        private readonly IProposalService _proposalService;
        private readonly ILocalizationProvider _localizationProvider;
        private readonly IRichTextReactModelBuilder _richTextReactModelBuilder;
        private readonly IContentRepository _contentRepository;
        private readonly IPageRepository _pageRepository;
        private readonly IUrlResolver _urlResolver;
        private readonly IProposalPageConfiguration _proposalPageConfiguration;
        private readonly IViewModelFactory _viewModelFactory;
        private readonly CategoryRepository _categoryRepository;
        private readonly ProposalPageViewModelMapper _mapper;
        private readonly IPageEditingAdapter _pageEditingAdapter;
        private readonly IContentAreaReactModelBuilder _contentAreaReactModelBuilder;
        private readonly IContentLoader _contentLoader;
        private readonly ICategoryLocalizationProvider _categoryLocalizationProvider;
        private readonly IPersonPageStringBuilder _personPageStringBuilder;
        private readonly IOptionsModalReactModelBuilder _optionsModalReactModelBuilder;
        private readonly IDateCardStatusHandler _dateCardStatusHandler;
        private readonly IProposalContactReactModelBuilder _proposalContactReactModelBuilder;
        private readonly IArticleHeaderReactModelBuilder _articleHeaderReactModelBuilder;
        private readonly IDownloadListReactModelBuilder _downloadListReactModelBuilder;

        public ProposalPageViewModelMapperTests()
        {
            _proposalService = A.Fake<IProposalService>();
            _localizationProvider = A.Fake<ILocalizationProvider>();
            _richTextReactModelBuilder = A.Fake<IRichTextReactModelBuilder>();
            _contentRepository = A.Fake<IContentRepository>();
            _pageRepository = A.Fake<IPageRepository>();
            _urlResolver = A.Fake<IUrlResolver>();
            _proposalPageConfiguration = A.Fake<IProposalPageConfiguration>();
            _viewModelFactory = A.Fake<IViewModelFactory>();
            _categoryRepository = A.Fake<CategoryRepository>();
            _pageEditingAdapter = A.Fake<IPageEditingAdapter>();
            _contentAreaReactModelBuilder = A.Fake<IContentAreaReactModelBuilder>();
            _contentLoader = A.Fake<IContentLoader>();
            _contentLoader = A.Fake<IContentLoader>();
            _categoryLocalizationProvider = A.Fake<ICategoryLocalizationProvider>();
            _personPageStringBuilder = A.Fake<IPersonPageStringBuilder>();
            _optionsModalReactModelBuilder = A.Fake<IOptionsModalReactModelBuilder>();
            _dateCardStatusHandler = A.Fake<IDateCardStatusHandler>();
            _proposalContactReactModelBuilder = A.Fake<IProposalContactReactModelBuilder>();
            _articleHeaderReactModelBuilder = A.Fake<IArticleHeaderReactModelBuilder>();
            _downloadListReactModelBuilder = A.Fake<IDownloadListReactModelBuilder>();
            ContentLanguage.Instance = A.Fake<ContentLanguage>();
            _mapper = new ProposalPageViewModelMapper(
                _proposalService,
                _richTextReactModelBuilder,
                _localizationProvider,
                _contentRepository,
                _pageRepository,
                _urlResolver,
                _proposalPageConfiguration,
                _viewModelFactory,
                _contentAreaReactModelBuilder,
                _pageEditingAdapter,
                _contentLoader,
                _categoryLocalizationProvider,
                _categoryRepository,
                _personPageStringBuilder,
                _optionsModalReactModelBuilder,
                _dateCardStatusHandler,
                _proposalContactReactModelBuilder,
                _articleHeaderReactModelBuilder,
                _downloadListReactModelBuilder);
        }

        [Fact]
        public void WithDefaultValues_ShouldMapDefaultFields()
        {
            const string expectedDescriptionTitle = "purpose";
            var personPage = new PersonPage();
            ArrangeTranslation(expectedDescriptionTitle, "PurposeTitle");
            var contactContentArea = A.Fake<ContentArea>();

            var page = A.Fake<ProposalPage>();
            page.Name = "title";
            page.Contact.Contacts = contactContentArea;

            var expectedRichTextComponent = new ReactModels.RichText();
            var expectedOptionsModal = new ReactModels.OptionsModal();
            var expectedDateCardStatusComponentList = new List<ReactModels.DateCardStatus>();
            var expectedProposalContactComponent = new ReactModels.ProposalContact();
            A.CallTo(() => _richTextReactModelBuilder.BuildReactModel(A<XhtmlString>._, A<string>._))
                .Returns(expectedRichTextComponent);
            A.CallTo(() => _optionsModalReactModelBuilder.BuildShareContent(page))
                .Returns(expectedOptionsModal);
            A.CallTo(() => _dateCardStatusHandler.BuildDateCardStatusList(page))
                .Returns(expectedDateCardStatusComponentList);
            A.CallTo(() => _contentLoader.TryGet(A<ContentReference>._, out personPage))
                .Returns(true);
            A.CallTo(() => _proposalContactReactModelBuilder.BuildReactModel(A<PersonPage>._, A<bool>._, A<bool>._))
                .Returns(expectedProposalContactComponent);
            A.CallTo(() => contactContentArea.FilteredItems)
                .Returns(new List<ContentAreaItem> { A.Fake<ContentAreaItem>() });

            var result = (ReactModels.ProposalPage)_mapper.GetPageViewModel(page).Model;

            Assert.Equal(page.Name, result.Title);
            Assert.Equal(expectedDescriptionTitle, result.DescriptionTitle);
            Assert.Equal(expectedRichTextComponent, result.DescriptionText);
            Assert.Same(expectedOptionsModal, result.Share);
            Assert.Equal(expectedDateCardStatusComponentList, result.StatusList);
            Assert.Equal(expectedProposalContactComponent, result.Contact);
        }

        [Fact]
        public void WhenProposalIsActive_ShouldMapApplyButtonOnLastTabContentSection()
        {
            const string expectedUrl = "url?1=123&2=no";
            const string expectedApplyButtonText = "apply";
            ArrangeTranslation(expectedApplyButtonText, "ApplyButtonText");

            A.CallTo(() => _proposalPageConfiguration.MyPageUrl)
                .Returns("url?1={0}&2={1}");

            var page = (ProposalPage)ProposalPageBuilder.Create.Default()
                .WithProposalState(ProposalState.Active)
                .WithSpecificApplicationTypeId("123");

            var result = (ReactModels.ProposalPage)_mapper.GetPageViewModel(page).Model;

            Assert.Equal(expectedApplyButtonText, result.ApplyButton.Text);
            Assert.Equal(expectedUrl, result.ApplyButton.Url);
            Assert.Equal(expectedApplyButtonText, result.Tabs.Items[0].Content.ContentSections.Last().Application.Link.Text);
            Assert.Equal(expectedUrl, result.Tabs.Items[0].Content.ContentSections.Last().Application.Link.Url);
        }

        [Fact]
        public void WhenProposalIsActive_ShouldMapApplyButtonWithDefaultTextOnLastTabContentSection()
        {
            const string name = "Proposal name";
            const string applicationTextFormat = "Expected{0}";
            var expectedText = string.Format(applicationTextFormat, name);

            ArrangeTranslation(applicationTextFormat, "ApplicationTextFormat");

            var page = (ProposalPage)ProposalPageBuilder.Create.Default()
                .WithName(name)
                .WithProposalState(ProposalState.Active);

            var result = (ReactModels.ProposalPage)_mapper.GetPageViewModel(page).Model;

            Assert.Equal(expectedText, result.Tabs.Items[0].Content.ContentSections.Last().Application.Text);
        }

        [Fact]
        public void WhenProposalIsActive_ShouldMapApplyButtonWithEditorTextOnLastTabContentSection()
        {
            const string expectedText = "Application Text";
            const string name = "Proposal name";
            const string applicationTextFormat = "TextFormat{0}";

            ArrangeTranslation(applicationTextFormat, "ApplicationTextFormat");

            var page = (ProposalPage)ProposalPageBuilder.Create.Default()
                .WithName(name)
                .WithProposalState(ProposalState.Active)
                .WithApplicationText(expectedText);

            var result = (ReactModels.ProposalPage)_mapper.GetPageViewModel(page).Model;

            Assert.Equal(expectedText, result.Tabs.Items[0].Content.ContentSections.Last().Application.Text);
        }

        [Theory]
        [InlineData(ProposalState.Planned)]
        [InlineData(ProposalState.Deleted)]
        [InlineData(ProposalState.Cancelled)]
        [InlineData(ProposalState.Completed)]
        public void WhenProposalIsNotActive_ShouldNotMapApplyButton(ProposalState state)
        {
            var page = A.Fake<ProposalPage>();
            page.ProposalState = state;

            var result = (ReactModels.ProposalPage)_mapper.GetPageViewModel(page).Model;

            Assert.Null(result.ApplyButton);
            Assert.Null(result.Tabs.Items[0].Content.ContentSections[0].Application?.Link);
        }

        [Fact]
        public void WithDefaultValues_ShouldMapApplicationType()
        {
            const string expectedApplicationTypeLabel = "type";
            ArrangeTranslation(expectedApplicationTypeLabel, "ApplicationType");

            var applicationTypePage = A.Fake<ApplicationTypePage>();
            applicationTypePage.Name = "type name";
            applicationTypePage.ArticlePageReference = null;
            A.CallTo(() => _contentRepository.Get<ApplicationTypePage>(A<ContentReference>._, A<CultureInfo>._))
                .Returns(applicationTypePage);

            var result = ((ReactModels.ProposalPage)
                _mapper.GetPageViewModel(A.Fake<ProposalPage>()).Model).MetadataLeft.Items[0];

            Assert.Equal(expectedApplicationTypeLabel, result.Label);
            Assert.Equal(applicationTypePage.Name, result.Text);
        }

        [Fact]
        public void WhenApplicationTypeHasArticleReference_ShouldMapApplicationTypeWithLink()
        {
            const string expectedApplicationTypeLabel = "type";
            ArrangeTranslation(expectedApplicationTypeLabel, "ApplicationType");

            const string expectedUrl = "url";
            ArrangeGetUrl(expectedUrl);

            var applicationTypePage = A.Fake<ApplicationTypePage>();
            applicationTypePage.Name = "type name";
            applicationTypePage.ArticlePageReference = PageReference.EmptyReference;
            A.CallTo(() => _contentRepository.Get<ApplicationTypePage>(A<ContentReference>._, A<CultureInfo>._))
                .Returns(applicationTypePage);

            var result = ((ReactModels.ProposalPage)
                _mapper.GetPageViewModel(A.Fake<ProposalPage>()).Model).MetadataLeft.Items[0];

            Assert.Equal(expectedApplicationTypeLabel, result.Label);
            Assert.Equal(applicationTypePage.Name, result.Links[0].Text);
            Assert.Equal(expectedUrl, result.Links[0].Url);
        }

        [Fact]
        public void WhenHasProjectOutlineRequirementAndReference_ShouldMapProjectOutlineLink()
        {
            const string expectedUrl = "url-to-outline";
            const string expectedProjectOutlineLabel = "project outline";
            ArrangeTranslation(expectedProjectOutlineLabel, "MandatoryProjectOutline");

            var projectOutlineReference = A.Fake<ProposalPage>();
            projectOutlineReference.Name = "project outline proposal";
            A.CallTo(() => _contentRepository.Get<ProposalPage>(A<ContentReference>._, A<CultureInfo>._))
                .Returns(projectOutlineReference);

            ArrangeGetUrl(expectedUrl);

            var page = A.Fake<ProposalPage>();
            page.ProjectOutlineRequirement = ProjectOutlineRequirement.Mandatory;
            var result = ((ReactModels.ProposalPage)
                _mapper.GetPageViewModel(page).Model).MetadataLeft.Items[1];

            Assert.Equal(expectedProjectOutlineLabel, result.Label);
            Assert.Equal(projectOutlineReference.Name, result.Links[0].Text);
            Assert.Equal(expectedUrl, result.Links[0].Url);
        }

        [Fact]
        public void WhenHasNonMandatoryProjectOutlineRequirement_ShouldMapNonMandatoryLabel()
        {
            const string expectedProjectOutlineLabel = "project outline";
            ArrangeTranslation(expectedProjectOutlineLabel, "ProjectOutline");

            A.CallTo(() => _contentRepository.Get<ProposalPage>(A<ContentReference>._, A<CultureInfo>._))
                .Returns(A.Fake<ProposalPage>());

            var page = A.Fake<ProposalPage>();
            page.ProjectOutlineRequirement = ProjectOutlineRequirement.NonMandatory;
            var result = ((ReactModels.ProposalPage)
                _mapper.GetPageViewModel(page).Model).MetadataLeft.Items[1];

            Assert.Equal(expectedProjectOutlineLabel, result.Label);
        }

        [Fact]
        public void WhenProjectOutlineChildrenHasValues_ShouldMapLinks()
        {
            const string expectedProjectOutlineForLabel = "project outline for";
            const string expectedUrl1 = "url-1";
            const string expectedUrl2 = "url-2";

            ArrangeTranslation(expectedProjectOutlineForLabel, "ProjectOutlineFor");

            var contentReference1 = new ContentReference(1);
            var contentReference2 = new ContentReference(2);

            var outlinePage1 = A.Fake<ProposalPage>();
            outlinePage1.Name = "outline 1";
            outlinePage1.ContentLink = contentReference1;

            var outlinePage2 = A.Fake<ProposalPage>();
            outlinePage2.Name = "outline 2";
            outlinePage2.ContentLink = contentReference2;

            A.CallTo(() => _contentRepository.Get<ProposalPage>(contentReference1, A<CultureInfo>._))
                .Returns(outlinePage1);

            A.CallTo(() => _contentRepository.Get<ProposalPage>(contentReference2, A<CultureInfo>._))
                .Returns(outlinePage2);

            A.CallTo(() => _urlResolver.GetUrl(outlinePage1.ContentLink, A<string>._, A<UrlResolverArguments>._))
                .Returns(expectedUrl1);

            A.CallTo(() => _urlResolver.GetUrl(outlinePage2.ContentLink, A<string>._, A<UrlResolverArguments>._))
                .Returns(expectedUrl2);

            var page = A.Fake<ProposalPage>();
            page.ChildProjectOutlines = new List<ContentReference>
            {
                contentReference1,
                contentReference2
            };

            var result = ((ReactModels.ProposalPage)
                _mapper.GetPageViewModel(page).Model).MetadataLeft.Items[1];

            Assert.Equal(expectedProjectOutlineForLabel, result.Label);
            Assert.Equal(outlinePage1.Name, result.Links[0].Text);
            Assert.Equal(expectedUrl1, result.Links[0].Url);
            Assert.Equal(outlinePage2.Name, result.Links[1].Text);
            Assert.Equal(expectedUrl2, result.Links[1].Url);
        }

        [Theory]
        [InlineData("17. desember 2018, 12:00 CET", 2018, 12, 17, 11)]
        [InlineData("6. juni 2018, 12:00 CEST", 2018, 6, 6, 10)]
        public void WithDefaultValues_ShouldMapDeadline(string expectedDeadline, int year, int month, int day, int hour)
        {
            const string expectedDeadlineLabel = "deadline";

            ArrangeTranslation(expectedDeadlineLabel, "Deadline");
            var culture = new CultureInfo("nb-NO") { DateTimeFormat = { TimeSeparator = ":" } };

            using (new FakeServiceLocator().WithLanguageContext(culture))
            {
                var page = A.Fake<ProposalPage>();
                page.Deadline = new DateTime(year, month, day, hour, 0, 0, DateTimeKind.Utc);

                A.CallTo(() => page.DeadlineComputed).Returns(page.Deadline);

                var result = ((ReactModels.ProposalPage)
                    _mapper.GetPageViewModel(page).Model).MetadataLeft.Items[1];

                Assert.Equal(expectedDeadlineLabel, result.Label);
                Assert.Equal(expectedDeadline, result.Text);
            }
        }

        [Fact]
        [UseCulture("nb-NO")]
        public void WithDefaultValuesNorwegianCulture_ShouldMapProjectSize()
        {
            const string expectedProjectSizeLabel = "size";
            const string expectedProjectSizeFormat = "NOK {0}-{1}";
            const string nonBreakingSpace = " ";
            var expectedProjectSize = $"NOK 100{nonBreakingSpace}000-1{nonBreakingSpace}000{nonBreakingSpace}000";

            ArrangeTranslation(expectedProjectSizeLabel, "ProjectSize");
            ArrangeTranslation(expectedProjectSizeFormat, "ProjectSizeFormat");

            var page = A.Fake<ProposalPage>();
            page.MinApplicationAmount = 100000;
            page.MaxApplicationAmount = 1000000;

            var result = ((ReactModels.ProposalPage)
                _mapper.GetPageViewModel(page).Model).MetadataRight.Items[0];

            Assert.Equal(expectedProjectSizeLabel, result.Label);
            Assert.Equal(expectedProjectSize, result.Text);
        }

        [Fact]
        [UseCulture("en-US")]
        public void WithDefaultValuesEnglishCulture_ShouldMapProjectSize()
        {
            const string expectedProjectSizeLabel = "size";
            const string expectedProjectSizeFormat = "NOK {0}-{1}";
            const string nonBreakingSpace = " ";
            var expectedProjectSize = $"NOK 100{nonBreakingSpace}000-1{nonBreakingSpace}000{nonBreakingSpace}000";

            ArrangeTranslation(expectedProjectSizeLabel, "ProjectSize");
            ArrangeTranslation(expectedProjectSizeFormat, "ProjectSizeFormat");

            var page = A.Fake<ProposalPage>();
            page.MinApplicationAmount = 100000;
            page.MaxApplicationAmount = 1000000;

            var result = ((ReactModels.ProposalPage)
                _mapper.GetPageViewModel(page).Model).MetadataRight.Items[0];

            Assert.Equal(expectedProjectSizeLabel, result.Label);
            Assert.Equal(expectedProjectSize, result.Text);
        }

        [Fact]
        [UseCulture("nb-NO")]
        public void WithDefaultValues_ShouldMapDuration()
        {
            const string expectedDurationLabel = "duration";
            const string expectedDuration = "10-20 måneder";
            const string expectedDurationFormat = "{0}-{1} måneder";

            ArrangeTranslation(expectedDurationLabel, "Duration");
            ArrangeTranslation(expectedDurationFormat, "DurationFormat");

            var page = A.Fake<ProposalPage>();
            page.MinProjectLength = 10;
            page.MaxProjectLength = 20;

            var result = ((ReactModels.ProposalPage)
                _mapper.GetPageViewModel(page).Model).MetadataRight.Items[0];

            Assert.Equal(expectedDurationLabel, result.Label);
            Assert.Equal(expectedDuration, result.Text);
        }

        [Fact]
        public void WithDefaultValues_ShouldMapAmount()
        {
            const string expectedAmountLabel = "amount";
            const string expectedNokFormat = "nok {0}";
            const string expectedAmountText = "nok 1000000";

            ArrangeTranslation(expectedAmountLabel, "Amount");
            ArrangeTranslation(expectedNokFormat, "NokFormat");

            var page = A.Fake<ProposalPage>();
            page.ProposalAmount = "1000000";

            var result = ((ReactModels.ProposalPage)
                _mapper.GetPageViewModel(page).Model).MetadataRight.Items[0];

            Assert.Equal(expectedAmountLabel, result.Label);
            Assert.Equal(expectedAmountText, result.Text);
        }

        [Fact]
        public void WithProposalAmountText_ShouldMapAmountWithText()
        {
            const string expectedNokFormat = "nok {0}";
            const string expectedAmountText = "nok 1000000. text";

            ArrangeTranslation(expectedNokFormat, "NokFormat");

            var page = A.Fake<ProposalPage>();
            page.ProposalAmount = "1000000";
            page.ProposalAmountText = "text";

            var result = ((ReactModels.ProposalPage)
                _mapper.GetPageViewModel(page).Model).MetadataRight.Items[0];

            Assert.Equal(expectedAmountText, result.Text);
        }

        [Fact]
        public void WithProposalAmountUrlText_ShouldMapAmountWithUrl()
        {
            const string expectedUrl = "url";
            ArrangeGetUrl(expectedUrl);

            var frontPage = A.Fake<FrontPage>();
            frontPage.ProposalsPage = PageReference.EmptyReference;
            A.CallTo(() => _pageRepository.GetCurrentStartPage()).Returns(frontPage);

            var page = A.Fake<ProposalPage>();
            page.ProposalAmount = "1000000";
            page.ProposalAmountUrlText = "text";
            page.ApplicationTypeReference = PageReference.EmptyReference;

            var result = ((ReactModels.ProposalPage)
                _mapper.GetPageViewModel(page).Model).MetadataRight.Items[0].Links[0];

            Assert.Equal(page.ProposalAmountUrlText, result.Text);
            Assert.Equal($"{expectedUrl}?applicationTypes=0", result.Url);
        }

        [Fact]
        public void WithMultipleCategories_ShouldMapSubjectsList()
        {
            const string expectedSubjectLabel = "subjects";
            const string expectedUrlCat1 = "#desc1";
            const string expectedUrlCat2 = "#desc2";
            const string expectedDescription1 = "desc1";
            const string expectedDescription2 = "desc2";
            var page = A.Fake<ProposalPage>();

            ArrangeTranslation(expectedSubjectLabel, "Subjects");

            A.CallTo(() => _proposalService.GetSortedSubjects(page))
                .Returns(new List<SubjectBlock> {
                    new SubjectBlock { SubjectName = "desc1", SubjectCode = "desc1" },
                    new SubjectBlock { SubjectName = "desc2", SubjectCode = "desc2" } });

            page.Category = new CategoryList(new List<int> { 1, 2 });

            var result = ((ReactModels.ProposalPage)
                _mapper.GetPageViewModel(page).Model).MetadataLeft.Items[2];

            Assert.Equal(expectedSubjectLabel, result.Label);
            Assert.Equal(expectedDescription1, result.Links[0].Text);
            Assert.Equal(expectedUrlCat1, result.Links[0].Url);
            Assert.Equal(expectedDescription2, result.Links[1].Text);
            Assert.Equal(expectedUrlCat2, result.Links[1].Url);
        }

        [Fact]
        public void WithDefaultValues_ShouldMapTargetGroups()
        {
            const string expectedTargetGroupLabel = "target group";
            const string expectedCategoryName = "cat1";
            var parentCategory = new Category(CategoryConstants.TargetGroupRoot, null);
            var category = new Category(expectedCategoryName, expectedCategoryName) { Parent = parentCategory };

            ArrangeTranslation(expectedTargetGroupLabel, "TargetGroups");

            A.CallTo(() => _categoryRepository.Get(1))
                .Returns(category);
            A.CallTo(() => _categoryLocalizationProvider.GetCategoryName(A<int>._))
                .Returns(expectedCategoryName);

            var page = A.Fake<ProposalPage>();
            page.Category = new CategoryList(new List<int> { 1 });

            var result = ((ReactModels.ProposalPage)
                _mapper.GetPageViewModel(page).Model).MetadataLeft.Items[2];

            Assert.Equal(expectedTargetGroupLabel, result.Label);
            Assert.Equal(expectedCategoryName, result.Text);
        }

        [Fact]
        public void WithMultipleCategories_ShouldMapTargetGroupsAsCsv()
        {
            const string expectedTargetGroupLabel = "target groups";
            const string expectedCategoryName = "cat1, cat2";
            var parentCategory = new Category(CategoryConstants.TargetGroupRoot, null);
            var category1 = new Category("cat1", "cat1") { Parent = parentCategory, ID = 1 };
            var category2 = new Category("cat2", "cat2") { Parent = parentCategory, ID = 2 };
            ArrangeTranslation(expectedTargetGroupLabel, "TargetGroups");

            A.CallTo(() => _categoryRepository.Get(1)).Returns(category1);
            A.CallTo(() => _categoryRepository.Get(2)).Returns(category2);
            A.CallTo(() => _categoryLocalizationProvider.GetCategoryName(1))
                .Returns("cat1");
            A.CallTo(() => _categoryLocalizationProvider.GetCategoryName(2))
                .Returns("cat2");

            var page = A.Fake<ProposalPage>();
            page.Category = new CategoryList(new List<int> { 1, 2 });

            var result = ((ReactModels.ProposalPage)
                _mapper.GetPageViewModel(page).Model).MetadataLeft.Items[2];

            Assert.Equal(expectedTargetGroupLabel, result.Label);
            Assert.Equal(expectedCategoryName, result.Text);
        }

        [Fact]
        [UseCulture("nb-NO")]
        public void WhenPageHasUpcomingProposal_ShouldMapUpcomingProposalLink()
        {
            const string expectedUpcomingProposalLabel = "upcoming";

            const string expectedUrl = "url";
            var upcomingProposal = A.Fake<ProposalPage>();
            upcomingProposal.Deadline = new DateTime(2018, 12, 19);
            string expectedTimestamp = upcomingProposal.Deadline.Value.ToDisplayDate().ToString("dd. MMMM yyyy");

            ArrangeTranslation(expectedUpcomingProposalLabel, "UpcomingProposal");

            A.CallTo(() => upcomingProposal.DeadlineComputed).Returns(upcomingProposal.Deadline);
            A.CallTo(() => _contentRepository.Get<ProposalPage>(A<ContentReference>._))
                .Returns(upcomingProposal);

            ArrangeGetUrl(expectedUrl);

            var page = A.Fake<ProposalPage>();
            page.UpcomingProposalReference = PageReference.EmptyReference;

            var result = ((ReactModels.ProposalPage)
                _mapper.GetPageViewModel(page).Model).MetadataRight.Items[0];

            Assert.Equal(expectedUpcomingProposalLabel, result.Label);
            Assert.Null(result.Text);
            Assert.Equal(expectedTimestamp, result.Links[0].Text);
            Assert.Equal(expectedUrl, result.Links[0].Url);
        }

        [Fact]
        public void WithEmptyValues_ShouldMapRequiredMetadata()
        {
            var page = A.Fake<ProposalPage>();
            page.UpcomingProposalReference = null;
            page.ProposalAmount = null;
            page.MinApplicationAmount = 0;
            page.MaxApplicationAmount = 0;
            page.MinProjectLength = 0;
            page.MaxProjectLength = 0;

            var result = ((ReactModels.ProposalPage)
                _mapper.GetPageViewModel(page).Model);

            Assert.Equal(3, result.MetadataLeft.Items.Count);
            Assert.Equal(0, result.MetadataRight.Items.Count);
        }

        [Fact]
        public void WithDefaultValues_ShouldMapTimeline()
        {
            var expectedResult = new ReactPartialViewModel(new TimeLineBlock().ReactComponentName(), new ReactModels.TimelineBlock());

            A.CallTo(() => _viewModelFactory.GetPartialViewModel(A<IContentData>._))
                .Returns(expectedResult);

            var timeline = new TimeLineBlock { TimeLineItems = new List<TimeLineItem> { new TimeLineItem() } };
            var page = A.Fake<ProposalPage>();
            page.Timeline = timeline;

            var result = ((ReactModels.ProposalPage)
                _mapper.GetPageViewModel(page).Model).Timeline;

            Assert.Equal(expectedResult.Model, result);
        }

        [Fact]
        public void WithEmptyTimeline_ShouldNotMapTimeline()
        {
            var emptyTimeline = new ReactPartialViewModel(
                new TimeLineBlock().ReactComponentName(),
                new ReactModels.TimelineBlock());

            A.CallTo(() => _viewModelFactory.GetPartialViewModel(A<IContentData>._))
                .Returns(emptyTimeline);

            var result = ((ReactModels.ProposalPage)
                _mapper.GetPageViewModel(A.Fake<ProposalPage>()).Model).Timeline;

            Assert.Null(result);
        }

        [Fact]
        public void WithDefaultValues_ShouldMapContentArea()
        {
            var expectedMoreContent = new ReactModels.ContentArea();

            var page = A.Fake<ProposalPage>();
            A.CallTo(() => _contentAreaReactModelBuilder.BuildReactModel(A<ContentArea>._, nameof(page.Content), A<RenderOptionQueryParameter>._))
                .Returns(expectedMoreContent);

            var result = ((ReactModels.ProposalPage)_mapper.GetPageViewModel(page).Model).Tabs.Items[0].Content.ContentSections[2];

            Assert.Same(expectedMoreContent, result.MoreContent);
        }

        [Fact]
        public void WithDefaultValues_ShouldMapProposalTab()
        {
            const string expectedTabTitle = "tab";

            ArrangeTranslation(expectedTabTitle, "ProposalTabLabel");

            var result = ((ReactModels.ProposalPage)
                _mapper.GetPageViewModel(A.Fake<ProposalPage>()).Model).Tabs.Items[0];

            Assert.Equal(expectedTabTitle, result.Name);
            Assert.NotNull(result.Guid);
        }

        [Fact]
        public void WithDefaultValues_UsingOldDesign_ShouldMapTexts()
        {
            A.CallTo(() => _pageEditingAdapter.PageIsInEditMode()).Returns(true);

            const string expectedAboutTitle = "about";
            const string expectedIndendedApplicantsTitle = "applicants";
            const string expectedProjectParticipantsTitle = "participants";
            const string expectedSubjectTitle = "subject";
            const string expectedDesignRequirementsTitle = "design requirements";
            const string expectedRelatedSubjectTitle = "related";
            const string expectedAssessmentCriteriaTitle = "assessment";
            const string expectedEvaluationTitle = "evaluation";

            var expectedAboutTextComponent = new ReactModels.RichText();
            var expectedIntendedApplicantsTextComponent = new ReactModels.RichText();
            var expectedProjectParticipantsTextComponent = new ReactModels.RichText();
            var expectedSubjectTextComponent = new ReactModels.RichText();
            var expectedDesignRequirementsTextComponent = new ReactModels.RichText();
            var expectedRelatedSubjectTextComponent = new ReactModels.RichText();
            var expectedAssessmentCriteriaTextComponent = new ReactModels.RichText();
            var expectedGeneralAssessmentCriteriaComponent = new ReactModels.RichText();
            var expectedEvaluationTextComponent = new ReactModels.RichText();

            var page = A.Fake<ProposalPage>();
            page.DesignRequirements = new XhtmlString("design");
            page.AssessmentCriteriaText = new XhtmlString("assessment");
            page.GeneralAssessmentCriteria = new XhtmlString("general");
            page.Evaluation = new XhtmlString("evaluation");

            ArrangeRichTextBuilder(expectedAboutTextComponent, nameof(ProposalPage.About));
            ArrangeRichTextBuilder(expectedIntendedApplicantsTextComponent, nameof(ProposalPage.IntendedApplicants));
            ArrangeRichTextBuilder(expectedProjectParticipantsTextComponent, nameof(ProposalPage.ProjectParticipants));
            ArrangeRichTextBuilder(expectedSubjectTextComponent, nameof(ProposalPage.Subject));
            ArrangeRichTextBuilder(expectedDesignRequirementsTextComponent, nameof(ProposalPage.DesignRequirements));
            ArrangeRichTextBuilder(expectedRelatedSubjectTextComponent, nameof(ProposalPage.RelevantSubjectsText));
            ArrangeRichTextBuilder(expectedAssessmentCriteriaTextComponent, nameof(ProposalPage.AssessmentCriteriaText));
            ArrangeRichTextBuilder(expectedGeneralAssessmentCriteriaComponent, nameof(ProposalPage.GeneralAssessmentCriteria));
            ArrangeRichTextBuilder(expectedEvaluationTextComponent, nameof(ProposalPage.Evaluation));

            ArrangeTranslation(expectedAboutTitle, "AboutTitle");
            ArrangeTranslation(expectedIndendedApplicantsTitle, "IntendedApplicantsTitle");
            ArrangeTranslation(expectedProjectParticipantsTitle, "ProjectParticipantsTitle");
            ArrangeTranslation(expectedSubjectTitle, "SubjectTitle");
            ArrangeTranslation(expectedDesignRequirementsTitle, "DesignRequirementsTitle");
            ArrangeTranslation(expectedRelatedSubjectTitle, "RelevantSubjectsTitle");
            ArrangeTranslation(expectedAssessmentCriteriaTitle, "AssessmentCriteriaTitle");
            ArrangeTranslation(expectedEvaluationTitle, "EvaluationTitle");

            var result = ((ReactModels.ProposalPage)
                _mapper.GetPageViewModel(page).Model).Tabs.Items[0].Content;

            var section1 = result.ContentSections[0];
            var section2 = result.ContentSections[1];
            var section3 = result.ContentSections[2];

            Assert.Equal(3, section1.Texts.Count);

            Assert.Same(expectedAboutTextComponent, section1.Ingress);
            Assert.Same(expectedIntendedApplicantsTextComponent, section1.Texts[0].Text);
            Assert.Same(expectedProjectParticipantsTextComponent, section1.Texts[1].Text);
            Assert.Same(expectedSubjectTextComponent, section1.Texts[2].Text);
            Assert.Same(expectedRelatedSubjectTextComponent, section2.ContentDescription.Text);
            Assert.Same(expectedDesignRequirementsTextComponent, section3.MoreTexts[0].Text);
            Assert.Same(expectedAssessmentCriteriaTextComponent, section3.MoreTexts[1].Text);
            Assert.Same(expectedGeneralAssessmentCriteriaComponent, section3.MoreTexts[2].Text);
            Assert.Same(expectedEvaluationTextComponent, section3.MoreTexts[3].Text);

            Assert.Equal(expectedAboutTitle, section1.Title);
            Assert.Equal(expectedIndendedApplicantsTitle, section1.Texts[0].Title);
            Assert.Equal(expectedProjectParticipantsTitle, section1.Texts[1].Title);
            Assert.Equal(expectedSubjectTitle, section1.Texts[2].Title);
            Assert.Equal(expectedRelatedSubjectTitle, section2.ContentDescription.Title);
            Assert.Equal(expectedDesignRequirementsTitle, section3.MoreTexts[0].Title);
            Assert.Equal(expectedAssessmentCriteriaTitle, section3.MoreTexts[1].Title);
            Assert.Equal(expectedEvaluationTitle, section3.MoreTexts[3].Title);
        }

        [Fact]
        public void WithDefaultValues_ShouldMapTexts()
        {
            A.CallTo(() => _pageEditingAdapter.PageIsInEditMode()).Returns(true);

            const string expectedAboutTitle = "about";
            const string expectedIndendedApplicantsTitle = "applicants";
            const string expectedProjectParticipantsTitle = "participants";
            const string expectedSubjectTitle = "subject";
            const string expectedDesignRequirementsTitle = "design requirements";
            const string expectedRelatedSubjectTitle = "related";
            const string expectedAssessmentCriteriaTitle = "assessment";
            const string expectedEvaluationTitle = "evaluation";

            var expectedAboutTextComponent = new ReactModels.RichText();
            var expectedIntendedApplicantsTextComponent = new ReactModels.RichText();
            var expectedProjectParticipantsTextComponent = new ReactModels.RichText();
            var expectedSubjectTextComponent = new ReactModels.RichText();
            var expectedDesignRequirementsTextComponent = new ReactModels.RichText();
            var expectedRelatedSubjectTextComponent = new ReactModels.RichText();
            var expectedAssessmentCriteriaTextComponent = new ReactModels.RichText();
            var expectedGeneralAssessmentCriteriaComponent = new ReactModels.RichText();
            var expectedEvaluationTextComponent = new ReactModels.RichText();

            var page = A.Fake<ProposalPage>();
            page.UseAlternativeSubjectBlockPresentation = true;
            page.DesignRequirements = new XhtmlString("design");
            page.AssessmentCriteriaText = new XhtmlString("assessment");
            page.GeneralAssessmentCriteria = new XhtmlString("general");
            page.Evaluation = new XhtmlString("evaluation");

            ArrangeRichTextBuilder(expectedAboutTextComponent, nameof(ProposalPage.About));
            ArrangeRichTextBuilder(expectedIntendedApplicantsTextComponent, nameof(ProposalPage.IntendedApplicants));
            ArrangeRichTextBuilder(expectedProjectParticipantsTextComponent, nameof(ProposalPage.ProjectParticipants));
            ArrangeRichTextBuilder(expectedSubjectTextComponent, nameof(ProposalPage.Subject));
            ArrangeRichTextBuilder(expectedDesignRequirementsTextComponent, nameof(ProposalPage.DesignRequirements));
            ArrangeRichTextBuilder(expectedRelatedSubjectTextComponent, nameof(ProposalPage.RelevantSubjectsText));
            ArrangeRichTextBuilder(expectedAssessmentCriteriaTextComponent, nameof(ProposalPage.AssessmentCriteriaText));
            ArrangeRichTextBuilder(expectedGeneralAssessmentCriteriaComponent, nameof(ProposalPage.GeneralAssessmentCriteria));
            ArrangeRichTextBuilder(expectedEvaluationTextComponent, nameof(ProposalPage.Evaluation));

            ArrangeTranslation(expectedAboutTitle, "AboutTitle");
            ArrangeTranslation(expectedIndendedApplicantsTitle, "IntendedApplicantsTitle");
            ArrangeTranslation(expectedProjectParticipantsTitle, "ProjectParticipantsTitle");
            ArrangeTranslation(expectedSubjectTitle, "SubjectTitle");
            ArrangeTranslation(expectedDesignRequirementsTitle, "DesignRequirementsTitle");
            ArrangeTranslation(expectedRelatedSubjectTitle, "RelevantSubjectsTitle");
            ArrangeTranslation(expectedAssessmentCriteriaTitle, "AssessmentCriteriaTitle");
            ArrangeTranslation(expectedEvaluationTitle, "EvaluationTitle");

            var result = ((ReactModels.ProposalPage)
                _mapper.GetPageViewModel(page).Model).Tabs.Items[0].Content;

            var section1 = result.ContentSections[0];
            var section2 = result.ContentSections[1];
            var section3 = result.ContentSections[2];

            Assert.Equal(3, section1.Texts.Count);

            Assert.Same(expectedAboutTextComponent, section1.Ingress);
            Assert.Same(expectedIntendedApplicantsTextComponent, section1.Texts[0].Text);
            Assert.Same(expectedProjectParticipantsTextComponent, section1.Texts[1].Text);
            Assert.Same(expectedSubjectTextComponent, section1.Texts[2].Text);
            Assert.Same(expectedRelatedSubjectTextComponent, section2.Ingress);
            Assert.Same(expectedDesignRequirementsTextComponent, section3.MoreTexts[0].Text);
            Assert.Same(expectedAssessmentCriteriaTextComponent, section3.MoreTexts[1].Text);
            Assert.Same(expectedGeneralAssessmentCriteriaComponent, section3.MoreTexts[2].Text);
            Assert.Same(expectedEvaluationTextComponent, section3.MoreTexts[3].Text);

            Assert.Equal(expectedAboutTitle, section1.Title);
            Assert.Equal(expectedIndendedApplicantsTitle, section1.Texts[0].Title);
            Assert.Equal(expectedProjectParticipantsTitle, section1.Texts[1].Title);
            Assert.Equal(expectedSubjectTitle, section1.Texts[2].Title);
            Assert.Equal(expectedRelatedSubjectTitle, section2.Title);
            Assert.Equal(expectedDesignRequirementsTitle, section3.MoreTexts[0].Title);
            Assert.Equal(expectedAssessmentCriteriaTitle, section3.MoreTexts[1].Title);
            Assert.Equal(expectedEvaluationTitle, section3.MoreTexts[3].Title);
        }

        [Fact]
        public void WithProcessBlock_ShouldMapProcess()
        {
            var expectedProcess = new ReactModels.Process();

            var page = A.Fake<ProposalPage>();
            A.CallTo(() => _contentAreaReactModelBuilder.BuildReactModel(A<ContentArea>._, nameof(page.ProcessContentArea), A<RenderOptionQueryParameter>._))
                .Returns(new ReactModels.ContentArea { Blocks = new List<ReactModels.ContentAreaItem> { new ReactModels.ContentAreaItem { ComponentName = nameof(ReactModels.Process), ComponentData = expectedProcess } } });

            var result = ((ReactModels.ProposalPage)_mapper.GetPageViewModel(page).Model).Tabs.Items[0].Content.ContentSections[0];

            Assert.Same(expectedProcess, result.Process);
        }

        [Fact]
        public void WhenArchivingOfDataTextIsNotEmpty_ShouldMapArchivingOfDataText()
        {
            var expectedArchivingOfDataTitle = "title";
            var expectedArchivingOfDataTextComponent = new ReactModels.RichText();
            var page = A.Fake<ProposalPage>();
            page.ArchivingOfDataText = new XhtmlString("something!");

            ArrangeRichTextBuilder(expectedArchivingOfDataTextComponent, nameof(page.ArchivingOfDataText));
            ArrangeTranslation(expectedArchivingOfDataTitle, "ArchivingOfDataTitle");

            var result = ((ReactModels.ProposalPage)
                _mapper.GetPageViewModel(page).Model).Tabs.Items[0].Content.ContentSections[0].Texts[0];

            Assert.Same(expectedArchivingOfDataTextComponent, result.Text);
            Assert.Equal(expectedArchivingOfDataTitle, result.Title);
        }

        [Fact]
        public void WhenAdditionalTextIsNotEmpty_ShouldMapAdditionalText()
        {
            var expectedAdditionalTextComponent = new ReactModels.RichText();
            var page = A.Fake<ProposalPage>();
            page.AdditionalText = new XhtmlString("something!");
            page.AdditionalTextTitle = "title";

            ArrangeRichTextBuilder(expectedAdditionalTextComponent, nameof(page.AdditionalText));

            var result = ((ReactModels.ProposalPage)
                _mapper.GetPageViewModel(page).Model).Tabs.Items[0].Content.ContentSections[0].Texts[0];

            Assert.Same(expectedAdditionalTextComponent, result.Text);
            Assert.Equal(page.AdditionalTextTitle, result.Title);
        }

        [Fact]
        public void WhenRelevantSubjectsTextIsNotEmpty_UsingOldDesign_ShouldMapRelevantSubjectsText()
        {
            const string expectedSubjectTitle = "subject";
            ArrangeTranslation(expectedSubjectTitle, "RelevantSubjectsTitle");

            var expectedRelevantSubjectsTextComponent = new ReactModels.RichText();
            var page = A.Fake<ProposalPage>();
            page.RelevantSubjectsText = new XhtmlString("something!");

            ArrangeRichTextBuilder(expectedRelevantSubjectsTextComponent, nameof(page.RelevantSubjectsText));

            var result = ((ReactModels.ProposalPage)
                _mapper.GetPageViewModel(page).Model).Tabs.Items[0].Content.ContentSections[1].ContentDescription;

            Assert.Same(expectedRelevantSubjectsTextComponent, result.Text);
            Assert.Equal(expectedSubjectTitle, result.Title);
        }

        [Fact]
        public void WhenRelevantSubjectsTextIsNotEmpty_ShouldMapRelevantSubjectsText()
        {
            const string expectedSubjectTitle = "subject";
            ArrangeTranslation(expectedSubjectTitle, "RelevantSubjectsTitle");

            var expectedRelevantSubjectsTextComponent = new ReactModels.RichText();
            var page = A.Fake<ProposalPage>();
            page.UseAlternativeSubjectBlockPresentation = true;
            page.RelevantSubjectsText = new XhtmlString("something!");

            ArrangeRichTextBuilder(expectedRelevantSubjectsTextComponent, nameof(page.RelevantSubjectsText));

            var result = ((ReactModels.ProposalPage)
                _mapper.GetPageViewModel(page).Model).Tabs.Items[0].Content.ContentSections[1];

            Assert.Same(expectedRelevantSubjectsTextComponent, result.Ingress);
            Assert.Equal(expectedSubjectTitle, result.Title);
        }

        [Fact]
        public void WhenNoTextFieldsHaveValues_ShouldMapDefaultMenu()
        {
            const string expectedShortcutTitle = "shortcuts";
            const string expectedCollapseLabel = "collapse";
            const string expectedExpandLabel = "expand";
            const string expectedAboutTitle = "about";
            const string expectedAboutUrl = "#AboutNavigationTitle";

            ArrangeTranslation(expectedShortcutTitle, "ShortcutsTitle");
            ArrangeTranslation(expectedCollapseLabel, "CollapseLabel");
            ArrangeTranslation(expectedExpandLabel, "ExpandLabel");
            ArrangeTranslation(expectedAboutTitle, "AboutNavigationTitle");

            var result = ((ReactModels.ProposalPage)
                _mapper.GetPageViewModel(A.Fake<ProposalPage>()).Model).Tabs.Items[0].Content.Menu;

            Assert.Equal(expectedShortcutTitle, result.Title);
            Assert.Equal(expectedCollapseLabel, result.Accordion.CollapseLabel);
            Assert.Equal(expectedExpandLabel, result.Accordion.ExpandLabel);
            Assert.NotNull(result.Accordion.Guid);
            Assert.True(result.Accordion.InitiallyOpen);
            Assert.Equal(1, result.NavGroups.Count);

            var group1 = result.NavGroups[0];
            Assert.Equal(expectedAboutTitle, group1.TitleLink.Text);
            Assert.Equal(expectedAboutUrl, group1.TitleLink.Url);
            Assert.Equal(3, group1.Links.Items.Count);
        }

        [Fact]
        public void WhenTextFieldsHaveValues_ShouldMapFullMenu()
        {
            var page = A.Fake<ProposalPage>();
            var nonEmptyText = new XhtmlString("something!");
            page.ArchivingOfDataText = nonEmptyText;
            page.AdditionalText = nonEmptyText;
            page.DesignRequirements = nonEmptyText;
            page.AssessmentCriteriaText = nonEmptyText;
            page.Evaluation = nonEmptyText;

            var result = ((ReactModels.ProposalPage)
                _mapper.GetPageViewModel(page).Model).Tabs.Items[0].Content.Menu;

            Assert.Equal(5, result.NavGroups[0].Links.Items.Count);
            Assert.Equal(3, result.NavGroups[1].Links.Items.Count);
        }

        [Fact]
        public void WhenPageDoesNotHaveMessage_ShouldNotMapMessage()
        {
            var contentArea = A.Fake<ContentArea>();
            A.CallTo(() => contentArea.Items).Returns(null);

            var page = A.Fake<ProposalPage>();
            page.Messages = contentArea;

            var result = ((ReactModels.ProposalPage)_mapper.GetPageViewModel(page).Model)
                .Message;

            Assert.Null(result);
        }

        [Fact]
        public void WhenPageDoesNotHaveVisibleMessage_ShouldNotMapMessage()
        {
            var contentArea = A.Fake<ContentArea>();
            A.CallTo(() => contentArea.FilteredItems).Returns(new List<ContentAreaItem>());

            var page = A.Fake<ProposalPage>();
            page.Messages = contentArea;

            var result = ((ReactModels.ProposalPage)_mapper.GetPageViewModel(page).Model)
                .Message;

            Assert.Null(result);
        }

        [Fact]
        public void WhenPageHasMessage_ShouldMapMessage()
        {
            var expectedResult = new ReactPartialViewModel(new LocalMessageBlock().ReactComponentName(), new ReactModels.Message());
            var contentArea = A.Fake<ContentArea>();
            A.CallTo(() => contentArea.FilteredItems)
                .Returns(new List<ContentAreaItem>
                {
                    new ContentAreaItem()
                });

            A.CallTo(() => _contentLoader.Get<LocalMessageBlock>(A<ContentReference>._)).Returns(A.Fake<LocalMessageBlock>());
            A.CallTo(() => _viewModelFactory.GetPartialViewModel(A<IContentData>._)).Returns(expectedResult);

            var page = A.Fake<ProposalPage>();
            page.Messages = contentArea;

            var result = ((ReactModels.ProposalPage)_mapper.GetPageViewModel(page).Model)
                .Message;

            Assert.Equal(expectedResult.Model, result);
        }

        [Fact]
        public void WhenPageHasSubjectBlocks_ShouldMapSubjectShortcuts()
        {
            const string expectedHtmlId = "#name";
            var expectedCategory = new Category("name", "desc");

            var contentArea = A.Fake<ContentArea>();
            A.CallTo(() => contentArea.FilteredItems)
                .Returns(new List<ContentAreaItem>
                {
                    new ContentAreaItem()
                });

            A.CallTo(() => _contentLoader.Get<SubjectBlock>(A<ContentReference>._)).Returns(A.Fake<SubjectBlock>());
            A.CallTo(() => _categoryRepository.Get(A<int>._)).Returns(expectedCategory);
            A.CallTo(() => _categoryLocalizationProvider.GetCategoryName(A<int>._)).Returns(expectedCategory.Description);

            var page = A.Fake<ProposalPage>();
            page.SubjectBlocks = contentArea;

            var result = ((ReactModels.ProposalPage)
                _mapper.GetPageViewModel(page).Model).Tabs.Items[0].Content.Menu.NavGroups[1].Links;

            Assert.Single(result.Items);
            Assert.Equal(expectedCategory.Description, result.Items[0].Link.Text);
            Assert.Equal(expectedHtmlId, result.Items[0].Link.Url);
        }

        [Fact]
        public void WhenPageDoesNotHaveTableauContent_ShouldNotMapApplicationProcesssing()
        {
            var page = A.Fake<ProposalPage>();
            page.ApplicationResult = null;

            var result = (ReactModels.ProposalPage)_mapper.GetPageViewModel(page).Model;

            Assert.Single(result.Tabs.Items);
        }

        [Fact]
        public void WhenPageHasTableauContent_ShouldMapApplicationProcessing()
        {
            var tableauContent = A.Fake<ContentArea>();
            A.CallTo(() => tableauContent.FilteredItems)
                .Returns(new List<ContentAreaItem> { new ContentAreaItem() });

            var page = A.Fake<ProposalPage>();
            page.TableauContent = tableauContent;

            const string expectedApplicationProcessingTabLabel = "processing";
            const string expectedApplicationProcessingTitle = "processing title";
            ArrangeTranslation(expectedApplicationProcessingTabLabel, "ApplicationProcessingTabLabel");
            ArrangeTranslation(expectedApplicationProcessingTitle, "AboutApplicationProcessingTitle");

            var expectedRichTextComponent = new ReactModels.RichText();
            A.CallTo(() => _richTextReactModelBuilder.BuildReactModel(A<XhtmlString>._, A<string>._))
                .Returns(expectedRichTextComponent);

            var expectedTableauContent = new ReactModels.ContentArea();
            A.CallTo(() => _contentAreaReactModelBuilder.BuildReactModel(A<ContentArea>._, nameof(page.TableauContent), A<RenderOptionQueryParameter>._))
                .Returns(expectedTableauContent);

            var result = ((ReactModels.ProposalPage)_mapper.GetPageViewModel(page).Model).Tabs.Items[1];

            Assert.Equal(expectedApplicationProcessingTabLabel, result.Name);
            Assert.Equal(expectedApplicationProcessingTitle, result.Content.ContentSections[0].Title);
            Assert.Equal(expectedRichTextComponent, result.Content.ContentSections[0].Ingress);
            Assert.Equal(expectedTableauContent, result.Content.ContentSections[0].Content);
        }

        [Fact]
        public void WhenApplicationResultIsNotSet_ShouldNotMapApplicationResult()
        {
            var page = A.Fake<ProposalPage>();
            page.ApplicationResult = null;

            var result = (ReactModels.ProposalPage)_mapper.GetPageViewModel(page).Model;

            Assert.Single(result.Tabs.Items);
        }

        [Fact]
        public void WhenApplicationResultIsSet_ShouldMapApplicationResult()
        {
            var page = A.Fake<ProposalPage>();
            page.ApplicationResult = PageReference.EmptyReference;

            const string expectedApplicationResultsTabLabel = "results";
            const string expectedApplicationResultsTitle = "results title";
            ArrangeTranslation(expectedApplicationResultsTabLabel, "ApplicationResultsTabLabel");
            ArrangeTranslation(expectedApplicationResultsTitle, "AboutApplicationResultsTitle");

            var expectedRichTextComponent = new ReactModels.RichText();
            A.CallTo(() => _richTextReactModelBuilder.BuildReactModel(A<XhtmlString>._, A<string>._))
                .Returns(expectedRichTextComponent);

            var resultsPage = A.Fake<ApplicationResultPage>();
            A.CallTo(() => _contentLoader.Get<ApplicationResultPage>(page.ApplicationResult))
                .Returns(resultsPage);

            var expectedResultsViewModel = new ReactPartialViewModel(
                resultsPage.ReactComponentName(),
                new ReactModels.DescriptionListAndTables());
            A.CallTo(() => _viewModelFactory.GetPartialViewModel(resultsPage))
                .Returns(expectedResultsViewModel);

            var result = ((ReactModels.ProposalPage)_mapper.GetPageViewModel(page).Model).Tabs.Items[1];

            Assert.Equal(expectedApplicationResultsTabLabel, result.Name);

            var content = result.Content.ContentSections[0];
            Assert.Equal(expectedApplicationResultsTitle, content.Title);
            Assert.Equal(expectedRichTextComponent, content.Ingress);
            Assert.Equal(expectedResultsViewModel.ReactComponentName, content.Content.Blocks[0].ComponentName);
            Assert.Equal(expectedResultsViewModel.Model, content.Content.Blocks[0].ComponentData);
        }

        [Fact]
        [UseCulture("nb-NO")]
        public void WhenApplicationResultIsSetAndDeadlineIsPassed_ShouldSetLastTabAsActive()
        {
            var page = A.Fake<ProposalPage>();
            page.ApplicationResult = new PageReference(1);
            page.DeadlineOverride = new DateTime(2018, 2, 3);

            A.CallTo(() => page.DeadlineComputed).Returns(page.DeadlineOverride);
            A.CallTo(() => _richTextReactModelBuilder.BuildReactModel(A<XhtmlString>._, A<string>._))
                .Returns(new ReactModels.RichText());

            var resultsPage = A.Fake<ApplicationResultPage>();
            A.CallTo(() => _contentLoader.Get<ApplicationResultPage>(page.ApplicationResult))
                .Returns(resultsPage);

            A.CallTo(() => _viewModelFactory.GetPartialViewModel(resultsPage))
                .Returns(new ReactPartialViewModel(
                    resultsPage.ReactComponentName(),
                    new ReactModels.DescriptionListAndTables()));

            var result = ((ReactModels.ProposalPage)_mapper.GetPageViewModel(page).Model).Tabs;

            Assert.Equal(result.Items.Last().Guid, result.ActiveTab);
        }

        private void ArrangeGetUrl(string expectedUrl) =>
            A.CallTo(() => _urlResolver.GetUrl(A<ContentReference>._, A<string>._, A<UrlResolverArguments>._))
                .Returns(expectedUrl);

        private void ArrangeTranslation(string expected, string label) =>
            A.CallTo(() => _localizationProvider.GetLabel(nameof(ProposalPage), label))
                .Returns(expected);

        private void ArrangeRichTextBuilder(ReactModels.RichText expectedTextComponent, string onPageEditName) =>
            A.CallTo(() => _richTextReactModelBuilder.BuildReactModel(A<XhtmlString>._, onPageEditName))
                .Returns(expectedTextComponent);
    }
}