using System.Collections.Generic;
using System.Linq;
using FakeItEasy;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Builders;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.QueryParameters;
using Forskningsradet.Core.Models.RequestModels;
using Forskningsradet.Core.Models.SearchModels;
using Forskningsradet.Core.Services;
using Forskningsradet.Core.Services.Contracts;
using Forskningsradet.Core.ViewModelMappers.Pages;
using Xunit;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.UnitTests.Core.ViewModelMappers.Pages
{
    public class EventListPageViewModelMapperTests
    {
        private readonly IEventListService _eventListService;
        private readonly FilterLayoutReactModelBuilder _filterLayoutReactModelBuilder;
        private readonly IDateCardReactModelBuilder _dateCardReactModelBuilder;
        private readonly ILocalizationProvider _localizationProvider;
        private readonly EventListPageViewModelMapper _mapper;
        private readonly ISearchApiUrlResolver _searchApiUrlResolver;

        public EventListPageViewModelMapperTests()
        {
            _eventListService = A.Fake<IEventListService>();
            _dateCardReactModelBuilder = A.Fake<IDateCardReactModelBuilder>();
            _localizationProvider = A.Fake<ILocalizationProvider>();
            _searchApiUrlResolver = A.Fake<ISearchApiUrlResolver>();
            _filterLayoutReactModelBuilder = new FilterLayoutReactModelBuilder(new FilterGroupReactModelBuilder(), A.Fake<ICategoryLocalizationProvider>(), _localizationProvider);

            _mapper = new EventListPageViewModelMapper(
                _eventListService,
                _filterLayoutReactModelBuilder,
                _dateCardReactModelBuilder,
                _localizationProvider,
                _searchApiUrlResolver);
        }

        [Fact]
        public void GetViewModel_WhenAllFieldsProvided_ShouldMapFields()
        {
            const string expectedEmptyListText = "emptylist";
            const string expectedTopFilterTitleLabel = "TopT";
            const string expectedTopFilterOptionAllLabel = "OptionAllLabel";
            const string expectedTopFilterOptionFutureLabel = "OptionFutureLabel";
            const string expectedTopFilterOptionPastLabel = "OptionPastLabel";
            var expectedForm = new ReactModels.Form
            {
                Endpoint = "",
                SubmitButtonText = "submit"
            };

            ArrangeTranslation(expectedEmptyListText, "EmptyEventList");
            ArrangeTranslation(expectedTopFilterTitleLabel, "TopFilterTitle");
            ArrangeTranslation(expectedTopFilterOptionAllLabel, "OptionAll");
            ArrangeTranslation(expectedTopFilterOptionFutureLabel, "OptionFuture");
            ArrangeTranslation(expectedTopFilterOptionPastLabel, "OptionPast");
            ArrangeTranslation(expectedForm.SubmitButtonText, "SubmitButtonText");
            A.CallTo(() => _eventListService.Search(A<EventListRequestModel>._))
                .Returns(
                    new EventFilterSearchResults(
                        new List<EventPage>(),
                        new List<CategoryGroup>(),
                        0));

            var page = A.Fake<EventListPage>();
            page.PageName = "T";
            var query = new EventListQueryParameter { TimeFrame = TimeFrameFilter.Future };

            var result = (ReactModels.EventListPage)_mapper.GetPageViewModel(page, query).Model;

            Assert.Equal(page.PageName, result.Title);
            Assert.Equal(expectedEmptyListText, result.EmptyList.Text);
            Assert.Equal(expectedTopFilterTitleLabel, result.TopFilter.Title);

            Assert.True(result.TopFilter.Options[0].IsCurrent);
            Assert.Equal(expectedTopFilterOptionFutureLabel, result.TopFilter.Options[0].Link.Text);
            Assert.Equal($"?{QueryParameters.TimeFrame}={(int)TimeFrameFilter.Future}", result.TopFilter.Options[0].Link.Url);

            Assert.False(result.TopFilter.Options[1].IsCurrent);
            Assert.Equal(expectedTopFilterOptionPastLabel, result.TopFilter.Options[1].Link.Text);
            Assert.Equal($"?{QueryParameters.TimeFrame}={(int)TimeFrameFilter.Past}", result.TopFilter.Options[1].Link.Url);

            Assert.Equal(expectedForm.Endpoint, result.Form.Endpoint);
            Assert.Equal(expectedForm.SubmitButtonText, result.Form.SubmitButtonText);
        }

        [Fact]
        public void GetViewModel_WhenEventsAreReturned_ShouldMapEvents()
        {
            var dateCard = new ReactModels.DateCard();
            ArrangeEventSearch(new List<EventPage> { new EventPage() });
            A.CallTo(() => _dateCardReactModelBuilder.BuildReactModel(A<EventPage>._, null, true, false, false))
                .Returns(dateCard);

            var result = ((ReactModels.EventListPage)_mapper.GetPageViewModel(new EventListPage(), null).Model);

            Assert.Same(dateCard, result.Events[0]);
        }

        [Fact]
        public void GetViewModel_WhenHideEventTagsEnabled_CallsBuildCardWithSkipTagsOption()
        {
            var dateCard = new ReactModels.DateCard();
            ArrangeEventSearch(new List<EventPage> { new EventPage() });

            var eventListPage = new EventListPage
            {
                HideEventTags = true
            };

            _mapper.GetPageViewModel(eventListPage, null);

            A.CallTo(() => _dateCardReactModelBuilder.BuildReactModel(A<EventPage>._, A<RenderOptionQueryParameter>._, A<bool>._, A<bool>._, true))
                .MustHaveHappenedOnceExactly();
        }

        [Theory]
        [InlineData(TimeFrameFilter.Future)]
        [InlineData(TimeFrameFilter.Past)]
        public void GetViewModel_WhenQueryParameterTimeFrameIsSet_ShouldSetOnlyOneTimeFrameOption(TimeFrameFilter selectedTimeFrame)
        {
            var query = new EventListQueryParameter
            {
                TimeFrame = selectedTimeFrame
            };

            var result = (ReactModels.EventListPage)_mapper.GetPageViewModel(new EventListPage(), query).Model;

            Assert.Equal(1, result.TopFilter.Options.Count(x => x.IsCurrent));
        }

        private void ArrangeEventSearch(List<EventPage> eventList)
        {
            var result = new EventFilterSearchResults(
                eventList,
                new List<CategoryGroup>(),
                eventList.Count);

            A.CallTo(() => _eventListService.Search(A<EventListRequestModel>._))
                .Returns(result);
        }

        private void ArrangeTranslation(string expected, string label) =>
            A.CallTo(() => _localizationProvider.GetLabel(nameof(EventListPage), label))
                .Returns(expected);
    }
}