using System.Collections.Generic;
using EPiServer.Core;
using EPiServer.SpecializedProperties;
using FakeItEasy;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Facades.Contracts;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.QueryParameters;
using Forskningsradet.Core.ViewModelMappers.Pages;
using Xunit;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.UnitTests.Core.ViewModelMappers.Pages
{
    public class PortfolioPageViewModelMapperTests
    {
        private readonly ILinkListReactModelBuilder _linkListReactModelBuilder;
        private readonly IContentAreaReactModelBuilder _contentAreaReactModelBuilder;
        private readonly IPageEditingAdapter _pageEditingAdapter;
        private readonly PortfolioPageViewModelMapper _mapper;

        public PortfolioPageViewModelMapperTests()
        {
            _linkListReactModelBuilder = A.Fake<ILinkListReactModelBuilder>();
            _contentAreaReactModelBuilder = A.Fake<IContentAreaReactModelBuilder>();
            _pageEditingAdapter = A.Fake<IPageEditingAdapter>();
            _mapper = new PortfolioPageViewModelMapper(
                _linkListReactModelBuilder,
                _contentAreaReactModelBuilder,
                _pageEditingAdapter);
        }

        [Fact]
        public void GetViewModel_WhenAllFieldsProvided_ShouldMapFields()
        {
            const string expectedTitle = "Title";
            const string expectedIngress = "ingress";
            var expectedContentArea = new ReactModels.ContentArea();
            var expectedLinks = new List<ReactModels.Link>();

            A.CallTo(() => _contentAreaReactModelBuilder.BuildReactModel(A<ContentArea>._, A<string>._, A<RenderOptionQueryParameter>._))
                .Returns(expectedContentArea);
            A.CallTo(() => _linkListReactModelBuilder.BuildReactModel(A<LinkItemCollection>._, A<string>._))
                .Returns(new ReactModels.LinkList { Items = expectedLinks });

            var page = A.Fake<PortfolioPage>();
            page.PageName = expectedTitle;
            page.MainIntro = expectedIngress;
            page.Title = null;
            page.TitleWithCustomSeparator = null;

            var result = (ReactModels.ContentAreaPage)_mapper.GetPageViewModel(page).Model;

            Assert.Equal(expectedTitle, result.PageHeader.Title);
            Assert.Equal(expectedIngress, result.PageHeader.Ingress);
            Assert.Equal(expectedLinks, result.PageHeader.PortfolioLinks);
            Assert.Equal(expectedContentArea, result.Content);
        }

        [Fact]
        public void GetViewModel_WhenTitleIsSet_ShouldOverwritePageName()
        {
            const string expectedTitle = "Title";

            var page = A.Fake<PortfolioPage>();
            page.PageName = "PageName";
            page.Title = expectedTitle;
            page.TitleWithCustomSeparator = null;

            var result = (ReactModels.ContentAreaPage)_mapper.GetPageViewModel(page).Model;

            Assert.Equal(expectedTitle, result.PageHeader.Title);
        }

        [Fact]
        public void GetViewModel_WhenPageIsInEditMode_ShouldMapOnPageEditing()
        {
            A.CallTo(() => _pageEditingAdapter.PageIsInEditMode()).Returns(true);

            var result = (ReactModels.ContentAreaPage) _mapper.GetPageViewModel(new PortfolioPage()).Model;

            Assert.NotNull(result.PageHeader.OnPageEditing);
        }
    }
}
