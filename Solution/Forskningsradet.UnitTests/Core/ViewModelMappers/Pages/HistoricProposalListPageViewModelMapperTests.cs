using System.Collections.Generic;
using EPiServer.Core;
using EPiServer.Web.Routing;
using FakeItEasy;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.Enums;
using Forskningsradet.Core.Models.QueryParameters;
using Forskningsradet.Core.Models.RequestModels;
using Forskningsradet.Core.Models.SearchModels;
using Forskningsradet.Core.Services;
using Forskningsradet.Core.Services.Contracts;
using Forskningsradet.Core.ViewModelMappers.Pages;
using Xunit;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.UnitTests.Core.ViewModelMappers.Pages
{
    public class HistoricProposalListPageViewModelMapperTests
    {
        private readonly IPublicationListService _publicationListService;
        private readonly IPublicationReactModelBuilder _publicationReactModelBuilder;
        private readonly ILocalizationProvider _localizationProvider;
        private readonly IUrlResolver _urlResolver;
        private readonly HistoricProposalListPageViewModelMapper _mapper;

        public HistoricProposalListPageViewModelMapperTests()
        {
            _publicationListService = A.Fake<IPublicationListService>();
            _publicationReactModelBuilder = A.Fake<IPublicationReactModelBuilder>();
            _localizationProvider = A.Fake<ILocalizationProvider>();
            _urlResolver = A.Fake<IUrlResolver>();
            _mapper = new HistoricProposalListPageViewModelMapper(
                _publicationListService,
                _publicationReactModelBuilder,
                _localizationProvider,
                _urlResolver);
        }

        [Fact]
        public void GetViewModel_WithSearchHits_ShouldOrderByPublishedDescendingWithinYear()
        {
            const string expectedFirstTitle = "A";
            const string expectedSecondTitle = "B";

            var page1 = A.Fake<HistoricProposalPage>();
            page1.PageName = "B";
            page1.StartPublish = new System.DateTime(2002, 4, 5);
            page1.ContentLink = new ContentReference(1);
            page1.Year = 2002;

            var page2 = A.Fake<HistoricProposalPage>();
            page2.PageName = "A";
            page2.StartPublish = new System.DateTime(2002, 12, 13);
            page2.ContentLink = new ContentReference(2);
            page2.Year = 2002;

            ArrangeSearch(new List<HistoricProposalPage> { page1, page2 });
            A.CallTo(() => _publicationReactModelBuilder.BuildReactModel(A<HistoricProposalPage>.That.Matches(x => x.ContentLink.ID == page1.ContentLink.ID), PublicationMetadata.Standard, A<bool>._))
                .Returns(new ReactModels.SearchResult { Title = page1.PageName });
            A.CallTo(() => _publicationReactModelBuilder.BuildReactModel(A<HistoricProposalPage>.That.Matches(x => x.ContentLink.ID == page2.ContentLink.ID), PublicationMetadata.Standard, A<bool>._))
                .Returns(new ReactModels.SearchResult { Title = page2.PageName });

            var result = ((ReactModels.PublicationsPage)_mapper.GetPageViewModel(A.Fake<HistoricProposalListPage>(), A.Fake<PublicationListQueryParameter>())
                .Model)
                .Results[0]
                .Results
                .Blocks;

            Assert.Equal(2, result.Count);
            Assert.Equal(expectedFirstTitle, ((ReactModels.SearchResult)result[0].ComponentData).Title);
            Assert.Equal(expectedSecondTitle, ((ReactModels.SearchResult)result[1].ComponentData).Title);
        }

        private void ArrangeSearch(List<HistoricProposalPage> searchHits)
        {
            var result = new PublicationListSearchResults(
                searchHits,
                new List<FilterOption>(),
                new List<FilterOption>(),
                searchHits.Count);

            A.CallTo(() => _publicationListService.Search(A<HistoricProposalRequest>._))
                .Returns(result);
        }
    }
}