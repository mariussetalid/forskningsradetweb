using System;
using EPiServer.Core;
using EPiServer.Web.Routing;
using FakeItEasy;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Extensions;
using Forskningsradet.Core.Facades.Contracts;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.QueryParameters;
using Forskningsradet.Core.Services;
using Forskningsradet.Core.ViewModelMappers.Pages;
using Xunit;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.UnitTests.Core.ViewModelMappers.Pages
{
    public class InformationArticlePageViewModelMapperTests
    {
        private readonly IContentAreaReactModelBuilder _contentAreaReactModelBuilder;
        private readonly IRichTextReactModelBuilder _richTextReactModelBuilder;
        private readonly ILocalizationProvider _localizationProvider;
        private readonly IPageEditingAdapter _pageEditingAdapter;
        private readonly ISocialMediaLinkListReactModelBuilder _socialMediaLinkListReactModelBuilder;
        private readonly ITagLinkListReactModelBuilder _tagLinkListReactModelBuilder;
        private readonly IUrlResolver _urlResolver;
        private readonly IContentListService _contentListService;
        private readonly IOptionsModalReactModelBuilder _optionsModalReactModelBuilder;
        private readonly InformationArticlePageViewModelMapper _mapper;

        public InformationArticlePageViewModelMapperTests()
        {
            _contentAreaReactModelBuilder = A.Fake<IContentAreaReactModelBuilder>();
            _richTextReactModelBuilder = A.Fake<IRichTextReactModelBuilder>();
            _localizationProvider = A.Fake<ILocalizationProvider>();
            _pageEditingAdapter = A.Fake<IPageEditingAdapter>();
            _socialMediaLinkListReactModelBuilder = A.Fake<ISocialMediaLinkListReactModelBuilder>();
            _tagLinkListReactModelBuilder = A.Fake<ITagLinkListReactModelBuilder>();
            _urlResolver = A.Fake<IUrlResolver>();
            _contentListService = A.Fake<IContentListService>();
            _optionsModalReactModelBuilder = A.Fake<IOptionsModalReactModelBuilder>();
            _mapper = new InformationArticlePageViewModelMapper(
                _contentAreaReactModelBuilder,
                _richTextReactModelBuilder,
                _localizationProvider,
                _pageEditingAdapter,
                _socialMediaLinkListReactModelBuilder,
                _tagLinkListReactModelBuilder,
                _urlResolver,
                _contentListService,
                _optionsModalReactModelBuilder);
        }

        [Fact]
        public void GetViewModel_WhenAllFieldsProvided_ShouldMapFields()
        {
            const string expectedTitle = "Title";
            const string expectedIngress = "ingress";
            const string expectedAuthor = "a";
            const string expectedTranslator = "t";
            var expectedContent = new ReactModels.RichText();
            var expectedContentArea = new ReactModels.ContentArea();

            var articlePage = A.Fake<InformationArticlePage>();
            articlePage.PageName = expectedTitle;
            articlePage.MainIntro = expectedIngress;
            articlePage.Author = expectedAuthor;
            articlePage.TranslatedBy = expectedTranslator;

            EPiServer.Globalization.ContentLanguage.PreferredCulture = new System.Globalization.CultureInfo("sv-SE");
            A.CallTo(() => _richTextReactModelBuilder.BuildReactModel(A<XhtmlString>._, A<string>._))
                .Returns(expectedContent);
            A.CallTo(() => _contentAreaReactModelBuilder.BuildReactModel(A<ContentArea>._, A<string>._, A<RenderOptionQueryParameter>._))
                .Returns(expectedContentArea);
            A.CallTo(() => _optionsModalReactModelBuilder.BuildShareContent(articlePage))
                .Returns(new ReactModels.OptionsModal());

            var result = (ReactModels.ArticlePage)_mapper.GetPageViewModel(articlePage).Model;

            Assert.Equal(expectedTitle, result.Title);
            Assert.Equal(expectedIngress, result.Ingress);
            Assert.Equal(expectedContent, result.Content);
            Assert.Equal(expectedContentArea, result.Sidebar);
        }

        [Fact]
        public void GetViewModel_WhenPublishedSameAsChanged_ShouldDisplayPublishedInByline()
        {
            const string expectedLabel = "Label";
            var expectedDate = new DateTime(2012, 2, 13, 0, 0, 0);
            string expectedText = $"{expectedLabel} {expectedDate.ToDisplayDate():dd.MM.yyyy}";

            EPiServer.Globalization.ContentLanguage.PreferredCulture = new System.Globalization.CultureInfo("sv-SE");
            A.CallTo(() => _localizationProvider.GetLabel(A<string>._, A<string>._))
                .Returns(expectedLabel);

            var articlePage = A.Fake<InformationArticlePage>();
            articlePage.StartPublish = expectedDate;
            articlePage.Changed = expectedDate;

            var result = (ReactModels.ArticlePage)_mapper.GetPageViewModel(articlePage).Model;

            Assert.Equal(expectedText, result.Footer.Byline.Items[0].Text);
        }

        [Fact]
        public void GetViewModel_WhenPublishedDifferentDayThanChanged_ShouldDisplayPublishedAndChangedInByline()
        {
            const string expectedLabel = "Label";
            var expectedDate = new DateTime(2012, 2, 13, 0, 0, 0);

            EPiServer.Globalization.ContentLanguage.PreferredCulture = new System.Globalization.CultureInfo("sv-SE");
            A.CallTo(() => _localizationProvider.GetLabel(A<string>._, A<string>._))
                .Returns(expectedLabel);

            var articlePage = A.Fake<InformationArticlePage>();
            articlePage.StartPublish = expectedDate;
            articlePage.Changed = expectedDate.AddDays(1);
            string expectedPublishedText = $"{expectedLabel} {articlePage.StartPublish.Value.ToDisplayDate():dd.MM.yyyy}";
            string expectedChangedText = $"{expectedLabel} {articlePage.Changed.ToDisplayDate():dd.MM.yyyy}";

            var result = (ReactModels.ArticlePage)_mapper.GetPageViewModel(articlePage).Model;

            Assert.Equal(expectedPublishedText, result.Footer.Byline.Items[0].Text);
            Assert.Equal(expectedChangedText, result.Footer.Byline.Items[1].Text);
        }

        [Fact]
        public void GetViewModel_WhenPageIsInEditMode_ShouldMapOnPageEditing()
        {
            EPiServer.Globalization.ContentLanguage.PreferredCulture = new System.Globalization.CultureInfo("sv-SE");
            A.CallTo(() => _pageEditingAdapter.PageIsInEditMode()).Returns(true);

            var result = (ReactModels.ArticlePage)_mapper.GetPageViewModel(new InformationArticlePage()).Model;

            Assert.NotNull(result.OnPageEditing);
            Assert.NotNull(result.Footer.Byline.Items[0].OnPageEditing);
            Assert.NotNull(result.Footer.Byline.Items[1].OnPageEditing);
        }
    }
}