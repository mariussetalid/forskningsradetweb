using System.Collections.Generic;
using System.Linq;
using EPiServer;
using EPiServer.Core;
using EPiServer.Web.Routing;
using FakeItEasy;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Configuration;
using Forskningsradet.Core.Facades.Contracts;
using Forskningsradet.Core.Models.ContentModels.Media;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.ContentModels.PropertyList;
using Forskningsradet.Core.Models.QueryParameters;
using Forskningsradet.Core.Services;
using Forskningsradet.Core.Services.Contracts;
using Forskningsradet.Core.ViewModelMappers.Pages;
using Xunit;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.UnitTests.Core.ViewModelMappers.Pages
{
    public class ContentAreaPageViewModelMapperTests
    {
        private readonly IContentAreaReactModelBuilder _contentAreaReactModelBuilder;
        private readonly IFluidImageReactModelBuilder _fluidImageReactModelBuilder;
        private readonly IContentLoader _contentLoader;
        private readonly IContentListService _contentListService;
        private readonly IPageEditingAdapter _pageEditingAdapter;
        private readonly IGoogleMapsConfiguration _googleMapsConfiguration;
        private readonly ITagLinkListReactModelBuilder _tagLinkListReactModelBuilder;
        private readonly IRichTextReactModelBuilder _richTextReactModelBuilder;
        private readonly ILocalizationProvider _localizationProvider;
        private readonly IUrlResolver _urlResolver;
        private readonly IUrlCheckingService _urlCheckingService;
        private readonly ContentAreaPageViewModelMapper _mapper;

        public ContentAreaPageViewModelMapperTests()
        {
            _contentAreaReactModelBuilder = A.Fake<IContentAreaReactModelBuilder>();
            _fluidImageReactModelBuilder = A.Fake<IFluidImageReactModelBuilder>();
            _contentLoader = A.Fake<IContentLoader>();
            _contentListService = A.Fake<IContentListService>();
            _pageEditingAdapter = A.Fake<IPageEditingAdapter>();
            _googleMapsConfiguration = A.Fake<IGoogleMapsConfiguration>();
            _tagLinkListReactModelBuilder = A.Fake<ITagLinkListReactModelBuilder>();
            _richTextReactModelBuilder = A.Fake<IRichTextReactModelBuilder>();
            _localizationProvider = A.Fake<ILocalizationProvider>();
            _urlResolver = A.Fake<IUrlResolver>();
            _urlCheckingService = A.Fake<IUrlCheckingService>();
            _mapper = new ContentAreaPageViewModelMapper(
                _contentAreaReactModelBuilder,
                _fluidImageReactModelBuilder,
                _contentLoader,
                _contentListService,
                _pageEditingAdapter,
                _googleMapsConfiguration,
                _tagLinkListReactModelBuilder,
                _richTextReactModelBuilder,
                _localizationProvider,
                _urlResolver,
                _urlCheckingService);
        }

        [Fact]
        public void GetViewModel_WhenAllFieldsProvided_ShouldMapFields()
        {
            const string expectedTitle = "Title";
            const string expectedImageAltText = "Alt";
            const string expectedImageUrl = "url";
            const string expectedIngress = "ingress";
            var expectedContentArea = new ReactModels.ContentArea();

            A.CallTo(() => _contentLoader.Get<ImageFile>(A<ContentReference>._))
                .Returns(new ImageFile { AltText = expectedImageAltText, ContentLink = ContentReference.EmptyReference });
            A.CallTo(() => _fluidImageReactModelBuilder.BuildReactModel(A<ImageFile>._, A<string>._))
                .Returns(new ReactModels.FluidImage { Alt = expectedImageAltText, Src = expectedImageUrl });
            A.CallTo(() => _contentAreaReactModelBuilder.BuildReactModel(A<ContentArea>._, A<string>._, A<RenderOptionQueryParameter>._))
                .Returns(expectedContentArea);

            var contentAreaPage = A.Fake<ContentAreaPage>();
            contentAreaPage.PageName = expectedTitle;
            contentAreaPage.MainIntro = expectedIngress;
            contentAreaPage.Title = null;
            contentAreaPage.TitleWithCustomSeparator = null;

            var result = (ReactModels.ContentAreaPage)_mapper.GetPageViewModel(contentAreaPage).Model;

            Assert.Equal(expectedTitle, result.PageHeader.Title);
            Assert.Equal(expectedIngress, result.PageHeader.Ingress);
            Assert.Equal(expectedImageAltText, result.PageHeader.Image.Alt);
            Assert.Equal(expectedImageUrl, result.PageHeader.Image.Src);
            Assert.Equal(expectedContentArea, result.Content);
        }

        [Fact]
        public void GetViewModel_WhenTitleIsSet_ShouldOverwritePageName()
        {
            const string expectedTitle = "Title";

            var contentAreaPage = A.Fake<ContentAreaPage>();
            contentAreaPage.PageName = "PageName";
            contentAreaPage.Title = expectedTitle;
            contentAreaPage.TitleWithCustomSeparator = null;

            var result = (ReactModels.ContentAreaPage)_mapper.GetPageViewModel(contentAreaPage).Model;

            Assert.Equal(expectedTitle, result.PageHeader.Title);
        }

        [Fact]
        public void GetViewModel_WhenMarkersPresent_ShouldNotMapImage()
        {
            var contentAreaPage = A.Fake<ContentAreaPage>();
            const string expectedImageAltText = "Alt";
            const string expectedImageUrl = "url";

            A.CallTo(() => _contentLoader.Get<ImageFile>(A<ContentReference>._))
                .Returns(new ImageFile { AltText = expectedImageAltText, ContentLink = ContentReference.EmptyReference });
            A.CallTo(() => _fluidImageReactModelBuilder.BuildReactModel(A<ImageFile>._, A<string>._))
                .Returns(new ReactModels.FluidImage { Alt = expectedImageAltText, Src = expectedImageUrl });

            contentAreaPage.Markers = new List<MapMarker>
            {
                new MapMarker()
            };

            var result = (ReactModels.ContentAreaPage)_mapper.GetPageViewModel(contentAreaPage).Model;

            Assert.Null(result.PageHeader.Image);
        }

        [Fact]
        public void GetViewModel_WhenMarkersNotEmpty_ShouldMapMarkersCorrectly()
        {
            var expectedMarkerTitle = "MarkerTitle";
            var expectedMarkerLinkUrl = new Url("MarkerLinkUrl");
            var expectedMarkerLinkText = "MarkerLinkText";
            var expectedMarkerAddress = new ReactModels.RichText();
            var expectedMarkerLatitude = 59.0F;
            var expectedMarkerLongitude = 10.0F;
            var expectedMarkerPopupTitle = "MarkerPopupTitle";

            A.CallTo(() => _richTextReactModelBuilder.BuildReactModel(A<XhtmlString>._, A<string>._))
                .Returns(expectedMarkerAddress);
            A.CallTo(() => _urlResolver.GetUrl(A<UrlBuilder>.That.Matches(x => x.Path == expectedMarkerLinkUrl.Path), A<UrlResolverArguments>._))
                .Returns(expectedMarkerLinkUrl.ToString());

            var contentAreaPage = A.Fake<ContentAreaPage>();
            contentAreaPage.Markers = new List<MapMarker>
            {
                new MapMarker
                {
                    Name = expectedMarkerTitle,
                    Link = expectedMarkerLinkUrl,
                    LinkText = expectedMarkerLinkText,
                    Address = new XhtmlString(),
                    Latitude = expectedMarkerLatitude,
                    Longitude = expectedMarkerLongitude,
                    PopUpTitle = expectedMarkerPopupTitle
                }
            };

            var result = (ReactModels.ContentAreaPage)_mapper.GetPageViewModel(contentAreaPage).Model;

            Assert.Equal(expectedMarkerTitle, result.PageHeader.Map.Markers.First().Name);
            Assert.Equal(expectedMarkerLinkUrl, result.PageHeader.Map.Markers.First().Popup.Link.Url);
            Assert.Equal(expectedMarkerLinkText, result.PageHeader.Map.Markers.First().Popup.Link.Text);
            Assert.Equal(expectedMarkerAddress, result.PageHeader.Map.Markers.First().Popup.Address);
            Assert.Equal(expectedMarkerLatitude, result.PageHeader.Map.Markers.First().Latitude);
            Assert.Equal(expectedMarkerLongitude, result.PageHeader.Map.Markers.First().Longitude);
            Assert.Equal(expectedMarkerPopupTitle, result.PageHeader.Map.Markers.First().Popup.Title);
        }

        [Fact]
        public void GetViewModel_WhenMarkerIsNotPresent_ShouldMapMapToNull()
        {
            var contentAreaPage = A.Fake<ContentAreaPage>();

            var result = (ReactModels.ContentAreaPage)_mapper.GetPageViewModel(contentAreaPage).Model;

            Assert.Null(result.PageHeader.Map);
        }

        [Fact]
        public void GetViewModel_WhenImageIsNotSet_ShouldNotMapImage()
        {
            var contentAreaPage = new ContentAreaPage
            {
                Image = null
            };

            var result = (ReactModels.ContentAreaPage)_mapper.GetPageViewModel(contentAreaPage).Model;

            Assert.Null(result.PageHeader.Image);
        }

        [Fact]
        public void GetViewModel_WhenPageIsInEditMode_ShouldMapOnPageEditing()
        {
            A.CallTo(() => _fluidImageReactModelBuilder.BuildReactModel(A<ImageFile>._, A<string>._))
                .Returns(new ReactModels.FluidImage { OnPageEditing = new ReactModels.FluidImage_OnPageEditing() });
            A.CallTo(() => _pageEditingAdapter.PageIsInEditMode()).Returns(true);

            var result = (ReactModels.ContentAreaPage)_mapper.GetPageViewModel(new ContentAreaPage()).Model;

            Assert.NotNull(result.PageHeader.OnPageEditing);
            Assert.NotNull(result.PageHeader.Image.OnPageEditing);
        }
    }
}
