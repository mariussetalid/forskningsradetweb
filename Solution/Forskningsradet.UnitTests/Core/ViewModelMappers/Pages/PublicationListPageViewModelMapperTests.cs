using System;
using System.Collections.Generic;
using System.Linq;
using EPiServer.Core;
using EPiServer.Web.Routing;
using FakeItEasy;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Builders;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.Enums;
using Forskningsradet.Core.Models.QueryParameters;
using Forskningsradet.Core.Models.RequestModels;
using Forskningsradet.Core.Models.SearchModels;
using Forskningsradet.Core.Services;
using Forskningsradet.Core.Services.Contracts;
using Forskningsradet.Core.ViewModelMappers.Pages;
using Xunit;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.UnitTests.Core.ViewModelMappers.Pages
{
    public class PublicationListPageViewModelMapperTests
    {
        private readonly IPublicationListService _publicationListService;
        private readonly IPublicationReactModelBuilder _publicationReactModelBuilder;
        private readonly ILocalizationProvider _localizationProvider;
        private readonly IUrlResolver _urlResolver;
        private readonly ISearchApiUrlResolver _searchApiUrlResolver;
        private readonly FilterGroupReactModelBuilder _filterGroupReactModelBuilder;
        private readonly IPaginationReactModelBuilder _paginationReactModelBuilder;
        private readonly PublicationListPageViewModelMapper _mapper;

        public PublicationListPageViewModelMapperTests()
        {
            _publicationListService = A.Fake<IPublicationListService>();
            _publicationReactModelBuilder = A.Fake<IPublicationReactModelBuilder>();
            _localizationProvider = A.Fake<ILocalizationProvider>();
            _urlResolver = A.Fake<IUrlResolver>();
            _searchApiUrlResolver = A.Fake<ISearchApiUrlResolver>();
            _filterGroupReactModelBuilder = new FilterGroupReactModelBuilder();
            _paginationReactModelBuilder = A.Fake<IPaginationReactModelBuilder>();

            _mapper = new PublicationListPageViewModelMapper(
                _publicationListService,
                _publicationReactModelBuilder,
                _localizationProvider,
                _urlResolver,
                _searchApiUrlResolver,
                _filterGroupReactModelBuilder,
                _paginationReactModelBuilder
                );
        }

        [Fact]
        public void GetViewModel_WithDefaultValues_ShouldMapDefaultFields()
        {
            const string expectedUrl = "url-to-self";
            const string expectedSearchLabel = "s";
            const string expectedSubmitButtonLabel = "sub";
            const string expectedResetLabel = "r";
            const string expectedShowResultsLabel = "show_res";

            var page = A.Fake<PublicationListPage>();
            page.PageName = "page name";
            page.ContentLink = new PageReference(1);

            ArrangeSearch(new List<PublicationPage>());
            ArrangeTranslation(expectedSearchLabel, "Search");
            ArrangeTranslation(expectedSubmitButtonLabel, "SubmitButtonText");
            ArrangeTranslation(expectedResetLabel, "Reset");
            ArrangeTranslation(expectedShowResultsLabel, "ShowResultsFormat");

            A.CallTo(() => _urlResolver.GetUrl(A<ContentReference>.That.Matches(x => x.ID == page.ContentLink.ID), A<string>._, A<UrlResolverArguments>._))
                .Returns(expectedUrl);

            var result = (ReactModels.PublicationsPage)_mapper.GetPageViewModel(
                    page,
                    A.Fake<PublicationListQueryParameter>())
                .Model;

            Assert.NotNull(result.EmptyList);
            Assert.Null(result.ResultsDescription);
            Assert.Equal(page.PageName, result.Title);
            Assert.Equal(expectedUrl, result.Form.Endpoint);
            Assert.Equal(expectedSubmitButtonLabel, result.Form.SubmitButtonText);
            Assert.Equal(expectedSearchLabel, result.Search.Submit.Text);
            Assert.Equal("q", result.Search.Input.Name);
            Assert.Equal("q", result.Search.Input.Label);
            Assert.Equal(expectedSearchLabel, result.Search.Input.Placeholder);
            Assert.Null(result.Search.Input.Value);
            Assert.Equal(expectedResetLabel, result.FilterLayout.Filters.Labels.Reset);
            Assert.Equal(expectedShowResultsLabel, result.FilterLayout.Filters.Labels.ShowResults);
        }

        [Fact]
        public void GetViewModel_WithDefaultValues_ShouldInjectResultsCountToShowResultsLabel()
        {
            var expectedShowResultsLabel = "LBL show 10 results";
            ArrangeTranslation("LBL show {0} results", "ShowResultsFormat");

            var page = A.Fake<PublicationListPage>();

            A.CallTo(() => _publicationListService.Search(A<PublicationRequest>._))
                .Returns(new PublicationListSearchResults(new PublicationBasePage[0], new FilterOption[0], new FilterOption[0], 10));

            var result = (ReactModels.PublicationsPage)_mapper.GetPageViewModel(
                    page,
                    A.Fake<PublicationListQueryParameter>())
                .Model;

            Assert.Equal(expectedShowResultsLabel, result.FilterLayout.Filters.Labels.ShowResults);
        }

        [Fact]
        public void GetViewModel_WhenThereAreNoSearchHits_ShouldMapEmptyList()
        {
            const string expectedEmptyLabel = "empty";
            ArrangeTranslation(expectedEmptyLabel, "EmptySearch");

            var result = (ReactModels.PublicationsPage)_mapper.GetPageViewModel(
                    A.Fake<PublicationListPage>(),
                    A.Fake<PublicationListQueryParameter>())
                .Model;

            Assert.NotNull(result.EmptyList);
            Assert.Equal(expectedEmptyLabel, result.EmptyList.Text);
        }

        [Fact]
        public void GetViewModel_WithSearchHit_ShouldMapNavigation()
        {
            const string expectedFirstGroup = "2002";
            const string expectedSecondGroup = "2000";

            var page1 = A.Fake<PublicationPage>();
            page1.Year = 2002;
            var page2 = A.Fake<PublicationPage>();
            page2.Year = 2000;

            ArrangeSearch(new List<PublicationPage> { page1, page2 },
                years: new List<int> { page1.Year, page2.Year }
            );

            var result = ((ReactModels.PublicationsPage)_mapper.GetPageViewModel(
                    A.Fake<PublicationListPage>(),
                    A.Fake<PublicationListQueryParameter>())
                .Model).Navigation;

            Assert.Equal(2, result.Count);

            var firstGroup = result[0];
            Assert.Equal(expectedFirstGroup, firstGroup.Link.Text);
            Assert.Equal($"?year={expectedFirstGroup}", firstGroup.Link.Url);

            var secondGroup = result[1];
            Assert.Equal(expectedSecondGroup, secondGroup.Link.Text);
            Assert.Equal($"?year={expectedSecondGroup}", secondGroup.Link.Url);
        }

        [Fact]
        public void GetViewModel_WithoutQueryParameters_ShouldSetLatestYearInHits()
        {
            const int expectedLatestYear = 2015;
            var page = A.Fake<PublicationPage>();
            page.Year = 2002;
            ArrangeSearch(
                new List<PublicationPage>(),
                years: new List<int> { expectedLatestYear, page.Year }
            );
            var query = new PublicationListQueryParameter
            {
                Query = "",
                Year = 0,
                Types = new int[0]
            };

            var result = ((ReactModels.PublicationsPage)_mapper.GetPageViewModel(
                    A.Fake<PublicationListPage>(),
                    query)
                .Model).Navigation;

            Assert.Equal(2, result.Count);

            var firstGroup = result[0];
            Assert.Equal(expectedLatestYear.ToString(), firstGroup.Link.Text);
            Assert.True(firstGroup.IsCurrent);

            var secondGroup = result[1];
            Assert.Equal(page.Year.ToString(), secondGroup.Link.Text);
            Assert.False(secondGroup.IsCurrent);
        }

        [Fact]
        public void GetViewModel_WithQuery_ShouldOnlySetUrlForGroupFoundInResult()
        {
            var page1 = A.Fake<PublicationPage>();
            page1.Year = 2002;
            var page2 = A.Fake<PublicationPage>();
            page2.Year = 2000;
            ArrangeSearch(
                new List<PublicationPage> { page1 },
                years: new List<int> { page1.Year, page2.Year }
            );
            var query = new PublicationListQueryParameter
            {
                Query = "q"
            };

            var result = ((ReactModels.PublicationsPage)_mapper.GetPageViewModel(
                    A.Fake<PublicationListPage>(),
                    query)
                .Model).Navigation;

            Assert.Equal(2, result.Count);

            var firstGroup = result[0];
            Assert.Equal(page1.Year.ToString(), firstGroup.Link.Text);
            Assert.NotNull(firstGroup.Link.Url);

            var secondGroup = result[1];
            Assert.Equal(page2.Year.ToString(), secondGroup.Link.Text);
            Assert.Null(secondGroup.Link.Url);
        }

        [Fact]
        public void GetViewModel_WithTypes_ShouldOnlySetUrlForGroupFoundInResult()
        {
            var page1 = A.Fake<PublicationPage>();
            page1.Year = 2002;
            var page2 = A.Fake<PublicationPage>();
            page2.Year = 2000;
            ArrangeSearch(
                new List<PublicationPage> { page1 },
                years: new List<int> { page1.Year, page2.Year }
            );
            var query = new PublicationListQueryParameter
            {
                Types = new[] { 1 }
            };

            var result = ((ReactModels.PublicationsPage)_mapper.GetPageViewModel(
                    A.Fake<PublicationListPage>(),
                    query)
                .Model).Navigation;

            Assert.Equal(2, result.Count);

            var firstGroup = result[0];
            Assert.Equal(page1.Year.ToString(), firstGroup.Link.Text);
            Assert.NotNull(firstGroup.Link.Url);

            var secondGroup = result[1];
            Assert.Equal(page2.Year.ToString(), secondGroup.Link.Text);
            Assert.Null(secondGroup.Link.Url);
        }

        [Fact]
        public void GetViewModel_WithQueryAndTypesAndInvalidYear_ShouldOnlySetUrlForGroupFoundInResult()
        {
            var page1 = A.Fake<PublicationPage>();
            page1.Year = 2002;
            var page2 = A.Fake<PublicationPage>();
            page2.Year = 2000;
            ArrangeSearch(
                new List<PublicationPage> { page1 },
                years: new List<int> { page1.Year, page2.Year }
            );
            var query = new PublicationListQueryParameter
            {
                Query = "q",
                Types = new[] { 1 },
                Year = 2001
            };

            var result = ((ReactModels.PublicationsPage)_mapper.GetPageViewModel(
                    A.Fake<PublicationListPage>(),
                    query)
                .Model).Navigation;

            Assert.Equal(2, result.Count);

            var firstGroup = result[0];
            Assert.Equal(page1.Year.ToString(), firstGroup.Link.Text);
            Assert.NotNull(firstGroup.Link.Url);

            var secondGroup = result[1];
            Assert.Equal(page2.Year.ToString(), secondGroup.Link.Text);
            Assert.Null(secondGroup.Link.Url);
        }

        [Fact]
        public void GetViewModel_WithQueryAndYear_ShouldSetUrlForAllGroups()
        {
            var page1 = A.Fake<PublicationPage>();
            page1.Year = 2002;
            var page2 = A.Fake<PublicationPage>();
            page2.Year = 2000;
            ArrangeSearch(
                new List<PublicationPage> { page1 },
                years: new List<int> { page1.Year, page2.Year }
            );
            var query = new PublicationListQueryParameter
            {
                Query = "q",
                Year = page1.Year
            };

            var result = ((ReactModels.PublicationsPage)_mapper.GetPageViewModel(
                    A.Fake<PublicationListPage>(),
                    query)
                .Model).Navigation;

            Assert.Equal(2, result.Count);

            var firstGroup = result[0];
            Assert.Equal(page1.Year.ToString(), firstGroup.Link.Text);
            Assert.NotNull(firstGroup.Link.Url);

            var secondGroup = result[1];
            Assert.Equal(page2.Year.ToString(), secondGroup.Link.Text);
            Assert.NotNull(secondGroup.Link.Url);
        }

        [Fact]
        public void GetViewModel_WithTypeAndYear_ShouldSetUrlForAllGroups()
        {
            var page1 = A.Fake<PublicationPage>();
            page1.Year = 2002;
            var page2 = A.Fake<PublicationPage>();
            page2.Year = 2000;
            ArrangeSearch(
                new List<PublicationPage> { page1 },
                years: new List<int> { page1.Year, page2.Year }
            );
            var query = new PublicationListQueryParameter
            {
                Types = new[] { 1 },
                Year = page1.Year
            };

            var result = ((ReactModels.PublicationsPage)_mapper.GetPageViewModel(
                    A.Fake<PublicationListPage>(),
                    query)
                .Model).Navigation;

            Assert.Equal(2, result.Count);

            var firstGroup = result[0];
            Assert.Equal(page1.Year.ToString(), firstGroup.Link.Text);
            Assert.NotNull(firstGroup.Link.Url);

            var secondGroup = result[1];
            Assert.Equal(page2.Year.ToString(), secondGroup.Link.Text);
            Assert.NotNull(secondGroup.Link.Url);
        }

        [Fact]
        public void GetViewModel_WithSearchHit_ShouldMapSearchGroups()
        {
            const string expectedFirstGroup = "2002";
            const string expectedSecondGroup = "2000";

            var page1 = A.Fake<PublicationPage>();
            page1.PageName = "FirstPage";
            page1.Year = 2002;
            var page2 = A.Fake<PublicationPage>();
            page2.PageName = "SecondPage";
            page2.Year = 2000;

            ArrangeSearch(new List<PublicationPage> { page1, page2 },
                years: new List<int> { page1.Year, page2.Year }
            );

            var result = ((ReactModels.PublicationsPage)_mapper.GetPageViewModel(
                    A.Fake<PublicationListPage>(),
                    A.Fake<PublicationListQueryParameter>())
                .Model).Results;

            Assert.Equal(2, result.Count);

            var firstGroup = result[0];
            Assert.Equal(expectedFirstGroup, firstGroup.Title);
            Assert.Equal(expectedFirstGroup, firstGroup.HtmlId);
            Assert.Equal(1, firstGroup.Results.Blocks.Count);

            var secondGroup = result[1];
            Assert.Equal(expectedSecondGroup, secondGroup.Title);
            Assert.Equal(expectedSecondGroup, secondGroup.HtmlId);
            Assert.Equal(1, secondGroup.Results.Blocks.Count); ;
        }

        [Fact]
        public void GetViewModel_WithSearchHit_ShouldMapSearchHit()
        {
            const string expectedAttachmentUrl = "attachment-url";
            const string expectedTypeLabel = "T";
            const string expectedNumberOfPagesLabel = "7";
            const string expectedAuthorLabel = "A";
            const string expectedYearLabel = "2002";
            var page = A.Fake<PublicationPage>();
            page.PageName = "FirstPage";
            page.ContentLink = new ContentReference(1);
            page.Subtitle = "S";
            page.Year = 2002;
            page.Attachment = new ContentReference(2);
            page.Type = "P";
            page.PublicationType = PublicationType.Program;
            page.NumberOfPages = 7;
            page.Author = "Author";

            ArrangeSearch(new List<PublicationPage> { page });
            ArrangeEnumTranslation(page.Type, PublicationType.Program);
            ArrangeTranslation(expectedTypeLabel, "Type");
            ArrangeTranslation(expectedNumberOfPagesLabel, "NumberOfPages");
            ArrangeTranslation(expectedAuthorLabel, "Author");
            ArrangeTranslation(expectedYearLabel, "Year");

            A.CallTo(() => _urlResolver.GetUrl(A<ContentReference>.That.Matches(x => x.ID == page.Attachment.ID), A<string>._, A<UrlResolverArguments>._))
                .Returns(expectedAttachmentUrl);
            A.CallTo(() => _publicationReactModelBuilder.BuildReactModel(A<PublicationPage>._, PublicationMetadata.Standard, false))
                .Returns(new ReactModels.SearchResult
                {
                    Title = page.PageName,
                    Url = expectedAttachmentUrl,
                    Text = page.Subtitle,
                    Metadata = new ReactModels.Metadata
                    {
                        Items = new List<ReactModels.Metadata_Items>
                        {
                            new ReactModels.Metadata_Items { Label = expectedTypeLabel, Text = page.Type },
                            new ReactModels.Metadata_Items { Label = expectedNumberOfPagesLabel, Text = page.NumberOfPages.ToString() },
                            new ReactModels.Metadata_Items { Label = expectedAuthorLabel, Text = page.Author },
                            new ReactModels.Metadata_Items { Label = expectedYearLabel, Text = page.Year.ToString() }
                        }
                    }
                });

            var result = ((ReactModels.PublicationsPage)_mapper.GetPageViewModel(
                    A.Fake<PublicationListPage>(),
                    A.Fake<PublicationListQueryParameter>())
                .Model).Results;

            Assert.Equal(1, result[0].Results.Blocks.Count);

            var searchHit = (ReactModels.SearchResult)result[0].Results.Blocks[0].ComponentData;
            Assert.Equal(page.PageName, searchHit.Title);
            Assert.Equal(page.Subtitle, searchHit.Text);
            Assert.Equal(expectedAttachmentUrl, searchHit.Url);

            var metadata = searchHit.Metadata.Items;
            Assert.Equal(expectedTypeLabel, metadata[0].Label);
            Assert.Equal(page.Type, metadata[0].Text);
            Assert.Equal(expectedNumberOfPagesLabel, metadata[1].Label);
            Assert.Equal(page.NumberOfPages.ToString(), metadata[1].Text);
            Assert.Equal(expectedAuthorLabel, metadata[2].Label);
            Assert.Equal(page.Author, metadata[2].Text);
            Assert.Equal(expectedYearLabel, metadata[3].Label);
            Assert.Equal(page.Year.ToString(), metadata[3].Text);
        }

        [Fact]
        public void GetViewModel_WithSearchHits_ShouldOrderAlphabeticallyWithinYear()
        {
            const string expectedFirstTitle = "A";
            const string expectedSecondTitle = "B";

            var page1 = A.Fake<PublicationPage>();
            page1.PageName = "B";
            page1.ContentLink = new ContentReference(1);
            page1.Year = 2002;

            var page2 = A.Fake<PublicationPage>();
            page2.PageName = "A";
            page2.ContentLink = new ContentReference(2);
            page2.Year = 2002;

            ArrangeSearch(new List<PublicationPage> { page1, page2 });
            A.CallTo(() => _publicationReactModelBuilder.BuildReactModel(A<PublicationPage>.That.Matches(x => x.ContentLink.ID == page1.ContentLink.ID), PublicationMetadata.Standard, false))
                .Returns(new ReactModels.SearchResult { Title = page1.PageName });
            A.CallTo(() => _publicationReactModelBuilder.BuildReactModel(A<PublicationPage>.That.Matches(x => x.ContentLink.ID == page2.ContentLink.ID), PublicationMetadata.Standard, false))
                .Returns(new ReactModels.SearchResult { Title = page2.PageName });

            var result = ((ReactModels.PublicationsPage)_mapper.GetPageViewModel(A.Fake<PublicationListPage>(), A.Fake<PublicationListQueryParameter>())
                .Model)
                .Results[0]
                .Results
                .Blocks;

            Assert.Equal(2, result.Count);
            Assert.Equal(expectedFirstTitle, ((ReactModels.SearchResult)result[0].ComponentData).Title);
            Assert.Equal(expectedSecondTitle, ((ReactModels.SearchResult)result[1].ComponentData).Title);
        }

        [Fact]
        public void GetViewModel_WithType_ShouldMapDefaultFields()
        {
            const string expectedCollapseLabel = "col";
            const string expectedExpandLabel = "exp";

            ArrangeSearch(new List<PublicationPage>(), types: new List<FilterOption>());
            ArrangeTranslation(expectedCollapseLabel, "Collapse");
            ArrangeTranslation(expectedExpandLabel, "Expand");

            var result = (ReactModels.PublicationsPage)_mapper.GetPageViewModel(
                    A.Fake<PublicationListPage>(),
                    A.Fake<PublicationListQueryParameter>())
                .Model;

            var filter = result.FilterLayout.Filters.Items[0];

            Assert.NotNull(filter.Accordion);
            Assert.NotNull(filter.Accordion.Guid);
            Assert.Equal(expectedCollapseLabel, filter.Accordion.CollapseLabel);
            Assert.Equal(expectedExpandLabel, filter.Accordion.ExpandLabel);
            Assert.False(filter.Accordion.InitiallyOpen);
        }

        [Fact]
        public void GetViewModel_WithFilters_ShouldMapFilters()
        {
            const string expectedFilterName = "type";
            const string expectedFilterTitle = "t";
            const string expectedFilterLabel = "option (1)";

            var filterOption = new FilterOption
            {
                Name = "option",
                Value = "2",
                Count = 1
            };
            ArrangeSearch(new List<PublicationPage>(),
                types: new List<FilterOption> { filterOption });
            ArrangeTranslation(expectedFilterTitle, "FilterTitleType");

            var result = (ReactModels.PublicationsPage)_mapper.GetPageViewModel(
                    A.Fake<PublicationListPage>(),
                    A.Fake<PublicationListQueryParameter>())
                .Model;

            var filter = result.FilterLayout.Filters.Items[0];
            Assert.Equal(expectedFilterTitle, filter.Title);

            var resultOption = filter.Options[0];
            Assert.Equal(expectedFilterName, resultOption.Name);
            Assert.Equal(expectedFilterLabel, resultOption.Label);
            Assert.Equal(filterOption.Value, resultOption.Value);
            Assert.False(resultOption.Checked);
        }

        [Fact]
        public void GetViewModel_WhenTypeIsSelected_ShouldCheckTypeCheckbox()
        {
            const int value = 1;
            var query = new PublicationListQueryParameter { Types = new[] { value } };
            var facet = new FilterOption { Value = value.ToString() };
            ArrangeSearch(new List<PublicationPage>(),
                types: new List<FilterOption> { facet });

            var result = ((ReactModels.PublicationsPage)_mapper.GetPageViewModel(
                    A.Fake<PublicationListPage>(),
                    query)
                .Model).FilterLayout.Filters.Items[0];

            Assert.True(result.Options[0].Checked);
            Assert.True(result.Accordion.InitiallyOpen);
        }

        [Fact]
        public void GetViewModel_WhenCreatingNavigationUrl_ShouldPreserveSearchQuery()
        {
            const string expectedUrl = "?q=abc&year=2000";
            var query = new PublicationListQueryParameter { Query = "abc" };
            var page = GetSearchHitForTest();
            page.Year = 2000;

            ArrangeSearch(
                new List<PublicationPage> { page },
                years: new List<int> { page.Year }
            );

            var result = (ReactModels.PublicationsPage)_mapper.GetPageViewModel(
                    A.Fake<PublicationListPage>(),
                    query)
                .Model;

            Assert.Equal(expectedUrl, result.Navigation[0].Link.Url);
        }

        [Fact]
        public void GetViewModel_WithSearchInAttachmentsAndNonEmptyQuery_FillsPagination()
        {
            var query = new PublicationListQueryParameter { Query = "abc" };
            var listPage = A.Fake<PublicationListPage>();
            A.CallTo(() => listPage.IncludeContentOfAttachmentWhenSearching).Returns(true);

            A.CallTo(() => _paginationReactModelBuilder.BuildReactModel(A<string>._, A<int>._, A<int>._, A<int>._, A<string>._))
                .Returns(new ReactModels.Pagination());

            var result = (ReactModels.PublicationsPage)_mapper.GetPageViewModel(listPage, query)
                .Model;

            Assert.NotNull(result.Pagination);
        }

        [Fact]
        public void GetViewModel_WithSearchInAttachmentsButEmptyQuery_DoesNotFillPagination()
        {
            var query = new PublicationListQueryParameter { Query = null };
            var listPage = A.Fake<PublicationListPage>();
            A.CallTo(() => listPage.IncludeContentOfAttachmentWhenSearching).Returns(true);

            A.CallTo(() => _paginationReactModelBuilder.BuildReactModel(A<string>._, A<int>._, A<int>._, A<int>._, A<string>._))
                .Returns(new ReactModels.Pagination());

            var result = (ReactModels.PublicationsPage)_mapper.GetPageViewModel(listPage, query)
                .Model;

            Assert.Null(result.Pagination);
        }

        [Fact]
        public void GetViewModel_WithoutSearchInAttachments_DoesNotFillPagination()
        {
            var query = new PublicationListQueryParameter() { Query = "abc" };
            var listPage = A.Fake<PublicationListPage>();
            A.CallTo(() => listPage.IncludeContentOfAttachmentWhenSearching).Returns(false);

            A.CallTo(() => _paginationReactModelBuilder.BuildReactModel(A<string>._, A<int>._, A<int>._, A<int>._, A<string>._))
                .Returns(new ReactModels.Pagination());

            var result = (ReactModels.PublicationsPage)_mapper.GetPageViewModel(listPage, query)
                .Model;

            Assert.Null(result.Pagination);
        }

        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        [InlineData(100)]
        public void GetViewModel_WithSearchInAttachmentsAndNonEmptyQuery_ShouldRequestSearchForGivenPage(int page)
        {
            var query = new PublicationListQueryParameter
            {
                Query = "abc",
                Page = page
            };

            var listPage = A.Fake<PublicationListPage>();
            A.CallTo(() => listPage.IncludeContentOfAttachmentWhenSearching).Returns(true);

            _mapper.GetPageViewModel(listPage, query);

            A.CallTo(() => _publicationListService.Search(A<PublicationRequest>.That
                    .Matches(x =>
                        x.Page == page
                        && x.PageSize == SearchConstants.SearchPageSize
                        )))
                .MustHaveHappenedOnceExactly();
        }

        [Fact]
        public void GetViewModel_WithoutSearchInAttachments_ShouldRequestSearchWithMaxPageSize()
        {
            var query = new PublicationListQueryParameter { Query = "abc" };
            var listPage = A.Fake<PublicationListPage>();
            A.CallTo(() => listPage.IncludeContentOfAttachmentWhenSearching).Returns(false);

            _mapper.GetPageViewModel(listPage, query);

            A.CallTo(() => _publicationListService.Search(A<PublicationRequest>.That
                    .Matches(x =>
                        x.PageSize == SearchConstants.MaxPageSize
                    )))
                .MustHaveHappenedOnceExactly();
        }

        [Fact]
        public void GetViewModel_WithSearchInAttachmentsButEmptyQuery_ShouldRequestSearchWithMaxPageSize()
        {
            var query = new PublicationListQueryParameter { Query = null };
            var listPage = A.Fake<PublicationListPage>();
            A.CallTo(() => listPage.IncludeContentOfAttachmentWhenSearching).Returns(true);

            _mapper.GetPageViewModel(listPage, query);

            A.CallTo(() => _publicationListService.Search(A<PublicationRequest>.That
                    .Matches(x =>
                        x.PageSize == SearchConstants.MaxPageSize
                    )))
                .MustHaveHappenedOnceExactly();
        }

        [Fact]
        public void GetViewModel_FillsPagination_WithAllRequestedParams()
        {
            var query = new PublicationListQueryParameter { Query = "somethingIAmSearchingFor", Types = new[] { 3333, 4444 }, Year = 2020 };
            var expectedQueryValues = new[] { "somethingIAmSearchingFor", "3333", "4444", "2020" };

            var listPage = A.Fake<PublicationListPage>();
            A.CallTo(() => listPage.IncludeContentOfAttachmentWhenSearching).Returns(true);

            _mapper.GetPageViewModel(listPage, query);

            A.CallTo(() => _paginationReactModelBuilder.BuildReactModel(
                    A<string>.That.Matches(queryString => expectedQueryValues.All(queryString.Contains)),
                    A<int>._,
                    A<int>._,
                    A<int>._,
                    A<string>._))
                .MustHaveHappened();
        }

        private static PublicationPage GetSearchHitForTest()
        {
            var searchHit = A.Fake<PublicationPage>();
            searchHit.PageName = "hit";
            return searchHit;
        }

        private void ArrangeEnumTranslation<TEnum>(string expected, TEnum value) where TEnum : Enum
            => A.CallTo(() => _localizationProvider.GetEnumName<TEnum>(value))
                .Returns(expected);

        private void ArrangeTranslation(string expected, string label)
            => A.CallTo(() => _localizationProvider.GetLabel(nameof(PublicationListPage), label))
                .Returns(expected);

        private void ArrangeSearch(
            List<PublicationPage> searchHits,
            List<FilterOption> types = null,
            List<int> years = null,
            int? totalMatching = null)
        {
            var result = new PublicationListSearchResults(
                searchHits,
                types ?? new List<FilterOption>(),
                years?.Select(x => new FilterOption { Count = 1, Name = x.ToString(), Value = x.ToString() }) ?? new List<FilterOption>(),
                totalMatching ?? searchHits.Count);

            A.CallTo(() => _publicationListService.Search(A<PublicationRequest>._))
                .Returns(result);
        }
    }
}