using System;
using System.Collections.Generic;
using System.Linq;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.Web.Routing;
using FakeItEasy;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Builders;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Extensions;
using Forskningsradet.Core.Factories;
using Forskningsradet.Core.Models.ApiModels.Evurdering;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.QueryParameters;
using Forskningsradet.Core.Models.SearchModels;
using Forskningsradet.Core.Services;
using Forskningsradet.Core.Services.Contracts;
using Forskningsradet.Core.ViewModelMappers.Pages;
using Forskningsradet.UnitTests.Builders;
using Xunit;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.UnitTests.Core.ViewModelMappers.Pages
{
    public class ProposalListPageViewModelMapperTests
    {
        private readonly IProposalService _proposalService;
        private readonly ILocalizationProvider _localizationProvider;
        private readonly IUrlResolver _urlResolver;
        private readonly IRichTextReactModelBuilder _richTextReactModelBuilder;
        private readonly IContentAreaReactModelBuilder _contentAreaReactModelBuilder;
        private readonly IDateCardDatesReactModelBuilder _dateCardDatesReactModelBuilder;
        private readonly IDateCardStatusHandler _dateCardStatusHandler;
        private readonly FilterLayoutReactModelBuilder _filterLayoutReactModelBuilder;
        private readonly ICategoryLocalizationProvider _categoryLocalizationProvider;
        private readonly CategoryRepository _categoryRepository;
        private readonly IMessageReactModelBuilder _messageReactModelBuilder;
        private readonly IContentLoader _contentLoader;
        private readonly IViewModelFactory _viewModelFactory;
        private readonly ISearchApiUrlResolver _searchApiUrlResolver;
        private readonly ProposalListPageViewModelMapper _mapper;

        public ProposalListPageViewModelMapperTests()
        {
            _proposalService = A.Fake<IProposalService>();
            _localizationProvider = A.Fake<ILocalizationProvider>();
            _urlResolver = A.Fake<IUrlResolver>();
            _richTextReactModelBuilder = A.Fake<IRichTextReactModelBuilder>();
            _contentAreaReactModelBuilder = A.Fake<IContentAreaReactModelBuilder>();
            _dateCardDatesReactModelBuilder = A.Fake<IDateCardDatesReactModelBuilder>();
            _dateCardStatusHandler = A.Fake<IDateCardStatusHandler>();
            _categoryLocalizationProvider = A.Fake<ICategoryLocalizationProvider>();
            _categoryRepository = A.Fake<CategoryRepository>();
            _messageReactModelBuilder = A.Fake<IMessageReactModelBuilder>();
            _contentLoader = A.Fake<IContentLoader>();
            _viewModelFactory = A.Fake<IViewModelFactory>();
            _searchApiUrlResolver = A.Fake<ISearchApiUrlResolver>();
            _filterLayoutReactModelBuilder = new FilterLayoutReactModelBuilder(new FilterGroupReactModelBuilder(), _categoryLocalizationProvider, _localizationProvider);
            _mapper = new ProposalListPageViewModelMapper(
                _proposalService,
                _localizationProvider,
                _urlResolver,
                _richTextReactModelBuilder,
                _contentAreaReactModelBuilder,
                _dateCardDatesReactModelBuilder,
                _dateCardStatusHandler,
                _filterLayoutReactModelBuilder,
                _categoryLocalizationProvider,
                _categoryRepository,
                _messageReactModelBuilder,
                _contentLoader,
                _viewModelFactory,
                _searchApiUrlResolver
            );
        }

        [Fact]
        [UseCulture("nb-NO")]
        public void WithDefaultValues_ShouldMapDefaultFields()
        {
            const string expectedCurrentLabel = "current";
            const string expectedCompletedLabel = "completed";
            const string expectedApplicationTypeTitle = "application type (1 proposal)";
            const string expectedUrl = "url-to-self";
            const string expectedProposalUrl = "url-to-proposal";
            const string expectedDeadlineTitle = "deadline";
            const string expectedProjectSizeLabel = "project size";
            const string expectedProjectSizeFormat = "NOK {0}-{1}";
            const string expectedDurationLabel = "duration";
            const string expectedDuration = "10-20 måneder";
            const string expectedDurationFormat = "{0}-{1} måneder";
            const string expectedAmountLabel = "amount";
            const string expectedAmountFormat = "NOK {0}";
            const string expectedAmount = "NOK 100000";
            const string expectedProposal = "proposal";

            const string nonBreakingSpace = " ";
            var expectedProjectSize = $"NOK 100{nonBreakingSpace}000-1{nonBreakingSpace}000{nonBreakingSpace}000";
            var expectedRichTextComponent = new ReactModels.RichText();
            var expectedContentAreaComponent = new ReactModels.ContentArea();
            var expectedDateCardDatesComponent = new ReactModels.DateCardDates();
            var expectedDateCardStatus = new ReactModels.DateCardStatus();

            var page = A.Fake<ProposalListPage>();
            page.PageName = "page name";
            page.ContentLink = new PageReference(1);
            page.NumberOfVisibleItems = 4;

            var searchHit = (ProposalPage)GetProposalPageForTest()
                .WithProposalState(ProposalState.Active);
            var applicationType = GetApplicationTypeForTest(searchHit);
            var expectedMonthText = searchHit.Deadline?.ToDisplayDate().ToString("MMM yyyy");
            var expectedDay = searchHit.Deadline?.ToDisplayDate().ToString(DateTimeFormats.NorwegianDateDayLong);

            ArrangeSearch(new List<ApplicationTypeGroup> { applicationType });
            ArrangeTranslation(expectedCurrentLabel, "StatusFilterCurrent");
            ArrangeTranslation(expectedCompletedLabel, "StatusFilterCompleted");
            ArrangeTranslation(expectedDeadlineTitle, "Deadline");
            ArrangeTranslation(expectedProjectSizeLabel, "ProjectSize");
            ArrangeTranslation(expectedDurationLabel, "Duration");
            ArrangeTranslation(expectedProjectSizeFormat, "ProjectSizeFormat");
            ArrangeTranslation(expectedDurationFormat, "DurationFormat");
            ArrangeTranslation(expectedProposal, "ProposalInGroup");
            ArrangeTranslation(expectedAmountLabel, "Amount");
            ArrangeTranslation(expectedAmountFormat, "NokFormat");

            A.CallTo(() => _urlResolver.GetUrl(A<ContentReference>.That.Matches(x => x.ID == page.ContentLink.ID), A<string>._, A<UrlResolverArguments>._))
                .Returns(expectedUrl);
            A.CallTo(() => _urlResolver.GetUrl(A<ContentReference>.That.Matches(x => x.ID == searchHit.ContentLink.ID), A<string>._, A<UrlResolverArguments>._))
                .Returns(expectedProposalUrl);
            A.CallTo(() => _richTextReactModelBuilder.BuildReactModel(A<XhtmlString>._, A<string>._))
                .Returns(expectedRichTextComponent);
            A.CallTo(() => _contentAreaReactModelBuilder.BuildReactModel(A<ContentArea>._, A<string>._, A<RenderOptionQueryParameter>._))
                .Returns(expectedContentAreaComponent);
            A.CallTo(() => _dateCardDatesReactModelBuilder.BuildReactModel(A<ProposalBasePage>._, A<string>.That.Matches(x => x == expectedDeadlineTitle), A<string>._, A<bool>._))
                .Returns(expectedDateCardDatesComponent);
            A.CallTo(() => _dateCardStatusHandler.GetCurrentStatus(A<ProposalBasePage>._))
                .Returns(expectedDateCardStatus);

            var result = (ReactModels.ProposalListPage)_mapper.GetPageViewModel(
                page, A.Fake<ProposalListQueryParameter>()).Model;

            Assert.Null(result.EmptyList);
            Assert.Equal(page.PageName, result.Title);
            Assert.Equal(expectedUrl, result.Form.Endpoint);
            Assert.Equal(expectedContentAreaComponent, result.FilterLayout.ContentArea);

            Assert.True(result.TopFilter.Options[0].IsCurrent);
            Assert.Equal(expectedCurrentLabel, result.TopFilter.Options[0].Link.Text);
            Assert.Equal($"?{QueryParameters.TimeFrame}={(int)TimeFrameFilter.Future}", result.TopFilter.Options[0].Link.Url);

            Assert.False(result.TopFilter.Options[1].IsCurrent);
            Assert.Equal(expectedCompletedLabel, result.TopFilter.Options[1].Link.Text);
            Assert.Equal($"?{QueryParameters.TimeFrame}={(int)TimeFrameFilter.Past}", result.TopFilter.Options[1].Link.Url);

            Assert.Equal(page.NumberOfVisibleItems, result.Groups[0].NumberOfVisibleItems);
            Assert.Equal(expectedApplicationTypeTitle, result.Groups[0].Title);
            Assert.Equal(applicationType.SubTitle, result.Groups[0].Subtitle);
            Assert.Equal(expectedRichTextComponent, result.Groups[0].Text);

            var resultProposal = result.Groups[0].Proposals[0];
            Assert.Equal(searchHit.Id, resultProposal.Id);
            Assert.Equal(searchHit.ListTitle, resultProposal.Title);
            Assert.Equal(expectedProposalUrl, resultProposal.Url);
            Assert.Equal(searchHit.ListIntro, resultProposal.Text);
            Assert.Equal(expectedAmountLabel, resultProposal.Metadata.Items[0].Label);
            Assert.Equal(expectedAmount, resultProposal.Metadata.Items[0].Text);
            Assert.Equal(expectedProjectSizeLabel, resultProposal.Metadata.Items[1].Label);
            Assert.Equal(expectedProjectSize, resultProposal.Metadata.Items[1].Text);
            Assert.Equal(expectedDurationLabel, resultProposal.Metadata.Items[2].Label);
            Assert.Equal(expectedDuration, resultProposal.Metadata.Items[2].Text);
            Assert.Same(expectedDateCardDatesComponent, resultProposal.DateContainer);
            Assert.Equal(expectedDateCardStatus, resultProposal.Status);
        }

        [Fact]
        public void WhenGivenInternationalProposalWithMetaData_ShouldMapMetaData()
        {
            const string expectedDateFormat = "dd. MMMM yyyy";
            const string expectedProgram = "Europeisk fellesprogram";
            var expectedStatus = new ReactModels.DateCardStatus();

            var searchHit = (InternationalProposalPage)GetInternationalProposalPageForTest()
                .WithListTitle("listTitle")
                .WithListIntro("listIntro")
                .WithActivationDateDisplayName("Startskudd")
                .WithActivationDate(new DateTime(2050, 1, 14))
                .WithFirstDeadlineDisplayName("Skissefrist")
                .WithFirstDeadline(new DateTime(2050, 1, 21))
                .WithDeadlineDisplayName("Endelig frist")
                .WithDeadline(new DateTime(2050, 6, 10))
                .WithProgram(expectedProgram);

            var applicationType = GetApplicationTypeForTest(searchHit);
            var expectedActivationDate = searchHit.ActivationDate.Value.ToDisplayDate().ToString(expectedDateFormat);
            var expectedFirstDeadline = searchHit.FirstDeadline.Value.ToDisplayDate().ToString(expectedDateFormat);
            var expectedDeadline = searchHit.Deadline.Value.ToDisplayDate().ToString(expectedDateFormat);
            ArrangeSearch(new List<ApplicationTypeGroup> { applicationType });
            A.CallTo(() => _dateCardStatusHandler.GetCurrentStatus(A<ProposalBasePage>._))
                .Returns(expectedStatus);

            var result = (ReactModels.ProposalListPage)_mapper.GetPageViewModel(
                A.Fake<ProposalListPage>(), A.Fake<ProposalListQueryParameter>()).Model;

            Assert.Equal(expectedStatus, result.Groups[0].Proposals[0].Status);
            Assert.Equal(searchHit.ListTitle, result.Groups[0].Proposals[0].Title);
            Assert.Equal(searchHit.ActivationDateDisplayName, result.Groups[0].Proposals[0].Metadata.Items[0].Label);
            Assert.Equal(expectedActivationDate, result.Groups[0].Proposals[0].Metadata.Items[0].Text);
            Assert.Equal(searchHit.FirstDeadlineDisplayName, result.Groups[0].Proposals[0].Metadata.Items[1].Label);
            Assert.Equal(expectedFirstDeadline, result.Groups[0].Proposals[0].Metadata.Items[1].Text);
            Assert.Equal(searchHit.DeadlineDisplayName, result.Groups[0].Proposals[0].Metadata.Items[2].Label);
            Assert.Equal(expectedDeadline, result.Groups[0].Proposals[0].Metadata.Items[2].Text);
            Assert.Equal(expectedProgram, result.Groups[0].Proposals[0].Metadata.Items[3].Text);
            Assert.Equal(searchHit.ListIntro, result.Groups[0].Proposals[0].Text);
        }

        [Fact]
        public void WithDateTimeBeforeTwoDeadlines_ShouldMapFirstDeadlineWithCorrectLabels()
        {
            var searchHit = (InternationalProposalPage)GetInternationalProposalPageForTest()
                .WithFirstDeadline(new DateTime(2050, 1, 21))
                .WithFirstDeadlineDisplayName("Skissefrist");

            var applicationType = GetApplicationTypeForTest(searchHit);
            ArrangeSearch(new List<ApplicationTypeGroup> { applicationType });

            var result = (ReactModels.ProposalListPage)_mapper.GetPageViewModel(
                A.Fake<ProposalListPage>(), A.Fake<ProposalListQueryParameter>()).Model;

            Assert.Equal(searchHit.FirstDeadlineDisplayName, result.Groups[0].Proposals[0].Metadata.Items[0].Label);
        }

        [Fact]
        public void WithDateTimeBetweenTwoDeadlines_ShouldMapSecondDeadlineWithCorrectLabels()
        {
            var searchHit = (InternationalProposalPage)GetInternationalProposalPageForTest()
                .WithFirstDeadline(new DateTime(2010, 1, 21))
                .WithDeadline(new DateTime(2060, 6, 10))
                .WithDeadlineDisplayName("Endelig søknadsfrist");

            var applicationType = GetApplicationTypeForTest(searchHit);
            ArrangeSearch(new List<ApplicationTypeGroup> { applicationType });

            var result = (ReactModels.ProposalListPage)_mapper.GetPageViewModel(
                A.Fake<ProposalListPage>(), A.Fake<ProposalListQueryParameter>()).Model;

            Assert.Equal(searchHit.DeadlineDisplayName, result.Groups[0].Proposals[0].Metadata.Items[1].Label);
        }

        [Fact]
        public void WithSubjects_ShouldMapSubjectTags()
        {
            const string expectedSubject = "subject";
            var parentCategory = new Category(CategoryConstants.SubjectRoot, "Description");
            var category = new Category(parentCategory, "name");
            A.CallTo(() => _categoryRepository.Get(A<int>._))
                .Returns(category);
            A.CallTo(() => _categoryLocalizationProvider.GetCategoryName(A<int>._))
                .Returns(expectedSubject);
            ArrangeSearch(new List<ApplicationTypeGroup> { GetApplicationTypeForTest(GetProposalPageForTest()) });

            var result = ((ReactModels.ProposalListPage)_mapper.GetPageViewModel(
                A.Fake<ProposalListPage>(),
                A.Fake<ProposalListQueryParameter>()
            ).Model).Groups[0].Proposals[0].Tags.Items;

            Assert.Equal(expectedSubject, result[0].Text);
        }

        [Fact]
        public void WhenThereAreNoProposals_ShouldMapEmptyList()
        {
            const string expectedEmptyLabel = "empty";
            ArrangeTranslation(expectedEmptyLabel, "EmptyListLabel");

            var result = (ReactModels.ProposalListPage)_mapper.GetPageViewModel(
                A.Fake<ProposalListPage>(), A.Fake<ProposalListQueryParameter>()).Model;

            Assert.NotNull(result.EmptyList);
            Assert.Equal(expectedEmptyLabel, result.EmptyList.Text);
        }

        [Fact]
        public void WhenThereAreSeveralProposals_ShouldMapPlural()
        {
            const string expectedApplicationTypeTitle = "application type (2 proposals)";
            const string expectedProposalsText = "proposals";
            var searchHit = GetProposalPageForTest();
            var applicationType = GetApplicationTypeForTest(searchHit, searchHit);

            ArrangeSearch(new List<ApplicationTypeGroup> { applicationType });
            ArrangeTranslation(expectedProposalsText, "ProposalsInGroup");

            var result = (ReactModels.ProposalListPage)_mapper.GetPageViewModel(
                A.Fake<ProposalListPage>(), A.Fake<ProposalListQueryParameter>()).Model;

            Assert.Equal(expectedApplicationTypeTitle, result.Groups[0].Title);
        }

        [Fact]
        public void WhenVisibleItemsLimitIsExceeded_ShouldMapCollapseAndExpand()
        {
            const string expectedExpandLabel = "1 expand";
            const string expectedCollapseLabel = "collapse";

            var searchHit = GetProposalPageForTest();
            var applicationType = GetApplicationTypeForTest(searchHit, searchHit, searchHit, searchHit, searchHit);

            ArrangeSearch(new List<ApplicationTypeGroup> { applicationType });
            ArrangeTranslation("{0} expand", "ExpandLabel");
            ArrangeTranslation(expectedCollapseLabel, "CollapseLabel");

            var proposalListPage = A.Fake<ProposalListPage>();
            proposalListPage.NumberOfVisibleItems = 4;
            var result = (ReactModels.ProposalListPage)_mapper.GetPageViewModel(
                proposalListPage, A.Fake<ProposalListQueryParameter>()).Model;

            Assert.Equal(expectedExpandLabel, result.Groups[0].Accordion.ExpandLabel);
            Assert.Equal(expectedCollapseLabel, result.Groups[0].Accordion.CollapseLabel);
        }

        [Fact]
        public void WhenListIntroIsEmpty_ShouldUsePurposeAsText()
        {
            const string expectedText = "text";

            var proposalPage = GetProposalPageForTest()
                .WithListIntro(null)
                .WithPurpose(new XhtmlString(expectedText));

            var applicationType = GetApplicationTypeForTest(proposalPage);
            ArrangeSearch(new List<ApplicationTypeGroup> { applicationType });

            var result = (ReactModels.ProposalListPage)_mapper.GetPageViewModel(
                A.Fake<ProposalListPage>(), A.Fake<ProposalListQueryParameter>()).Model;

            Assert.Equal(expectedText, result.Groups[0].Proposals[0].Text);
        }

        [Fact]
        public void WhenDisableLinkIsTrue_ShouldNotMapUrl()
        {
            var proposalPage = GetProposalPageForTest()
                .WithDisableLink(true);

            var applicationType = GetApplicationTypeForTest(proposalPage);
            ArrangeSearch(new List<ApplicationTypeGroup> { applicationType });

            var result = (ReactModels.ProposalListPage)_mapper.GetPageViewModel(
                A.Fake<ProposalListPage>(), A.Fake<ProposalListQueryParameter>()).Model;

            Assert.Null(result.Groups[0].Proposals[0].Url);
        }

        [Fact]
        public void WhenThereAreMultipleApplicationTypes_ShouldMapMultipleGroups()
        {
            var proposalPage = GetProposalPageForTest();
            var applicationType1 = GetApplicationTypeForTest(proposalPage);
            var applicationType2 = GetApplicationTypeForTest(proposalPage);
            ArrangeSearch(new List<ApplicationTypeGroup> { applicationType1, applicationType2 });

            var result = (ReactModels.ProposalListPage)_mapper.GetPageViewModel(
                A.Fake<ProposalListPage>(), A.Fake<ProposalListQueryParameter>()).Model;

            Assert.Equal(2, result.Groups.Count);
        }

        [Fact]
        public void WhenStatusFilterIsSet_ShouldMapStatusAsActive()
        {
            var query = new ProposalListQueryParameter { TimeFrame = TimeFrameFilter.Past };

            var result = (ReactModels.ProposalListPage)_mapper
                .GetPageViewModel(A.Fake<ProposalListPage>(), query).Model;

            Assert.True(result.TopFilter.Options[1].IsCurrent);
        }

        [Fact]
        [UseCulture("en-US")]
        public void WithDeafultValuesEnglishCulture_ShouldMapProjectSize()
        {
            const string expectedProjectSizeFormat = "NOK {0}-{1}";
            const string nonBreakingSpace = " ";
            var expectedProjectSize = $"NOK 100{nonBreakingSpace}000-1{nonBreakingSpace}000{nonBreakingSpace}000";

            var proposalPage = GetProposalPageForTest();
            var applicationType = GetApplicationTypeForTest(proposalPage);
            ArrangeSearch(new List<ApplicationTypeGroup> { applicationType });
            ArrangeTranslation(expectedProjectSizeFormat, "ProjectSizeFormat");

            var result = ((ReactModels.ProposalListPage)_mapper.GetPageViewModel(
                A.Fake<ProposalListPage>(), A.Fake<ProposalListQueryParameter>()).Model).Groups[0].Proposals[0];

            Assert.Equal(expectedProjectSize, result.Metadata.Items[1].Text);
        }

        private static ApplicationTypeGroup GetApplicationTypeForTest(params ProposalBasePage[] proposals) =>
            new ApplicationTypeGroup
            {
                Pages = proposals,
                Title = "application type",
                SubTitle = "application type sub title",
                Text = new XhtmlString()
            };

        private static ProposalPageBuilder GetProposalPageForTest() =>
            ProposalPageBuilder.Create.Default()
                .WithContentLink(new PageReference(2))
                .WithListTitle("hit")
                .WithListIntro("intro")
                .WithCategory(new CategoryList(new List<int> { 1 }))
                .WithId("1")
                .WithDeadline(new DateTime(2018, 11, 22))
                .WithMinApplicationAmount(100000)
                .WithMaxApplicationAmount(1000000)
                .WithMinProjectLength(10)
                .WithMaxProjectLength(20)
                .WithProposalAmount("100000");

        private static InternationalProposalPageBuilder GetInternationalProposalPageForTest() =>
            InternationalProposalPageBuilder.Create.Default()
                .WithContentLink(new PageReference(2))
                .WithListTitle("hit")
                .WithListIntro("intro")
                .WithCategory(new CategoryList(new List<int> { 1 }))
                .WithId("1")
                .WithDeadline(null);

        private void ArrangeTranslation(string expected, string label) =>
            A.CallTo(() => _localizationProvider.GetLabel(nameof(ProposalListPage), label))
                .Returns(expected);

        private void ArrangeSearch(
            List<ApplicationTypeGroup> applicationTypeGroups,
            List<CategoryGroup> categories = null
            )
        {
            var result = new ProposalSearchResult(
                applicationTypeGroups,
                categories ?? new List<CategoryGroup>(),
                applicationTypeGroups.Sum(x => x.Pages.Count()));

            A.CallTo(() => _proposalService.Search(A<ContentReference>._, A<TimeFrameFilter>._, A<string[]>._, A<string[]>._, A<int[]>._, A<string[]>._))
                .Returns(result);
        }
    }
}