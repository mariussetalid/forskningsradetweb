using System.Collections.Generic;
using EPiServer.Core;
using EPiServer.Web.Routing;
using FakeItEasy;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.QueryParameters;
using Forskningsradet.Core.Models.SearchModels;
using Forskningsradet.Core.Services;
using Forskningsradet.Core.Services.Contracts;
using Forskningsradet.Core.ViewModelMappers.Pages;
using Xunit;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.UnitTests.Core.ViewModelMappers.Pages
{
    public class GlossaryPageViewModelMapperTests
    {
        private readonly IGlossaryService _glossaryService;
        private readonly IContentAreaReactModelBuilder _contentAreaReactModelBuilder;
        private readonly ILocalizationProvider _localizationProvider;
        private readonly GlossaryPageViewModelMapper _mapper;

        public GlossaryPageViewModelMapperTests()
        {
            _glossaryService = A.Fake<IGlossaryService>();
            _contentAreaReactModelBuilder = A.Fake<IContentAreaReactModelBuilder>();
            _localizationProvider = A.Fake<ILocalizationProvider>();

            _mapper = new GlossaryPageViewModelMapper(
                _glossaryService,
                _contentAreaReactModelBuilder,
                A.Fake<IUrlResolver>(),
                _localizationProvider);
        }

        [Fact]
        public void GetViewModel_WhenAllFieldsProvided_ShouldMapFields()
        {
            const string expectedLabel = "Label";
            const string expectedLetter = "F";
            const string expectedLetterUrl = "#F";
            var expectedContentArea = new ReactModels.ContentArea();

            var glossaryPage = A.Fake<GlossaryPage>();
            glossaryPage.PageName = "T";
            var wordPage = A.Fake<WordPage>();
            wordPage.PageName = "FakeWord";
            wordPage.MainIntro = "Intro";

            A.CallTo(() => _localizationProvider.GetLabel(A<string>._, A<string>._))
                .Returns(expectedLabel);
            A.CallTo(() => _contentAreaReactModelBuilder.BuildReactModel(A<ContentArea>._, A<string>._, A<RenderOptionQueryParameter>._))
                .Returns(expectedContentArea);
            A.CallTo(() => _glossaryService.GetWordsMatchingQuery(A<GlossaryPage>._, A<string>._))
                .Returns(new GlossarySearchResults(
                    new List<WordPage> { wordPage },
                    new List<string> { "F" },
                    0));

            var result = (ReactModels.GroupedSearchPage)_mapper.GetPageViewModel(glossaryPage, null).Model;

            Assert.Equal(glossaryPage.PageName, result.Title);
            Assert.Equal(expectedLabel, result.Search.Input.Placeholder);
            Assert.Equal(expectedLabel, result.Search.Submit.Text);
            Assert.Equal(expectedLetterUrl, result.Navigation[0].Link.Url);
            Assert.Equal(expectedLetter, result.Navigation[0].Link.Text);
            Assert.Equal(expectedLetter, result.Results[0].Title);
            Assert.Equal(expectedLetter, result.Results[0].HtmlId);
            Assert.Equal(wordPage.PageName, (result.Results[0].Results.Blocks[0].ComponentData as ReactModels.SearchResult)?.Title);
            Assert.Equal(wordPage.MainIntro, (result.Results[0].Results.Blocks[0].ComponentData as ReactModels.SearchResult)?.Text);
            Assert.Same(expectedContentArea, result.FilterLayout.ContentArea);
            Assert.Null(result.Pagination);
            Assert.Null(result.Search.Input.Label);
            Assert.Null(result.Search.Submit.Title);
            Assert.Null(result.ResultsDescription);
            Assert.Null(result.Search.ExternalResultsLabel);
            Assert.Null(result.Search.ExternalResultsEndpoint);
            Assert.Null(result.Search.ResultsDescription);
        }

        [Fact]
        public void GetViewModel_WhenListIsEmpty_ShouldMapEmptyField()
        {
            const string expectedEmptyText = "no results";

            ArrangeTranslation(expectedEmptyText, "EmptyListLabel");

            var result = (ReactModels.GroupedSearchPage)_mapper.GetPageViewModel(A.Fake<GlossaryPage>(), A.Fake<GlossaryQueryParameter>()).Model;

            Assert.NotNull(result.EmptyList);
            Assert.Equal(expectedEmptyText, result.EmptyList.Text);
        }

        [Fact]
        public void GetViewModel_WithQuery_ShouldReturnSearchValue()
        {
            const string expectedQuery = "que";

            var query = new GlossaryQueryParameter { Query = expectedQuery };
            var result = (ReactModels.GroupedSearchPage)_mapper.GetPageViewModel(new GlossaryPage(), query).Model;

            Assert.Equal(expectedQuery, result.Search.Input.Value);
        }

        [Fact]
        public void GetViewModel_WithQuery_ShouldTrimQuery()
        {
            const string expectedQuery = "que";
            var query = new GlossaryQueryParameter { Query = $" {expectedQuery} " };
            var wordPage = A.Fake<WordPage>();
            wordPage.PageName = "F";

            A.CallTo(() => _glossaryService.GetWordsMatchingQuery(A<GlossaryPage>._, A<string>._))
                .Returns(new GlossarySearchResults(new List<WordPage> { wordPage }, new List<string>(), 0));

            var result = (ReactModels.GroupedSearchPage)_mapper.GetPageViewModel(new GlossaryPage(), query).Model;

            Assert.Equal(expectedQuery, result.Search.Input.Value);
        }

        [Fact]
        public void GetViewModel_WithQuery_ShouldReturnMatchingPages()
        {
            const string expectedFirstLetter = "F";
            const string expectedSecondLetter = "S";

            var query = new GlossaryQueryParameter { Query = "q" };
            var wordPage1 = A.Fake<WordPage>();
            wordPage1.PageName = "FirstWordPage";
            var wordPage2 = A.Fake<WordPage>();
            wordPage2.PageName = "SecondWordPage";

            A.CallTo(() => _glossaryService.GetWordsMatchingQuery(A<GlossaryPage>._, A<string>._))
                .Returns(new GlossarySearchResults(
                    new List<WordPage> { wordPage2 },
                    new List<string> { "F", "S" },
                    0));

            var result = (ReactModels.GroupedSearchPage)_mapper.GetPageViewModel(new GlossaryPage(), query).Model;

            Assert.Equal(1, result.Results.Count);
            Assert.Equal(expectedSecondLetter, result.Results[0].Title);
            Assert.Equal(expectedFirstLetter, result.Navigation[0].Link.Text);
            Assert.Equal(expectedSecondLetter, result.Navigation[1].Link.Text);
        }

        [Fact]
        public void GetViewModel_WhenPagesAreReturned_ShouldOnlySetUrlForLettersFoundInResult()
        {
            const string expectedLetterWithUrl = "U";
            const string expectedLetterWithoutUrl = "X";

            var query = new GlossaryQueryParameter { Query = "q" };
            var wordPage1 = A.Fake<WordPage>();
            wordPage1.PageName = "Uu";
            var wordPage2 = A.Fake<WordPage>();
            wordPage2.PageName = "Xx";

            A.CallTo(() => _glossaryService.GetWordsMatchingQuery(A<GlossaryPage>._, A<string>._))
                .Returns(new GlossarySearchResults(
                    new List<WordPage> { wordPage1 },
                    new List<string> { "U", "X" },
                    0));

            var result = (ReactModels.GroupedSearchPage)_mapper.GetPageViewModel(new GlossaryPage(), query).Model;

            Assert.Equal(1, result.Results.Count);
            Assert.Equal(expectedLetterWithUrl, result.Navigation[0].Link.Text);
            Assert.NotNull(result.Navigation[0].Link.Url);
            Assert.Equal(expectedLetterWithoutUrl, result.Navigation[1].Link.Text);
            Assert.Null(result.Navigation[1].Link.Url);
        }

        private void ArrangeTranslation(string expected, string label)
        {
            A.CallTo(() => _localizationProvider.GetLabel(nameof(GlossaryPage), label))
                .Returns(expected);
        }
    }
}