using System;
using System.Collections.Generic;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.Web.Routing;
using FakeItEasy;
using Forskningsradet.Core.Builders;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Extensions;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.QueryParameters;
using Forskningsradet.Core.Models.RequestModels;
using Forskningsradet.Core.Models.SearchModels;
using Forskningsradet.Core.Services;
using Forskningsradet.Core.Services.Contracts;
using Forskningsradet.Core.ViewModelMappers.Pages;
using Xunit;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.UnitTests.Core.ViewModelMappers.Pages
{
    public class CategoryListPageViewModelMapperTests
    {
        private readonly IContentListService _contentListService;
        private readonly IContentLoader _contentLoader;
        private readonly IUrlResolver _urlResolver;
        private readonly FilterLayoutReactModelBuilder _filterLayoutReactModelBuilder;
        private readonly ITagLinkListReactModelBuilder _tagLinkListReactModelBuilder;
        private readonly IFluidImageReactModelBuilder _fluidImageReactModelBuilder;
        private readonly IContentAreaReactModelBuilder _contentAreaReactModelBuilder;
        private readonly IPaginationReactModelBuilder _paginationReactModelBuilder;
        private readonly IContentTypeRepository<PageType> _pageTypeRepository;
        private readonly ISearchApiUrlResolver _searchApiUrlResolver;
        private readonly ICategoryFilterService _categoryFilterService;
        private readonly CategoryRepository _categoryRepository;
        private readonly ICategoryLocalizationProvider _categoryLocalizationProvider;
        private readonly ILocalizationProvider _localizationProvider;
        private readonly CategoryListPageViewModelMapper _mapper;

        public CategoryListPageViewModelMapperTests()
        {
            _contentListService = A.Fake<IContentListService>();
            _contentLoader = A.Fake<IContentLoader>();
            _urlResolver = A.Fake<IUrlResolver>();
            _tagLinkListReactModelBuilder = A.Fake<ITagLinkListReactModelBuilder>();
            _fluidImageReactModelBuilder = A.Fake<IFluidImageReactModelBuilder>();
            _contentAreaReactModelBuilder = A.Fake<IContentAreaReactModelBuilder>();
            _paginationReactModelBuilder = A.Fake<IPaginationReactModelBuilder>();
            _pageTypeRepository = A.Fake<IContentTypeRepository<PageType>>();
            _searchApiUrlResolver = A.Fake<ISearchApiUrlResolver>();
            _categoryFilterService = A.Fake<ICategoryFilterService>();
            _categoryRepository = A.Fake<CategoryRepository>();
            _categoryLocalizationProvider = A.Fake<ICategoryLocalizationProvider>();
            _localizationProvider = A.Fake<ILocalizationProvider>();
            _filterLayoutReactModelBuilder = new FilterLayoutReactModelBuilder(new FilterGroupReactModelBuilder(), _categoryLocalizationProvider, _localizationProvider);

            _mapper = new CategoryListPageViewModelMapper(
                _contentListService,
                _contentLoader,
                _urlResolver,
                _filterLayoutReactModelBuilder,
                _tagLinkListReactModelBuilder,
                _fluidImageReactModelBuilder,
                _contentAreaReactModelBuilder,
                _paginationReactModelBuilder,
                _pageTypeRepository,
                _searchApiUrlResolver,
                _categoryFilterService,
                _categoryRepository,
                _categoryLocalizationProvider,
                _localizationProvider);
        }

        [Fact]
        public void GetViewModel_WhenAllFieldsProvided_ShouldMapFields()
        {
            const string expectedEmptyListText = "emptylist";
            var expectedForm = new ReactModels.Form
            {
                Endpoint = "",
                SubmitButtonText = "submit"
            };

            ArrangeTranslation(expectedEmptyListText, "EmptyCategoryList");
            ArrangeTranslation(expectedForm.SubmitButtonText, "SubmitButtonText");
            A.CallTo(() => _contentListService.Search<EditorialPage>(A<ContentListRequestModel>._))
                .Returns(
                    new ContentListSearchResults(
                        new List<EditorialPage>(),
                        new List<CategoryGroup>(),
                        0));

            var page = A.Fake<CategoryListPage>();
            page.PageName = "T";

            var query = GetDefaultQueryParameters();

            var result = (ReactModels.CategoryListPage)_mapper.GetPageViewModel(page, query).Model;

            Assert.Equal(page.PageName, result.Title);
            Assert.Equal(expectedEmptyListText, result.EmptyList.Text);
            Assert.Equal(expectedForm.Endpoint, result.Form.Endpoint);
            Assert.Equal(expectedForm.SubmitButtonText, result.Form.SubmitButtonText);
        }

        [Fact]
        [UseCulture("nb-NO")]
        public void GetViewModel_WhenPagesAreReturned_ShouldMapGroups()
        {
            const string expectedTitle = "T";
            const string expectedUrl = "U";
            const string expectedText = "Te";
            const string expectedLabel = "L";

            var editorialPage = A.Fake<EditorialPage>();
            editorialPage.ListTitle = expectedTitle;
            editorialPage.ListIntro = expectedText;
            editorialPage.StartPublish = new DateTime(2001, 7, 25);
            string expectedPublishedDate = editorialPage.StartPublish.Value.ToDisplayDate().ToString("dd. MMMM yyyy");

            ArrangeSearch(new List<EditorialPage> { editorialPage });
            ArrangeTranslation(expectedLabel);
            A.CallTo(() => _urlResolver.GetUrl(A<ContentReference>._, A<string>._, A<UrlResolverArguments>._))
                .Returns(expectedUrl);

            var page = A.Fake<CategoryListPage>();
            page.Category = new CategoryList(new List<int> { 1 });

            var result = ((ReactModels.CategoryListPage)_mapper.GetPageViewModel(page, GetDefaultQueryParameters()).Model)
                .Groups[0];

            Assert.Equal(expectedTitle, result.Title.Text);
            Assert.Equal(expectedUrl, result.Title.Url);
            Assert.Equal(expectedText, result.Text);
            Assert.Equal(expectedLabel, result.Published.Type);
            Assert.Equal(expectedPublishedDate, result.Published.Date);
        }

        [Fact]
        public void GetViewModel_WithTagCategoryRoot_ShouldMapTags()
        {
            const string expectedLabel = "L";
            var editorialPage = A.Fake<EditorialPage>();
            editorialPage.Category = new CategoryList(new[] { 1 });

            ArrangeSearch(new List<EditorialPage> { editorialPage });
            ArrangeTranslation(expectedLabel);
            A.CallTo(() => _categoryRepository.Get(A<int>._))
                .Returns(new Category("1", "1") { ID = 1 });

            var page = A.Fake<CategoryListPage>();
            page.TagCategoryRoot = new CategoryList(new[] { 1 });

            var result = ((ReactModels.CategoryListPage)_mapper.GetPageViewModel(page, GetDefaultQueryParameters()).Model)
                .Groups[0]
                .Tags;

            Assert.Equal(expectedLabel, result.Tags[0].Text);
        }

        private void ArrangeSearch(List<EditorialPage> pages)
        {
            var result = new ContentListSearchResults(
                pages,
                new List<CategoryGroup>(),
                pages.Count);

            A.CallTo(() => _contentListService.Search<EditorialPage>(A<ContentListRequestModel>._))
                .Returns(result);
        }

        private CategoryListQueryParameter GetDefaultQueryParameters() =>
            new CategoryListQueryParameter { Page = 1 };

        private void ArrangeTranslation(string label)
        {
            A.CallTo(() => _categoryLocalizationProvider.GetCategoryName(A<int>._))
                .Returns(label);
            A.CallTo(() => _localizationProvider.GetLabel(nameof(CategoryListPage), A<string>._))
                .Returns(label);
        }

        private void ArrangeTranslation(string expected, string label) =>
            A.CallTo(() => _localizationProvider.GetLabel(nameof(CategoryListPage), label))
                .Returns(expected);
    }
}