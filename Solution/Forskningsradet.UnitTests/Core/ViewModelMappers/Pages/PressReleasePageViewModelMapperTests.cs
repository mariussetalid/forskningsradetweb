using System;
using EPiServer.Core;
using EPiServer.Web.Routing;
using FakeItEasy;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Services;
using Forskningsradet.Core.ViewModelMappers.Pages;
using Forskningsradet.UnitTests.Builders;
using Xunit;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.UnitTests.Core.ViewModelMappers.Pages
{
    public class PressReleasePageViewModelMapperTests
    {
        private readonly IUrlResolver _urlResolver;
        private readonly IOptionsModalReactModelBuilder _optionsModalReactModelBuilder;
        private readonly ILocalizationProvider _localizationProvider;
        private readonly PressReleasePageViewModelMapper _mapper;

        public PressReleasePageViewModelMapperTests()
        {
            _urlResolver = A.Fake<IUrlResolver>();
            _optionsModalReactModelBuilder = A.Fake<IOptionsModalReactModelBuilder>();
            _localizationProvider = A.Fake<ILocalizationProvider>();

            _mapper = new PressReleasePageViewModelMapper(
                _urlResolver,
                _optionsModalReactModelBuilder,
                _localizationProvider);

            EPiServer.Globalization.ContentLanguage.PreferredCulture = new System.Globalization.CultureInfo("nb-NO");
        }

        [Fact]
        public void GetPageViewModel_WithDefaultFields_ShouldMapFields()
        {
            const string expectedTitle = "abc";
            var page = (PressReleasePage)PressReleasePageBuilder.Create.Default()
                .WithName("page")
                .WithTitle(expectedTitle);

            var result = (ReactModels.ArticlePage)_mapper.GetPageViewModel(page).Model;

            Assert.Equal(expectedTitle, result.Title);
        }

        [Fact]
        [UseCulture("nb-NO")]
        public void GetPageViewModel_WithDefaultFields_ShouldMapArticleHeader()
        {
            const string expectedPressReleaseLabel = "press release";
            const string publishedLabel = "published";
            const string expectedDownloadLabel = "download";
            const string expectedDownloadUrl = "https://url/Download";

            var expectedComputedPublishedText = $"{publishedLabel} 11 nov 2019";

            ArrangeTranslation(expectedPressReleaseLabel, "PressRelease");
            ArrangeTranslation(publishedLabel, "Published");
            ArrangeTranslation(expectedDownloadLabel, "Download");

            A.CallTo(() => _urlResolver.GetUrl(A<ContentReference>._, A<string>._, A<UrlResolverArguments>._))
                .Returns(expectedDownloadUrl);

            var page = (PressReleasePage)PressReleasePageBuilder.Create.Default()
                .WithExternalPublishedAt(new DateTime(2019, 11, 11));

            var result = ((ReactModels.ArticlePage)_mapper.GetPageViewModel(page).Model).Header;

            Assert.Equal(expectedPressReleaseLabel, result.Byline.Items[0].Text);
            Assert.Equal(expectedComputedPublishedText, result.Byline.Items[1].Text);
            Assert.Equal(expectedDownloadLabel, result.Download.Text);
            Assert.Equal(expectedDownloadUrl, result.Download.Url);
        }

        [Fact]
        public void GetPageViewModel_WithImage_ShouldMapImageAtTopOfPage()
        {
            const string expectedImageUrl = "url";
            const string expectedImageCaption = "caption";

            var page = (PressReleasePage)PressReleasePageBuilder.Create.Default()
                .WithImageUrl(expectedImageUrl)
                .WithImageCaption(expectedImageCaption);

            var result = (ReactModels.ImageWithCaption)((ReactModels.ArticlePage)_mapper.GetPageViewModel(page).Model)
                .Content
                .Blocks[0]
                .ComponentData;

            Assert.Equal(expectedImageCaption, result.Caption);
            Assert.Equal(expectedImageCaption, result.Image.Alt);
            Assert.Equal(expectedImageUrl, result.Image.Src);
        }

        private void ArrangeTranslation(string label, string key) =>
            A.CallTo(() => _localizationProvider.GetLabel(nameof(PressReleaseListPage), A<string>.That.Matches(x => x == key)))
                .Returns(label);
    }
}