using System.Collections.Generic;
using EPiServer;
using EPiServer.Core;
using EPiServer.Web.Routing;
using FakeItEasy;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Configuration;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Repositories.Contracts;
using Forskningsradet.Core.Services;
using Forskningsradet.Core.Services.Contracts;
using Forskningsradet.Core.ViewModelMappers.Layout;
using Forskningsradet.UnitTests.Builders;
using Xunit;

namespace Forskningsradet.UnitTests.Core.ViewModelMappers.Pages
{
    public class SatelliteFrontPageLayoutViewModelMapperTests
    {
        private readonly ILocalizationProvider _localizationProvider;
        private readonly IContentRepository _contentRepository;
        private readonly SatelliteFrontPageLayoutViewModelMapper _mapper;

        public SatelliteFrontPageLayoutViewModelMapperTests()
        {
            var urlResolver = A.Fake<IUrlResolver>();
            var urlCheckingService = A.Fake<IUrlCheckingService>();
            var pageStructureService = A.Fake<IPageStructureService>();
            var contentLoader = A.Fake<IContentLoader>();
            var contentFilterService = A.Fake<IContentFilterService>();
            var footerReactModelBuilder = A.Fake<IFooterReactModelBuilder>();
            var breadcrumbReactModelBuilder = A.Fake<IBreadcrumbsReactModelBuilder>();
            var googleTagManagerConfiguration = A.Fake<IGoogleTagManagerConfiguration>();
            _localizationProvider = A.Fake<ILocalizationProvider>();
            var pageRepository = A.Fake<IPageRepository>();
            var messageReactModelBuilder = A.Fake<IMessageReactModelBuilder>();
            var publishedStateAssessor = A.Fake<IPublishedStateAssessor>();
            _contentRepository = A.Fake<IContentRepository>();

            _mapper = new SatelliteFrontPageLayoutViewModelMapper(
                urlResolver,
                urlCheckingService,
                pageStructureService,
                contentLoader,
                contentFilterService,
                footerReactModelBuilder,
                breadcrumbReactModelBuilder,
                googleTagManagerConfiguration,
                _localizationProvider,
                pageRepository,
                messageReactModelBuilder,
                _contentRepository,
                publishedStateAssessor);
        }

        [Fact]
        public void GetViewModel_WhenAllFieldsProvided_ShouldMapFields()
        {
            const string expectedHomeLabel = "home";
            const string expectedSearchLabel = "s";
            const string expectedSearchPlaceholderLabel = "sph";
            const string expectedCloseSearchButtonLabel = "csb";
            const string expectedMenuLabel = "men";
            const string expectedOpenMenuLabel = "om";
            const string expectedGoToContentLabel = "gtc";
            const string expectedCloseButtonLabel = "cb";
            const string expectedPlanetLabel = "planet";

            ArrangeTranslation(expectedHomeLabel, "HomeButtonLabel");
            ArrangeTranslation(expectedSearchLabel, "Search");
            ArrangeTranslation(expectedSearchPlaceholderLabel, "SearchPlaceholder");
            ArrangeTranslation(expectedCloseSearchButtonLabel, "CloseSearchButtonLabel");
            ArrangeTranslation(expectedMenuLabel, "MenuLabel");
            ArrangeTranslation(expectedOpenMenuLabel, "OpenMenu");
            ArrangeTranslation(expectedGoToContentLabel, "GoToContent");
            ArrangeTranslation(expectedCloseButtonLabel, "CloseButtonText");
            ArrangeTranslation(expectedPlanetLabel, "PlanetName");

            var frontPage = (SatelliteFrontPage)SatelliteFrontPageBuilder.Create.Default()
                .WithSearchPage(new PageReference(2))
                .WithMainMenuRoot(new PageReference(3))
                .WithShowTopBanner(true);

            var result = _mapper.GetViewModel(frontPage, new PageReference(1));

            Assert.Equal(expectedSearchLabel, result.Header.GlobalSearch.Button.Text);
            Assert.Equal(expectedSearchPlaceholderLabel, result.Header.GlobalSearch.Input.Placeholder);
            Assert.Equal(QueryParameters.SearchQuery, result.Header.GlobalSearch.Input.Name);
            Assert.Equal(expectedCloseSearchButtonLabel, result.Header.GlobalSearch.CloseButtonText);

            Assert.Equal(expectedHomeLabel, result.Header.LinkToHome.Text);

            Assert.Equal(expectedMenuLabel, result.Header.MenuText);
            Assert.Equal(expectedCloseButtonLabel, result.Header.Menu.CloseButtonText);
            Assert.Equal(expectedOpenMenuLabel, result.Header.TabMenu.OpenMenuLabel);
            Assert.Equal(expectedGoToContentLabel, result.Header.TabMenu.MainContentLink.Text);
            Assert.Equal("#main", result.Header.TabMenu.MainContentLink.Url);
            Assert.Null(result.Header.Logo);

            Assert.Equal(expectedPlanetLabel, result.Header.Satellite.Header.Text);
            Assert.True(result.Header.Satellite.IsVisible);
        }

        [Fact]
        public void GetViewModel_WithLoginLink_ShouldMapPriorityLink()
        {
            const string expectedLoginLabel = "log";
            ArrangeTranslation(expectedLoginLabel, "Login");

            var frontPage = (SatelliteFrontPage)SatelliteFrontPageBuilder.Create.Default()
                .WithLoginUrl(new Url("/log"));
            var result = _mapper.GetViewModel(frontPage, new PageReference(1));

            Assert.Equal(expectedLoginLabel, result.Header.PriorityLink.Text);
        }

        [Fact]
        [UseCulture("nb-NO")]
        private void GetLanguageLabel_WhenPageHasSingleLanguage_ShouldNotMapLanguageLabels()
        {
            var singleLanguageBranch = new List<PageData>
            {
                new PageData()
            };
            var languageLabel = "lang";
            ArrangeTranslation(languageLabel, "English");
            A.CallTo(() => _contentRepository.GetLanguageBranches<PageData>(A<PageReference>._))
                .Returns(singleLanguageBranch);

            var result = _mapper.GetViewModel(SatelliteFrontPageBuilder.Create.Default(), new PageReference(1));

            Assert.DoesNotContain(result.Header.LinkList, x => x.Text == languageLabel);

        }

        private void ArrangeTranslation(string expected, string label)
        {
            A.CallTo(() => _localizationProvider.GetLabel(nameof(FrontPage), label))
                .Returns(expected);
        }
    }
}