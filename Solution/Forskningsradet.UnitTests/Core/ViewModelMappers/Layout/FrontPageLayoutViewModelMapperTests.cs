using System.Collections.Generic;
using System.Globalization;
using EPiServer;
using EPiServer.Core;
using EPiServer.Web.Routing;
using FakeItEasy;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Configuration;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Services;
using Forskningsradet.Core.Services.Contracts;
using Forskningsradet.Core.ViewModelMappers.Layout;
using Forskningsradet.UnitTests.Builders;
using Xunit;

namespace Forskningsradet.UnitTests.Core.ViewModelMappers.Layout
{
    public class FrontPageLayoutViewModelMapperTests
    {
        private readonly ILocalizationProvider _localizationProvider;
        private readonly IContentRepository _contentRepository;
        private readonly IPublishedStateAssessor _publishedStateAssessor;
        private readonly FrontPageLayoutViewModelMapper _mapper;

        public FrontPageLayoutViewModelMapperTests()
        {
            var urlResolver = A.Fake<IUrlResolver>();
            var urlCheckingService = A.Fake<IUrlCheckingService>();
            var pageStructureService = A.Fake<IPageStructureService>();
            var contentLoader = A.Fake<IContentLoader>();
            var contentFilterService = A.Fake<IContentFilterService>();
            var footerReactModelBuilder = A.Fake<IFooterReactModelBuilder>();
            var breadcrumbReactModelBuilder = A.Fake<IBreadcrumbsReactModelBuilder>();
            var googleTagManagerConfiguration = A.Fake<IGoogleTagManagerConfiguration>();
            var messageReactModelBuilder = A.Fake<IMessageReactModelBuilder>();
            _localizationProvider = A.Fake<ILocalizationProvider>();
            _contentRepository = A.Fake<IContentRepository>();
            _publishedStateAssessor = A.Fake<IPublishedStateAssessor>();

            _mapper = new FrontPageLayoutViewModelMapper(
                urlResolver,
                urlCheckingService,
                pageStructureService,
                contentLoader,
                contentFilterService,
                footerReactModelBuilder,
                breadcrumbReactModelBuilder,
                googleTagManagerConfiguration,
                _localizationProvider,
                messageReactModelBuilder,
                _contentRepository,
                _publishedStateAssessor);
        }

        [Fact]
        public void GetViewModel_WhenAllFieldsProvided_ShouldMapFields()
        {
            const string expectedHomeLabel = "home";
            const string expectedSearchLabel = "s";
            const string expectedSearchPlaceholderLabel = "sph";
            const string expectedCloseSearchButtonLabel = "csb";
            const string expectedMenuLabel = "men";
            const string expectedOpenMenuLabel = "om";
            const string expectedGoToContentLabel = "gtc";
            const string expectedCloseButtonLabel = "cb";

            ArrangeTranslation(expectedHomeLabel, "HomeButtonLabel");
            ArrangeTranslation(expectedSearchLabel, "Search");
            ArrangeTranslation(expectedSearchPlaceholderLabel, "SearchPlaceholder");
            ArrangeTranslation(expectedCloseSearchButtonLabel, "CloseSearchButtonLabel");
            ArrangeTranslation(expectedMenuLabel, "MenuLabel");
            ArrangeTranslation(expectedOpenMenuLabel, "OpenMenu");
            ArrangeTranslation(expectedGoToContentLabel, "GoToContent");
            ArrangeTranslation(expectedCloseButtonLabel, "CloseButtonText");

            var frontPage = (FrontPage)FrontPageBuilder.Create.Default()
                .WithSearchPage(new PageReference(2))
                .WithMainMenuRoot(new PageReference(3));

            var result = _mapper.GetViewModel(frontPage, new PageReference(1));

            Assert.Equal(expectedSearchLabel, result.Header.GlobalSearch.Button.Text);
            Assert.Equal(expectedSearchPlaceholderLabel, result.Header.GlobalSearch.Input.Placeholder);
            Assert.Equal(QueryParameters.SearchQuery, result.Header.GlobalSearch.Input.Name);
            Assert.Equal(expectedCloseSearchButtonLabel, result.Header.GlobalSearch.CloseButtonText);

            Assert.Equal(expectedHomeLabel, result.Header.LinkToHome.Text);
            Assert.Equal(expectedMenuLabel, result.Header.MenuText);
            Assert.Equal(expectedCloseButtonLabel, result.Header.Menu.CloseButtonText);
            Assert.Equal(expectedOpenMenuLabel, result.Header.TabMenu.OpenMenuLabel);
            Assert.Equal(expectedGoToContentLabel, result.Header.TabMenu.MainContentLink.Text);
            Assert.Equal("#main", result.Header.TabMenu.MainContentLink.Url);
            Assert.Null(result.Header.Logo);

            Assert.Null(result.Header.Satellite);
        }

        [Fact]
        public void GetViewModel_WithLoginLink_ShouldMapPriorityLink()
        {
            const string expectedLoginLabel = "log";
            ArrangeTranslation(expectedLoginLabel, "Login");

            var frontPage = (FrontPage)FrontPageBuilder.Create.Default()
                .WithLoginUrl(new Url("/log"));
            var result = _mapper.GetViewModel(frontPage, new PageReference(1));

            Assert.Equal(expectedLoginLabel, result.Header.PriorityLink.Text);
        }

        [Fact]
        [UseCulture("no")]
        private void GetLanguageLabel_WhenPageHasMultiplePublishedLanguages_ShouldMapLanguageLabels()
        {
            var multipleLanguageBranches = new List<PageData>
            {
                CreatePublishedPage("no"),
                CreatePublishedPage("en")
            };
            var expectedLanguageLabel = "lang";
            ArrangeTranslation(expectedLanguageLabel, "English");
            A.CallTo(() => _contentRepository.GetLanguageBranches<PageData>(A<PageReference>._))
                .Returns(multipleLanguageBranches);

            var result = _mapper.GetViewModel((FrontPage)FrontPageBuilder.Create.Default(), new PageReference(1));

            Assert.Contains(result.Header.LinkList, x => x.Text == expectedLanguageLabel);
        }

        [Fact]
        [UseCulture("no")]
        private void GetLanguageLabel_WhenPageHasSingleLanguage_ShouldNotMapLanguageLabels()
        {
            var singleLanguageBranch = new List<PageData>
            {
                CreatePublishedPage("no"),
            };
            var languageLabel = "lang";
            ArrangeTranslation(languageLabel, "English");
            A.CallTo(() => _contentRepository.GetLanguageBranches<PageData>(A<PageReference>._))
                .Returns(singleLanguageBranch);

            var result = _mapper.GetViewModel(FrontPageBuilder.Create.Default(), new PageReference(1));

            Assert.DoesNotContain(result.Header.LinkList, x => x.Text == languageLabel);
        }

        [Fact]
        [UseCulture("no")]
        private void GetLanguageLabel_WhenPageHasSinglePublishedLanguage_ShouldNotMapLanguageLabels()
        {
            var multipleLanguageBranches = new List<PageData>
            {
                CreatePublishedPage("no"),
                CreateNotPublishedPage("en")
            };
            var englishLanguageLabel = "lang";
            ArrangeTranslation(englishLanguageLabel, "English");
            A.CallTo(() => _contentRepository.GetLanguageBranches<PageData>(A<PageReference>._))
                .Returns(multipleLanguageBranches);

            var result = _mapper.GetViewModel(FrontPageBuilder.Create.Default(), new PageReference(1));

            Assert.DoesNotContain(result.Header.LinkList, x => x.Text == englishLanguageLabel);
        }

        private void ArrangeTranslation(string expected, string label)
        {
            A.CallTo(() => _localizationProvider.GetLabel(nameof(FrontPage), label))
                .Returns(expected);
        }

        private PageData CreatePublishedPage(string language)
        {
            var page = CreateNotPublishedPage(language);
            A.CallTo(() => _publishedStateAssessor.IsPublished(page, PublishedStateCondition.None)).Returns(true);
            return page;
        }

        private PageData CreateNotPublishedPage(string language)
        {
            var page = A.Fake<PageData>();
            A.CallTo(() => page.Language).Returns(new CultureInfo(language));
            return page;
        }
    }
}