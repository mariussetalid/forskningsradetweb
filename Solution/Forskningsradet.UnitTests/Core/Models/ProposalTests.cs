using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Models.ApiModels.Evurdering;
using Xunit;

namespace Forskningsradet.UnitTests.Core.Models
{
    public class ProposalTests
    {
        [Fact]
        public void GetTitle_WithNorwegianCulture_ShouldReturnTitle()
        {
            var proposal = new Proposal
            {
                Title = "NO title",
                EnglishTitle = "EN title"
            };

            var result = proposal.GetTitle(LanguageConstants.NorwegianLanguageBranch);
            
            Assert.Equal(proposal.Title, result);
        }
        
        [Fact]
        public void GetTitle_WithNorwegianCultureAndEmptyTitle_ShouldReturnEnglishTitle()
        {
            var proposal = new Proposal
            {
                Title = "",
                EnglishTitle = "EN title"
            };

            var result = proposal.GetTitle(LanguageConstants.NorwegianLanguageBranch);
            
            Assert.Equal(proposal.EnglishTitle, result);
        }
        
        [Fact]
        public void GetTitle_WithEnglishCulture_ShouldReturnEnglishTitle()
        {
            var proposal = new Proposal
            {
                Title = "NO title",
                EnglishTitle = "EN title"
            };

            var result = proposal.GetTitle(LanguageConstants.EnglishLanguageBranch);
            
            Assert.Equal(proposal.EnglishTitle, result);
        }
    }
}