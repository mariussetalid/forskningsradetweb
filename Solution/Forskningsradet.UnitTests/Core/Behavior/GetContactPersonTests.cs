using System;
using System.Collections.Generic;
using System.Linq;
using EPiServer;
using EPiServer.Core;
using FakeItEasy;
using Forskningsradet.Core.Behavior;
using Forskningsradet.Core.Models.ApiModels.Idm;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Repositories.Contracts;
using Xunit;

namespace Forskningsradet.UnitTests.Core.Behavior
{
    public class GetContactPersonTests
    {
        private readonly IContentRepository _contentRepository;
        private readonly IPageRepository _pageRepository;
        private readonly GetContactPersonQueryHandlerForTest _handler;

        public GetContactPersonTests()
        {
            _contentRepository = A.Fake<IContentRepository>();
            _pageRepository = A.Fake<IPageRepository>();
            _handler = new GetContactPersonQueryHandlerForTest(_contentRepository, _pageRepository);
        }
        
        [Fact]
        public void Handle_WhenContactPersonIsFound_ShouldMapPageToContract()
        {
            var expectedId = Guid.NewGuid();
            
            var personPage = A.Fake<PersonPage>();
            personPage.ExternalId = expectedId.ToString();
            personPage.FirstName = "first";
            personPage.LastName = "last";
            personPage.JobTitle = "title";
            personPage.Department = "dept";
            personPage.Mobile = "123";
            personPage.Phone = "321";
            personPage.Email = "a@b.cd";
            personPage.ResourceType = "A";
            
            A.CallTo(() => _contentRepository.GetChildren<PersonPage>(A<ContentReference>._))
                .Returns(new List<PersonPage> {personPage});

            var response = _handler.HandleQuery(new GetContactPerson.Query {FilterPublished = false, Id = new IdmContactPersonId(expectedId.ToString()) });
            
            Assert.Equal(expectedId, response.Id);
            Assert.Equal(personPage.FirstName, response.FirstName);
            Assert.Equal(personPage.LastName, response.LastName);
            Assert.Equal(personPage.JobTitle, response.Title);
            Assert.Equal(personPage.Department, response.DepartmentName);
            Assert.Equal(personPage.Mobile, response.Mobile);
            Assert.Equal(personPage.Phone, response.Phone);
            Assert.Equal(personPage.Email, response.Email);
            Assert.Equal(personPage.ResourceType, response.ResourceType);
        }
        
        [Fact]
        public void Handle_WhenFilterPublishIsTrueAndAllPagesAreFilteredOut_ShouldReturnNull()
        {
            var expectedId = Guid.NewGuid();
            
            var personPage = A.Fake<PersonPage>();
            personPage.ExternalId = expectedId.ToString();
            
            var personPages = new List<PersonPage> {personPage};
            
            A.CallTo(() => _contentRepository.GetChildren<PersonPage>(A<ContentReference>._))
                .Returns(personPages);
            
            A.CallTo(() => _pageRepository.FilterPublished(personPages))
                .Returns(Enumerable.Empty<PersonPage>());
            
            var response = _handler.HandleQuery(new GetContactPerson.Query {Id = new IdmContactPersonId(expectedId.ToString()) });
            
            Assert.Null(response);
        }
        
        [Fact]
        public void Handle_WhenFilterPublishIsTrueAndPagesAreNotFilteredOut_ShouldReturnMatch()
        {
            var expectedId = Guid.NewGuid();
            
            var personPage = A.Fake<PersonPage>();
            personPage.ExternalId = expectedId.ToString();

            var anotherPage = A.Fake<PersonPage>();
            anotherPage.ExternalId = "another id";
            
            var personPages = new List<PersonPage> {personPage, anotherPage};
            
            A.CallTo(() => _contentRepository.GetChildren<PersonPage>(A<ContentReference>._))
                .Returns(personPages);
            
            A.CallTo(() => _pageRepository.FilterPublished(personPages))
                .Returns(personPages);
            
            var response = _handler.HandleQuery(new GetContactPerson.Query {Id = new IdmContactPersonId(expectedId.ToString())});
            
            Assert.Equal(expectedId, response.Id);
        }
        
        private class GetContactPersonQueryHandlerForTest : GetContactPerson.Handler
        {
            public GetContactPersonQueryHandlerForTest(IContentRepository contentRepository, IPageRepository pageRepository)
                : base(contentRepository, pageRepository) {}

            public IdmContactPerson HandleQuery(GetContactPerson.Query query)
                => base.Handle(query);
        }
    }
}