using System;
using FakeItEasy;
using Forskningsradet.Common.Logging;
using Forskningsradet.Core.Behavior;
using Forskningsradet.Core.Models.ApiModels.Evurdering;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Processes;
using Forskningsradet.Core.Processes.Contexts;
using Forskningsradet.Core.Services.Contracts;
using Xunit;

namespace Forskningsradet.UnitTests.Core.Behavior
{
    public class CreateOrUpdateProposalTests
    {
        private readonly CreateOrUpdateProposalCommandHandlerForTest _handler;
        private readonly IProcess _process;
        private readonly IProposalPageProvider _proposalPageProvider;

        public CreateOrUpdateProposalTests()
        {
            var processFactory = A.Fake<IProcessFactory>();
            _process = A.Fake<IProcess>();
            A.CallTo(() => processFactory.GetProcessForContext(A<ProposalContext>._))
                .Returns(_process);
            
            _proposalPageProvider = A.Fake<IProposalPageProvider>();
            _handler = new CreateOrUpdateProposalCommandHandlerForTest(_proposalPageProvider, processFactory, A.Fake<IIntegrationLogger>());
        }
        
        [Fact]
        public void WithProposal_ShouldRunUpdateProcess()
        {
            var command = new CreateOrUpdateProposal.Command
            {
                Proposal = new Proposal {Title = "title"}
            };
            
            _handler.HandleCommand(command);
            
            A.CallTo(() => _process.Run())
                .MustHaveHappenedOnceExactly();
        }
        
        [Fact]
        public void WhenProposalHasEnglishTitle_ShouldRunUpdateProcessForEnglishVersion()
        {
            var command = new CreateOrUpdateProposal.Command
            {
                Proposal = new Proposal {Title = "title", EnglishTitle = "en title"}
            };
            
            _handler.HandleCommand(command);
            
            A.CallTo(() => _process.Run())
                .MustHaveHappenedTwiceExactly();
        }

        [Fact]
        public void WhenNewerProposalExists_ShouldThrowArgumentException()
        {
            var command = new CreateOrUpdateProposal.Command
            {
                Proposal = new Proposal {Title = "title", EnglishTitle = "en title"},
                OriginalMessageTimestamp = new DateTime(2018, 3, 25)
            };

            var proposalPage = A.Fake<ProposalPage>();
            proposalPage.UpdatedAt = new DateTime(2018, 3, 26);
            A.CallTo(() => _proposalPageProvider.GetProposalPage(command.Proposal, A<string>._))
                .Returns(proposalPage);
            
            void Action() => _handler.HandleCommand(command);
            Assert.Throws<ArgumentException>((Action) Action);
        }
        
        private class CreateOrUpdateProposalCommandHandlerForTest : CreateOrUpdateProposal.Handler
        {
            public CreateOrUpdateProposalCommandHandlerForTest(
                IProposalPageProvider proposalPageProvider, 
                IProcessFactory processFactory,
                IIntegrationLogger logger)
                : base(proposalPageProvider, processFactory, logger) { }
            
            public void HandleCommand(CreateOrUpdateProposal.Command command)
                => base.Handle(command);
        }
    }
}