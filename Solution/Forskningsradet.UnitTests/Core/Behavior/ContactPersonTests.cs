using System;
using System.Collections.Generic;
using System.Globalization;
using EPiServer;
using EPiServer.Core;
using EPiServer.Shell.Configuration;
using FakeItEasy;
using Forskningsradet.Core.Behavior;
using Forskningsradet.Core.Models.ApiModels.Idm;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Repositories.Contracts;
using Xunit;

namespace Forskningsradet.UnitTests.Core.Behavior
{
    public class ContactPersonTests
    {
        private readonly IPageRepository _pageRepository;
        private readonly IContentRepository _contentRepository;

        public ContactPersonTests()
        {
            _pageRepository = A.Fake<IPageRepository>();
            _contentRepository = A.Fake<IContentRepository>();
        }
        
        [Fact]
        public void Handle_CreateCommand_ShouldCreateAndSavePage()
        {
            const string norwegianLanguageBranch = "no";
            var newPage = A.Fake<PersonPage>();
            newPage.Language = new CultureInfo(norwegianLanguageBranch);
            var contactPerson = GetContactPersonForTest();
            var frontPage = A.Fake<FrontPage>();
            frontPage.ContactPersonsPage = new PageReference(1);

            A.CallTo(() => _pageRepository.GetCurrentStartPage())
                .Returns(frontPage);

            A.CallTo(() => _pageRepository.Create<PersonPage>(A<ContentReference>._, A<CultureInfo>._))
                .Returns(newPage);

            var handler = new CreateContactPersonCommandHandlerForTest(_pageRepository);
            
            handler.HandleCommand(new CreateContactPerson.Command
            {
                ContactPerson = contactPerson
            });
            
            A.CallTo(() => _pageRepository.Save(
                    A<PersonPage>.That.Matches(p =>
                           p.FirstName == contactPerson.FirstName
                        && p.LastName == contactPerson.LastName
                        && p.JobTitle == contactPerson.Title
                        && p.Department == contactPerson.DepartmentName
                        && p.ResourceType == contactPerson.ResourceType
                        && p.Mobile == contactPerson.Mobile
                        && p.Phone == contactPerson.Phone
                        && p.Email == contactPerson.Email
                        && p.Language.Name == norwegianLanguageBranch
                    )))
                .MustHaveHappened();
        }

        [Fact]
        public void Handle_CreateCommand_ShouldCreateTranslatedVersion()
        {
            const string englishLanguageBranch = "en";
            var page = A.Fake<PersonPage>();
            page.Language = new CultureInfo(englishLanguageBranch);
            page.FirstName = "first";
            page.LastName = "last";
            page.JobTitle = "title";
            page.Department = "dept";
            page.ResourceType = "type";
            page.Mobile = "123";
            page.Phone = "321";
            page.Email = "a@b.cd";

            A.CallTo(() =>_pageRepository.CreateLanguageBranch<PersonPage>(A<ContentReference>._, englishLanguageBranch))
                .Returns(page);
            
            var frontPage = A.Fake<FrontPage>();
            frontPage.ContactPersonsPage = new PageReference(1);

            A.CallTo(() => _pageRepository.GetCurrentStartPage())
                .Returns(frontPage);
            
            var command = new CreateContactPerson.Command {ContactPerson = GetContactPersonForTest()};
            var handler = new CreateContactPersonCommandHandlerForTest(_pageRepository);
            
            handler.HandleCommand(command);
            
            A.CallTo(() => _pageRepository.Save(
                    A<PersonPage>.That.Matches(p =>
                           p.FirstName == page.FirstName
                        && p.LastName == page.LastName
                        && p.JobTitle == string.Empty
                        && p.Department == string.Empty
                        && p.ResourceType == page.ResourceType
                        && p.Mobile == page.Mobile
                        && p.Phone == page.Phone
                        && p.Email == page.Email
                        && p.Language.Name == englishLanguageBranch
                    )))
                .MustHaveHappened();
        }

        [Fact]
        public void Handle_CreateCommandWithMissingConfiguration_ShouldThrowException()
        {
            var command = new CreateContactPerson.Command {ContactPerson = GetContactPersonForTest()};
            var handler = new CreateContactPersonCommandHandlerForTest(_pageRepository);

            Assert.Throws<MissingConfigurationException>(() => handler.HandleCommand(command));
        }

        [Fact]
        public void Handle_UpdateCommandWithChanges_ShouldUpdatePage()
        {
            var contactPerson = GetContactPersonForTest();

            var personPage = new ClonablePersonPage
            {
                ExternalId = contactPerson.Id.ToString(),
                FirstName = contactPerson.FirstName,
                LastName = contactPerson.LastName,
                JobTitle = "changed!",
                Department = contactPerson.DepartmentName,
                ResourceType = contactPerson.ResourceType,
                Mobile = contactPerson.Mobile,
                Phone = contactPerson.Phone,
                Email = contactPerson.Email
            };

            A.CallTo(() => _contentRepository.GetChildren<PersonPage>(A<ContentReference>._))
                .Returns(new List<PersonPage> {personPage});

            A.CallTo(() => _contentRepository.Get<PersonPage>(A<ContentReference>._, A<CultureInfo>._))
                .Returns(new ClonablePersonPage());
            
            var command = new UpdateContactPerson.Command{ContactPerson = contactPerson};
            var handler = new UpdateContactPersonCommandHandlerForTest(_pageRepository, _contentRepository);
            
            handler.HandleCommand(command);
            
            A.CallTo(() => _pageRepository.Save(
                A<PersonPage>.That.Matches(p =>
                       p.FirstName == contactPerson.FirstName
                    && p.LastName == contactPerson.LastName
                    && p.JobTitle == contactPerson.Title
                    && p.Department == contactPerson.DepartmentName
                    && p.ResourceType == contactPerson.ResourceType
                    && p.Mobile == contactPerson.Mobile
                    && p.Phone == contactPerson.Phone
                    && p.Email == contactPerson.Email
                ))).MustHaveHappened();
        }
        
        [Fact]
        public void Handle_UpdateCommandWithChanges_ShouldUpdateEnglishVersion()
        {
            var contactPerson = GetContactPersonForTest();

            var personPage = new ClonablePersonPage
            {
                ExternalId = contactPerson.Id.ToString(),
                ResourceId = contactPerson.ResourceId,
                FirstName = contactPerson.FirstName,
                LastName = contactPerson.LastName,
                JobTitle = "changed!",
                Department = "changed!",
                ResourceType = "changed!",
                Mobile = contactPerson.Mobile,
                Phone = contactPerson.Phone,
                Email = contactPerson.Email,
                ExternalLastUpdate = contactPerson.AgressoLastUpdate,
            };

            A.CallTo(() => _contentRepository.GetChildren<PersonPage>(A<ContentReference>._))
                .Returns(new List<PersonPage> {personPage});

            A.CallTo(() => _contentRepository.Get<PersonPage>(A<ContentReference>._, A<CultureInfo>._))
                .Returns(personPage);
            
            var command = new UpdateContactPerson.Command{ContactPerson = contactPerson};
            var handler = new UpdateContactPersonCommandHandlerForTest(_pageRepository, _contentRepository);
            
            handler.HandleCommand(command);
            
            A.CallTo(() => _pageRepository.Save(
                A<PersonPage>.That.Matches(p =>
                    p.FirstName == contactPerson.FirstName
                    && p.LastName == contactPerson.LastName
                    && p.JobTitle == string.Empty
                    && p.Department == string.Empty
                    && p.ResourceType == contactPerson.ResourceType
                    && p.Mobile == contactPerson.Mobile
                    && p.Phone == contactPerson.Phone
                    && p.Email == contactPerson.Email
                    && p.ExternalLastUpdate == contactPerson.AgressoLastUpdate
                ))).MustHaveHappened();
        }

        [Fact]
        public void Handle_UpdateCommandWithoutChanges_ShouldNotUpdatePage()
        {
            var contactPerson = GetContactPersonForTest();

            var personPage = A.Fake<PersonPage>();
            personPage.ExternalId = contactPerson.Id.ToString();
            personPage.ResourceId = contactPerson.ResourceId;
            personPage.FirstName = contactPerson.FirstName;
            personPage.LastName = contactPerson.LastName;
            personPage.JobTitle = contactPerson.Title;
            personPage.Department = contactPerson.DepartmentName;
            personPage.ResourceType = contactPerson.ResourceType;
            personPage.Mobile = contactPerson.Mobile;
            personPage.Phone = contactPerson.Phone;
            personPage.Email = contactPerson.Email;
            personPage.ExternalLastUpdate = contactPerson.AgressoLastUpdate;

            A.CallTo(() => _contentRepository.GetChildren<PersonPage>(A<ContentReference>._))
                .Returns(new List<PersonPage>
                {
                    personPage
                });
            
            var command = new UpdateContactPerson.Command{ContactPerson = contactPerson};
            var handler = new UpdateContactPersonCommandHandlerForTest(_pageRepository, _contentRepository);
            
            handler.HandleCommand(command);
            
            A.CallTo(() => _pageRepository.Save(A<PageData>._)).MustNotHaveHappened();
        }
        
        [Fact]
        public void Handle_UpdateCommandOnNonExistingPage_ShouldDoNothing()
        {
            A.CallTo(() => _contentRepository.GetChildren<PersonPage>(A<ContentReference>._))
                .Returns(new List<PersonPage>());
            
            var command = new UpdateContactPerson.Command {ContactPerson = GetContactPersonForTest()};
            var handler = new UpdateContactPersonCommandHandlerForTest(_pageRepository, _contentRepository);
            
            handler.HandleCommand(command);
            
            A.CallTo(() => _pageRepository.Save(A<PersonPage>._)).MustNotHaveHappened();
        }

        [Fact]
        public void Handle_DeleteCommandWithGuid_ShouldDeletePage()
        {
            var pageId = Guid.NewGuid();
            var personPage = A.Fake<PersonPage>();
            personPage.PageLink = new PageReference();
            personPage.ExternalId = pageId.ToString();

            A.CallTo(() => _contentRepository.GetChildren<PersonPage>(A<ContentReference>._))
                .Returns(new List<PersonPage> {personPage});
            
            var command = new DeleteContactPerson.Command {Id = new IdmContactPersonId(pageId.ToString()) };
            var handler = new DeleteContactPersonCommandHandlerForTest(_pageRepository, _contentRepository);
            
            handler.HandleCommand(command);

            A.CallTo(() => _pageRepository.UnPublishAllLanguageBranches<PersonPage>(personPage.PageLink)).MustHaveHappened();
        }

        [Fact]
        public void Handle_DeleteCommandWithResourceId_ShouldDeletePage()
        {
            var pageId = 1;
            var personPage = A.Fake<PersonPage>();
            personPage.PageLink = new PageReference();
            personPage.ResourceId = pageId.ToString();

            A.CallTo(() => _contentRepository.GetChildren<PersonPage>(A<ContentReference>._))
                .Returns(new List<PersonPage> { personPage });

            var command = new DeleteContactPerson.Command { Id = new IdmContactPersonId(pageId.ToString()) };
            var handler = new DeleteContactPersonCommandHandlerForTest(_pageRepository, _contentRepository);

            handler.HandleCommand(command);

            A.CallTo(() => _pageRepository.UnPublishAllLanguageBranches<PersonPage>(personPage.PageLink)).MustHaveHappened();
        }

        [Fact]
        public void Handle_DeleteCommandOnNonExistingPage_ShouldDoNothing()
        {
            A.CallTo(() => _contentRepository.GetChildren<PersonPage>(A<ContentReference>._))
                .Returns(new List<PersonPage>());
            
            var command = new DeleteContactPerson.Command {Id = new IdmContactPersonId(Guid.NewGuid().ToString())};
            var handler = new DeleteContactPersonCommandHandlerForTest(_pageRepository, _contentRepository);
            
            handler.HandleCommand(command);
            
            A.CallTo(() => _pageRepository.UnPublish(A<PersonPage>._)).MustNotHaveHappened();
        }
        
        private static IdmContactPerson GetContactPersonForTest()
        {
            var contactPerson = new IdmContactPerson
            {
                Id = Guid.Empty,
                ResourceId = "1",
                FirstName = "first",
                LastName = "last",
                Title = "title",
                DepartmentName = "dept",
                ResourceType = "resource",
                Mobile = "mob",
                Phone = "phone",
                Email = "mail",
                AgressoLastUpdate = "2015-07-26 12:39:06.14"
            };
            return contactPerson;
        }

        private class CreateContactPersonCommandHandlerForTest : CreateContactPerson.Handler
        {
            public CreateContactPersonCommandHandlerForTest(IPageRepository pageRepository)
                : base(pageRepository) { }
            
            public void HandleCommand(CreateContactPerson.Command command)
                => base.Handle(command);
        }
        
        private class UpdateContactPersonCommandHandlerForTest : UpdateContactPerson.Handler
        {
            public UpdateContactPersonCommandHandlerForTest(IPageRepository pageRepository, IContentRepository contentRepository)
                : base(pageRepository, contentRepository) { }
            
            public void HandleCommand(UpdateContactPerson.Command command)
                => base.Handle(command);
        }
        
        private class DeleteContactPersonCommandHandlerForTest : DeleteContactPerson.Handler
        {
            public DeleteContactPersonCommandHandlerForTest(IPageRepository pageRepository, IContentRepository contentRepository)
                : base(pageRepository, contentRepository) { }
            
            public void HandleCommand(DeleteContactPerson.Command command)
                => base.Handle(command);
        }
        
        private class ClonablePersonPage : PersonPage
        {
            public override string Name { get; set; }
            public override DateTime? StopPublish { get; set; }
            
            protected override object CreateWriteableCloneImplementation() => this;
        }
    }
}