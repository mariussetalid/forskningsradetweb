using System;
using System.Collections.Generic;
using System.Linq;
using EPiServer;
using EPiServer.Core;
using FakeItEasy;
using Forskningsradet.Core.Behavior;
using Forskningsradet.Core.Models.ApiModels.Idm;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Repositories.Contracts;
using Xunit;

namespace Forskningsradet.UnitTests.Core.Behavior
{
    public class GetContactPersonsTests
    {
        private readonly IContentRepository _contentRepository;
        private readonly IPageRepository _pageRepository;
        private readonly GetContactPersonsQueryHandlerForTest _handler;
        
        public GetContactPersonsTests()
        {
            _contentRepository = A.Fake<IContentRepository>();
            _pageRepository = A.Fake<IPageRepository>();
            _handler = new GetContactPersonsQueryHandlerForTest(_contentRepository, _pageRepository);
        }
        
        [Fact]
        public void Handle_WhenContactPersonIsFound_ShouldMapPageToContract()
        {
            var expectedId = Guid.NewGuid();
            
            var personPage = A.Fake<PersonPage>();
            personPage.ExternalId = expectedId.ToString();
            personPage.FirstName = "first";
            personPage.LastName = "last";
            personPage.JobTitle = "title";
            personPage.Department = "dept";
            personPage.Mobile = "123";
            personPage.Phone = "321";
            personPage.Email = "a@b.cd";
            personPage.ResourceType = "A";

            var personPages = new List<PersonPage> {personPage};
            
            A.CallTo(() => _contentRepository.GetChildren<PersonPage>(A<ContentReference>._))
                .Returns(personPages);
            
            A.CallTo(() => _pageRepository.FilterPublished(personPages))
                .Returns(personPages);

            var response = _handler.HandleQuery(new GetContactPersons.Query());

            var contactPerson = response.Result.First();
            
            Assert.Equal(expectedId, contactPerson.Id);
            Assert.Equal(personPage.FirstName, contactPerson.FirstName);
            Assert.Equal(personPage.LastName, contactPerson.LastName);
            Assert.Equal(personPage.JobTitle, contactPerson.Title);
            Assert.Equal(personPage.Department, contactPerson.DepartmentName);
            Assert.Equal(personPage.Mobile, contactPerson.Mobile);
            Assert.Equal(personPage.Phone, contactPerson.Phone);
            Assert.Equal(personPage.Email, contactPerson.Email);
            Assert.Equal(personPage.ResourceType, contactPerson.ResourceType);
            Assert.Equal(1, response.ResultCount);
        }
        
        [Fact]
        public void Handle_WhenPageIsNotPublished_ShouldNotBeReturned()
        {
            var page1 = A.Fake<PersonPage>();
            var page2 = A.Fake<PersonPage>();

            page1.ExternalId = Guid.Empty.ToString();
            page2.ExternalId = Guid.Empty.ToString();
            
            var personPages = new List<PersonPage> {page1, page2};
            
            A.CallTo(() => _contentRepository.GetChildren<PersonPage>(A<ContentReference>._))
                .Returns(personPages);
            
            A.CallTo(() => _pageRepository.FilterPublished(personPages))
                .Returns(personPages.Take(1));
            
            var response = _handler.HandleQuery(new GetContactPersons.Query());
            
            Assert.Equal(1, response.ResultCount);
        }

        [Fact]
        public void Handle_WhenPageHasNoId_ShouldNotBeReturned()
        {
            var page1 = A.Fake<PersonPage>();
            var page2 = A.Fake<PersonPage>();

            page1.ExternalId = Guid.Empty.ToString();
            
            var personPages = new List<PersonPage> {page1, page2};
            
            A.CallTo(() => _contentRepository.GetChildren<PersonPage>(A<ContentReference>._))
                .Returns(personPages);
            
            A.CallTo(() => _pageRepository.FilterPublished(personPages))
                .Returns(personPages);
            
            var response = _handler.HandleQuery(new GetContactPersons.Query());
            
            Assert.Equal(1, response.ResultCount);
        }

        [Fact]
        public void Handle_GivenSearchTerm_ShouldFilterResponse()
        {
            const string expectedGuid = "c808622f-4ba0-4963-bad5-d999756a9bd5";
            
            var page1 = A.Fake<PersonPage>();
            var page2 = A.Fake<PersonPage>();

            page1.ExternalId = Guid.Empty.ToString();
            page2.ExternalId = expectedGuid;
            
            var personPages = new List<PersonPage> {page1, page2};
            
            A.CallTo(() => _contentRepository.GetChildren<PersonPage>(A<ContentReference>._))
                .Returns(personPages);
            
            A.CallTo(() => _pageRepository.FilterPublished(personPages))
                .Returns(personPages);

            var query = new GetContactPersons.Query();
            query.SearchTerms.Add(expectedGuid);
            
            var response = _handler.HandleQuery(query);
            
            Assert.Equal(1, response.ResultCount);
        }
        
        private class GetContactPersonsQueryHandlerForTest : GetContactPersons.Handler
        {
            public GetContactPersonsQueryHandlerForTest(IContentRepository contentRepository, IPageRepository pageRepository)
                : base(contentRepository, pageRepository) {}

            public IdmContactPersonsResult HandleQuery(GetContactPersons.Query query)
                => base.Handle(query);
        }
    }
}