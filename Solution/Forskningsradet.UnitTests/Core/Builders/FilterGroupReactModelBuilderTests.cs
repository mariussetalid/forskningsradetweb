using System.Collections.Generic;
using Forskningsradet.Core.Builders;
using Forskningsradet.Core.Models.SearchModels;
using Xunit;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.UnitTests.Core.Builders
{
    public class FilterGroupReactModelBuilderTests
    {
        private readonly FilterGroupReactModelBuilder _builder;

        public FilterGroupReactModelBuilderTests()
        {
            _builder = new FilterGroupReactModelBuilder();
        }

        [Fact]
        public void BuildReactModel_WhenSelectionIsStringArray_ShouldMapFields()
        {
            const string expectedTitle = "T";
            var expectedAccordion = new ReactModels.Accordion();

            var result = _builder.BuildReactModel(expectedAccordion, expectedTitle, "", new List<FilterOption>(), new string[0]);

            Assert.Equal(expectedTitle, result.Title);
            Assert.Same(expectedAccordion, result.Accordion);
        }

        [Fact]
        public void BuildReactModel_WhenSelectionIsIntArray_ShouldMapFields()
        {
            const string expectedTitle = "T";
            var expectedAccordion = new ReactModels.Accordion();

            var result = _builder.BuildReactModel(expectedAccordion, expectedTitle, "", new List<FilterOption>(), new int[0]);

            Assert.Equal(expectedTitle, result.Title);
            Assert.Same(expectedAccordion, result.Accordion);
        }

        [Fact]
        public void BuildReactModel_WhenGivenOptions_ShouldMapOptions()
        {
            const string expectedName = "N";
            const string expectedLabel = "N (1)";
            const string expectedValue = "V";
            var filterOption = new FilterOption
            {
                Name = expectedName,
                Count = 1,
                Value = expectedValue
            };

            var result = _builder.BuildReactModel(new ReactModels.Accordion(), "", expectedName, new List<FilterOption> { filterOption }, new[] { expectedValue })
                .Options[0];

            Assert.Equal(expectedName, result.Name);
            Assert.Equal(expectedLabel, result.Label);
            Assert.Equal(expectedValue, result.Value);
            Assert.True(result.Checked);
        }
    }
}