﻿using System;
using System.Linq;
using EPiServer.Core;
using EPiServer.Web.Routing;
using FakeItEasy;
using Forskningsradet.Core.Builders;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.QueryParameters;
using Forskningsradet.Core.Services;
using Forskningsradet.UnitTests.Builders;
using Xunit;

namespace Forskningsradet.UnitTests.Core.Builders
{
    public class ArticleBlockReactModelBuilderTests
    {
        private readonly IUrlResolver _urlResolver;
        private readonly ILocalizationProvider _localizationProvider;
        private readonly ArticleBlockReactModelBuilder _builder;

        public ArticleBlockReactModelBuilderTests()
        {
            _urlResolver = A.Fake<IUrlResolver>();
            _localizationProvider = A.Fake<ILocalizationProvider>();
            _builder = new ArticleBlockReactModelBuilder(_urlResolver, _localizationProvider);
        }

        [Fact]
        public void BuildReactModel_WhenPageIsNull_ShouldReturnNull()
        {
            var result = _builder.BuildReactModel(null, RenderWidthOption.None);

            Assert.Null(result);
        }

        [Fact]
        public void BuildReactModel_WithNewsArticlePage_ShouldMapFields()
        {
            var expectedTitle = "title";
            var expectedUrl = "url";
            var expectedText = "text";

            A.CallTo(() => _urlResolver.GetUrl(A<ContentReference>._, A<string>._, A<UrlResolverArguments>._))
                .Returns(expectedUrl);

            var page = (NewsArticlePage)NewsArticlePageBuilder.Create.Default()
                .WithListTitle(expectedTitle)
                .WithListIntro(expectedText)
                .WithStartPublish(new DateTime(2010, 12, 13));

            var result = _builder.BuildReactModel(page, RenderWidthOption.None);

            Assert.Equal(expectedTitle, result.Title.Text);
            Assert.Equal(expectedUrl, result.Title.Url);
            Assert.Equal(expectedText, result.Text);
            Assert.Single(result.Byline.Items);
        }

        [Fact]
        public void BuildReactModel_WithNewsArticlePageWithoutStartPublish_ShouldStillMapByline()
        {
            var page = (NewsArticlePage)NewsArticlePageBuilder.Create.Default()
                .WithStartPublish(null);

            var result = _builder.BuildReactModel(page, RenderWidthOption.None);

            Assert.Single(result.Byline.Items);
        }

        [Fact]
        [UseCulture("no")]
        public void BuildReactModel_WithNewsArticlePageWithStartPublishAndChanged_ShouldDisplayChanged()
        {
            var label = "label";
            var expectedBylineText = $"{label} 14.12.2011";
            var page = (NewsArticlePage)NewsArticlePageBuilder.Create.Default()
                .WithStartPublish(new DateTime(2010, 10, 13))
                .WithChanged(new DateTime(2011, 12, 14));

            A.CallTo(() => _localizationProvider.GetLabel(A<string>._, A<string>._))
                .Returns(label);

            var result = _builder.BuildReactModel(page, RenderWidthOption.None);

            Assert.Equal(expectedBylineText, result.Byline.Items.First().Text);
        }

        [Fact]
        [UseCulture("no")]
        public void BuildReactModel_WithPressReleasePageWithStartPublishAndChanged_ShouldDisplayStartPublish()
        {
            var label = "label";
            var expectedBylineText = $"{label} 13.10.2010";
            var page = (PressReleasePage)PressReleasePageBuilder.Create.Default()
                .WithStartPublish(new DateTime(2010, 10, 13))
                .WithChanged(new DateTime(2011, 12, 14));

            A.CallTo(() => _localizationProvider.GetLabel(A<string>._, A<string>._))
                .Returns(label);

            var result = _builder.BuildReactModel(page, RenderWidthOption.None);

            Assert.Equal(expectedBylineText, result.Byline.Items.First().Text);
        }

        [Fact]
        public void BuildReactModel_WithNewsArticlePageWithRenderOptionIsInSidebar_ShouldMapUsedInSidebar()
        {
            var result = _builder.BuildReactModel(NewsArticlePageBuilder.Create.Default(), RenderWidthOption.IsInSidebar);

            Assert.True(result.UsedInSidebar);
        }
    }
}
