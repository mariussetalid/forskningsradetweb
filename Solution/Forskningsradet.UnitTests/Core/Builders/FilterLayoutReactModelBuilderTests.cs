using System.Collections.Generic;
using System.Linq;
using EPiServer.DataAbstraction;
using FakeItEasy;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Builders;
using Forskningsradet.Core.Models.QueryParameters;
using Forskningsradet.Core.Models.RequestModels;
using Forskningsradet.Core.Models.SearchModels;
using Forskningsradet.Core.Services;
using Xunit;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.UnitTests.Core.Builders
{
    public class FilterLayoutReactModelBuilderTests
    {
        private readonly ILocalizationProvider _localizationProvider;
        private readonly ICategoryLocalizationProvider _categoryLocalizationProvider;
        private readonly FilterLayoutReactModelBuilder _builder;

        public FilterLayoutReactModelBuilderTests()
        {
            _localizationProvider = A.Fake<ILocalizationProvider>();
            _categoryLocalizationProvider = A.Fake<ICategoryLocalizationProvider>();
            _builder = new FilterLayoutReactModelBuilder(
                new FilterGroupReactModelBuilder(),
                _categoryLocalizationProvider,
                _localizationProvider);
        }

        [Fact]
        public void BuildReactModel_WithProposalListQueryParameter_ShouldMapLabels()
        {
            const string expectedLabel = "label";
            A.CallTo(() => _localizationProvider.GetLabel(nameof(ReactModels.FilterLayout), A<string>._))
                    .Returns(expectedLabel);
            var model = new FilterLayoutModel(
                new ProposalListQueryParameter(),
                new List<CategoryGroup>
                {
                    new CategoryGroup(
                        new Category("category", "") { ID = 1 },
                        FilterGroupType.None,
                        new List<FilterOption>
                        {
                            new FilterOption { Count = 1, Name = "", Value = "" }
                        })
                },
                null,
                null,
                10);

            var result = _builder.BuildReactModel(model);

            Assert.Equal(expectedLabel, result.Filters.MobileTitle);
            Assert.Equal(expectedLabel, result.Filters.Labels.Reset);
            Assert.Equal(expectedLabel, result.Filters.Labels.ShowResults);
            Assert.Equal(expectedLabel, result.Filters.Items[0].Accordion.CollapseLabel);
            Assert.Equal(expectedLabel, result.Filters.Items[0].Accordion.ExpandLabel);
        }

        [Fact]
        public void BuildReactModel_WithSearchQueryParameterAndRangeSlider_ShouldMapLabels()
        {
            const string expectedLabel = "label";
            A.CallTo(() => _localizationProvider.GetLabel(nameof(ReactModels.FilterLayout), A<string>._))
                .Returns(expectedLabel);
            A.CallTo(() => _localizationProvider.GetEnumName(A<FilterGroupType>._))
                .Returns(expectedLabel);
            var model = CreateFilterLayoutModelForDefaultSearchPage();
            model.Parameters.ShowRangeSlider = true;

            var result = _builder.BuildReactModel(model);

            Assert.Equal(expectedLabel, result.Filters.MobileTitle);
            Assert.Equal(expectedLabel, result.Filters.Labels.Reset);
            Assert.Equal(expectedLabel, result.Filters.Labels.ShowResults);
            Assert.Equal(expectedLabel, result.Filters.Items[0].Accordion.CollapseLabel);
            Assert.Equal(expectedLabel, result.Filters.Items[0].Accordion.ExpandLabel);
            Assert.Equal(expectedLabel, result.Filters.RangeFilter.Label);
            Assert.Equal(expectedLabel, result.Filters.RangeFilter.Submit.Text);
            Assert.Equal(expectedLabel, result.Filters.RangeFilter.From.Label);
            Assert.Equal(expectedLabel, result.Filters.RangeFilter.From.Placeholder);
            Assert.Equal(expectedLabel, result.Filters.RangeFilter.To.Label);
            Assert.Equal(expectedLabel, result.Filters.RangeFilter.To.Placeholder);
        }

        [Fact]
        public void BuildReactModel_WithEventListQueryParameter_ShouldMapLabels()
        {
            const string expectedLabel = "label";
            A.CallTo(() => _localizationProvider.GetLabel(nameof(ReactModels.FilterLayout), A<string>._))
                .Returns(expectedLabel);
            var model = CreateFilterLayoutModelForEventList();

            var result = _builder.BuildReactModel(model);

            Assert.Equal(expectedLabel, result.Filters.MobileTitle);
            Assert.Equal(expectedLabel, result.Filters.Labels.Reset);
            Assert.Equal(expectedLabel, result.Filters.Labels.ShowResults);
            Assert.Equal(expectedLabel, result.Filters.Items[0].Accordion.CollapseLabel);
            Assert.Equal(expectedLabel, result.Filters.Items[0].Accordion.ExpandLabel);
            Assert.Equal(expectedLabel, result.Filters.Checkboxes[0].Label);
            Assert.Equal(SearchConstants.EventListFilter.CheckboxVideoValue, result.Filters.Checkboxes[0].Value);
        }

        [Fact]
        public void BuildReactModel_ShouldInjectResultsCountInShowResultsLabel()
        {
            const string expectedLabel = "Show 10 results";

            A.CallTo(() => _localizationProvider.GetLabel(nameof(ReactModels.FilterLayout), A<string>._))
                .Returns("Show {0} results");

            var model = new FilterLayoutModel(new EventListQueryParameter(), new List<CategoryGroup>(), null, 10);

            var result = _builder.BuildReactModel(model);

            Assert.Equal(expectedLabel, result.Filters.Labels.ShowResults);
        }

        [Fact]
        public void BuildReactModel_WithProposalListQueryParameterContainingContentArea_ShouldMapContentArea()
        {
            var expectedContentAreaComponent = new ReactModels.ContentArea();
            var model = new FilterLayoutModel(new ProposalListQueryParameter(), new List<CategoryGroup>(), expectedContentAreaComponent, "", 10);

            var result = _builder.BuildReactModel(model);

            Assert.Same(expectedContentAreaComponent, result.ContentArea);
        }

        [Fact]
        public void BuildReactModel_WithSearchQueryParameterAndLeftPlacement_ShouldMapFilterIsLeft()
        {
            var model = new FilterLayoutModel(GetDefaultSearchQueryParameter(), new List<CategoryGroup>(), "", 10, false, leftSidePlacement: true);

            var result = _builder.BuildReactModel(model);

            Assert.True(result.IsLeft);
        }

        [Fact]
        public void BuildReactModel_WithDefaultSearchQueryParameter_ShouldMapFilterTitle()
        {
            var expectedTitle = "title";
            var model = new FilterLayoutModel(GetDefaultSearchQueryParameter(), new List<CategoryGroup>(), expectedTitle, 10, false, leftSidePlacement: true);

            var result = _builder.BuildReactModel(model);

            Assert.Equal(expectedTitle, result.Filters.Title);
            Assert.Equal(expectedTitle, result.Filters.MobileTitle);
        }

        [Fact]
        public void BuildReactModel_WithDefaultEventListQueryParameter_ShouldMapFilterTitle()
        {
            var expectedTitle = "title";
            var model = new FilterLayoutModel(GetDefaultEventListQueryParameter(), new List<CategoryGroup>(), expectedTitle, 10);

            var result = _builder.BuildReactModel(model);

            Assert.Equal(expectedTitle, result.Filters.Title);
            Assert.Equal(expectedTitle, result.Filters.MobileTitle);
        }

        [Fact]
        public void BuildReactModel_WithDefaultProposalListQueryParameter_ShouldMapFilterTitle()
        {
            var expectedTitle = "title";
            var model = new FilterLayoutModel(GetDefaultProposalListQueryParameter(), new List<CategoryGroup>(), new ReactModels.ContentArea(), expectedTitle, 10);

            var result = _builder.BuildReactModel(model);

            Assert.Equal(expectedTitle, result.Filters.Title);
            Assert.Equal(expectedTitle, result.Filters.MobileTitle);
        }

        [Fact]
        public void BuildReactModel_WithDefaultProposalListQueryParameter_ShouldMapDefaultProposalListFiltersInCorrectOrder()
        {
            var model = CreateFilterLayoutModelForDefaultProposalList();

            var result = _builder.BuildReactModel(model);

            Assert.Equal(QueryParameters.Subject, result.Filters.Items[0].Options[0].Name);
            Assert.Equal(QueryParameters.TargetGroup, result.Filters.Items[1].Options[0].Name);
            Assert.Equal(QueryParameters.DeadlineTypes, result.Filters.Items[2].Options[0].Name);
            Assert.Equal(QueryParameters.ApplicationTypes, result.Filters.Items[3].Options[0].Name);
        }

        [Fact]
        public void BuildReactModel_WithDefaultSearchQueryParameter_ShouldMapDefaultSearchFiltersInCorrectOrder()
        {
            var model = CreateFilterLayoutModelForDefaultSearchPage();

            var result = _builder.BuildReactModel(model).Filters;

            Assert.Equal(QueryParameters.Subject, result.Items[0].Options[0].Name);
            Assert.Equal(QueryParameters.TargetGroup, result.Items[1].Options[0].Name);
            Assert.Equal(QueryParameters.ResultType, result.Items[2].Options[0].Name);
        }

        [Fact]
        public void BuildReactModel_WithDefaultEventListQueryParameter_ShouldMapDefaultEventFiltersInCorrectOrder()
        {
            var model = CreateFilterLayoutModelForDefaultEventListPage();

            var result = _builder.BuildReactModel(model).Filters;

            Assert.Equal(QueryParameters.Subject, result.Items[0].Options[0].Name);
            Assert.Equal(QueryParameters.TargetGroup, result.Items[1].Options[0].Name);
            Assert.Equal(QueryParameters.EventType, result.Items[2].Options[0].Name);
            Assert.Equal(QueryParameters.Location, result.Items[3].Options[0].Name);
            Assert.Equal(QueryParameters.Video, result.Checkboxes[0].Name);
        }

        [Fact]
        public void BuildReactModel_WithSearchQueryParameter_ShouldMapCustomCategoryFilters()
        {
            var model = CreateFilterLayoutModelForSearchPage();

            var result = _builder.BuildReactModel(model)
                .Filters.Items[0].Options[0];

            Assert.Equal(QueryParameters.Categories, result.Name);
        }

        [Fact]
        public void GetViewModel_WhenCustomCategoriesHasNestedHits_ShouldMapSubOptions()
        {
            const string expectedChildValue = "50";
            const string expectedGrandChildValue = "55";
            var model = CreateFilterLayoutModelForSearchPage(new List<CategoryGroup>
            {
                new CategoryGroup(
                    new Category("otherCategory", "") {ID = 5},
                    FilterGroupType.None,
                    new List<FilterOption>
                    {
                        new NestedFilterOption
                        {
                            Name = "otherName",
                            Value = "50",
                            Count = 1,
                            FilterOptions = new List<FilterOption>
                            {
                                new FilterOption
                                {
                                    Name = "otherName",
                                    Value = "55",
                                    Count = 1
                                }
                            }
                        }
                    })
            });

            var result = _builder.BuildReactModel(model).Filters;

            Assert.Equal(expectedChildValue, result.Items[0].Options[0].Value);
            Assert.Equal(expectedGrandChildValue, result.Items[0].Options[0].SubOptions[0].Value);
        }

        [Fact]
        public void BuildReactModel_WithEventListQueryParameter_ShouldMapCustomCategoryFilters()
        {
            var model = CreateFilterLayoutModelForEventList();

            var result = _builder.BuildReactModel(model)
                .Filters.Items[0].Options[0];

            Assert.Equal(QueryParameters.Categories, result.Name);
        }

        [Fact]
        public void BuildReactModel_WithProposalListQueryParameter_ShouldNotMapSlider()
        {
            var model = CreateFilterLayoutModelForDefaultProposalList();

            var result = _builder.BuildReactModel(model)
                .Filters;

            Assert.Null(result.RangeFilter);
        }

        [Fact]
        public void BuildReactModel_WithSearchQueryParameterAndRangeSlider_ShouldMapSlider()
        {
            const int expectedFromValue = 1;
            const int expectedToValue = 2;
            var model = CreateFilterLayoutModelForSearchPage(new List<CategoryGroup>
            {
                new CategoryGroup(
                    FilterGroupType.Year,
                    new List<FilterOption>
                    {
                        new FilterOption {Name = "option", Count = 1, Value = "1"},
                        new FilterOption {Name = "option", Count = 1, Value = "2"}
                    })
            });
            model.Parameters.ShowRangeSlider = true;

            var result = _builder.BuildReactModel(model)
                .Filters.RangeFilter;

            Assert.Equal(QueryParameters.YearFrom, result.From.Name);
            Assert.Equal(QueryParameters.YearTo, result.To.Name);
            Assert.Equal(expectedFromValue, result.Min);
            Assert.Equal(expectedToValue, result.Max);
            Assert.Equal(expectedFromValue.ToString(), result.From.Value);
            Assert.Equal(expectedToValue.ToString(), result.To.Value);
        }

        [Fact]
        public void BuildReactModel_WithEnableRangeSlider_ShouldMapSlider()
        {
            var model = CreateFilterLayoutModelForSearchPage(new List<CategoryGroup>
            {
                new CategoryGroup(
                    FilterGroupType.Year,
                    new List<FilterOption>
                    {
                        new FilterOption {Name = "option", Count = 1, Value = "1"},
                        new FilterOption {Name = "option", Count = 1, Value = "2"},
                    })
            });
            model.Parameters.ShowRangeSlider = true;

            var result = _builder.BuildReactModel(model)
                .Filters.RangeFilter;

            Assert.NotNull(result);
        }

        [Fact]
        public void BuildReactModel_WithDisableRangeSlider_ShouldNotMapSlider()
        {
            var model = CreateFilterLayoutModelForSearchPage(new List<CategoryGroup>
            {
                new CategoryGroup(
                    FilterGroupType.Year,
                    new List<FilterOption>
                    {
                        new FilterOption {Name = "option", Count = 1, Value = "1"},
                        new FilterOption {Name = "option", Count = 1, Value = "2"},
                    })
            });
            model.Parameters.ShowRangeSlider = false;

            var result = _builder.BuildReactModel(model)
                .Filters.RangeFilter;

            Assert.Null(result);
        }

        [Fact]
        public void BuildReactModel_WithEventListQueryParameter_ShouldNotMapSlider()
        {
            var model = CreateFilterLayoutModelForDefaultEventListPage();

            var result = _builder.BuildReactModel(model)
                .Filters;

            Assert.Null(result.RangeFilter);
        }

        [Fact]
        public void BuildReactModel_WithProposalListQueryParameterAndAllFiltersAreSelected_ShouldCheckCheckboxes()
        {
            var model = CreateFilterLayoutModelForDefaultProposalList(GetDefaultProposalListQueryParameter());

            var result = _builder.BuildReactModel(model);

            Assert.True(result.Filters.Items.All(x => x.Options[0].Checked));
            Assert.True(result.Filters.Items.All(x => x.Accordion.InitiallyOpen));
        }

        [Fact]
        public void BuildReactModel_WithSearchQueryParameterAndAllFiltersAreSelected_ShouldCheckCheckboxes()
        {
            var model = CreateFilterLayoutModelForDefaultSearchPage();

            var result = _builder.BuildReactModel(model);

            Assert.True(result.Filters.Items.All(x => x.Options[0].Checked));
            Assert.True(result.Filters.Items.All(x => x.Accordion.InitiallyOpen));
        }

        [Fact]
        public void BuildReactModel_WithEventListQueryParameterAndAllFiltersAreSelected_ShouldCheckCheckboxes()
        {
            var model = CreateFilterLayoutModelForDefaultEventListPage();

            var result = _builder.BuildReactModel(model);

            Assert.True(result.Filters.Items.All(x => x.Options[0].Checked));
            Assert.True(result.Filters.Items.All(x => x.Accordion.InitiallyOpen));
        }

        private static FilterLayoutModel CreateFilterLayoutModelForDefaultProposalList(ProposalListQueryParameter parameter = null)
        {
            var filterOption = new FilterOption
            {
                Name = "option",
                Value = "1",
                Count = 1
            };
            var categoryGroups = new List<CategoryGroup>
            {
                new CategoryGroup(
                    new Category("category", "") {ID = 1},
                    FilterGroupType.Subject,
                    new List<FilterOption> {filterOption}),
                new CategoryGroup(
                    new Category("category", "") {ID = 1},
                    FilterGroupType.TargetGroup,
                    new List<FilterOption> {filterOption}),
                new CategoryGroup(
                    FilterGroupType.DeadlineType,
                    new List<FilterOption> {filterOption}),
                new CategoryGroup(
                    FilterGroupType.ApplicationType,
                    new List<FilterOption> {filterOption})
            };
            return new FilterLayoutModel(parameter ?? new ProposalListQueryParameter(), categoryGroups, null, "", 10);
        }

        private static ProposalListQueryParameter GetDefaultProposalListQueryParameter()
        {
            return new ProposalListQueryParameter
            {
                TimeFrame = TimeFrameFilter.Future,
                Subjects = new string[] { "1" },
                TargetGroups = new string[] { "1" },
                ApplicationTypes = new int[] { 1 },
                DeadlineTypes = new string[] { "1" }
            };
        }

        private static FilterLayoutModel CreateFilterLayoutModelForDefaultSearchPage()
        {
            var filterOption = new FilterOption
            {
                Name = "option",
                Value = "1",
                Count = 1
            };
            var categoryGroups = new List<CategoryGroup>
            {
                new CategoryGroup(
                    new Category("category", "") {ID = 1},
                    FilterGroupType.Subject,
                    new List<FilterOption> {filterOption}),
                new CategoryGroup(
                    new Category("category", "") {ID = 2},
                    FilterGroupType.TargetGroup,
                    new List<FilterOption> {filterOption}),
                new CategoryGroup(
                    FilterGroupType.ContentType,
                    new List<FilterOption> {filterOption}),
                new CategoryGroup(
                    FilterGroupType.Year,
                    new List<FilterOption> {filterOption, filterOption})
            };
            return new FilterLayoutModel(GetDefaultSearchQueryParameter(), categoryGroups, null, 10);
        }

        private static FilterLayoutModel CreateFilterLayoutModelForSearchPage(List<CategoryGroup> categoryGroups = null)
        {
            if (categoryGroups is null)
            {
                categoryGroups = new List<CategoryGroup>
                {
                    new CategoryGroup
                    (
                        new Category("category", "") {ID = 1},
                        FilterGroupType.None,
                        new List<FilterOption>
                        {
                            new FilterOption
                            {
                                Name = "option",
                                Value = "1",
                                Count = 1
                            }
                        }
                    )
                };
            }
            var parameters = new SearchQueryParameter(
                null,
                null,
                null,
                new string[] { "1" },
                "",
                null,
                null,
                null);
            return new FilterLayoutModel(parameters, categoryGroups, "", 10);
        }

        private static SearchQueryParameter GetDefaultSearchQueryParameter()
        {
            return new SearchQueryParameter(
                new string[] { "1" },
                new string[] { "1" },
                new string[] { "1" },
                new string[] { "1" },
                "",
                null,
                null,
                null
            );
        }

        private static FilterLayoutModel CreateFilterLayoutModelForDefaultEventListPage()
        {
            var filterOption = new FilterOption
            {
                Name = "option",
                Value = "1",
                Count = 1
            };
            var categoryGroups = new List<CategoryGroup>
            {
                new CategoryGroup(
                    new Category("category", "") {ID = 1},
                    FilterGroupType.Subject,
                    new List<FilterOption> {filterOption}),
                new CategoryGroup(
                    new Category("category", "") {ID = 1},
                    FilterGroupType.TargetGroup,
                    new List<FilterOption> {filterOption}),
                new CategoryGroup(
                    FilterGroupType.EventType,
                    new List<FilterOption> {filterOption}),
                new CategoryGroup(
                    FilterGroupType.Location,
                    new List<FilterOption> {filterOption}),
                new CategoryGroup(
                    new Category("otherCategory", "") {ID = 5},
                    FilterGroupType.None,
                    new List<FilterOption> {filterOption})
            };
            return new FilterLayoutModel(GetDefaultEventListQueryParameter(), categoryGroups, "", 10);
        }

        private static FilterLayoutModel CreateFilterLayoutModelForEventList()
        {
            var filterOption = new FilterOption
            {
                Name = "option",
                Value = "1",
                Count = 1
            };
            var categoryGroups = new List<CategoryGroup>
            {
                new CategoryGroup(
                    new Category("category", "") {ID = 1},
                    FilterGroupType.None,
                    new List<FilterOption> {filterOption})
            };
            var parameters = new EventListQueryParameter
            {
                Categories = new int[] { 1 },
            };
            return new FilterLayoutModel(parameters, categoryGroups, null, 10);
        }

        private static EventListQueryParameter GetDefaultEventListQueryParameter()
        {
            return new EventListQueryParameter
            {
                TimeFrame = TimeFrameFilter.Future,
                Subjects = new int[] { 1 },
                TargetGroups = new int[] { 1 },
                Categories = new int[] { 1 },
                Type = new string[] { "1" },
                Location = new string[] { "1" },
                VideoOnly = false
            };
        }
    }
}