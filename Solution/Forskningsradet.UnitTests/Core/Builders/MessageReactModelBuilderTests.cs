﻿using FakeItEasy;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Facades.Contracts;
using Forskningsradet.Core.Models.Enums;
using Xunit;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.UnitTests.Core.Builders
{
    public class MessageReactModelBuilderTests
    {
        private readonly IPageEditingAdapter _pageEditingAdapter;
        private readonly MessageReactModelBuilder _mapper;

        public MessageReactModelBuilderTests()
        {
            _pageEditingAdapter = A.Fake<IPageEditingAdapter>();
            _mapper = new MessageReactModelBuilder(_pageEditingAdapter);
        }

        [Theory]
        [InlineData(MessageTheme.Red, ReactModels.IconWarning_Theme.Red)]
        [InlineData(MessageTheme.Blue, ReactModels.IconWarning_Theme.Blue)]
        [InlineData(MessageTheme.Yellow, ReactModels.IconWarning_Theme.None)]
        public void BuildReactModel_GivenStyle_ShouldMapCorrectStyle(MessageTheme style, ReactModels.IconWarning_Theme expectedTheme)
        {
            var result = _mapper.BuildReactModel(null, null, style);

            Assert.Equal(expectedTheme, result.Theme);
        }

        [Fact]
        public void BuildReactModel_GivenTextAndPropertyName_ShouldMapCorrectly()
        {
            var text = "lorem ipsum";
            var propertyName = "property";
            A.CallTo(() => _pageEditingAdapter.PageIsInEditMode()).Returns(true);

            var result = _mapper.BuildReactModel(text, propertyName).Text;
                
            Assert.Equal(text, result.Text);
            Assert.Equal(propertyName, result.OnPageEditing.Text);
        }
    }
}
