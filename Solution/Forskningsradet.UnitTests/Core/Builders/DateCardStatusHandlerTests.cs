using System.Collections.Generic;
using FakeItEasy;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Models.ApiModels.Evurdering;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Services;
using Forskningsradet.UnitTests.Builders;
using Xunit;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.UnitTests.Core.Builders
{
    public class DateCardStatusHandlerTests
    {
        private readonly IDateCardStatusReactModelBuilder _dateCardStatusReactModelBuilder;
        private readonly DateCardStatusHandler _helper;

        public DateCardStatusHandlerTests()
        {
            _dateCardStatusReactModelBuilder = A.Fake<IDateCardStatusReactModelBuilder>();
            _helper = new DateCardStatusHandler(_dateCardStatusReactModelBuilder);
        }

        [Fact]
        public void BuildDateCardStatusList_GivenInternationalProposalPage_ShouldReturnSingleStatus()
        {
            var activeProposal = InternationalProposalPageBuilder.Create.Default()
                .WithProposalStateActive();
            var plannedProposal = InternationalProposalPageBuilder.Create.Default()
                .WithProposalStatePlanned();
            var completedProposal = InternationalProposalPageBuilder.Create.Default()
                .WithProposalStateCompleted();

            var resultActive = _helper.BuildDateCardStatusList(activeProposal);
            var resultPlanned = _helper.BuildDateCardStatusList(plannedProposal);
            var resultCompleted = _helper.BuildDateCardStatusList(completedProposal);

            Assert.Single(resultActive);
            Assert.Single(resultPlanned);
            Assert.Single(resultCompleted);
        }

        [Fact]
        public void GetCurrentStatus_GivenInternationalProposalPage_ShouldBuildReactModel()
        {
            var expectedDateCardStatus = new ReactModels.DateCardStatus();
            var activeProposal = (InternationalProposalPage)InternationalProposalPageBuilder.Create.Default()
                .WithProposalStateActive();
            var plannedProposal = (InternationalProposalPage)InternationalProposalPageBuilder.Create.Default()
                .WithProposalStatePlanned();
            var completedProposal = (InternationalProposalPage)InternationalProposalPageBuilder.Create.Default()
                .WithProposalStateCompleted();
            A.CallTo(() => _dateCardStatusReactModelBuilder.BuildReactModel(A<ProposalState>._))
                .Returns(expectedDateCardStatus);

            var resultActive = _helper.GetCurrentStatus(activeProposal);
            var resultPlanned = _helper.GetCurrentStatus(plannedProposal);
            var resultCompleted = _helper.GetCurrentStatus(completedProposal);

            Assert.Same(expectedDateCardStatus, resultActive);
            Assert.Same(expectedDateCardStatus, resultPlanned);
            Assert.Same(expectedDateCardStatus, resultCompleted);
        }

        [Theory]
        [InlineData(ProposalState.Active)]
        [InlineData(ProposalState.Completed)]
        [InlineData(ProposalState.Cancelled)]
        [InlineData(ProposalState.Planned)]
        public void BuildDateCardStatusList_GivenProposalPageWithoutApplicationResult_ShouldReturnSingleStatus(ProposalState proposalState)
        {
            var proposal = ProposalPageBuilder.Create.Default()
                .WithProposalState(proposalState);

            var result = _helper.BuildDateCardStatusList(proposal);

            Assert.Single(result);
        }

        [Theory]
        [InlineData(ProposalState.Active)]
        [InlineData(ProposalState.Completed)]
        [InlineData(ProposalState.Cancelled)]
        [InlineData(ProposalState.Deleted)]
        [InlineData(ProposalState.Planned)]
        public void BuildDateCardStatusList_GivenProposalPageWithApplicationResult_ShouldReturnListFromBuilder(ProposalState proposalState)
        {
            var expectedDateCardStatusList = new List<ReactModels.DateCardStatus>();
            var proposal = ProposalPageBuilder.Create.Default()
                .WithProposalState(proposalState)
                .WithNonEmptyApplicationResult();

            A.CallTo(() => _dateCardStatusReactModelBuilder.BuildDateCardStatusList(proposalState, true))
                .Returns(expectedDateCardStatusList);

            var result = _helper.BuildDateCardStatusList(proposal);

            Assert.Same(expectedDateCardStatusList, result);
        }

        [Theory]
        [InlineData(ProposalState.Active)]
        [InlineData(ProposalState.Completed)]
        [InlineData(ProposalState.Cancelled)]
        [InlineData(ProposalState.Deleted)]
        [InlineData(ProposalState.Planned)]
        public void GetCurrentStatus_GivenProposalPageWithApplicationResult_ShouldTakeLastStatusFromBuilder(ProposalState proposalState)
        {
            var expectedDateCardStatus = new ReactModels.DateCardStatus { Theme = ReactModels.DateCardStatus_Theme.ResultIsPublished };
            var proposal = ProposalPageBuilder.Create.Default()
                .WithProposalState(proposalState)
                .WithNonEmptyApplicationResult();

            A.CallTo(() => _dateCardStatusReactModelBuilder.BuildDateCardStatusList(proposalState, true))
                .Returns(new List<ReactModels.DateCardStatus>
                {
                    new ReactModels.DateCardStatus(),
                    expectedDateCardStatus
                });

            var result = _helper.GetCurrentStatus(proposal);

            Assert.Same(expectedDateCardStatus, result);
        }

        [Theory]
        [InlineData(ProposalState.Active)]
        [InlineData(ProposalState.Completed)]
        [InlineData(ProposalState.Cancelled)]
        [InlineData(ProposalState.Deleted)]
        [InlineData(ProposalState.Planned)]
        public void GetCurrentStatus_GivenProposalPageWithoutApplicationResult_ShouldTakeLastStatusFromBuilder(ProposalState proposalState)
        {
            var expectedDateCardStatus = new ReactModels.DateCardStatus();
            var proposal = ProposalPageBuilder.Create.Default()
                .WithProposalState(proposalState);

            A.CallTo(() => _dateCardStatusReactModelBuilder.BuildDateCardStatusList(proposalState, false))
                .Returns(new List<ReactModels.DateCardStatus>
                {
                    expectedDateCardStatus
                });

            var result = _helper.GetCurrentStatus(proposal);

            Assert.Same(expectedDateCardStatus, result);
        }
    }
}