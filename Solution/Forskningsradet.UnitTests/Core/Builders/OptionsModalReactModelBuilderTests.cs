﻿using System;
using EPiServer;
using EPiServer.Globalization;
using EPiServer.Web.Routing;
using FakeItEasy;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Builders;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Services;
using Forskningsradet.UnitTests.Builders;
using Xunit;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.UnitTests.Core.Builders
{
    public class OptionsModalReactModelBuilderTests
    {
        private readonly ILocalizationProvider _localizationProvider;
        private readonly IUrlResolver _urlResolver;
        private readonly IContentLoader _contentLoader;
        private readonly IDownloadListReactModelBuilder _downloadListReactModelBuilder;
        private readonly OptionsModalReactModelBuilder _builder;

        public OptionsModalReactModelBuilderTests()
        {
            _localizationProvider = A.Fake<ILocalizationProvider>();
            _urlResolver = A.Fake<IUrlResolver>();
            _contentLoader = A.Fake<IContentLoader>();
            _downloadListReactModelBuilder = A.Fake<IDownloadListReactModelBuilder>();
            ContentLanguage.Instance = A.Fake<ContentLanguage>();
            _builder = new OptionsModalReactModelBuilder(_urlResolver, _localizationProvider, _contentLoader, _downloadListReactModelBuilder);
        }

        [Fact]
        public void BuildShareContent_WhenAllFiledsProvided_ShouldMapFields()
        {
            var page = A.Fake<EditorialPage>();
            page.PageName = "name";
            
            var expectedItemsCount = Enum.GetNames(typeof(ReactModels.ShareOptions_Items_Icon)).Length - 1;
            var expectedMailUrl = string.Format(ShareConstants.MailUrlFormat, page.Name, "");
            var expectedTwitterUrl = string.Format(ShareConstants.TwitterUrlFormat, page.Name, "");
            var expectedFacebookUrl = string.Format(ShareConstants.FacebookUrlFormat, "");
            var expectedLinkedInUrl = string.Format(ShareConstants.LinkedinUrlFormat, page.Name, "");

            const string expectedOpenButtonText = "Open";
            const string expectedCloseLabel = "Close";
            ArrangeTranslation(expectedOpenButtonText, "OpenButtonText");
            ArrangeTranslation(expectedCloseLabel, "CloseLabel");

            var result = _builder.BuildShareContent(page);

            Assert.Equal(expectedItemsCount, result.ShareContent.Items.Count);
            Assert.Equal(expectedOpenButtonText, result.OpenButtonText);
            Assert.Equal(expectedCloseLabel, result.CloseButtonLabel);
            Assert.Equal(expectedMailUrl, result.ShareContent.Items[0].Url);
            Assert.Equal(expectedTwitterUrl, result.ShareContent.Items[1].Url);
            Assert.Equal(expectedFacebookUrl, result.ShareContent.Items[2].Url);
            Assert.Equal(expectedLinkedInUrl, result.ShareContent.Items[3].Url);
        }

        [Fact]
        public void BuildDownloadContent_WhenAllFieldsProvided_ShouldMapFields()
        {
            const string expectedOpenButtonText = "Open";
            const string expectedCloseLabel = "Close";
            var expectedDownloadList = new ReactModels.DownloadList();

            ArrangeTranslation(expectedOpenButtonText, "DownloadTemplates");
            ArrangeTranslation(expectedCloseLabel, "CloseLabel");

            var page = (ProposalPage)ProposalPageBuilder.Create.Default();
            
            A.CallTo(() => _downloadListReactModelBuilder.BuildReactModel(page))
                .Returns(expectedDownloadList);
                
            var result = _builder.BuildDownloadContent(page);

            Assert.Equal(expectedOpenButtonText, result.OpenButtonText);
            Assert.Equal(expectedCloseLabel, result.CloseButtonLabel);
            Assert.Equal(expectedDownloadList, result.DownloadContent);
        }

        private void ArrangeTranslation(string expected, string label) =>
            A.CallTo(() => _localizationProvider.GetLabel(nameof(ReactModels.OptionsModal), label))
                .Returns(expected);
    }
}
