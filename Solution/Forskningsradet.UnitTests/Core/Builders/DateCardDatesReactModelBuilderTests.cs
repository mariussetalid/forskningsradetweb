using System;
using FakeItEasy;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Builders;
using Forskningsradet.Core.Extensions;
using Forskningsradet.Core.Models.ApiModels.Evurdering;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Services;
using Xunit;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.UnitTests.Core.Builders
{
    public class DateCardDatesReactModelBuilderTests
    {
        private readonly ILocalizationProvider _localizationProvider;
        private readonly DateCardDatesReactModelBuilder _mapper;

        public DateCardDatesReactModelBuilderTests()
        {
            _localizationProvider = A.Fake<ILocalizationProvider>();
            _mapper = new DateCardDatesReactModelBuilder(_localizationProvider);
        }

        [Fact]
        [UseCulture("nb-NO")]
        public void BuildReactModel_WhithEventPage_ShouldMapFields()
        {
            const string expectedDay = "13.";
            const string expectedMonth = "jan";
            const string expectedYear = "2000";
            const string expectedPastDateLabel = "date past";
            var pastDate = new DateTime(2000, 1, 13, 5, 6, 7);
            var eventPage = A.Fake<EventPage>();
            eventPage.StartDate = pastDate;
            A.CallTo(() => eventPage.ComputedEventDate)
                .Returns(pastDate);
            A.CallTo(() => eventPage.ComputedEventEndDate)
                .Returns(pastDate);

            ArrangeTranslation(expectedPastDateLabel, "PastDate");

            var result = _mapper.BuildReactModel(eventPage, string.Empty);

            Assert.True(result.IsPastDate);
            Assert.Equal(expectedPastDateLabel, result.Labels.PastDate);
            Assert.Equal(expectedDay, result.Dates[0].Day);
            Assert.Equal(expectedMonth, result.Dates[0].Month);
            Assert.Equal(expectedYear, result.Dates[0].Year);
        }

        [Fact]
        [UseCulture("nb-NO")]
        public void BuildReactModel_WithProposalPage_ShouldMapFields()
        {
            const string expectedDeadlineTitle = "deadline";
            const string expectedDay = "22.";
            var page = A.Fake<ProposalPage>();
            page.PageName = "page name";
            page.Deadline = new DateTime(2018, 11, 22);
            A.CallTo(() => page.DeadlineComputed)
                .Returns(page.Deadline);

            var result = _mapper.BuildReactModel(page, expectedDeadlineTitle, null, false);

            Assert.True(result.IsPastDate);
            Assert.Equal(expectedDeadlineTitle, result.DatesTitle);
            Assert.Null(result.DatesDescription);
            Assert.Equal(page.Deadline?.ToDisplayDate().ToString("MMM yyyy"), result.Dates[0].Month);
            Assert.Equal(expectedDay, result.Dates[0].Day);
        }

        [Fact]
        public void BuildReactModel_WithInternationalProposalPage_ShouldMapIsInternational()
        {
            var page = A.Fake<InternationalProposalPage>();

            var result = _mapper.BuildReactModel(page, null, null, false);

            Assert.True(result.IsInternational);
        }

        [Fact]
        public void BuildReactModel_WithContinuousProposal_ShouldSetCorrectDate()
        {
            const string expectedDateDescription = "desc";
            var page = A.Fake<ProposalPage>();
            page.Deadline = null;
            page.DeadlineOverride = null;
            page.DeadlineType = DeadlineType.Continuous;
            A.CallTo(() => page.DeadlineComputed).Returns(page.DeadlineOverride);

            var result = _mapper.BuildReactModel(page, null, expectedDateDescription);

            Assert.Null(result.Dates[0].Day);
            Assert.Equal(expectedDateDescription, result.DatesDescription);
        }

        [Fact]
        [UseCulture("nb-NO")]
        public void BuildReactModel_WhenProposalTypeHasDeadline_ShouldSetCorrectDate()
        {
            var page = A.Fake<ProposalPage>();
            page.Deadline = null;
            page.DeadlineOverride = new DateTime(2018, 11, 24);
            var expectedMonthText = page.DeadlineOverride.Value.ToDisplayDate().ToString("MMM yyyy");
            var expectedDay = page.DeadlineOverride.Value.ToDisplayDate().ToString(DateTimeFormats.NorwegianDateDayLong);
            A.CallTo(() => page.DeadlineComputed).Returns(page.DeadlineOverride);

            var result = _mapper.BuildReactModel(page, null, null).Dates[0];

            Assert.Equal(expectedMonthText, result.Month);
            Assert.Equal(expectedDay, result.Day);
        }

        [Fact]
        [UseCulture("nb-NO")]
        public void BuildReactModel_WithProposalPageWithDateTimeBeforeTwoDeadlines_ShouldMapFirstDeadlineWithCorrectLabels()
        {
            const string expectedDatesTitle = "Søknadsfrist 1";
            var page = A.Fake<InternationalProposalPage>();
            page.FirstDeadline = new DateTime(2050, 1, 21);
            page.FirstDeadlineDisplayName = "Skissefrist";
            var expectedDate = page.FirstDeadline.Value.ToDisplayDate().ToString(DateTimeFormats.NorwegianDateDayLong);
            var expectedMonthYear = page.FirstDeadline.Value.ToDisplayDate().ToString("MMM yyyy");
            A.CallTo(() => page.DeadlineComputed).Returns(page.FirstDeadline);

            var result = _mapper.BuildReactModel(page, expectedDatesTitle, null);

            Assert.Equal(expectedDate, result.Dates[0].Day);
            Assert.Equal(expectedMonthYear, result.Dates[0].Month);
            Assert.Equal(expectedDatesTitle, result.DatesTitle);
        }

        [Fact]
        [UseCulture("nb-NO")]
        public void BuildReactModel_WithProposalPageWithDateTimeBetweenTwoDeadlines_ShouldMapSecondDeadlineWithCorrectLabels()
        {
            const string expectedDatesTitle = "Søknadsfrist 2";
            var page = A.Fake<InternationalProposalPage>();
            page.FirstDeadline = new DateTime(2010, 1, 21);
            page.Deadline = new DateTime(2060, 6, 10);
            page.DeadlineDisplayName = "Endelig søknadsfrist";
            var expectedDate = page.Deadline.Value.ToDisplayDate().ToString(DateTimeFormats.NorwegianDateDayLong);
            var expectedMonthYear = page.Deadline.Value.ToDisplayDate().ToString("MMM yyyy");
            A.CallTo(() => page.DeadlineComputed).Returns(page.Deadline);

            var result = _mapper.BuildReactModel(page, expectedDatesTitle, null);

            Assert.Equal(expectedDate, result.Dates[0].Day);
            Assert.Equal(expectedMonthYear, result.Dates[0].Month);
            Assert.Equal(expectedDatesTitle, result.DatesTitle);
        }

        [Fact]
        public void BuildReactModel_WithProposalPageWithOneDeadline_ShouldMapDeadlineLabelCorrectly()
        {
            const string expectedDatesTitle = "Søknadsfrist";
            var page = A.Fake<InternationalProposalPage>();
            page.Deadline = new DateTime(2050, 1, 1);

            var result = _mapper.BuildReactModel(page, expectedDatesTitle, null);

            Assert.Equal(expectedDatesTitle, result.DatesTitle);
        }

        [Fact]
        public void BuildReactModel_WithEventImage_ShouldMapEventImage()
        {
            var expectedEventImageComponent = new ReactModels.EventImage();
            var eventPage = A.Fake<EventPage>();

            var result = _mapper.BuildReactModel(eventPage, null, expectedEventImageComponent);

            Assert.Same(expectedEventImageComponent, result.EventImage);
        }

        [Fact]
        [UseCulture("nb-NO")]
        public void BuildReactModel_WhenEventDateIsSameDate_ShouldOnlyMapStart()
        {
            var date = new DateTime(2015, 11, 14, 10, 10, 0);
            var eventPage = new EventPage
            {
                StartDate = date,
                EndDate = date.AddHours(3)
            };

            var result = _mapper.BuildReactModel(eventPage, string.Empty);

            Assert.Equal(1, result.Dates.Count);
        }

        [Fact]
        [UseCulture("nb-NO")]
        public void BuildReactModel_WhenEventDateIsCurrentYear_ShouldNotMapYear()
        {
            var presentYear = DateTime.Today.Year;
            var eventPage = new EventPage
            {
                StartDate = new DateTime(presentYear, 3, 2),
                EndDate = new DateTime(presentYear, 4, 3)
            };

            var result = _mapper.BuildReactModel(eventPage, string.Empty);

            Assert.Null(result.Dates[0].Year);
            Assert.Null(result.Dates[1].Year);
        }

        [Fact]
        [UseCulture("nb-NO")]
        public void BuildReactModel_WhenEventDateIsNotCurrentYear_ShouldMapYear()
        {
            var present = DateTime.Today;
            var past = present.AddYears(-2);
            var future = present.AddYears(2);
            var eventPage = new EventPage
            {
                StartDate = past,
                EndDate = future
            };

            var result = _mapper.BuildReactModel(eventPage, string.Empty);

            Assert.NotNull(result.Dates[0].Year);
            Assert.NotNull(result.Dates[1].Year);
        }

        [Fact]
        [UseCulture("nb-NO")]
        public void BuildReactModel_WhenProposalPageHasDeadline_ShouldMapDeadlineDate()
        {
            var expectedDay = "13.";
            var page = A.Fake<ProposalPage>();
            page.Deadline = new DateTime(2017, 5, 13);
            A.CallTo(() => page.DeadlineComputed).Returns(page.Deadline);

            var result = _mapper.BuildReactModel(page, "label", "description");

            Assert.Equal(expectedDay, result.Dates[0].Day);
            Assert.Equal($"{page.Deadline.Value.ToDisplayDate():MMM} {page.Deadline.Value:yyyy}", result.Dates[0].Month);
            Assert.True(result.IsPastDate);
        }

        [Fact]
        [UseCulture("nb-NO")]
        public void BuildReactModel_WhenProposalIsCompleted_ShouldBeMarkedAsPast()
        {
            var page = A.Fake<ProposalPage>();
            page.Deadline = new DateTime(2047, 5, 13);
            page.ProposalState = ProposalState.Completed;
            A.CallTo(() => page.DeadlineComputed).Returns(page.Deadline);

            var result = _mapper.BuildReactModel(page, "label", "description");

            Assert.True(result.IsPastDate);
        }

        [Fact]
        public void BuildReactModel_WhenProposalIsContinuousAndCompleted_ShouldBeMarkedAsPast()
        {
            var page = A.Fake<ProposalPage>();
            page.Deadline = null;
            page.DeadlineOverride = null;
            page.DeadlineType = DeadlineType.Continuous;
            A.CallTo(() => page.DeadlineComputed).Returns(page.DeadlineOverride);
            page.ProposalState = ProposalState.Completed;

            var result = _mapper.BuildReactModel(page, "label", "description");

            Assert.True(result.IsPastDate);
        }

        [Fact]
        public void BuildReactModel_WhenProposalIsContinuousAndactive_ShouldNotBeMarkedAsPast()
        {
            var page = A.Fake<ProposalPage>();
            page.Deadline = null;
            page.DeadlineOverride = null;
            page.DeadlineType = DeadlineType.Continuous;
            A.CallTo(() => page.DeadlineComputed).Returns(page.DeadlineOverride);
            page.ProposalState = ProposalState.Active;

            var result = _mapper.BuildReactModel(page, "label", "description");

            Assert.False(result.IsPastDate);
        }

        private void ArrangeTranslation(string expected, string label) => A.CallTo(() =>
            _localizationProvider.GetLabel(nameof(EventListPage), label))
                .Returns(expected);
    }
}