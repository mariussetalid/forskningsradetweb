using System;
using System.Collections.Generic;
using System.Linq;
using EPiServer;
using EPiServer.Core;
using EPiServer.Web.Routing;
using FakeItEasy;
using Forskningsradet.Core.Builders;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.Enums;
using Forskningsradet.Core.Models.QueryParameters;
using Forskningsradet.Core.Repositories.Contracts;
using Forskningsradet.Core.Services;
using Forskningsradet.UnitTests.Builders;
using Xunit;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.UnitTests.Core.Builders
{
    public class DateCardReactModelBuilderTests
    {
        private readonly IUrlResolver _urlResolver;
        private readonly IContentLoader _contentLoader;
        private readonly ILocalizationProvider _localizationProvider;
        private readonly IDateCardDatesReactModelBuilder _dateCardDatesReactModelBuilder;
        private readonly IDateCardTagsReactModelBuilder _dateCardTagsReactModelBuilder;
        private readonly IFluidImageReactModelBuilder _fluidImageReactModelBuilder;
        private readonly IPageRepository _pageRepository;
        private readonly DateCardReactModelBuilder _mapper;

        public DateCardReactModelBuilderTests()
        {
            _urlResolver = A.Fake<IUrlResolver>();
            _contentLoader = A.Fake<IContentLoader>();
            _localizationProvider = A.Fake<ILocalizationProvider>();
            _dateCardDatesReactModelBuilder = A.Fake<IDateCardDatesReactModelBuilder>();
            _dateCardTagsReactModelBuilder = A.Fake<IDateCardTagsReactModelBuilder>();
            _fluidImageReactModelBuilder = A.Fake<IFluidImageReactModelBuilder>();
            _pageRepository = A.Fake<IPageRepository>();
            _mapper = new DateCardReactModelBuilder(_urlResolver, _contentLoader, _localizationProvider, _dateCardDatesReactModelBuilder, _dateCardTagsReactModelBuilder, _fluidImageReactModelBuilder, _pageRepository);
        }

        [Fact]
        [UseCulture("nb-NO")]
        public void GetViewModel_WhenAllFieldsProvided_ShouldMapFields()
        {
            const string expectedEventUrl = "evUrl";
            const string expectedTypeLabel = "TypLabel";
            const string expectedDurationLabel = "DurLabel";
            const string expectedLocationLabel = "LocLabel";
            const string expectedTypeText = "tText";
            const string expectedMediaUrl = "url";
            const string expectedMediaLabel = "medLabel";
            var expectedDateCardDatesComponentModel = new ReactModels.DateCardDates();
            var expectedDateCardTagsComponentModel = new ReactModels.DateCardTags();
            var eventPage = A.Fake<EventPage>();
            eventPage.EventData = new EventDataBlock
            {
                Type = EventType.Conference
            };
            eventPage.PageName = "e name";
            eventPage.ListTitle = null;
            eventPage.ListIntro = "listintro";
            eventPage.OptionalSubtitle = "optionalsubtitle";
            eventPage.ContentLink = new ContentReference(5);
            eventPage.PageLink = new PageReference(5);
            eventPage.Duration = "duraText";
            eventPage.Location = "locaText";
            eventPage.VideoLink = new Url("videoUrl");
            eventPage.Category = new CategoryList(new List<int> { 2 });
            A.CallTo(() => _urlResolver.GetUrl(A<ContentReference>.That.Matches(x => x == eventPage.ContentLink), A<string>._, A<UrlResolverArguments>._))
                .Returns(expectedEventUrl);
            A.CallTo(() => _urlResolver.GetUrl(A<UrlBuilder>._, A<UrlResolverArguments>._))
                .Returns(expectedMediaUrl);
            A.CallTo(() => _dateCardDatesReactModelBuilder.BuildReactModel(eventPage, A<string>._, A<ReactModels.EventImage>._))
                .Returns(expectedDateCardDatesComponentModel);
            A.CallTo(() => _dateCardTagsReactModelBuilder.BuildReactModel(eventPage.Category, A<int>._))
                .Returns(expectedDateCardTagsComponentModel);

            ArrangeEnumTranslation(expectedTypeText, EventType.Conference);
            ArrangeTranslation(expectedTypeLabel, "Type");
            ArrangeTranslation(expectedDurationLabel, "Duration");
            ArrangeTranslation(expectedLocationLabel, "Location");
            ArrangeTranslation(expectedMediaLabel, "VideoText");

            var result = _mapper.BuildReactModel(eventPage);

            Assert.Equal(eventPage.PageName, result.Title);
            Assert.Equal(eventPage.OptionalSubtitle, result.Subtitle);
            Assert.Equal(eventPage.PageLink.ID.ToString(), result.Id);
            Assert.Equal(expectedEventUrl, result.Url);
            Assert.Equal(expectedTypeLabel, result.Metadata.Items[0].Label);
            Assert.Equal(expectedTypeText, result.Metadata.Items[0].Text);
            Assert.Equal(expectedDurationLabel, result.Metadata.Items[1].Label);
            Assert.Equal(eventPage.Duration, result.Metadata.Items[1].Text);
            Assert.Equal(expectedLocationLabel, result.Metadata.Items[2].Label);
            Assert.Equal(eventPage.Location, result.Metadata.Items[2].Text);
            Assert.Equal(eventPage.ListIntro, result.Text);
            Assert.Same(expectedDateCardDatesComponentModel, result.DateContainer);
            Assert.Same(expectedDateCardTagsComponentModel, result.Tags);
            Assert.Equal(expectedMediaUrl, result.Media.Items[0].Url);
            Assert.Equal(expectedMediaLabel, result.Media.Items[0].Text);
            Assert.Equal(ReactModels.DateCardMedia_Items_Icon.Video, result.Media.Items[0].Icon);
        }

        [Fact]
        public void GetViewModel_WithFutureEvent_ShouldMapDateContainerWithEventImage()
        {
            var expectedDateContainer = new ReactModels.DateCardDates();
            var eventPage = new EventPage
            {
                EndDate = new DateTime(2050, 12, 2, 0, 0, 0)
            };
            A.CallTo(() => _dateCardDatesReactModelBuilder.BuildReactModel(eventPage, A<string>._, A<ReactModels.EventImage>.That.IsNull()))
                .Returns(new ReactModels.DateCardDates());
            A.CallTo(() => _dateCardDatesReactModelBuilder.BuildReactModel(eventPage, A<string>._, A<ReactModels.EventImage>.That.IsNotNull()))
                .Returns(expectedDateContainer);

            var result = _mapper.BuildReactModel(eventPage);

            Assert.Same(expectedDateContainer, result.DateContainer);
        }

        [Fact]
        public void GetViewModel_WithPastEvent_ShouldMapDateContainerWithoutEventImage()
        {
            var expectedDateContainer = new ReactModels.DateCardDates();
            var eventPage = new EventPage
            {
                EndDate = new DateTime(2001, 12, 2, 0, 0, 0)
            };
            A.CallTo(() => _dateCardDatesReactModelBuilder.BuildReactModel(eventPage, A<string>._, A<ReactModels.EventImage>.That.IsNull()))
                .Returns(expectedDateContainer);
            A.CallTo(() => _dateCardDatesReactModelBuilder.BuildReactModel(eventPage, A<string>._, A<ReactModels.EventImage>.That.IsNotNull()))
                .Returns(new ReactModels.DateCardDates());

            var result = _mapper.BuildReactModel(eventPage);

            Assert.Same(expectedDateContainer, result.DateContainer);
        }

        [Fact]
        public void GetViewModel_WithVideoLink_ShouldMapLinkToVideo()
        {
            const string expectedMediaUrl = "url";
            const string expectedMediaLabel = "medLabel";
            var eventPage = new EventPage
            {
                VideoLink = new Url("videoUrl")
            };
            A.CallTo(() => _urlResolver.GetUrl(A<UrlBuilder>._, A<UrlResolverArguments>._))
                .Returns(expectedMediaUrl);
            ArrangeTranslation(expectedMediaLabel, "VideoText");

            var result = _mapper.BuildReactModel(eventPage)
                .Media.Items[0];

            Assert.Equal(expectedMediaUrl, result.Url);
            Assert.Equal(expectedMediaLabel, result.Text);
            Assert.Equal(ReactModels.DateCardMedia_Items_Icon.Video, result.Icon);
        }

        [Fact]
        public void GetViewModel_PastEventWithVideoLink_ShouldMapLinkToVideoWithPastText()
        {
            const string expectedMediaLabel = "medLabel";
            var eventPage = new EventPage
            {
                VideoLink = new Url("videoUrl"),
                EndDate = new DateTime(2001, 12, 2, 0, 0, 0)
            };
            A.CallTo(() => _urlResolver.GetUrl(A<UrlBuilder>._, A<UrlResolverArguments>._))
                .Returns("url");
            ArrangeTranslation(expectedMediaLabel, "VideoTextPastEvent");

            var result = _mapper.BuildReactModel(eventPage)
                .Media.Items[0];

            Assert.Equal(expectedMediaLabel, result.Text);
        }

        [Fact]
        public void GetViewModel_DigitalEvent_ShouldMapCameraIconAndStreamText()
        {
            const string expectedMediaLabel = "medLabel";
            var eventPage = new EventPage
            {
                EventData = new EventDataBlock
                {
                    Type = EventType.Digital
                }
            };
            ArrangeTranslation(expectedMediaLabel, "StreamedEvent");

            var result = _mapper.BuildReactModel(eventPage)
                .Media.Items[0];

            Assert.Equal(expectedMediaLabel, result.Text);
            Assert.Equal(ReactModels.DateCardMedia_Items_Icon.Camera, result.Icon);
        }

        [Fact]
        public void GetViewModel_DigitalEventWithRecordingWillBeAvailable_ShouldMapStreamAndRecordingWillBeAvailableText()
        {
            const string expectedMediaLabel = "medLabel";
            var eventPage = new EventPage
            {
                EventData = new EventDataBlock
                {
                    Type = EventType.Digital,
                    RecordingWillBeAvailable = true
                }
            };
            ArrangeTranslation(expectedMediaLabel, "StreamedAndRecordedEvent");

            var result = _mapper.BuildReactModel(eventPage)
                .Media.Items[0];

            Assert.Equal(expectedMediaLabel, result.Text);
        }

        [Fact]
        public void GetViewModel_DigitalEventWithPortfolioMode_ShouldMapCameraIconOnly()
        {
            var eventPage = new EventPage
            {
                EventData = new EventDataBlock
                {
                    Type = EventType.Digital
                }
            };

            var result = _mapper.BuildReactModel(eventPage, null, false, portfolioMode: true)
                .Media.Items[0];

            Assert.Equal(ReactModels.DateCardMedia_Items_Icon.Camera, result.Icon);
            Assert.Null(result.Url);
            Assert.Null(result.Text);
        }

        [Fact]
        public void GetViewModel_WithPortfolioMode_ShouldNotBuildTags()
        {
            var eventPage = A.Fake<EventPage>();
            eventPage.Category = new CategoryList(new[] { 1, 2, 3 });

            A.CallTo(() => _dateCardTagsReactModelBuilder.BuildReactModel(A<CategoryList>._, A<int>._))
                .Returns(new ReactModels.DateCardTags());

            var result = _mapper.BuildReactModel(eventPage, portfolioMode: true);

            Assert.Null(result.Tags);
        }

        [Fact]
        public void GetViewModel_WithSkipTags_ShouldNotBuildTags()
        {
            var eventPage = A.Fake<EventPage>();
            eventPage.Category = new CategoryList(new[] { 1, 2, 3 });

            A.CallTo(() => _dateCardTagsReactModelBuilder.BuildReactModel(A<CategoryList>._, A<int>._))
                .Returns(new ReactModels.DateCardTags());

            var result = _mapper.BuildReactModel(eventPage, skipTags: true);

            Assert.Null(result.Tags);
        }

        [Theory]
        [InlineData(EventImageStyle.None, ReactModels.EventImage_Background.None)]
        [InlineData(EventImageStyle.One, ReactModels.EventImage_Background.Background1)]
        [InlineData(EventImageStyle.Two, ReactModels.EventImage_Background.Background2)]
        [InlineData(EventImageStyle.Three, ReactModels.EventImage_Background.Background3)]
        public void GetViewModel_WithoutListImage_ShouldMapEventImageStyle(EventImageStyle style, ReactModels.EventImage_Background expectedBackground)
        {
            var eventPage = A.Fake<EventPage>();
            eventPage.ListImage = null;
            eventPage.EventImageStyle = style;
            A.CallTo(() => _dateCardDatesReactModelBuilder.BuildReactModel(eventPage, A<string>._, A<ReactModels.EventImage>.That.Matches(x => x.Background == expectedBackground)))
                .Returns(new ReactModels.DateCardDates { EventImage = new ReactModels.EventImage { Background = expectedBackground } });

            var result = _mapper.BuildReactModel(eventPage, null, true, true);

            Assert.Equal(expectedBackground, result.DateContainer.EventImage.Background);
        }

        [Fact]
        public void GetViewModel_WhenUsedInSidebar_TextShouldBeNull()
        {
            var eventPage = new EventPage();

            var result = _mapper.BuildReactModel(eventPage, new RenderOptionQueryParameter { RenderWidthOption = RenderWidthOption.IsInSidebar });

            Assert.Null(result.Text);
        }

        [Fact]
        public void GetViewModel_WhenUseTextIsSetToFalse_TextShouldBeNull()
        {
            var eventPage = new EventPage();

            var result = _mapper.BuildReactModel(eventPage, null, false);

            Assert.Null(result.Text);
        }

        [Fact]
        public void GetViewModel_WhenUsedInSidebar_DurationShouldBeNull()
        {
            const string expectedDuration = "fillertext";
            var eventPage = new EventPage();
            eventPage.Duration = expectedDuration;

            var result = _mapper.BuildReactModel(eventPage, new RenderOptionQueryParameter { RenderWidthOption = RenderWidthOption.IsInSidebar });

            Assert.False(result.Metadata.Items.Select(x => x.Text == expectedDuration).Any());
        }

        [Fact]
        public void GetViewModel_WhenCanceled_ShouldAddCanceledPrefixToTitle()
        {
            var expectedTitlePrefix = "C_A_N_C_E_L_E_D !!! ";

            EventPage eventPage = EventPageBuilder.Create.Default()
                .WithPageLink(new PageReference(1))
                .WithCanceled(true)
                .WithListTitle("Event title");

            A.CallTo(() => _localizationProvider.GetLabel(nameof(EventListPage), "CanceledEventTitlePrefix"))
                .Returns(expectedTitlePrefix);

            var result = _mapper.BuildReactModel(eventPage);

            Assert.StartsWith(expectedTitlePrefix, result.Title);
        }

        [Fact]
        public void GetViewModel_WhenCanceled_ShouldProvideCancelledEventImageInListFromFrontPage()
        {
            EventPage eventPage = EventPageBuilder.Create.Default()
                .WithPageLink(new PageReference(1))
                .WithCanceled(true);

            var frontPage = A.Fake<FrontPageBase>();
            A.CallTo(() => _pageRepository.GetCurrentStartPageAsFrontPageBase()).Returns(frontPage);

            var result = _mapper.BuildReactModel(eventPage);

            A.CallTo(() => frontPage.CanceledEventImageInList).MustHaveHappened();
        }

        private void ArrangeTranslation(string expected, string label) => A.CallTo(() =>
            _localizationProvider.GetLabel(nameof(EventListPage), label))
                .Returns(expected);

        private void ArrangeEnumTranslation<TEnum>(string expected, TEnum value) where TEnum : Enum => A.CallTo(() =>
            _localizationProvider.GetEnumName<TEnum>(value))
                .Returns(expected);
    }
}