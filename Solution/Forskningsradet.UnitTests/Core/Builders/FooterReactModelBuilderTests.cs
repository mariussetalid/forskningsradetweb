using EPiServer;
using EPiServer.Core;
using EPiServer.SpecializedProperties;
using EPiServer.Web.Routing;
using FakeItEasy;
using Forskningsradet.Core.Builders;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Services.Contracts;
using Xunit;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.UnitTests.Core.Builders
{
    public class FooterReactModelBuilderTests
    {
        private readonly IUrlResolver _urlResolver;
        private readonly IUrlCheckingService _urlCheckingService;
        private readonly IRichTextReactModelBuilder _richTextReactModelBuilder;
        private readonly FooterReactModelBuilder _builder;

        public FooterReactModelBuilderTests()
        {
            _urlResolver = A.Fake<IUrlResolver>();
            _urlCheckingService = A.Fake<IUrlCheckingService>();
            _richTextReactModelBuilder = A.Fake<IRichTextReactModelBuilder>();
            _builder = new FooterReactModelBuilder(_urlResolver, _urlCheckingService, _richTextReactModelBuilder);
        }

        [Fact]
        public void BuildReactModel_WhenAllFieldsProvided_ShouldMapFields()
        {
            const string expectedUrl = "url";
            var expectedRichText = new ReactModels.RichText();

            A.CallTo(() => _richTextReactModelBuilder.BuildReactModel(A<XhtmlString>._, A<string>._))
                .Returns(expectedRichText);
            A.CallTo(() => _urlResolver.GetUrl(A<UrlBuilder>._, A<UrlResolverArguments>._))
                .Returns(expectedUrl);

            var footer = new FooterBlock
            {
                ContactInfo = new XhtmlString("The string"),
                Button = new EditLinkBlock
                {
                    Text = "Button Text",
                    Link = new Url("The link")
                },
                MainLinkList = new LinkListBlock { Title = "LinkList Title", LinkItems = new LinkItemCollection { new LinkItem { Text = "Link Text", Href = "Link Href" } } },
                SecondaryLinkList = new LinkListBlock { Title = "SecondLinkList Title", LinkItems = new LinkItemCollection { new LinkItem { Text = "SecondLink Text", Href = "SecondLink Href" } } },
                OtherLinkList = new LinkListBlock { Title = "OtherLinkList Title", LinkItems = new LinkItemCollection { new LinkItem { Text = "OtherLink Text", Href = "OtherLink Href" } } },
                FacebookLink = new Url("faceUrl"),
                LinkedinLink = new Url("linkUrl"),
                TwitterLink = new Url("twitterUrl")
            };

            var result = _builder.BuildReactModel(footer, "name");

            Assert.Same(expectedRichText, result.ContactInfo);
            Assert.Equal(footer.Button.Text, result.NewsLetter.Text);
            Assert.Equal(expectedUrl, result.NewsLetter.Url);
            Assert.Equal(footer.MainLinkList.Title, result.LinkLists[0].Title);
            Assert.Equal(footer.MainLinkList.LinkItems[0].Text, result.LinkLists[0].Links[0].Text);
            Assert.Equal(expectedUrl, result.LinkLists[0].Links[0].Url);
            Assert.Equal(footer.SecondaryLinkList.Title, result.LinkLists[1].Title);
            Assert.Equal(footer.SecondaryLinkList.LinkItems[0].Text, result.LinkLists[1].Links[0].Text);
            Assert.Equal(expectedUrl, result.LinkLists[1].Links[0].Url);
            Assert.Equal(footer.OtherLinkList.LinkItems[0].Text, result.InfoLinks[0].Text);
            Assert.Equal(expectedUrl, result.InfoLinks[0].Url);
            Assert.Equal(ReactModels.SocialMediaLink_Provider.Facebook, result.SocialMedia.Items[0].Provider);
            Assert.Equal(ReactModels.SocialMediaLink_Provider.Linkedin, result.SocialMedia.Items[1].Provider);
            Assert.Equal(ReactModels.SocialMediaLink_Provider.Twitter, result.SocialMedia.Items[2].Provider);
            Assert.Equal(footer.FacebookLink.ToString(), result.SocialMedia.Items[0].Url);
            Assert.Equal(footer.LinkedinLink.ToString(), result.SocialMedia.Items[1].Url);
            Assert.Equal(footer.TwitterLink.ToString(), result.SocialMedia.Items[2].Url);
        }

        [Fact]
        public void BuildReactModel_WhenFooterIsNull_ShouldReturnNull()
        {
            var result = _builder.BuildReactModel(null, null);

            Assert.Null(result);
        }

        [Fact]
        public void BuildReactModel_WhenNewsletterIsNull_ShouldNotReturnNewsletter()
        {
            var result = _builder.BuildReactModel(new FooterBlock(), null);

            Assert.Null(result.NewsLetter);
        }

        [Fact]
        public void BuildReactModel_WhenSocialMediaLinksAreNull_ShouldNotReturnSocialMediaLinks()
        {
            var result = _builder.BuildReactModel(new FooterBlock(), null);

            Assert.Null(result.SocialMedia);
        }
    }
}