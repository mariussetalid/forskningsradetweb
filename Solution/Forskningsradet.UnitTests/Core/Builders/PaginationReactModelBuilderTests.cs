using FakeItEasy;
using Forskningsradet.Core.Builders;
using Forskningsradet.Core.Models.QueryParameters;
using Forskningsradet.Core.Services;
using System.Linq;
using Xunit;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.UnitTests.Core.Builders
{
    public class PaginationReactModelBuilderTests
    {
        private readonly ILocalizationProvider _localizationProvider;
        private readonly PaginationReactModelBuilder _builder;

        public PaginationReactModelBuilderTests()
        {
            _localizationProvider = A.Fake<ILocalizationProvider>();

            _builder = new PaginationReactModelBuilder(_localizationProvider);
        }

        [Theory]
        [InlineData(1, 1)]
        [InlineData(0, 1)]
        public void BuildReactModel_WithTotalPagesLessOrEqualToPageSize_ShouldNotMapPagination(int lessOrEqualToPageSize, int pageSize)
        {
            var query = new CategoryListQueryParameter
            {
                Categories = new int[0],
                Page = 1
            };
            var result = _builder.BuildReactModel("", query.Page, lessOrEqualToPageSize, pageSize, "");

            Assert.Null(result);
        }

        [Fact]
        public void BuildReactModel_WithNavigationBothWays_ShouldMapAccessibilityLabels()
        {
            const string expectedLabel = "label";
            A.CallTo(() => _localizationProvider.GetLabel(nameof(ReactModels.Pagination), A<string>._))
                    .Returns(expectedLabel);

            var result = _builder.BuildReactModel("", 2, 3, 1, "");

            Assert.Equal(expectedLabel, result.Title);
            Assert.Equal(expectedLabel, result.Pages.First().Label);
            Assert.Equal(expectedLabel, result.Pages.First().Link.Text);
            Assert.Equal(expectedLabel, result.Pages[1].Label);
            Assert.Equal(expectedLabel, result.Pages[2].Label);
            Assert.Equal(expectedLabel, result.Pages.Last().Label);
            Assert.Equal(expectedLabel, result.Pages.Last().Link.Text);
        }

        [Theory]
        [InlineData(0, 1)]
        [InlineData(1, 1)]
        [InlineData(2, 1)]
        [InlineData(8, 2)]
        public void BuildReactModel_WhenCurrentPageIsZero_ShouldBeSameAsCurrentPageIsOne(int totalPages, int pageSize)
        {
            var resultZero = _builder.BuildReactModel("", 0, totalPages, pageSize, "");
            var resultOne = _builder.BuildReactModel("", 1, totalPages, pageSize, "");

            Assert.Equal(resultZero?.Pages.Count(), resultOne?.Pages.Count());
            Assert.Equal(resultZero?.Pages.First().Link.Url, resultOne?.Pages.First().Link.Url);
            Assert.Equal(resultZero?.Pages.Last().Link.Url, resultOne?.Pages.Last().Link.Url);
        }

        [Fact]
        public void BuildReactModel_WithNavigationBothWaysAndPageCountIs10_ShouldMapNavigationToPreviousAndNext()
        {
            const string expectedPrevious = "?page=4";
            const string expectedNext = "?page=6";

            var result = _builder.BuildReactModel("", 5, 100, 10, "");

            Assert.Equal(expectedPrevious, result.Pages.First().Link.Url);
            Assert.Equal(expectedNext, result.Pages.Last().Link.Url);
        }

        [Fact]
        public void BuildReactModel_WithNavigationBothWaysAndPageCountIsGreaterThan10_ShouldMapNavigationToFirstAndPrevious()
        {
            const string expectedFirst = "?page=1";
            const string expectedPrevious = "?page=14";
            const string expectedFirstNumericLink = "11";

            var result = _builder.BuildReactModel("", 15, 300, 10, "");

            Assert.Equal(expectedFirst, result.Pages.First().Link.Url);
            Assert.Equal(expectedPrevious, result.Pages[1].Link.Url);
            Assert.Equal(expectedFirstNumericLink, result.Pages[2].Link.Text);
        }

        [Fact]
        public void BuildReactModel_WithNavigationBothWaysAndPageCountIsGreaterThan10_ShouldMapNavigationToNextAndLast()
        {
            const string expectedLast = "?page=30";
            const string expectedNext = "?page=16";
            const string expectedLastNumericLink = "20";

            var result = _builder.BuildReactModel("", 15, 300, 10, "");

            var lastIndex = result.Pages.Count - 1;
            Assert.Equal(expectedLastNumericLink, result.Pages[lastIndex - 2].Link.Text);
            Assert.Equal(expectedNext, result.Pages[lastIndex - 1].Link.Url);
            Assert.Equal(expectedLast, result.Pages.Last().Link.Url);
        }

        [Fact]
        public void BuildReactModel_WhenOnFirstPage_ShouldNotMapPrevious()
        {
            const string expectedLabel = "label";
            A.CallTo(() => _localizationProvider.GetLabel(nameof(ReactModels.Pagination), A<string>._))
                .Returns(expectedLabel);

            var result = _builder.BuildReactModel("", 1, 3, 1, "");

            Assert.Equal("1", result.Pages.First().Link.Text);
        }

        [Fact]
        public void BuildReactModel_WhenOnLastPage_ShouldNotMapNext()
        {
            const string expectedLabel = "label";
            A.CallTo(() => _localizationProvider.GetLabel(nameof(ReactModels.Pagination), A<string>._))
                .Returns(expectedLabel);

            var result = _builder.BuildReactModel("", 3, 3, 1, "");

            Assert.Equal("3", result.Pages.Last().Link.Text);
        }

        [Fact]
        public void BuildReactModel_WhenCurrentPageIsPastPageSize_ShouldShowNextIntervalOfPages()
        {
            var result = _builder.BuildReactModel("", 15, 165, 10, "");

            Assert.Equal("?page=1", result.Pages[0].Link.Url);
            Assert.Equal("?page=14", result.Pages[1].Link.Url);
            Assert.Equal("11", result.Pages[2].Link.Text);
        }

        [Fact]
        public void BuildReactModel_WhenCurrentPageIsLastPage_CurrentPageShouldBeLastLink()
        {
            var result = _builder.BuildReactModel("", 15, 150, 10, "");

            Assert.Equal("15", result.Pages.Last().Link.Text);
        }
    }
}