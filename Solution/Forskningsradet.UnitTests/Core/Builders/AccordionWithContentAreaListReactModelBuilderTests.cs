using System.Linq;
using FakeItEasy;
using Forskningsradet.Core.Builders;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Facades.Contracts;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Services;
using Xunit;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.UnitTests.Core.Builders
{
    public class AccordionWithContentAreaListReactModelBuilderTests
    {
        private readonly IRichTextReactModelBuilder _richTextReactModelBuilder;
        private readonly IContentAreaReactModelBuilder _contentAreaReactModelBuilder;
        private readonly IAccordionReactModelBuilder _accordionReactModelBuilder;
        private readonly IPageEditingAdapter _pageEditingAdapter;
        private readonly ILocalizationProvider _localizationProvider;
        private readonly AccordionWithContentAreaListReactModelBuilder _builder;

        public AccordionWithContentAreaListReactModelBuilderTests()
        {
            _richTextReactModelBuilder = A.Fake<IRichTextReactModelBuilder>();
            _contentAreaReactModelBuilder = A.Fake<IContentAreaReactModelBuilder>();
            _accordionReactModelBuilder = A.Fake<IAccordionReactModelBuilder>();
            _pageEditingAdapter = A.Fake<IPageEditingAdapter>();
            _localizationProvider = A.Fake<ILocalizationProvider>();
            _builder = new AccordionWithContentAreaListReactModelBuilder(
                _richTextReactModelBuilder,
                _contentAreaReactModelBuilder,
                _accordionReactModelBuilder,
                _pageEditingAdapter,
                _localizationProvider);
        }

        [Fact]
        public void BuildReactModel_ShouldMapAccordion()
        {
            var expectedAccordion = new ReactModels.Accordion();
            A.CallTo(() => _accordionReactModelBuilder.BuildReactModel(A<bool>._)).Returns(expectedAccordion);

            var result = _builder.BuildReactModel(new StructuredScheduleBlock());

            Assert.Same(expectedAccordion, result.Accordion);
        }

        [Fact]
        public void BuildReactModel_ShouldSetFirstElementInitiallyOpen()
        {
            var result = _builder.BuildReactModel(new StructuredScheduleBlock());

            Assert.True(result.Content.FirstOrDefault().InitiallyOpen);
        }

        [Fact]
        public void BuildReactModel_ShouldNotSetAllElementsInitiallyOpen()
        {
            var result = _builder.BuildReactModel(new StructuredScheduleBlock(), new StructuredScheduleBlock());

            Assert.False(result.Content.All(x => x.InitiallyOpen));
        }

        [Fact]
        public void BuildReactModel_WhenInEditMode_ShouldSetAllElementsInitiallyOpen()
        {
            A.CallTo(() => _pageEditingAdapter.PageIsInEditMode()).Returns(true);

            var result = _builder.BuildReactModel(new StructuredScheduleBlock(), new StructuredScheduleBlock());

            Assert.True(result.Content.All(x => x.InitiallyOpen));
        }
    }
}