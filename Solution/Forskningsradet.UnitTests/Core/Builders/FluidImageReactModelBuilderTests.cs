using EPiServer;
using EPiServer.Core;
using EPiServer.Web.Routing;
using FakeItEasy;
using Forskningsradet.Core.Builders;
using Forskningsradet.Core.Facades.Contracts;
using Forskningsradet.Core.Models.ContentModels.Media;
using Xunit;

namespace Forskningsradet.UnitTests.Core.Builders
{
    public class FluidImageReactModelBuilderTests
    {
        private readonly IUrlResolver _urlResolver;
        private readonly IContentLoader _contentLoader;
        private readonly IPageEditingAdapter _pageEditingAdapter;
        private readonly FluidImageReactModelBuilder _builder;

        public FluidImageReactModelBuilderTests()
        {
            _urlResolver = A.Fake<IUrlResolver>();
            _contentLoader = A.Fake<IContentLoader>();
            _pageEditingAdapter = A.Fake<IPageEditingAdapter>();
            _builder = new FluidImageReactModelBuilder(_urlResolver, _contentLoader, _pageEditingAdapter);
        }

        [Fact]
        public void BuildReactModel_WhenAllFieldsProvided_ShouldMapFields()
        {
            const string expectedAlt = "A";
            const string expectedSrc = "S";
            const string expectedImageName = "image";

            A.CallTo(() => _urlResolver.GetUrl(A<ContentReference>._, A<string>._, A<UrlResolverArguments>._))
                .Returns(expectedSrc);
            A.CallTo(() => _pageEditingAdapter.PageIsInEditMode()).Returns(true);


            var imageFile = new ImageFile
            {
                AltText = expectedAlt,
            };

            var result = _builder.BuildReactModel(imageFile, expectedImageName);

            Assert.Equal(expectedAlt, result.Alt);
            Assert.Equal(expectedSrc, result.Src);
            Assert.Equal(expectedImageName, result.OnPageEditing.Image);
        }

        [Fact]
        public void BuildReactModel_WhenImageFileIsNull_ShouldReturnNotNull()
        {
            A.CallTo(() => _pageEditingAdapter.PageIsInEditMode()).Returns(true);

            var result = _builder.BuildReactModel((ImageFile)null, null);

            Assert.NotNull(result);
        }

        [Fact]
        public void BuildReactModel_WhenImageFileIsNull_ShouldReturnEmptyAlt()
        {
            A.CallTo(() => _pageEditingAdapter.PageIsInEditMode()).Returns(true);

            var result = _builder.BuildReactModel((ImageFile)null, null);

            Assert.Equal(string.Empty, result.Alt);
        }

        [Fact]
        public void BuildReactModel_WhenImageFileIsNull_ShouldReturnEmptySrc()
        {
            A.CallTo(() => _pageEditingAdapter.PageIsInEditMode()).Returns(true);

            var result = _builder.BuildReactModel((ImageFile)null, null);

            Assert.Equal(string.Empty, result.Src);
        }

        [Fact]
        public void BuildReactModel_WhenPageIsInEditMode_ShouldMapOnPageEditing()
        {
            A.CallTo(() => _pageEditingAdapter.PageIsInEditMode()).Returns(true);

            var result = _builder.BuildReactModel((ImageFile)null, null);

            Assert.NotNull(result.OnPageEditing);
        }

        [Fact]
        public void BuildReactModel_WhenPropertyNameIsSet_ShouldMapToOnPageEditing()
        {
            A.CallTo(() => _pageEditingAdapter.PageIsInEditMode()).Returns(true);

            var expectedImageName = "im";
            var result = _builder.BuildReactModel((ImageFile)null, expectedImageName);

            Assert.Equal(expectedImageName, result.OnPageEditing.Image);
        }
    }
}