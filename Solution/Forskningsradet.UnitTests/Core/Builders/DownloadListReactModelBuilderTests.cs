﻿using System.Collections.Generic;
using EPiServer;
using EPiServer.Core;
using EPiServer.Globalization;
using EPiServer.Web.Routing;
using FakeItEasy;
using Forskningsradet.Core.Builders;
using Forskningsradet.Core.Models.ContentModels.Media;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Services;
using Xunit;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.UnitTests.Core.Builders
{
    public class DownloadListReactModelBuilderTests
    {
        private readonly IContentLoader _contentLoader;
        private readonly IUrlResolver _urlResolver;
        private readonly ILocalizationProvider _localizationProvider;
        private readonly DownloadListReactModelBuilder _builder;

        public DownloadListReactModelBuilderTests()
        {
            _contentLoader = A.Fake<IContentLoader>();
            _urlResolver = A.Fake<IUrlResolver>();
            _localizationProvider = A.Fake<ILocalizationProvider>();
            ContentLanguage.Instance = A.Fake<ContentLanguage>();
            _builder = new DownloadListReactModelBuilder(_contentLoader, _urlResolver, _localizationProvider);
        }

        [Fact]
        public void BuildReactModel_WhenAllFieldsProvided_ShouldMapField()
        {
            const string expectedDownloadAllText = "Download all";
            const string expectedDownloadProposalLabel = "proposal label";
            const string expectedDownloadTemplatesLabel = "templates label";
            const string expectedUrl = "url";
            const string pageName = "pagename";
            const string mediaName = "mediaName";
            const string expectedPageDownloadFileName = pageName + ".pdf";

            ArrangeTranslation(expectedDownloadAllText, "DownloadAll");
            ArrangeTranslation(expectedDownloadProposalLabel, "DownloadProposal");
            ArrangeTranslation(expectedDownloadTemplatesLabel, "DownloadTemplates");

            var proposalPage = A.Fake<ProposalPage>();
            proposalPage.Name = pageName;
            proposalPage.ApplicationTemplates = new List<ContentReference> { new ContentReference(2), new ContentReference(3) };
            
            var genericmedia = A.Fake<GenericMedia>();
            genericmedia.Name = mediaName;
            genericmedia.FileExtension = ".ppt";

            A.CallTo(() => _urlResolver.GetUrl(A<ContentReference>._, A<string>._, A<UrlResolverArguments>._))
                .Returns(expectedUrl);
            A.CallTo(() => _contentLoader.Get<GenericMedia>(A<ContentReference>._))
                .Returns(genericmedia);

            var result = _builder.BuildReactModel(proposalPage);

            Assert.Equal(expectedDownloadAllText, result.DownloadAllText);
            Assert.Equal(expectedDownloadProposalLabel, result.Groups[0].Heading);
            Assert.Equal(ReactModels.DocumentIcon_IconTheme.Pdf, result.Groups[0].Items[0].IconTheme);
            Assert.Equal(expectedPageDownloadFileName, result.Groups[0].Items[0].Text);
            Assert.Equal(expectedUrl, result.Groups[0].Items[0].Url);

            Assert.Equal(expectedDownloadTemplatesLabel, result.Groups[1].Heading);
            Assert.Equal(mediaName, result.Groups[1].Items[0].Text);
            Assert.Equal(ReactModels.DocumentIcon_IconTheme.Ppt, result.Groups[1].Items[0].IconTheme);
            Assert.Equal(expectedUrl, result.Groups[1].Items[0].Url);
        }

        [Fact]
        public void BuildTemplatesList_WhenAllFieldsProviced_ShouldMapAllFields()
        {
            const string expectedDownloadAllText = "Download all";
            const string expectedDownloadTemplatesLabel = "templates label";
            const string expectedUrl = "url";

            ArrangeTranslation(expectedDownloadAllText, "DownloadAll");
            ArrangeTranslation(expectedDownloadTemplatesLabel, "DownloadTemplates");

            var proposalPage = A.Fake<ProposalPage>();
            proposalPage.ApplicationTemplates = new List<ContentReference> { new ContentReference(2), new ContentReference(3) };

            var genericmedia = A.Fake<GenericMedia>();
            genericmedia.Name = "mediaName";
            genericmedia.FileExtension = ".doc";

            A.CallTo(() => _urlResolver.GetUrl(A<ContentReference>._, A<string>._, A<UrlResolverArguments>._))
                .Returns(expectedUrl);
            A.CallTo(() => _contentLoader.Get<GenericMedia>(A<ContentReference>._))
                .Returns(genericmedia);

            var result = _builder.BuildTemplatesList(proposalPage);

            Assert.Equal(expectedDownloadAllText, result.DownloadAllText);
            Assert.Equal(expectedDownloadTemplatesLabel, result.Groups[0].Heading);
            Assert.Equal(genericmedia.Name, result.Groups[0].Items[0].Text);
            Assert.Equal(ReactModels.DocumentIcon_IconTheme.Word, result.Groups[0].Items[0].IconTheme);
            Assert.Equal(expectedUrl, result.Groups[0].Items[0].Url);
        }

        private void ArrangeTranslation(string expected, string label) =>
            A.CallTo(() => _localizationProvider.GetLabel(nameof(ReactModels.DownloadList), label))
                .Returns(expected);
    }
}
