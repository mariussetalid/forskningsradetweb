﻿using EPiServer;
using EPiServer.Core;
using EPiServer.Web.Routing;
using FakeItEasy;
using Forskningsradet.Core.Builders;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ContentModels.Media;
using Forskningsradet.Core.Services.Contracts;
using Forskningsradet.UnitTests.Builders;
using Xunit;

namespace Forskningsradet.UnitTests.Core.Builders
{
    public class QuotesBlockReactModelBuilderTests
    {
        private readonly IContentLoader _contentLoader;
        private readonly IContentLoadHelper _contentLoadHelper;
        private readonly IUrlResolver _urlResolver;
        private readonly QuotesBlockReactModelBuilder _builder;

        public QuotesBlockReactModelBuilderTests()
        {
            _contentLoader = A.Fake<IContentLoader>();
            _urlResolver = A.Fake<IUrlResolver>();
            _contentLoadHelper = A.Fake<IContentLoadHelper>();
            _builder = new QuotesBlockReactModelBuilder(_contentLoader, _contentLoadHelper, _urlResolver);
        }

        [Fact]
        public void BuildReactModel_WithImageWithQuoteBlock_ShouldMapFields()
        {
            var expectedTitle = "title";
            var expectedUrl = "url";
            var expectedQuoteBy = "quotee";
            var expectedText = "text";
            var expectedAddQuoteDash = false;

            var imageFile = new ImageFile();
            A.CallTo(() => _contentLoader.TryGet(A<ContentReference>._, out imageFile))
                .Returns(true);
            A.CallTo(() => _urlResolver.GetUrl(A<ContentReference>._, A<string>._, A<UrlResolverArguments>._))
                .Returns(expectedUrl);
            A.CallTo(() => _contentLoadHelper.GetFilteredItems<NpImageWithQuoteBlock>(A<ContentArea>._))
                .Returns(
                    NpImageWithQuoteBlockBuilder.Create.Default()
                        .WithText(expectedText)
                        .WithQuoteBy(expectedQuoteBy)
                        .WithAddQuoteDash(expectedAddQuoteDash)
                        .AsList()
                );

            var result = _builder.BuildReactModel(
                    NpQuotesBlockBuilder.Create.Default()
                        .WithTitle(expectedTitle)
                        .WithQuotes(ContentAreaBuilder.Create.Default().WithDefaultFilteredItems())
            );

            Assert.Equal(expectedTitle, result.Title);
            Assert.Equal(expectedUrl, result.Quotes[0].Image.Src);
            Assert.Equal(expectedQuoteBy, result.Quotes[0].Quotee);
            Assert.Equal(expectedText, result.Quotes[0].Text);
            Assert.Equal(expectedAddQuoteDash, result.Quotes[0].AddQuoteDash);
        }
    }
}
