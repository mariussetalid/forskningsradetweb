using FakeItEasy;
using Forskningsradet.Core.Builders;
using Forskningsradet.Core.Services;
using Xunit;

namespace Forskningsradet.UnitTests.Core.Builders
{
    public class AccordionReactModelBuilderTests
    {
        private readonly ILocalizationProvider _localizationProvider;
        private readonly AccordionReactModelBuilder _builder;

        public AccordionReactModelBuilderTests()
        {
            _localizationProvider = A.Fake<ILocalizationProvider>();
            _builder = new AccordionReactModelBuilder(
                _localizationProvider);
        }

        [Theory]
        [InlineData(true)]
        [InlineData(false)]
        public void BuildReactModel_ShouldSetInitiallyOpen(bool initiallyOpen)
        {
            var result = _builder.BuildReactModel(initiallyOpen);

            Assert.Equal(initiallyOpen, result.InitiallyOpen);
        }
    }
}