using System.Collections.Generic;
using System.Linq;
using EPiServer;
using EPiServer.Core;
using EPiServer.Web.Routing;
using FakeItEasy;
using Forskningsradet.Core.Builders;
using Forskningsradet.Core.Models.ContentModels.Media;
using Forskningsradet.Core.Models.ContentModels.PropertyList;
using Xunit;

namespace Forskningsradet.UnitTests.Core.Builders
{
    public class ProcessReactModelBuilderTests
    {
        private readonly IContentLoader _contentLoader;
        private readonly IUrlResolver _urlResolver;
        private readonly ProcessReactModelBuilder _builder;

        public ProcessReactModelBuilderTests()
        {
            _contentLoader = A.Fake<IContentLoader>();
            _urlResolver = A.Fake<IUrlResolver>();
            _builder = new ProcessReactModelBuilder(
                _contentLoader,
                _urlResolver);
        }

        [Fact]
        public void BuildReactModel_WithText_ShouldMapIntroText()
        {
            const string expectedText = "t";

            var result = _builder.BuildReactModel(null, expectedText, null);

            Assert.Equal(expectedText, result.IntroText);
        }

        [Fact]
        public void BuildReactModel_WithCarouselStyle_ShouldMapIsCarousel()
        {
            var result = _builder.BuildReactModel(null, null, null, Forskningsradet.Core.Models.Enums.ProcessStyle.Carousel);

            Assert.True(result.IsCarousel);
        }

        [Fact]
        public void BuildReactModel_WithItemWithIngress_ShouldMapItemText()
        {
            const string expectedText = "t";

            var result = _builder.BuildReactModel(null, null, new List<ProcessItem> { new ProcessItem { Ingress = expectedText } }, Forskningsradet.Core.Models.Enums.ProcessStyle.Carousel);

            Assert.Equal(expectedText, result.Items.First().Text);
        }

        [Fact]
        public void BuildReactModel_WithItemWithTitle_ShouldMapItemTitle()
        {
            const string expectedTitle = "title";

            var result = _builder.BuildReactModel(null, null, new List<ProcessItem> { new ProcessItem { Title = expectedTitle} }, Forskningsradet.Core.Models.Enums.ProcessStyle.Carousel);

            Assert.Equal(expectedTitle, result.Items.First().Title);
        }

        [Fact]
        public void BuildReactModel_WithItemWithUrl_ShouldMapItemUrl()
        {
            const string expectedUrl = "#url";
            var url = new Url(expectedUrl);
            A.CallTo(() => _urlResolver.GetUrl(A<UrlBuilder>._, A<UrlResolverArguments>._)).Returns(expectedUrl);

            var result = _builder.BuildReactModel(null, null, new List<ProcessItem> { new ProcessItem { Url = url } }, Forskningsradet.Core.Models.Enums.ProcessStyle.Carousel);

            Assert.Equal(expectedUrl, result.Items.First().Url);
        }

        [Fact]
        public void BuildReactModel_WithItemWithIcon_ShouldMapItemIcon()
        {
            const string expectedAltText = "a";
            const string expectedSrc = "#";
            A.CallTo(() => _contentLoader.Get<ImageFile>(A<ContentReference>._))
                .Returns(new ImageFile { AltText = expectedAltText, ContentLink = new ContentReference() });
            A.CallTo(() => _urlResolver.GetUrl(A<ContentReference>._, A<string>._, A<UrlResolverArguments>._))
                .Returns(expectedSrc);

            var result = _builder.BuildReactModel(null, null, new List<ProcessItem> { new ProcessItem { Image = new ContentReference(1) } }, Forskningsradet.Core.Models.Enums.ProcessStyle.Carousel);

            Assert.Equal(expectedAltText, result.Items.First().Icon.Alt);
            Assert.Equal(expectedSrc, result.Items.First().Icon.Src);
        }
    }
}