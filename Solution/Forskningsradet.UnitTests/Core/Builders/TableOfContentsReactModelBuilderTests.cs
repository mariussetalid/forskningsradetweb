using System.Collections.Generic;
using EPiServer;
using EPiServer.Core;
using EPiServer.SpecializedProperties;
using EPiServer.Web.Routing;
using FakeItEasy;
using Forskningsradet.Core.Builders;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.RequestModels;
using Forskningsradet.Core.Services;
using Forskningsradet.Core.Services.Contracts;
using Forskningsradet.UnitTests.Builders;
using Xunit;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.UnitTests.Core.Builders
{
    public class TableOfContentsReactModelBuilderTests
    {
        private readonly IUrlResolver _urlResolver;
        private readonly IUrlCheckingService _urlCheckingService;
        private readonly IContentLoader _contentLoader;
        private readonly ILocalizationProvider _localizationProvider;
        private readonly TableOfContentsReactModelBuilder _builder;

        public TableOfContentsReactModelBuilderTests()
        {
            _urlResolver = A.Fake<IUrlResolver>();
            _urlCheckingService = A.Fake<IUrlCheckingService>();
            _contentLoader = A.Fake<IContentLoader>();
            _localizationProvider = A.Fake<ILocalizationProvider>();
            _builder = new TableOfContentsReactModelBuilder(
                _urlResolver,
                _urlCheckingService,
                _contentLoader,
                _localizationProvider);
        }

        [Fact]
        public void BuildReactModel_WhenAllFieldsProvided_ShouldMapFields()
        {
            const string expectedUrl = "url";
            const string expectedTitle = "T";
            const string expectedLinkUrl = "linkHref";
            const string expectedLinkText = "linkText";

            var page = new LargeDocumentPage
            {
                Title = "pageTitle",
                LinkItems = new LinkItemCollection(new List<LinkItem> {new LinkItem {Href = expectedLinkUrl, Text = expectedLinkText}})
            };

            A.CallTo(() => _contentLoader.Get<LargeDocumentPage>(A<ContentReference>._))
                .Returns(page);
            A.CallTo(() => _urlResolver.GetUrl(A<ContentReference>._, A<string>._, A<UrlResolverArguments>._))
                .Returns(expectedUrl);
            A.CallTo(() => _urlResolver.GetUrl(A<UrlBuilder>._, A<UrlResolverArguments>._))
                .Returns(expectedLinkUrl);

            ArrangeTranslation(expectedTitle, "TableOfContentsTitle");
            var node = NodeBuilder.Create.Default();
            var model = new LargeDocumentModel(false, 1, page.ContentLink, node, node, page.LinkItems);

            var result = _builder.BuildReactModel(model);

            Assert.Equal(expectedTitle, result.Title);
            Assert.Equal(page.Title, result.Items[0].Link.Link.Text);
            Assert.Equal(expectedUrl, result.Items[0].Link.Link.Url);

            var resultLink = result.Items[1].Link.Link;
            Assert.Equal(expectedLinkUrl, resultLink.Url);
            Assert.Equal(expectedLinkText, resultLink.Text);
        }

        [Fact]
        public void WhenFirstLevelChaptersExists_ShouldIncludeChapters()
        {
            var expectedCount = 3;
            Node rootNode = NodeBuilder.Create.Default();
            var chapterA = NodeBuilder.Create.Default().WithDepth(1).WithId(11).WithParentNode(rootNode);
            var chapterB = NodeBuilder.Create.Default().WithDepth(1).WithId(12).WithParentNode(rootNode);

            rootNode.AddChild(chapterA);
            rootNode.AddChild(chapterB);

            var rootPage = A.Fake<LargeDocumentPage>();
            rootPage.ContentLink = rootNode.ContentReference;
            var chapterPage = A.Fake<LargeDocumentPageBase>();
            chapterPage.ContentLink = new ContentReference(10);
            chapterPage.VisibleInMenu = true;

            A.CallTo(() => _contentLoader.Get<LargeDocumentPage>(A<ContentReference>._))
                .Returns(rootPage);
            A.CallTo(() => _contentLoader.Get<LargeDocumentPageBase>(A<ContentReference>._))
                .Returns(chapterPage);

            var model = new LargeDocumentModel(false, 4, rootNode.ContentReference, rootNode, rootNode);

            var result = _builder.BuildReactModel(model);

            Assert.Equal(expectedCount, result.Items.Count);
        }

        [Fact]
        public void WhenAncestorsExist_ShouldSetIsCurrentForAncestors()
        {
            Node rootNode = NodeBuilder.Create.Default();
            Node chapterA = NodeBuilder.Create.Default().WithDepth(1).WithId(11).WithParentNode(rootNode);
            Node chapterB = NodeBuilder.Create.Default().WithDepth(1).WithId(12).WithParentNode(rootNode);
            Node a1 = NodeBuilder.Create.Default().WithDepth(2).WithId(111).WithParentNode(chapterA);
            Node b1 = NodeBuilder.Create.Default().WithDepth(2).WithId(121).WithParentNode(chapterB);
            Node a11 = NodeBuilder.Create.Default().WithDepth(3).WithId(1111).WithParentNode(a1);
            Node a111 = NodeBuilder.Create.Default().WithDepth(4).WithId(11111).WithParentNode(a11);

            rootNode.AddChild(chapterA);
            rootNode.AddChild(chapterB);
            chapterA.AddChild(a1);
            chapterB.AddChild(b1);
            a1.AddChild(a11);
            a11.AddChild(a111);

            var rootPage = A.Fake<LargeDocumentPage>();
            rootPage.ContentLink = rootNode.ContentReference;
            var chapterPage = A.Fake<LargeDocumentPageBase>();
            chapterPage.ContentLink = new ContentReference(10);
            chapterPage.VisibleInMenu = true;

            A.CallTo(() => _contentLoader.Get<LargeDocumentPage>(A<ContentReference>._))
                .Returns(rootPage);
            A.CallTo(() => _contentLoader.Get<LargeDocumentPageBase>(A<ContentReference>._))
                .Returns(chapterPage);

            var model = new LargeDocumentModel(false, 4, rootNode.ContentReference, a111, rootNode);

            var result = _builder.BuildReactModel(model);

            var resultChapterA = result.Items[1];

            Assert.True(resultChapterA.LinkOrLinkList.IsCurrent);
            Assert.True(resultChapterA.LinkOrLinkList.LinkOrLinkList[0].LinkOrLinkList.IsCurrent);
            Assert.True(resultChapterA.LinkOrLinkList.LinkOrLinkList[0].LinkOrLinkList.LinkOrLinkList[0].LinkOrLinkList.IsCurrent);
            Assert.True(resultChapterA.LinkOrLinkList.LinkOrLinkList[0].LinkOrLinkList.LinkOrLinkList[0].LinkOrLinkList.Link.IsCurrent);
        }

        private void ArrangeTranslation(string expected, string label)
        {
            A.CallTo(() => _localizationProvider.GetLabel(nameof(LargeDocumentPage), label))
                .Returns(expected);
        }
    }
}