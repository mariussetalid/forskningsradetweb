﻿using EPiServer;
using EPiServer.Core;
using EPiServer.Web.Routing;
using FakeItEasy;
using Forskningsradet.Core.Builders;
using Forskningsradet.Core.Builders.Contracts;
using Forskningsradet.Core.Facades.Contracts;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ContentModels.Media;
using Forskningsradet.Core.Services.Contracts;
using Forskningsradet.UnitTests.Builders;
using Xunit;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.UnitTests.Core.Builders
{
    public class CampaignBlockReactModelBuilderTests
    {
        private readonly IFluidImageReactModelBuilder _fluidImageReactModelBuilder;
        private readonly IContentLoader _contentLoader;
        private readonly IUrlResolver _urlResolver;
        private readonly IUrlCheckingService _urlCheckingService;
        private readonly IPageEditingAdapter _pageEditingAdapter;
        private readonly CampaignBlockReactModelBuilder _builder;

        public CampaignBlockReactModelBuilderTests()
        {
            _fluidImageReactModelBuilder = A.Fake<IFluidImageReactModelBuilder>();
            _contentLoader = A.Fake<IContentLoader>();
            _urlResolver = A.Fake<IUrlResolver>();
            _urlCheckingService = A.Fake<IUrlCheckingService>();
            _pageEditingAdapter = A.Fake<IPageEditingAdapter>();
            _builder = new CampaignBlockReactModelBuilder(
                _fluidImageReactModelBuilder,
                _contentLoader,
                _urlResolver,
                _urlCheckingService,
                _pageEditingAdapter);
        }

        [Fact]
        public void BuildReactModel_WhenAllFieldsProvided_ShouldMapFields()
        {
            const string expectedTitle = "title";
            const string expectedButtonText = "link text";
            const string expectedEditLinkUrl = "edit link url";
            var expectedImage = new ReactModels.FluidImage
            {
                Alt = "image alt",
                Src = "image url"
            };

            A.CallTo(() => _urlResolver.GetUrl(A<UrlBuilder>.That.Matches(x => x.Path == expectedEditLinkUrl), A<UrlResolverArguments>._))
                .Returns(expectedEditLinkUrl);
            A.CallTo(() => _fluidImageReactModelBuilder.BuildReactModel(A<ImageFile>._, A<string>._))
                .Returns(expectedImage);

            CampaignBlock campaignBlock = CampaignBlockBuilder.Create.Default()
                .WithImage(new ContentReference())
                .WithTitle(expectedTitle)
                .WithButton(
                    new EditLinkBlock
                    {
                        Text = expectedButtonText,
                        Link = new Url(expectedEditLinkUrl)
                    }
                );

            var result = _builder.BuildReactModel(campaignBlock);

            Assert.Equal(expectedTitle, result.Title);
            Assert.Equal(expectedButtonText, result.Cta.Text);
            Assert.Equal(expectedEditLinkUrl, result.Cta.Url);
            Assert.Equal(expectedImage.Alt, result.Image.Alt);
            Assert.Equal(expectedImage.Src, result.Image.Src);
        }

        [Theory]
        [InlineData(CampaignBlockStyle.None, ReactModels.CampaignBlock_EditorTheme.None)]
        [InlineData(CampaignBlockStyle.DarkBlue, ReactModels.CampaignBlock_EditorTheme.DarkBlue)]
        public void BuildReactModel_GivenStyle_ShouldMapCorrectEditorTheme(CampaignBlockStyle style, ReactModels.CampaignBlock_EditorTheme expectedTheme)
        {
            CampaignBlock campaignBlock = CampaignBlockBuilder.Create.Default()
                .WithStyle(style);

            var result = _builder.BuildReactModel(campaignBlock);

            Assert.Equal(expectedTheme, result.EditorTheme);
        }

        [Fact]
        public void BuildReactModel_WhenButtonIsNotSet_ShouldMapNotEmptyLink()
        {
            CampaignBlock campaignBlock = CampaignBlockBuilder.Create.Default()
                .WithButton(null);

            var result = _builder.BuildReactModel(campaignBlock);

            Assert.NotNull(result.Cta);
        }

        [Fact]
        public void BuildReactModel_WhenPageIsInEditMode_ShouldMapOnPageEditing()
        {
            CampaignBlock campaignBlock = CampaignBlockBuilder.Create.Default();
            A.CallTo(() => _pageEditingAdapter.PageIsInEditMode()).Returns(true);
            A.CallTo(() => _fluidImageReactModelBuilder.BuildReactModel(A<ImageFile>._, A<string>._))
                .Returns(new ReactModels.FluidImage { OnPageEditing = new ReactModels.FluidImage_OnPageEditing() });

            var result = _builder.BuildReactModel(campaignBlock);

            Assert.NotNull(result.OnPageEditing);
            Assert.NotNull(result.Cta.OnPageEditing);
            Assert.NotNull(result.Image.OnPageEditing);
        }
    }
}
