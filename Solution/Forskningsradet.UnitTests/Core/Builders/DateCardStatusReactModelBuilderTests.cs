using System.Linq;
using FakeItEasy;
using Forskningsradet.Core.Builders;
using Forskningsradet.Core.Models.ApiModels.Evurdering;
using Forskningsradet.Core.Services;
using Xunit;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.UnitTests.Core.Builders
{
    public class DateCardStatusReactModelBuilderTests
    {
        private readonly ILocalizationProvider _localizationProvider;
        private readonly DateCardStatusReactModelBuilder _mapper;

        public DateCardStatusReactModelBuilderTests()
        {
            _localizationProvider = A.Fake<ILocalizationProvider>();
            _mapper = new DateCardStatusReactModelBuilder(_localizationProvider);
        }

        [Theory]
        [InlineData(ProposalState.Active, ReactModels.DateCardStatus_Theme.IsActive)]
        [InlineData(ProposalState.Completed, ReactModels.DateCardStatus_Theme.IsCompleted)]
        [InlineData(ProposalState.Cancelled, ReactModels.DateCardStatus_Theme.IsCanceled)]
        [InlineData(ProposalState.Planned, ReactModels.DateCardStatus_Theme.IsPlanned)]
        public void BuildReactModel_GivenProposalState_ShouldReturnCorrectStatusTheme(ProposalState proposalState, ReactModels.DateCardStatus_Theme expectedTheme)
        {
            var result = _mapper.BuildReactModel(proposalState);

            Assert.Equal(expectedTheme, result.Theme);
        }

        [Fact]
        public void BuildDateCardStatusList_WhenProposalStateIsCompletedAndHasApplicationResult_ShouldMapStatusesCompletedAndResultPublished()
        {
            var result = _mapper.BuildDateCardStatusList(ProposalState.Completed, hasResultStatus: true).ToList();

            Assert.Equal(ReactModels.DateCardStatus_Theme.IsCompleted, result[0]?.Theme);
            Assert.Equal(ReactModels.DateCardStatus_Theme.ResultIsPublished, result[1]?.Theme);
        }

        [Theory]
        [InlineData(ProposalState.Active, ReactModels.DateCardStatus_Theme.IsActive)]
        [InlineData(ProposalState.Completed, ReactModels.DateCardStatus_Theme.IsCompleted)]
        [InlineData(ProposalState.Cancelled, ReactModels.DateCardStatus_Theme.IsCanceled)]
        [InlineData(ProposalState.Planned, ReactModels.DateCardStatus_Theme.IsPlanned)]
        public void BuildDateCardStatusList_GivenProposalState_ShouldReturnCorrectStatusTheme(ProposalState proposalState, ReactModels.DateCardStatus_Theme expectedTheme)
        {
            var result = _mapper.BuildDateCardStatusList(proposalState, false).ToList();

            Assert.Equal(expectedTheme, result.First().Theme);
        }

        [Theory]
        [InlineData(ProposalState.Active)]
        [InlineData(ProposalState.Completed)]
        [InlineData(ProposalState.Cancelled)]
        [InlineData(ProposalState.Planned)]
        public void BuildDateCardStatusList_GivenProposalState_ShouldReturnStatusLabel(ProposalState proposalState)
        {
            const string expectedStatusLabel = "label";

            ArrangeTranslation(expectedStatusLabel);

            var result = _mapper.BuildDateCardStatusList(proposalState, false).ToList();

            Assert.Equal(expectedStatusLabel, result.First().Text);
        }

        private void ArrangeTranslation(string expected) =>
            A.CallTo(() => _localizationProvider.GetLabel(nameof(ReactModels.DateCardStatus), A<string>._))
                .Returns(expected);
    }
}