using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.Find.UnifiedSearch;
using EPiServer.Web.Routing;
using FakeItEasy;
using Forskningsradet.Core.Builders;
using Forskningsradet.Core.Models.Enums;
using Forskningsradet.Core.Services;
using Forskningsradet.UnitTests.Builders;
using Xunit;
using ReactModels = Forskningsradet.Core.Models.ReactModels;

namespace Forskningsradet.UnitTests.Core.Builders
{
    public class PublicationReactModelBuilderTests
    {
        private readonly IContentLoader _contentLoader;
        private readonly IUrlResolver _urlResolver;
        private readonly ILocalizationProvider _localizationProvider;
        private readonly CategoryRepository _categoryRepository;
        private readonly PublicationReactModelBuilder _builder;

        public PublicationReactModelBuilderTests()
        {
            _contentLoader = A.Fake<IContentLoader>();
            _urlResolver = A.Fake<IUrlResolver>();
            _localizationProvider = A.Fake<ILocalizationProvider>();
            _categoryRepository = A.Fake<CategoryRepository>();
            _builder = new PublicationReactModelBuilder(
                _contentLoader,
                _urlResolver,
                _localizationProvider,
                _categoryRepository
                );
        }

        [Fact]
        public void BuildReactModel_WhenAllFieldsProvided_ShouldMapFields()
        {
            var expectedTitle = "Title";
            var expectedUrl = "url";
            var expectedText = "text";
            var expectedIconTheme = ReactModels.DocumentIcon_IconTheme.Pdf;

            var publicationPage = PublicationPageBuilder.Create.Default()
                .WithPageName(expectedTitle)
                .WithAttachment(new ContentReference(1))
                .WithSubtitle(expectedText);

            A.CallTo(() => _urlResolver.GetUrl(A<ContentReference>._, A<string>._, A<UrlResolverArguments>._))
                .Returns(expectedUrl);

            var result = _builder.BuildReactModel(publicationPage, renderOption: PublicationMetadata.Standard, showMetadataPdfIcon: false);

            Assert.Equal(expectedTitle, result.Title);
            Assert.Equal(expectedUrl, result.Url);
            Assert.Equal(expectedText, result.Text);
            Assert.Equal(expectedIconTheme, result.Icon.IconTheme);
            Assert.NotNull(result.Metadata.Items);
        }

        [Fact]
        public void BuildReactModel_WithHistoricProposalPage_WhenAllFieldsProvided_ShouldMapFields()
        {
            var expectedTitle = "Title";
            var expectedUrl = "url";
            var expectedText = "text";
            var expectedIconTheme = ReactModels.DocumentIcon_IconTheme.Pdf;

            var historicProposalPage = HistoricProposalPageBuilder.Create.Default()
                .WithPageName(expectedTitle)
                .WithAttachment(new ContentReference(1))
                .WithSubtitle(expectedText);

            A.CallTo(() => _urlResolver.GetUrl(A<ContentReference>._, A<string>._, A<UrlResolverArguments>._))
                .Returns(expectedUrl);

            var result = _builder.BuildReactModel(historicProposalPage, renderOption: PublicationMetadata.Standard, showMetadataPdfIcon: false);

            Assert.Equal(expectedTitle, result.Title);
            Assert.Equal(expectedUrl, result.Url);
            Assert.Equal(expectedText, result.Text);
            Assert.Equal(expectedIconTheme, result.Icon.IconTheme);
            Assert.NotNull(result.Metadata.Items);
        }

        [Fact]
        public void BuildReactModel_WithUnifiedSearchHit_ShouldMapExcerptAsText()
        {
            var expectedText = "text";
            var publicationPage = PublicationPageBuilder.Create.Default();
            var hit = new UnifiedSearchHit
            {
                Excerpt = expectedText
            };

            var result = _builder.BuildReactModel(publicationPage, renderOption: PublicationMetadata.Standard, showMetadataPdfIcon: false, hit.Excerpt);

            Assert.Equal(expectedText, result.Text);
        }

        [Fact]
        public void BuildReactModel_WithPublicationMetadataSmall_ShouldMapSingleMetadata()
        {
            var publicationPage = PublicationPageBuilder.Create.Default();
            var renderOption = PublicationMetadata.Small;

            var result = _builder.BuildReactModel(publicationPage, renderOption, showMetadataPdfIcon: false);

            Assert.Single(result.Metadata.Items);
        }

        [Fact]
        public void BuildReactModel_WithPublicationMetadataStandard_ShouldMapMetadataWithAuthor()
        {
            var expectedText = "author";
            var publicationPage = PublicationPageBuilder.Create.Default()
                .WithAuthor(expectedText);
            var renderOption = PublicationMetadata.Standard;

            var result = _builder.BuildReactModel(publicationPage, renderOption, showMetadataPdfIcon: false);

            Assert.Contains(result.Metadata.Items, x => x.Text == expectedText);
        }

        [Fact]
        public void BuildReactModel_WithPublicationMetadataLarge_ShouldMapMetadataWithPublisher()
        {
            var expectedText = "publisher";
            var publicationPage = PublicationPageBuilder.Create.Default()
                .WithPublisher(expectedText);
            var renderOption = PublicationMetadata.Large;

            var result = _builder.BuildReactModel(publicationPage, renderOption, showMetadataPdfIcon: false);

            Assert.Contains(result.Metadata.Items, x => x.Text == expectedText);
        }
    }
}