﻿using Forskningsradet.Core.Services;
using Forskningsradet.Core.Services.Contracts;
using Xunit;
namespace Forskningsradet.UnitTests.Core.Services
{
    public class HtmlExtractorTests
    {
        private readonly IHtmlExtractor _sut;

        public HtmlExtractorTests()
        {
            _sut = new HtmlExtractor();
        }

        [Theory]
        [InlineData("<div>Result<div>InnerResult</div></div>", "ResultInnerResult")]
        [InlineData("<div>Result</div>", "Result")]
        public void ExtractTextFromFirstDiv_ShouldReturnContentOfDiv_WhenInputIsValid(string html, string expected)
        {
            var result = _sut.ExtractTextFromFirstDiv(html);

            Assert.Equal(expected, result);
        }

        [Theory]
        [InlineData("<div>Result")]
        [InlineData("Result</div>")]
        [InlineData(null)]
        public void ExtractTextFromFirstDiv_ShouldReturnStringEmpty_WhenInputStringIsInvalid(string html)
        {
            var result = _sut.ExtractTextFromFirstDiv(html);

            Assert.Equal(string.Empty, result);
        }
    }
}
