﻿using System;
using Forskningsradet.Core.Models.ApiModels.Evurdering;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.SearchModels;
using Forskningsradet.Core.Services;
using Forskningsradet.UnitTests.Builders;
using Xunit;

namespace Forskningsradet.UnitTests.Core.Services
{
    public class ProposalPageStatusHandlerTests
    {
        private readonly ProposalPageStatusHandler _handler;

        public ProposalPageStatusHandlerTests()
        {
            _handler = new ProposalPageStatusHandler();
        }

        [Fact]
        public void GetStatus_ShouldReturn_Result_WhenProposal_HasApplicationResult()
        {
            ProposalBasePage proposal = ProposalPageBuilder.Create.Default()
                .WithNonEmptyApplicationResult();

            var result = _handler.GetStatus(proposal);

            Assert.Equal(TimeFrameFilter.Result, result);
        }

        [Fact]
        public void GetStatus_ShouldReturn_Future_WhenProposalStateIs_Active()
        {
            ProposalBasePage proposal = ProposalPageBuilder.Create.Default()
                .WithProposalState(ProposalState.Active);

            var result = _handler.GetStatus(proposal);

            Assert.Equal(TimeFrameFilter.Future, result);
        }

        [Fact]
        public void GetStatus_ShouldReturn_Future_WhenProposalStateIs_Planned()
        {
            ProposalBasePage proposal = ProposalPageBuilder.Create.Default()
                .WithProposalState(ProposalState.Planned);

            var result = _handler.GetStatus(proposal);

            Assert.Equal(TimeFrameFilter.Future, result);
        }

        [Fact]
        public void GetStatus_ShouldReturn_Future_WhenProposalStateIs_Cancelled_And_NotHiddenOnActiveTab_And_DeadlineInFuture()
        {

            ProposalBasePage proposal = ProposalPageBuilder.Create.Default()
                .WithProposalState(ProposalState.Cancelled)
                .WithCancellationShouldHideFromActiveTab(false)
                .WithDeadline(DateTime.Now.AddDays(1))
                .WithDeadlineType(DeadlineType.Date);

            var result = _handler.GetStatus(proposal);

            Assert.Equal(TimeFrameFilter.Future, result);
        }

        [Fact]
        public void GetStatus_ShouldReturn_Future_WhenProposalStateIs_Cancelled_And_NotHiddenOnActiveTab_And_HasContinuousDeadlineType()
        {
            ProposalBasePage proposal = ProposalPageBuilder.Create.Default()
                .WithProposalState(ProposalState.Cancelled)
                .WithCancellationShouldHideFromActiveTab(false)
                .WithDeadlineType(DeadlineType.Continuous);

            var result = _handler.GetStatus(proposal);

            Assert.Equal(TimeFrameFilter.Future, result);
        }

        [Fact]
        public void GetStatus_ShouldReturn_Past_WhenProposalStateIs_Completed()
        {
            ProposalBasePage proposal = ProposalPageBuilder.Create.Default()
                .WithProposalState(ProposalState.Completed);

            var result = _handler.GetStatus(proposal);

            Assert.Equal(TimeFrameFilter.Past, result);
        }

        [Fact]
        public void GetStatus_ShouldReturn_Past_WhenProposalStateIs_Deleted()
        {
            ProposalBasePage proposal = ProposalPageBuilder.Create.Default()
                .WithProposalState(ProposalState.Deleted);

            var result = _handler.GetStatus(proposal);

            Assert.Equal(TimeFrameFilter.Past, result);
        }

        [Fact]
        public void GetStatus_ShouldReturn_Past_WhenProposalStateIs_Cancelled_IfItIs_HiddenFromActiveTab()
        {
            ProposalBasePage proposal = ProposalPageBuilder.Create.Default()
                .WithProposalState(ProposalState.Cancelled)
                .WithCancellationShouldHideFromActiveTab(true)
                .WithDeadline(DateTime.Now.AddDays(1))
                .WithDeadlineType(DeadlineType.Continuous);

            var result = _handler.GetStatus(proposal);

            Assert.Equal(TimeFrameFilter.Past, result);
        }

        [Fact]
        public void GetStatus_ShouldReturn_Past_WhenProposalStateIs_Cancelled_With_DeadlineInPast_EvenIfItIsNot_HiddenFromActiveTab()
        {
            ProposalBasePage proposal = ProposalPageBuilder.Create.Default()
                .WithProposalState(ProposalState.Cancelled)
                .WithCancellationShouldHideFromActiveTab(false)
                .WithDeadline(DateTime.Now.AddDays(-1))
                .WithDeadlineType(DeadlineType.Date);

            var result = _handler.GetStatus(proposal);

            Assert.Equal(TimeFrameFilter.Past, result);
        }

        [Fact]
        public void GetStatus_ShouldReturn_Past_WhenProposalStateIs_Cancelled_With_DeadlineInPast_And_NotSetDeadlineType_EvenIfItIsNot_HiddenFromActiveTab()
        {
            ProposalBasePage proposal = ProposalPageBuilder.Create.Default()
                .WithProposalState(ProposalState.Cancelled)
                .WithCancellationShouldHideFromActiveTab(false)
                .WithDeadline(DateTime.Now.AddDays(-1))
                .WithDeadlineType(DeadlineType.NotSet);

            var result = _handler.GetStatus(proposal);

            Assert.Equal(TimeFrameFilter.Past, result);
        }
    }
}
