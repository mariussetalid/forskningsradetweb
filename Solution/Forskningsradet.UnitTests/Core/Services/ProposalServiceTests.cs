using System.Collections.Generic;
using System.Linq;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.Find;
using FakeItEasy;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Repositories.Contracts;
using Forskningsradet.Core.Services;
using Forskningsradet.Core.Services.Contracts;
using Xunit;

namespace Forskningsradet.UnitTests.Core.Services
{
    public class ProposalServiceTests
    {
        [Fact]
        public void GetSortedSubjects_ShouldReturnSubjectBlockOrder()
        {
            var expectedSubjectA = new SubjectBlock {Subject = 10};
            var expectedSubjectB = new SubjectBlock {Subject = 20};
            
            var subjectBlockReferenceA = new ContentReference(1);
            var subjectBlockReferenceB = new ContentReference(2);
            
            var subjectBlocks = A.Fake<ContentArea>();
            A.CallTo(() => subjectBlocks.FilteredItems)
                .Returns(new List<ContentAreaItem>
                {
                    new ContentAreaItem {ContentLink = subjectBlockReferenceA},
                    new ContentAreaItem {ContentLink = subjectBlockReferenceB}
                });
            
            var page = A.Fake<ProposalPage>();
            page.SubjectBlocks = subjectBlocks;

            var contentLoader = A.Fake<IContentLoader>();
            A.CallTo(() => contentLoader.Get<SubjectBlock>(
                    A<ContentReference>.That.Matches(x => x == subjectBlockReferenceA)))
                .Returns(expectedSubjectA);
            A.CallTo(() => contentLoader.Get<SubjectBlock>(
                    A<ContentReference>.That.Matches(x => x == subjectBlockReferenceB)))
                .Returns(expectedSubjectB);

            var service = new ProposalService(
                A.Fake<IClient>(), 
                A.Fake<IPageStructureService>(),
                A.Fake<IPageRepository>(), 
                contentLoader, 
                A.Fake<ICategoryGroupService>(),
                A.Fake<CategoryRepository>(),
                A.Fake<ILocalizationProvider>(),
                A.Fake<IProposalPageStatusHandler>());

            var result = service.GetSortedSubjects(page).ToList();
            
            Assert.Equal(2, result.Count);
            Assert.Equal(expectedSubjectA.Subject, result[0].Subject);
            Assert.Equal(expectedSubjectB.Subject, result[1].Subject);
        }
    }
}