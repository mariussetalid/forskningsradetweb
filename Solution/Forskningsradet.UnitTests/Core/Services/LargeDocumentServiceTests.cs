using FakeItEasy;
using Forskningsradet.Core.Models.RequestModels;
using Forskningsradet.Core.Services;
using Forskningsradet.UnitTests.Builders;
using Xunit;

namespace Forskningsradet.UnitTests.Core.Services
{
    public class LargeDocumentServiceTests
    {
        private readonly IPageStructureService _pageStructureService;
        private readonly LargeDocumentService _service;

        public LargeDocumentServiceTests()
        {
            _pageStructureService = A.Fake<IPageStructureService>();
            _service = new LargeDocumentService(_pageStructureService);
        }

        [Fact]
        public void GetNextPage_WithNextSiblingAndAtCorrectDepth_ShouldReturnNextSibling()
        {
            var expectedId = 12;
            Node rootNode = NodeBuilder.Create.Default();
            Node chapterA = NodeBuilder.Create.Default().WithDepth(1).WithId(11).WithParentNode(rootNode);
            Node chapterB = NodeBuilder.Create.Default().WithDepth(1).WithId(expectedId).WithParentNode(rootNode);

            rootNode.AddChild(chapterA);
            rootNode.AddChild(chapterB);

            var model = new LargeDocumentModel(false, 1, rootNode.ContentReference, chapterA, rootNode);

            var result = _service.GetNextPage(model);

            Assert.Equal(expectedId, result.ID);
        }

        [Fact]
        public void GetNextPage_WithoutNextSiblingAndAtCorrectDepth_ShouldReturnNextSiblingOfParent()
        {
            var expectedId = 12;
            Node rootNode = NodeBuilder.Create.Default();
            Node chapterA = NodeBuilder.Create.Default().WithDepth(1).WithId(11).WithParentNode(rootNode);
            Node chapterB = NodeBuilder.Create.Default().WithDepth(1).WithId(expectedId).WithParentNode(rootNode);
            Node a1 = NodeBuilder.Create.Default().WithDepth(2).WithId(111).WithParentNode(chapterA);

            rootNode.AddChild(chapterA);
            rootNode.AddChild(chapterB);
            chapterA.AddChild(a1);

            var model = new LargeDocumentModel(false, 2, rootNode.ContentReference, a1, rootNode);

            var result = _service.GetNextPage(model);

            Assert.Equal(expectedId, result.ID);
        }

        [Fact]
        public void GetNextPage_AtHigherDepthWithChildrenAndSibling_ShouldReturnFirstChild()
        {
            var expectedId = 1111;
            Node rootNode = NodeBuilder.Create.Default();
            Node chapterA = NodeBuilder.Create.Default().WithDepth(1).WithId(11).WithParentNode(rootNode);
            Node chapterB = NodeBuilder.Create.Default().WithDepth(1).WithId(12).WithParentNode(rootNode);
            Node a1 = NodeBuilder.Create.Default().WithDepth(2).WithId(111).WithParentNode(chapterA);
            Node a2 = NodeBuilder.Create.Default().WithDepth(2).WithId(112).WithParentNode(chapterA);
            Node a11 = NodeBuilder.Create.Default().WithDepth(3).WithId(expectedId).WithParentNode(a1);
            Node a12 = NodeBuilder.Create.Default().WithDepth(3).WithId(1112).WithParentNode(a1);

            rootNode.AddChild(chapterA);
            rootNode.AddChild(chapterB);
            chapterA.AddChild(a1);
            chapterA.AddChild(a2);
            a1.AddChild(a11);
            a1.AddChild(a12);

            var model = new LargeDocumentModel(false, 3, rootNode.ContentReference, a1, rootNode);

            var result = _service.GetNextPage(model);

            Assert.Equal(expectedId, result.ID);
        }

        [Fact]
        public void GetNextPage_FromLastPageOfChapterAtDeepestDepth_ShouldReturnHighestPageOfNextChapter()
        {
            var expectedId = 12;
            Node rootNode = NodeBuilder.Create.Default();
            Node chapterA = NodeBuilder.Create.Default().WithDepth(1).WithId(11).WithParentNode(rootNode);
            Node chapterB = NodeBuilder.Create.Default().WithDepth(1).WithId(expectedId).WithParentNode(rootNode);
            Node a1 = NodeBuilder.Create.Default().WithDepth(2).WithId(111).WithParentNode(chapterA);
            Node b1 = NodeBuilder.Create.Default().WithDepth(2).WithId(121).WithParentNode(chapterB);
            Node a11 = NodeBuilder.Create.Default().WithDepth(3).WithId(1111).WithParentNode(a1);
            Node a111 = NodeBuilder.Create.Default().WithDepth(4).WithId(11111).WithParentNode(a11);

            rootNode.AddChild(chapterA);
            rootNode.AddChild(chapterB);
            chapterA.AddChild(a1);
            chapterB.AddChild(b1);
            a1.AddChild(a11);
            a11.AddChild(a111);

            var model = new LargeDocumentModel(false, 4, rootNode.ContentReference, a111, rootNode);

            var result = _service.GetNextPage(model);

            Assert.Equal(expectedId, result.ID);
        }

        [Fact]
        public void GetNextPage_FromLastPageAtDeepestDepth_ShouldReturnEmptyReference()
        {
            var expectedId = 0;
            Node rootNode = NodeBuilder.Create.Default();
            Node chapterA = NodeBuilder.Create.Default().WithDepth(1).WithId(11).WithParentNode(rootNode);
            Node a1 = NodeBuilder.Create.Default().WithDepth(2).WithId(111).WithParentNode(chapterA);
            Node a11 = NodeBuilder.Create.Default().WithDepth(3).WithId(1111).WithParentNode(a1);
            Node a111 = NodeBuilder.Create.Default().WithDepth(4).WithId(11111).WithParentNode(a11);

            rootNode.AddChild(chapterA);
            chapterA.AddChild(a1);
            a1.AddChild(a11);
            a11.AddChild(a111);

            var model = new LargeDocumentModel(false, 4, rootNode.ContentReference, a111, rootNode);

            var result = _service.GetNextPage(model);

            Assert.Equal(expectedId, result.ID);
        }

        [Fact]
        public void GetNextPage_FromRootNodeWithDepthZero_ShouldReturnEmptyReference()
        {
            var expectedId = 0;
            Node rootNode = NodeBuilder.Create.Default();
            Node chapterA = NodeBuilder.Create.Default().WithDepth(1).WithId(11).WithParentNode(rootNode);
            Node chapterB = NodeBuilder.Create.Default().WithDepth(1).WithId(expectedId).WithParentNode(rootNode);

            rootNode.AddChild(chapterA);
            rootNode.AddChild(chapterB);

            var model = new LargeDocumentModel(false, 0, rootNode.ContentReference, rootNode, rootNode);

            var result = _service.GetNextPage(model);

            Assert.Equal(expectedId, result.ID);
        }

        [Fact]
        public void GetPreviousPage_WithPreviousSiblingAndAtCorrectDepth_ShouldReturnPreviousSibling()
        {
            var expectedId = 11;
            Node rootNode = NodeBuilder.Create.Default();
            Node chapterA = NodeBuilder.Create.Default().WithDepth(1).WithId(expectedId).WithParentNode(rootNode);
            Node chapterB = NodeBuilder.Create.Default().WithDepth(1).WithId(12).WithParentNode(rootNode);

            rootNode.AddChild(chapterA);
            rootNode.AddChild(chapterB);

            var model = new LargeDocumentModel(false, 1, rootNode.ContentReference, chapterB, rootNode);

            var result = _service.GetPreviousPage(model);

            Assert.Equal(expectedId, result.ID);
        }

        [Fact]
        public void GetPreviousPage_WithPreviousSiblingAndAtHigherDepth_ShouldReturnLastPageAtCorrectDepthOfPreviousSibling()
        {
            var expectedId = 111;
            Node rootNode = NodeBuilder.Create.Default();
            Node chapterA = NodeBuilder.Create.Default().WithDepth(1).WithId(11).WithParentNode(rootNode);
            Node chapterB = NodeBuilder.Create.Default().WithDepth(1).WithId(12).WithParentNode(rootNode);
            Node a1 = NodeBuilder.Create.Default().WithDepth(2).WithId(expectedId).WithParentNode(chapterA);
            Node b1 = NodeBuilder.Create.Default().WithDepth(2).WithId(121).WithParentNode(chapterB);
            Node a11 = NodeBuilder.Create.Default().WithDepth(3).WithId(1111).WithParentNode(a1);
            Node a111 = NodeBuilder.Create.Default().WithDepth(4).WithId(11111).WithParentNode(a11);

            rootNode.AddChild(chapterA);
            rootNode.AddChild(chapterB);
            chapterA.AddChild(a1);
            chapterB.AddChild(b1);
            a1.AddChild(a11);
            a11.AddChild(a111);

            var model = new LargeDocumentModel(false, 2, rootNode.ContentReference, chapterB, rootNode);

            var result = _service.GetPreviousPage(model);

            Assert.Equal(expectedId, result.ID);
        }

        [Fact]
        public void GetPreviousPage_WithoutPreviousSibling_ShouldReturnParent()
        {
            var expectedId = 11;
            Node rootNode = NodeBuilder.Create.Default();
            Node chapterA = NodeBuilder.Create.Default().WithDepth(1).WithId(expectedId).WithParentNode(rootNode);
            Node chapterB = NodeBuilder.Create.Default().WithDepth(1).WithId(12).WithParentNode(rootNode);
            Node a1 = NodeBuilder.Create.Default().WithDepth(2).WithId(111).WithParentNode(chapterA);

            rootNode.AddChild(chapterA);
            rootNode.AddChild(chapterB);
            chapterA.AddChild(a1);

            var model = new LargeDocumentModel(false, 4, rootNode.ContentReference, a1, rootNode);

            var result = _service.GetPreviousPage(model);

            Assert.Equal(expectedId, result.ID);
        }

        [Fact]
        public void GetPreviousPage_FromHighestPageOfLastChapter_ShouldReturnLastPageOfPreviousChapterAtDeepestDepth()
        {
            var expectedId = 11111;
            Node rootNode = NodeBuilder.Create.Default();
            Node chapterA = NodeBuilder.Create.Default().WithDepth(1).WithId(11).WithParentNode(rootNode);
            Node chapterB = NodeBuilder.Create.Default().WithDepth(1).WithId(12).WithParentNode(rootNode);
            Node a1 = NodeBuilder.Create.Default().WithDepth(2).WithId(111).WithParentNode(chapterA);
            Node b1 = NodeBuilder.Create.Default().WithDepth(2).WithId(121).WithParentNode(chapterB);
            Node a11 = NodeBuilder.Create.Default().WithDepth(3).WithId(1111).WithParentNode(a1);
            Node a111 = NodeBuilder.Create.Default().WithDepth(4).WithId(expectedId).WithParentNode(a11);

            rootNode.AddChild(chapterA);
            rootNode.AddChild(chapterB);
            chapterA.AddChild(a1);
            chapterB.AddChild(b1);
            a1.AddChild(a11);
            a11.AddChild(a111);

            var model = new LargeDocumentModel(false, 4, rootNode.ContentReference, chapterB, rootNode);

            var result = _service.GetPreviousPage(model);

            Assert.Equal(expectedId, result.ID);
        }

        [Fact]
        public void GetPreviousPage_WithoutPreviousSiblingAndWithoutParent_ShouldReturnEmptyReference()
        {
            var expectedId = 0;
            Node rootNode = NodeBuilder.Create.Default();

            var model = new LargeDocumentModel(false, 1, rootNode.ContentReference, rootNode, rootNode);

            var result = _service.GetPreviousPage(model);

            Assert.Equal(expectedId, result.ID);
        }
    }
}