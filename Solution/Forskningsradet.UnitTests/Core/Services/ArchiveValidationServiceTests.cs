using FakeItEasy;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Services;
using Xunit;

namespace Forskningsradet.UnitTests.Core.Services
{
    public class ArchiveValidationServiceTests
    {
        private readonly ArchiveValidationService _validator;

        public ArchiveValidationServiceTests() => 
            _validator = new ArchiveValidationService();

        [Fact]
        public void Validate_WithValidFields_ShouldReturnResultWithoutErrors()
        {
            var page = A.Fake<EditorialPage>();
            page.IsArchivable = true;
            page.CaseResponsible = "abc";
            
            var result = _validator.Validate(page);
            
            Assert.False(result.HasErrors);
        }
        
        [Fact]
        public void Validate_WithUnderscoreInCaseResponsible_ShouldReturnResultWithoutErrors()
        {
            var page = A.Fake<EditorialPage>();
            page.IsArchivable = true;
            page.CaseResponsible = "ext_abc";
            
            var result = _validator.Validate(page);
            
            Assert.False(result.HasErrors);
        }

        [Fact]
        public void Validate_WhenNotArchivable_ShouldReturnResultWithoutErrors()
        {
            var page = A.Fake<EditorialPage>();
            page.IsArchivable = false;
            page.CaseResponsible = null;
            
            var result = _validator.Validate(page);
            
            Assert.False(result.HasErrors);
        }
        
        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData("a-a")]
        [InlineData("a@b.c")]
        [InlineData("   ")]
        public void Validate_WithInvalidCaseReponsible_ShouldReturnErrors(string caseResponsible)
        {
            var page = A.Fake<EditorialPage>();
            page.IsArchivable = true;
            page.CaseResponsible = caseResponsible;
            
            var result = _validator.Validate(page);
            
            Assert.True(result.HasErrors);
        }
    }
}