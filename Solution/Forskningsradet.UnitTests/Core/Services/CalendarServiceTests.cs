using System;
using EPiServer.Core;
using EPiServer.Web.Routing;
using FakeItEasy;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Services;
using Forskningsradet.UnitTests.Builders;
using Xunit;

namespace Forskningsradet.UnitTests.Core.Services
{
    public class CalendarServiceTests
    {
        private readonly IUrlResolver _urlResolver;
        private readonly CalendarService _service;

        public CalendarServiceTests()
        {
            _urlResolver = A.Fake<IUrlResolver>();
            _service = new CalendarServiceForTest(_urlResolver);
        }

        [Fact]
        public void GetCalendarString_ShouldContainNecessaryFields_WhenGivenEmptyData()
        {
            const string CRLF = "\r\n";
            var expectedString = $"BEGIN:VCALENDAR" + CRLF +
                                 "PRODID:-//The Research Council of Norway//Forskningsradet Web//EN" + CRLF +
                                 "VERSION:2.0" + CRLF +
                                 "METHOD:PUBLISH" + CRLF +
                                 "BEGIN:VEVENT" + CRLF +
                                 "DTSTART:00010101T000000Z" + CRLF +
                                 "DTEND:00010101T000000Z" + CRLF +
                                 "UID:00000000-0000-0000-0000-000000000000" + CRLF +
                                 "DESCRIPTION:" + CRLF +
                                 "LOCATION:" + CRLF +
                                 "SUMMARY:" + CRLF +
                                 "END:VEVENT" + CRLF +
                                 "END:VCALENDAR";

            var result = _service.GetCalendarString(new DateTime(1, 1, 1, 0, 0, 0, DateTimeKind.Utc), new DateTime(1, 1, 1, 0, 0, 0, DateTimeKind.Utc), "", "", "", "");

            Assert.Equal(expectedString, result);
        }

        [Fact]
        public void GetCalendarString_ShouldReturnCorrectData_WhenGivenValidData()
        {
            const string CRLF = "\r\n";
            var dateTime = new DateTime(1998, 11, 14, 3, 2, 1, DateTimeKind.Utc);
            var summary = "The Summary";
            var description = "The Description";
            var location = "The Location";
            var expectedDateTime = "19981114T030201Z";
            var expectedUrl = "https://url";
            var expectedString = "BEGIN:VCALENDAR" + CRLF +
                                 "PRODID:-//The Research Council of Norway//Forskningsradet Web//EN" + CRLF +
                                 "VERSION:2.0" + CRLF +
                                 "METHOD:PUBLISH" + CRLF +
                                 "BEGIN:VEVENT" + CRLF +
                                 $"DTSTART:{expectedDateTime}" + CRLF +
                                 $"DTEND:{expectedDateTime}" + CRLF +
                                 "UID:00000000-0000-0000-0000-000000000000" + CRLF +
                                 $"DESCRIPTION:{expectedUrl} {description}" + CRLF +
                                 $"LOCATION:{location}" + CRLF +
                                 $"SUMMARY:{summary}" + CRLF +
                                 "END:VEVENT" + CRLF +
                                 "END:VCALENDAR";

            var result = _service.GetCalendarString(dateTime, dateTime, summary, location, description, expectedUrl);

            Assert.Equal(expectedString, result);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("The Location")]
        public void GetCalendarStringForEvent_ShouldReturnSameAsGetCalendarString(string where)
        {
            var title = "The Title";
            var intro = "The Intro";
            var location = where ?? "The Location";
            var url = "url";
            var dateTime = new DateTime(1998, 11, 14, 3, 2, 1);
            var eventPage = EventPageBuilder.Create.Default()
                .WithPageLink(new PageReference(1))
                .WithListTitle(title)
                .WithListIntro(intro)
                .WithStartDate(dateTime)
                .WithEndDate(dateTime)
                .WithLocation(location)
                .WithEventData(new EventDataBlock { Where = where });
            A.CallTo(() => _urlResolver.GetUrl(A<ContentReference>._, A<string>._, A<UrlResolverArguments>._))
                .Returns(url);

            var expectedResult = _service.GetCalendarString(dateTime, dateTime, title, location, intro, url);

            var result = _service.GetCalendarStringForEvent(eventPage);

            Assert.Equal(expectedResult, result);
        }

        [Fact]
        public void GetCalendarStringForEvent_ShouldReturnNull_WhenStartDateIsNull()
        {
            var eventPage = new EventPage
            {
                StartDate = null,
                EndDate = new DateTime(1998, 11, 14, 3, 2, 1),
            };

            var result = _service.GetCalendarStringForEvent(eventPage);

            Assert.Null(result);
        }

        [Fact]
        public void GetCalendarStringForEvent_ShouldReturnNull_WhenEndDateIsNull()
        {
            var eventPage = new EventPage
            {
                StartDate = new DateTime(1998, 11, 14, 3, 2, 1),
                EndDate = null,
            };

            var result = _service.GetCalendarStringForEvent(eventPage);

            Assert.Null(result);
        }
    }

    internal class CalendarServiceForTest : CalendarService
    {
        public CalendarServiceForTest(IUrlResolver urlResolver) : base(urlResolver)
        {

        }

        public override Guid NewGuid() => Guid.Empty;
    }
}