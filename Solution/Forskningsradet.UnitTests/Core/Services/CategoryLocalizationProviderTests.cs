using System;
using System.Collections.Generic;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.Framework.Localization;
using FakeItEasy;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.ContentModels.PropertyList;
using Forskningsradet.Core.Repositories.Contracts;
using Forskningsradet.Core.Services;
using Xunit;
using LocalizationProvider = Forskningsradet.Core.Services.LocalizationProvider;

namespace Forskningsradet.UnitTests.Core.Services
{
    public class CategoryLocalizationProviderTests
    {
        private readonly IPageRepository _pageRepository;
        private readonly IContentLoader _contentLoader;
        private readonly CategoryRepository _categoryRepository;
        private readonly ICategoryLocalizationProvider _service;

        public CategoryLocalizationProviderTests()
        {
            var localizationService = A.Fake<LocalizationService>();
            _pageRepository = A.Fake<IPageRepository>();
            _contentLoader = A.Fake<IContentLoader>();
            _categoryRepository = A.Fake<CategoryRepository>();
            _service = new LocalizationProvider(localizationService, _pageRepository, _contentLoader, _categoryRepository);
        }

        [Fact]
        public void GetCategoryName_ShouldReturnTranslatedValue_WhenTranslationForCategoryExist()
        {
            EPiServer.Globalization.ContentLanguage.PreferredCulture = new System.Globalization.CultureInfo("sv-SE");
            const string expectedTranslation = "translation";
            var frontPageWithCategoryTranslationPage = A.Fake<FrontPage>();
            frontPageWithCategoryTranslationPage.CategoryTranslationPage = new PageReference(1);
            var categoryPage = A.Fake<CategoryTranslationPage>();
            categoryPage.TargetGroups = new List<CategoryTranslationTargetGroup>
            {
                new CategoryTranslationTargetGroup
                {
                    TargetGroup = 1,
                    Translation = expectedTranslation
                }
            };
            A.CallTo(() => _pageRepository.GetCurrentStartPage()).Returns(frontPageWithCategoryTranslationPage);
            A.CallTo(() => _contentLoader.Get<CategoryTranslationPage>(A<ContentReference>._)).Returns(categoryPage);

            var result = _service.GetCategoryName(1);

            Assert.Equal(expectedTranslation, result);
        }

        [Fact]
        public void GetCategoryName_ShouldReturnLocalizedDescription_WhenTranslationForCategoryExistButCultureIsNorwegian()
        {
            EPiServer.Globalization.ContentLanguage.PreferredCulture = new System.Globalization.CultureInfo("no");
            var category = new Category("name", "description") { ID = 1 };
            var frontPageWithCategoryTranslationPage = A.Fake<FrontPage>();
            frontPageWithCategoryTranslationPage.CategoryTranslationPage = new PageReference(1);
            var categoryPage = A.Fake<CategoryTranslationPage>();
            categoryPage.TargetGroups = new List<CategoryTranslationTargetGroup>
            {
                new CategoryTranslationTargetGroup
                {
                    TargetGroup = 1,
                    Translation = "translation"
                }
            };
            A.CallTo(() => _pageRepository.GetCurrentStartPage()).Returns(frontPageWithCategoryTranslationPage);
            A.CallTo(() => _categoryRepository.Get(A<int>._)).Returns(category);
            A.CallTo(() => _contentLoader.Get<CategoryTranslationPage>(A<ContentReference>._)).Returns(categoryPage);

            var result = _service.GetCategoryName(1);

            Assert.Equal(category.LocalizedDescription, result);
        }

        [Fact]
        public void GetCategoryName_ShouldReturnLocalizedDescription_WhenNoTranslationForCategoryExist()
        {
            EPiServer.Globalization.ContentLanguage.PreferredCulture = new System.Globalization.CultureInfo("sv-SE");
            var category = new Category("name", "description") { ID = 1 };
            var frontPageWithCategoryTranslationPage = A.Fake<FrontPage>();
            frontPageWithCategoryTranslationPage.CategoryTranslationPage = new PageReference(1);
            var categoryPage = A.Fake<CategoryTranslationPage>();
            categoryPage.TargetGroups = new List<CategoryTranslationTargetGroup>
            {
                new CategoryTranslationTargetGroup
                {
                    TargetGroup = 2,
                    Translation = "translation"
                }
            };
            A.CallTo(() => _pageRepository.GetCurrentStartPage()).Returns(frontPageWithCategoryTranslationPage);
            A.CallTo(() => _categoryRepository.Get(A<int>._)).Returns(category);
            A.CallTo(() => _contentLoader.Get<CategoryTranslationPage>(A<ContentReference>._)).Returns(categoryPage);

            var result = _service.GetCategoryName(1);

            Assert.Equal(category.LocalizedDescription, result);
        }

        [Fact]
        public void GetCategoryName_ShouldReturnLocalizedDescription_WhenCategoryTranslationPageNotFound()
        {
            EPiServer.Globalization.ContentLanguage.PreferredCulture = new System.Globalization.CultureInfo("sv-SE");
            var category = new Category("name", "description");

            A.CallTo(() => _pageRepository.GetCurrentStartPage()).Returns(A.Fake<FrontPage>());
            A.CallTo(() => _categoryRepository.Get(A<int>._)).Returns(category);

            var result = _service.GetCategoryName(1);

            Assert.Equal(category.LocalizedDescription, result);
        }

        [Fact]
        public void GetCategoryName_ShouldReturnLocalizedDescription_WhenNoTranslationsExists()
        {
            EPiServer.Globalization.ContentLanguage.PreferredCulture = new System.Globalization.CultureInfo("sv-SE");
            var category = new Category("name", "description") { ID = 1 };
            var frontPageWithCategoryTranslationPage = A.Fake<FrontPage>();
            frontPageWithCategoryTranslationPage.CategoryTranslationPage = new PageReference(1);

            A.CallTo(() => _pageRepository.GetCurrentStartPage()).Returns(frontPageWithCategoryTranslationPage);
            A.CallTo(() => _categoryRepository.Get(A<int>._)).Returns(category);
            A.CallTo(() => _contentLoader.Get<CategoryTranslationPage>(A<ContentReference>._)).Returns(A.Fake<CategoryTranslationPage>());

            var result = _service.GetCategoryName(1);

            Assert.Equal(category.LocalizedDescription, result);
        }
    }
}