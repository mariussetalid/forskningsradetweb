using EPiServer.Find;
using EPiServer.Find.Framework.Statistics;
using FakeItEasy;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.SearchModels;
using Forskningsradet.Core.Services;
using Forskningsradet.UnitTests.Builders;
using Xunit;

namespace Forskningsradet.UnitTests.Core.Services
{
    public class FindSearchTrackingServiceTests
    {
        private readonly IClient _searchClient;
        private readonly IStatisticTagsHelper _statisticTagsHelper;
        private readonly FindSearchTrackingService _service;

        public FindSearchTrackingServiceTests()
        {
            _searchClient = A.Fake<IClient>();
            _statisticTagsHelper = A.Fake<IStatisticTagsHelper>();
            _service = new FindSearchTrackingService(_searchClient, _statisticTagsHelper);
        }

        [Fact]
        public void GetTrackHitRequest_WithValidParameters_ShouldReturnSearchHitTrackRequest()
        {
            var expectedQuery = "q";

            var result = _service.GetTrackHitRequest(expectedQuery, new SearchQueryTrackResult { TrackId = "1" }, (ProposalPage)ProposalPageBuilder.Create.Default(), 0);

            Assert.Equal(expectedQuery, result.Query);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        public void TrackQuery_WithoutQuery_ShouldReturnNull(string emptyQuery)
        {
            var result = _service.TrackQuery(emptyQuery, 0);

            Assert.Null(result);
        }
    }
}