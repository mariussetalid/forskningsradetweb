using FakeItEasy;
using Forskningsradet.Common.Logging;
using Forskningsradet.Core.Processes;
using Xunit;

namespace Forskningsradet.UnitTests.Core.Processes
{
    public class ProcessRunnerTests
    {
        private readonly TestContext _testContext;

        public ProcessRunnerTests() => _testContext = new TestContext(A.Fake<IIntegrationLogger>());

        [Fact]
        public void Run_WithStep_ShouldCallRunOnStep()
        {
            var step = A.Fake<ProcessStep<Context>>();
            var process = new ProcessRunner<Context>(_testContext)
                .AddStep(step);
            
            process.Run();

            A.CallTo(() => step.Run()).MustHaveHappened();
        }

        [Fact]
        public void Run_WithMultipleSteps_ShouldCallRunOnAllSteps()
        {
            var step1 = A.Fake<ProcessStep<Context>>();
            var step2 = A.Fake<ProcessStep<Context>>();
            var process = new ProcessRunner<Context>(_testContext)
                .AddStep(step1)
                .AddStep(step2);
            
            process.Run();

            A.CallTo(() => step1.Run()).MustHaveHappened();
            A.CallTo(() => step2.Run()).MustHaveHappened();
        }

        [Fact]
        public void Run_WhenStepChangesContext_ContextShouldBeChangedForSubsequentSteps()
        {
            const string expected = "expected";
            
            var step = A.Fake<ProcessStep<Context>>();
            var context = _testContext;

            A.CallTo(() => step.Run())
                .Invokes(() => context.ChangeableProperty = expected);
            
            var process = new ProcessRunner<Context>(context)
                .AddStep(step);
            
            process.Run();
            
            Assert.Equal(expected, context.ChangeableProperty);
        }

        [Fact]
        public void Run_WhenStepAbortsExecution_ShouldNotRunSubsequentSteps()
        {
            var context = _testContext;
            var step1 = A.Fake<ProcessStep<Context>>();
            var step2 = A.Fake<ProcessStep<Context>>();

            A.CallTo(() => step1.Run()).Invokes(() => context.Abort = true);
            
            var process = new ProcessRunner<Context>(context)
                .AddStep(step1)
                .AddStep(step2);
            
            process.Run();
            
            A.CallTo(() => step2.Run()).MustNotHaveHappened();
        }

        [Fact]
        public void Run_WhenStepReportsStatus_ShouldAppendToJobProgress()
        {
            const string expectedProgress = "progress";
            
            var context = _testContext;
            var step = A.Fake<ProcessStep<Context>>();

            A.CallTo(() => step.Run()).Invokes(() => context.Progress.Add(expectedProgress));
            
            var process = new ProcessRunner<Context>(context)
                .AddStep(step);
            
            process.Run();
            
            Assert.Equal(expectedProgress, context.Progress[0]);
        }
    }

    public class TestContext : Context
    {
        public string ChangeableProperty { get; set; }
        
        public TestContext(IIntegrationLogger logger)
            : base(logger) { }
        
        public override string GetProgressReport()
            => string.Empty;
    }
}