using System;
using System.Collections.Generic;
using System.Globalization;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using FakeItEasy;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Processes.Providers;
using Xunit;

namespace Forskningsradet.UnitTests.Core.Processes.Providers
{
    public class EpiPageVersionProviderTests
    {
        const int ExpectedVersion = 1;
        
        private readonly IContentVersionRepository _repository;
        private readonly EpiPageVersionProvider _provider;
        private readonly EditorialPage _page;

        public EpiPageVersionProviderTests()
        {
            _repository = A.Fake<IContentVersionRepository>();
            _provider = new EpiPageVersionProvider(_repository);
            
            _page = A.Fake<EditorialPage>();
            _page.ContentLink = ContentReference.EmptyReference;
            ((ILocalizable)_page).Language = new CultureInfo("no");
        }
        
        [Fact]
        public void GetPageVersion_WithOneVersion_ShouldReturnThatVersion()
        {
            ACallToRepositoryReturnsVersions(_page.ContentLink, new VersionForTest{VersionNumber = ExpectedVersion});

            AssertReturnedVersionIsExpected();
        }

        [Fact]
        public void GetPageVersion_WithMultipleVersions_ShouldReturnLatest()
        {
            ACallToRepositoryReturnsVersions(_page.ContentLink,
                new VersionForTest{VersionNumber = 2, SaveDate = new DateTime(2016, 1, 1)},
                new VersionForTest{VersionNumber = ExpectedVersion, SaveDate = new DateTime(2018, 1, 1)}
            );
            
            AssertReturnedVersionIsExpected();
        }
        
        [Fact]
        public void GetPageVersion_WithMultipleVersionsAndLanguages_ShouldReturnLatestInCurrentLanguage()
        {
            ACallToRepositoryReturnsVersions(_page.ContentLink,
                new VersionForTest{VersionNumber = 2, LanguageBranch = "en"},
                new VersionForTest{VersionNumber = ExpectedVersion, LanguageBranch = "no"}
            );
            
            AssertReturnedVersionIsExpected();
        }

        [Fact]
        public void GetPageVersion_WithNewerVersionOnDifferentLanguageBranch_ShouldReturnLatestInCurrentLanguage()
        {
            ACallToRepositoryReturnsVersions(_page.ContentLink,
                new VersionForTest{VersionNumber = 2, LanguageBranch = "en", SaveDate = new DateTime(2017, 1, 1)},
                new VersionForTest{VersionNumber = ExpectedVersion, LanguageBranch = "no", SaveDate = new DateTime(2016, 1, 1)}
            );
            
            AssertReturnedVersionIsExpected();
        }
        
        [Fact]
        public void GetPageVersion_WhenNoVersionsAreFound_ShouldReturnZero()
        {
            AssertReturnedVersionIsExpected(0);
        }
        
        private void AssertReturnedVersionIsExpected(int? expected = null)
        {
            var version = _provider.GetPageVersion(_page);
            Assert.Equal(expected ?? ExpectedVersion, version);
        }

        private void ACallToRepositoryReturnsVersions(ContentReference reference, params VersionForTest[] expectedVersions)
        {
            int ignored;
            var list = A.Fake<List<ContentVersion>>();
            
            foreach (var expectedVersion in expectedVersions)
            {
                var contentLink = A.Fake<ContentReference>();
                contentLink.WorkID = expectedVersion.VersionNumber;
                
                var fakedVersion = A.Fake<ContentVersion>();
                fakedVersion.ContentLink = contentLink;
                fakedVersion.Saved = expectedVersion.SaveDate;
                fakedVersion.LanguageBranch = expectedVersion.LanguageBranch;
                
                list.Add(fakedVersion);
            }
            
            A.CallTo(() => _repository.List(
                    A<VersionFilter>.That.Matches(x => x.ContentLink == reference), 
                    A<int>._, 
                    A<int>._, 
                    out ignored))
                .Returns(list);
        }

        class VersionForTest
        {
            public int VersionNumber { get; set; }
            public DateTime SaveDate { get; set; }
            public string LanguageBranch { get; set; } = "no";
        }
    }
}