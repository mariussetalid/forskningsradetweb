using System;
using FakeItEasy;
using Forskningsradet.Common.Constants;
using Forskningsradet.Common.Logging;
using Forskningsradet.Core.MessageBrokers;
using Forskningsradet.Core.Processes.Contexts;
using Forskningsradet.Core.Processes.Steps.Archiving;
using Xunit;

namespace Forskningsradet.UnitTests.Core.Processes.Steps.Archiving
{
    public class ResendFailedMessagesTests
    {
        private readonly IArchiveMessageBroker _broker;
        private readonly ArchivingContext _context;
        private readonly ResendFailedMessages _step;

        public ResendFailedMessagesTests()
        {
            _broker = A.Fake<IArchiveMessageBroker>();
            _context = new ArchivingContext(A.Fake<IIntegrationLogger>());
            _step = new ResendFailedMessages(_broker)
                {Context = _context};
        }

        [Fact]
        public void Run_WhenPageHasNotBeenArchived_ShouldIgnore()
        {
            var idToResend = Guid.NewGuid();
            const string expectedResponsibleCode = "abc";

            _context.ArchivePageMessages.Add(
                GetMessageEntity(new ArchivePage
                {
                    Id = idToResend,
                    Responsible = expectedResponsibleCode,
                    IsArchived = false
                }));

            _step.Run();

            A.CallTo(() => _broker.Send(A<ArchivePage>._))
                .MustNotHaveHappened();
            A.CallTo(() => _broker.CompleteMessage(A<Guid>._))
                .MustNotHaveHappened();
        }
        
        [Fact]
        public void Run_WhenPageHasNotBeenArchivedAndSignatureIsInvalid_ShouldResendWithDefaultSignature()
        {
            _context.ArchivePageMessages.Add(
                GetMessageEntity(new ArchivePage
                {
                    Id = Guid.NewGuid(),
                    Responsible = "abc",
                    IsArchived = false
                }));
            _context.InvalidSignature = true;

            _step.Run();

            A.CallTo(() => _broker.Send(
                    A<ArchivePage>.That.Matches(x => 
                        x.Responsible == ArchiveConstants.DefaultResponsible)))
                .MustHaveHappened();
        }
        
        [Fact]
        public void Run_WhenAllPagesHaveBeenArchived_ShouldCompleteMessage()
        {
            _context.ArchivePageMessages.Add(GetMessageEntity(new ArchivePage {IsArchived = true}));

            _step.Run();

            A.CallTo(() => _broker.CompleteMessage(A<Guid>._))
                .MustHaveHappened();
        }
        
        [Fact]
        public void Run_WhenSomePagesHaveBeenArchived_ShouldOnlyCompleteWhenIsArchived()
        {
            _context.ArchivePageMessages.Add(GetMessageEntity(new ArchivePage {IsArchived = false}));
            _context.ArchivePageMessages.Add(GetMessageEntity(new ArchivePage {IsArchived = true}));
            _context.ArchivePageMessages.Add(GetMessageEntity(new ArchivePage {IsArchived = false}));

            _step.Run();

            A.CallTo(() => _broker.CompleteMessage(A<Guid>._))
                .MustHaveHappenedOnceExactly();
        }
        
        private static MessageEntity<ArchivePage> GetMessageEntity(ArchivePage page) =>
            new MessageEntity<ArchivePage>(Guid.Empty, page);
    }
}