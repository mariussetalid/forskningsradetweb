using System;
using System.Collections.Generic;
using System.Linq;
using FakeItEasy;
using Forskningsradet.Common.Logging;
using Forskningsradet.Core.MessageBrokers;
using Forskningsradet.Core.Processes.Contexts;
using Forskningsradet.Core.Processes.Steps.Archiving;
using Xunit;

namespace Forskningsradet.UnitTests.Core.Processes.Steps.Archiving
{
    public class GetPagesPreparedForArchivingTests
    {
        private readonly IArchiveMessageBroker _broker;
        private readonly ArchivingContext _context;
        private readonly GetPagesPreparedForArchiving _step;

        public GetPagesPreparedForArchivingTests()
        {
            _broker = A.Fake<IArchiveMessageBroker>();
            _context = new ArchivingContext(A.Fake<IIntegrationLogger>());
            _step = new GetPagesPreparedForArchiving(_broker)
                {Context = _context};
        }
        
        [Fact]
        public void Run_WhenPreparedPagesExist_ShouldPopulateContext()
        {
            A.CallTo(() => _broker.ReceiveMessages())
                .Returns(new List<MessageEntity<ArchivePage>>
                {
                    new MessageEntity<ArchivePage>(Guid.Empty, new ArchivePage()),
                    new MessageEntity<ArchivePage>(Guid.Empty, new ArchivePage())
                });
            
            _step.Run();
            
            Assert.Equal(2, _context.ArchivePageMessages.Count);
        }

        [Fact]
        public void Run_WhenNoPagesArePrepared_ShouldStopProcess()
        {
            A.CallTo(() => _broker.ReceiveMessages())
                .Returns(Enumerable.Empty<MessageEntity<ArchivePage>>());
            
            _step.Run();

            Assert.True(_context.Abort);
        }
    }
}