using System;
using System.Linq;
using System.Threading.Tasks;
using FakeItEasy;
using Forskningsradet.Common.Logging;
using Forskningsradet.Core.Logging;
using Forskningsradet.Core.MessageBrokers;
using Forskningsradet.Core.Processes.Contexts;
using Forskningsradet.Core.Processes.Steps.Archiving;
using Forskningsradet.ServiceAgents.Archiving;
using Xunit;

namespace Forskningsradet.UnitTests.Core.Processes.Steps.Archiving
{
    public class DispatchPagesToArchiveTests
    {
        private readonly IArchivingServiceAgent _archivingServiceAgent;
        private readonly DispatchPagesToArchive _step;
        private readonly ArchivingContext _context;

        public DispatchPagesToArchiveTests()
        {
            var logger = A.Fake<IIntegrationLogger>();
            A.CallTo(() => logger.IsEnabled(LogLevel.Error))
                .Returns(true);
            
            _context = new ArchivingContext(logger);
            
            _archivingServiceAgent = A.Fake<IArchivingServiceAgent>();
            _step = new DispatchPagesToArchive(_archivingServiceAgent)
                {Context = _context};
        }

        [Fact]
        public void Run_WhenDispatchingRequest_ShouldCallWithContextData()
        {
            var preparedArchivePage = new ArchivePage
            {
                CaseId = "ID",
                CaseTitle = "Title",
                Type = "Type",
                Responsible = "Responsible",
                Activities = "activity1",
                JournalTitle = "Journal title",
                FileName = "File name",
                FileContents = "File contents"
            };
            
            _context.ArchivePageMessages.Add(GetMessageEntity(preparedArchivePage));
            
            _step.Run();

            A.CallTo(() => _archivingServiceAgent.ArchivePage(A<ArchivingRequest>.That.Matches(
                    x =>
                           x.Sak.Id == preparedArchivePage.CaseId
                        && x.Sak.Tittel == preparedArchivePage.CaseTitle
                        && x.Sak.Type == preparedArchivePage.Type
                        && x.Sak.Saksansvarlig == preparedArchivePage.Responsible
                        && x.Sak.Aktiviteter[0] == preparedArchivePage.Activities
                        && x.JournalPost.Tittel == preparedArchivePage.JournalTitle
                        && x.Fil.Filnavn == preparedArchivePage.FileName
                        && x.Fil.Filinnhold == preparedArchivePage.FileContents
                 )))
                .MustHaveHappened();
        }
        
        [Theory]
        [InlineData(true)]
        [InlineData(false)]
        public void Run_WhenDispatchIsSuccessful_ShouldMarkPageAsArchived(bool success)
        {
            A.CallTo(() => _archivingServiceAgent.ArchivePage(A<ArchivingRequest>._))
                .Returns(new ArchivingResponse {Result = success});
            
            var page = new ArchivePage();
            _context.ArchivePageMessages.Add(GetMessageEntity(page));
            
            _step.Run();
            
            Assert.Equal(success, page.IsArchived);
        }
        
        [Fact]
        public void Run_WhenDispatchIsUnsuccessful_ShouldReportError()
        {
            const string expectedError = "some message";
            
            A.CallTo(() => _archivingServiceAgent.ArchivePage(A<ArchivingRequest>._))
                .Returns(new ArchivingResponse {Result = false, Message = expectedError});
            
            var page = new ArchivePage();
            _context.ArchivePageMessages.Add(GetMessageEntity(page));
            
            _step.Run();
            
            Assert.EndsWith(expectedError, _context.Progress.First());
        }

        [Fact]
        public void Run_WhenArchivingResponseIsEmpty_ShouldReportError()
        {
            A.CallTo(() => _archivingServiceAgent.ArchivePage(A<ArchivingRequest>._))
                .Returns(Task.FromResult<ArchivingResponse>(null));
            
            var page = new ArchivePage();
            _context.ArchivePageMessages.Add(GetMessageEntity(page));
            
            _step.Run();
            
            Assert.NotNull(_context.Progress.FirstOrDefault());
        }
        
        [Fact]
        public void Run_WhenResponsibleIsInvalid_ShouldFlagContext()
        {
            A.CallTo(() => _archivingServiceAgent.ArchivePage(A<ArchivingRequest>._))
                .Returns(new ArchivingResponse {Result = false, Message = "Det finnes ingen identiteter med gidkode"});
            
            var page = new ArchivePage();
            _context.ArchivePageMessages.Add(GetMessageEntity(page));
            
            _step.Run();
            
            Assert.True(_context.InvalidSignature);
        }
        
        [Fact]
        public void Run_WithOnePreparedPage_ShouldCallDispatcherOnce()
        {
            _context.ArchivePageMessages.Add(GetMessageEntity(new ArchivePage()));
            
            _step.Run();
            
            A.CallTo(() => _archivingServiceAgent.ArchivePage(A<ArchivingRequest>._))
                .MustHaveHappenedOnceExactly();
        }
        
        [Fact]
        public void Run_WithTenPreparedPages_ShouldCallDispatcherTenTimes()
        {
            for(var i=0; i<10; i++)
                _context.ArchivePageMessages.Add(GetMessageEntity(new ArchivePage()));
            
            _step.Run();
            
            A.CallTo(() => _archivingServiceAgent.ArchivePage(A<ArchivingRequest>._))
                .MustHaveHappened(10, Times.Exactly);
        }

        private static MessageEntity<ArchivePage> GetMessageEntity(ArchivePage page) => 
            new MessageEntity<ArchivePage>(Guid.Empty, page);
    }
}