using EPiServer.Core;
using FakeItEasy;
using Forskningsradet.Common.Logging;
using Forskningsradet.Core.Models.ApiModels.Evurdering;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Processes.Contexts;
using Forskningsradet.Core.Processes.Steps.Proposals;
using Forskningsradet.Core.Repositories.Contracts;
using Forskningsradet.Core.Services.Contracts;
using Forskningsradet.UnitTests.Builders;
using Xunit;

namespace Forskningsradet.UnitTests.Core.Processes.Steps.Proposals
{
    public class HandleActiveDraftTests
    {
        private readonly HandleActiveDraft _step;
        private readonly IProposalPageProvider _proposalPageProvider;
        private readonly IPageRepository _pageRepository;

        public HandleActiveDraftTests()
        {
            _proposalPageProvider = A.Fake<IProposalPageProvider>();
            _pageRepository = A.Fake<IPageRepository>();
            _step = new HandleActiveDraft(_proposalPageProvider, _pageRepository);
        }

        [Fact]
        public void HandleActiveDraft_WithOriginalPage_OriginalPageShouldBeChangedToCurrentlyPublishedVersion()
        {
            var expectedOriginalPage = (ProposalPage)ProposalPageBuilder.Create.Default();
            A.CallTo(() => _pageRepository.GetCurrentlyPublishedVersion<ProposalPage>(A<ContentReference>._, A<string>._))
                .Returns(expectedOriginalPage);
            var context = new ProposalContext(A.Fake<IIntegrationLogger>())
            {
                OriginalPage = ProposalPageBuilder.Create.Default(),
                CurrentPage = ProposalPageBuilder.Create.Default(),
                Proposal = ProposalBuilder.Create.Default(ProgramBuilder.Create.Default(), ApplicationTypeBuilder.Create.Default())
                    .WithStatus(ProposalState.Completed)
            };

            _step.Context = context;
            _step.Run();

            Assert.Same(expectedOriginalPage, context.OriginalPage);
        }

        [Fact]
        public void HandleActiveDraft_WithOriginalPageWithoutPublishedVersion_OriginalPageShouldRemainUnpublishedVersion()
        {
            var expectedOriginalPage = (ProposalPage)ProposalPageBuilder.Create.Default();
            A.CallTo(() => _pageRepository.GetCurrentlyPublishedVersion<ProposalPage>(A<ContentReference>._, A<string>._))
                .Returns(null);
            var context = new ProposalContext(A.Fake<IIntegrationLogger>())
            {
                OriginalPage = expectedOriginalPage,
                CurrentPage = ProposalPageBuilder.Create.Default(),
                Proposal = ProposalBuilder.Create.Default(ProgramBuilder.Create.Default(), ApplicationTypeBuilder.Create.Default())
                    .WithStatus(ProposalState.Completed)
            };

            _step.Context = context;
            _step.Run();

            Assert.Same(expectedOriginalPage, context.OriginalPage);
        }
    }
}