using System.Collections.Generic;
using EPiServer.Core;
using FakeItEasy;
using Forskningsradet.Common.Logging;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.Validation;
using Forskningsradet.Core.Processes.Contexts;
using Forskningsradet.Core.Processes.Steps.Proposals;
using Forskningsradet.Core.Repositories.Contracts;
using Forskningsradet.Core.Services.Contracts;
using Xunit;

namespace Forskningsradet.UnitTests.Core.Processes.Steps.Proposals
{
    public class PublishProposalTests
    {
        private readonly PublishProposal _step;
        private readonly IPageRepository _pageRepository;
        private readonly IProposalValidationService _proposalValidationService;

        public PublishProposalTests()
        {
            _pageRepository = A.Fake<IPageRepository>();
            _proposalValidationService = A.Fake<IProposalValidationService>();
            _step = new PublishProposal(_pageRepository, _proposalValidationService);
        }

        [Fact]
        public void PublishProposal_WhenCurrentPageIsPublishableAndRequiredFieldsAreSet_ShouldPublish()
        {
            A.CallTo(() => _proposalValidationService.Validate(A<ProposalPage>._))
                .Returns(new ValidationResult());

            var context = new ProposalContext(A.Fake<IIntegrationLogger>())
            {
                IsPublishable = true
            };

            _step.Context = context;
            _step.Run();

            A.CallTo(() => _pageRepository.Save(A<PageData>._)).MustHaveHappened();
        }

        [Fact]
        public void PublishProposal_WhenCurrentPageIsPublishableAndRequiredFieldsAreNotSet_ShouldSaveDraft()
        {
            A.CallTo(() => _proposalValidationService.Validate(A<ProposalPage>._))
                .Returns(new ValidationResult
                {
                    ErrorMessages = new List<string> { "error" }
                });

            var context = new ProposalContext(A.Fake<IIntegrationLogger>())
            {
                IsPublishable = true
            };

            _step.Context = context;
            _step.Run();

            A.CallTo(() => _pageRepository.SaveAsDraft(A<PageData>._)).MustHaveHappened();
        }

        [Fact]
        public void PublishProposal_WhenCurrentPageIsNotPublishableAndRequiredFieldsAreSet_ShouldSaveDraft()
        {
            A.CallTo(() => _proposalValidationService.Validate(A<ProposalPage>._))
                .Returns(new ValidationResult());

            var context = new ProposalContext(A.Fake<IIntegrationLogger>())
            {
                IsPublishable = false
            };

            _step.Context = context;
            _step.Run();

            A.CallTo(() => _pageRepository.SaveAsDraft(A<PageData>._)).MustHaveHappened();
        }
    }
}