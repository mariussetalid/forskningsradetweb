using FakeItEasy;
using Forskningsradet.Common.Logging;
using Forskningsradet.Core.Models.ApiModels.Evurdering;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Processes.Contexts;
using Forskningsradet.Core.Processes.Steps.Proposals;
using Forskningsradet.Core.Repositories.Contracts;
using Forskningsradet.UnitTests.Builders;
using Xunit;

namespace Forskningsradet.UnitTests.Core.Processes.Steps.Proposals
{
    public class HandleInactiveStateTests
    {
        private readonly HandleInactiveState _step;
        private readonly IPageRepository _pageRepository;

        public HandleInactiveStateTests()
        {
            _pageRepository = A.Fake<IPageRepository>();
            _step = new HandleInactiveState(_pageRepository);
        }

        [Theory]
        [InlineData(ProposalState.Active)]
        [InlineData(ProposalState.Cancelled)]
        [InlineData(ProposalState.Planned)]
        [InlineData(ProposalState.Completed)]
        [InlineData(ProposalState.Deleted)]
        public void HandleInactiveState_WithAnyState_ShouldSetCurrentPageAsNonArchivable(ProposalState proposalState)
        {
            var context = new ProposalContext(A.Fake<IIntegrationLogger>())
            {
                OriginalPage = ProposalPageBuilder.Create.Default(),
                CurrentPage = ProposalPageBuilder.Create.Default(),
                Proposal = ProposalBuilder.Create.Default(ProgramBuilder.Create.Default(), ApplicationTypeBuilder.Create.Default())
                    .WithStatus(proposalState)
            };

            _step.Context = context;
            _step.Run();

            Assert.False(context.CurrentPage.IsArchivable);
        }

        [Fact]
        public void HandleInactiveState_WithoutOriginalPage_ShouldSetNotPublishableOnContext()
        {
            var context = new ProposalContext(A.Fake<IIntegrationLogger>())
            {
                OriginalPage = null,
                CurrentPage = ProposalPageBuilder.Create.Default(),
                Proposal = ProposalBuilder.Create.Default(ProgramBuilder.Create.Default(), ApplicationTypeBuilder.Create.Default())
            };

            _step.Context = context;
            _step.Run();

            Assert.False(context.IsPublishable);
        }

        [Fact]
        public void HandleInactiveState_WhenOriginalPageIsPublished_ShouldSetPublishableOnContext()
        {
            var context = new ProposalContext(A.Fake<IIntegrationLogger>())
            {
                OriginalPage = ProposalPageBuilder.Create.Default(),
                CurrentPage = ProposalPageBuilder.Create.Default(),
                Proposal = ProposalBuilder.Create.Default(ProgramBuilder.Create.Default(), ApplicationTypeBuilder.Create.Default())
            };
            A.CallTo(() => _pageRepository.IsPublished(A<ProposalPage>._))
                .Returns(true);

            _step.Context = context;
            _step.Run();

            Assert.True(context.IsPublishable);
        }
    }
}