using System;
using System.Collections.Generic;
using System.Linq;
using FakeItEasy;
using Forskningsradet.Common.Constants;
using Forskningsradet.Common.Logging;
using Forskningsradet.Core.Models.ApiModels.Evurdering;
using Forskningsradet.Core.Models.ContentModels.PropertyList;
using Forskningsradet.Core.Processes.Contexts;
using Forskningsradet.Core.Processes.Steps.Proposals;
using Forskningsradet.Core.Services;
using Forskningsradet.UnitTests.Builders;
using Xunit;

namespace Forskningsradet.UnitTests.Core.Processes.Steps.Proposals
{
    public class UpdateTimelineTests
    {
        private readonly UpdateTimeline _step;
        private readonly List<DateTime> ValidDateTimeList = new List<DateTime> { new DateTime(2018, 11, 1) };

        public UpdateTimelineTests()
        {
            _step = new UpdateTimeline(A.Fake<ILocalizationProvider>());
        }

        [Fact]
        public void UpdateTimeline_WhenDatesAreNew_ShouldAddDatesOnTimeline()
        {
            var applicationTypeBuilder = ApplicationTypeBuilder.Create.Default();
            var context = new ProposalContext(A.Fake<IIntegrationLogger>())
            {
                CurrentPage = ProposalPageBuilder.Create.Default().WithTimeline(),
                Proposal = ProposalBuilder.Create.Default(ProgramBuilder.Create.Default(), applicationTypeBuilder),
                LanguageBranch = LanguageConstants.MasterLanguageBranch,
            };

            _step.Context = context;
            _step.Run();

            Assert.NotNull(context.CurrentPage.Timeline);

            var items = context.CurrentPage.Timeline.TimeLineItems;
            Assert.Equal(context.Proposal.StartDate, GetTimeByTimelineType(items, TimeLineItemType.StartDate));
            Assert.Equal(context.Proposal.ApplicationDeadline, GetTimeByTimelineType(items, TimeLineItemType.Deadline));

            var applicationType = (ApplicationType)applicationTypeBuilder;
            Assert.Equal(applicationType.EarliestStartDate, GetTimeByTimelineType(items, TimeLineItemType.EarliestStartDate));
            Assert.Equal(applicationType.LatestProjectPeriodStart, GetTimeByTimelineType(items, TimeLineItemType.LatestProjectPeriodStart));
            Assert.Equal(applicationType.LatestProjectPeriodEnd, GetTimeByTimelineType(items, TimeLineItemType.LatestProjectPeriodEnd));
        }

        [Fact]
        public void UpdateTimeline_WhenDatesExist_ShouldUpdateDatesOnTimeline()
        {
            var applicationTypeBuilder = ApplicationTypeBuilder.Create.Default();
            var context = new ProposalContext(A.Fake<IIntegrationLogger>())
            {
                CurrentPage = ProposalPageBuilder.Create.Default()
                    .WithTimeline(
                        new TimeLineItem { DateTimes = ValidDateTimeList, Type = TimeLineItemType.StartDate },
                        new TimeLineItem { DateTimes = ValidDateTimeList, Type = TimeLineItemType.Deadline },
                        new TimeLineItem { DateTimes = ValidDateTimeList, Type = TimeLineItemType.EarliestStartDate },
                        new TimeLineItem { DateTimes = ValidDateTimeList, Type = TimeLineItemType.LatestProjectPeriodStart },
                        new TimeLineItem { DateTimes = ValidDateTimeList, Type = TimeLineItemType.LatestProjectPeriodEnd }
                    ),
                Proposal = ProposalBuilder.Create.Default(ProgramBuilder.Create.Default(), applicationTypeBuilder),
                LanguageBranch = LanguageConstants.MasterLanguageBranch
            };

            _step.Context = context;
            _step.Run();

            Assert.NotNull(context.CurrentPage.Timeline);

            var items = context.CurrentPage.Timeline.TimeLineItems;
            Assert.Equal(context.Proposal.StartDate, GetTimeByTimelineType(items, TimeLineItemType.StartDate));
            Assert.Equal(context.Proposal.ApplicationDeadline, GetTimeByTimelineType(items, TimeLineItemType.Deadline));

            var applicationType = (ApplicationType)applicationTypeBuilder;
            Assert.Equal(applicationType.EarliestStartDate, GetTimeByTimelineType(items, TimeLineItemType.EarliestStartDate));
            Assert.Equal(applicationType.LatestProjectPeriodStart, GetTimeByTimelineType(items, TimeLineItemType.LatestProjectPeriodStart));
            Assert.Equal(applicationType.LatestProjectPeriodEnd, GetTimeByTimelineType(items, TimeLineItemType.LatestProjectPeriodEnd));
        }

        [Fact]
        public void UpdateTimeline_WhenDatesExistOnPageAndNotInProposal_ShouldDeleteDateOnTimeline()
        {
            var applicationTypeBuilder = ApplicationTypeBuilder.Create.Default()
                .WithEarliestStartDate(null)
                .WithLatestProjectPeriodStart(null)
                .WithLatestProjectPeriodEnd(null);
            var context = new ProposalContext(A.Fake<IIntegrationLogger>())
            {
                CurrentPage = ProposalPageBuilder.Create.Default()
                    .WithTimeline(
                        new TimeLineItem { DateTimes = ValidDateTimeList, Type = TimeLineItemType.StartDate },
                        new TimeLineItem { DateTimes = ValidDateTimeList, Type = TimeLineItemType.Deadline },
                        new TimeLineItem { DateTimes = ValidDateTimeList, Type = TimeLineItemType.EarliestStartDate },
                        new TimeLineItem { DateTimes = ValidDateTimeList, Type = TimeLineItemType.LatestProjectPeriodStart },
                        new TimeLineItem { DateTimes = ValidDateTimeList, Type = TimeLineItemType.LatestProjectPeriodEnd }
                    ),
                Proposal = ProposalBuilder.Create.Default(ProgramBuilder.Create.Default(), applicationTypeBuilder)
                    .WithStartDate(null)
                    .WithApplicationDeadline(null),
                LanguageBranch = LanguageConstants.MasterLanguageBranch,
            };

            _step.Context = context;
            _step.Run();

            Assert.Empty(context.CurrentPage.Timeline.TimeLineItems);
        }

        private DateTime? GetTimeByTimelineType(IEnumerable<TimeLineItem> items, TimeLineItemType type) =>
            items.Single(x => x.Type == type).DateTimes.First();
    }
}