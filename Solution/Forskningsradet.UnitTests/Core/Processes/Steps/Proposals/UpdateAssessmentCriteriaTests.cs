using System;
using System.Collections.Generic;
using System.Globalization;
using EPiServer.Core;
using FakeItEasy;
using Forskningsradet.Common.Constants;
using Forskningsradet.Common.Logging;
using Forskningsradet.Core.Models.ApiModels.Evurdering;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Processes.Contexts;
using Forskningsradet.Core.Processes.Steps.Proposals;
using Forskningsradet.Core.Repositories.Contracts;
using Forskningsradet.UnitTests.Builders;
using Xunit;

namespace Forskningsradet.UnitTests.Core.Processes.Steps.Proposals
{
    public class UpdateAssessmentCriteriaTests
    {
        private readonly UpdateAssessmentCriteria _step;
        private readonly IBlockRepository _blockRepository;

        public UpdateAssessmentCriteriaTests()
        {
            _blockRepository = A.Fake<IBlockRepository>();
            _step = new UpdateAssessmentCriteria(_blockRepository);
        }

        [Fact]
        public void UpdateAssessmentCriteria_WithContextContainingApplicationTypeWithMainCriteria_ShouldUpdateAssessmentCriteriaOnCurrentPage()
        {
            var expectedContentArea = (ContentArea)ContentAreaBuilder.Create.Default();
            A.CallTo(() => _blockRepository.UpdateBlocks(
                    A<IReadOnlyCollection<MainCriterion>>._,
                    A<ContentArea>._,
                    A<ContentReference>._,
                    A<CultureInfo>._,
                    A<Action<AssessmentCriterionBlock, MainCriterion>>._))
                .Returns(expectedContentArea);

            var context = GetDefaultContextWithApplicationTypeWithMainCriteria();

            _step.Context = context;
            _step.Run();

            Assert.Same(expectedContentArea, context.CurrentPage.AssessmentCriteria);
        }

        ProposalContext GetDefaultContextWithApplicationTypeWithMainCriteria() =>
            new ProposalContext(A.Fake<IIntegrationLogger>())
            {
                LanguageBranch = LanguageConstants.MasterLanguageBranch,
                CurrentPage = ProposalPageBuilder.Create.Default(),
                Proposal = ProposalBuilder.Create.Default(
                    ProgramBuilder.Create.Default(),
                    ApplicationTypeBuilder.Create.Default()
                    .WithMainCriteria((MainCriterion[])MainCriterionBuilder.Create.Default())
                    )
            };
    }
}