using FakeItEasy;
using Forskningsradet.Common.Logging;
using Forskningsradet.Core.Processes.Contexts;
using Forskningsradet.Core.Processes.Steps.Proposals;
using Forskningsradet.UnitTests.Builders;
using Xunit;

namespace Forskningsradet.UnitTests.Core.Processes.Steps.Proposals
{
    public class CancelProposalTests
    {
        private readonly CancelProposal _step;

        public CancelProposalTests()
        {
            _step = new CancelProposal();
        }

        [Fact]
        public void CancelProposal_WithCurrentPageWithProposalAmount_ShouldSetProposalAmountToEmpty()
        {
            var context = new ProposalContext(A.Fake<IIntegrationLogger>())
            {
                CurrentPage = ProposalPageBuilder.Create.Default()
                    .WithProposalAmount("100")
            };

            _step.Context = context;
            _step.Run();

            Assert.Equal(string.Empty, context.CurrentPage.ProposalAmount);
        }
    }
}