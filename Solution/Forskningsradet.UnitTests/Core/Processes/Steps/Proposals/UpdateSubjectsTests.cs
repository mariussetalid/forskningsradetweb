using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using EPiServer.Core;
using FakeItEasy;
using Forskningsradet.Common.Constants;
using Forskningsradet.Common.Logging;
using Forskningsradet.Core.Models.ApiModels.Evurdering;
using Forskningsradet.Core.Models.ContentModels.Blocks;
using Forskningsradet.Core.Processes.Contexts;
using Forskningsradet.Core.Processes.Steps.Proposals;
using Forskningsradet.Core.Repositories.Contracts;
using Forskningsradet.UnitTests.Builders;
using Xunit;

namespace Forskningsradet.UnitTests.Core.Processes.Steps.Proposals
{
    public class UpdateSubjectsTests
    {
        private readonly UpdateSubjects _step;
        private readonly IBlockRepository _blockRepository;

        public UpdateSubjectsTests()
        {
            _blockRepository = A.Fake<IBlockRepository>();
            _step = new UpdateSubjects(_blockRepository);
        }

        [Fact]
        public void UpdateSubjects_WithApplicationType_ShouldUpdateSubjectBlocks()
        {
            var expectedContentArea = (ContentArea)ContentAreaBuilder.Create.Default();
            A.CallTo(() => _blockRepository.UpdateBlocks(A<IReadOnlyCollection<TranslatedSubject>>._, A<ContentArea>._, A<ContentReference>._, A<CultureInfo>._, A<Action<SubjectBlock, TranslatedSubject>>._))
                .Returns(expectedContentArea);

            var applicationTypeBuilder = ApplicationTypeBuilder.Create.Default()
                .WithSubjects((List<Subject>)SubjectBuilder.Create.Default().WithSubSubjects());
            var context = new ProposalContext(A.Fake<IIntegrationLogger>())
            {
                LanguageBranch = LanguageConstants.MasterLanguageBranch,
                CurrentPage = ProposalPageBuilder.Create.Default(),
                Proposal = ProposalBuilder.Create.Default(ProgramBuilder.Create.Default(), applicationTypeBuilder)
            };

            _step.Context = context;
            _step.Run();

            Assert.Same(expectedContentArea, context.CurrentPage.SubjectBlocks);
        }

        [Fact]
        public void UpdateSubjects_WithApplicationTypeWithSubject_ShouldMapTranslatedSubjectWithId()
        {
            var expectedId = "UTLPSHELSE";

            var applicationTypeBuilder = ApplicationTypeBuilder.Create.Default()
                .WithSubjects((List<Subject>)SubjectBuilder.Create.Default().WithCode(expectedId));
            var context = new ProposalContext(A.Fake<IIntegrationLogger>())
            {
                LanguageBranch = LanguageConstants.MasterLanguageBranch,
                CurrentPage = ProposalPageBuilder.Create.Default(),
                Proposal = ProposalBuilder.Create.Default(ProgramBuilder.Create.Default(), applicationTypeBuilder)
            };

            _step.Context = context;
            _step.Run();

            A.CallTo(() => _blockRepository.UpdateBlocks(
                A<IReadOnlyCollection<TranslatedSubject>>.That.Matches(collection => collection.Any(x => x.Id == expectedId)),
                A<ContentArea>._,
                A<ContentReference>._,
                A<CultureInfo>._,
                A<Action<SubjectBlock, TranslatedSubject>>._))
                .MustHaveHappened();
        }

        [Fact]
        public void UpdateSubjects_WithApplicationTypeWithSubjectAndWithEnglish_ShouldMapTranslatedSubjectWithEnglishTitle()
        {
            var expectedTitle = "English";

            var applicationTypeBuilder = ApplicationTypeBuilder.Create.Default()
                .WithSubjects((List<Subject>)SubjectBuilder.Create.Default().WithName("Norwegian", expectedTitle));
            var context = new ProposalContext(A.Fake<IIntegrationLogger>())
            {
                LanguageBranch = LanguageConstants.EnglishLanguageBranch,
                CurrentPage = ProposalPageBuilder.Create.Default(),
                Proposal = ProposalBuilder.Create.Default(ProgramBuilder.Create.Default(), applicationTypeBuilder)
            };

            _step.Context = context;
            _step.Run();

            A.CallTo(() => _blockRepository.UpdateBlocks(
                A<IReadOnlyCollection<TranslatedSubject>>.That.Matches(collection => collection.Any(x => x.Title == expectedTitle)),
                A<ContentArea>._,
                A<ContentReference>._,
                A<CultureInfo>._,
                A<Action<SubjectBlock, TranslatedSubject>>._))
                .MustHaveHappened();
        }

        [Fact]
        public void UpdateSubjects_WithApplicationTypeWithSubsubjects_ShouldMapTranslatedSubjectChildren()
        {
            var applicationTypeBuilder = ApplicationTypeBuilder.Create.Default()
                .WithSubjects((List<Subject>)SubjectBuilder.Create.Default().WithSubSubjects());
            var context = new ProposalContext(A.Fake<IIntegrationLogger>())
            {
                LanguageBranch = LanguageConstants.MasterLanguageBranch,
                CurrentPage = ProposalPageBuilder.Create.Default(),
                Proposal = ProposalBuilder.Create.Default(ProgramBuilder.Create.Default(), applicationTypeBuilder)
            };

            _step.Context = context;
            _step.Run();

            A.CallTo(() => _blockRepository.UpdateBlocks(
                A<IReadOnlyCollection<TranslatedSubject>>.That.Matches(collection => collection.Any(x => x.SubjectChildren.Any())),
                A<ContentArea>._,
                A<ContentReference>._,
                A<CultureInfo>._,
                A<Action<SubjectBlock, TranslatedSubject>>._))
                .MustHaveHappened();
        }
    }
}