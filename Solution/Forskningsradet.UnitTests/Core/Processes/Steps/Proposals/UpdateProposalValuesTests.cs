using System;
using EPiServer.Core;
using FakeItEasy;
using Forskningsradet.Common.Constants;
using Forskningsradet.Common.Logging;
using Forskningsradet.Core.Models.ApiModels.Evurdering;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Processes.Contexts;
using Forskningsradet.Core.Processes.Steps.Proposals;
using Forskningsradet.Core.Repositories.Contracts;
using Forskningsradet.UnitTests.Builders;
using Xunit;

namespace Forskningsradet.UnitTests.Core.Processes.Steps.Proposals
{
    public class UpdateProposalValuesTests
    {
        private readonly UpdateProposalValues _step;
        private readonly IPageRepository _pageRepository;

        public UpdateProposalValuesTests()
        {
            _pageRepository = A.Fake<IPageRepository>();
            _step = new UpdateProposalValues(_pageRepository);
        }

        [Fact]
        [UseCulture("nb-NO")]
        public void WithProposal_ShouldUpdatePageValues()
        {
            const int expectedMinApplicationAmount = 1000;
            const int expectedMaxApplicationAmount = 2000;
            const string nonBreakingSpace = " ";
            var expectedAmount = $"10{nonBreakingSpace}000";

            var applicationTypePageBuilder = ApplicationTypePageBuilder.Create.Default()
                .WithPageLink(PageReference.SelfReference);
            A.CallTo(() => _pageRepository.GetPageByExternalId<ApplicationTypePage>(A<string>._, A<PageReference>._, A<string>._))
                .Returns(applicationTypePageBuilder);

            var programBuilder = ProgramBuilder.Create.Default();
            var applicationTypeBuilder = ApplicationTypeBuilder.Create.Default();
            var context = new ProposalContext(A.Fake<IIntegrationLogger>())
            {
                LanguageBranch = LanguageConstants.MasterLanguageBranch,
                CurrentPage = ProposalPageBuilder.Create.Default(),
                Proposal = ProposalBuilder.Create.Default(programBuilder, applicationTypeBuilder)
            };

            _step.Context = context;
            _step.Run();

            Assert.Equal(context.Proposal.Title, context.CurrentPage.Name);
            Assert.Equal(((Program)programBuilder).Title, context.CurrentPage.Activity);
            Assert.Equal(context.Proposal.Status, context.CurrentPage.ProposalState);
            Assert.Equal(context.Proposal.DeadlineType, context.CurrentPage.DeadlineType);
            Assert.Equal(expectedAmount, context.CurrentPage.ProposalAmount);
            Assert.Equal(context.Proposal.CaseResponsible, context.CurrentPage.CaseResponsible);
            Assert.Equal(context.Proposal.StartDate, context.CurrentPage.StartDate);
            Assert.Equal(expectedMinApplicationAmount, context.CurrentPage.MinApplicationAmount);
            Assert.Equal(expectedMaxApplicationAmount, context.CurrentPage.MaxApplicationAmount);

            ApplicationType expectedApplicationType = applicationTypeBuilder;
            Assert.Equal(expectedApplicationType.MinProjectLength, context.CurrentPage.MinProjectLength);
            Assert.Equal(expectedApplicationType.MaxProjectLength, context.CurrentPage.MaxProjectLength);
            Assert.Equal(((ApplicationTypePage)applicationTypePageBuilder).PageLink, context.CurrentPage.ApplicationTypeReference);
            Assert.Equal(expectedApplicationType.EarliestStartDate, context.CurrentPage.EarliestProjectPeriodStart);
            Assert.Equal(expectedApplicationType.LatestProjectPeriodStart, context.CurrentPage.LatestProjectPeriodStart);
            Assert.Equal(expectedApplicationType.LatestProjectPeriodEnd, context.CurrentPage.LatestProjectPeriodEnd);
        }

        [Fact]
        public void UpdateProposalValues_WithEnglishLanguageBranch_ShouldSetPageNameToEnglishName()
        {
            var context = new ProposalContext(A.Fake<IIntegrationLogger>())
            {
                LanguageBranch = LanguageConstants.EnglishLanguageBranch,
                CurrentPage = ProposalPageBuilder.Create.Default(),
                Proposal = ProposalBuilder.Create
                    .Default(ProgramBuilder.Create.Default(), ApplicationTypeBuilder.Create.Default())
                    .WithEnglishTitle("English")
            };

            _step.Context = context;
            _step.Run();

            Assert.Equal(context.Proposal.EnglishTitle, context.CurrentPage.Name);
        }

        [Fact]
        public void UpdateProposalValues_WithProposal_ShouldNotChangeOriginalPage()
        {
            var expectedOriginalName = "name";
            var expectedOriginalActivity = "activity";
            var expectedOriginalProposalState = ProposalState.Active;
            var expectedOriginalDeadlineType = DeadlineType.NotSet;
            var expectedOriginalCaseResponsible = "responsible";
            DateTime? expectedOriginalStartDate = null;
            var originalPage = ProposalPageBuilder.Create.Default()
                .WithName(expectedOriginalName)
                .WithActivity(expectedOriginalActivity)
                .WithProposalState(expectedOriginalProposalState)
                .WithDeadlineType(expectedOriginalDeadlineType)
                .WithCaseResponsible(expectedOriginalCaseResponsible)
                .WithStartDate(expectedOriginalStartDate);

            var context = new ProposalContext(A.Fake<IIntegrationLogger>())
            {
                LanguageBranch = LanguageConstants.MasterLanguageBranch,
                CurrentPage = ProposalPageBuilder.Create.Default(),
                OriginalPage = originalPage,
                Proposal = ProposalBuilder.Create.Default(ProgramBuilder.Create.Default(), ApplicationTypeBuilder.Create.Default())
            };

            _step.Context = context;
            _step.Run();

            Assert.Equal(expectedOriginalName, context.OriginalPage.Name);
            Assert.Equal(expectedOriginalActivity, context.OriginalPage.Activity);
            Assert.Equal(expectedOriginalProposalState, context.OriginalPage.ProposalState);
            Assert.Equal(expectedOriginalDeadlineType, context.OriginalPage.DeadlineType);
            Assert.Equal(expectedOriginalCaseResponsible, context.OriginalPage.CaseResponsible);
            Assert.Equal(expectedOriginalStartDate, context.OriginalPage.StartDate);
        }
    }
}