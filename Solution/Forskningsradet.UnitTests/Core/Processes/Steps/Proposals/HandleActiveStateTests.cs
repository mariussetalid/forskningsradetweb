using FakeItEasy;
using Forskningsradet.Common.Logging;
using Forskningsradet.Core.Models.ApiModels.Evurdering;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Processes.Contexts;
using Forskningsradet.Core.Processes.Steps.Proposals;
using Forskningsradet.Core.Repositories.Contracts;
using Forskningsradet.UnitTests.Builders;
using Xunit;

namespace Forskningsradet.UnitTests.Core.Processes.Steps.Proposals
{
    public class HandleActiveStateTests
    {
        private readonly HandleActiveState _step;
        private readonly IPageRepository _pageRepository;

        public HandleActiveStateTests()
        {
            _pageRepository = A.Fake<IPageRepository>();
            _step = new HandleActiveState(_pageRepository);
        }

        [Theory]
        [InlineData(ProposalState.Active)]
        [InlineData(ProposalState.Cancelled)]
        public void HandleActiveState_WhenProposalStateIsActiveOrCancelled_ShouldSetCurrentPageAsArchivable(ProposalState proposalState)
        {
            var context = new ProposalContext(A.Fake<IIntegrationLogger>())
            {
                OriginalPage = ProposalPageBuilder.Create.Default(),
                CurrentPage = ProposalPageBuilder.Create.Default(),
                Proposal = ProposalBuilder.Create.Default(ProgramBuilder.Create.Default(), ApplicationTypeBuilder.Create.Default())
                    .WithStatus(proposalState)
            };

            _step.Context = context;
            _step.Run();

            Assert.True(context.CurrentPage.IsArchivable);
        }

        [Theory]
        [InlineData(ProposalState.Planned)]
        [InlineData(ProposalState.Completed)]
        [InlineData(ProposalState.Deleted)]
        public void HandleActiveState_WhenProposalStateIsPlannedOrCompletedOrDeleted_ShouldSetCurrentPageAsNonArchivable(ProposalState proposalState)
        {
            var context = new ProposalContext(A.Fake<IIntegrationLogger>())
            {
                OriginalPage = ProposalPageBuilder.Create.Default(),
                CurrentPage = ProposalPageBuilder.Create.Default(),
                Proposal = ProposalBuilder.Create.Default(ProgramBuilder.Create.Default(), ApplicationTypeBuilder.Create.Default())
                    .WithStatus(proposalState)
            };

            _step.Context = context;
            _step.Run();

            Assert.False(context.CurrentPage.IsArchivable);
        }

        [Theory]
        [InlineData(false, ProposalState.Active)]
        [InlineData(false, ProposalState.Cancelled)]
        [InlineData(true, ProposalState.Planned)]
        [InlineData(false, ProposalState.Completed)]
        [InlineData(false, ProposalState.Deleted)]
        public void HandleActiveState_WhenOriginalPageIsPublishedAndPlanned_ShouldSetPublishableOnContext(bool expectedIsPublishable, ProposalState proposalState)
        {
            var context = new ProposalContext(A.Fake<IIntegrationLogger>())
            {
                OriginalPage = ProposalPageBuilder.Create.Default()
                    .WithProposalState(proposalState),
                CurrentPage = ProposalPageBuilder.Create.Default(),
                Proposal = ProposalBuilder.Create.Default(ProgramBuilder.Create.Default(), ApplicationTypeBuilder.Create.Default())
            };
            A.CallTo(() => _pageRepository.IsPublished(A<ProposalPage>._))
                .Returns(true);

            _step.Context = context;
            _step.Run();

            Assert.Equal(expectedIsPublishable, context.IsPublishable);
        }

        [Theory]
        [InlineData(true, ProposalState.Active)]
        [InlineData(false, ProposalState.Cancelled)]
        [InlineData(true, ProposalState.Planned)]
        [InlineData(false, ProposalState.Completed)]
        [InlineData(false, ProposalState.Deleted)]
        public void HandleActiveState_WhenOriginalPageIsActiveAndDeadlineTypeContinuous_ShouldSetPublishableOnContext(bool expectedIsPublishable, ProposalState proposalState)
        {
            var context = new ProposalContext(A.Fake<IIntegrationLogger>())
            {
                OriginalPage = ProposalPageBuilder.Create.Default()
                    .WithProposalState(proposalState)
                    .WithDeadlineType(DeadlineType.Continuous),
                CurrentPage = ProposalPageBuilder.Create.Default(),
                Proposal = ProposalBuilder.Create.Default(ProgramBuilder.Create.Default(), ApplicationTypeBuilder.Create.Default())
            };
            A.CallTo(() => _pageRepository.IsPublished(A<ProposalPage>._))
                .Returns(true);

            _step.Context = context;
            _step.Run();

            Assert.Equal(expectedIsPublishable, context.IsPublishable);
        }
    }
}