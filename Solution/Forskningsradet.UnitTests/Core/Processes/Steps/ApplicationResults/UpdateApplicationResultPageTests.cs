using FakeItEasy;
using Forskningsradet.Common.Logging;
using Forskningsradet.Core.Models.ApiModels.DWH;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Processes.Contexts;
using Forskningsradet.Core.Processes.Steps.ApplicationResults;
using Xunit;

namespace Forskningsradet.UnitTests.Core.Processes.Steps.ApplicationResults
{
    public class UpdateApplicationResultPageTests
    {
        private readonly UpdateApplicationResultPage _step;
        private readonly ApplicationResultContext _context;

        public UpdateApplicationResultPageTests()
        {
            _context = new ApplicationResultContext(A.Fake<IIntegrationLogger>())
            {
                Page = A.Fake<ApplicationResultPage>(),
                ApplicationResult = new ApplicationResult()
            };
            _step = new UpdateApplicationResultPage {Context = _context};
        }
        
        [Fact]
        public void WithAppliedAmount_ShouldUpdateAppliedAmount()
        {
            _context.ApplicationResult.AppliedAmount = 10.2;
            
            _step.Run();
            
            Assert.Equal("10", _context.Page.AppliedAmount);
        }
        
        [Fact]
        public void WithAwardedAmount_ShouldUpdateAwardedAmount()
        {
            _context.ApplicationResult.AwardedAmount = 10.6;
            
            _step.Run();
            
            Assert.Equal("10", _context.Page.AwardedAmount);
        }
        
        [Fact]
        public void WithReceivedApplications_ShouldUpdateNumberOfApplications()
        {
            _context.ApplicationResult.ReceivedApplications = 10;
            
            _step.Run();
            
            Assert.Equal("10", _context.Page.NumberOfApplications);
        }
        
        [Fact]
        public void WithReceivedAndRejectedApplications_ShouldUpdateNumberOfApprovedApplications()
        {
            _context.ApplicationResult.ReceivedApplications = 10;
            _context.ApplicationResult.RejectedApplications = 5;
            
            _step.Run();
            
            Assert.Equal("5", _context.Page.NumberOfApprovedApplications);
        }
        
        [Fact]
        public void WithGradeDistributions_ShouldUpdateGradeDistribution()
        {
            _context.ApplicationResult.GradeDistributions = new[]
            {
                new GradeDistribution
                {
                    Grade = 1,
                    Count = 2
                }
            };
            
            _step.Run();
            
            Assert.Equal("1", _context.Page.GradeDistribution[0].Grade);
            Assert.Equal("2", _context.Page.GradeDistribution[0].Count);
        }
        
        [Fact]
        public void WithApplications_ShouldUpdateGrantedProjects()
        {
            const string expectedProjectTitle = "title";
            const string expectedOrganization = "org";
            _context.ApplicationResult.Applications = new[]
            {
                new Application
                {
                    ProjectNumber = 1,
                    ProjectTitle = expectedProjectTitle,
                    Organization = expectedOrganization
                }
            };
            
            _step.Run();
            
            Assert.Equal("1", _context.Page.GrantedProjects[0].Number);
            Assert.Equal(expectedProjectTitle, _context.Page.GrantedProjects[0].Title);
            Assert.Equal(expectedOrganization, _context.Page.GrantedProjects[0].Organization);
        }
        
        [Fact]
        public void WithSubjectSpecialists_ShouldUpdateSubjectSpecialists()
        {
            const string expectedName = "name";
            const string expectedOrganization = "org";
            _context.ApplicationResult.SubjectSpecialists = new[]
            {
                new SubjectSpecialist
                {
                    Name = expectedName,
                    Country = expectedOrganization
                }
            };
            
            _step.Run();
            
            Assert.Equal(expectedName, _context.Page.SubjectSpecialists[0].Name);
            Assert.Equal(expectedOrganization, _context.Page.SubjectSpecialists[0].Organization);
        }
    }
}