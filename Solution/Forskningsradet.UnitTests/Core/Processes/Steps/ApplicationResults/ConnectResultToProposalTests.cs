using EPiServer.Core;
using FakeItEasy;
using Forskningsradet.Common.Logging;
using Forskningsradet.Core.Models.ApiModels.DWH;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Processes.Contexts;
using Forskningsradet.Core.Processes.Steps.ApplicationResults;
using Forskningsradet.Core.Repositories.Contracts;
using Xunit;

namespace Forskningsradet.UnitTests.Core.Processes.Steps.ApplicationResults
{
    public class ConnectResultToProposalTests
    {
        private readonly IPageRepository _pageRepository;
        private readonly ApplicationResultContext _context;
        private readonly ConnectResultToProposal _step;

        public ConnectResultToProposalTests()
        {
            var applicationResultPage = A.Fake<ApplicationResultPage>();
            applicationResultPage.PageLink = new PageReference(1234);
            _context = new ApplicationResultContext(A.Fake<IIntegrationLogger>())
            {
                Page = applicationResultPage,
                ApplicationResult = new ApplicationResult
                {
                    ProposalId = 123
                }
            };
            _pageRepository = A.Fake<IPageRepository>();
            _step = new ConnectResultToProposal(_pageRepository) {Context = _context};

            var frontPage = A.Fake<FrontPage>();
            frontPage.ProposalsPage = new PageReference(1);
            A.CallTo(() => _pageRepository.GetCurrentStartPage())
                .Returns(frontPage);
        }
        
        [Fact]
        public void WhenProposalPageExistsAndIsPublished_ShouldConnectResultsAndRePublish()
        {
            A.CallTo(() => _pageRepository.GetPageByExternalId<ProposalPage>(A<string>._, A<PageReference>._, A<string>._))
                .Returns(new ClonableProposalPage());

            A.CallTo(() => _pageRepository.IsPublished(A<PageData>._)).Returns(true);
            
            _step.Run();
            
            A.CallTo(() => _pageRepository.Save(A<PageData>._)).MustHaveHappened();
            A.CallTo(() => _pageRepository.SaveAsDraft(A<PageData>._)).MustNotHaveHappened();
        }

        [Fact]
        public void WhenProposalPageExistsAndIsInDraft_ShouldConnectResultAndSaveDraft()
        {
            A.CallTo(() => _pageRepository.GetPageByExternalId<ProposalPage>(A<string>._, A<PageReference>._, A<string>._))
                .Returns(new ClonableProposalPage());

            A.CallTo(() => _pageRepository.IsPublished(A<PageData>._)).Returns(false);
            
            _step.Run();
            
            A.CallTo(() => _pageRepository.Save(A<PageData>._)).MustNotHaveHappened();
            A.CallTo(() => _pageRepository.SaveAsDraft(A<PageData>._)).MustHaveHappened();
        }

        [Fact]
        public void WhenProposalPageIsAlreadyConnected_ShouldNotUpdatePage()
        {
            var page = new ClonableProposalPage {ApplicationResult = _context.Page.PageLink};

            A.CallTo(() => _pageRepository.GetPageByExternalId<ProposalPage>(A<string>._, A<PageReference>._, A<string>._))
                .Returns(page);

            A.CallTo(() => _pageRepository.IsPublished(A<PageData>._)).Returns(true);
            
            _step.Run();
            
            A.CallTo(() => _pageRepository.Save(A<PageData>._)).MustNotHaveHappened();
            A.CallTo(() => _pageRepository.SaveAsDraft(A<PageData>._)).MustNotHaveHappened();
        }
        
        private class ClonableProposalPage : ProposalPage
        {
            protected override object CreateWriteableCloneImplementation() => this;
        }
    }
}