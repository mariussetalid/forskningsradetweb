using System.Collections.Generic;
using FakeItEasy;
using Forskningsradet.Common.Logging;
using Forskningsradet.Core.Processes.Contexts;
using Forskningsradet.Core.Processes.Steps.UpdatePressReleases;
using Forskningsradet.ServiceAgents.Contracts;
using Forskningsradet.ServiceAgents.ResponseModels;
using Xunit;

namespace Forskningsradet.UnitTests.Core.Processes.Steps.UpdatePressReleases
{
    public class FetchPressReleasesTests
    {
        private readonly FetchPressReleases _step;
        private readonly INtbServiceAgent _ntbServiceAgent;
        private readonly UpdatePressReleasesContext _context;

        public FetchPressReleasesTests()
        {
            _ntbServiceAgent = A.Fake<INtbServiceAgent>();
            _step = new FetchPressReleases(_ntbServiceAgent);
            _context = new UpdatePressReleasesContext(A.Fake<IIntegrationLogger>());
            _step.Context = _context;
        }
        
        [Fact]
        public void Run_ShouldFetchFromServiceAgent()
        {
            _step.Run();
            
            A.CallTo(() => _ntbServiceAgent.GetPressReleases(0)).MustHaveHappenedOnceExactly();
        }
        
        [Fact]
        public void Run_WithSimpleResponse_ShouldAddToContext()
        {
            var pressReleases = CreatePressReleasesForTest(1, 1, null);
            A.CallTo(() => _ntbServiceAgent.GetPressReleases(A<int>._))
                .Returns(pressReleases);
            
            _step.Run();
            
            var result = _context.PressReleases[0];
            Assert.Equal(pressReleases.Releases[0].Id, result.Id);
        }
        
        [Fact]
        public void Run_WithMultiPageResponse_ShouldFetchFromServiceAgentUntilLastPage()
        {
            var page1 = CreatePressReleasesForTest(2, 1, 1);
            var page2 = CreatePressReleasesForTest(2, 1, null);
            A.CallTo(() => _ntbServiceAgent.GetPressReleases(0)).Returns(page1);
            A.CallTo(() => _ntbServiceAgent.GetPressReleases(1)).Returns(page2);
            
            _step.Run();

            A.CallTo(() => _ntbServiceAgent.GetPressReleases(A<int>._)).MustHaveHappenedTwiceExactly();
        }
        
        [Fact]
        public void Run_WithMultiPageResponse_ShouldCumulativelyAddToContext()
        {
            var page1 = CreatePressReleasesForTest(6, 4, 1);
            var page2 = CreatePressReleasesForTest(6, 2, null);
            A.CallTo(() => _ntbServiceAgent.GetPressReleases(0)).Returns(page1);
            A.CallTo(() => _ntbServiceAgent.GetPressReleases(1)).Returns(page2);
            
            _step.Run();

            Assert.Equal(6, _context.PressReleases.Count);
            Assert.Equal(page1.Releases[0].Id, _context.PressReleases[0].Id);
            Assert.Equal(page1.Releases[1].Id, _context.PressReleases[1].Id);
            Assert.Equal(page1.Releases[2].Id, _context.PressReleases[2].Id);
            Assert.Equal(page1.Releases[3].Id, _context.PressReleases[3].Id);
            Assert.Equal(page2.Releases[0].Id, _context.PressReleases[4].Id);
            Assert.Equal(page2.Releases[1].Id, _context.PressReleases[5].Id);
        }

        private static NtbPressReleases CreatePressReleasesForTest(int totalCount, int pressReleasesOnPage, int? nextPage)
        {
            var pressReleases = new NtbPressReleases
            {
                TotalCount = totalCount, 
                NextPage = nextPage, 
                Releases = new List<NtbPressRelease>()
            };

            for (var i = 1; i <= pressReleasesOnPage; i++)
            {
                pressReleases.Releases.Add(new NtbPressRelease
                {
                    Id = i
                });
            }
            
            return pressReleases;
        }
    }
}