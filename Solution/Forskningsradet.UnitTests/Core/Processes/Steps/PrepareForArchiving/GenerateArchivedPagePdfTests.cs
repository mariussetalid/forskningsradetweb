using FakeItEasy;
using Forskningsradet.Common.Logging;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Processes.Contexts;
using Forskningsradet.Core.Processes.Providers;
using Forskningsradet.Core.Processes.Steps.PrepareForArchiving;
using Xunit;

namespace Forskningsradet.UnitTests.Core.Processes.Steps.PrepareForArchiving
{
    public class GenerateArchivedPagePdfTests
    {
        private readonly IHtmlToPdfProvider _htmlToPdfProvider;
        private readonly PrepareForArchivingContext _context;
        private readonly GenerateArchivedPagePdf _step;

        public GenerateArchivedPagePdfTests()
        {
            _htmlToPdfProvider = A.Fake<IHtmlToPdfProvider>();
            _context = new PrepareForArchivingContext(A.Fake<IIntegrationLogger>())
                {Page = A.Fake<EditorialPage>()};
            _step = new GenerateArchivedPagePdf(_htmlToPdfProvider)
                {Context = _context};
        }
        
        [Fact]
        public void Run_WhenPageCanBeConvertedToPdf_ShouldReturnBase64Pdf()
        {
            const string expectedPdfString = "pdf";
            
            A.CallTo(() => _htmlToPdfProvider.GetPageAsArchivablePdf(A<EditorialPage>._))
                .Returns(expectedPdfString);
            
            _step.Run();
            
            Assert.Equal(expectedPdfString, _context.PdfContent);
        }

        [Fact]
        public void Run_WhenPdfGenerationFails_ShouldStopProcess()
        {
            A.CallTo(() => _htmlToPdfProvider.GetPageAsArchivablePdf(A<EditorialPage>._))
                .Returns(null);

            _step.Run();
            
            Assert.True(_context.Abort);
        }
    }
}