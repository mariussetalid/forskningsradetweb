using FakeItEasy;
using Forskningsradet.Common.Logging;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Processes.Contexts;
using Forskningsradet.Core.Processes.Steps.PrepareForArchiving;
using Xunit;

namespace Forskningsradet.UnitTests.Core.Processes.Steps.PrepareForArchiving
{
    public class ValidatePageForArchivingTests
    {
        [Fact]
        public void Run_WhenAllRequiredFieldsAreProvided_ShouldPass()
        {
            var page = A.Fake<NewsArticlePage>();
            page.Name = "test";
            page.CaseResponsible = "Gunnar";
            
            AssertThatRunSucceeds(page);
        }

        [Fact]
        public void Run_WithoutPageName_ShouldFailValidation()
        {
            var page = A.Fake<NewsArticlePage>();
            page.CaseResponsible = "Gunnar";
            
            AssertThatRunIsAborted(page);
        }
        
        [Fact]
        public void Run_WithoutCaseResponsible_ShouldFailValidation()
        {
            var page = A.Fake<NewsArticlePage>();
            page.Name = "test";
            
            AssertThatRunIsAborted(page);
        }
        
        [Fact]
        public void Run_WhenPageIsEmpty_ShouldFailValidation()
        {
            AssertThatRunIsAborted(null);
        }

        [Fact]
        public void Run_WhenPageIsProposalPage_MustHaveExternalIdSet()
        {
            var page = A.Fake<ProposalPage>();
            page.Name = "test";
            page.CaseResponsible = "Gunnar";
            page.Id = string.Empty;
            
            AssertThatRunIsAborted(page);
        }
        
        private static void AssertThatRunSucceeds(EditorialPage page)
            => RunWithPageData(page, false);
        
        private static void AssertThatRunIsAborted(EditorialPage page)
            => RunWithPageData(page, true);
        
        private static void RunWithPageData(EditorialPage page, bool shouldAbort)
        {
            var context = new PrepareForArchivingContext(A.Fake<IIntegrationLogger>())
            {
                Page = page
            };

            var step = new ValidatePageForArchiving {Context = context};

            step.Run();

            Assert.Equal(shouldAbort, context.Abort);
        }
    }
}