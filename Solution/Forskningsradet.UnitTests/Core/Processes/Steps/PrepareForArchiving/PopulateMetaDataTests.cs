using System;
using System.Globalization;
using EPiServer.Core;
using FakeItEasy;
using Forskningsradet.Common.Logging;
using Forskningsradet.Core.Models.ApiModels.Evurdering;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Processes.Contexts;
using Forskningsradet.Core.Processes.Providers;
using Forskningsradet.Core.Processes.Steps.PrepareForArchiving;
using Forskningsradet.Core.Services;
using Xunit;

namespace Forskningsradet.UnitTests.Core.Processes.Steps.PrepareForArchiving
{
    public class PopulateMetaDataTests
    {
        private readonly PopulateMetaData _step;
        private readonly IEpiPageVersionProvider _epiPageVersionProvider;
        private readonly ILocalizationProvider _localizationProvider;
        private readonly PrepareForArchivingContext _context;

        public PopulateMetaDataTests()
        {
            _context = new PrepareForArchivingContext(A.Fake<IIntegrationLogger>());
            _epiPageVersionProvider = A.Fake<IEpiPageVersionProvider>();
            _localizationProvider = A.Fake<ILocalizationProvider>();
            _step = new PopulateMetaData(_epiPageVersionProvider, _localizationProvider){Context = _context};
        }
        
        [Fact]
        public void WithPageData_ShouldPopulateContext()
        {
            var page = A.Fake<EditorialPage>();
            page.PageLink.ID = 123;
            page.CaseResponsible = "case responsible";
            
            _context.Page = page;
            
            _step.Run();
            
            Assert.Equal(page.PageLink.ID.ToString(), _context.CaseId);
            Assert.Equal(page.CaseResponsible, _context.CaseResponsible);
        }

        [Fact]
        public void WithProposalPage_ShouldUseCustomId()
        {
            const int pageId = 123;
            const string externalId = "321";
            
            var page = A.Fake<ProposalPage>();
            page.PageLink.ID = pageId;
            page.Id = externalId;
            
            _context.Page = page;
            
            _step.Run();
            
            Assert.Equal(externalId, _context.CaseId);
        }
        
        [Fact]
        public void WithEditorialPage_ShouldUseCaseTypeN()
        {
            _context.Page = A.Fake<EditorialPage>();
            
            _step.Run();
            
            Assert.Equal(ArchiveCaseType.N, _context.Type);
        }
        
        [Fact]
        public void WithProposalPage_ShouldUseTypeU()
        {
            _context.Page = A.Fake<ProposalPage>();
            
            _step.Run();
            
            Assert.Equal(ArchiveCaseType.U, _context.Type);
        }

        [Fact]
        public void WithEditorialPage_ShouldPrefixCaseTitle()
        {
            var page = A.Fake<EditorialPage>();
            page.Name = "title";
            _context.Page = page;
            
            _step.Run();
            
            Assert.Equal($"Nettside: {page.Name}", _context.Title);
        }
        
        [Fact]
        public void WithProposalPage_ShouldPrefixCaseTitle()
        {
            var page = A.Fake<ProposalPage>();
            page.Name = "title";
            page.DeadlineType = DeadlineType.Continuous;
            _context.Page = page;
            
            _step.Run();
            
            Assert.Equal($"Utlysning: {page.Name}", _context.Title);
        }
        
        [Fact]
        public void WithProposalPageAndDeadline_ShouldSuffixTitle()
        {
            var page = A.Fake<ProposalPage>();
            page.Name = "title";
            page.DeadlineType = DeadlineType.Date;
            page.Deadline = new DateTime(2019, 3, 22);
            _context.Page = page;

            A.CallTo(() => page.DeadlineComputed).Returns(page.Deadline);

            _step.Run();
            
            Assert.Equal($"Utlysning: {page.Name} 22.03.2019", _context.Title);
        }
        
        [Fact]
        public void WhenPageHasTitleOverride_ShouldUseOverrideValue()
        {
            var page = A.Fake<EditorialPage>();
            page.Name = "A";
            page.ArchiveTitleOverride = "B";
            _context.Page = page;
            
            _step.Run();
            
            Assert.Equal($"Nettside: {page.ArchiveTitleOverride}", _context.Title);
        }
        
        [Fact]
        public void WhenPageIsEditorialPage_ShouldNotSetActivity()
        {
            var page = A.Fake<EditorialPage>();
            _context.Page = page;
            
            _step.Run();
            
            Assert.Null(_context.Activity);
        }
        
        [Fact]
        public void WhenPageIsProposalPage_ShouldSetActivity()
        {
            var page = A.Fake<ProposalPage>();
            page.Activity = "activity";
            _context.Page = page;
            
            _step.Run();
            
            Assert.Equal(page.Activity, _context.Activity);
        }

        [Fact]
        public void WithEditorialPage_ShouldSuffixJournalTitleWithVersion()
        {
            const int expectedVersion = 1;
            
            var page = A.Fake<EditorialPage>();
            page.Name = "title";
            _context.Page = page;
            
            A.CallTo(() => _epiPageVersionProvider.GetPageVersion(A<PageData>._))
                .Returns(expectedVersion);
            
            _step.Run();

            Assert.Equal($"{page.Name} - {expectedVersion}", _context.JournalTitle);
        }
        
        [Fact]
        public void WithProposalPage_ShouldSuffixJournalTitleWithVersionAndState()
        {
            const int expectedVersion = 1;
            const string expectedStateName = "aktiv";
            const string expectedTitle = "title";
            
            var page = A.Fake<ProposalPage>();
            page.Name = expectedTitle;
            page.DeadlineType = DeadlineType.Continuous;
            page.ProposalState = ProposalState.Active;
            _context.Page = page;
            
            A.CallTo(() => _epiPageVersionProvider.GetPageVersion(A<PageData>._))
                .Returns(expectedVersion);

            A.CallTo(() => _localizationProvider.GetEnumName<ProposalState>(ProposalState.Active, A<CultureInfo>._))
                .Returns(expectedStateName);
            
            _step.Run();
            
            Assert.Equal($"{expectedTitle} - {expectedVersion} - {expectedStateName}", _context.JournalTitle);
        }

        [Fact]
        public void WhenPageInContextIsNull_ShouldAbort()
        {
            _context.Page = null;
            
            _step.Run();
            
            Assert.True(_context.Abort);
        }
    }
}