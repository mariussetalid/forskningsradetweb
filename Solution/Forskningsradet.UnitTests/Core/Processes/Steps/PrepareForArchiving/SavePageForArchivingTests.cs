using FakeItEasy;
using Forskningsradet.Common.Logging;
using Forskningsradet.Core.MessageBrokers;
using Forskningsradet.Core.Processes.Contexts;
using Forskningsradet.Core.Processes.Steps.PrepareForArchiving;
using Xunit;

namespace Forskningsradet.UnitTests.Core.Processes.Steps.PrepareForArchiving
{
    public class SavePageForArchivingTests
    {
        [Fact]
        public void Run_WithPreparedPage_ShouldCallRepositoryWithContextData()
        {
            const string expectedId = "123";
            const string expectedTitle = "Title";
            const ArchiveCaseType expectedType = ArchiveCaseType.N;
            const string expectedResponsible = "Responsible";
            const string expectedActivities = "activity";
            const string expectedJournalTitle = "Title - 1";
            const string expectedFileName = "Title.pdf";
            const string expectedFileContents = "OISDJF";

            var context = new PrepareForArchivingContext(A.Fake<IIntegrationLogger>())
            {
                CaseId = expectedId,
                Title = expectedTitle,
                Type = expectedType,
                CaseResponsible = expectedResponsible,
                Activity = expectedActivities,
                JournalTitle = expectedJournalTitle,
                PdfContent = expectedFileContents
            };

            var broker = A.Fake<IArchiveMessageBroker>();
            
            var step = new SavePageForArchiving(broker) {Context = context};
            
            step.Run();

            A.CallTo(() => broker.Send(
                    A<ArchivePage>.That.Matches(x =>
                           x.CaseId == expectedId.ToString()
                        && x.CaseTitle == expectedTitle
                        && x.Type == expectedType.ToString()
                        && x.Responsible == expectedResponsible
                        && x.Activities == expectedActivities
                        && x.JournalTitle == expectedJournalTitle
                        && x.FileName == expectedFileName
                        && x.FileContents == expectedFileContents
                        && !x.IsArchived
                    )))
                .MustHaveHappenedOnceExactly();
        }
    }
}