﻿using System;
using System.Collections.Generic;
using System.Linq;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Extensions;
using Forskningsradet.Core.Models.ApiModels.Evurdering;
using Forskningsradet.Core.Models.ContentModels.Pages;
using Forskningsradet.Core.Models.SearchModels;
using Forskningsradet.UnitTests.Builders;
using Xunit;

namespace Forskningsradet.UnitTests.Core.Extensions
{
    public class EnumerableExtensionTests
    {
        [Fact]
        public void GetOrderedProposals_WithFutureTimeFrame_ShouldOrderByDeadlineAscending()
        {
            var list = new List<ProposalBasePage>
            {
                ProposalPageBuilder.Create.Default().WithDeadline(new DateTime(2051, 1, 1, 0, 0, 0)),
                ProposalPageBuilder.Create.Default().WithDeadline(new DateTime(2053, 1, 1, 0, 0, 0)),
                ProposalPageBuilder.Create.Default().WithDeadline(new DateTime(2052, 1, 1, 0, 0, 0))
            };

            var result = list.GetOrderedProposals(TimeFrameFilter.Future).ToList();

            Assert.Equal(2051, result[0].Deadline.Value.Year);
            Assert.Equal(2052, result[1].Deadline.Value.Year);
            Assert.Equal(2053, result[2].Deadline.Value.Year);
        }

        [Fact]
        public void GetOrderedProposals_WithPastTimeFrame_ShouldOrderByDeadlineDescending()
        {
            var list = new List<ProposalBasePage>
            {
                ProposalPageBuilder.Create.Default().WithDeadline(new DateTime(2001, 1, 1, 0, 0, 0)),
                ProposalPageBuilder.Create.Default().WithDeadline(new DateTime(2003, 1, 1, 0, 0, 0)),
                ProposalPageBuilder.Create.Default().WithDeadline(new DateTime(2002, 1, 1, 0, 0, 0))
            };

            var result = list.GetOrderedProposals(TimeFrameFilter.Past).ToList();

            Assert.Equal(2003, result[0].Deadline.Value.Year);
            Assert.Equal(2002, result[1].Deadline.Value.Year);
            Assert.Equal(2001, result[2].Deadline.Value.Year);
        }

        [Fact]
        public void GetOrderedProposals_WithProposalWithoutDeadline_ShouldOrderClosestDeadlineFirstThenEveryoneWithoutDeadlineThenRestWithDeadline()
        {
            var list = new List<ProposalBasePage>
            {
                ProposalPageBuilder.Create.Default().WithDeadline(null),
                ProposalPageBuilder.Create.Default().WithDeadline(new DateTime(2051, 1, 1, 0, 0, 0)),
                ProposalPageBuilder.Create.Default().WithDeadline(new DateTime(2053, 1, 1, 0, 0, 0)),
                ProposalPageBuilder.Create.Default().WithDeadline(new DateTime(2052, 1, 1, 0, 0, 0))
            };

            var result = list.GetOrderedProposals(TimeFrameFilter.Future).ToList();

            Assert.Equal(2051, result[0].Deadline.Value.Year);
            Assert.Null(result[1].Deadline);
            Assert.Equal(2052, result[2].Deadline.Value.Year);
            Assert.Equal(2053, result[3].Deadline.Value.Year);
        }

        [Fact]
        public void GetOrderedProposals_WithSameDeadline_ShouldOrderBySortingNumberInListAscending()
        {
            var list = new List<ProposalBasePage>
            {
                ProposalPageBuilder.Create
                    .Default()
                    .WithDeadline(new DateTime(2050, 1, 1, 0, 0, 0))
                    .WithSortingNumberInList(2),
                ProposalPageBuilder.Create
                    .Default()
                    .WithDeadline(new DateTime(2050, 1, 1, 0, 0, 0))
                    .WithSortingNumberInList(null),
                ProposalPageBuilder.Create
                    .Default()
                    .WithDeadline(new DateTime(2050, 1, 1, 0, 0, 0))
                    .WithSortingNumberInList(1),
            };

            var result = list.GetOrderedProposals(TimeFrameFilter.Future).ToList();

            Assert.Equal(1, result[0].SortingNumberInList);
            Assert.Equal(2, result[1].SortingNumberInList);
            Assert.Null(result[2].SortingNumberInList);
        }

        [Theory]
        [InlineData(TimeFrameFilter.Future)]
        [InlineData(TimeFrameFilter.Past)]
        [InlineData(TimeFrameFilter.Result)]
        public void GetOrderedProposals_WithDifferentProposalState_ShouldBeInSpecificOrder(TimeFrameFilter timeFrame)
        {
            var list = new List<ProposalBasePage>
            {
                ProposalPageBuilder.Create.Default().WithProposalState(ProposalState.Completed),
                ProposalPageBuilder.Create.Default().WithProposalState(ProposalState.Planned),
                ProposalPageBuilder.Create.Default().WithProposalState(ProposalState.Active),
                ProposalPageBuilder.Create.Default().WithProposalState(ProposalState.Deleted),
                ProposalPageBuilder.Create.Default().WithProposalState(ProposalState.Cancelled),
            };

            var result = list.GetOrderedProposals(timeFrame).ToList();

            Assert.Equal(ProposalState.Deleted, result[0].ProposalState);
            Assert.Equal(ProposalState.Completed, result[1].ProposalState);
            Assert.Equal(ProposalState.Active, result[2].ProposalState);
            Assert.Equal(ProposalState.Planned, result[3].ProposalState);
            Assert.Equal(ProposalState.Cancelled, result[4].ProposalState);
        }

        [Fact]
        public void OrderFilterOptons_WhenQueryParameterIsNotDeadlineRelated_ShouldOrderAlphabetically()
        {
            var list = new List<FilterOption>
            {
                new FilterOption { Name = "c", Value = "-12378545" },
                new FilterOption { Name = "b", Value = "-12378545" },
                new FilterOption { Name = "a", Value = "-12378545" }
            };

            var result = list.OrderFilterOptons("");

            Assert.Equal("a", result.ElementAt(0).Name);
            Assert.Equal("b", result.ElementAt(1).Name);
            Assert.Equal("c", result.ElementAt(2).Name);
        }

        [Fact]
        public void OrderFilterOptons_WhenQueryParameterIsDeadlineTypes_ShouldOrderByDateValueDescendingThenAlphabetically()
        {
            var deadlineValueForMay22nd2019 = "-8586431236854775808";
            var deadlineValueForMay6th2020 = "-8586128836854775808";
            var deadlineValueForMay27th2020 = "-8586110692854775808";
            var list = new List<FilterOption>
            {
                new FilterOption { Name = "a", Value = "Løpende" },
                new FilterOption { Name = "b", Value = deadlineValueForMay6th2020 },
                new FilterOption { Name = "c", Value = deadlineValueForMay27th2020 },
                new FilterOption { Name = "d", Value = "Planlagt" },
                new FilterOption { Name = "e", Value = deadlineValueForMay22nd2019 }
            };

            var result = list.OrderFilterOptons(QueryParameters.DeadlineTypes);

            Assert.Equal("c", result.ElementAt(0).Name);
            Assert.Equal("b", result.ElementAt(1).Name);
            Assert.Equal("e", result.ElementAt(2).Name);
            Assert.Equal("a", result.ElementAt(3).Name);
            Assert.Equal("d", result.ElementAt(4).Name);
        }

        [Fact]
        public void OrderFilterOptons_WhenQueryParameterIsDeadlineTypesAndTimeFrameIsFuture_ShouldOrderByDateValueAscendingThenAlphabetically()
        {
            var deadlineValueForMay22nd2019 = "-8586431236854775808";
            var deadlineValueForMay6th2020 = "-8586128836854775808";
            var deadlineValueForMay27th2020 = "-8586110692854775808";
            var list = new List<FilterOption>
            {
                new FilterOption { Name = "a", Value = "Løpende" },
                new FilterOption { Name = "b", Value = deadlineValueForMay6th2020 },
                new FilterOption { Name = "c", Value = deadlineValueForMay27th2020 },
                new FilterOption { Name = "d", Value = "Planlagt" },
                new FilterOption { Name = "e", Value = deadlineValueForMay22nd2019 }
            };

            var result = list.OrderFilterOptons(QueryParameters.DeadlineTypes, TimeFrameFilter.Future);

            Assert.Equal("e", result.ElementAt(0).Name);
            Assert.Equal("b", result.ElementAt(1).Name);
            Assert.Equal("c", result.ElementAt(2).Name);
            Assert.Equal("a", result.ElementAt(3).Name);
            Assert.Equal("d", result.ElementAt(4).Name);
        }

        [Fact]
        public void OrderFilterOptons_WhenQueryParameterIsDeadlines_ShouldOrderByValueDescending()
        {
            var list = new List<FilterOption>
            {
                new FilterOption { Name = "a", Value = "-8888" },
                new FilterOption { Name = "b", Value = "-9999" },
                new FilterOption { Name = "c", Value = "-7777" }
            };

            var result = list.OrderFilterOptons(QueryParameters.Deadlines);

            Assert.Equal("b", result.ElementAt(0).Name);
            Assert.Equal("a", result.ElementAt(1).Name);
            Assert.Equal("c", result.ElementAt(2).Name);
        }

        [Fact]
        public void OrderRelatedProposals_WhenListIsOfProposalPages_ShouldOrderCorrectly()
        {
            var page0 = new ProposalPage { ProposalState = ProposalState.Active, DeadlineType = DeadlineType.Date };
            var page1 = new ProposalPage { ProposalState = ProposalState.Active, DeadlineType = DeadlineType.Continuous };
            var page2 = new ProposalPage { ProposalState = ProposalState.Planned, DeadlineType = DeadlineType.Date };
            var page3 = new ProposalPage { ProposalState = ProposalState.Planned, DeadlineType = DeadlineType.Continuous };
            var page4 = new ProposalPage { ProposalState = ProposalState.Planned, DeadlineType = DeadlineType.NotSet };

            var list = new List<ProposalPage>
            {
                page3,
                page4,
                page0,
                page1,
                page2,
            };

            var result = list.OrderRelatedProposals();

            Assert.Equal(page0, result.ElementAt(0));
            Assert.Equal(page1, result.ElementAt(1));
            Assert.Equal(page2, result.ElementAt(2));
            Assert.Equal(page3, result.ElementAt(3));
            Assert.Equal(page4, result.ElementAt(4));
        }
    }
}
