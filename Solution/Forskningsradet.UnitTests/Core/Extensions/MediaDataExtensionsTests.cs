using System.IO;
using EPiServer.Core;
using EPiServer.Framework.Blobs;
using FakeItEasy;
using Forskningsradet.Core.Extensions;
using Xunit;

namespace Forskningsradet.UnitTests.Core.Extensions
{
    public class MediaDataExtensionsTests
    {
        [Theory]
        [UseCulture("nb-NO")]
        [InlineData(0, "0 B")]
        [InlineData(1, "1 B")]
        [InlineData(1000, "1000 B")]
        [InlineData(10000, "9,766 KB")]
        [InlineData(1500000, "1,43 MB")]
        [InlineData(2000000000, "1,862 GB")]
        public void GetFileSize_ShouldFormatSize(long size, string expected)
        {
            var blob = A.Fake<Blob>();
            blob.WriteAllBytes(new byte[size]);
            A.CallTo(() => blob.OpenRead()).Returns(new MemoryStream(new byte[size]));
            var media = new MediaData {BinaryData = blob};

            var result = media.GetFileSize();

            Assert.Equal(expected, result);
        }
    }
}