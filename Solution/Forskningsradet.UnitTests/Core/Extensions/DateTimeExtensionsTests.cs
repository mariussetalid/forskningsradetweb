using System;
using System.Globalization;
using Forskningsradet.Common.Constants;
using Forskningsradet.Core.Extensions;
using Forskningsradet.UnitTests.TestUtils;
using Xunit;

namespace Forskningsradet.UnitTests.Core.Extensions
{
    public class DateTimeExtensionsTests
    {
        [Theory]
        [InlineData(DateTimeKind.Unspecified)]
        [InlineData(DateTimeKind.Local)]
        [InlineData(DateTimeKind.Utc)]
        public void ToClosestDateUnspecified_WhenDateTimeKindSet_ShouldBeKindUnspecified(DateTimeKind kind)
        {
            var result = new DateTime(1999, 2, 14, 0, 0, 0, kind).ToClosestDateUnspecified();
            Assert.Equal(DateTimeKind.Unspecified, result.Kind);
        }

        [Fact]
        public void ToClosestDateUnspecified_WhenDateHasTime_ShouldBeOnlyDate()
        {
            var expectedDate = new DateTime(2010, 2, 14);

            var result = new DateTime(2010, 2, 14, 5, 0, 0).ToClosestDateUnspecified();
            Assert.Equal(expectedDate, result);
        }

        [Fact]
        public void ToClosestDateUnspecified_WhenTimeIsCloserToNextDay_ShouldBeNextDate()
        {
            var expectedDate = new DateTime(2010, 2, 15);

            var result = new DateTime(2010, 2, 14, 23, 0, 0).ToClosestDateUnspecified();
            Assert.Equal(expectedDate, result);
        }

        [Fact]
        public void IsPast_WhenDateIsPast_ShouldBePast()
        {
            var result = new DateTime(1999, 2, 14).IsPast();
            Assert.True(result);
        }

        [Fact]
        public void IsPast_WhenDateIsFuture_ShouldNotBePast()
        {
            var result = new DateTime(2100, 3, 14).IsPast();
            Assert.False(result);
        }

        [Fact]
        public void IsPast_WhenDateIsToday_ShouldNotBePast()
        {
            var result = DateTime.Today.IsPast();
            Assert.False(result);
        }

        [Theory]
        [InlineData(2020, 3, 29)]
        [InlineData(2020, 10, 24)]
        [InlineData(2020, 7, 11)]
        public void IsSummerTime_WhenSummerTime_ShouldReturnTrue(int year, int month, int day)
        {
            var dateTime = new DateTime(year, month, day, 1, 0, 0, DateTimeKind.Utc);

            var result = dateTime.IsNorwegianSummerTime();

            Assert.True(result);
        }

        [Theory]
        [InlineData(2020, 3, 28)]
        [InlineData(2020, 10, 25)]
        [InlineData(2020, 12, 30)]
        public void IsSummerTime_WhenWinterTime_ShouldReturnFalse(int year, int month, int day)
        {
            var dateTime = new DateTime(year, month, day, 0, 0, 0, DateTimeKind.Utc);

            var result = dateTime.IsNorwegianSummerTime();

            Assert.False(result);
        }

        [Theory]
        [InlineData(2020, 4, 3, "nb-no", "03.04.2020")]
        [InlineData(2020, 5, 2, "en-us", "02.05.2020")]
        [InlineData(2020, 7, 6, "en-gb", "06.07.2020")]
        public void ToNorwegianDateString_ShouldAlwaysBeNorwegian(int year, int month, int day, string culture, string expected)
        {
            using (new FakeServiceLocator().WithLanguageContext(new CultureInfo(culture)))
            {
                var result = new DateTime(year, month, day, 5, 30, 0)
                    .ToNorwegianDateString();

                Assert.Equal(expected, result);
            }
        }

        [Theory]
        [InlineData(2020, 1, 1, 1, 1, "nb-no", "1. januar 2020, 01:01 CET")]
        [InlineData(2020, 1, 1, 1, 1, "en-us", "1 January 2020, 01:01 CET")]
        [InlineData(2020, 6, 14, 13, 31, "nb-no", "14. juni 2020, 13:31 CEST")]
        [InlineData(2020, 6, 14, 13, 31, "en-us", "14 June 2020, 13:31 CEST")]
        public void DeadlineFormattedWithDaylightSavingNameOrStandardNameEnglish(int year, int month, int day, int hour, int minute, string culture, string expected)
        {
            var cultureInfo = new CultureInfo(culture);
            cultureInfo.DateTimeFormat.TimeSeparator = ":";
            using (new FakeServiceLocator().WithLanguageContext(cultureInfo))
            {
                var result = new DateTime(year, month, day, hour, minute, 0)
                    .DeadlineFormattedWithDaylightSavingNameOrStandardName();

                Assert.Equal(expected, result);
            }
        }

        [Theory]
        [InlineData(2017, 1, 2, 3, 4, "nb-no")]
        [InlineData(2017, 1, 2, 3, 4, "en-us")]
        [InlineData(2018, 6, 14, 13, 31, "nb-no")]
        [InlineData(2018, 6, 14, 13, 31, "en-us")]
        public void DeadlineFormattedWithDaylightSavingNameOrStandardName_ShouldBeEquivalentYearOverYear(int year, int month, int day, int hour, int minute, string culture)
        {
            using (new FakeServiceLocator().WithLanguageContext(new CultureInfo(culture)))
            {
                var yearIncremented = year + 1;

                var result = new DateTime(year, month, day, hour, minute, 0)
                    .DeadlineFormattedWithDaylightSavingNameOrStandardName()
                    .Replace(year.ToString(), "year");

                var resultDifferentYear = new DateTime(yearIncremented, month, day, hour, minute, 0)
                    .DeadlineFormattedWithDaylightSavingNameOrStandardName()
                    .Replace(yearIncremented.ToString(), "year");

                Assert.Equal(result, resultDifferentYear);
            }
        }

        [Theory]
        [InlineData(2017, 5, 7, 8, 9, "nb-no")]
        [InlineData(2018, 8, 13, 12, 21, "nb-no")]
        [UseCulture("nb-NO")]
        public void CultureSpecificDayMonthYear_ShouldSupportNorwegian(int year, int month, int day, int hour, int minute, string culture)
        {
            using (new FakeServiceLocator().WithLanguageContext(new CultureInfo(culture)))
            {
                var dateTime = new DateTime(year, month, day, hour, minute, 0);

                var result = dateTime.CultureSpecificDayMonthYear();

                Assert.Equal(dateTime.ToString(DateTimeFormats.NorwegianDayMonth) + " " + dateTime.Year.ToString(), result);
            }
        }

        [Theory]
        [InlineData(2017, 5, 7, 8, 9, "en-us")]
        [InlineData(2018, 8, 13, 12, 21, "en-us")]
        [UseCulture("en-US")]
        public void CultureSpecificDayMonthYear_ShouldSupportEnglish(int year, int month, int day, int hour, int minute, string culture)
        {
            using (new FakeServiceLocator().WithLanguageContext(new CultureInfo(culture)))
            {
                var dateTime = new DateTime(year, month, day, hour, minute, 0);

                var result = dateTime.CultureSpecificDayMonthYear();

                Assert.Equal(dateTime.ToString(DateTimeFormats.EnglishDayMonthYear), result);
            }
        }

        [Theory]
        [InlineData(2017, 5, 7, 8, 9, "nb-no")]
        [InlineData(2018, 8, 13, 12, 21, "nb-no")]
        public void CultureSpecificDay_ShouldSupportNorwegian(int year, int month, int day, int hour, int minute, string culture)
        {
            using (new FakeServiceLocator().WithLanguageContext(new CultureInfo(culture)))
            {
                var dateTime = new DateTime(year, month, day, hour, minute, 0);

                var result = dateTime.CultureSpecificDay();

                Assert.Equal(dateTime.ToString(DateTimeFormats.NorwegianDateDayLong), result);
            }
        }

        [Theory]
        [InlineData(2017, 5, 7, 8, 9, "en-us")]
        [InlineData(2018, 8, 13, 12, 21, "en-us")]
        public void CultureSpecificDay_ShouldSupportEnglish(int year, int month, int day, int hour, int minute, string culture)
        {
            using (new FakeServiceLocator().WithLanguageContext(new CultureInfo(culture)))
            {
                var dateTime = new DateTime(year, month, day, hour, minute, 0);

                var result = dateTime
                    .CultureSpecificDay();

                Assert.Equal(dateTime.ToString(DateTimeFormats.DateDayLong), result);
            }
        }

        [Theory]
        [InlineData(2017, 1, 2, 3, 4, "nb-no")]
        [InlineData(2017, 1, 2, 3, 4, "en-us")]
        [InlineData(2018, 6, 14, 13, 31, "nb-no")]
        [InlineData(2018, 6, 14, 13, 31, "en-us")]
        public void CultureSpecificDay_ShouldBeEquivalentYearOverYear(int year, int month, int day, int hour, int minute, string culture)
        {
            using (new FakeServiceLocator().WithLanguageContext(new CultureInfo(culture)))
            {
                var yearIncremented = year + 1;

                var result = new DateTime(year, month, day, hour, minute, 0)
                    .CultureSpecificDay();

                var resultDifferentYear = new DateTime(yearIncremented, month, day, hour, minute, 0)
                    .CultureSpecificDay();

                Assert.Equal(result, resultDifferentYear);
            }
        }
    }
}