using Forskningsradet.Core.Extensions;
using Xunit;

namespace Forskningsradet.UnitTests.Core.Extensions
{
    public class StringExtensionsTests
    {
        [Theory]
        [InlineData("test")]
        [InlineData("test-test")]
        [InlineData("test_test")]
        [InlineData("test1test")]
        public void EnsureValidHtmlId_ReturnsValidIds(string id)
        {
            var modified = id.EnsureValidHtmlId();
            Assert.Equal(id, modified);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("1test")]
        [InlineData("_test")]
        [InlineData("�test")]
        public void EnsureValidHtmlId_DiscardsInvalidIds(string id)
        {
            var modified = id.EnsureValidHtmlId();
            Assert.Null(modified);
        }

        [Fact]
        public void EnsureValidHtmlId_EncodesValuesForAttributes()
        {
            var id = "test\"<script>\"test";
            var modified = id.EnsureValidHtmlId();
            Assert.Equal("test&quot;&lt;script>&quot;test", modified);
        }

        [Fact]
        public void EnsureValidEnglishWindowsFileName_ReplacesNorwegianLetters()
        {
            var name = "������";
            var modified = name.EnsureValidEnglishWindowsFileName();
            Assert.Equal("______", modified);
        }

        [Fact]
        public void EnsureValidEnglishWindowsFileName_RemovesInvalidFileNameChars()
        {
            var name = "Na<m|e";
            var modified = name.EnsureValidEnglishWindowsFileName();
            Assert.Equal("Name", modified);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData("av")]
        [InlineData("den")]
        [InlineData("er")]
        [InlineData("i")]
        public void RemoveNorwegianStopwords_RemovesStopwords(string text)
        {
            var modified = text.RemoveNorwegianStopwords();
            Assert.Equal(string.Empty, modified);
        }

        [Theory]
        [InlineData("avtale", "avtale")]
        [InlineData("verden", "verden")]
        [InlineData("energi", "energi")]
        public void RemoveNorwegianStopwords_DoesNotRemoveStopwordsAsPartOfWords(string text, string expected)
        {
            var modified = text.RemoveNorwegianStopwords();
            Assert.Equal(expected, modified);
        }

        [Theory]
        [InlineData("av.", "av.")]
        [InlineData("er:", "er:")]
        [InlineData("-i", "-i")]
        public void RemoveNorwegianStopwords_DoesNotRemoveStopwordsWhenNotSeparatedBySpaces(string text, string expected)
        {
            var modified = text.RemoveNorwegianStopwords();
            Assert.Equal(expected, modified);
        }
    }
}