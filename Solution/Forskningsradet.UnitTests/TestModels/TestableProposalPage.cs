﻿using System;
using System.Globalization;
using EPiServer.Core;
using Forskningsradet.Core.Models.ContentModels.Pages;

namespace Forskningsradet.UnitTests.Builders
{
    public class TestableProposalPage : ProposalPage
    {
        public override ContentReference ContentLink { get; set; }
        public override CategoryList Category { get; set; }
        public override string Name { get; set; }
        public override DateTime Changed { get; set; }
        public override CultureInfo Language { get; set; }
    }
}
