﻿using System.Collections.Generic;
using EPiServer.Core;

namespace Forskningsradet.UnitTests.Builders
{
    public class TestableContentArea : ContentArea
    {
        public override IEnumerable<ContentAreaItem> FilteredItems => SettableFilteredItemsForTest;

        public IEnumerable<ContentAreaItem> SettableFilteredItemsForTest { get; set; }
    }
}
