using System;
using System.Net;
using System.Net.Http;
using System.Web.Http.Controllers;
using Forskningsradet.Web.Infrastructure;
using Xunit;

namespace Forskningsradet.UnitTests.Web.Infrastructure
{
    public class RequireHttpsAttributeTests
    {
        [Fact]
        public void OnAuthorize_WhenUriSchemeIsHttps_ShouldNotAlterTheResponse()
        {
            var attribute = new RequireHttpsAttribute();
            var context = BuildContext("https://www.forskningsradet.no");

            attribute.OnAuthorization(context);
            
            Assert.Null(context.Response);
        }

        [Theory]
        [InlineData("http://www.forskningsradet.no")]
        [InlineData("http://forskningsradet.no")]
        public void OnAuthorize_WhenUriSchemeIsNotHttps_ShouldReturnForbiddenResponse(string uri)
        {
            var attribute = new RequireHttpsAttribute();
            var context = BuildContext(uri);

            attribute.OnAuthorization(context);
            
            Assert.NotNull(context.Response);
            Assert.Equal(HttpStatusCode.Forbidden, context.Response.StatusCode);
        }
        
        private static HttpActionContext BuildContext(string uri)
        {
            var context = new HttpActionContext();
            var request = new HttpRequestMessage(HttpMethod.Get, new Uri(uri));

            context.ControllerContext = new HttpControllerContext {Request = request};
            return context;
        }
    }
}