using System;
using Forskningsradet.Core.Models.ApiModels.Evurdering.Converters;
using Newtonsoft.Json;
using Xunit;

namespace Forskningsradet.UnitTests.Web.Infrastructure
{
    public class EpochDateTimeConverterTests
    {
        [Fact]
        public void ReadJson_WithValidEpochTimestamp_ShouldConvertDateTime()
        {
            var expectedDate = new DateTime(2018, 12, 18, 12, 49, 22, 0, DateTimeKind.Utc);

            var result = JsonConvert.DeserializeObject<TestObject>("{\"Test\": 1545137362000}");
            
            Assert.Equal(expectedDate, result.Test);
        }
        
        [Fact]
        public void ReadJson_WithEmptyValue_ShouldConvertToNull()
        {
            var result = JsonConvert.DeserializeObject<TestObject>("{\"Test\": null}");
            
            Assert.Null(result.Test);
        }
        
        [Fact]
        public void ReadJson_WithWrongType_ShouldReturnNull()
        {
            var result = JsonConvert.DeserializeObject<TestObject>("{\"Test\": \"y0\"}");

            Assert.Null(result.Test);
        }

        [Fact]
        public void WriteJson_WithEpochStart_ShouldReturnEpochTime()
        {
            var result = JsonConvert.SerializeObject(new TestObject {Test = new DateTime(1970, 1, 1)});
            
            Assert.Equal("{\"Test\":0}", result);
        }
        
        [Fact]
        public void WriteJson_WithValidDate_ShouldReturnEpochTime()
        {
            var result = JsonConvert.SerializeObject(new TestObject {Test = new DateTime(2019, 3, 29, 11, 8, 7, 55)});
            
            Assert.Equal("{\"Test\":1553857687055}", result);
        }
        
        private class TestObject
        {
            [JsonConverter(typeof(EpochDateTimeConverter))]
            public DateTime? Test { get; set; }
        }
    }
}