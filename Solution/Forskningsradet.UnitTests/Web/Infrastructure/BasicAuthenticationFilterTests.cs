using System;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web.Http.Controllers;
using FakeItEasy;
using Forskningsradet.Core.Services;
using Forskningsradet.UnitTests.TestUtils;
using Forskningsradet.Web.Infrastructure;
using Xunit;

namespace Forskningsradet.UnitTests.Web.Infrastructure
{
    public class BasicAuthenticationFilterTests
    {
        private readonly IApiKeyStore _apiKeyStore;
        private const string ValidScheme = "Basic";
        private const string ValidUserName = "user1";
        private const string ValidPassword = "pa$$word";

        public BasicAuthenticationFilterTests()
        {
            _apiKeyStore = A.Fake<IApiKeyStore>();
        }

        [Fact]
        public void OnAuthorization_WithValidHeaderAndCredentials_UserShouldBeAuthenticated()
        {
            using (new FakeServiceLocator().WithApiKeyStore(_apiKeyStore))
            {
                A.CallTo(() => _apiKeyStore.FindSecretByUsername(ValidUserName)).Returns(ValidPassword);

                var filter = new BasicAuthenticationFilter();
                var context = BuildContext(ValidScheme, ValidUserName, ValidPassword);

                filter.OnAuthorization(context);

                Assert.Equal(ValidUserName, Thread.CurrentPrincipal.Identity.Name);
            }
        }

        [Fact]
        public void OnAuthorization_WhenNoAuthHeaderIsProvided_ShouldReturnChallenge()
        {
            var filter = new BasicAuthenticationFilter();
            var context = BuildContext(ValidScheme, ValidUserName, ValidPassword);
            context.Request.Headers.Clear();

            filter.OnAuthorization(context);

            AssertChallenge(context);
        }

        [Fact]
        public void OnAuthorization_AuthenticationSchemeIsNotBasic_ShouldReturnChallenge()
        {
            var filter = new BasicAuthenticationFilter();
            var context = BuildContext("VisualBasic", ValidUserName, ValidPassword);

            filter.OnAuthorization(context);

            AssertChallenge(context);
        }

        [Fact]
        public void OnAuthorization_WithNoCredentials_ShouldReturnChallenge()
        {
            var filter = new BasicAuthenticationFilter();
            var context = BuildContext(ValidScheme, null, null);

            filter.OnAuthorization(context);

            AssertChallenge(context);
        }

        [Fact]
        public void OnAuthorization_WithInvalidCredentials_ShouldReturnChallenge()
        {
            using (new FakeServiceLocator().WithApiKeyStore(_apiKeyStore))
            {
                const string badUsername = "user2";
                const string badPassword = "pa€€word";

                A.CallTo(() => _apiKeyStore.FindSecretByUsername(badUsername)).Returns(null);

                var filter = new BasicAuthenticationFilter();
                var context = BuildContext(ValidScheme, badUsername, badPassword);

                filter.OnAuthorization(context);

                AssertChallenge(context);
                A.CallTo(() => _apiKeyStore.FindSecretByUsername(A<string>.That.Matches(s => s == badUsername)))
                    .MustHaveHappened();
            }
        }

        [Fact]
        public void OnAuthorization_WithEmptyPassword_ShouldReturnChallenge()
        {
            using (new FakeServiceLocator().WithApiKeyStore(_apiKeyStore))
            {
                const string badPassword = null;

                A.CallTo(() => _apiKeyStore.FindSecretByUsername(ValidUserName)).Returns(ValidPassword);

                var filter = new BasicAuthenticationFilter();
                var context = BuildContext(ValidScheme, ValidUserName, badPassword);

                filter.OnAuthorization(context);

                AssertChallenge(context);
                A.CallTo(() => _apiKeyStore.FindSecretByUsername(A<string>.That.Matches(s => s == ValidUserName)))
                    .MustHaveHappened();
            }
        }

        private static void AssertChallenge(HttpActionContext context)
        {
            Assert.Equal(HttpStatusCode.Unauthorized, context.Response.StatusCode);
            Assert.Equal("Basic realm=\"forskningsradet.no\"", context.Response.Headers.WwwAuthenticate.ToString());
        }

        private static HttpActionContext BuildContext(string scheme, string username, string password)
        {
            var context = new HttpActionContext();
            var request = new HttpRequestMessage();

            var credentials = username != null
                ? Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes($"{username}:{password}"))
                : string.Empty;

            var authHeader = $"{scheme} {credentials}";

            request.Headers.Add("Authorization", authHeader);

            context.ControllerContext = new HttpControllerContext { Request = request };
            return context;
        }
    }
}