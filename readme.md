# Forskningsrådet Web

The solution for the website for Forskningsrådet.

## Getting started

**NOTE:** This getting started guide may not be complete.

### Building

To run the project you need to build the c# project and the frontend separately.

#### Backend code

Build from you IDE or using the command line (requires [nuget](https://www.nuget.org/downloads))

```
nuget install Solution\Forskningsradet.Web\packages.config -o Solution\packages
msbuild Solution\Forskningsradet.Web.sln
```

#### Frontend code

You need Node.js, [yarn](https://yarnpkg.com/lang/en/), Python and `windows-build-tools` or XCode command line tools in order to build the frontend. More info can be found in the [frontend readme](Solution/Forskningsradet.Web/Frontend/README.md).

Using yarn

```
cd Solution/Forskningsradet.Web/Frontend
yarn && yarn build
```

Using powershell

```
Solution/Forskningsradet.Web/frontend-ci.ps1
```

##### Dependencies

The build server is at the time of writing set up to run `yarn audit` before building this codebase, which means if there are known vulnerabilities in `node_modules` these _have_ to be fixed before new code can be deployed to production.

### Running the site

Before you can run the site you need to set up

#### IIS

Add a IIS site pointing to the web project folder at `Solution/Forskningsradet.Web`. The site needs to run with https at port 44390 in order for the Azure AD login work.

#### License

In order to use Episerver and Episerver Find you need a license for both. Place them in the `Solution/Forskningsradet.Web` folder and name them `License.config` and `EPiServerFind.local.config` respectively.

#### Local config

In addition to the license you need to create a SQL Server database and add the following to your `connectionStrings.local.config`

```
<connectionStrings>
    <add name="EPiServerDB" providerName="System.Data.SqlClient" connectionString="Server=.;Database=Forskningsradet;User ID=fr_web;Password=YOUR_PASSWORD;MultipleActiveResultSets=true" />
</connectionStrings>
```

#### EvoPDF service

This is not needed to run the site, but required if you want to use any download-this-page-as-pdf features. The site uses EvoPDF to generate PDFs. This needs to run as a windows service on your local computer. To get this up and running, download the [HTML to PDF converter for Azure](https://www.evopdf.com/azure-html-to-pdf-converter.aspx) and follow the instructions for setting the tool up as a windows service (outlined in a readme file in the downloaded zip file).
**Note:** in late 2019 a new version was published and version 7.5.0 was unlisted. Here's the link to setup EvoPDF v7.5 on a local windows computer: [Azure Html to pdf converter v7.5.0](https://www.evopdf.com/downloads/EvoHtmlToPdf-Azure-v7.5.zip)

## Developer notes

Various practices are documented here to keep consistency in the solution. Make sure that any changes to this document is reflected throughout the solution.

### Pull requests to `develop`

All changes made to the `develop` branch should go through a pull request.

### Episerver `GroupNames`

When the `DisplayAttribute` `GroupNames` is used for a property, then `GroupNames` should be set explicitly for all properties on that `ContentType`.

### Consistent Episerver GUID Format

The GUID used in the `ContentTypeAttribute` should be formatted in upper-case letters and only surrounded by quotes, no brackets. This can be easily generated in VS by using the ReSharper shortcut `nguid`.

Example: `EA278D6C-598F-4FEA-A34E-332E3C65BDD8`

### Use of `PageEditing.PageIsInEditMode`

Use of `PageEditing.PageIsInEditMode` directly should only be needed from a `Controller` or a `View`. It can then be passed down to various services and mappers.

### Passing parameters to `ViewModelMapper`s

Parameters passed to `ViewModelMapper`s should be wrapped in a model of type `QueryParameterBase`.

### Passing parameters from `ViewModelMapper`s to services
Parameters passed from `ViewModelMapper`s to services should preferably be either wrapped in a model under the namespace `Forskningsradet.Core.Models.RequestModels` or passed without wrapping them in a model. Currently several services actually wrap the parameters in the `QueryParameterBase` sent to the `ViewModelMapper`. This creates an unnecessary coupling between the service and controller, and should be avoided.
